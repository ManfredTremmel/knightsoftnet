// Pruefung der Formulareingabe bei Suche nach Nachhilfelehrern/Innen

function chkSuchen() {

  if (document.forms[0].Vorwahl.value.length == 0) {
    alert("Bitte geben Sie Ihre Telefonvorwahl ein");
    document.forms[0].Vorwahl.focus();
    return false;
  }

  chkZ = 1;
  for (i = 0; i < document.forms[0].Vorwahl.value.length; ++i) {
    if ((document.forms[0].Vorwahl.value.charAt(i) < "0" || document.forms[0].Vorwahl.value.charAt(i) > "9")
        && document.forms[0].Vorwahl.value.charAt(i) != "*") {
      chkZ = -1;
    }
  }
  if (chkZ == -1) {
    alert("Die Telefonvorwahl ist nicht nummerisch. Bitte geben Sie einen nummerischen Wert ein");
    document.forms[0].Vorwahl.focus();
    return false;
  }

  if ((document.forms[0].Fach.options[0].selected == true || document.forms[0].Fach.options[17].selected == true)
      && document.forms[0].FachAlter.value.length == 0) {
    alert("Bitte waehlen Sie ein Fach aus");
    document.forms[0].Fach.focus();
    return false;
  }

  if (document.forms[0].JahrSemSel.options[0].selected == true) {
    if (document.forms[0].JahrSem.value.length == 0)
      ;
    else {
      chkZ = 1;
      for (i = 0; i < document.forms[0].JahrSem.value.length; ++i) {
        if ((document.forms[0].JahrSem.value.charAt(i) < "0" || document.forms[0].JahrSem.value.charAt(i) > "9")
            && document.forms[0].JahrSem.value.charAt(i) != "*") {
          chkZ = -1;
        }
      }
      if (chkZ == -1) {
        alert("Der vorgegebene Jahrgang ist nicht nummerisch. Bitte geben Sie einen nummerischen Wert ein");
        document.forms[0].JahrSem.focus();
        return false;
      }
    }

  } else {
    if (document.forms[0].JahrSem.value.length == 0)
      ;
    else {
      chkZ = 1;
      for (i = 0; i < document.forms[0].JahrSem.value.length; ++i) {
        if ((document.forms[0].JahrSem.value.charAt(i) < "0" || document.forms[0].JahrSem.value.charAt(i) > "9")
            && document.forms[0].JahrSem.value.charAt(i) != "*") {
          chkZ = -1;
        }
      }
      if (chkZ == -1) {
        alert("Das vorgegebene Semester ist nicht nummerisch. Bitte geben Sie einen nummerischen Wert ein");
        document.forms[0].JahrSem.focus();
        return false;
      }
    }
  }
  return true;
}
