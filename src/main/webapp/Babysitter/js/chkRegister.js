// Pruefung der Formulareingabe bei Insert oder Aenderung

function chkRegister() {
  if (document.forms[0].Vorwahl.value.length == 0) {
    alert("Bitte geben Sie ihre Telefonvorwahl an");
    document.forms[0].Vorwahl.focus();
    return false;
  }
  chkZ = 1;
  for (i = 0; i < document.forms[0].Vorwahl.value.length; ++i) {
    if (document.forms[0].Vorwahl.value.charAt(i) < "0" || document.forms[0].Vorwahl.value.charAt(i) > "9") {
      chkZ = -1;
    }
  }
  if (chkZ == -1) {
    alert("Die Vorwahl ist nicht nummerisch. Bitte geben Sie einen nummerischen Wert ein");
    document.forms[0].Vorwahl.focus();
    return false;
  }

  if (document.forms[0].Geburtsjahr.value.length == 0) {
    alert("Bitte geben Sie ihr Geburtsjahr an");
    document.forms[0].Geburtsjahr.focus();
    return false;
  }
  chkZ = 1;
  for (i = 0; i < document.forms[0].Geburtsjahr.value.length; ++i) {
    if (document.forms[0].Geburtsjahr.value.charAt(i) < "0" || document.forms[0].Geburtsjahr.value.charAt(i) > "9") {
      chkZ = -1;
    }
  }
  if (chkZ == -1) {
    alert("Das Geburtsjahr ist nicht nummerisch. Bitte geben Sie einen nummerischen Wert ein");
    document.forms[0].Geburtsjahr.focus();
    return false;
  }

  return chkRegisterAllgemein();
}
