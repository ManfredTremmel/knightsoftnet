// Dynamische Buttons im Navigationsframe
var nof = parent.frames[1].document;

var endung = new Array("_normal.gif", "_highlighted.gif", "_pressed.gif");

var laenge_end2 = endung[2].length;

var pfad = "/pics/";

var KSBildnamen = new Array("Startseite", "Suchen", "Registrieren", "Login",
// "Gewinnspiel",
"Impressum", "FAQ");

var KSBild = new Array(KSBildnamen.length);

var KSBildstat =
    new Array("Klicken Sie hier um zur Startseite zu gelangen",
        "Klicken Sie hier um die Suchfunktion zur Suchfunktion zu gelangen",
        "Klicken Sie hier um sich in die Datenbank einzutragen", "Klicken Sie hier um Ihre Daten zu &auml;ndern",
        // "Klicken Sie hier um am Gewinnspiel teilzunehmen",
        "Klicken Sie hier um das Impressum zu lesen",
        "Klicken Sie hier um die Antworten zu den h&auml;ufig gestellten Fragen zu erhalten");

for (i = 0; i < KSBild.length; i++) {
  KSBild[i] = new Array(3);
  for (j = 0; j < endung.length; j++) {
    KSBild[i][j] = new Image();
    KSBild[i][j].src = pfad + KSBildnamen[i] + endung[j];
  }
}

function ButtonOnMouseOver(Seitenname) {
  for (i = 0; KSBildnamen[i] != Seitenname; i++);
  window.status = KSBildstat[i];
  return Bildwechsel(i, 1, 0);
}

function ButtonOnMouseDown(Seitenname) {
  for (i = 0; KSBildnamen[i] != Seitenname; i++);
  return Bildwechsel(i, 2, 0);
}

function ButtonOnMouseOut(Seitenname) {
  for (i = 0; KSBildnamen[i] != Seitenname; i++);
  window.status = '';
  return Bildwechsel(i, 0, 0);
}

function AktiviereSeite(Seitenname) {
  for (i = 0; KSBildnamen[i] != Seitenname; i++);
  return Bildwechsel(i, 2, 1);
}

function Bildwechsel(Bildnr, Bildart, Wechsel) {
  if (nof != null && nof.images != null && Bildnr < nof.images.length) {
    if (Wechsel == 1) {
      for (i = 0; i < KSBildnamen.length; i++) {
        if (Bildnr != i)
          nof.images[i].src = KSBild[i][0].src;
      }
      nof.images[Bildnr].src = KSBild[Bildnr][Bildart].src;
    } else {
      if (nof.images[Bildnr].src != KSBild[Bildnr][2].src)
        nof.images[Bildnr].src = KSBild[Bildnr][Bildart].src;
    }
  }
  return true;
}
