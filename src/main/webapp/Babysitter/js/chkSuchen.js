// Pruefung der Formulareingabe fuer Suche

function chkSuchen() {

  if (document.forms[0].Suchstring.value.length == 0) {
    alert("Bitte geben Sie einen Suchstring ein");
    document.forms[0].Suchstring.focus();
    return false;
  }

  chkZ = 1;
  for (i = 0; i < document.forms[0].Suchstring.value.length; ++i) {
    if ((document.forms[0].Suchstring.value.charAt(i) < "0" || document.forms[0].Suchstring.value.charAt(i) > "9")
        && document.forms[0].Suchstring.value.charAt(i) != "*") {
      chkZ = -1;
    }
  }
  if (chkZ == -1) {
    alert("Der Suchstring ist nicht nummerisch. Bitte geben Sie einen nummerischen Wert ein");
    document.forms[0].Suchstring.focus();
    return false;
  }

  return true;
}
