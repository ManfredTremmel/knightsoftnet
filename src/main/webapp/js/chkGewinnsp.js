// Pruefung der Formulareingabe bei Gewinnspieleingaben

function chkGewinnsp() {

  chkZ = -1;
  for (i = 0; i < document.forms[0].eMail.value.length; ++i) {
    if (document.forms[0].eMail.value.charAt(i) == "@")
      chkZ = i;
  }
  if (chkZ == -1) {
    alert("Bitte geben Sie eine gueltige E-Mail-Adresse ein");
    document.forms[0].eMail.focus();
    return false;
  }
  chkZ = 1;
  for (i = chkZ; i < document.forms[0].eMail.value.length; ++i) {
    if (document.forms[0].eMail.value.charAt(i) == ".")
      chkZ = -1;
  }
  if (chkZ != -1) {
    alert("Bitte geben Sie eine gueltige E-Mail-Adresse ein");
    document.forms[0].eMail.focus();
    return false;
  }

  if (document.forms[0].Name.value.length == 0) {
    alert("Bitte geben Sie ihren Nachnamen an");
    document.forms[0].Name.focus();
    return false;
  }

  if (document.forms[0].Vorname.value.length == 0) {
    alert("Bitte geben Sie ihren Vornamen an");
    document.forms[0].Vorname.focus();
    return false;
  }

  if (document.forms[0].Strasse.value.length == 0) {
    alert("Bitte geben Sie ihre vollstaendige Adresse an");
    document.forms[0].Strasse.focus();
    return false;
  }

  if (document.forms[0].PLZ.value.length < 4) {
    alert("Bitte geben Sie ihre vollstaendige Adresse an");
    document.forms[0].PLZ.focus();
    return false;
  }
  chkZ = 1;
  for (i = 0; i < document.forms[0].PLZ.value.length; ++i) {
    if (document.forms[0].PLZ.value.charAt(i) < "0" || document.forms[0].PLZ.value.charAt(i) > "9") {
      chkZ = -1;
    }
  }
  if (chkZ == -1) {
    alert("Die Postleitzahl ist nicht nummerisch. Bitte geben Sie einen nummerischen Wert ein");
    document.forms[0].PLZ.focus();
    return false;
  }

  if (document.forms[0].Ort.value.length == 0) {
    alert("Bitte geben Sie ihre vollstaendige Adresse an");
    document.forms[0].Ort.focus();
    return false;
  }

  return true;
}
