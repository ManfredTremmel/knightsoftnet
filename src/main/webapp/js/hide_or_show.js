function hide_or_show(element_id) {
  var element = document.getElementById(element_id);
  if (element.style.visibility == "hidden") {
    element.style.height = "100%";
    element.style.visibility = "visible";
  } else {
    element.style.height = "1px";
    element.style.visibility = "hidden";
  }
}
