<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/XSL/Transform/1.0">

	<xsl:template match="faq">
		<xsl:processing-instruction name="cocoon-format">type="text/html"</xsl:processing-instruction>
		<html>
			<head>
				<xsl:apply-templates select="title" mode="head"/>
				<xsl:apply-templates select="js"/>
			</head>
			<body onload="javascript:AktiviereSeite('FAQ');">
				<!-- <xsl:apply-templates select="banner"/> -->
				<xsl:apply-templates select="title" mode="line"/>

				<xsl:apply-templates select="inhalt" mode="directory"/>
				<xsl:apply-templates select="inhalt" mode="text"/>
			</body>
		</html>
	</xsl:template>

	<xsl:template match="br">
		<br></br>
	</xsl:template>

	<xsl:template match="inhalt" mode="directory">
		<h1><a name="inhalt"><xsl:value-of select="/faq/directorytitle"/></a></h1>
		<ol>
			<xsl:apply-templates select="thema" mode="directory"/>
		</ol>
	</xsl:template>

	<xsl:template match="thema" mode="directory">
		<li><a href="#{themenkurzref}"><xsl:value-of select="themenbezeichnung"/></a>
			<ul>
				<xsl:apply-templates select="fragenblock" mode="directory"/>
			</ul>
		</li>
	</xsl:template>

	<xsl:template match="fragenblock" mode="directory">
		<li><a href="#{fragenkurzref}"><xsl:value-of select="frage"/></a></li>
	</xsl:template>

	<xsl:template match="inhalt" mode="text">
		<xsl:apply-templates select="thema" mode="text"/>
	</xsl:template>

	<xsl:template match="thema" mode="text">
		<h2><a name="{themenkurzref}"><xsl:value-of select="themenbezeichnung"/></a></h2>
		<xsl:apply-templates select="fragenblock" mode="text"/>
	</xsl:template>

	<xsl:template match="fragenblock" mode="text">
		<h4><a name="{fragenkurzref}"><xsl:value-of select="frage"/></a></h4>
		<xsl:apply-templates select="antwort" mode="text"/>
		<p style="text-align:right"><a href="#inhalt"><xsl:value-of select="/faq/back"/></a></p>
	</xsl:template>

	<xsl:template match="antwort" mode="text">
		<xsl:apply-templates select="paragraph" mode="text"/>
	</xsl:template>

	<xsl:template match="paragraph" mode="text">
		<p><xsl:apply-templates/></p>
	</xsl:template>

	<xsl:template match="link">
		<a href="{@ref}"><xsl:apply-templates/></a>
	</xsl:template>

	<xsl:template match="mailto">
		<a href="mailto:{@ref}"><xsl:apply-templates/></a>
	</xsl:template>

	<xsl:template match="title" mode="line">
		<table width="100%">
			<tr>
				<td class="ueb"><xsl:apply-templates/></td>
			</tr>
		</table>
	</xsl:template>

	<xsl:template match="title" mode="head">
		<!-- Copyright 1999, 2000 by KnightSoft IT-Services oHG -->
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<meta name="MSSmartTagsPreventParsing" content="TRUE"/>
		<title><xsl:apply-templates/></title>
		<link rel="stylesheet" type="text/css" href="/css/default.css"></link>
	</xsl:template>

	<xsl:template match="js">
		<script src="{src}" type="text/javascript"></script>
	</xsl:template>

	<xsl:template match="banner">
		<table width="100%">
			<tr>
				<td rowspan="2">&#160;</td>
				<td style="width:{width}px;"><p class="anzeige"><xsl:value-of select="marcalt"/></p></td>
				<td rowspan="2">&#160;</td>
			</tr>
			<tr>
				<td>
					<!-- {commentbeginn} -->
					<a href="{link}">
						<img src="{picture}" width="{width}" height="{height}" alt="{alt}"></img>
					</a>
					<!-- {commentend} -->
				</td>
			</tr>
		</table>
	</xsl:template>

	<xsl:template match="text()">
		<xsl:value-of select="." />
	</xsl:template>

</xsl:stylesheet>
