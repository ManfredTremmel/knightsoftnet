// Pruefung der Formulareingabe beim Suchen

	function chkSuchen()
	{

		if(document.forms[0].suchezielplz.value.length == 0 &&
		   document.forms[0].suchezielort.value.length == 0)
		{
			alert("Bitte geben Sie einen Ort oder dessen Postleitzahl ein");
			document.forms[0].suchezielplz.focus();
			return false;
		}

		if(document.forms[0].suchezielplz.value.length > 0 &&
		   document.forms[0].suchezielplz.value.length < 3)
		{
			alert("Bitte geben Sie eine g�ltige Postleitzahl ein");
			document.forms[0].suchezielplz.focus();
			return false;
		}
				
		chkZ = 1;
		for(i=0;i<document.forms[0].suchezielplz.value.length;++i)
		{
			if((document.forms[0].suchezielplz.value.charAt(i) < "0"   ||
				 document.forms[0].suchezielplz.value.charAt(i) > "9")  &&
				document.forms[0].suchezielplz.value.charAt(i) != "*")
			{
		  		chkZ = -1;
			}
		}
		if(chkZ == -1)
		{
			alert("Die gesuchte Postleitzahl ist nicht nummerisch. Bitte geben Sie einen nummerischen Wert ein");
			document.forms[0].suchezielplz.focus();
			return false;
		}

		return true;
	}
