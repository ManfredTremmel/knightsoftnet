// Pruefung der Formulareingabe bei der Registrierung

function chkRegister() {
  // Einmalige Fahrt
  if (document.forms[0].einmalige_fahrt.checked == true) {

    if (document.forms[0].abfahrtsdatum_tt.value.length < 1) {
      alert("Bitte geben Sie das Abfahrtsdatum ein (z.B. 01.01.1999)");
      document.forms[0].abfahrtsdatum_tt.focus();
      return false;
    }
    chkZ = 1;
    for (i = 0; i < document.forms[0].abfahrtsdatum_tt.value.length; ++i) {
      if (document.forms[0].abfahrtsdatum_tt.value.charAt(i) < "0" || document.forms[0].abfahrtsdatum_tt.value.charAt(i) > "9") {
        chkZ = -1;
      }
    }
    if (chkZ == -1) {
      alert("Das Abfahrtsdatum ist nicht nummerisch. Bitte geben Sie einen nummerischen Wert ein");
      document.forms[0].abfahrtsdatum_tt.focus();
      return false;
    }
    if (document.forms[0].abfahrtsdatum_tt.value > 31 || document.forms[0].abfahrtsdatum_tt.value == 00) {
      alert("Bitte geben Sie ein g�ltiges Abfahrtsdatum ein (z.B. 01.01.1999)");
      document.forms[0].abfahrtsdatum_tt.focus();
      return false;
    }

    if (document.forms[0].abfahrtsdatum_mm.value.length < 1) {
      alert("Bitte geben Sie das Abfahrtsdatum ein (z.B. 01.01.1999)");
      document.forms[0].abfahrtsdatum_mm.focus();
      return false;
    }
    chkZ = 1;
    for (i = 0; i < document.forms[0].abfahrtsdatum_mm.value.length; ++i) {
      if (document.forms[0].abfahrtsdatum_mm.value.charAt(i) < "0" || document.forms[0].abfahrtsdatum_mm.value.charAt(i) > "9") {
        chkZ = -1;
      }
    }
    if (chkZ == -1) {
      alert("Das Abfahrtsdatum ist nicht nummerisch. Bitte geben Sie einen nummerischen Wert ein");
      document.forms[0].abfahrtsdatum_mm.focus();
      return false;
    }
    if (document.forms[0].abfahrtsdatum_mm.value > 12 || document.forms[0].abfahrtsdatum_mm.value == 00) {
      alert("Bitte geben Sie ein g�ltiges Abfahrtsdatum ein (z.B. 01.01.1999)");
      document.forms[0].abfahrtsdatum_mm.focus();
      return false;
    }


    if (document.forms[0].abfahrtsdatum_jjjj.value.length < 4) {
      alert("Bitte geben Sie das Abfahrtsdatum ein (z.B. 01.01.1999)");
      document.forms[0].abfahrtsdatum_jjjj.focus();
      return false;
    }
    chkZ = 1;
    for (i = 0; i < document.forms[0].abfahrtsdatum_jjjj.value.length; ++i) {
      if (document.forms[0].abfahrtsdatum_jjjj.value.charAt(i) < "0"
          || document.forms[0].abfahrtsdatum_jjjj.value.charAt(i) > "9") {
        chkZ = -1;
      }
    }
    if (chkZ == -1) {
      alert("Das Abfahrtsdatum ist nicht nummerisch. Bitte geben Sie einen nummerischen Wert ein");
      document.forms[0].abfahrtsdatum_jjjj.focus();
      return false;
    }
  }

  // Abfahrts-PLZ
  if (document.forms[0].abfahrtsplz.value.length < 4) {
    alert("Bitte geben Sie die Postleitzahl des Abfahrtsortes ein");
    document.forms[0].abfahrtsplz.focus();
    return false;
  }
  chkZ = 1;
  for (i = 0; i < document.forms[0].abfahrtsplz.value.length; ++i) {
    if (document.forms[0].abfahrtsplz.value.charAt(i) < "0" || document.forms[0].abfahrtsplz.value.charAt(i) > "9") {
      chkZ = -1;
    }
  }
  if (chkZ == -1) {
    alert("Die Postleitzahl des Abfahrtsorts ist nicht nummerisch. Bitte geben Sie einen nummerischen Wert ein");
    document.forms[0].abfahrtsplz.focus();
    return false;
  }
  // Ort
  if (document.forms[0].abfahrtsort.value.length == 0) {
    alert("Der Abfahrtsort fehlt");
    document.forms[0].abfahrtsort.focus();
    return false;
  }
  // Abfahrtszeit
  if (document.forms[0].abfahrtszeit_hh.value.length > 0 || document.forms[0].abfahrtszeit_mm.value.length > 0) {
    if (document.forms[0].abfahrtszeit_hh.value.length == 0) {
      alert("Bitte geben Sie eine g�ltige Abfahrtszeit ein (z.B. 12:45)");
      document.forms[0].abfahrtszeit_hh.focus();
      return false;
    }
    if (document.forms[0].abfahrtszeit_mm.value.length == 0) {
      alert("Bitte geben Sie eine g�ltige Abfahrtszeit ein (z.B. 12:45)");
      document.forms[0].abfahrtszeit_mm.focus();
      return false;
    }

    chkZ = 1;
    for (i = 0; i < document.forms[0].abfahrtszeit_hh.value.length; ++i) {
      if (document.forms[0].abfahrtszeit_hh.value.charAt(i) < "0" || document.forms[0].abfahrtszeit_hh.value.charAt(i) > "9") {
        chkZ = -1;
      }
    }
    if (chkZ == -1) {
      alert("Die eingegebene Abfahrtszeit ist nicht nummerisch. Bitte geben Sie einen nummerischen Wert ein");
      document.forms[0].abfahrtszeit_hh.focus();
      return false;
    }

    chkZ = 1;
    for (i = 0; i < document.forms[0].abfahrtszeit_mm.value.length; ++i) {
      if (document.forms[0].abfahrtszeit_mm.value.charAt(i) < "0" || document.forms[0].abfahrtszeit_mm.value.charAt(i) > "9") {
        chkZ = -1;
      }
    }
    if (chkZ == -1) {
      alert("Die eingegebene Abfahrtszeit ist nicht nummerisch. Bitte geben Sie einen nummerischen Wert ein");
      document.forms[0].abfahrtszeit_mm.focus();
      return false;
    }
  }

  // Ziel-PLZ
  if (document.forms[0].zielplz.value.length < 4) {
    alert("Bitte geben Sie die Postleitzahl des Ankunftsortes ein");
    document.forms[0].zielplz.focus();
    return false;
  }
  chkZ = 1;
  for (i = 0; i < document.forms[0].zielplz.value.length; ++i) {
    if (document.forms[0].zielplz.value.charAt(i) < "0" || document.forms[0].zielplz.value.charAt(i) > "9") {
      chkZ = -1;
    }
  }
  if (chkZ == -1) {
    alert("Die Postleitzahl des Ankunftsortes ist nicht nummerisch. Bitte geben Sie einen nummerischen Wert ein");
    document.forms[0].zielplz.focus();
    return false;
  }
  // Ort
  if (document.forms[0].zielort.value.length == 0) {
    alert("Der Ankunftsort fehlt");
    document.forms[0].zielort.focus();
    return false;
  }
  // Ankunftszeit
  if (document.forms[0].ankunftszeit_hh.value.length > 0 || document.forms[0].ankunftszeit_mm.value.length > 0) {
    if (document.forms[0].ankunftszeit_hh.value.length == 0) {
      alert("Bitte geben Sie eine g�ltige Ankunftszeit ein (z.B. 12:45)");
      document.forms[0].ankunftszeit_hh.focus();
      return false;
    }
    if (document.forms[0].ankunftszeit_mm.value.length == 0) {
      alert("Bitte geben Sie eine g�ltige Ankunftszeit ein (z.B. 12:45)");
      document.forms[0].ankunftszeit_mm.focus();
      return false;
    }

    chkZ = 1;
    for (i = 0; i < document.forms[0].ankunftszeit_hh.value.length; ++i) {
      if (document.forms[0].ankunftszeit_hh.value.charAt(i) < "0" || document.forms[0].ankunftszeit_hh.value.charAt(i) > "9") {
        chkZ = -1;
      }
    }
    if (chkZ == -1) {
      alert("Die eingegebene Ankunftszeit ist nicht nummerisch. Bitte geben Sie einen nummerischen Wert ein");
      document.forms[0].ankunftszeit_hh.focus();
      return false;
    }

    chkZ = 1;
    for (i = 0; i < document.forms[0].ankunftszeit_mm.value.length; ++i) {
      if (document.forms[0].ankunftszeit_mm.value.charAt(i) < "0" || document.forms[0].ankunftszeit_mm.value.charAt(i) > "9") {
        chkZ = -1;
      }
    }
    if (chkZ == -1) {
      alert("Die eingegebene Ankunftszeit ist nicht nummerisch. Bitte geben Sie einen nummerischen Wert ein");
      document.forms[0].ankunftszeit_mm.focus();
      return false;
    }
  }
  // R�ckfahrt (neu 29.02.2000
  if (document.forms[0].rueckfahrt.checked == true) {
    // Einmalige Fahrt
    if (document.forms[0].einmalige_fahrt.checked == true) {

      if (document.forms[0].rueckfahrtsdatum_tt.value.length < 1) {
        alert("Bitte geben Sie das R�ckfahrtsdatum ein (z.B. 01.01.1999)");
        document.forms[0].rueckfahrtsdatum_tt.focus();
        return false;
      }
      chkZ = 1;
      for (i = 0; i < document.forms[0].rueckfahrtsdatum_tt.value.length; ++i) {
        if (document.forms[0].rueckfahrtsdatum_tt.value.charAt(i) < "0"
            || document.forms[0].rueckfahrtsdatum_tt.value.charAt(i) > "9") {
          chkZ = -1;
        }
      }
      if (chkZ == -1) {
        alert("Das R�ckfahrtsdatum ist nicht nummerisch. Bitte geben Sie einen nummerischen Wert ein");
        document.forms[0].rueckfahrtsdatum_tt.focus();
        return false;
      }
      if (document.forms[0].rueckfahrtsdatum_tt.value > 31 || document.forms[0].rueckfahrtsdatum_tt.value == 00) {
        alert("Bitte geben Sie ein g�ltiges R�ckfahrtsdatum ein (z.B. 01.01.1999)");
        document.forms[0].rueckfahrtsdatum_tt.focus();
        return false;
      }

      if (document.forms[0].rueckfahrtsdatum_mm.value.length < 1) {
        alert("Bitte geben Sie das R�ckfahrtsdatum ein (z.B. 01.01.1999)");
        document.forms[0].rueckfahrtsdatum_mm.focus();
        return false;
      }
      chkZ = 1;
      for (i = 0; i < document.forms[0].rueckfahrtsdatum_mm.value.length; ++i) {
        if (document.forms[0].rueckfahrtsdatum_mm.value.charAt(i) < "0"
            || document.forms[0].rueckfahrtsdatum_mm.value.charAt(i) > "9") {
          chkZ = -1;
        }
      }
      if (chkZ == -1) {
        alert("Das R�ckfahrtsdatum ist nicht nummerisch. Bitte geben Sie einen nummerischen Wert ein");
        document.forms[0].rueckfahrtsdatum_mm.focus();
        return false;
      }
      if (document.forms[0].rueckfahrtsdatum_mm.value > 12 || document.forms[0].rueckfahrtsdatum_mm.value == 00) {
        alert("Bitte geben Sie ein g�ltiges R�ckfahrtsdatum ein (z.B. 01.01.1999)");
        document.forms[0].rueckfahrtsdatum_mm.focus();
        return false;
      }


      if (document.forms[0].rueckfahrtsdatum_jjjj.value.length < 4) {
        alert("Bitte geben Sie das R�ckfahrtsdatum ein (z.B. 01.01.1999)");
        document.forms[0].rueckfahrtsdatum_jjjj.focus();
        return false;
      }
      chkZ = 1;
      for (i = 0; i < document.forms[0].rueckfahrtsdatum_jjjj.value.length; ++i) {
        if (document.forms[0].rueckfahrtsdatum_jjjj.value.charAt(i) < "0"
            || document.forms[0].rueckfahrtsdatum_jjjj.value.charAt(i) > "9") {
          chkZ = -1;
        }
      }
      if (chkZ == -1) {
        alert("Das R�ckfahrtsdatum ist nicht nummerisch. Bitte geben Sie einen nummerischen Wert ein");
        document.forms[0].rueckfahrtsdatum_jjjj.focus();
        return false;
      }
    }

    // Abfahrtszeit (R�ckfahrt)
    if (document.forms[0].rueck_abfahrtszeit_hh.value.length > 0 || document.forms[0].rueck_abfahrtszeit_mm.value.length > 0) {
      if (document.forms[0].rueck_abfahrtszeit_hh.value.length == 0) {
        alert("Bitte geben Sie eine g�ltige R�ckfahrt-Abfahrtszeit ein (z.B. 12:45)");
        document.forms[0].rueck_abfahrtszeit_hh.focus();
        return false;
      }
      if (document.forms[0].rueck_abfahrtszeit_mm.value.length == 0) {
        alert("Bitte geben Sie eine g�ltige R�ckfahrt-Abfahrtszeit ein (z.B. 12:45)");
        document.forms[0].rueck_abfahrtszeit_mm.focus();
        return false;
      }

      chkZ = 1;
      for (i = 0; i < document.forms[0].rueck_abfahrtszeit_hh.value.length; ++i) {
        if (document.forms[0].rueck_abfahrtszeit_hh.value.charAt(i) < "0"
            || document.forms[0].rueck_abfahrtszeit_hh.value.charAt(i) > "9") {
          chkZ = -1;
        }
      }
      if (chkZ == -1) {
        alert("Die eingegebene R�ckfahrts-Abfahrtszeit ist nicht nummerisch. Bitte geben Sie einen nummerischen Wert ein");
        document.forms[0].rueck_abfahrtszeit_hh.focus();
        return false;
      }

      chkZ = 1;
      for (i = 0; i < document.forms[0].rueck_abfahrtszeit_mm.value.length; ++i) {
        if (document.forms[0].rueck_abfahrtszeit_mm.value.charAt(i) < "0"
            || document.forms[0].rueck_abfahrtszeit_mm.value.charAt(i) > "9") {
          chkZ = -1;
        }
      }
      if (chkZ == -1) {
        alert("Die eingegebene R�ckfahrt-Abfahrtszeit ist nicht nummerisch. Bitte geben Sie einen nummerischen Wert ein");
        document.forms[0].rueck_abfahrtszeit_mm.focus();
        return false;
      }
    }

    // Ankunftszeit (R�ckfahrt)
    if (document.forms[0].rueck_ankunftszeit_hh.value.length > 0 || document.forms[0].rueck_ankunftszeit_mm.value.length > 0) {
      if (document.forms[0].rueck_ankunftszeit_hh.value.length == 0) {
        alert("Bitte geben Sie eine g�ltige R�ckfahrt-Ankunftszeit ein (z.B. 12:45)");
        document.forms[0].rueck_ankunftszeit_hh.focus();
        return false;
      }
      if (document.forms[0].rueck_ankunftszeit_mm.value.length == 0) {
        alert("Bitte geben Sie eine g�ltige R�ckfahrt-Ankunftszeit ein (z.B. 12:45)");
        document.forms[0].rueck_ankunftszeit_mm.focus();
        return false;
      }

      chkZ = 1;
      for (i = 0; i < document.forms[0].rueck_ankunftszeit_hh.value.length; ++i) {
        if (document.forms[0].rueck_ankunftszeit_hh.value.charAt(i) < "0"
            || document.forms[0].rueck_ankunftszeit_hh.value.charAt(i) > "9") {
          chkZ = -1;
        }
      }
      if (chkZ == -1) {
        alert("Die eingegebene R�ckfahrts-Ankunftszeit ist nicht nummerisch. Bitte geben Sie einen nummerischen Wert ein");
        document.forms[0].rueck_ankunftszeit_hh.focus();
        return false;
      }

      chkZ = 1;
      for (i = 0; i < document.forms[0].rueck_Ankunftszeit_mm.value.length; ++i) {
        if (document.forms[0].rueck_ankunftszeit_mm.value.charAt(i) < "0"
            || document.forms[0].rueck_ankunftszeit_mm.value.charAt(i) > "9") {
          chkZ = -1;
        }
      }
      if (chkZ == -1) {
        alert("Die eingegebene R�ckfahrt-Ankunftszeit ist nicht nummerisch. Bitte geben Sie einen nummerischen Wert ein");
        document.forms[0].rueck_ankunftszeit_mm.focus();
        return false;
      }
    }
  }

  return chkRegisterAllgemein();
}
