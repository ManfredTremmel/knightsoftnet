/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.knightsoftnet.carpool;

import de.knightsoft.common.TextException;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class CarCheck extends de.knightsoft.knightsoftnet.common.Check {
  String einmaligeFahrt;

  String abfahrtsdatum;
  String abfahrtsdatumTt;
  String abfahrtsdatumMm;
  String abfahrtsdatumJjjj;
  String abfahrtsplz;
  String abfahrtsort;
  String abfahrtsstrasse;
  String abfahrtszeit;
  String abfahrtszeitHh;
  String abfahrtszeitMm;

  String zielplz;
  String zielort;
  String zielstrasse;
  String ankunftszeit;
  String ankunftszeitHh;
  String ankunftszeitMm;

  String rueckfahrt;
  String rueckfahrtsdatum;
  String rueckfahrtsdatumTt;
  String rueckfahrtsdatumMm;
  String rueckfahrtsdatumJjjj;
  String rueckAbfahrtszeit;
  String rueckAbfahrtszeitHh;
  String rueckAbfahrtszeitMm;
  String rueckAnkunftszeit;
  String rueckAnkunftszeitHh;
  String rueckAnkunftszeitMm;

  /**
   * Constructor default.
   */
  public CarCheck() {
    super();

    einmaligeFahrt = "N";

    abfahrtsdatumTt = StringUtils.EMPTY;
    abfahrtsdatumMm = StringUtils.EMPTY;
    abfahrtsdatumJjjj = StringUtils.EMPTY;
    abfahrtsdatum = StringUtils.EMPTY;
    abfahrtsplz = StringUtils.EMPTY;
    abfahrtsort = StringUtils.EMPTY;
    abfahrtsstrasse = StringUtils.EMPTY;
    abfahrtszeitHh = StringUtils.EMPTY;
    abfahrtszeitMm = StringUtils.EMPTY;
    abfahrtszeit = StringUtils.EMPTY;
    zielplz = StringUtils.EMPTY;
    zielort = StringUtils.EMPTY;
    zielstrasse = StringUtils.EMPTY;
    ankunftszeitHh = StringUtils.EMPTY;
    ankunftszeitMm = StringUtils.EMPTY;
    ankunftszeit = StringUtils.EMPTY;
    rueckfahrt = StringUtils.EMPTY;

    rueckfahrtsdatumTt = StringUtils.EMPTY;
    rueckfahrtsdatumMm = StringUtils.EMPTY;
    rueckfahrtsdatumJjjj = StringUtils.EMPTY;
    rueckfahrtsdatum = StringUtils.EMPTY;
    rueckAbfahrtszeitHh = StringUtils.EMPTY;
    rueckAbfahrtszeitMm = StringUtils.EMPTY;
    rueckAbfahrtszeit = StringUtils.EMPTY;

    rueckAnkunftszeitHh = StringUtils.EMPTY;
    rueckAnkunftszeitMm = StringUtils.EMPTY;
    rueckAnkunftszeit = StringUtils.EMPTY;
  }

  /**
   * Constructor aus dem Bild auslesen.
   *
   * @param req HttpServletRequest
   */
  public CarCheck(final HttpServletRequest req) {
    super(req);

    einmaligeFahrt = req.getParameter("einmalige_fahrt");

    abfahrtsdatumTt = req.getParameter("abfahrtsdatum_tt");
    abfahrtsdatumMm = req.getParameter("abfahrtsdatum_mm");
    abfahrtsdatumJjjj = req.getParameter("abfahrtsdatum_jjjj");
    if (abfahrtsdatumTt.equals(StringUtils.EMPTY) || abfahrtsdatumMm.equals(StringUtils.EMPTY)
        || abfahrtsdatumJjjj.equals(StringUtils.EMPTY)) {
      abfahrtsdatum = StringUtils.EMPTY;
    } else {
      abfahrtsdatum = abfahrtsdatumJjjj + "-" + abfahrtsdatumMm + "-" + abfahrtsdatumTt;
    }
    abfahrtsplz = req.getParameter("abfahrtsplz");
    abfahrtsort = req.getParameter("abfahrtsort");
    abfahrtsstrasse = req.getParameter("abfahrtsstrasse");
    abfahrtszeitHh = req.getParameter("abfahrtszeit_hh");
    abfahrtszeitMm = req.getParameter("abfahrtszeit_mm");
    if (abfahrtszeitHh.equals(StringUtils.EMPTY) || abfahrtszeitMm.equals(StringUtils.EMPTY)) {
      abfahrtszeit = StringUtils.EMPTY;
    } else {
      abfahrtszeit = abfahrtszeitHh + ":" + abfahrtszeitMm + ":00"; // NOPMD
    }

    zielplz = req.getParameter("zielplz");
    zielort = req.getParameter("zielort");
    zielstrasse = req.getParameter("zielstrasse");
    ankunftszeitHh = req.getParameter("ankunftszeit_hh");
    ankunftszeitMm = req.getParameter("ankunftszeit_mm");
    if (ankunftszeitHh.equals(StringUtils.EMPTY) || ankunftszeitMm.equals(StringUtils.EMPTY)) {
      ankunftszeit = StringUtils.EMPTY;
    } else {
      ankunftszeit = ankunftszeitHh + ":" + ankunftszeitMm + ":00"; // NOPMD
    }

    // Beginn der neuen Felder vom 29.02.2000
    rueckfahrt = req.getParameter("rueckfahrt");

    rueckfahrtsdatumTt = req.getParameter("rueckfahrtsdatum_tt");
    rueckfahrtsdatumMm = req.getParameter("rueckfahrtsdatum_mm");
    rueckfahrtsdatumJjjj = req.getParameter("rueckfahrtsdatum_jjjj");
    if (rueckfahrtsdatumTt.equals(StringUtils.EMPTY) || rueckfahrtsdatumMm.equals(StringUtils.EMPTY)
        || rueckfahrtsdatumJjjj.equals(StringUtils.EMPTY)) {
      rueckfahrtsdatum = StringUtils.EMPTY;
    } else {
      rueckfahrtsdatum = rueckfahrtsdatumJjjj + "-" + rueckfahrtsdatumMm + "-" + rueckfahrtsdatumTt;
    }

    rueckAbfahrtszeitHh = req.getParameter("rueck_abfahrtszeit_hh");
    rueckAbfahrtszeitMm = req.getParameter("rueck_abfahrtszeit_mm");
    if (rueckAbfahrtszeitHh.equals(StringUtils.EMPTY) || rueckAbfahrtszeitMm.equals(StringUtils.EMPTY)) {
      rueckAbfahrtszeit = StringUtils.EMPTY;
    } else {
      rueckAbfahrtszeit = rueckAbfahrtszeitHh + ":" + rueckAbfahrtszeitMm //
          + ":00"; // NOPMD
    }

    rueckAnkunftszeitHh = req.getParameter("rueck_ankunftszeit_hh");
    rueckAnkunftszeitMm = req.getParameter("rueck_ankunftszeit_mm");
    if (rueckAnkunftszeitHh.equals(StringUtils.EMPTY) || rueckAnkunftszeitMm.equals(StringUtils.EMPTY)) {
      rueckAnkunftszeit = StringUtils.EMPTY;
    } else {
      rueckAnkunftszeit = rueckAnkunftszeitHh + ":" + rueckAnkunftszeitMm //
          + ":00"; // NOPMD
      // Ende der neuen Felder vom 29.02.2000
    }
  }

  /**
   * Constructor aus der Datenbank auslesen.
   *
   * @param result result set
   */
  public CarCheck(final ResultSet result) throws SQLException {
    super(result);

    einmaligeFahrt = result.getString("einmalige_fahrt");

    abfahrtsdatum = result.getString("abfahrtsdatum");
    if (abfahrtsdatum == null) {
      abfahrtsdatum = StringUtils.EMPTY;
      abfahrtsdatumTt = StringUtils.EMPTY;
      abfahrtsdatumMm = StringUtils.EMPTY;
      abfahrtsdatumJjjj = StringUtils.EMPTY;
    } else {
      abfahrtsdatumTt = abfahrtsdatum.substring(8, 10);
      abfahrtsdatumMm = abfahrtsdatum.substring(5, 7);
      abfahrtsdatumJjjj = abfahrtsdatum.substring(0, 4);
    }
    abfahrtsplz = result.getString("abfahrtsplz");
    if (abfahrtsplz == null) {
      abfahrtsplz = StringUtils.EMPTY;
    }
    abfahrtsort = result.getString("abfahrtsort");
    if (abfahrtsort == null) {
      abfahrtsort = StringUtils.EMPTY;
    }
    abfahrtsstrasse = result.getString("abfahrtsstrasse");
    if (abfahrtsstrasse == null) {
      abfahrtsstrasse = StringUtils.EMPTY;
    }
    abfahrtszeit = result.getString("abfahrtszeit");
    if (abfahrtszeit == null) {
      abfahrtszeit = StringUtils.EMPTY;
      abfahrtszeitHh = StringUtils.EMPTY;
      abfahrtszeitMm = StringUtils.EMPTY;
    } else {
      abfahrtszeitHh = abfahrtszeit.substring(0, 2);
      abfahrtszeitMm = abfahrtszeit.substring(3, 5);
    }

    zielplz = result.getString("zielplz");
    if (zielplz == null) {
      zielplz = StringUtils.EMPTY;
    }
    zielort = result.getString("zielort");
    if (zielort == null) {
      zielort = StringUtils.EMPTY;
    }
    zielstrasse = result.getString("zielstrasse");
    if (zielstrasse == null) {
      zielstrasse = StringUtils.EMPTY;
    }
    ankunftszeit = result.getString("ankunftszeit");
    if (ankunftszeit == null) {
      ankunftszeit = StringUtils.EMPTY;
      ankunftszeitHh = StringUtils.EMPTY;
      ankunftszeitMm = StringUtils.EMPTY;
    } else {
      ankunftszeitHh = ankunftszeit.substring(0, 2);
      ankunftszeitMm = ankunftszeit.substring(3, 5);
    }

    rueckfahrt = result.getString("rueckfahrt");
    rueckfahrtsdatum = result.getString("rueckfahrtsdatum");
    if (rueckfahrtsdatum == null) {
      rueckfahrtsdatum = StringUtils.EMPTY;
      rueckfahrtsdatumTt = StringUtils.EMPTY;
      rueckfahrtsdatumMm = StringUtils.EMPTY;
      rueckfahrtsdatumJjjj = StringUtils.EMPTY;
    } else {
      rueckfahrtsdatumTt = rueckfahrtsdatum.substring(8, 10);
      rueckfahrtsdatumMm = rueckfahrtsdatum.substring(5, 7);
      rueckfahrtsdatumJjjj = rueckfahrtsdatum.substring(0, 4);
    }

    rueckAbfahrtszeit = result.getString("rueckAbfahrtszeit");
    if (rueckAbfahrtszeit == null) {
      rueckAbfahrtszeit = StringUtils.EMPTY;
      rueckAbfahrtszeitHh = StringUtils.EMPTY;
      rueckAbfahrtszeitMm = StringUtils.EMPTY;
    } else {
      rueckAbfahrtszeitHh = rueckAbfahrtszeit.substring(0, 2);
      rueckAbfahrtszeitMm = rueckAbfahrtszeit.substring(3, 5);
    }

    rueckAnkunftszeit = result.getString("rueckAnkunftszeit");
    if (rueckAnkunftszeit == null) {
      rueckAnkunftszeit = StringUtils.EMPTY;
      rueckAnkunftszeitHh = StringUtils.EMPTY;
      rueckAnkunftszeitMm = StringUtils.EMPTY;
    } else {
      rueckAnkunftszeitHh = rueckAnkunftszeit.substring(0, 2);
      rueckAnkunftszeitMm = rueckAnkunftszeit.substring(3, 5);
    }
  }

  @Override
  public void checkDaten() throws de.knightsoft.common.TextException {
    int fahrtdatum = 0;
    int rueckdatum = 0;

    final GregorianCalendar myCalendar = new GregorianCalendar();
    myCalendar.setTimeZone(TimeZone.getTimeZone("ECT"));
    final java.util.Date jetzt = new java.util.Date();
    myCalendar.setTime(jetzt);

    final int tagesdatum = myCalendar.get(Calendar.YEAR) * 10000 + (myCalendar.get(Calendar.MONTH) + 1) * 100
        + myCalendar.get(Calendar.DAY_OF_MONTH);

    super.checkDaten();

    if (einmaligeFahrt == null) {
      einmaligeFahrt = "N";
      abfahrtsdatum = StringUtils.EMPTY;
      abfahrtsdatumTt = StringUtils.EMPTY;
      abfahrtsdatumMm = StringUtils.EMPTY;
      abfahrtsdatumJjjj = StringUtils.EMPTY;
    } else {
      einmaligeFahrt = "J";
      if (abfahrtsdatumTt.equals(StringUtils.EMPTY) || abfahrtsdatumMm.equals(StringUtils.EMPTY)
          || abfahrtsdatumJjjj.equals(StringUtils.EMPTY)) {
        throw new de.knightsoft.common.TextException(
            "Bei einmaligen Fahrten muss ein vollständiges Datum mit angegeben werden!");
      }

      if (!StringUtils.isNumeric(abfahrtsdatumTt)) {
        throw new de.knightsoft.common.TextException("Der eingegebene Abfahrtstag ist nicht nummerisch!");
      }
      if (Integer.parseInt(abfahrtsdatumTt) < 1 || Integer.parseInt(abfahrtsdatumTt) > 31) {
        throw new de.knightsoft.common.TextException("Der eingegebene Abfahrtstag ist nicht gültig!");
      }

      if (!StringUtils.isNumeric(abfahrtsdatumMm)) {
        throw new de.knightsoft.common.TextException("Der eingegebene Abfahrtsmonat ist nicht nummerisch!");
      }
      if (Integer.parseInt(abfahrtsdatumMm) < 1 || Integer.parseInt(abfahrtsdatumMm) > 12) {
        throw new de.knightsoft.common.TextException("Der eingegebene Abfahrtsmonat ist nicht gültig!");
      }

      abfahrtsdatumJjjj = checkJahr(abfahrtsdatumJjjj, true, false);
      abfahrtsdatum = abfahrtsdatumJjjj + "-" + abfahrtsdatumMm + "-" + abfahrtsdatumTt;

      fahrtdatum = Integer.parseInt(abfahrtsdatumJjjj) * 10000 + Integer.parseInt(abfahrtsdatumMm) * 100
          + Integer.parseInt(abfahrtsdatumTt);
      if (fahrtdatum < tagesdatum) {
        throw new de.knightsoft.common.TextException(
            "Der Abfahrtstermin der Fahrt liegt in der Vergangenheit, dies ist nicht zulässig!");
      }
    }

    if (abfahrtsplz.length() < 4) {
      throw new de.knightsoft.common.TextException("Die Postleitzahl des Abfahrtsorts ist nicht fünfstellig gefüllt!");
    } else {
      if (!StringUtils.isNumeric(abfahrtsplz)) {
        throw new de.knightsoft.common.TextException("Die Postleitzahl des Abfahrtsorts ist nicht nummerisch!");
      }
    }

    if (abfahrtsort.equals(StringUtils.EMPTY)) {
      throw new de.knightsoft.common.TextException("Der Abfahrtsort ist nicht gefüllt!");
    }

    if (abfahrtszeitHh.equals(StringUtils.EMPTY) || abfahrtszeitMm.equals(StringUtils.EMPTY)) {
      abfahrtszeit = StringUtils.EMPTY;
    } else {
      if (!StringUtils.isNumeric(abfahrtszeitHh)) {
        throw new de.knightsoft.common.TextException("Die eingegebene Abfahrtsstunde ist nicht nummerisch!");
      }
      if (Integer.parseInt(abfahrtszeitHh) < 0 || Integer.parseInt(abfahrtszeitHh) > 23) {
        throw new TextException("Die eingegebene Abfahrtsstunde nicht gültig!");
      }

      if (!StringUtils.isNumeric(abfahrtszeitMm)) {
        throw new de.knightsoft.common.TextException("Die eingegebenen Minuten der Abfahrtszeit sind nicht nummerisch!");
      }
      if (Integer.parseInt(abfahrtszeitMm) < 0 || Integer.parseInt(abfahrtszeitMm) > 59) {
        throw new de.knightsoft.common.TextException("Der eingegebenen Minuten der Abfahrtszeit sind nicht gültig!");
      }
    }

    if (zielplz.length() < 4) {
      throw new de.knightsoft.common.TextException("Die Postleitzahl des Zielorts ist nicht fünfstellig gefüllt!");
    } else {
      if (!StringUtils.isNumeric(zielplz)) {
        throw new de.knightsoft.common.TextException("Die Postleitzahl des Zielorts ist nicht nummerisch!");
      }
    }

    if (zielort.equals(StringUtils.EMPTY)) {
      throw new de.knightsoft.common.TextException("Der Zielort ist nicht gefüllt!");
    }

    if (ankunftszeitHh.equals(StringUtils.EMPTY) || ankunftszeitMm.equals(StringUtils.EMPTY)) {
      ankunftszeit = StringUtils.EMPTY;
    } else {
      if (!StringUtils.isNumeric(ankunftszeitHh)) {
        throw new de.knightsoft.common.TextException("Die eingegebene Ankunftsstunde ist nicht nummerisch!");
      }
      if (Integer.parseInt(ankunftszeitHh) < 0 || Integer.parseInt(ankunftszeitHh) > 23) {
        throw new TextException("Die eingegebene Ankunftsstunde nicht gültig!");
      }

      if (!StringUtils.isNumeric(ankunftszeitMm)) {
        throw new de.knightsoft.common.TextException("Die eingegebenen Minuten der Ankunftszeit sind nicht nummerisch!");
      }
      if (Integer.parseInt(ankunftszeitMm) < 0 || Integer.parseInt(ankunftszeitMm) > 59) {
        throw new de.knightsoft.common.TextException("Der eingegebenen Minuten der Ankunftszeit sind nicht gültig!");
      }
    }

    if (rueckfahrt == null) {
      rueckfahrt = "N";
      rueckfahrtsdatum = StringUtils.EMPTY;
      rueckfahrtsdatumTt = StringUtils.EMPTY;
      rueckfahrtsdatumMm = StringUtils.EMPTY;
      rueckfahrtsdatumJjjj = StringUtils.EMPTY;
    } else {
      rueckfahrt = "J";
      if (einmaligeFahrt.equals("J")) {
        if (rueckfahrtsdatumTt.equals(StringUtils.EMPTY) || rueckfahrtsdatumMm.equals(StringUtils.EMPTY)
            || rueckfahrtsdatumJjjj.equals(StringUtils.EMPTY)) {
          throw new de.knightsoft.common.TextException(
              "Bei einmaligen Fahrten mit Rückfahrt muss ein vollständiges Rückfahrtsdatum " + "mit angegeben werden!");
        }

        if (!StringUtils.isNumeric(rueckfahrtsdatumTt)) {
          throw new de.knightsoft.common.TextException("Der eingegebene Rückfahrtstag ist nicht nummerisch!");
        }
        if (Integer.parseInt(rueckfahrtsdatumTt) < 1 || Integer.parseInt(rueckfahrtsdatumTt) > 31) {
          throw new de.knightsoft.common.TextException("Der eingegebene Rückfahrtstag ist nicht gültig!");
        }

        if (!StringUtils.isNumeric(rueckfahrtsdatumMm)) {
          throw new de.knightsoft.common.TextException("Der eingegebene Rückfahrtsmonat ist nicht nummerisch!");
        }
        if (Integer.parseInt(rueckfahrtsdatumMm) < 1 || Integer.parseInt(rueckfahrtsdatumMm) > 12) {
          throw new de.knightsoft.common.TextException("Der eingegebene Rückfahrtsmonat ist nicht gültig!");
        }

        rueckfahrtsdatumJjjj = checkJahr(rueckfahrtsdatumJjjj, true, false);
        rueckfahrtsdatum = rueckfahrtsdatumJjjj + "-" + rueckfahrtsdatumMm + "-" + rueckfahrtsdatumTt;

        rueckdatum = Integer.parseInt(rueckfahrtsdatumJjjj) * 10000 + Integer.parseInt(rueckfahrtsdatumMm) * 100
            + Integer.parseInt(rueckfahrtsdatumTt);
        if (rueckdatum < fahrtdatum) {
          throw new de.knightsoft.common.TextException(
              "Der Rückfahrtstermin liegt vor dem Abfahrtstermin, sind Sie im Besitz " + "einer Zeitmaschine?");
        }
      } else {
        rueckfahrtsdatum = StringUtils.EMPTY;
        rueckfahrtsdatumTt = StringUtils.EMPTY;
        rueckfahrtsdatumMm = StringUtils.EMPTY;
        rueckfahrtsdatumJjjj = StringUtils.EMPTY;
      }

      if (rueckAbfahrtszeitHh.equals(StringUtils.EMPTY) || rueckAbfahrtszeitMm.equals(StringUtils.EMPTY)) {
        rueckAbfahrtszeit = StringUtils.EMPTY;
      } else {
        if (!StringUtils.isNumeric(rueckAbfahrtszeitHh)) {
          throw new de.knightsoft.common.TextException("Die eingegebene Rückfahrt-Abfahrtsstunde ist nicht nummerisch!");
        }
        if (Integer.parseInt(rueckAbfahrtszeitHh) < 0 || Integer.parseInt(rueckAbfahrtszeitHh) > 23) {
          throw new de.knightsoft.common.TextException("Die eingegebene Rückfahrt-Abfahrtsstunde nicht gültig!");
        }

        if (!StringUtils.isNumeric(rueckAbfahrtszeitMm)) {
          throw new de.knightsoft.common.TextException(
              "Die eingegebenen Minuten der Rückfahrt-Abfahrtszeit sind nicht nummerisch!");
        }
        if (Integer.parseInt(rueckAbfahrtszeitMm) < 0 || Integer.parseInt(rueckAbfahrtszeitMm) > 59) {
          throw new de.knightsoft.common.TextException(
              "Der eingegebenen Minuten der Rückfahrt-Abfahrtszeit sind nicht gültig!");
        }
      }

      if (rueckAnkunftszeitHh.equals(StringUtils.EMPTY) || rueckAnkunftszeitMm.equals(StringUtils.EMPTY)) {
        rueckAnkunftszeit = StringUtils.EMPTY;
      } else {
        if (!StringUtils.isNumeric(rueckAnkunftszeitHh)) {
          throw new de.knightsoft.common.TextException("Die eingegebene Rückfahrt-Ankunftsstunde ist nicht nummerisch!");
        }
        if (Integer.parseInt(rueckAnkunftszeitHh) < 0 || Integer.parseInt(rueckAnkunftszeitHh) > 23) {
          throw new de.knightsoft.common.TextException("Die eingegebene Rückfahrt-Ankunftsstunde nicht gültig!");
        }

        if (!StringUtils.isNumeric(rueckAnkunftszeitMm)) {
          throw new de.knightsoft.common.TextException(
              "Die eingegebenen Minuten der Rückfahrt-Ankunftszeit sind nicht nummerisch!");
        }
        if (Integer.parseInt(rueckAnkunftszeitMm) < 0 || Integer.parseInt(rueckAnkunftszeitMm) > 59) {
          throw new de.knightsoft.common.TextException(
              "Der eingegebenen Minuten der Rückfahrt-Ankunftszeit sind nicht gültig!");
        }
      }
    }
  }

  /**
   * preapre insert.
   *
   * @return sql string
   */
  public String prepareInsert() throws de.knightsoft.common.TextException {
    final StringBuilder sqlStringA = new StringBuilder(256);
    final StringBuilder sqlStringB = new StringBuilder(128);

    sqlStringA.append(", einmalige_fahrt, abfahrtsplz, abfahrtsort, zielplz, zielort");
    sqlStringB.append(", " + de.knightsoft.common.StringToSql.convert(einmaligeFahrt) + ", "
        + de.knightsoft.common.StringToSql.convert(abfahrtsplz) + ", " + de.knightsoft.common.StringToSql.convert(abfahrtsort)
        + ", " + de.knightsoft.common.StringToSql.convert(zielplz) + ", " + de.knightsoft.common.StringToSql.convert(zielort));

    if (StringUtils.isNotEmpty(abfahrtsdatum)) {
      sqlStringA.append(", abfahrtsdatum");
      sqlStringB.append(", ").append(de.knightsoft.common.StringToSql.convert(abfahrtsdatum));
    }

    if (StringUtils.isNotEmpty(abfahrtsstrasse)) {
      sqlStringA.append(", abfahrtsstrasse");
      sqlStringB.append(", ").append(de.knightsoft.common.StringToSql.convert(abfahrtsstrasse));
    }

    if (StringUtils.isNotEmpty(abfahrtszeit)) {
      sqlStringA.append(", abfahrtszeit");
      sqlStringB.append(", ").append(de.knightsoft.common.StringToSql.convert(abfahrtszeit));
    }

    if (StringUtils.isNotEmpty(zielstrasse)) {
      sqlStringA.append(", zielstrasse");
      sqlStringB.append(", ").append(de.knightsoft.common.StringToSql.convert(zielstrasse));
    }

    if (StringUtils.isNotEmpty(ankunftszeit)) {
      sqlStringA.append(", ankunftszeit");
      sqlStringB.append(", ").append(de.knightsoft.common.StringToSql.convert(ankunftszeit));
    }

    // Beginn der neuen Felder vom 29.02.2000
    sqlStringA.append(", rueckfahrt");
    sqlStringB.append(", ").append(de.knightsoft.common.StringToSql.convert(rueckfahrt));

    if (StringUtils.isNotEmpty(rueckfahrtsdatum)) {
      sqlStringA.append(", rueckfahrtsdatum");
      sqlStringB.append(", ").append(de.knightsoft.common.StringToSql.convert(rueckfahrtsdatum));
    }

    if (StringUtils.isNotEmpty(rueckAbfahrtszeit)) {
      sqlStringA.append(", rueckAbfahrtszeit");
      sqlStringB.append(", ").append(de.knightsoft.common.StringToSql.convert(rueckAbfahrtszeit));
    }

    if (StringUtils.isNotEmpty(rueckAnkunftszeit)) {
      sqlStringA.append(", rueckAnkunftszeit");
      sqlStringB.append(", ").append(de.knightsoft.common.StringToSql.convert(rueckAnkunftszeit));
    }

    return this.prepareInsert(sqlStringA.toString(), sqlStringB.toString(), "KSFG");
  }

  @Override
  public String prepareUpdate() throws de.knightsoft.common.TextException {
    return super.prepareUpdate() + ", einmalige_fahrt=" + de.knightsoft.common.StringToSql.convert(einmaligeFahrt)
        + ", abfahrtsplz=" + de.knightsoft.common.StringToSql.convert(abfahrtsplz) + ", abfahrtsort="
        + de.knightsoft.common.StringToSql.convert(abfahrtsort) + ", zielplz="
        + de.knightsoft.common.StringToSql.convert(zielplz) + ", zielort=" + de.knightsoft.common.StringToSql.convert(zielort)
        + ", abfahrtsdatum=" + de.knightsoft.common.StringToSql.convert(abfahrtsdatum) + ", abfahrtsstrasse="
        + de.knightsoft.common.StringToSql.convert(abfahrtsstrasse) + ", abfahrtszeit="
        + de.knightsoft.common.StringToSql.convert(abfahrtszeit) + ", zielstrasse="
        + de.knightsoft.common.StringToSql.convert(zielstrasse) + ", ankunftszeit="
        + de.knightsoft.common.StringToSql.convert(ankunftszeit) + ", rueckfahrt="
        + de.knightsoft.common.StringToSql.convert(rueckfahrt) + ", rueckfahrtsdatum="
        + de.knightsoft.common.StringToSql.convert(rueckfahrtsdatum) + ", rueckAbfahrtszeit="
        + de.knightsoft.common.StringToSql.convert(rueckAbfahrtszeit) + ", rueckAnkunftszeit="
        + de.knightsoft.common.StringToSql.convert(rueckAnkunftszeit);
  }

  /**
   * create html table.
   *
   * @param search do search
   * @return html table
   */
  public String htmlTabelle(final boolean search) {
    final String tabelleString1 = null;
    final StringBuilder tabelleString2 = new StringBuilder(1600);
    tabelleString2.append("        <tr>\n" + "            <td align=\"left\">Einmalige Fahrt:</td>\n"
        + "            <td align=\"left\">" + jnAusgabe(einmaligeFahrt) + "</td>\n" + "        </tr>\n");

    if (StringUtils.isNotEmpty(abfahrtsdatum)) {
      tabelleString2
          .append("        <tr>\n" + "            <td align=\"left\">Abfahrtsdatum:</td>\n" + "            <td align=\"left\">"
              + abfahrtsdatumTt + "." + abfahrtsdatumMm + "." + abfahrtsdatumJjjj + "</td>\n" + "        </tr>\n");
    }

    tabelleString2.append(
        "        <tr>\n" + "            <td align=\"left\">Abfahrts PLZ + Ort:</td>\n" + "            <td align=\"left\">"
            + de.knightsoft.common.StringToHtml.convert(abfahrtsplz + " " + abfahrtsort) + "</td>\n" + "        </tr>\n");

    if (StringUtils.isNotEmpty(abfahrtsstrasse)) {
      tabelleString2.append(
          "        <tr>\n" + "            <td align=\"left\">Abfahrtsstrasse:</td>\n" + "            <td align=\"left\">"
              + de.knightsoft.common.StringToHtml.convert(abfahrtsstrasse) + "</td>\n" + "        </tr>\n");
    }

    if (StringUtils.isNotEmpty(abfahrtszeit)) {
      tabelleString2.append("        <tr>\n" + "            <td align=\"left\">Abfahrtszeit:</td>\n"
          + "            <td align=\"left\">" + abfahrtszeit + "</td>\n" + "        </tr>\n");
    }

    tabelleString2.append(
        "        <tr>\n" + "            <td align=\"left\">Ankunfts PLZ + Ort:</td>\n" + "            <td align=\"left\">"
            + de.knightsoft.common.StringToHtml.convert(zielplz + " " + zielort) + "</td>\n" + "        </tr>\n");

    if (StringUtils.isNotEmpty(zielstrasse)) {
      tabelleString2.append(
          "        <tr>\n" + "            <td align=\"left\">Ankunftsstrasse:</td>\n" + "            <td align=\"left\">"
              + de.knightsoft.common.StringToHtml.convert(zielstrasse) + "</td>\n" + "        </tr>\n");
    }

    if (StringUtils.isNotEmpty(ankunftszeit)) {
      tabelleString2.append("        <tr>\n" + "            <td align=\"left\">Ankunftszeit:</td>\n"
          + "            <td align=\"left\">" + ankunftszeit + "</td>\n" + "        </tr>\n");
    }

    tabelleString2.append("        <tr>\n" + "            <td align=\"left\">R&uuml;ckfahrt:</td>\n"
        + "            <td align=\"left\">" + jnAusgabe(rueckfahrt) + "</td>\n" + "        </tr>\n");

    if (StringUtils.isNotEmpty(rueckfahrtsdatum)) {
      tabelleString2.append(
          "        <tr>\n" + "            <td align=\"left\">R&uuml;ckfahrtsdatum:</td>\n" + "            <td align=\"left\">"
              + rueckfahrtsdatumTt + "." + rueckfahrtsdatumMm + "." + rueckfahrtsdatumJjjj + "</td>\n" + "        </tr>\n");
    }

    if (StringUtils.isNotEmpty(rueckAbfahrtszeit)) {
      tabelleString2.append("        <tr>\n" + "            <td align=\"left\">R&uuml;ckfahrts-Abfahrtszeit:</td>\n"
          + "            <td align=\"left\">" + rueckAbfahrtszeit + "</td>\n" + "        </tr>\n");
    }

    if (StringUtils.isNotEmpty(rueckAnkunftszeit)) {
      tabelleString2.append("        <tr>\n" + "            <td align=\"left\">R&uuml;ckfahrts-Ankunftszeit:</td>\n"
          + "            <td align=\"left\">" + rueckAnkunftszeit + "</td>\n" + "        </tr>\n");
    }

    return this.htmlTabelle(tabelleString1, tabelleString2.toString(), search, "Fahrgemeinschaft");
  }

  @Override
  public String htmlHidden() {
    return super.htmlHidden() + "<input type=\"hidden\" name=\"einmaligeFahrt\" value=\""
        + de.knightsoft.common.StringToHtml.convert(einmaligeFahrt) + "\">\n"
        + "<input type=\"hidden\" name=\"abfahrtsdatum_tt\" value=\""
        + de.knightsoft.common.StringToHtml.convert(abfahrtsdatumTt) + "\">\n"
        + "<input type=\"hidden\" name=\"abfahrtsdatum_mm\" value=\""
        + de.knightsoft.common.StringToHtml.convert(abfahrtsdatumMm) + "\">\n"
        + "<input type=\"hidden\" name=\"abfahrtsdatum_jjjj\" value=\""
        + de.knightsoft.common.StringToHtml.convert(abfahrtsdatumJjjj) + "\">\n"
        + "<input type=\"hidden\" name=\"abfahrtsplz\" value=\"" + de.knightsoft.common.StringToHtml.convert(abfahrtsplz)
        + "\">\n" + "<input type=\"hidden\" name=\"abfahrtsort\" value=\""
        + de.knightsoft.common.StringToHtml.convert(abfahrtsort) + "\">\n"
        + "<input type=\"hidden\" name=\"abfahrtsstrasse\" value=\""
        + de.knightsoft.common.StringToHtml.convert(abfahrtsstrasse) + "\">\n"
        + "<input type=\"hidden\" name=\"abfahrtszeit_hh\" value=\"" + de.knightsoft.common.StringToHtml.convert(abfahrtszeitHh)
        + "\">\n" + "<input type=\"hidden\" name=\"abfahrtszeit_mm\" value=\""
        + de.knightsoft.common.StringToHtml.convert(abfahrtszeitMm) + "\">\n"
        + "<input type=\"hidden\" name=\"zielplz\" value=\"" + de.knightsoft.common.StringToHtml.convert(zielplz) + "\">\n"
        + "<input type=\"hidden\" name=\"zielort\" value=\"" + de.knightsoft.common.StringToHtml.convert(zielort) + "\">\n"
        + "<input type=\"hidden\" name=\"zielstrasse\" value=\"" + de.knightsoft.common.StringToHtml.convert(zielstrasse)
        + "\">\n" + "<input type=\"hidden\" name=\"ankunftszeit_hh\" value=\""
        + de.knightsoft.common.StringToHtml.convert(ankunftszeitHh) + "\">\n"
        + "<input type=\"hidden\" name=\"ankunftszeit_mm\" value=\"" + de.knightsoft.common.StringToHtml.convert(ankunftszeitMm)
        + "\">\n" + "<input type=\"hidden\" name=\"rueckfahrt\" value=\""
        + de.knightsoft.common.StringToHtml.convert(rueckfahrt) + "\">\n"
        + "<input type=\"hidden\" name=\"rueckfahrtsdatum_tt\" value=\""
        + de.knightsoft.common.StringToHtml.convert(rueckfahrtsdatumTt) + "\">"
        + "<input type=\"hidden\" name=\"rueckfahrtsdatum_mm\" value=\""
        + de.knightsoft.common.StringToHtml.convert(rueckfahrtsdatumMm) + "\">"
        + "<input type=\"hidden\" name=\"rueckfahrtsdatum_jjjj\" value=\""
        + de.knightsoft.common.StringToHtml.convert(rueckfahrtsdatumJjjj) + "\">\n"
        + "<input type=\"hidden\" name=\"rueck_abfahrtszeit_hh\" value=\""
        + de.knightsoft.common.StringToHtml.convert(rueckAbfahrtszeitHh) + "\">\n"
        + "<input type=\"hidden\" name=\"rueck_abfahrtszeit_mm\" value=\""
        + de.knightsoft.common.StringToHtml.convert(rueckAbfahrtszeitMm) + "\">\n"
        + "<input type=\"hidden\" name=\"rueck_ankunftszeit_hh\" value=\""
        + de.knightsoft.common.StringToHtml.convert(rueckAnkunftszeitHh) + "\">\n"
        + "<input type=\"hidden\" name=\"rueck_ankunftszeit_mm\" value=\""
        + de.knightsoft.common.StringToHtml.convert(rueckAnkunftszeitMm) + "\">\n";
  }
}
