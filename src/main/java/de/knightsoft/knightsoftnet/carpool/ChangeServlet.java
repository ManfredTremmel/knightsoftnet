/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.knightsoftnet.carpool;

import de.knightsoft.common.Constants;
import de.knightsoft.common.TextException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

public class ChangeServlet extends de.knightsoft.knightsoftnet.common.AbstractChangeServlet {

  private static final long serialVersionUID = -8537028689018366153L;

  /**
   * init-Methode.
   */
  @Override
  public void init(final ServletConfig config) throws ServletException {
    service = "Fahrgemeinschaft";
    servlet = "KnightSoftFahrgemeinschaftPflege";
    servletReg = "KnightSoftFahrgemeinschaftInsert";
    wbild = de.knightsoft.knightsoftnet.common.KnConst.WBILD_FAHR_CHANGE;
    wurl = de.knightsoft.knightsoftnet.common.KnConst.WURL_FAHR_CHANGE;
    wbildReg = de.knightsoft.knightsoftnet.common.KnConst.WBILD_FAHR_REG;
    wurlReg = de.knightsoft.knightsoftnet.common.KnConst.WURL_FAHR_REG;
    dbTabelle = "KnightSoft_Fahrgemeinschaft";
    loginCount = "FGLoginCounter";
    loginDbNummer = "FGLoginDBNummer";
    homeUrl = de.knightsoft.knightsoftnet.common.KnConst.HTML_FG_URL;
    emailAb = de.knightsoft.knightsoftnet.common.KnConst.EMAIL_FG;
    logoTextFarbe = de.knightsoft.knightsoftnet.common.KnConst.FAHRGEMEINSCHAFT_FARBE;
    logoText = de.knightsoft.knightsoftnet.common.KnConst.FAHRGEMEINSCHAFT_TEXT;
    thisNav = de.knightsoft.knightsoftnet.carpool.Navigation.FAHRGEMEINSCHAFT_NAV;
    homePage = de.knightsoft.knightsoftnet.common.KnConst.HTML_BASE + "Fahrgemeinschaft/index.html";
    passwortStart = "KSFG";
    super.init(config);
  }

  /**
   * Ändern-Seite ausgeben.
   */
  @Override
  protected void changeWindow(final HttpServletRequest req, final HttpServletResponse res, final ServletOutputStream out,
      final long cookieDbNr, final String hinweis) throws IOException, de.knightsoft.common.TextException {
    de.knightsoft.knightsoftnet.carpool.CarCheck fahrgemeinschaftDaten = null;
    try {
      fahrgemeinschaftDaten = new de.knightsoft.knightsoftnet.carpool.CarCheck(leseDbSatz(cookieDbNr));
    } catch (final java.sql.SQLException e) {
      throw new TextException("Fehler beim Datenbankzugriff: " + e.toString(), e);
    }

    final String htmlSeite = this.changeHtml(req, res, de.knightsoft.knightsoftnet.common.KnConst.JAVA_SCRIPT_FG_URL, hinweis,
        fahrgemeinschaftDaten, servlet);
    out.println(htmlSeite);
  }

  @Override
  protected String changeHtml(final HttpServletRequest req, final HttpServletResponse res, final String hinweis)
      throws de.knightsoft.common.TextException {
    final de.knightsoft.knightsoftnet.carpool.CarCheck aCheck = new de.knightsoft.knightsoftnet.carpool.CarCheck();
    return this.changeHtml(req, res, de.knightsoft.knightsoftnet.common.KnConst.JAVA_SCRIPT_FG_URL, hinweis, aCheck,
        servletReg);
  }

  /**
   * change html.
   *
   * @param req HttpServletRequest
   * @param res HttpServletResponse
   * @param javaScriptServiceUrl java script service url
   * @param hinweis hint text
   * @param pcarCheck check
   * @param servletName servlet name
   * @return created html
   */
  public String changeHtml(final HttpServletRequest req, final HttpServletResponse res, final String javaScriptServiceUrl,
      final String hinweis, final de.knightsoft.knightsoftnet.carpool.CarCheck pcarCheck, final String servletName)
      throws de.knightsoft.common.TextException {
    final StringBuilder sb = new StringBuilder(4096);
    sb.append(
        "                <tr>\n" + "                    <td style=\"text-align:left;\">Einmalige Fahrt (und Datum):</td>\n");
    if ("J".equals(pcarCheck.einmaligeFahrt)) {
      sb.append(
          "                    <td style=\"text-align:left;\">" + "<input name=\"einmaligeFahrt\" type=\"checkbox\" checked>"
              + "&nbsp;&nbsp;<input type=\"number\" name=\"abfahrtsdatum_tt\" min=\"1\" max=\"31\" value=\""
              + de.knightsoft.common.StringToHtml.convert(pcarCheck.abfahrtsdatumTt)
              + "\">.<input type=\"number\" name=\"abfahrtsdatum_mm\" min=\"1\" max=\"12\" value=\""
              + de.knightsoft.common.StringToHtml.convert(pcarCheck.abfahrtsdatumMm)
              + "\">.<input type=\"number\" name=\"abfahrtsdatum_jjjj\" min=\"2020\" max=\"3000\" " + "value=\""
              + de.knightsoft.common.StringToHtml.convert(pcarCheck.abfahrtsdatumJjjj) + "\"></td>\n");
    } else {
      sb.append("                    <td style=\"text-align:left;\">"
          + "<input name=\"einmaligeFahrt\" type=\"checkbox\">&nbsp;&nbsp;"
          + "<input type=\"number\" name=\"abfahrtsdatum_tt\" min=\"1\" max=\"31\">."
          + "<input type=\"number\" name=\"abfahrtsdatum_mm\" min=\"1\" max=\"12\">."
          + "<input type=\"number\" name=\"abfahrtsdatum_jjjj\" min=\"2020\" maxl=\"3000\"></td>\n");
    }
    sb.append("                </tr>\n" + "                <tr>\n"
        + "                    <td style=\"text-align:left;\">Abfahrts PLZ + Ort:</td>\n"
        + "                    <td style=\"text-align:left;\">"
        + "<input type=\"text\" name=\"abfahrtsplz\" size=\"5\" maxlength=\"5\" value=\""
        + de.knightsoft.common.StringToHtml.convert(pcarCheck.abfahrtsplz)
        + "\" required=\"required\"><input type=\"text\" name=\"abfahrtsort\" size=\"23\" maxlength=\"50\" value=\""
        + de.knightsoft.common.StringToHtml.convert(pcarCheck.abfahrtsort) + "\"required=\"required\"></td>\n"
        + "                </tr>\n" + "                <tr>\n"
        + "                    <td style=\"text-align:left; color:gray;\"> &sup1;) " + "Abfahrtsstrasse:</td>\n"
        + "                    <td style=\"text-align:left;\">"
        + "<input type=\"text\" name=\"abfahrtsstrasse\" size=\"30\" maxlength=\"50\" value=\""
        + de.knightsoft.common.StringToHtml.convert(pcarCheck.abfahrtsstrasse) + "\"></td>\n" + "                </tr>\n"
        + "                <tr>\n" + "                    <td style=\"text-align:left; color:gray;\"> &sup1;) "
        + "Abfahrtszeit:</td>\n" + "                    <td style=\"text-align:left;\">"
        + "<input type=\"number\" name=\"abfahrtszeit_hh\" min=\"0\" max=\"23\" value=\""
        + de.knightsoft.common.StringToHtml.convert(pcarCheck.abfahrtszeitHh)
        + "\">:<input type=\"number\" name=\"abfahrtszeit_mm\" min=\"0\" max=\"59\" value=\""
        + de.knightsoft.common.StringToHtml.convert(pcarCheck.abfahrtszeitMm) + "\"></td>\n" + "                </tr>\n"
        + "                <tr>\n" + "                    <td style=\"text-align:left;\">Ankunfts PLZ + Ort:</td>\n"
        + "                    <td style=\"text-align:left;\">"
        + "<input type=\"text\" name=\"zielplz\" size=\"5\" maxlength=\"5\" value=\""
        + de.knightsoft.common.StringToHtml.convert(pcarCheck.zielplz)
        + "\"required=\"required\"><input type=\"text\" name=\"zielort\" size=\"23\" maxlength=\"50\" value=\""
        + de.knightsoft.common.StringToHtml.convert(pcarCheck.zielort) + "\"required=\"required\"></td>\n"
        + "                </tr>\n" + "                <tr>\n"
        + "                    <td style=\"text-align:left; color:gray;\"> &sup1;) " + "Ankunftsstrasse:</td>\n"
        + "                    <td style=\"text-align:left;\">"
        + "<input type=\"text\" name=\"zielstrasse\" size=\"30\" maxlength=\"50\" value=\""
        + de.knightsoft.common.StringToHtml.convert(pcarCheck.zielstrasse) + "\"></td>\n" + "                </tr>\n"
        + "                <tr>\n" + "                    <td style=\"text-align:left; color:gray;\"> &sup1;) "
        + "(vermutliche) Ankunftszeit:</td>\n" + "                    <td style=\"text-align:left;\">"
        + "<input type=\"number\" name=\"ankunftszeit_hh\" min=\"0\" max=\"23\" value=\""
        + de.knightsoft.common.StringToHtml.convert(pcarCheck.ankunftszeitHh)
        + "\">:<input type=\"number\" name=\"ankunftszeit_mm\" min=\"0\" max=\"59\" value=\""
        + de.knightsoft.common.StringToHtml.convert(pcarCheck.ankunftszeitMm) + "\"></td>\n" + "                </tr>\n"
        + "                <tr>\n" + "                    <td style=\"text-align:left; color:gray;\"> &sup1;) "
        + "R&uuml;ckfahrt (falls gew&uuml;nscht):</td>\n");
    if ("J".equals(pcarCheck.rueckfahrt)) {
      sb.append("                    <td style=\"text-align:left;\">"
          + "<input name=\"rueckfahrt\" type=\"checkbox\" checked></td>\n");
    } else {
      sb.append("                    <td style=\"text-align:left;\">" + "<input name=\"rueckfahrt\" type=\"checkbox\"></td>\n");
    }
    sb.append("                </tr>\n" + "                <tr>\n"
        + "                    <td style=\"text-align:left; color:gray;\"> &sup1;) "
        + "R&uuml;ckfahrtsdatum (bei einmaligen Fahrten):</td>\n" + "                    <td style=\"text-align:left;\">"
        + "<input type=\"number\" name=\"rueckfahrtsdatum_tt\" min=\"1\" max=\"31\" value=\""
        + de.knightsoft.common.StringToHtml.convert(pcarCheck.rueckfahrtsdatumTt) + "\">."
        + "<input type=\"number\" name=\"rueckfahrtsdatum_mm\" min=\"1\" max=\"12\" value=\""
        + de.knightsoft.common.StringToHtml.convert(pcarCheck.rueckfahrtsdatumMm) + "\">."
        + "<input type=\"number\" name=\"rueckfahrtsdatum_jjjj\" min=\"2020\" max=\"3000\" value=\""
        + de.knightsoft.common.StringToHtml.convert(pcarCheck.rueckfahrtsdatumJjjj) + "\"></td>\n" + "                </tr>\n"
        + "                <tr>\n" + "                    <td style=\"text-align:left; color:gray;\"> &sup1;) "
        + "Abfahrtszeit (R&uuml;ckfahrt):</td>\n" + "                    <td style=\"text-align:left;\"><input type=\"number\" "
        + "name=\"rueck_abfahrtszeit_hh\" min=\"1\" max=\"23\" value=\""
        + de.knightsoft.common.StringToHtml.convert(pcarCheck.rueckAbfahrtszeitHh)
        + "\">:<input type=\"number\" name=\"rueck_abfahrtszeit_mm\" min=\"0\" max=\"59\" " + "value=\""
        + de.knightsoft.common.StringToHtml.convert(pcarCheck.rueckAbfahrtszeitMm) + "\"></td>\n" + "                </tr>\n"
        + "                <tr>\n" + "                    <td style=\"text-align:left; color:gray;\"> &sup1;) "
        + "(vermutliche) Ankunftszeit (R&uuml;ckfahrt):</td>\n" + "                    <td style=\"text-align:left;\">"
        + "<input type=\"number\" name=\"rueck_ankunftszeit_hh\" min=\"0\" max=\"23\" value=\""
        + de.knightsoft.common.StringToHtml.convert(pcarCheck.rueckAnkunftszeitHh) + "\">:"
        + "<input type=\"number\" name=\"rueck_ankunftszeit_mm\" min=\"0\" max=\"59\" value=\""
        + de.knightsoft.common.StringToHtml.convert(pcarCheck.rueckAnkunftszeitMm) + "\"></td>\n" + "                </tr>\n");

    return aendernHtml(req, res, javaScriptServiceUrl, hinweis, pcarCheck, sb.toString(), servletName);
  }

  /**
   * Ändern des ausgewählten Datensatzes.
   */
  @Override
  protected long verarbeitungAendern(final HttpServletRequest req, final HttpServletResponse res, final ServletOutputStream out)
      throws de.knightsoft.common.TextException, IOException {
    final long cookieDbNr = super.verarbeitungAendern(req, res, out);

    // Eingaben einlesen
    final de.knightsoft.knightsoftnet.carpool.CarCheck fahrgemeinschaftDaten =
        new de.knightsoft.knightsoftnet.carpool.CarCheck(req);

    // Die eingegebenen Daten prüfen
    fahrgemeinschaftDaten.checkDaten();

    upDateDbSatz(req, res, out, fahrgemeinschaftDaten.prepareUpdate(), cookieDbNr);

    return cookieDbNr;
  }

  @Override
  public String getServletInfo() {
    return "Change/update information about car pool in database\n\n" + Constants.COPYRIGHT;
  }
}
