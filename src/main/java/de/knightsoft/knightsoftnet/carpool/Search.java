/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.knightsoftnet.carpool;

import de.knightsoft.common.AbstractVisualDb;
import de.knightsoft.knightsoftnet.common.KnConst;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * The <code>Search</code> class is based on the abstract class de.knightsoft.common.AbstractVisualDb and implements the search
 * function for the KnightSoft Fahrgemeinschaften-Vermittlung. It's no longer a real servlet, only a part of one.
 *
 *
 * @author Manfred Tremmel
 * @version 2.0.0, 19.08.2006
 */
public class Search extends de.knightsoft.common.AbstractVisualDb {

  private static final String SEARCH_SQL_ONE = //
      "SELECT     * " //
          + "FROM       KnightSoft_Fahrgemeinschaft " //
          + "WHERE      zaehler = ? " //
          + " AND       eintrag_gesperrt = 'N' ";

  /**
   * Constructor.
   *
   * @param thisDatabase Connection to database
   * @param servletname Name of the Servlet
   * @exception SQLException if a database access error occurs
   * @exception de.knightsoft.common.TextException to give out a text message
   * @see de.knightsoft.common.TextException
   */
  public Search(final Connection thisDatabase, final String servletname)
      throws java.sql.SQLException, de.knightsoft.common.TextException {
    super(thisDatabase, KnConst.HTML_BASE, servletname, KnConst.GIF_URL + "16x16/find.png", "Suchen", "Suchen", null, null, 0,
        null);
  }

  /**
   * The Method <code>SuchergebnisUebersicht</code> generates the html output of the search overview.
   *
   * @param req HttpServletRequest from the Servlet
   * @param res HttpServletResponse from the Servlet
   * @param result ResultSet to get the entries from
   * @param land country code
   * @param bietesuche search for "Bieter" or "Sucher"
   * @param einmalig one TimeField drive
   * @param suchezielplz zip code of the target
   * @param suchezielort name of the tartge city
   * @return HTML code of the formular
   */
  private String suchergebnisUebersicht(final HttpServletResponse res, final ResultSet result, final String land,
      final String bietesuche, final String einmalig, final String suchezielplz, final String suchezielort)
      throws SQLException {
    int anzahlGefunden = 0;
    String datum;
    String adresseVon = null;
    String adresseZu = null;
    boolean rueckfahrt = false;
    String emailText = StringUtils.EMPTY;
    final StringBuilder inhalt = new StringBuilder(2500);

    inhalt.append("        <div style=\"text-align:center;\">\n");

    while (result.next()) {
      // E-Mail Anschrift aufbereiten
      if (result.getString("geschlecht").equals("W")) {
        emailText = "Sehr geehrte Frau " + result.getString("name") + ",";
      } else {
        emailText = "Sehr geehrter Herr " + result.getString("name") + ",";
      }

      if (result.getString("abfahrtsstrasse") == null) {
        adresseVon =
            de.knightsoft.common.StringToHtml.convert(result.getString("abfahrtsplz") + " " + result.getString("abfahrtsort"));
      } else {
        adresseVon = de.knightsoft.common.StringToHtml.convert(result.getString("abfahrtsstrasse")) + "<BR>"
            + de.knightsoft.common.StringToHtml
                .convert(result.getString("abfahrtsplz") + " " + result.getString("abfahrtsort"));
      }

      if (result.getString("zielstrasse") == null) {
        adresseZu = de.knightsoft.common.StringToHtml.convert(result.getString("zielplz") + " " + result.getString("zielort"));
      } else {
        adresseZu = de.knightsoft.common.StringToHtml.convert(result.getString("zielstrasse")) + "<BR>"
            + de.knightsoft.common.StringToHtml.convert(result.getString("zielplz") + " " + result.getString("zielort"));
      }

      if (anzahlGefunden == 0) {
        inhalt.append("        <div style=\"text-align:center;\">\n"
            + "        <table style=\"margin-left:auto; margin-right:auto;\" border=\"0\" "
            + "cellspacing=\"5\" cellpadding=\"0\">\n" + "            <tr style=\"background-color:#CCCCCC;\">\n"
            + "                <th style=\"text-align:left;\" valign=\"top\">" + "Name<br>E-Mail</th>\n"
            + "                <th style=\"text-align:left;\" valign=\"top\">" + "Abfahrtsort<br>und Zeit</th>\n"
            + "                <th style=\"text-align:left;\" valign=\"top\">" + "Ankunftsort<br>und Zeit</th>\n"
            + "                <th style=\"text-align:left;\" valign=\"top\">" + "Details<br>&nbsp;</th>\n"
            + "            </tr>\n");
      }

      if (anzahlGefunden % 2 == 0) {
        inhalt.append("            <tr style=\"background-color:#FFFFFF;\">\n");
      } else {
        inhalt.append("            <tr style=\"background-color:#CCCCCC;\">\n");
      }

      inhalt.append("                <td style=\"text-align:left;\" valign=\"top\">"
          + de.knightsoft.common.StringToHtml.convert(result.getString("name")) + ", "
          + de.knightsoft.common.StringToHtml.convert(result.getString("vorname")) + "<br>" + "<a href=\"MAILTO:"
          + de.knightsoft.common.StringToHtml.convert(result.getString("email"))
          + "?subject=Anfrage bezueglich Ihres Fahrgemeinschaft-Angebots im KnightSoft-Net" + "&amp;body="
          + de.knightsoft.common.StringToHtml.convert(emailText) + "\"> "
          + de.knightsoft.common.StringToHtml.convert(result.getString("email")) + "</a></td>\n");

      inhalt.append("                <td style=\"text-align:left;\" valign=\"top\">");

      inhalt.append(adresseVon);
      if (StringUtils.isNotEmpty(result.getString("abfahrtszeit"))) {
        inhalt.append("<br>Zeit: ").append(result.getString("abfahrtszeit"));
      }
      datum = result.getString("abfahrtsdatum");
      if (StringUtils.isNotEmpty(datum)) {
        inhalt.append("<br>Datum: ").append(datum.substring(8, 10)).append('.').append(datum.substring(5, 7)).append('.')
            .append(datum.substring(0, 4));
      }
      if ("J".equals(result.getString("rueckfahrt"))) {
        rueckfahrt = true;
      }
      if (rueckfahrt) {
        inhalt.append("<br><hr><b>R&uuml;ckfahrt:</b><br>").append(adresseZu);
        if (StringUtils.isNotEmpty(result.getString("rueckAbfahrtszeit"))) {
          inhalt.append("<br>Zeit: ").append(result.getString("rueckAbfahrtszeit"));
        }
        datum = result.getString("rueckfahrtsdatum");
        if (StringUtils.isNotEmpty(datum)) {
          inhalt.append("<br>Datum: ").append(datum.substring(8, 10)).append('.').append(datum.substring(5, 7)).append('.')
              .append(datum.substring(0, 4));
        }
      }
      inhalt.append("</td>\n");

      inhalt.append("                <td style=\"text-align:left;\" valign=\"top\">");
      inhalt.append(adresseZu);
      if (StringUtils.isNotEmpty(result.getString("ankunftszeit"))) {
        inhalt.append("<br>Zeit: ").append(result.getString("ankunftszeit"));
      }
      if (rueckfahrt) {
        if ("J".equals(einmalig)) {
          inhalt.append("<br>&nbsp;<br><hr><b>R&uuml;ckfahrt:</b><br>").append(adresseVon);
        } else {
          inhalt.append("<br><hr><b>R&uuml;ckfahrt:</b><br>").append(adresseVon);
        }
        if (StringUtils.isNotEmpty(result.getString("rueckAnkunftszeit"))) {
          inhalt.append("<br>Zeit: ").append(result.getString("rueckAnkunftszeit"));
        }
      }
      inhalt.append("</td>\n");

      inhalt.append("                <td style=\"text-align:left;\">\n" + "                    <form action=\""
          + res.encodeURL(KnConst.HTML_BASE + "Fahrgemeinschaft/suchen.html")
          + "\" method=\"post\" enctype=\"application/x-www-form-urlencoded; charset=utf-8\" " + "accept-charset=\"utf-8\">\n"
          + "                        <p>\n" + "                            <input type=\"hidden\" name=\"Stufe\" value=\"3\">\n"
          + "                            <input type=\"hidden\" name=\"land\" value=\""
          + de.knightsoft.common.StringToHtml.convert(land) + "\">\n"
          + "                            <input type=\"hidden\" name=\"bietesuche\" value=\""
          + de.knightsoft.common.StringToHtml.convert(bietesuche) + "\">\n"
          + "                            <input type=\"hidden\" name=\"einmalig\" value=\""
          + de.knightsoft.common.StringToHtml.convert(einmalig) + "\">\n"
          + "                            <input type=\"hidden\" name=\"suchezielplz\" value=\""
          + de.knightsoft.common.StringToHtml.convert(suchezielplz) + "\">\n"
          + "                            <input type=\"hidden\" name=\"suchezielort\" value=\""
          + de.knightsoft.common.StringToHtml.convert(suchezielort) + "\">\n"
          + "                            <input type=\"hidden\" name=\"detailnr\" value=\""
          + de.knightsoft.common.StringToHtml.convert(result.getString("zaehler")) + "\">\n"
          + "                            <input type=\"submit\" name=\"abschicken\" " + "value=\"Details\">\n"
          + "                        </p>\n" + "                    </form>\n" + "                </td>\n"
          + "            </tr>\n");

      anzahlGefunden++;
    }

    switch (anzahlGefunden) {
      case 0:
        inhalt.append("        <p>Es wurde leider keine Fahrgemeinschaft gefunden.</p>\n");
        break;
      case 1:
        inhalt.append("        </table>\n" + "        </div>\n" + "        <p>Es wurde eine Fahrgemeinschaft gefunden.</p>\n");
        break;
      default:
        inhalt.append("        </table>\n" + "        </div>\n" + "        <p>Es wurden " + anzahlGefunden
            + " Fahrgemeinschaften gefunden.</p>\n");
        break;
    }

    inhalt.append("        <p><a href=\"" + res.encodeURL(KnConst.HTML_BASE + "Fahrgemeinschaft/suchen.html")
        + "\">Erneute Suche</a></p>\n" + "        </div>\n");

    return inhalt.toString();
  }

  /**
   * The Method <code>SuchergebnisDetail</code> generates the html output of the detailed search.
   *
   * @param res HttpServletResponse from the Servlet
   * @param fahrgemeinschaftDaten Fahrgemeinschaft data from the database
   * @param land country code
   * @param bietesuche search for "Bieter" or "Sucher"
   * @param einmalig one TimeField drive
   * @param suchezielplz zip code of the target
   * @param suchezielort name of the tartge city
   * @return HTML code of the formular
   */
  private String suchergebnisDetail(final HttpServletResponse res,
      final de.knightsoft.knightsoftnet.carpool.CarCheck fahrgemeinschaftDaten, final String land, final String bietesuche,
      final String einmalig, final String suchezielplz, final String suchezielort) {
    return "        <div style=\"text-align:center;\">\n" + "        <form action=\""
        + res.encodeURL(KnConst.HTML_BASE + "Fahrgemeinschaft/suchen.html")
        + "\" method=\"post\" enctype=\"application/x-www-form-urlencoded; charset=utf-8\" " + "accept-charset=\"utf-8\">\n"
        + "            <p><input type=\"hidden\" name=\"Stufe\" value=\"1\">\n"
        + "            <input type=\"hidden\" name=\"land\" value=\"" + de.knightsoft.common.StringToHtml.convert(land)
        + "\">\n" + "            <input type=\"hidden\" name=\"bietesuche\" value=\""
        + de.knightsoft.common.StringToHtml.convert(bietesuche) + "\">\n"
        + "            <input type=\"hidden\" name=\"einmalig\" value=\"" + de.knightsoft.common.StringToHtml.convert(einmalig)
        + "\">\n" + "            <input type=\"hidden\" name=\"suchezielplz\" value=\""
        + de.knightsoft.common.StringToHtml.convert(suchezielplz) + "\">\n"
        + "            <input type=\"hidden\" name=\"suchezielort\" value=\""
        + de.knightsoft.common.StringToHtml.convert(suchezielort) + "\"></p>\n" + fahrgemeinschaftDaten.htmlTabelle(true)
        + "            <p><input type=\"submit\" name=\"submit\" value=\"Zur&uuml;ck\"></p>\n" + "        </form>\n"
        + "        </div>\n";
  }

  /**
   * The Method <code>verarbeitungSucheKompakt</code> is used for a simple search.
   *
   * @param res HttpServletResponse from the Servlet
   * @param land country code
   * @param bietesuche search for "Bieter" or "Sucher"
   * @param punique one TimeField drive
   * @param suchezielplz zip code of the target
   * @param suchezielort name of the tartge city
   * @return HTML code of the formular
   */
  private String verarbeitungSucheKompakt(final HttpServletResponse res, final String land, final String bietesuche,
      final String punique, final String suchezielplz, final String suchezielort) throws de.knightsoft.common.TextException {
    final StringBuilder sqlSuche = new StringBuilder(256);
    final StringBuilder sqlSort = new StringBuilder(128);

    String einmalig = punique;
    sqlSuche.append("land=" + de.knightsoft.common.StringToSql.convert(land) + " AND bietesuche="
        + de.knightsoft.common.StringToSql.convert(bietesuche) + " AND eintrag_gesperrt='N' AND einmalige_fahrt=");
    sqlSort.append("abfahrtsplz");
    if (einmalig == null || einmalig.equals("N")) {
      einmalig = "N";
    } else {
      einmalig = "J";
      sqlSort.append(", abfahrtsdatum");
    }
    sqlSuche.append(de.knightsoft.common.StringToSql.convert(einmalig));

    if (StringUtils.isEmpty(suchezielplz) && StringUtils.isEmpty(suchezielort)) {
      throw new de.knightsoft.common.TextException(
          "Für die Suche muss entweder die Postleitzahl oder die " + "Ortsbezeichnung angegeben werden!");
    }

    if (StringUtils.isNotEmpty(suchezielplz)) {
      if (suchezielplz.length() < 3) {
        throw new de.knightsoft.common.TextException(
            "Die Postleitzahl ist nicht korrekt gefüllt, eine Suche ist nicht möglich!");
      }

      sqlSuche.append(" AND (" + de.knightsoft.common.StringToSql.searchString("zielplz", suchezielplz)
          + " OR (rueckfahrt='J' AND " + de.knightsoft.common.StringToSql.searchString("abfahrtsplz", suchezielplz) + "))");
    }

    if (StringUtils.isNotEmpty(suchezielort)) {
      sqlSuche.append(" AND (" + de.knightsoft.common.StringToSql.searchString("zielort", "*" + suchezielort + "*")
          + " OR (rueckfahrt='J' AND " + de.knightsoft.common.StringToSql.searchString("abfahrtsort", "*" + suchezielort + "*")
          + "))");
    }

    // Lesezugriff auf die Datenbank
    try (final ResultSet result = myDataBase.createStatement().executeQuery(
        "SELECT * FROM KnightSoft_Fahrgemeinschaft WHERE " + sqlSuche.toString() + " ORDER BY " + sqlSort.toString())) {

      // Ausgabe der Suchergebnisseite
      final String resultString = suchergebnisUebersicht(res, result, land, bietesuche, einmalig, suchezielplz, suchezielort);
      return resultString;
    } catch (final java.sql.SQLException e) {
      throw new de.knightsoft.common.TextException(
          "Fehler beim Datenbanksuchen: " + e.toString() + "\n\nSQLSuche:\n" + sqlSuche.toString(), e);
    }
  }

  /**
   * The Method <code>verarbeitungSucheKomplex</code> is used for detailed search. Not implemented here, its just a call to
   * verarbeitungSucheKompakt.
   *
   * @param res HttpServletResponse from the Servlet
   * @param land country code
   * @param bietesuche search for "Bieter" or "Sucher"
   * @param einmalig one TimeField drive
   * @param suchezielplz zip code of the target
   * @param suchezielort name of the tartge city
   * @return HTML code of the formular
   */
  private String verarbeitungSucheKomplex(final HttpServletResponse res, final String land, final String bietesuche,
      final String einmalig, final String suchezielplz, final String suchezielort) throws de.knightsoft.common.TextException {
    return verarbeitungSucheKompakt(res, land, bietesuche, einmalig, suchezielplz, suchezielort);
  }

  /**
   * The Method <code>verarbeitungDetailansicht</code> is used for to display the detailed search results.
   *
   * @param req HttpServletRequest from the Servlet
   * @param res HttpServletResponse from the Servlet
   * @param land country code
   * @param bietesuche search for "Bieter" or "Sucher"
   * @param einmalig one TimeField drive
   * @param suchezielplz zip code of the target
   * @param suchezielort name of the tartge city
   * @return HTML code of the formular
   */
  private String verarbeitungDetailansicht(final HttpServletRequest req, final HttpServletResponse res, final String land,
      final String bietesuche, final String einmalig, final String suchezielplz, final String suchezielort)
      throws de.knightsoft.common.TextException {
    final String dbNr = req.getParameter("detailnr");

    try (final PreparedStatement searchSQLOneStatement = myDataBase.prepareStatement(Search.SEARCH_SQL_ONE)) {
      // Lesezugriff auf die Datenbank
      de.knightsoft.knightsoftnet.carpool.CarCheck fahrgemeinschaftDaten = null;
      searchSQLOneStatement.clearParameters();
      searchSQLOneStatement.setInt(1, Integer.parseInt(dbNr));
      try (final ResultSet result = searchSQLOneStatement.executeQuery()) {
        if (!result.next()) {
          throw new de.knightsoft.common.TextException("Diese Eintragung existiert nicht mehr!");
        }
        fahrgemeinschaftDaten = new de.knightsoft.knightsoftnet.carpool.CarCheck(result);
      }

      // Ausgabe der Suchergebnisseite
      return suchergebnisDetail(res, fahrgemeinschaftDaten, land, bietesuche, einmalig, suchezielplz, suchezielort);
    } catch (final java.sql.SQLException e) {
      throw new de.knightsoft.common.TextException("Fehler beim Datenbankzugriff für Detailansicht: " + e.toString(), e);
    }
  }

  /**
   * The Method <code>HTMLPage</code> generates the html formular.
   *
   * @param res HttpServletResponse from the Servlet
   * @param hint Hint to be displayed on the screen
   * @param session The Data of the current Session
   * @param navTyp Navigation type
   * @return HTML code of the formular
   */
  @Override
  protected String htmlPage(final HttpServletResponse res, final String hint, final HttpSession session, final String navTyp) {
    session.setAttribute(servletName + AbstractVisualDb.SELECTED_FIELD_TEXT + serviceName, "suchezielort");
    return "        <p>F&uuml;r Sch&auml;den, die im Rahmen des von uns vermittelten "
        + "Vertragsverh&auml;ltnisses entstehen, k&ouml;nnen wir keine Haftung &uuml;bernehmen." + "</p>\n" + "        <p>\n"
        + "        Hier k&ouml;nnen Sie unsere Datenbank nach Fahrgemeinschaften durchsuchen.\n"
        + "        Dazu w&auml;hlen Sie bitte, ob sie eine einmalige Mitfahrgelegenheit suchen, "
        + "oder eine dauerhafte (Pendler).\n" + "        Geben Sie bitte auch die Postleitzahl und/oder den Ortsnamen Ihres "
        + "gew&uuml;nschten Zielortes in die Suchfelder ein und dr&uuml;cken den Suchen-Knopf.\n" + "        </p>\n"
        + "        <p><b>Hinweise:</b> Sie k&ouml;nnen ab der dritten Stelle der PLZ den "
        + "Platzhalter * verwenden, um einen gr&ouml;&szlig;eren Bereich abzudecken,\n"
        + "        unvollst&auml;ndige Ortsangaben werden automatisch komplettiert. Weitere " + "Hinweise auf der <a href=\""
        + res.encodeURL(KnConst.HTML_BASE + "Fahrgemeinschaft/faq.html") + "\">FAQ-Seite</a>.</p>\n"
        + "        <div style=\"text-align:center;\">\n"
        + (hint == null ? "" : "    <h2>" + de.knightsoft.common.StringToHtml.convert(hint) + "</h2>\n")
        + "            <p>Suche nach Einmaligen- und Pendler-Fahrgemeinschaften:</p>\n" + "            <form action=\""
        + res.encodeURL(KnConst.HTML_BASE + "Fahrgemeinschaft/suchen.html")
        + " \" method=\"POST\" enctype=\"application/x-www-form-urlencoded; charset=utf-8\" "
        + "accept-charset=\"utf-8\" OnSubmit=\"return chkSuchen()\">\n"
        + "                <table style=\"margin-left:auto; margin-right:auto;\" cellspacing=\"2\""
        + " cellpadding=\"2\" border=\"0\">\n" + "                    <tr>\n"
        + "                        <td style=\"text-align:left;\">Land:</td>\n"
        + "                        <td style=\"text-align:left;\">\n"
        + "                            <input type=\"hidden\" name=\"Stufe\" value=\"1\">\n"
        + "                            <select name=\"land\" size=\"1\">\n"
        + "                                <option value=\"de\">Deutschland</option>\n"
        + "                                <option value=\"at\">&Ouml;sterreich</option>\n"
        + "                                <option value=\"ch\">Schweiz</option>\n" + "                            </select>\n"
        + "                        </td>\n" + "                    </tr>\n" + "                    <tr>\n"
        + "                        <td style=\"text-align:left;\">Biete/Suche:</td>\n"
        + "                        <td style=\"text-align:left;\">\n"
        + "                            <select name=\"bietesuche\" size=\"1\">\n"
        + "                                <option value=\"b\">Ich suche eine Mitfahrgelegenheit" + "</option>\n"
        + "                                <option value=\"s\">Ich suche Leute, die mit mir " + "mitfahren wollen</option>\n"
        + "                            </select>\n" + "                        </td>\n" + "                    </tr>\n"
        + "                    <tr>\n" + "                        <td style=\"text-align:left;\">"
        + "Nur einmalige Mitfahrgelegenheit:</td>\n" + "                        <td style=\"text-align:left;\">"
        + "<input name=\"einmalig\" type=\"checkbox\"></td>\n" + "                    </tr>\n" + "                    <tr>\n"
        + "                        <td style=\"text-align:left;\">PLZ des Zielorts:</td>\n"
        + "                        <td style=\"text-align:left;\">"
        + "<input type=\"Text\" name=\"suchezielplz\" size=\"5\" maxlength=\"5\"></td>\n" + "                    </tr>\n"
        + "                    <tr>\n" + "                        <td style=\"text-align:left;\">Zielort:</td>\n"
        + "                        <td style=\"text-align:left;\">"
        + "<input type=\"Text\" name=\"suchezielort\" size=\"15\" maxlength=\"50\"></td>\n" + "                    </tr>\n"
        + "                    <tr>\n" + "                        <td style=\"text-align:center;\" colspan=\"2\">"
        + "<input type=\"submit\" value=\"Suchen\"></td>\n" + "                    </tr>\n" + "                </table>\n"
        + "            </form>\n" + "        </div>\n";
  }

  /**
   * The Method <code>doTheWork</code> coordinates all the work in this class
   *
   * @param req HttpServletRequest from the Servlet
   * @param res HttpServletResponse from the Servlet
   * @param session The Data of the current Session
   * @return html code of the formular
   * @exception de.knightsoft.common.TextException when a error in checking the passwords occours
   */
  @Override
  public String doTheWork(final HttpServletRequest req, final HttpServletResponse res, final HttpSession session)
      throws de.knightsoft.common.TextException {

    final String stufe = req.getParameter("Stufe");
    final String land = req.getParameter("land");
    final String bietesuche = req.getParameter("bietesuche");
    final String einmalig = req.getParameter("einmalig");
    final String suchezielplz = req.getParameter("suchezielplz");
    final String suchezielort = req.getParameter("suchezielort");
    final String navTyp = "suchen";
    String formular = null;

    try {
      session.removeAttribute(servletName + AbstractVisualDb.SELECTED_FIELD_TEXT + serviceName);
      switch (StringUtils.defaultString(stufe)) {
        // Stufe 1 = Suche kompakt (nur Eingabe der Vorwahl)
        case "1":
          formular = verarbeitungSucheKompakt(res, land, bietesuche, einmalig, suchezielplz, suchezielort);
          break;
        // Stufe 2 = Suche detailliert (komplexe Selektionsmöglichkeiten
        case "2":
          formular = verarbeitungSucheKomplex(res, land, bietesuche, einmalig, suchezielplz, suchezielort);
          break;
        // Stufe 3 = Bestätigung oder Ablehnung der Löschbestätigung
        case "3":
          formular = verarbeitungDetailansicht(req, res, land, bietesuche, einmalig, suchezielplz, suchezielort);
          break;
        default:
          formular = htmlPage(res, null, session, navTyp);
          break;
      }
    } catch (final de.knightsoft.common.TextException e) {
      formular = htmlPage(res, e.toString(), session, navTyp);
    }

    return formular;
  }

  /**
   * The Method <code>allowedToSee</code> tells you if the currently logged in user is allowed to access this application.
   *
   * @param session Data of the current session
   * @return true if it is allowed for this user
   */
  @Override
  public boolean allowedToSee(final HttpSession session) {
    return true;
  }

  /**
   * The Method <code>allowedToChange</code> tells you if the currently logged in user is allowed to change data in this
   * application.
   *
   * @param session Data of the current session
   * @return true if it is allowed for this user
   */
  @Override
  public boolean allowedToChange(final HttpSession session) {
    return true;
  }
}
