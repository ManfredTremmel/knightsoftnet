/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.knightsoftnet.carpool;

import de.knightsoft.knightsoftnet.common.KnConst;

import javax.naming.InitialContext;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import java.io.IOException;
import java.sql.Connection;

public class InsertServlet extends HttpServlet {

  private static final long serialVersionUID = -9024455170959095330L;

  protected Connection myDataBase;

  /**
   * init-Methode, DB-Öffnen.
   */
  @Override
  public void init(final ServletConfig config) throws ServletException {
    super.init(config);

    try {
      final InitialContext ic = new InitialContext();
      final DataSource lDataSource = (DataSource) ic.lookup("java:comp/env/jdbc/db_knightsoft");
      myDataBase = lDataSource.getConnection();
      ic.close();
    } catch (final java.sql.SQLException e) {
      throw new ServletException(e);
    } catch (final javax.naming.NamingException e) {
      throw new ServletException(e);
    }
  }

  /**
   * Nach der Eingabe, Daten auf Gültigkeit prüfen und zur Bestätigung durch den Benutzer nochmals ausgeben.
   */
  private void verarbeitungStufe1(final HttpServletRequest req, final HttpServletResponse res, final ServletOutputStream out,
      final de.knightsoft.knightsoftnet.carpool.CarCheck fahrgemeinschaftDaten)
      throws de.knightsoft.common.TextException, IOException {
    try {
      fahrgemeinschaftDaten.checkDaten();

      final String Inhalt = "<div style=\"text-align:center;\"><h2>Pr&uuml;fen Sie die eingegebenen Daten, "
          + "dr&uuml;cken Sie den \"Best&auml;tigen\" Knopf wenn die Daten korrekt sind und "
          + "Sie mit den Konditionen einverstanden sind</h2></div>\n" + "<P>&nbsp;</P>\n" + "<form action=\""
          + res.encodeURL(KnConst.SERVLET_URL + "KnightSoftFahrgemeinschaftInsert")
          + "\" method=\"POST\" enctype=\"application/x-www-form-urlencoded; charset=utf-8\" " + "accept-charset=\"utf-8\">\n"
          + "<input type=\"hidden\" name=\"Stufe\" value=\"2\">\n" + fahrgemeinschaftDaten.htmlHidden() + "\n"
          + fahrgemeinschaftDaten.htmlTabelle(false) + "\n" + KnConst.HINWEIS_REGISTER + "\n"
          + "<p style=\"text-align:center;\"><input type=\"submit\" name=\"Submittype\" "
          + "value=\"Best&auml;tigen\"><input type=\"submit\" name=\"Submittype\" " + "value=\"Abbrechen\"></p>\n"
          + "</form>\n";

      final String htmlPage =
          de.knightsoft.knightsoftnet.common.SeitenTemplate.htmlSeite(req, "KnightSoftFahrgemeinschaftInsert",
              "Daten werden eingef&uuml;gt", "", true, de.knightsoft.knightsoftnet.common.KnConst.FAHRGEMEINSCHAFT_FARBE,
              de.knightsoft.knightsoftnet.common.KnConst.FAHRGEMEINSCHAFT_TEXT, "Prüfung der Eingaben", Inhalt,
              de.knightsoft.knightsoftnet.carpool.Navigation.FAHRGEMEINSCHAFT_NAV);

      // HTML-Datei für Insertbestätigung schreiben
      out.println(htmlPage);
    } catch (final de.knightsoft.common.TextException e) {
      final de.knightsoft.knightsoftnet.carpool.ChangeServlet MyChangeServlet =
          new de.knightsoft.knightsoftnet.carpool.ChangeServlet();

      MyChangeServlet.setService("Fahrgemeinschaft");
      MyChangeServlet.setWbildReg(de.knightsoft.knightsoftnet.common.KnConst.WBILD_FAHR_REG);
      MyChangeServlet.setWurlReg(de.knightsoft.knightsoftnet.common.KnConst.WURL_FAHR_REG);
      if (fahrgemeinschaftDaten.einmaligeFahrt == null) {
        fahrgemeinschaftDaten.einmaligeFahrt = "N";
      }
      if (fahrgemeinschaftDaten.rueckfahrt == null) {
        fahrgemeinschaftDaten.rueckfahrt = "N";
      }
      if (fahrgemeinschaftDaten.news == null) {
        fahrgemeinschaftDaten.news = "J";
      }
      out.println(MyChangeServlet.changeHtml(req, res, de.knightsoft.knightsoftnet.common.KnConst.JAVA_SCRIPT_FG_URL,
          "Fehler beim Einfügen: " + e.getMessage(), fahrgemeinschaftDaten, "KnightSoftFahrgemeinschaftInsert"));
    }
  }

  /**
   * Die geprüften Daten in die Datenbank sichern und eine E-Mail an den Benutzer verschicken.
   */
  private void verarbeitungStufe2(final HttpServletRequest req, final HttpServletResponse res, final ServletOutputStream out,
      final de.knightsoft.knightsoftnet.carpool.CarCheck fahrgemeinschaftDaten)
      throws de.knightsoft.common.TextException, IOException {
    String sqlString = null;

    try {
      sqlString = fahrgemeinschaftDaten.prepareInsert();
      myDataBase.createStatement().executeUpdate("INSERT INTO KnightSoft_Fahrgemeinschaft " + sqlString);

      String inhalt = "<div style=\"text-align:center;\"><h2>Sie wurden als Fahrgemeinschaft in unsere "
          + "Datenbank aufgenommen.</h2>\n" + "<p>Eine E-Mail mit dem Passwort zum &auml;ndern oder l&ouml;schen Ihrer "
          + "Eintragung wird Ihnen automatisch zugesendet.</p>\n"
          + "<p>Bitte beachten Sie, dass Ihr Eintrag gesperrt bleibt, bis Sie sich einmal "
          + "mit dem zugesandten Passwort erfolgreich eingeloggt haben.</p>\n" + "<a href=\""
          + res.encodeURL(KnConst.SERVLET_URL + "KnightSoftFahrgemeinschaftPflege?Stufe=0") + "\"> zur&uuml;ck</a></div>\n";

      // E-Mail versenden
      try {
        sendPasswort(fahrgemeinschaftDaten.email, fahrgemeinschaftDaten.passwort, fahrgemeinschaftDaten.geschlecht,
            fahrgemeinschaftDaten.name);
      } catch (final IOException e) {
        inhalt += "Fehler beim Versenden der E-Mail: " + e.toString() + "\n"; // NOPMD
      }

      out.println(de.knightsoft.knightsoftnet.common.SeitenTemplate.htmlSeite(req, "KnightSoftFahrgemeinschaftInsert",
          "Aufnahme der Fahrgemeinschaft war erfolgreich", "", true,
          de.knightsoft.knightsoftnet.common.KnConst.FAHRGEMEINSCHAFT_FARBE,
          de.knightsoft.knightsoftnet.common.KnConst.FAHRGEMEINSCHAFT_TEXT, "Registrierung war erfolgreich", inhalt,
          de.knightsoft.knightsoftnet.carpool.Navigation.FAHRGEMEINSCHAFT_NAV));
    } catch (final java.sql.SQLException e) {
      // Um die Fehlerbehandlung nicht zweimal zu machen, schicken
      // wir die Meldung einfach weiter
      throw new de.knightsoft.common.TextException(e.toString() + "<p>Felder: " + sqlString, e);
    }
  }

  /**
   * Nach der Eingabe, Daten auf Gültigkeit prüfen und zur Bestätigung durch den Benutzer nochmals ausgeben.
   */
  private void sendPasswort(final String empfaengerEMail, final String passwort, final String geschlecht, final String name)
      throws IOException {
    final String eMailText = de.knightsoft.knightsoftnet.common.KnConst.registerEmail(name, geschlecht, empfaengerEMail,
        passwort, "Fahrgemeinschaften");

    new de.knightsoft.common.SendEMail(de.knightsoft.knightsoftnet.common.KnConst.EMAIL_FG,
        de.knightsoft.common.Constants.ORGANISATION, empfaengerEMail,
        "Registrierung in der " + de.knightsoft.common.Constants.COMPANY + " Fahrgemeinschaften-Vermittlung", //
        eMailText);
  }

  /**
   * Nach der Eingabe, Daten auf Gültigkeit prüfen und zur Bestätigung durch den Benutzer nochmals ausgeben.
   */
  private void verarbeitungAbbruch(final HttpServletRequest req, final HttpServletResponse res, final ServletOutputStream out)
      throws IOException {
    out.println(de.knightsoft.knightsoftnet.common.SeitenTemplate.htmlSeite(req, "KnightSoftFahrgemeinschaftInsert",
        "Abbruch der Fahrgemeinschafteintragung", "", true, de.knightsoft.knightsoftnet.common.KnConst.FAHRGEMEINSCHAFT_FARBE,
        de.knightsoft.knightsoftnet.common.KnConst.FAHRGEMEINSCHAFT_TEXT, "Abbruch der Eintragung",
        "<div style=\"text-align:center;\"><h2>Sie haben den Abbruch-Knopf bet&auml;tigt und wurden nicht in unsere Datenbank aufgenommen.</h2>\n<a href=\""
            + res.encodeURL(KnConst.SERVLET_URL + "KnightSoftFahrgemeinschaftPflege?Stufe=0") + "\"> zur&uuml;ck</a></div>\n",
        de.knightsoft.knightsoftnet.carpool.Navigation.FAHRGEMEINSCHAFT_NAV));
  }

  /**
   * Auslesen der gePOSTeden Suchkriterien, zugriff auf die Datenbank und Einfügen des Eintrags, wenn möglich.
   */
  @Override
  public void doPost(final HttpServletRequest req, final HttpServletResponse res) throws ServletException, IOException {

    // Setze als erstes den "content type" Header der Antwortseite auf HTML
    res.setContentType("text/html");

    // Writer für die HTML-Datei holen
    final ServletOutputStream out = res.getOutputStream();

    // Eingaben einlesen
    final de.knightsoft.knightsoftnet.carpool.CarCheck fahrgemeinschaftDaten =
        new de.knightsoft.knightsoftnet.carpool.CarCheck(req);

    try {
      if ("1".equals(fahrgemeinschaftDaten.stufe)) {
        verarbeitungStufe1(req, res, out, fahrgemeinschaftDaten);
      } else {
        final String typ = req.getParameter("Submittype");

        if ("Abbrechen".equals(typ)) {
          verarbeitungAbbruch(req, res, out);
        } else {
          verarbeitungStufe2(req, res, out, fahrgemeinschaftDaten);
        }
      }
      out.close();
    } catch (final de.knightsoft.common.TextException e) {
      // HTML-Datei für Fehlermeldung schreiben
      out.println(de.knightsoft.knightsoftnet.common.SeitenTemplate.htmlSeite(req, "KnightSoftFahrgemeinschaftInsert",
          "Fehler bei Datenbank-Insert", "", true, KnConst.FAHRGEMEINSCHAFT_FARBE, KnConst.FAHRGEMEINSCHAFT_TEXT,
          "Fehler beim Anlegen der Daten",
          "<div style=\"text-align:center;\"><h2>" + e.toHtml() + "</h2>\n<a href=\""
              + res.encodeURL(KnConst.SERVLET_URL + "KnightSoftFahrgemeinschaftPflege?Stufe=0") + "\"> zur&uuml;ck</a></div>\n",
          de.knightsoft.knightsoftnet.carpool.Navigation.FAHRGEMEINSCHAFT_NAV));
      out.close();
    }
  }

  @Override
  public String getServletInfo() {
    return "Insert a new car pool into the database\n\n" + de.knightsoft.common.Constants.COPYRIGHT;
  }
}
