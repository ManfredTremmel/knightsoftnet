/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.knightsoftnet.carpool;

import de.knightsoft.knightsoftnet.common.KnConst;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * The <code>Faq</code> class is based on the abstract class de.knightsoft.common.AbstractVisualDb and implements the faq page
 * of the KnightSoft Tipprunde.
 *
 *
 * @author Manfred Tremmel
 * @version 2.0.0, 19.08.2006
 */
public class Faq extends de.knightsoft.common.AbstractVisualDb {

  /**
   * Constructor.
   *
   * @param thisDatabase Connection to database
   * @param servletname Name of the Servlet
   * @exception SQLException if a database access error occurs
   * @exception de.knightsoft.common.TextException to give out a text message
   * @see de.knightsoft.common.TextException
   */
  public Faq(final Connection thisDatabase, final String servletname) throws SQLException, de.knightsoft.common.TextException {
    super(thisDatabase, KnConst.HTML_BASE, servletname, KnConst.GIF_URL + "16x16/faq.png", "FAQ",
        "Fahrgemeinschaften-FAQ (Frequently Asked Questions)");
  }

  /**
   * The Method <code>HTMLPage</code> generates the html formular.
   *
   * @param res HttpServletResponse from the Servlet
   * @param hint Hint to be displayed on the screen
   * @param session The Data of the current Session
   * @param navTyp Navigation type
   * @return HTML code of the formular
   */
  @Override
  protected String htmlPage(final HttpServletResponse res, final String hint, final HttpSession session, final String navTyp) {
    return "<h1><a name=\"inhalt\">Diese FAQ enth&auml;lt Fragen und Antworten zu folgenden " + "Punkten:</a></h1>\n" + "<ol>\n"
        + "    <li><a href=\"#faq\">Fragen zur FAQ</a>\n" + "        <ul>\n"
        + "            <li><a href=\"#faq_was\">Was bedeutet FAQ?</a></li>\n" + "        </ul>\n" + "    </li>\n"
        + "    <li><a href=\"#suche\">Fragen zum Suchen einer Fahrgemeinschaft</a>\n" + "        <ul>\n"
        + "            <li><a href=\"#su_findnix\">Ich finde keine Fahrgemeinschaft/"
        + "Mitfahrgelegenheit dorthin, wo ich hin will. Was kann ich tun?</a></li>\n" + "        </ul>\n" + "    </li>\n"
        + "    <li><a href=\"#registrieren\">Fragen zur Anmeldung einer Fahrgemeinschaft und zur " + "Freischaltung</a>\n"
        + "        <ul>\n" + "            <li><a href=\"#reg_findnix\">Ich habe mich angemeldet, finde meine Eintrag "
        + "mit der Suchfunktion jedoch nicht.</a></li>\n"
        + "            <li><a href=\"#reg_freiwarum\">Welchen Zweck hat die Freischaltung?" + "</a></li>\n"
        + "            <li><a href=\"#reg_freiwie\">Ich komme mit der Freischaltung nicht "
        + "zurecht, wie geht das?</a></li>\n"
        + "            <li><a href=\"#reg_pwneu\">Ich habe mich eingetragen aber kein Passwort "
        + "erhalten. Wie komm ich jetzt rein?</a></li>\n"
        + "            <li><a href=\"#reg_anfragen\">Wie verbessere ich meine Chancen auf " + "Anfragen?</a></li>\n"
        + "        </ul>\n" + "    </li>\n" + "    <li><a href=\"#pflege\">Fragen zur Daten&auml;nderung und -pflege</a>\n"
        + "        <ul>\n" + "            <li><a href=\"#pflege_pwaender\">Ich kann mir das zugesandte Passwort "
        + "nicht merken, kann ich es &auml;ndern?</a></li>\n"
        + "            <li><a href=\"#pflege_logsperr\">Ich hab mich dreimal beim Login "
        + "vertippt, jetzt geht nichts mehr. Was tun?</a></li>\n"
        + "            <li><a href=\"#pflege_logout\">Was bedeutet dieser Logout-Knopf bei " + "der Datenpflege?</a></li>\n"
        + "        </ul>\n" + "    </li>\n" + "    <li><a href=\"#sonst\">Sonstige Fragen</a>\n" + "        <ul>\n"
        + "            <li><a href=\"#sonst_erfolg\">Ich finde den Service toll, kann ich zum " + "Erfolg beitragen?</a></li>\n"
        + "        </ul>\n" + "    </li>\n" + "</ol>\n" + "<h2><a name=\"faq\">Fragen zur FAQ</a></h2>\n"
        + "<h4><a name=\"faq_was\">Was bedeutet FAQ?</a></h4>\n"
        + "<p>FAQ ist die Abk&uuml;rzung f&uuml;r das Englische &quot;Frequently Asked "
        + "Questions&quot; und bedeutet &uuml;bersetzt soviel wie &quot;H&auml;ufig gestellte "
        + "Fragen&quot;. Sie finden hier also die am h&auml;ufigsten gestellten Fragen und die "
        + "passenden Antworten dazu.</p>\n" + "<p style=\"text-align:right\"><a href=\"#inhalt\">Zur&uuml;ck</a></p>\n"
        + "<h2><a name=\"suche\">Fragen zum Suchen einer Fahrgemeinschaft</a></h2>\n"
        + "<h4><a name=\"su_findnix\">Ich finde keine Fahrgemeinschaft/Mitfahrgelegenheit "
        + "dorthin, wo ich hin will. Was kann ich tun?</a></h4>\n"
        + "<p>Nicht zu jedem Ort wurden Fahrgemeinschaften eingetragen. Versuchen Sie es neben "
        + "der Suche per Postleitzahl auch mit der Ortsbezeichnung. Gerade in gro&szlig;en "
        + "St&auml;dten gibt es mehrere Postleitzahlen, zum Teil hat sogar eine Strasse mehrere " + "Postleitzahlen.</p>\n"
        + "<p style=\"text-align:right\"><a href=\"#inhalt\">Zur&uuml;ck</a></p>\n"
        + "<h2><a name=\"registrieren\">Fragen zur Anmeldung einer Fahrgemeinschaft und zur " + "Freischaltung</a></h2>\n"
        + "<h4><a name=\"reg_findnix\">Ich habe mich angemeldet, finde meine Eintrag mit der "
        + "Suchfunktion jedoch nicht.</a></h4>\n"
        + "<p>Jeder neue Eintrag bleibt so lange gesperrt, bis Sie sich einmal mit dem "
        + "zugesandten Passwort korrekt eingeloggt haben. Beachten Sie hierzu auch die " + "nachfolgenden Punkte.</p>\n"
        + "<p style=\"text-align:right\"><a href=\"#inhalt\">Zur&uuml;ck</a></p>\n"
        + "<h4><a name=\"reg_freiwarum\">Welchen Zweck hat die Freischaltung?</a></h4>\n"
        + "<p>Bevor wir unsere Services designed haben, haben wir uns nat&uuml;rlich "
        + "Konkurrenzseiten angesehen. Dabei sind uns viele unbrauchbare oder mehrfache "
        + "Eintragungen aufgefallen. Um ein qualitativ hochwertiges Angebot anbieten zu "
        + "k&ouml;nnen, mu&szlig;ten wir Ma&szlig;nahmen treffen, um Scherzbolde und "
        + "Quertreiber drau&szlig;en zu halten. Unser Passwort- und Freischaltungssystem "
        + "bietet sicher keinen hundertprozentigen Schutz, bisher scheint es jedoch zu " + "Funktionieren.</p>\n"
        + "<p>F&uuml;r Sie ergibt sich aus dem Passwortsystem der Vorteil, Ihren Eintrag "
        + "jederzeit &auml;ndern zu k&ouml;nnen.</p>\n"
        + "<p style=\"text-align:right\"><a href=\"#inhalt\">Zur&uuml;ck</a></p>\n"
        + "<h4><a name=\"reg_freiwie\">Ich komme mit der Freischaltung nicht zurecht, wie " + "geht das?</a></h4>\n"
        + "<p>Sobald Sie Ihre Eintragung f&uuml;r den Service beendet haben, wird Ihnen umgehend "
        + "eine E-Mail mit einem automatisch generierten Passwort zugesandt.</p>\n"
        + "<p>Zur Freischaltung Ihres Eintrags bet&auml;tigen Sie den Login-Knopf in unserer "
        + "Navigationsleiste und tragen dort Ihre E-Mail-Adresse und das gesendete Passwort ein. "
        + "Beachten Sie die Gro&szlig;- und Kleinschreibung! Um Tippfehler zu vermeiden, "
        + "empfiehlt es sich, das Passwort aus der E-Mail zu kopieren und im Browser "
        + "reinzukopieren, markieren Sie hierzu aber bitte wirklich nur das Passwort und keine "
        + "anderen Teile der Mail.</p>\n"
        + "<p>Dr&uuml;cken Sie nun noch den Absenden-Knopf. Wenn alles funktioniert hat, erhalten "
        + "Sie die Anzeige Ihrer Daten, sowie die Meldung, dass Ihr Eintrag freigegeben wurde. "
        + "Auf diese Weise k&ouml;nnen Sie auch jederzeit Ihre Daten &Auml;ndern oder " + "L&ouml;schen.</p>\n"
        + "<p style=\"text-align:right\"><a href=\"#inhalt\">Zur&uuml;ck</a></p>\n"
        + "<h4><a name=\"reg_pwneu\">Ich habe mich eingetragen aber kein Passwort erhalten. "
        + "Wie komm ich jetzt rein?</a></h4>\n"
        + "<p>Sollten Sie Ihr Passwort nicht erhalten oder dieses vergessen haben, gehen Sie auf "
        + "die Login-Seite. Tragen Sie Ihre E-Mail-Adresse ein und bet&auml;tigen Sie den "
        + "&quot;Passwort vergessen *&quot; Knopf. Ein neues Passwort sollte Ihnen zugeschickt " + "werden.</p>\n"
        + "<p>Sollten Sie die Meldung erhalten, dass kein Benutzer mit diesem Passwort "
        + "eingetragen ist, haben Sie sich bei der Registrierung vermutlich vertippt. In diesem "
        + "Fall m&uuml;ssen Sie sich erneut registrieren. Der Eintrag mit dem fehlerhaften "
        + "Passwort wird nach 30 Tagen automatisch gel&ouml;scht.</p>\n"
        + "<p style=\"text-align:right\"><a href=\"#inhalt\">Zur&uuml;ck</a></p>\n"
        + "<h4><a name=\"reg_anfragen\">Wie verbessere ich meine Chancen auf Anfragen?</a></h4>\n"
        + "<p>Schreiben Sie etwas zu Ihrer Person in das Bemerkungsfeld. Die Leute wissen gerne, "
        + "mit wem sie es zu tun haben. Sollten Sie eine eigene Internetseite betreiben, tragen "
        + "Sie sie im Homepage-Feld ein. Teilen Sie dem potentiellen Auftraggeber dort die Daten "
        + "mit, die Sie f&uuml;r sinnvoll erachten, in unserem Anmeldeformular jedoch nicht " + "untergebracht haben.</p>\n"
        + "<p><a href=\"#sonst_erfolg\">Siehe auch die Frage: 'Ich finde den Service toll, kann "
        + "ich zum Erfolg beitragen?'</a></p>\n" + "<p style=\"text-align:right\"><a href=\"#inhalt\">Zur&uuml;ck</a></p>\n"
        + "<h2><a name=\"pflege\">Fragen zur Daten&auml;nderung und -pflege</a></h2>\n"
        + "<h4><a name=\"pflege_pwaender\">Ich kann mir das zugesandte Passwort nicht merken, "
        + "kann ich es &auml;ndern?</a></h4>\n"
        + "<p>Auf der Login-Seite gibt es die M&ouml;glichkeit, das Passwort zu &auml;ndern. "
        + "Geben Sie Ihr bisheriges Passwort ein. Bei &quot;Passwort neu&quot; und &quot;Passwort "
        + "best&auml;tigen&quot; tragen Sie jeweils das neu, von Ihnen gew&uuml;nschte Passwort "
        + "ein und Klicken Sie den &quot;Passwort &auml;ndern&quot; Knopf. Das neue Passwort "
        + "sollte aktiv sein, eine entsprechende Meldung wird angezeigt.</p>\n"
        + "<p>Bitte beachten Sie, dass das Passwort Ihre Daten vor Manipulationen sch&uuml;tzen "
        + "soll. Vermeiden Sie nach M&ouml;glichkeit also Namen, Telefonnummern oder Geburtsdaten "
        + "von Bekannten und Verwandten, sie stellen die ersten Angriffspunkte dar.</p>\n"
        + "<p style=\"text-align:right\"><a href=\"#inhalt\">Zur&uuml;ck</a></p>\n"
        + "<h4><a name=\"pflege_logsperr\">Ich hab mich dreimal beim Login vertippt, jetzt geht "
        + "nichts mehr. Was tun?</a></h4>\n"
        + "<p>Nach drei Fehlversuchen sind f&uuml;r die aktuelle Sitzung keine weiteren Versuche "
        + "erlaubt, dies dient dem Schutz Ihrer Daten. Damit soll Hackern die M&ouml;glichkeit "
        + "genommen werden, durch automatisiertes Ausprobieren das Passwort zu ermitteln.</p>\n"
        + "<p>Wenn Sie Ihren Browser beenden und neu starten, stehen Ihnen weitere drei Versuche " + "zur Verf&uuml;gung.</p>\n"
        + "<p style=\"text-align:right\"><a href=\"#inhalt\">Zur&uuml;ck</a></p>\n"
        + "<h4><a name=\"pflege_logout\">Was bedeutet dieser Logout-Knopf bei der Datenpflege?" + "</a></h4>\n"
        + "<p>Wenn Sie sich eingeloggt haben, k&ouml;nnen Sie Ihre Daten bearbeiten oder gar "
        + "l&ouml;schen. Um nach einem Seitenwechsel nicht jedesmal wieder die E-Mail und das "
        + "Passwort eingeben zu m&uuml;ssen, ist dies nur einmal pro Surfsitzung notwendig. Da "
        + "der Computer nun nicht wei&szlig;, wer vor ihm sitzt erm&ouml;glicht dies "
        + "selbstverst&auml;ndlich auch anderen Leuten, an diesem Rechner die Daten zu "
        + "Ver&auml;ndern. An Ihrem eigenen Rechner sollte das unproblematisch sein, aber an "
        + "einem Unirechner oder im Internet-Caf&eacute; wissen Sie nicht, wer f&uuml;nf Minuten "
        + "nach Ihnen am Rechner sitzt.</p>\n" + "<p>Benutzen Sie den &quot;Logout&quot;-Knopf, l&auml;sst das den Browser die "
        + "Zugangsdaten vergessen, damit sind Ihre Daten in jedem Fall sicher.</p>\n"
        + "<p style=\"text-align:right\"><a href=\"#inhalt\">Zur&uuml;ck</a></p>\n"
        + "<h2><a name=\"sonst\">Sonstige Fragen</a></h2>\n"
        + "<h4><a name=\"sonst_erfolg\">Ich finde den Service toll, kann ich zum Erfolg " + "beitragen?</a></h4>\n"
        + "<p>Wenn Sie uns helfen wollen, empfehlen Sie uns an Ihre Verwandten, Bekannten "
        + "und Arbeitskollegen weiter. Sollten Sie eine eigene Homepage betreiben, sind Links "
        + "auf unsere Seiten stets willkommen.</p>\n"
        + "<p>Wenn Sie Ideen f&uuml;r Verbesserungen haben, dann schreiben Sie uns "
        + "(<a href=\"mailto:Fahrgemeinschaften@KnightSoft-Net.de?subject=Verbesserungsvorschlag"
        + "\">Fahrgemeinschaft@KnightSoft-Net.de</a>), Ihre Anregungen und W&uuml;nsche sind " + "jederzeit gern gesehen.</p>\n"
        + "<p>Kennen Sie Suchmaschinen, in den wir noch nicht vertreten sind? Teilen Sie uns "
        + "mit, wo noch Bedarf besteht (<a href=\"mailto:Fahrgemeinschaft@KnightSoft-Net.de?"
        + "subject=Suchmaschinenvorschlag\">Fahrgemeinschaft@KnightSoft-Net.de</a>), oder "
        + "schlagen Sie uns selbst bei der Suchmaschine vor. Gerade die gro&szlig;en "
        + "Suchmaschinen sind oft sehr schwerf&auml;llig, Druck von der Basis hilft da meist " + "weiter.</p>\n"
        + "<p style=\"text-align:right\"><a href=\"#inhalt\">Zur&uuml;ck</a></p>\n";
  }

  /**
   * The Method <code>preventFromCache</code> tells you if this page should be cached or not.
   *
   * @param session Data of the current session
   * @return don't cache this site = true
   */
  @Override
  public boolean preventFromCache(final HttpSession session) {
    return false;
  }

  /**
   * The Method <code>allowedToSee</code> tells you if the currently logged in user is allowed to access this application.
   *
   * @param session Data of the current session
   * @return true if it is allowed for this user
   */
  @Override
  public boolean allowedToSee(final HttpSession session) {
    return true;
  }

  /**
   * The Method <code>allowedToChange</code> tells you if the currently logged in user is allowed to change data in this
   * application.
   *
   * @param session Data of the current session
   * @return true if it is allowed for this user
   */
  @Override
  public boolean allowedToChange(final HttpSession session) {
    return true;
  }

}
