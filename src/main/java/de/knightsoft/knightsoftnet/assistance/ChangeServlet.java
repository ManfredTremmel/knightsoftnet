/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.knightsoftnet.assistance;

import de.knightsoft.common.Constants;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

public class ChangeServlet extends de.knightsoft.knightsoftnet.common.AbstractChangeServlet {

  private static final long serialVersionUID = -8694839528766762065L;

  /**
   * init-Methode.
   */
  @Override
  public void init(final ServletConfig config) throws ServletException {
    service = "Nachhilfe";
    servlet = "KnightSoftNachhilfePflege";
    servletReg = "KnightSoftNachhilfeInsert";
    wbild = de.knightsoft.knightsoftnet.common.KnConst.WBILD_NACH_CHANGE;
    wurl = de.knightsoft.knightsoftnet.common.KnConst.WURL_NACH_CHANGE;
    wbildReg = de.knightsoft.knightsoftnet.common.KnConst.WBILD_NACH_REG;
    wurlReg = de.knightsoft.knightsoftnet.common.KnConst.WURL_NACH_REG;
    dbTabelle = "KnightSoft_Nachhilfe";
    loginCount = "NHLoginCounter";
    loginDbNummer = "NHLoginDBNummer";
    emailAb = de.knightsoft.knightsoftnet.common.KnConst.EMAIL_NH;
    logoTextFarbe = de.knightsoft.knightsoftnet.common.KnConst.NACHHILFE_FARBE;
    logoText = de.knightsoft.knightsoftnet.common.KnConst.NACHHILFE_TEXT;
    thisNav = de.knightsoft.knightsoftnet.assistance.Navigation.NACHHILFE_NAV;
    homePage = de.knightsoft.knightsoftnet.common.KnConst.HTML_BASE + "Nachhilfe/index.html";
    passwortStart = "KSNH";
    homeUrl = de.knightsoft.knightsoftnet.common.KnConst.HTML_NH_URL;
    super.init(config);
  }

  /**
   * Ändern-Seite ausgeben.
   */
  @Override
  protected void changeWindow(final HttpServletRequest req, final HttpServletResponse res, final ServletOutputStream out,
      final long cookieDbNr, final String hinweis) throws IOException, de.knightsoft.common.TextException {
    de.knightsoft.knightsoftnet.assistance.AssistCheck nachhilfeDaten = null;
    try {
      nachhilfeDaten = new de.knightsoft.knightsoftnet.assistance.AssistCheck(leseDbSatz(cookieDbNr));
    } catch (final java.sql.SQLException e) {
      throw new de.knightsoft.common.TextException("Fehler beim Datenbankzugriff: " + e.toString(), e);
    }

    final String htmlSeite = this.aendernHtml(req, res, de.knightsoft.knightsoftnet.common.KnConst.JAVA_SCRIPT_NH_URL, hinweis,
        nachhilfeDaten, servlet);
    out.println(htmlSeite);
  }

  @Override
  protected String changeHtml(final HttpServletRequest req, final HttpServletResponse res, final String hinweis)
      throws de.knightsoft.common.TextException {
    final de.knightsoft.knightsoftnet.assistance.AssistCheck aCheck = new de.knightsoft.knightsoftnet.assistance.AssistCheck();
    return this.aendernHtml(req, res, de.knightsoft.knightsoftnet.common.KnConst.JAVA_SCRIPT_NH_URL, hinweis, aCheck,
        servletReg);
  }

  /**
   * change html.
   *
   * @param req HttpServletRequest
   * @param res HttpServletResponse
   * @param javaScriptServiceUrl java script service url
   * @param hinweis hint
   * @param passistCheck check data class
   * @param servletName servlet name
   * @return html snippet
   */
  public String aendernHtml(final HttpServletRequest req, final HttpServletResponse res, final String javaScriptServiceUrl,
      final String hinweis, final AssistCheck passistCheck, final String servletName)
      throws de.knightsoft.common.TextException {
    final StringBuilder sb = new StringBuilder(16384);
    sb.append("                <tr>\n" + "                    <td style=\"text-align:left;\">Vorwahl des Ortsbereiches:<br>"
        + "<div class=\"small\">(dient zur Suche, bitte keine Mobil-Vorwahl angeben)</div></td>\n"
        + "                    <td style=\"text-align:left;\"><input type=\"text\" "
        + "name=\"Vorwahl\" size=\"6\" maxlength=\"6\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.vorwahl) + "\" required=\"required\"></td>\n"
        + "                </tr>\n" + "                <tr>\n"
        + "                    <td style=\"text-align:left;\">Geburtsjahr:</td>\n"
        + "                    <td style=\"text-align:left;\"><input type=\"number\" "
        + "name=\"Geburtsjahr\" min=\"1900\" max=\"3000\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.geburtsjahr) + "\" required=\"required\"></td>\n"
        + "                </tr>\n" + "                <tr>\n" + "                    <td style=\"text-align:left;\" colspan=2>"
        + "Ich gebe Nachhilfe in folgenden F&auml;chern:</td>\n" + "                </tr>\n" + "                <tr>\n"
        + "                    <td colspan=\"2\">\n" + "                        <table border=\"0\">\n"
        + "                            <tr>\n" + "                                <td>&nbsp;</td>\n"
        + "                                <td>&nbsp;</td>\n"
        + "                                <td style=\"text-align:left;\">Jahrgangsstufe:</td>\n"
        + "                                <td style=\"text-align:left;\">Semester:</td>\n"
        + "                            </tr>\n" + "                            <tr>\n"
        + "                                <td>&nbsp;</td>\n");
    if ("J".equals(passistCheck.biologie)) {
      sb.append("                                <td style=\"text-align:left;\">"
          + "<input type=\"Checkbox\" name=\"Biologie\" checked>Biologie</td>\n");
    } else {
      sb.append("                                <td style=\"text-align:left;\">"
          + "<input type=\"Checkbox\" name=\"Biologie\">Biologie</td>\n");
    }
    sb.append("                                <td style=\"text-align:left;\">&nbsp;&nbsp;"
        + "<input type=\"number\" name=\"Biologie_J_von\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.biologieJvon)
        + "\">-<input type=\"number\" name=\"Biologie_J_bis\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.biologieJbis) + "\"></td>\n"
        + "                                <td style=\"text-align:left;\">"
        + "<input type=\"number\" name=\"Biologie_S_von\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.biologieSvon)
        + "\">-<input type=\"number\" name=\"Biologie_S_bis\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.biologieSbis) + "\"></td>\n"
        + "                            </tr>\n" + "                            <tr>\n"
        + "                                <td>&nbsp;</td>\n");
    if ("J".equals(passistCheck.bwl)) {
      sb.append("                                <td style=\"text-align:left;\">"
          + "<input type=\"Checkbox\" name=\"BWL\" checked>BWL</td>\n");
    } else {
      sb.append("                                <td style=\"text-align:left;\">"
          + "<input type=\"Checkbox\" name=\"BWL\">BWL</td>\n");
    }
    sb.append("                                <td style=\"text-align:left;\">&nbsp;&nbsp;"
        + "<input type=\"number\" name=\"BWL_J_von\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.bwlJvon)
        + "\">-<input type=\"number\" name=\"BWL_J_bis\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.bwlJbis) + "\"></td>\n"
        + "                                <td style=\"text-align:left;\">"
        + "<input type=\"number\" name=\"BWL_S_von\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.bwlSvon)
        + "\">-<input type=\"number\" name=\"BWL_S_bis\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.bwlSbis) + "\"></td>\n" + "                            </tr>\n"
        + "                            <tr>\n" + "                                <td>&nbsp;</td>\n");
    if ("J".equals(passistCheck.chemie)) {
      sb.append("                                <td style=\"text-align:left;\">"
          + "<input type=\"Checkbox\" name=\"Chemie\" checked>Chemie</td>\n");
    } else {
      sb.append("                                <td style=\"text-align:left;\">"
          + "<input type=\"Checkbox\" name=\"Chemie\">Chemie</td>\n");
    }
    sb.append("                                <td style=\"text-align:left;\">&nbsp;&nbsp;"
        + "<input type=\"number\" name=\"Chemie_J_von\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.chemieJvon)
        + "\">-<input type=\"number\" name=\"Chemie_J_bis\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.chemieJbis) + "\"></td>\n"
        + "                                <td style=\"text-align:left;\">"
        + "<input type=\"number\" name=\"Chemie_S_von\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.chemieSvon)
        + "\">-<input type=\"number\" name=\"Chemie_S_bis\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.chemieSbis) + "\"></td>\n"
        + "                            </tr>\n" + "                            <tr>\n"
        + "                                <td>&nbsp;</td>\n");
    if ("J".equals(passistCheck.deutsch)) {
      sb.append("                                <td style=\"text-align:left;\">"
          + "<input type=\"Checkbox\" name=\"Deutsch\" checked>Deutsch</td>\n");
    } else {
      sb.append("                                <td style=\"text-align:left;\">"
          + "<input type=\"Checkbox\" name=\"Deutsch\">Deutsch</td>\n");
    }
    sb.append("                                <td style=\"text-align:left;\">&nbsp;&nbsp;"
        + "<input type=\"number\" name=\"Deutsch_J_von\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.deutschJvon)
        + "\">-<input type=\"number\" name=\"Deutsch_J_bis\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.deutschJbis) + "\"></td>\n"
        + "                                <td style=\"text-align:left;\">"
        + "<input type=\"number\" name=\"Deutsch_S_von\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.deutschSvon)
        + "\">-<input type=\"number\" name=\"Deutsch_S_bis\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.deutschSbis) + "\"></td>\n"
        + "                            </tr>\n");

    sb.append("                            <tr>\n" + "                                <td>&nbsp;</td>\n");
    if ("J".equals(passistCheck.englisch)) {
      sb.append("                                <td style=\"text-align:left;\">"
          + "<input type=\"Checkbox\" name=\"Englisch\" checked>Englisch</td>\n");
    } else {
      sb.append("                                <td style=\"text-align:left;\">"
          + "<input type=\"Checkbox\" name=\"Englisch\">Englisch</td>\n");
    }
    sb.append("                                <td style=\"text-align:left;\">&nbsp;&nbsp;"
        + "<input type=\"number\" name=\"Englisch_J_von\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.englischJvon)
        + "\">-<input type=\"number\" name=\"Englisch_J_bis\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.englischJbis) + "\"></td>\n"
        + "                                <td style=\"text-align:left;\">"
        + "<input type=\"number\" name=\"Englisch_S_von\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.englischSvon)
        + "\">-<input type=\"number\" name=\"Englisch_S_bis\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.englischSbis) + "\"></td>\n"
        + "                            </tr>\n" + "                            <tr>\n"
        + "                                <td>&nbsp;</td>\n");
    if ("J".equals(passistCheck.erdkunde)) {
      sb.append("                                <td style=\"text-align:left;\">"
          + "<input type=\"Checkbox\" name=\"Erdkunde\" checked>Erdkunde</td>\n");
    } else {
      sb.append("                                <td style=\"text-align:left;\">"
          + "<input type=\"Checkbox\" name=\"Erdkunde\">Erdkunde</td>\n");
    }
    sb.append("                                <td style=\"text-align:left;\">&nbsp;&nbsp;"
        + "<input type=\"number\" name=\"Erdkunde_J_von\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.erdkundeJvon)
        + "\">-<input type=\"number\" name=\"Erdkunde_J_bis\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.erdkundeJbis) + "\"></td>\n"
        + "                                <td style=\"text-align:left;\">"
        + "<input type=\"number\" name=\"Erdkunde_S_von\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.erdkundeSvon)
        + "\">-<input type=\"number\" name=\"Erdkunde_S_bis\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.erdkundeSbis) + "\"></td>\n"
        + "                            </tr>\n" + "                            <tr>\n"
        + "                                <td>&nbsp;</td>\n");
    if ("J".equals(passistCheck.franzoesisch)) {
      sb.append("                                <td style=\"text-align:left;\">"
          + "<input type=\"Checkbox\" name=\"Franzoesisch\" checked>Franz&ouml;sich</td>\n");
    } else {
      sb.append("                                <td style=\"text-align:left;\">"
          + "<input type=\"Checkbox\" name=\"Franzoesisch\">Franz&ouml;sich</td>\n");
    }
    sb.append("                                <td style=\"text-align:left;\">&nbsp;&nbsp;"
        + "<input type=\"number\" name=\"Franzoesisch_J_von\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.franzoesischJvon)
        + "\">-<input type=\"number\" name=\"Franzoesisch_J_bis\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.franzoesischJbis) + "\"></td>\n"
        + "                                <td style=\"text-align:left;\">"
        + "<input type=\"number\" name=\"Franzoesisch_S_von\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.franzoesischSvon)
        + "\">-<input type=\"number\" name=\"Franzoesisch_S_bis\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.franzoesischSbis) + "\"></td>\n"
        + "                            </tr>\n" + "                            <tr>\n"
        + "                                <td>&nbsp;</td>\n");
    if ("J".equals(passistCheck.geschichte)) {
      sb.append("                                <td style=\"text-align:left;\">"
          + "<input type=\"Checkbox\" name=\"Geschichte\" checked>Geschichte</td>\n");
    } else {
      sb.append("                                <td style=\"text-align:left;\">"
          + "<input type=\"Checkbox\" name=\"Geschichte\">Geschichte</td>\n");
    }
    sb.append("                                <td style=\"text-align:left;\">&nbsp;&nbsp;"
        + "<input type=\"number\" name=\"Geschichte_J_von\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.geschichteJvon)
        + "\">-<input type=\"number\" name=\"Geschichte_J_bis\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.geschichteJbis) + "\"></td>\n"
        + "                                <td style=\"text-align:left;\">"
        + "<input type=\"number\" name=\"Geschichte_S_von\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.geschichteSvon)
        + "\">-<input type=\"number\" name=\"Geschichte_S_bis\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.geschichteSbis) + "\"></td>\n"
        + "                            </tr>\n" + "                            <tr>\n"
        + "                                <td>&nbsp;</td>\n");
    if ("J".equals(passistCheck.griechisch)) {
      sb.append("                                <td style=\"text-align:left;\">"
          + "<input type=\"Checkbox\" name=\"Griechisch\" checked>Griechisch</td>\n");
    } else {
      sb.append("                                <td style=\"text-align:left;\">"
          + "<input type=\"Checkbox\" name=\"Griechisch\">Griechisch</td>\n");
    }
    sb.append("                                <td style=\"text-align:left;\">&nbsp;&nbsp;"
        + "<input type=\"number\" name=\"Griechisch_J_von\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.griechischJvon)
        + "\">-<input type=\"number\" name=\"Griechisch_J_bis\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.griechischJbis) + "\"></td>\n"
        + "                                <td style=\"text-align:left;\">"
        + "<input type=\"number\" name=\"Griechisch_S_von\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.griechischSvon)
        + "\">-<input type=\"number\" name=\"Griechisch_S_bis\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.griechischSbis) + "\"></td>\n"
        + "                            </tr>\n" + "                            <tr>\n"
        + "                                <td>&nbsp;</td>\n");
    if ("J".equals(passistCheck.informatik)) {
      sb.append("                                <td style=\"text-align:left;\">"
          + "<input type=\"Checkbox\" name=\"Informatik\" checked>Informatik</td>\n");
    } else {
      sb.append("                                <td style=\"text-align:left;\">"
          + "<input type=\"Checkbox\" name=\"Informatik\">Informatik</td>\n");
    }
    sb.append("                                <td style=\"text-align:left;\">&nbsp;&nbsp;"
        + "<input type=\"number\" name=\"Informatik_J_von\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.informatikJvon)
        + "\">-<input type=\"number\" name=\"Informatik_J_bis\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.informatikJbis) + "\"></td>\n"
        + "                                <td style=\"text-align:left;\">"
        + "<input type=\"number\" name=\"Informatik_S_von\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.informatikSvon)
        + "\">-<input type=\"number\" name=\"Informatik_S_bis\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.informatikSbis) + "\"></td>\n"
        + "                            </tr>\n" + "                            <tr>\n"
        + "                                <td>&nbsp;</td>\n");
    if ("J".equals(passistCheck.latein)) {
      sb.append("                                <td style=\"text-align:left;\">"
          + "<input type=\"Checkbox\" name=\"Latein\" checked>Latein</td>\n");
    } else {
      sb.append("                                <td style=\"text-align:left;\">"
          + "<input type=\"Checkbox\" name=\"Latein\">Latein</td>\n");
    }
    sb.append("                                <td style=\"text-align:left;\">&nbsp;&nbsp;"
        + "<input type=\"number\" name=\"Latein_J_von\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.lateinJvon)
        + "\">-<input type=\"number\" name=\"Latein_J_bis\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.lateinJbis) + "\"></td>\n"
        + "                                <td style=\"text-align:left;\">"
        + "<input type=\"number\" name=\"Latein_S_von\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.lateinSvon)
        + "\">-<input type=\"number\" name=\"Latein_S_bis\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.lateinSbis) + "\"></td>\n"
        + "                            </tr>\n" + "                            <tr>\n"
        + "                                <td>&nbsp;</td>\n");
    if ("J".equals(passistCheck.maschineschreiben)) {
      sb.append("                                <td style=\"text-align:left;\"><input "
          + "type=\"Checkbox\" name=\"Maschineschreiben\" checked>Maschineschreiben</td>\n");
    } else {
      sb.append("                                <td style=\"text-align:left;\">"
          + "<input type=\"Checkbox\" name=\"Maschineschreiben\">Maschineschreiben</td>\n");
    }
    sb.append("                                <td style=\"text-align:left;\">&nbsp;&nbsp;"
        + "<input type=\"number\" name=\"Maschineschreiben_J_von\" min=\"1\" max=\"13\" " + "value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.maschineschreibenJvon)
        + "\">-<input type=\"number\" name=\"Maschineschreiben_J_bis\" min=\"1\" max=\"13\"" + " value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.maschineschreibenJbis) + "\"></td>\n"
        + "                                <td style=\"text-align:left;\"><input type=\"number\" "
        + "name=\"Maschineschreiben_S_von\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.maschineschreibenSvon)
        + "\">-<input type=\"number\" name=\"Maschineschreiben_S_bis\" min=\"1\" max=\"13\" " + "value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.maschineschreibenSbis) + "\"></td>\n"
        + "                            </tr>\n" + "                            <tr>\n"
        + "                                <td>&nbsp;</td>\n");
    if ("J".equals(passistCheck.mathematik)) {
      sb.append("                                <td style=\"text-align:left;\">"
          + "<input type=\"Checkbox\" name=\"Mathematik\" checked>Mathematik</td>\n");
    } else {
      sb.append("                                <td style=\"text-align:left;\">"
          + "<input type=\"Checkbox\" name=\"Mathematik\">Mathematik</td>\n");
    }
    sb.append("                                <td style=\"text-align:left;\">&nbsp;&nbsp;"
        + "<input type=\"number\" name=\"Mathematik_J_von\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.mathematikJvon)
        + "\">-<input type=\"number\" name=\"Mathematik_J_bis\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.mathematikJbis) + "\"></td>\n"
        + "                                <td style=\"text-align:left;\">"
        + "<input type=\"number\" name=\"Mathematik_S_von\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.mathematikSvon)
        + "\">-<input type=\"number\" name=\"Mathematik_S_bis\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.mathematikSbis) + "\"></td>\n"
        + "                            </tr>\n" + "                            <tr>\n"
        + "                                <td>&nbsp;</td>\n");
    if ("J".equals(passistCheck.physik)) {
      sb.append("                                <td style=\"text-align:left;\">"
          + "<input type=\"Checkbox\" name=\"Physik\" checked>Physik</td>\n");
    } else {
      sb.append("                                <td style=\"text-align:left;\">"
          + "<input type=\"Checkbox\" name=\"Physik\">Physik</td>\n");
    }
    sb.append("                                <td style=\"text-align:left;\">&nbsp;&nbsp;"
        + "<input type=\"number\" name=\"Physik_J_von\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.physikJvon)
        + "\">-<input type=\"number\" name=\"Physik_J_bis\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.physikJbis) + "\"></td>\n"
        + "                                <td style=\"text-align:left;\"><input type=\"number\" "
        + "name=\"Physik_S_von\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.physikSvon)
        + "\">-<input type=\"number\" name=\"Physik_S_bis\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.physikSbis) + "\"></td>\n"
        + "                            </tr>\n" + "                            <tr>\n"
        + "                                <td>&nbsp;</td>\n");
    if ("J".equals(passistCheck.sozialkunde)) {
      sb.append("                                <td style=\"text-align:left;\">"
          + "<input type=\"Checkbox\" name=\"Sozialkunde\" checked>Sozialkunde</td>\n");
    } else {
      sb.append("                                <td style=\"text-align:left;\">"
          + "<input type=\"Checkbox\" name=\"Sozialkunde\">Sozialkunde</td>\n");
    }
    sb.append("                                <td style=\"text-align:left;\">&nbsp;&nbsp;"
        + "<input type=\"number\" name=\"Sozialkunde_J_von\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.sozialkundeJvon)
        + "\">-<input type=\"number\" name=\"Sozialkunde_J_bis\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.sozialkundeJbis) + "\"></td>\n"
        + "                                <td style=\"text-align:left;\">"
        + "<input type=\"number\" name=\"Sozialkunde_S_von\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.sozialkundeSvon)
        + "\">-<input type=\"number\" name=\"Sozialkunde_S_bis\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.sozialkundeSbis) + "\"></td>\n"
        + "                            </tr>\n" + "                            <tr>\n"
        + "                                <td>&nbsp;</td>\n");
    if ("J".equals(passistCheck.stenografie)) {
      sb.append("                                <td style=\"text-align:left;\">"
          + "<input type=\"Checkbox\" name=\"Stenografie\" checked>Stenografie</td>\n");
    } else {
      sb.append("                                <td style=\"text-align:left;\">"
          + "<input type=\"Checkbox\" name=\"Stenografie\">Stenografie</td>\n");
    }
    sb.append("                                <td style=\"text-align:left;\">&nbsp;&nbsp;"
        + "<input type=\"number\" name=\"Stenografie_J_von\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.stenografieJvon)
        + "\">-<input type=\"number\" name=\"Stenografie_J_bis\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.stenografieJbis) + "\"></td>\n"
        + "                                <td style=\"text-align:left;\">"
        + "<input type=\"number\" name=\"Stenografie_S_von\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.stenografieSvon)
        + "\">-<input type=\"number\" name=\"Stenografie_S_bis\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.stenografieSbis) + "\"></td>\n"
        + "                            </tr>\n" + "                            <tr>\n"
        + "                                <td>&nbsp;</td>\n"
        + "                                <td style=\"text-align:center;\" colspan=\"3\">"
        + "Weitere F&auml;cher, die oben nicht aufgef&uuml;hrt wurden<br><div class=\"small\">"
        + "(Fachbezeichnung sowie Jahrg&auml;nge bzw. Semester angeben)</div></td>\n" + "                            </tr>\n"
        + "                            <tr>\n" + "                                <td>&nbsp;</td>\n"
        + "                                <td style=\"text-align:left;\"><input type=\"number\" "
        + "name=\"weiteres_fach1\" size=\"25\" maxlength=\"30\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.weiteresFach1) + "\"></td>\n"
        + "                                <td style=\"text-align:left;\">&nbsp;&nbsp;"
        + "<input type=\"number\" name=\"weiteres_fach1_J_von\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.weiteresFach1Jvon)
        + "\">-<input type=\"number\" name=\"weiteres_fach1_J_bis\" min=\"1\" max=\"13\" " + "value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.weiteresFach1Jbis) + "\"></td>\n"
        + "                                <td style=\"text-align:left;\"><input type=\"number\" "
        + "name=\"weiteres_fach1_S_von\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.weiteresFach1Svon)
        + "\">-<input type=\"number\" name=\"weiteres_fach1_S_bis\" min=\"1\" max=\"13\" " + "value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.weiteresFach1Sbis) + "\"></td>\n"
        + "                            </tr>\n" + "                            <tr>\n"
        + "                                <td>&nbsp;</td>\n"
        + "                                <td style=\"text-align:left;\"><input type=\"number\" "
        + "name=\"weiteres_fach2\" size=\"25\" maxlength=\"30\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.weiteresFach2) + "\"></td>\n"
        + "                                <td style=\"text-align:left;\">&nbsp;&nbsp;"
        + "<input type=\"number\" name=\"weiteres_fach2_J_von\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.weiteresFach2Jvon)
        + "\">-<input type=\"number\" name=\"weiteres_fach2_J_bis\" min=\"1\" max=\"13\" " + "value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.weiteresFach2Jbis) + "\"></td>\n"
        + "                                <td style=\"text-align:left;\"><input type=\"number\" "
        + "name=\"weiteres_fach2_S_von\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.weiteresFach2Svon)
        + "\">-<input type=\"number\" name=\"weiteres_fach2_S_bis\" min=\"1\" max=\"13\" " + "value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.weiteresFach2Sbis) + "\"></td>\n"
        + "                            </tr>\n" + "                            <tr>\n"
        + "                                <td>&nbsp;</td>\n"
        + "                                <td style=\"text-align:left;\"><input type=\"number\" "
        + "name=\"weiteres_fach3\" size=\"25\" maxlength=\"30\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.weiteresFach3) + "\"></td>\n"
        + "                                <td style=\"text-align:left;\">&nbsp;&nbsp;"
        + "<input type=\"number\" name=\"weiteres_fach3_J_von\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.weiteresFach3Jvon)
        + "\">-<input type=\"number\" name=\"weiteres_fach3_J_bis\" min=\"1\" max=\"13\" " + "value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.weiteresFach3Jbis) + "\"></td>\n"
        + "                                <td style=\"text-align:left;\"><input type=\"number\" "
        + "name=\"weiteres_fach3_S_von\" min=\"1\" max=\"13\" value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.weiteresFach3Svon)
        + "\">-<input type=\"number\" name=\"weiteres_fach3_S_bis\" min=\"1\" max=\"13\" " + "value=\""
        + de.knightsoft.common.StringToHtml.convert(passistCheck.weiteresFach3Sbis) + "\"></td>\n"
        + "                            </tr>\n" + "                        </table>\n" + "                    </td>\n"
        + "                </tr>\n" + "                <tr>\n"
        + "                    <td style=\"text-align:left; color:gray; vertical-align:top;\">"
        + " &sup1;) Preisvorstellung:</td>\n" + "                    <td style=\"text-align:left;\"><textarea name=\"Preis\" "
        + "rows=3 cols=30>" + de.knightsoft.common.StringToHtml.convert(passistCheck.preis, false, false) + "</textarea></td>\n"
        + "                </tr>\n");

    return this.aendernHtml(req, res, javaScriptServiceUrl, hinweis, passistCheck, sb.toString(), servletName);
  }

  /**
   * Ändern des ausgewählten Datensatzes.
   */
  @Override
  protected long verarbeitungAendern(final HttpServletRequest req, final HttpServletResponse res, final ServletOutputStream out)
      throws de.knightsoft.common.TextException, IOException {
    final long cookieDbNr = super.verarbeitungAendern(req, res, out);

    // Eingaben einlesen
    final de.knightsoft.knightsoftnet.assistance.AssistCheck nachhilfeDaten =
        new de.knightsoft.knightsoftnet.assistance.AssistCheck(req);

    // Die eingegebenen Daten prüfen
    nachhilfeDaten.checkDaten();

    upDateDbSatz(req, res, out, nachhilfeDaten.prepareUpdate(), cookieDbNr);

    return cookieDbNr;
  }

  @Override
  public String getServletInfo() {
    return "Change/update information about assistance teachers in database\n\n" + Constants.COPYRIGHT;
  }
}
