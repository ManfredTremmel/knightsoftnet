/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.knightsoftnet.assistance;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AssistCheck extends de.knightsoft.knightsoftnet.common.Check {
  String vorwahl;
  String geburtsjahr;

  String biologie;
  String biologieJvon;
  String biologieJbis;
  String biologieSvon;
  String biologieSbis;

  String bwl;
  String bwlJvon;
  String bwlJbis;
  String bwlSvon;
  String bwlSbis;

  String chemie;
  String chemieJvon;
  String chemieJbis;
  String chemieSvon;
  String chemieSbis;

  String deutsch;
  String deutschJvon;
  String deutschJbis;
  String deutschSvon;
  String deutschSbis;

  String englisch;
  String englischJvon;
  String englischJbis;
  String englischSvon;
  String englischSbis;

  String erdkunde;
  String erdkundeJvon;
  String erdkundeJbis;
  String erdkundeSvon;
  String erdkundeSbis;

  String franzoesisch;
  String franzoesischJvon;
  String franzoesischJbis;
  String franzoesischSvon;
  String franzoesischSbis;

  String geschichte;
  String geschichteJvon;
  String geschichteJbis;
  String geschichteSvon;
  String geschichteSbis;

  String griechisch;
  String griechischJvon;
  String griechischJbis;
  String griechischSvon;
  String griechischSbis;

  String informatik;
  String informatikJvon;
  String informatikJbis;
  String informatikSvon;
  String informatikSbis;

  String latein;
  String lateinJvon;
  String lateinJbis;
  String lateinSvon;
  String lateinSbis;

  String maschineschreiben;
  String maschineschreibenJvon;
  String maschineschreibenJbis;
  String maschineschreibenSvon;
  String maschineschreibenSbis;

  String mathematik;
  String mathematikJvon;
  String mathematikJbis;
  String mathematikSvon;
  String mathematikSbis;

  String physik;
  String physikJvon;
  String physikJbis;
  String physikSvon;
  String physikSbis;

  String sozialkunde;
  String sozialkundeJvon;
  String sozialkundeJbis;
  String sozialkundeSvon;
  String sozialkundeSbis;

  String stenografie;
  String stenografieJvon;
  String stenografieJbis;
  String stenografieSvon;
  String stenografieSbis;

  String weiteresFach1;
  String weiteresFach1Jvon;
  String weiteresFach1Jbis;
  String weiteresFach1Svon;
  String weiteresFach1Sbis;

  String weiteresFach2;
  String weiteresFach2Jvon;
  String weiteresFach2Jbis;
  String weiteresFach2Svon;
  String weiteresFach2Sbis;

  String weiteresFach3;
  String weiteresFach3Jvon;
  String weiteresFach3Jbis;
  String weiteresFach3Svon;
  String weiteresFach3Sbis;

  String preis;

  /**
   * Konstruktor aus dem Bild auslesen.
   */
  public AssistCheck() {

    super();

    vorwahl = StringUtils.EMPTY;
    geburtsjahr = StringUtils.EMPTY;

    biologie = "N";
    biologieJvon = StringUtils.EMPTY;
    biologieJbis = StringUtils.EMPTY;
    biologieSvon = StringUtils.EMPTY;
    biologieSbis = StringUtils.EMPTY;

    bwl = "N";
    bwlJvon = StringUtils.EMPTY;
    bwlJbis = StringUtils.EMPTY;
    bwlSvon = StringUtils.EMPTY;
    bwlSbis = StringUtils.EMPTY;

    chemie = "N";
    chemieJvon = StringUtils.EMPTY;
    chemieJbis = StringUtils.EMPTY;
    chemieSvon = StringUtils.EMPTY;
    chemieSbis = StringUtils.EMPTY;

    deutsch = "N";
    deutschJvon = StringUtils.EMPTY;
    deutschJbis = StringUtils.EMPTY;
    deutschSvon = StringUtils.EMPTY;
    deutschSbis = StringUtils.EMPTY;

    englisch = "N";
    englischJvon = StringUtils.EMPTY;
    englischJbis = StringUtils.EMPTY;
    englischSvon = StringUtils.EMPTY;
    englischSbis = StringUtils.EMPTY;

    erdkunde = "N";
    erdkundeJvon = StringUtils.EMPTY;
    erdkundeJbis = StringUtils.EMPTY;
    erdkundeSvon = StringUtils.EMPTY;
    erdkundeSbis = StringUtils.EMPTY;

    franzoesisch = "N";
    franzoesischJvon = StringUtils.EMPTY;
    franzoesischJbis = StringUtils.EMPTY;
    franzoesischSvon = StringUtils.EMPTY;
    franzoesischSbis = StringUtils.EMPTY;

    geschichte = "N";
    geschichteJvon = StringUtils.EMPTY;
    geschichteJbis = StringUtils.EMPTY;
    geschichteSvon = StringUtils.EMPTY;
    geschichteSbis = StringUtils.EMPTY;

    griechisch = "N";
    griechischJvon = StringUtils.EMPTY;
    griechischJbis = StringUtils.EMPTY;
    griechischSvon = StringUtils.EMPTY;
    griechischSbis = StringUtils.EMPTY;

    informatik = "N";
    informatikJvon = StringUtils.EMPTY;
    informatikJbis = StringUtils.EMPTY;
    informatikSvon = StringUtils.EMPTY;
    informatikSbis = StringUtils.EMPTY;

    latein = "N";
    lateinJvon = StringUtils.EMPTY;
    lateinJbis = StringUtils.EMPTY;
    lateinSvon = StringUtils.EMPTY;
    lateinSbis = StringUtils.EMPTY;

    maschineschreiben = "N";
    maschineschreibenJvon = StringUtils.EMPTY;
    maschineschreibenJbis = StringUtils.EMPTY;
    maschineschreibenSvon = StringUtils.EMPTY;
    maschineschreibenSbis = StringUtils.EMPTY;

    mathematik = "N";
    mathematikJvon = StringUtils.EMPTY;
    mathematikJbis = StringUtils.EMPTY;
    mathematikSvon = StringUtils.EMPTY;
    mathematikSbis = StringUtils.EMPTY;

    physik = "N";
    physikJvon = StringUtils.EMPTY;
    physikJbis = StringUtils.EMPTY;
    physikSvon = StringUtils.EMPTY;
    physikSbis = StringUtils.EMPTY;

    sozialkunde = "N";
    sozialkundeJvon = StringUtils.EMPTY;
    sozialkundeJbis = StringUtils.EMPTY;
    sozialkundeSvon = StringUtils.EMPTY;
    sozialkundeSbis = StringUtils.EMPTY;

    stenografie = "N";
    stenografieJvon = StringUtils.EMPTY;
    stenografieJbis = StringUtils.EMPTY;
    stenografieSvon = StringUtils.EMPTY;
    stenografieSbis = StringUtils.EMPTY;

    weiteresFach1 = StringUtils.EMPTY;
    weiteresFach1Jvon = StringUtils.EMPTY;
    weiteresFach1Jbis = StringUtils.EMPTY;
    weiteresFach1Svon = StringUtils.EMPTY;
    weiteresFach1Sbis = StringUtils.EMPTY;

    weiteresFach2 = StringUtils.EMPTY;
    weiteresFach2Jvon = StringUtils.EMPTY;
    weiteresFach2Jbis = StringUtils.EMPTY;
    weiteresFach2Svon = StringUtils.EMPTY;
    weiteresFach2Sbis = StringUtils.EMPTY;

    weiteresFach3 = StringUtils.EMPTY;
    weiteresFach3Jvon = StringUtils.EMPTY;
    weiteresFach3Jbis = StringUtils.EMPTY;
    weiteresFach3Svon = StringUtils.EMPTY;
    weiteresFach3Sbis = StringUtils.EMPTY;

    preis = StringUtils.EMPTY;
  }

  /**
   * Konstruktor aus dem Bild auslesen.
   *
   * @param req HttpServletRequest
   */
  public AssistCheck(final HttpServletRequest req) {

    super(req);

    vorwahl = req.getParameter("Vorwahl");
    geburtsjahr = req.getParameter("Geburtsjahr");

    biologie = req.getParameter("Biologie");
    biologieJvon = req.getParameter("Biologie_J_von");
    biologieJbis = req.getParameter("Biologie_J_bis");
    biologieSvon = req.getParameter("Biologie_S_von");
    biologieSbis = req.getParameter("Biologie_S_bis");

    bwl = req.getParameter("BWL");
    bwlJvon = req.getParameter("BWL_J_von");
    bwlJbis = req.getParameter("BWL_J_bis");
    bwlSvon = req.getParameter("BWL_S_von");
    bwlSbis = req.getParameter("BWL_S_bis");

    chemie = req.getParameter("Chemie");
    chemieJvon = req.getParameter("Chemie_J_von");
    chemieJbis = req.getParameter("Chemie_J_bis");
    chemieSvon = req.getParameter("Chemie_S_von");
    chemieSbis = req.getParameter("Chemie_S_bis");

    deutsch = req.getParameter("Deutsch");
    deutschJvon = req.getParameter("Deutsch_J_von");
    deutschJbis = req.getParameter("Deutsch_J_bis");
    deutschSvon = req.getParameter("Deutsch_S_von");
    deutschSbis = req.getParameter("Deutsch_S_bis");

    englisch = req.getParameter("Englisch");
    englischJvon = req.getParameter("Englisch_J_von");
    englischJbis = req.getParameter("Englisch_J_bis");
    englischSvon = req.getParameter("Englisch_S_von");
    englischSbis = req.getParameter("Englisch_S_bis");

    erdkunde = req.getParameter("Erdkunde");
    erdkundeJvon = req.getParameter("Erdkunde_J_von");
    erdkundeJbis = req.getParameter("Erdkunde_J_bis");
    erdkundeSvon = req.getParameter("Erdkunde_S_von");
    erdkundeSbis = req.getParameter("Erdkunde_S_bis");

    franzoesisch = req.getParameter("Franzoesisch");
    franzoesischJvon = req.getParameter("Franzoesisch_J_von");
    franzoesischJbis = req.getParameter("Franzoesisch_J_bis");
    franzoesischSvon = req.getParameter("Franzoesisch_S_von");
    franzoesischSbis = req.getParameter("Franzoesisch_S_bis");

    geschichte = req.getParameter("Geschichte");
    geschichteJvon = req.getParameter("Geschichte_J_von");
    geschichteJbis = req.getParameter("Geschichte_J_bis");
    geschichteSvon = req.getParameter("Geschichte_S_von");
    geschichteSbis = req.getParameter("Geschichte_S_bis");

    griechisch = req.getParameter("Griechisch");
    griechischJvon = req.getParameter("Griechisch_J_von");
    griechischJbis = req.getParameter("Griechisch_J_bis");
    griechischSvon = req.getParameter("Griechisch_S_von");
    griechischSbis = req.getParameter("Griechisch_S_bis");

    informatik = req.getParameter("Informatik");
    informatikJvon = req.getParameter("Informatik_J_von");
    informatikJbis = req.getParameter("Informatik_J_bis");
    informatikSvon = req.getParameter("Informatik_S_von");
    informatikSbis = req.getParameter("Informatik_S_bis");

    latein = req.getParameter("Latein");
    lateinJvon = req.getParameter("Latein_J_von");
    lateinJbis = req.getParameter("Latein_J_bis");
    lateinSvon = req.getParameter("Latein_S_von");
    lateinSbis = req.getParameter("Latein_S_bis");

    maschineschreiben = req.getParameter("Maschineschreiben");
    maschineschreibenJvon = req.getParameter("Maschineschreiben_J_von");
    maschineschreibenJbis = req.getParameter("Maschineschreiben_J_bis");
    maschineschreibenSvon = req.getParameter("Maschineschreiben_S_von");
    maschineschreibenSbis = req.getParameter("Maschineschreiben_S_bis");

    mathematik = req.getParameter("Mathematik");
    mathematikJvon = req.getParameter("Mathematik_J_von");
    mathematikJbis = req.getParameter("Mathematik_J_bis");
    mathematikSvon = req.getParameter("Mathematik_S_von");
    mathematikSbis = req.getParameter("Mathematik_S_bis");

    physik = req.getParameter("Physik");
    physikJvon = req.getParameter("Physik_J_von");
    physikJbis = req.getParameter("Physik_J_bis");
    physikSvon = req.getParameter("Physik_S_von");
    physikSbis = req.getParameter("Physik_S_bis");

    sozialkunde = req.getParameter("Sozialkunde");
    sozialkundeJvon = req.getParameter("Sozialkunde_J_von");
    sozialkundeJbis = req.getParameter("Sozialkunde_J_bis");
    sozialkundeSvon = req.getParameter("Sozialkunde_S_von");
    sozialkundeSbis = req.getParameter("Sozialkunde_S_bis");

    stenografie = req.getParameter("Stenografie");
    stenografieJvon = req.getParameter("Stenografie_J_von");
    stenografieJbis = req.getParameter("Stenografie_J_bis");
    stenografieSvon = req.getParameter("Stenografie_S_von");
    stenografieSbis = req.getParameter("Stenografie_S_bis");

    weiteresFach1 = req.getParameter("weiteres_fach1");
    weiteresFach1Jvon = req.getParameter("weiteres_fach1_J_von");
    weiteresFach1Jbis = req.getParameter("weiteres_fach1_J_bis");
    weiteresFach1Svon = req.getParameter("weiteres_fach1_S_von");
    weiteresFach1Sbis = req.getParameter("weiteres_fach1_S_bis");

    weiteresFach2 = req.getParameter("weiteres_fach2");
    weiteresFach2Jvon = req.getParameter("weiteres_fach2_J_von");
    weiteresFach2Jbis = req.getParameter("weiteres_fach2_J_bis");
    weiteresFach2Svon = req.getParameter("weiteres_fach2_S_von");
    weiteresFach2Sbis = req.getParameter("weiteres_fach2_S_bis");

    weiteresFach3 = req.getParameter("weiteres_fach3");
    weiteresFach3Jvon = req.getParameter("weiteres_fach3_J_von");
    weiteresFach3Jbis = req.getParameter("weiteres_fach3_J_bis");
    weiteresFach3Svon = req.getParameter("weiteres_fach3_S_von");
    weiteresFach3Sbis = req.getParameter("weiteres_fach3_S_bis");

    preis = req.getParameter("Preis");
  }

  /**
   * Konstruktor aus der Datenbank auslesen.
   *
   * @param result ResultSet
   */
  public AssistCheck(final ResultSet result) throws SQLException {

    super(result);

    vorwahl = result.getString("vorwahl");
    geburtsjahr = result.getString("Geburtsjahr");
    if (geburtsjahr.length() == 10) {
      geburtsjahr = geburtsjahr.substring(0, 4);
    }

    biologie = result.getString("biologie");
    biologieJvon = result.getString("biologie_j_von");
    if (biologieJvon == null) {
      biologieJvon = StringUtils.EMPTY;
    }
    biologieJbis = result.getString("biologie_j_bis");
    if (biologieJbis == null) {
      biologieJbis = StringUtils.EMPTY;
    }
    biologieSvon = result.getString("biologie_s_von");
    if (biologieSvon == null) {
      biologieSvon = StringUtils.EMPTY;
    }
    biologieSbis = result.getString("biologie_s_bis");
    if (biologieSbis == null) {
      biologieSbis = StringUtils.EMPTY;
    }

    bwl = result.getString("bwl");
    bwlJvon = result.getString("bwl_j_von");
    if (bwlJvon == null) {
      bwlJvon = StringUtils.EMPTY;
    }
    bwlJbis = result.getString("bwl_j_bis");
    if (bwlJbis == null) {
      bwlJbis = StringUtils.EMPTY;
    }
    bwlSvon = result.getString("bwl_s_von");
    if (bwlSvon == null) {
      bwlSvon = StringUtils.EMPTY;
    }
    bwlSbis = result.getString("bwl_s_bis");
    if (bwlSbis == null) {
      bwlSbis = StringUtils.EMPTY;
    }

    chemie = result.getString("chemie");
    chemieJvon = result.getString("chemie_j_von");
    if (chemieJvon == null) {
      chemieJvon = StringUtils.EMPTY;
    }
    chemieJbis = result.getString("chemie_j_bis");
    if (chemieJbis == null) {
      chemieJbis = StringUtils.EMPTY;
    }
    chemieSvon = result.getString("chemie_s_von");
    if (chemieSvon == null) {
      chemieSvon = StringUtils.EMPTY;
    }
    chemieSbis = result.getString("chemie_s_bis");
    if (chemieSbis == null) {
      chemieSbis = StringUtils.EMPTY;
    }

    deutsch = result.getString("deutsch");
    deutschJvon = result.getString("deutsch_j_von");
    if (deutschJvon == null) {
      deutschJvon = StringUtils.EMPTY;
    }
    deutschJbis = result.getString("deutsch_j_bis");
    if (deutschJbis == null) {
      deutschJbis = StringUtils.EMPTY;
    }
    deutschSvon = result.getString("deutsch_s_von");
    if (deutschSvon == null) {
      deutschSvon = StringUtils.EMPTY;
    }
    deutschSbis = result.getString("deutsch_s_bis");
    if (deutschSbis == null) {
      deutschSbis = StringUtils.EMPTY;
    }

    englisch = result.getString("englisch");
    englischJvon = result.getString("englisch_j_von");
    if (englischJvon == null) {
      englischJvon = StringUtils.EMPTY;
    }
    englischJbis = result.getString("englisch_j_bis");
    if (englischJbis == null) {
      englischJbis = StringUtils.EMPTY;
    }
    englischSvon = result.getString("englisch_s_von");
    if (englischSvon == null) {
      englischSvon = StringUtils.EMPTY;
    }
    englischSbis = result.getString("englisch_s_bis");
    if (englischSbis == null) {
      englischSbis = StringUtils.EMPTY;
    }

    erdkunde = result.getString("erdkunde");
    erdkundeJvon = result.getString("erdkunde_j_von");
    if (erdkundeJvon == null) {
      erdkundeJvon = StringUtils.EMPTY;
    }
    erdkundeJbis = result.getString("erdkunde_j_bis");
    if (erdkundeJbis == null) {
      erdkundeJbis = StringUtils.EMPTY;
    }
    erdkundeSvon = result.getString("erdkunde_s_von");
    if (erdkundeSvon == null) {
      erdkundeSvon = StringUtils.EMPTY;
    }
    erdkundeSbis = result.getString("erdkunde_s_bis");
    if (erdkundeSbis == null) {
      erdkundeSbis = StringUtils.EMPTY;
    }

    franzoesisch = result.getString("franzoesisch");
    franzoesischJvon = result.getString("franzoesisch_j_von");
    if (franzoesischJvon == null) {
      franzoesischJvon = StringUtils.EMPTY;
    }
    franzoesischJbis = result.getString("franzoesisch_j_bis");
    if (franzoesischJbis == null) {
      franzoesischJbis = StringUtils.EMPTY;
    }
    franzoesischSvon = result.getString("franzoesisch_s_von");
    if (franzoesischSvon == null) {
      franzoesischSvon = StringUtils.EMPTY;
    }
    franzoesischSbis = result.getString("franzoesisch_s_bis");
    if (franzoesischSbis == null) {
      franzoesischSbis = StringUtils.EMPTY;
    }

    geschichte = result.getString("geschichte");
    geschichteJvon = result.getString("geschichte_j_von");
    if (geschichteJvon == null) {
      geschichteJvon = StringUtils.EMPTY;
    }
    geschichteJbis = result.getString("geschichte_j_bis");
    if (geschichteJbis == null) {
      geschichteJbis = StringUtils.EMPTY;
    }
    geschichteSvon = result.getString("geschichte_s_von");
    if (geschichteSvon == null) {
      geschichteSvon = StringUtils.EMPTY;
    }
    geschichteSbis = result.getString("geschichte_s_bis");
    if (geschichteSbis == null) {
      geschichteSbis = StringUtils.EMPTY;
    }

    griechisch = result.getString("griechisch");
    griechischJvon = result.getString("griechisch_j_von");
    if (griechischJvon == null) {
      griechischJvon = StringUtils.EMPTY;
    }
    griechischJbis = result.getString("griechisch_j_bis");
    if (griechischJbis == null) {
      griechischJbis = StringUtils.EMPTY;
    }
    griechischSvon = result.getString("griechisch_s_von");
    if (griechischSvon == null) {
      griechischSvon = StringUtils.EMPTY;
    }
    griechischSbis = result.getString("griechisch_s_bis");
    if (griechischSbis == null) {
      griechischSbis = StringUtils.EMPTY;
    }

    informatik = result.getString("informatik");
    informatikJvon = result.getString("informatik_j_von");
    if (informatikJvon == null) {
      informatikJvon = StringUtils.EMPTY;
    }
    informatikJbis = result.getString("informatik_j_bis");
    if (informatikJbis == null) {
      informatikJbis = StringUtils.EMPTY;
    }
    informatikSvon = result.getString("informatik_s_von");
    if (informatikSvon == null) {
      informatikSvon = StringUtils.EMPTY;
    }
    informatikSbis = result.getString("informatik_s_bis");
    if (informatikSbis == null) {
      informatikSbis = StringUtils.EMPTY;
    }

    latein = result.getString("latein");
    lateinJvon = result.getString("latein_j_von");
    if (lateinJvon == null) {
      lateinJvon = StringUtils.EMPTY;
    }
    lateinJbis = result.getString("latein_j_bis");
    if (lateinJbis == null) {
      lateinJbis = StringUtils.EMPTY;
    }
    lateinSvon = result.getString("latein_s_von");
    if (lateinSvon == null) {
      lateinSvon = StringUtils.EMPTY;
    }
    lateinSbis = result.getString("latein_s_bis");
    if (lateinSbis == null) {
      lateinSbis = StringUtils.EMPTY;
    }

    maschineschreiben = result.getString("maschineschreiben");
    maschineschreibenJvon = result.getString("maschineschreiben_j_von");
    if (maschineschreibenJvon == null) {
      maschineschreibenJvon = StringUtils.EMPTY;
    }
    maschineschreibenJbis = result.getString("maschineschreiben_j_bis");
    if (maschineschreibenJbis == null) {
      maschineschreibenJbis = StringUtils.EMPTY;
    }
    maschineschreibenSvon = result.getString("maschineschreiben_s_von");
    if (maschineschreibenSvon == null) {
      maschineschreibenSvon = StringUtils.EMPTY;
    }
    maschineschreibenSbis = result.getString("maschineschreiben_s_bis");
    if (maschineschreibenSbis == null) {
      maschineschreibenSbis = StringUtils.EMPTY;
    }

    mathematik = result.getString("mathematik");
    mathematikJvon = result.getString("mathematik_j_von");
    if (mathematikJvon == null) {
      mathematikJvon = StringUtils.EMPTY;
    }
    mathematikJbis = result.getString("mathematik_j_bis");
    if (mathematikJbis == null) {
      mathematikJbis = StringUtils.EMPTY;
    }
    mathematikSvon = result.getString("mathematik_s_von");
    if (mathematikSvon == null) {
      mathematikSvon = StringUtils.EMPTY;
    }
    mathematikSbis = result.getString("mathematik_s_bis");
    if (mathematikSbis == null) {
      mathematikSbis = StringUtils.EMPTY;
    }

    physik = result.getString("physik");
    physikJvon = result.getString("physik_j_von");
    if (physikJvon == null) {
      physikJvon = StringUtils.EMPTY;
    }
    physikJbis = result.getString("physik_j_bis");
    if (physikJbis == null) {
      physikJbis = StringUtils.EMPTY;
    }
    physikSvon = result.getString("physik_s_von");
    if (physikSvon == null) {
      physikSvon = StringUtils.EMPTY;
    }
    physikSbis = result.getString("physik_s_bis");
    if (physikSbis == null) {
      physikSbis = StringUtils.EMPTY;
    }

    sozialkunde = result.getString("sozialkunde");
    sozialkundeJvon = result.getString("sozialkunde_j_von");
    if (sozialkundeJvon == null) {
      sozialkundeJvon = StringUtils.EMPTY;
    }
    sozialkundeJbis = result.getString("sozialkunde_j_bis");
    if (sozialkundeJbis == null) {
      sozialkundeJbis = StringUtils.EMPTY;
    }
    sozialkundeSvon = result.getString("sozialkunde_s_von");
    if (sozialkundeSvon == null) {
      sozialkundeSvon = StringUtils.EMPTY;
    }
    sozialkundeSbis = result.getString("sozialkunde_s_bis");
    if (sozialkundeSbis == null) {
      sozialkundeSbis = StringUtils.EMPTY;
    }

    stenografie = result.getString("stenografie");
    stenografieJvon = result.getString("stenografie_j_von");
    if (stenografieJvon == null) {
      stenografieJvon = StringUtils.EMPTY;
    }
    stenografieJbis = result.getString("stenografie_j_bis");
    if (stenografieJbis == null) {
      stenografieJbis = StringUtils.EMPTY;
    }
    stenografieSvon = result.getString("stenografie_s_von");
    if (stenografieSvon == null) {
      stenografieSvon = StringUtils.EMPTY;
    }
    stenografieSbis = result.getString("stenografie_s_bis");
    if (stenografieSbis == null) {
      stenografieSbis = StringUtils.EMPTY;
    }

    weiteresFach1 = result.getString("weiteres_fach1");
    if (weiteresFach1 == null) {
      weiteresFach1 = StringUtils.EMPTY;
    }
    weiteresFach1Jvon = result.getString("weiteres_fach1_j_von");
    if (weiteresFach1Jvon == null) {
      weiteresFach1Jvon = StringUtils.EMPTY;
    }
    weiteresFach1Jbis = result.getString("weiteres_fach1_j_bis");
    if (weiteresFach1Jbis == null) {
      weiteresFach1Jbis = StringUtils.EMPTY;
    }
    weiteresFach1Svon = result.getString("weiteres_fach1_s_von");
    if (weiteresFach1Svon == null) {
      weiteresFach1Svon = StringUtils.EMPTY;
    }
    weiteresFach1Sbis = result.getString("weiteres_fach1_s_bis");
    if (weiteresFach1Sbis == null) {
      weiteresFach1Sbis = StringUtils.EMPTY;
    }

    weiteresFach2 = result.getString("weiteres_fach2");
    if (weiteresFach2 == null) {
      weiteresFach2 = StringUtils.EMPTY;
    }
    weiteresFach2Jvon = result.getString("weiteres_fach2_j_von");
    if (weiteresFach2Jvon == null) {
      weiteresFach2Jvon = StringUtils.EMPTY;
    }
    weiteresFach2Jbis = result.getString("weiteres_fach2_j_bis");
    if (weiteresFach2Jbis == null) {
      weiteresFach2Jbis = StringUtils.EMPTY;
    }
    weiteresFach2Svon = result.getString("weiteres_fach2_s_von");
    if (weiteresFach2Svon == null) {
      weiteresFach2Svon = StringUtils.EMPTY;
    }
    weiteresFach2Sbis = result.getString("weiteres_fach2_s_bis");
    if (weiteresFach2Sbis == null) {
      weiteresFach2Sbis = StringUtils.EMPTY;
    }

    weiteresFach3 = result.getString("weiteres_fach3");
    if (weiteresFach3 == null) {
      weiteresFach3 = StringUtils.EMPTY;
    }
    weiteresFach3Jvon = result.getString("weiteres_fach3_j_von");
    if (weiteresFach3Jvon == null) {
      weiteresFach3Jvon = StringUtils.EMPTY;
    }
    weiteresFach3Jbis = result.getString("weiteres_fach3_j_bis");
    if (weiteresFach3Jbis == null) {
      weiteresFach3Jbis = StringUtils.EMPTY;
    }
    weiteresFach3Svon = result.getString("weiteres_fach3_s_von");
    if (weiteresFach3Svon == null) {
      weiteresFach3Svon = StringUtils.EMPTY;
    }
    weiteresFach3Sbis = result.getString("weiteres_fach3_s_bis");
    if (weiteresFach3Sbis == null) {
      weiteresFach3Sbis = StringUtils.EMPTY;
    }

    preis = result.getString("preis_text");
    if (preis == null) {
      preis = StringUtils.EMPTY;
    }
  }

  /**
   * Eingegebenes Fach (Vorgaben) auf Korrektheit prüfen.
   *
   * @param fachName name of the subject
   * @param fach subject
   * @param pfachJvon subject year from
   * @param pfachJbis subject year to
   * @param pfachSvon subject semester from
   * @param pfachSbis subject semester to
   */
  private String[] fachPruefen1(final String fachName, final String pfach, final String pfachJvon, final String pfachJbis,
      final String pfachSvon, final String pfachSbis) throws de.knightsoft.common.TextException {
    String fachJvon = pfachJvon;
    String fachJbis = pfachJbis;
    String fachSvon = pfachSvon;
    String fachSbis = pfachSbis;
    String fach = pfach;
    if (StringUtils.isEmpty(fachJvon) && StringUtils.isEmpty(fachJbis) && StringUtils.isEmpty(fachSvon)
        && StringUtils.isEmpty(fachSbis)) {
      fach = "N";
    } else {
      fach = "J";

      if (StringUtils.isNotEmpty(fachJvon) && !StringUtils.isNumeric(fachJvon)) {
        throw new de.knightsoft.common.TextException(
            "Die Eingabe des Schuljahres von ist nicht nummerisch bei dem Fach " + fachName);
      }

      if (StringUtils.isNotEmpty(fachJbis) && !StringUtils.isNumeric(fachJbis)) {
        throw new de.knightsoft.common.TextException(
            "Die Eingabe des Schuljahres bis ist nicht nummerisch bei dem Fach " + fachName);
      }

      if (StringUtils.isNotEmpty(fachSvon) && !StringUtils.isNumeric(fachSvon)) {
        throw new de.knightsoft.common.TextException(
            "Die Eingabe des Semesters von ist nicht nummerisch bei dem Fach " + fachName);
      }

      if (StringUtils.isNotEmpty(fachSbis) && !StringUtils.isNumeric(fachSbis)) {
        throw new de.knightsoft.common.TextException(
            "Die Eingabe des Semesters bis ist nicht nummerisch bei dem Fach " + fachName);
      }

      if (StringUtils.isEmpty(fachJvon) && StringUtils.isEmpty(fachJbis)) {
        // nothing to do
      } else if (StringUtils.isEmpty(fachJvon) && StringUtils.isNotEmpty(fachJbis)) {
        fachJvon = fachJbis;
      } else if (StringUtils.isEmpty(fachJbis) && StringUtils.isNotEmpty(fachJvon)) {
        fachJbis = fachJvon;
      } else {
        if (Integer.parseInt(fachJvon) > Integer.parseInt(fachJbis)) {
          throw new de.knightsoft.common.TextException(
              "Die Eingabe des Schuljahres von ist grösser als die des Schuljahres bis " + "bei dem Fach " + fachName);
        }
      }

      if (StringUtils.isEmpty(fachSvon) && StringUtils.isEmpty(fachSbis)) {
        // nothing to do
      } else if (StringUtils.isEmpty(fachSvon) && StringUtils.isNotEmpty(fachSbis)) {
        fachSvon = fachSbis;
      } else if (StringUtils.isEmpty(fachSbis) && StringUtils.isNotEmpty(fachSvon)) {
        fachSbis = fachSvon;
      } else {
        if (Integer.parseInt(fachSvon) > Integer.parseInt(fachSbis)) {
          throw new de.knightsoft.common.TextException(
              "Die Eingabe des Semesters von ist grösser als die des Schuljahres bis bei dem Fach " + fachName);
        }
      }
    }

    return new String[] {fach, fachJvon, fachJbis, fachSvon, fachSbis};
  }

  /**
   * Eingegebenes Fach (selbst eingegeben) auf Korrektheit prüfen.
   *
   * @param fachName name of the subject
   * @param pfachJvon subject year from
   * @param pfachJbis subject year to
   * @param pfachSvon subject semester from
   * @param pfachSbis subject semester to
   * @return string array with from to ranches
   */
  private String[] fachPruefen2(final String fachName, final String pfachJvon, final String pfachJbis, final String pfachSvon,
      final String pfachSbis) throws de.knightsoft.common.TextException {
    String fachJvon = pfachJvon;
    String fachJbis = pfachJbis;
    String fachSvon = pfachSvon;
    String fachSbis = pfachSbis;

    if (StringUtils.isEmpty(fachJvon) && StringUtils.isEmpty(fachJbis) && StringUtils.isEmpty(fachSvon)
        && StringUtils.isEmpty(fachSvon)) {
      if (StringUtils.isNotEmpty(fachName)) {
        throw new de.knightsoft.common.TextException("Zu dem Fach " + fachName + " wurden keine Jahrgangsstufen angegeben!");
      }
    } else {
      if (StringUtils.isEmpty(fachName)) {
        throw new de.knightsoft.common.TextException(
            "Bei Jahrgangsangaben in den zusätzlichen Fächern, " + "muss auch die Fachbezeichnung gefüllt werden!");
      }

      if (StringUtils.isNotEmpty(fachJvon) && !StringUtils.isNumeric(fachJvon)) {
        throw new de.knightsoft.common.TextException(
            "Die Eingabe des Schuljahres von ist nicht nummerisch bei dem Fach " + fachName);
      }

      if (StringUtils.isNotEmpty(fachJbis) && !StringUtils.isNumeric(fachJbis)) {
        throw new de.knightsoft.common.TextException(
            "Die Eingabe des Schuljahres bis ist nicht nummerisch bei dem Fach " + fachName);
      }

      if (StringUtils.isNotEmpty(fachSvon) && !StringUtils.isNumeric(fachSvon)) {
        throw new de.knightsoft.common.TextException(
            "Die Eingabe des Semesters von ist nicht nummerisch bei dem Fach " + fachName);
      }

      if (StringUtils.isNotEmpty(fachSbis) && !StringUtils.isNumeric(fachSbis)) {
        throw new de.knightsoft.common.TextException(
            "Die Eingabe des Semesters bis ist nicht nummerisch bei dem Fach " + fachName);
      }

      if (StringUtils.isEmpty(fachJvon) && StringUtils.isEmpty(fachJbis)) {
        // nothing to do
      } else if (StringUtils.isEmpty(fachJvon) && StringUtils.isNotEmpty(fachJbis)) {
        fachJvon = fachJbis;
      } else if (StringUtils.isEmpty(fachJbis) && StringUtils.isNotEmpty(fachJvon)) {
        fachJbis = fachJvon;
      } else {
        if (Integer.parseInt(fachJvon) > Integer.parseInt(fachJbis)) {
          throw new de.knightsoft.common.TextException(
              "Die Eingabe des Schuljahres von ist grösser als die des " + "Schuljahres bis bei dem Fach " + fachName);
        }
      }

      if (StringUtils.isEmpty(fachSvon) && StringUtils.isEmpty(fachSbis)) {
        // nothing to do
      } else if (StringUtils.isEmpty(fachSvon) && StringUtils.isNotEmpty(fachSbis)) {
        fachSvon = fachSbis;
      } else if (StringUtils.isEmpty(fachSbis) && StringUtils.isNotEmpty(fachSvon)) {
        fachSbis = fachSvon;
      } else {
        if (Integer.parseInt(fachSvon) > Integer.parseInt(fachSbis)) {
          throw new de.knightsoft.common.TextException(
              "Die Eingabe des Semesters von ist grösser als die des " + "Semesters bis bei dem Fach " + fachName);
        }
      }
    }

    return new String[] {fachName, fachJvon, fachJbis, fachSvon, fachSbis};
  }

  @Override
  public void checkDaten() throws de.knightsoft.common.TextException {

    super.checkDaten();

    if (!vorwahl.equals("alle")) {
      if (vorwahl.length() < 2) {
        throw new de.knightsoft.common.TextException("Die Vorwahl ist nicht korrekt gefüllt!");
      } else {
        if (!StringUtils.isNumeric(vorwahl)) {
          throw new de.knightsoft.common.TextException("Die Telefonvorwahl ist nicht nummerisch!");
        }
      }
    }

    if (StringUtils.isEmpty(geburtsjahr)) {
      throw new de.knightsoft.common.TextException("Bitte geben Sie Ihr Geburtsjahr ein!");
    } else {
      geburtsjahr = checkJahr(geburtsjahr, false, true);
    }

    String[] rueckgabe;

    rueckgabe = fachPruefen1("Biologie", biologie, biologieJvon, biologieJbis, biologieSvon, biologieSbis);
    biologie = rueckgabe[0];
    biologieJvon = rueckgabe[1];
    biologieJbis = rueckgabe[2];
    biologieSvon = rueckgabe[3];
    biologieSbis = rueckgabe[4];

    rueckgabe = fachPruefen1("BWL", bwl, bwlJvon, bwlJbis, bwlSvon, bwlSbis);
    bwl = rueckgabe[0];
    bwlJvon = rueckgabe[1];
    bwlJbis = rueckgabe[2];
    bwlSvon = rueckgabe[3];
    bwlSbis = rueckgabe[4];

    rueckgabe = fachPruefen1("Chemie", chemie, chemieJvon, chemieJbis, chemieSvon, chemieSbis);
    chemie = rueckgabe[0];
    chemieJvon = rueckgabe[1];
    chemieJbis = rueckgabe[2];
    chemieSvon = rueckgabe[3];
    chemieSbis = rueckgabe[4];

    rueckgabe = fachPruefen1("Deutsch", deutsch, deutschJvon, deutschJbis, deutschSvon, deutschSbis);
    deutsch = rueckgabe[0];
    deutschJvon = rueckgabe[1];
    deutschJbis = rueckgabe[2];
    deutschSvon = rueckgabe[3];
    deutschSbis = rueckgabe[4];

    rueckgabe = fachPruefen1("Englisch", englisch, englischJvon, englischJbis, englischSvon, englischSbis);
    englisch = rueckgabe[0];
    englischJvon = rueckgabe[1];
    englischJbis = rueckgabe[2];
    englischSvon = rueckgabe[3];
    englischSbis = rueckgabe[4];

    rueckgabe = fachPruefen1("Erdkunde", erdkunde, erdkundeJvon, erdkundeJbis, erdkundeSvon, erdkundeSbis);
    erdkunde = rueckgabe[0];
    erdkundeJvon = rueckgabe[1];
    erdkundeJbis = rueckgabe[2];
    erdkundeSvon = rueckgabe[3];
    erdkundeSbis = rueckgabe[4];

    rueckgabe =
        fachPruefen1("Franzoesisch", franzoesisch, franzoesischJvon, franzoesischJbis, franzoesischSvon, franzoesischSbis);
    franzoesisch = rueckgabe[0];
    franzoesischJvon = rueckgabe[1];
    franzoesischJbis = rueckgabe[2];
    franzoesischSvon = rueckgabe[3];
    franzoesischSbis = rueckgabe[4];

    rueckgabe = fachPruefen1("Geschichte", geschichte, geschichteJvon, geschichteJbis, geschichteSvon, geschichteSbis);
    geschichte = rueckgabe[0];
    geschichteJvon = rueckgabe[1];
    geschichteJbis = rueckgabe[2];
    geschichteSvon = rueckgabe[3];
    geschichteSbis = rueckgabe[4];

    rueckgabe = fachPruefen1("Griechisch", griechisch, griechischJvon, griechischJbis, griechischSvon, griechischSbis);
    griechisch = rueckgabe[0];
    griechischJvon = rueckgabe[1];
    griechischJbis = rueckgabe[2];
    griechischSvon = rueckgabe[3];
    griechischSbis = rueckgabe[4];

    rueckgabe = fachPruefen1("Informatik", informatik, informatikJvon, informatikJbis, informatikSvon, informatikSbis);
    informatik = rueckgabe[0];
    informatikJvon = rueckgabe[1];
    informatikJbis = rueckgabe[2];
    informatikSvon = rueckgabe[3];
    informatikSbis = rueckgabe[4];

    rueckgabe = fachPruefen1("Latein", latein, lateinJvon, lateinJbis, lateinSvon, lateinSbis);
    latein = rueckgabe[0];
    lateinJvon = rueckgabe[1];
    lateinJbis = rueckgabe[2];
    lateinSvon = rueckgabe[3];
    lateinSbis = rueckgabe[4];

    rueckgabe = fachPruefen1("Maschineschreiben", maschineschreiben, maschineschreibenJvon, maschineschreibenJbis,
        maschineschreibenSvon, maschineschreibenSbis);
    maschineschreiben = rueckgabe[0];
    maschineschreibenJvon = rueckgabe[1];
    maschineschreibenJbis = rueckgabe[2];
    maschineschreibenSvon = rueckgabe[3];
    maschineschreibenSbis = rueckgabe[4];

    rueckgabe = fachPruefen1("Mathematik", mathematik, mathematikJvon, mathematikJbis, mathematikSvon, mathematikSbis);
    mathematik = rueckgabe[0];
    mathematikJvon = rueckgabe[1];
    mathematikJbis = rueckgabe[2];
    mathematikSvon = rueckgabe[3];
    mathematikSbis = rueckgabe[4];

    rueckgabe = fachPruefen1("Physik", physik, physikJvon, physikJbis, physikSvon, physikSbis);
    physik = rueckgabe[0];
    physikJvon = rueckgabe[1];
    physikJbis = rueckgabe[2];
    physikSvon = rueckgabe[3];
    physikSbis = rueckgabe[4];

    rueckgabe = fachPruefen1("Sozialkunde", sozialkunde, sozialkundeJvon, sozialkundeJbis, sozialkundeSvon, sozialkundeSbis);
    sozialkunde = rueckgabe[0];
    sozialkundeJvon = rueckgabe[1];
    sozialkundeJbis = rueckgabe[2];
    sozialkundeSvon = rueckgabe[3];
    sozialkundeSbis = rueckgabe[4];

    rueckgabe = fachPruefen1("Stenografie", stenografie, stenografieJvon, stenografieJbis, stenografieSvon, stenografieSbis);
    stenografie = rueckgabe[0];
    stenografieJvon = rueckgabe[1];
    stenografieJbis = rueckgabe[2];
    stenografieSvon = rueckgabe[3];
    stenografieSbis = rueckgabe[4];

    rueckgabe = fachPruefen2(weiteresFach1, weiteresFach1Jvon, weiteresFach1Jbis, weiteresFach1Svon, weiteresFach1Sbis);
    weiteresFach1 = rueckgabe[0];
    weiteresFach1Jvon = rueckgabe[1];
    weiteresFach1Jbis = rueckgabe[2];
    weiteresFach1Svon = rueckgabe[3];
    weiteresFach1Sbis = rueckgabe[4];

    rueckgabe = fachPruefen2(weiteresFach2, weiteresFach2Jvon, weiteresFach2Jbis, weiteresFach2Svon, weiteresFach2Sbis);
    weiteresFach2 = rueckgabe[0];
    weiteresFach2Jvon = rueckgabe[1];
    weiteresFach2Jbis = rueckgabe[2];
    weiteresFach2Svon = rueckgabe[3];
    weiteresFach2Sbis = rueckgabe[4];

    rueckgabe = fachPruefen2(weiteresFach3, weiteresFach3Jvon, weiteresFach3Jbis, weiteresFach3Svon, weiteresFach3Sbis);
    weiteresFach3 = rueckgabe[0];
    weiteresFach3Jvon = rueckgabe[1];
    weiteresFach3Jbis = rueckgabe[2];
    weiteresFach3Svon = rueckgabe[3];
    weiteresFach3Sbis = rueckgabe[4];
  }

  /**
   * prepare insert.
   *
   * @return sql string.
   */
  public String insertVorbereiten() throws de.knightsoft.common.TextException {

    final StringBuilder sqlStringA = new StringBuilder(1500);
    final StringBuilder sqlStringB = new StringBuilder(256);
    sqlStringA.append(", vorwahl, geburtsjahr");
    sqlStringB.append(", ").append(de.knightsoft.common.StringToSql.convert(vorwahl)).append(", ")
        .append(de.knightsoft.common.StringToSql.convert(geburtsjahr));

    if (StringUtils.isNotEmpty(preis)) {
      sqlStringA.append(", preis_text");
      sqlStringB.append(", ").append(de.knightsoft.common.StringToSql.convert(preis));
    }

    sqlStringA.append(", biologie");
    sqlStringB.append(", ").append(de.knightsoft.common.StringToSql.convert(biologie));
    if (StringUtils.isNotEmpty(biologieJvon)) {
      sqlStringA.append(", biologie_j_von, biologie_j_bis");
      sqlStringB.append(", ").append(biologieJvon).append(", ").append(biologieJbis);
    }
    if (StringUtils.isNotEmpty(biologieSvon)) {
      sqlStringA.append(", biologie_s_von, biologie_s_bis");
      sqlStringB.append(", ").append(biologieSvon).append(", ").append(biologieSbis);
    }

    sqlStringA.append(", bwl");
    sqlStringB.append(", ").append(de.knightsoft.common.StringToSql.convert(bwl));
    if (StringUtils.isNotEmpty(bwlJvon)) {
      sqlStringA.append(", bwl_j_von, bwl_j_bis");
      sqlStringB.append(", ").append(bwlJvon).append(", ").append(bwlJbis);
    }
    if (StringUtils.isNotEmpty(bwlSvon)) {
      sqlStringA.append(", bwl_s_von, bwl_s_bis");
      sqlStringB.append(", ").append(bwlSvon).append(", ").append(bwlSbis);
    }

    sqlStringA.append(", chemie");
    sqlStringB.append(", ").append(de.knightsoft.common.StringToSql.convert(chemie));
    if (StringUtils.isNotEmpty(chemieJvon)) {
      sqlStringA.append(", chemie_j_von, chemie_j_bis");
      sqlStringB.append(", ").append(chemieJvon).append(", ").append(chemieJbis);
    }
    if (StringUtils.isNotEmpty(chemieSvon)) {
      sqlStringA.append(", chemie_s_von, chemie_s_bis");
      sqlStringB.append(", ").append(chemieSvon).append(", ").append(chemieSbis);
    }

    sqlStringA.append(", deutsch");
    sqlStringB.append(", ").append(de.knightsoft.common.StringToSql.convert(deutsch));
    if (StringUtils.isNotEmpty(deutschJvon)) {
      sqlStringA.append(", deutsch_j_von, deutsch_j_bis");
      sqlStringB.append(", ").append(deutschJvon).append(", ").append(deutschJbis);
    }
    if (StringUtils.isNotEmpty(deutschSvon)) {
      sqlStringA.append(", deutsch_s_von, deutsch_s_bis");
      sqlStringB.append(", ").append(deutschSvon).append(", ").append(deutschSbis);
    }

    sqlStringA.append(", englisch");
    sqlStringB.append(", ").append(de.knightsoft.common.StringToSql.convert(englisch));
    if (StringUtils.isNotEmpty(englischJvon)) {
      sqlStringA.append(", englisch_j_von, englisch_j_bis");
      sqlStringB.append(", ").append(englischJvon).append(", ").append(englischJbis);
    }
    if (StringUtils.isNotEmpty(englischSvon)) {
      sqlStringA.append(", englisch_s_von, englisch_s_bis");
      sqlStringB.append(", ").append(englischSvon).append(", ").append(englischSbis);
    }

    sqlStringA.append(", erdkunde");
    sqlStringB.append(", ").append(de.knightsoft.common.StringToSql.convert(erdkunde));
    if (StringUtils.isNotEmpty(erdkundeJvon)) {
      sqlStringA.append(", erdkunde_j_von, erdkunde_j_bis");
      sqlStringB.append(", ").append(erdkundeJvon).append(", ").append(erdkundeJbis);
    }
    if (StringUtils.isNotEmpty(erdkundeSvon)) {
      sqlStringA.append(", erdkunde_s_von, erdkunde_s_bis");
      sqlStringB.append(", ").append(erdkundeSvon).append(", ").append(erdkundeSbis);
    }

    sqlStringA.append(", franzoesisch");
    sqlStringB.append(", ").append(de.knightsoft.common.StringToSql.convert(franzoesisch));
    if (StringUtils.isNotEmpty(franzoesischJvon)) {
      sqlStringA.append(", franzoesisch_j_von, franzoesisch_j_bis");
      sqlStringB.append(", ").append(franzoesischJvon).append(", ").append(franzoesischJbis);
    }
    if (StringUtils.isNotEmpty(franzoesischSvon)) {
      sqlStringA.append(", franzoesisch_s_von, franzoesisch_s_bis");
      sqlStringB.append(", ").append(franzoesischSvon).append(", ").append(franzoesischSbis);
    }

    sqlStringA.append(", geschichte");
    sqlStringB.append(", ").append(de.knightsoft.common.StringToSql.convert(geschichte));
    if (StringUtils.isNotEmpty(geschichteJvon)) {
      sqlStringA.append(", geschichte_j_von, geschichte_j_bis");
      sqlStringB.append(", ").append(geschichteJvon).append(", ").append(geschichteJbis);
    }
    if (StringUtils.isNotEmpty(geschichteSvon)) {
      sqlStringA.append(", geschichte_s_von, geschichte_s_bis");
      sqlStringB.append(", ").append(geschichteSvon).append(", ").append(geschichteSbis);
    }

    sqlStringA.append(", griechisch");
    sqlStringB.append(", ").append(de.knightsoft.common.StringToSql.convert(griechisch));
    if (StringUtils.isNotEmpty(griechischJvon)) {
      sqlStringA.append(", griechisch_j_von, griechisch_j_bis");
      sqlStringB.append(", ").append(griechischJvon).append(", ").append(griechischJbis);
    }
    if (StringUtils.isNotEmpty(griechischSvon)) {
      sqlStringA.append(", griechisch_s_von, griechisch_s_bis");
      sqlStringB.append(", ").append(griechischSvon).append(", ").append(griechischSbis);
    }

    sqlStringA.append(", informatik");
    sqlStringB.append(", ").append(de.knightsoft.common.StringToSql.convert(informatik));
    if (StringUtils.isNotEmpty(informatikJvon)) {
      sqlStringA.append(", informatik_j_von, informatik_j_bis");
      sqlStringB.append(", ").append(informatikJvon).append(", ").append(informatikJbis);
    }
    if (StringUtils.isNotEmpty(informatikSvon)) {
      sqlStringA.append(", informatik_s_von, informatik_s_bis");
      sqlStringB.append(", ").append(informatikSvon).append(", ").append(informatikSbis);
    }

    sqlStringA.append(", latein");
    sqlStringB.append(", ").append(de.knightsoft.common.StringToSql.convert(latein));
    if (StringUtils.isNotEmpty(lateinJvon)) {
      sqlStringA.append(", latein_j_von, latein_j_bis");
      sqlStringB.append(", ").append(lateinJvon).append(", ").append(lateinJbis);
    }
    if (StringUtils.isNotEmpty(lateinSvon)) {
      sqlStringA.append(", latein_s_von, latein_s_bis");
      sqlStringB.append(", ").append(lateinSvon).append(", ").append(lateinSbis);
    }

    sqlStringA.append(", maschineschreiben");
    sqlStringB.append(", ").append(de.knightsoft.common.StringToSql.convert(maschineschreiben));
    if (StringUtils.isNotEmpty(maschineschreibenJvon)) {
      sqlStringA.append(", maschineschreiben_j_von, maschineschreiben_j_bis");
      sqlStringB.append(", ").append(maschineschreibenJvon).append(", ").append(maschineschreibenJbis);
    }
    if (StringUtils.isNotEmpty(maschineschreibenSvon)) {
      sqlStringA.append(", maschineschreiben_s_von, maschineschreiben_s_bis");
      sqlStringB.append(", ").append(maschineschreibenSvon).append(", ").append(maschineschreibenSbis);
    }

    sqlStringA.append(", mathematik");
    sqlStringB.append(", ").append(de.knightsoft.common.StringToSql.convert(mathematik));
    if (StringUtils.isNotEmpty(mathematikJvon)) {
      sqlStringA.append(", mathematik_j_von, mathematik_j_bis");
      sqlStringB.append(", ").append(mathematikJvon).append(", ").append(mathematikJbis);
    }
    if (StringUtils.isNotEmpty(mathematikSvon)) {
      sqlStringA.append(", mathematik_s_von, mathematik_s_bis");
      sqlStringB.append(", ").append(mathematikSvon).append(", ").append(mathematikSbis);
    }

    sqlStringA.append(", physik");
    sqlStringB.append(", ").append(de.knightsoft.common.StringToSql.convert(physik));
    if (StringUtils.isNotEmpty(physikJvon)) {
      sqlStringA.append(", physik_j_von, physik_j_bis");
      sqlStringB.append(", ").append(physikJvon).append(", ").append(physikJbis);
    }
    if (StringUtils.isNotEmpty(physikSvon)) {
      sqlStringA.append(", physik_s_von, physik_s_bis");
      sqlStringB.append(", ").append(physikSvon).append(", ").append(physikSbis);
    }

    sqlStringA.append(", sozialkunde");
    sqlStringB.append(", ").append(de.knightsoft.common.StringToSql.convert(sozialkunde));
    if (StringUtils.isNotEmpty(sozialkundeJvon)) {
      sqlStringA.append(", sozialkunde_j_von, sozialkunde_j_bis");
      sqlStringB.append(", ").append(sozialkundeJvon).append(", ").append(sozialkundeJbis);
    }
    if (StringUtils.isNotEmpty(sozialkundeSvon)) {
      sqlStringA.append(", sozialkunde_s_von, sozialkunde_s_bis");
      sqlStringB.append(", ").append(sozialkundeSvon).append(", ").append(sozialkundeSbis);
    }

    sqlStringA.append(", stenografie");
    sqlStringB.append(", ").append(de.knightsoft.common.StringToSql.convert(stenografie));
    if (StringUtils.isNotEmpty(stenografieJvon)) {
      sqlStringA.append(", stenografie_j_von, stenografie_j_bis");
      sqlStringB.append(", ").append(stenografieJvon).append(", ").append(stenografieJbis);
    }
    if (StringUtils.isNotEmpty(stenografieSvon)) {
      sqlStringA.append(", stenografie_s_von, stenografie_s_bis");
      sqlStringB.append(", ").append(stenografieSvon).append(", ").append(stenografieSbis);
    }

    if (StringUtils.isNotEmpty(weiteresFach1)) {
      sqlStringA.append(", weiteres_fach1");
      sqlStringB.append(", ").append(de.knightsoft.common.StringToSql.convert(weiteresFach1));
      if (StringUtils.isNotEmpty(weiteresFach1Jvon)) {
        sqlStringA.append(", weiteres_fach1_j_von, weiteres_fach1_j_bis");
        sqlStringB.append(", ").append(weiteresFach1Jvon).append(", ").append(weiteresFach1Jbis);
      }
      if (StringUtils.isNotEmpty(weiteresFach1Svon)) {
        sqlStringA.append(", weiteres_fach1_s_von, weiteres_fach1_s_bis");
        sqlStringB.append(", ").append(weiteresFach1Svon).append(", ").append(weiteresFach1Sbis);
      }
    }

    if (StringUtils.isNotEmpty(weiteresFach2)) {
      sqlStringA.append(", weiteres_fach2");
      sqlStringB.append(", ").append(de.knightsoft.common.StringToSql.convert(weiteresFach2));
      if (StringUtils.isNotEmpty(weiteresFach2Jvon)) {
        sqlStringA.append(", weiteres_fach2_j_von, weiteres_fach2_j_bis");
        sqlStringB.append(", ").append(weiteresFach2Jvon).append(", ").append(weiteresFach2Jbis);
      }
      if (StringUtils.isNotEmpty(weiteresFach2Svon)) {
        sqlStringA.append(", weiteres_fach2_s_von, weiteres_fach2_s_bis");
        sqlStringB.append(", ").append(weiteresFach2Svon).append(", ").append(weiteresFach2Sbis);
      }
    }

    if (StringUtils.isNotEmpty(weiteresFach3)) {
      sqlStringA.append(", weiteres_fach3");
      sqlStringB.append(", ").append(de.knightsoft.common.StringToSql.convert(weiteresFach3));
      if (StringUtils.isNotEmpty(weiteresFach3Jvon)) {
        sqlStringA.append(", weiteres_fach3_j_von, weiteres_fach3_j_bis");
        sqlStringB.append(", ").append(weiteresFach3Jvon).append(", ").append(weiteresFach3Jbis);
      }
      if (StringUtils.isNotEmpty(weiteresFach3Svon)) {
        sqlStringA.append(", weiteres_fach3_s_von, weiteres_fach3_s_bis");
        sqlStringB.append(", ").append(weiteresFach3Svon).append(", ").append(weiteresFach3Sbis);
      }
    }

    return prepareInsert(sqlStringA.toString(), sqlStringB.toString(), "KSNH");
  }

  @Override
  public String prepareUpdate() throws de.knightsoft.common.TextException {

    return super.prepareUpdate() + ", vorwahl=" + de.knightsoft.common.StringToSql.convertString(vorwahl) + ", geburtsjahr="
        + de.knightsoft.common.StringToSql.convertString(geburtsjahr) + ", preis_text="
        + de.knightsoft.common.StringToSql.convertString(preis) + ", biologie="
        + de.knightsoft.common.StringToSql.convertString(biologie) + ", biologie_j_von="
        + de.knightsoft.common.StringToSql.convertNumber(biologieJvon) + ", biologie_j_bis="
        + de.knightsoft.common.StringToSql.convertNumber(biologieJbis) + ", biologie_s_von="
        + de.knightsoft.common.StringToSql.convertNumber(biologieSvon) + ", biologie_s_bis="
        + de.knightsoft.common.StringToSql.convertNumber(biologieSbis) + ", bwl="
        + de.knightsoft.common.StringToSql.convertString(bwl) + ", bwl_j_von="
        + de.knightsoft.common.StringToSql.convertNumber(bwlJvon) + ", bwl_j_bis="
        + de.knightsoft.common.StringToSql.convertNumber(bwlJbis) + ", bwl_s_von="
        + de.knightsoft.common.StringToSql.convertNumber(bwlSvon) + ", bwl_s_bis="
        + de.knightsoft.common.StringToSql.convertNumber(bwlSbis) + ", chemie="
        + de.knightsoft.common.StringToSql.convertString(chemie) + ", chemie_j_von="
        + de.knightsoft.common.StringToSql.convertNumber(chemieJvon) + ", chemie_j_bis="
        + de.knightsoft.common.StringToSql.convertNumber(chemieJbis) + ", chemie_s_von="
        + de.knightsoft.common.StringToSql.convertNumber(chemieSvon) + ", chemie_s_bis="
        + de.knightsoft.common.StringToSql.convertNumber(chemieSbis) + ", deutsch="
        + de.knightsoft.common.StringToSql.convertString(deutsch) + ", deutsch_j_von="
        + de.knightsoft.common.StringToSql.convertNumber(deutschJvon) + ", deutsch_j_bis="
        + de.knightsoft.common.StringToSql.convertNumber(deutschJbis) + ", deutsch_s_von="
        + de.knightsoft.common.StringToSql.convertNumber(deutschSvon) + ", deutsch_s_bis="
        + de.knightsoft.common.StringToSql.convertNumber(deutschSbis) + ", englisch="
        + de.knightsoft.common.StringToSql.convertString(englisch) + ", englisch_j_von="
        + de.knightsoft.common.StringToSql.convertNumber(englischJvon) + ", englisch_j_bis="
        + de.knightsoft.common.StringToSql.convertNumber(englischJbis) + ", englisch_s_von="
        + de.knightsoft.common.StringToSql.convertNumber(englischSvon) + ", englisch_s_bis="
        + de.knightsoft.common.StringToSql.convertNumber(englischSbis) + ", erdkunde="
        + de.knightsoft.common.StringToSql.convertString(erdkunde) + ", erdkunde_j_von="
        + de.knightsoft.common.StringToSql.convertNumber(erdkundeJvon) + ", erdkunde_j_bis="
        + de.knightsoft.common.StringToSql.convertNumber(erdkundeJbis) + ", erdkunde_s_von="
        + de.knightsoft.common.StringToSql.convertNumber(erdkundeSvon) + ", erdkunde_s_bis="
        + de.knightsoft.common.StringToSql.convertNumber(erdkundeSbis) + ", franzoesisch="
        + de.knightsoft.common.StringToSql.convertString(franzoesisch) + ", franzoesisch_j_von="
        + de.knightsoft.common.StringToSql.convertNumber(franzoesischJvon) + ", franzoesisch_j_bis="
        + de.knightsoft.common.StringToSql.convertNumber(franzoesischJbis) + ", franzoesisch_s_von="
        + de.knightsoft.common.StringToSql.convertNumber(franzoesischSvon) + ", franzoesisch_s_bis="
        + de.knightsoft.common.StringToSql.convertNumber(franzoesischSbis) + ", geschichte="
        + de.knightsoft.common.StringToSql.convertString(geschichte) + ", geschichte_j_von="
        + de.knightsoft.common.StringToSql.convertNumber(geschichteJvon) + ", geschichte_j_bis="
        + de.knightsoft.common.StringToSql.convertNumber(geschichteJbis) + ", geschichte_s_von="
        + de.knightsoft.common.StringToSql.convertNumber(geschichteSvon) + ", geschichte_s_bis="
        + de.knightsoft.common.StringToSql.convertNumber(geschichteSbis) + ", griechisch="
        + de.knightsoft.common.StringToSql.convertString(griechisch) + ", griechisch_j_von="
        + de.knightsoft.common.StringToSql.convertNumber(griechischJvon) + ", griechisch_j_bis="
        + de.knightsoft.common.StringToSql.convertNumber(griechischJbis) + ", griechisch_s_von="
        + de.knightsoft.common.StringToSql.convertNumber(griechischSvon) + ", griechisch_s_bis="
        + de.knightsoft.common.StringToSql.convertNumber(griechischSbis) + ", informatik="
        + de.knightsoft.common.StringToSql.convertString(informatik) + ", informatik_j_von="
        + de.knightsoft.common.StringToSql.convertNumber(informatikJvon) + ", informatik_j_bis="
        + de.knightsoft.common.StringToSql.convertNumber(informatikJbis) + ", informatik_s_von="
        + de.knightsoft.common.StringToSql.convertNumber(informatikSvon) + ", informatik_s_bis="
        + de.knightsoft.common.StringToSql.convertNumber(informatikSbis) + ", latein="
        + de.knightsoft.common.StringToSql.convertString(latein) + ", latein_j_von="
        + de.knightsoft.common.StringToSql.convertNumber(lateinJvon) + ", latein_j_bis="
        + de.knightsoft.common.StringToSql.convertNumber(lateinJbis) + ", latein_s_von="
        + de.knightsoft.common.StringToSql.convertNumber(lateinSvon) + ", latein_s_bis="
        + de.knightsoft.common.StringToSql.convertNumber(lateinSbis) + ", maschineschreiben="
        + de.knightsoft.common.StringToSql.convertString(maschineschreiben) + ", maschineschreiben_j_von="
        + de.knightsoft.common.StringToSql.convertNumber(maschineschreibenJvon) + ", maschineschreiben_j_bis="
        + de.knightsoft.common.StringToSql.convertNumber(maschineschreibenJbis) + ", maschineschreiben_s_von="
        + de.knightsoft.common.StringToSql.convertNumber(maschineschreibenSvon) + ", maschineschreiben_s_bis="
        + de.knightsoft.common.StringToSql.convertNumber(maschineschreibenSbis) + ", mathematik="
        + de.knightsoft.common.StringToSql.convertString(mathematik) + ", mathematik_j_von="
        + de.knightsoft.common.StringToSql.convertNumber(mathematikJvon) + ", mathematik_j_bis="
        + de.knightsoft.common.StringToSql.convertNumber(mathematikJbis) + ", mathematik_s_von="
        + de.knightsoft.common.StringToSql.convertNumber(mathematikSvon) + ", mathematik_s_bis="
        + de.knightsoft.common.StringToSql.convertNumber(mathematikSbis) + ", physik="
        + de.knightsoft.common.StringToSql.convertString(physik) + ", physik_j_von="
        + de.knightsoft.common.StringToSql.convertNumber(physikJvon) + ", physik_j_bis="
        + de.knightsoft.common.StringToSql.convertNumber(physikJbis) + ", physik_s_von="
        + de.knightsoft.common.StringToSql.convertNumber(physikSvon) + ", physik_s_bis="
        + de.knightsoft.common.StringToSql.convertNumber(physikSbis) + ", sozialkunde="
        + de.knightsoft.common.StringToSql.convertString(sozialkunde) + ", sozialkunde_j_von="
        + de.knightsoft.common.StringToSql.convertNumber(sozialkundeJvon) + ", sozialkunde_j_bis="
        + de.knightsoft.common.StringToSql.convertNumber(sozialkundeJbis) + ", sozialkunde_s_von="
        + de.knightsoft.common.StringToSql.convertNumber(sozialkundeSvon) + ", sozialkunde_s_bis="
        + de.knightsoft.common.StringToSql.convertNumber(sozialkundeSbis) + ", stenografie="
        + de.knightsoft.common.StringToSql.convertString(stenografie) + ", stenografie_j_von="
        + de.knightsoft.common.StringToSql.convertNumber(stenografieJvon) + ", stenografie_j_bis="
        + de.knightsoft.common.StringToSql.convertNumber(stenografieJbis) + ", stenografie_s_von="
        + de.knightsoft.common.StringToSql.convertNumber(stenografieSvon) + ", stenografie_s_bis="
        + de.knightsoft.common.StringToSql.convertNumber(stenografieSbis) + ", weiteres_fach1="
        + de.knightsoft.common.StringToSql.convertString(weiteresFach1) + ", weiteres_fach1_j_von="
        + de.knightsoft.common.StringToSql.convertNumber(weiteresFach1Jvon) + ", weiteres_fach1_j_bis="
        + de.knightsoft.common.StringToSql.convertNumber(weiteresFach1Jbis) + ", weiteres_fach1_s_von="
        + de.knightsoft.common.StringToSql.convertNumber(weiteresFach1Svon) + ", weiteres_fach1_s_bis="
        + de.knightsoft.common.StringToSql.convertNumber(weiteresFach1Sbis) + ", weiteres_fach2="
        + de.knightsoft.common.StringToSql.convertString(weiteresFach1) + ", weiteres_fach2_j_von="
        + de.knightsoft.common.StringToSql.convertNumber(weiteresFach1Jvon) + ", weiteres_fach2_j_bis="
        + de.knightsoft.common.StringToSql.convertNumber(weiteresFach1Jbis) + ", weiteres_fach2_s_von="
        + de.knightsoft.common.StringToSql.convertNumber(weiteresFach1Svon) + ", weiteres_fach2_s_bis="
        + de.knightsoft.common.StringToSql.convertNumber(weiteresFach1Sbis) + ", weiteres_fach3="
        + de.knightsoft.common.StringToSql.convertString(weiteresFach1) + ", weiteres_fach3_j_von="
        + de.knightsoft.common.StringToSql.convertNumber(weiteresFach1Jvon) + ", weiteres_fach3_j_bis="
        + de.knightsoft.common.StringToSql.convertNumber(weiteresFach1Jbis) + ", weiteres_fach3_s_von="
        + de.knightsoft.common.StringToSql.convertNumber(weiteresFach1Svon) + ", weiteres_fach3_s_bis="
        + de.knightsoft.common.StringToSql.convertNumber(weiteresFach1Sbis);
  }

  /**
   * create html table.
   *
   * @param search search string
   * @return html table
   */
  public String htmlTabelle(final boolean search) {

    final String tabelleString1 = "<tr><td align=\"left\">Vorwahl:</td><td align=\"left\">" + vorwahl
        + "</td></tr>\n<tr><td align=\"left\">Geburtsjahr:</td><td align=\"left\">" + geburtsjahr + "</td></tr>\n";
    final StringBuilder tabelleString2 = new StringBuilder(512);

    if ("J".equals(biologie)) {
      tabelleString2.append("<tr><td align=\"left\">Biologie:</td><td align=\"left\">");
      if (StringUtils.isNotEmpty(biologieJvon)) {
        tabelleString2.append("Jahrgang von: ").append(biologieJvon).append(" bis: ").append(biologieJbis).append("   ");
      }

      if (StringUtils.isNotEmpty(biologieSvon)) {
        tabelleString2.append("Semester von: ").append(biologieSvon).append(" bis: ").append(biologieSbis);
      }
      tabelleString2.append("</td></tr>\n");
    }

    if ("J".equals(bwl)) {
      tabelleString2.append("<tr><td align=\"left\">BWL:</td><td align=\"left\">");
      if (StringUtils.isNotEmpty(bwlJvon)) {
        tabelleString2.append("Jahrgang von: ").append(bwlJvon).append(" bis: ").append(bwlJbis).append("   ");
      }

      if (StringUtils.isNotEmpty(bwlSvon)) {
        tabelleString2.append("Semester von: ").append(bwlSvon).append(" bis: ").append(bwlSbis);
      }
      tabelleString2.append("</td></tr>\n");
    }

    if ("J".equals(chemie)) {
      tabelleString2.append("<tr><td align=\"left\">Chemie:</td><td align=\"left\">");
      if (StringUtils.isNotEmpty(chemieJvon)) {
        tabelleString2.append("Jahrgang von: ").append(chemieJvon).append(" bis: ").append(chemieJbis).append("   ");
      }

      if (StringUtils.isNotEmpty(chemieSvon)) {
        tabelleString2.append("Semester von: ").append(chemieSvon).append(" bis: ").append(chemieSbis);
      }
      tabelleString2.append("</td></tr>\n");
    }

    if ("J".equals(deutsch)) {
      tabelleString2.append("<tr><td align=\"left\">Deutsch:</td><td align=\"left\">");
      if (StringUtils.isNotEmpty(deutschJvon)) {
        tabelleString2.append("Jahrgang von: ").append(deutschJvon).append(" bis: ").append(deutschJbis).append("   ");
      }

      if (StringUtils.isNotEmpty(deutschSvon)) {
        tabelleString2.append("Semester von: ").append(deutschSvon).append(" bis: ").append(deutschSbis);
      }
      tabelleString2.append("</td></tr>\n");
    }

    if ("J".equals(englisch)) {
      tabelleString2.append("<tr><td align=\"left\">Englisch:</td><td align=\"left\">");
      if (StringUtils.isNotEmpty(englischJvon)) {
        tabelleString2.append("Jahrgang von: ").append(englischJvon).append(" bis: ").append(englischJbis).append("   ");
      }

      if (StringUtils.isNotEmpty(englischSvon)) {
        tabelleString2.append("Semester von: ").append(englischSvon).append(" bis: ").append(englischSbis);
      }
      tabelleString2.append("</td></tr>\n");
    }

    if ("J".equals(erdkunde)) {
      tabelleString2.append("<tr><td align=\"left\">Erdkunde:</td><td align=\"left\">");
      if (StringUtils.isNotEmpty(erdkundeJvon)) {
        tabelleString2.append("Jahrgang von: ").append(erdkundeJvon).append(" bis: ").append(erdkundeJbis).append("   ");
      }

      if (StringUtils.isNotEmpty(erdkundeSvon)) {
        tabelleString2.append("Semester von: ").append(erdkundeSvon).append(" bis: ").append(erdkundeSbis);
      }
      tabelleString2.append("</td></tr>\n");
    }

    if ("J".equals(franzoesisch)) {
      tabelleString2.append("<tr><td align=\"left\">Franz&ouml;sisch:</td><td align=\"left\">");
      if (StringUtils.isNotEmpty(franzoesischJvon)) {
        tabelleString2.append("Jahrgang von: ").append(franzoesischJvon).append(" bis: ").append(franzoesischJbis)
            .append("   ");
      }

      if (StringUtils.isNotEmpty(franzoesischSvon)) {
        tabelleString2.append("Semester von: ").append(franzoesischSvon).append(" bis: ").append(franzoesischSbis);
      }
      tabelleString2.append("</td></tr>\n");
    }

    if ("J".equals(geschichte)) {
      tabelleString2.append("<tr><td align=\"left\">Geschichte:</td><td align=\"left\">");
      if (StringUtils.isNotEmpty(geschichteJvon)) {
        tabelleString2.append("Jahrgang von: ").append(geschichteJvon).append(" bis: ").append(geschichteJbis).append("   ");
      }

      if (StringUtils.isNotEmpty(geschichteSvon)) {
        tabelleString2.append("Semester von: ").append(geschichteSvon).append(" bis: ").append(geschichteSbis);
      }
      tabelleString2.append("</td></tr>\n");
    }

    if ("J".equals(griechisch)) {
      tabelleString2.append("<tr><td align=\"left\">Griechisch:</td><td align=\"left\">");
      if (StringUtils.isNotEmpty(griechischJvon)) {
        tabelleString2.append("Jahrgang von: ").append(griechischJvon).append(" bis: ").append(griechischJbis).append("   ");
      }

      if (StringUtils.isNotEmpty(griechischSvon)) {
        tabelleString2.append("Semester von: ").append(griechischSvon).append(" bis: ").append(griechischSbis);
      }
      tabelleString2.append("</td></tr>\n");
    }

    if ("J".equals(informatik)) {
      tabelleString2.append("<tr><td align=\"left\">Informatik:</td><td align=\"left\">");
      if (StringUtils.isNotEmpty(informatikJvon)) {
        tabelleString2.append("Jahrgang von: ").append(informatikJvon).append(" bis: ").append(informatikJbis).append("   ");
      }

      if (StringUtils.isNotEmpty(informatikSvon)) {
        tabelleString2.append("Semester von: ").append(informatikSvon).append(" bis: ").append(informatikSbis);
      }
      tabelleString2.append("</td></tr>\n");
    }

    if ("J".equals(latein)) {
      tabelleString2.append("<tr><td align=\"left\">Latein:</td><td align=\"left\">");
      if (StringUtils.isNotEmpty(lateinJvon)) {
        tabelleString2.append("Jahrgang von: ").append(lateinJvon).append(" bis: ").append(lateinJbis).append("   ");
      }

      if (StringUtils.isNotEmpty(lateinSvon)) {
        tabelleString2.append("Semester von: ").append(lateinSvon).append(" bis: ").append(lateinSbis);
      }
      tabelleString2.append("</td></tr>\n");
    }

    if ("J".equals(maschineschreiben)) {
      tabelleString2.append("<tr><td align=\"left\">Maschineschreiben:</td><td align=\"left\">");
      if (StringUtils.isNotEmpty(maschineschreibenJvon)) {
        tabelleString2.append("Jahrgang von: ").append(maschineschreibenJvon).append(" bis: ").append(maschineschreibenJbis)
            .append("   ");
      }

      if (StringUtils.isNotEmpty(maschineschreibenSvon)) {
        tabelleString2.append("Semester von: ").append(maschineschreibenSvon).append(" bis: ").append(maschineschreibenSbis);
      }
      tabelleString2.append("</td></tr>\n");
    }

    if ("J".equals(mathematik)) {
      tabelleString2.append("<tr><td align=\"left\">Mathematik:</td><td align=\"left\">");
      if (StringUtils.isNotEmpty(mathematikJvon)) {
        tabelleString2.append("Jahrgang von: ").append(mathematikJvon).append(" bis: ").append(mathematikJbis).append("   ");
      }

      if (StringUtils.isNotEmpty(mathematikSvon)) {
        tabelleString2.append("Semester von: ").append(mathematikSvon).append(" bis: ").append(mathematikSbis);
      }
      tabelleString2.append("</td></tr>\n");
    }

    if ("J".equals(physik)) {
      tabelleString2.append("<tr><td align=\"left\">Physik:</td><td align=\"left\">");
      if (StringUtils.isNotEmpty(physikJvon)) {
        tabelleString2.append("Jahrgang von: ").append(physikJvon).append(" bis: ").append(physikJbis).append("   ");
      }

      if (StringUtils.isNotEmpty(physikSvon)) {
        tabelleString2.append("Semester von: ").append(physikSvon).append(" bis: ").append(physikSbis);
      }
      tabelleString2.append("</td></tr>\n");
    }

    if ("J".equals(sozialkunde)) {
      tabelleString2.append("<tr><td align=\"left\">Sozialkunde:</td><td align=\"left\">");
      if (StringUtils.isNotEmpty(sozialkundeJvon)) {
        tabelleString2.append("Jahrgang von: ").append(sozialkundeJvon).append(" bis: ").append(sozialkundeJbis).append("   ");
      }

      if (StringUtils.isNotEmpty(sozialkundeSvon)) {
        tabelleString2.append("Semester von: ").append(sozialkundeSvon).append(" bis: ").append(sozialkundeSbis);
      }
      tabelleString2.append("</td></tr>\n");
    }

    if ("J".equals(stenografie)) {
      tabelleString2.append("<tr><td align=\"left\">Stenografie:</td><td align=\"left\">");
      if (StringUtils.isNotEmpty(stenografieJvon)) {
        tabelleString2.append("Jahrgang von: ").append(stenografieJvon).append(" bis: ").append(stenografieJbis).append("   ");
      }

      if (StringUtils.isNotEmpty(stenografieSvon)) {
        tabelleString2.append("Semester von: ").append(stenografieSvon).append(" bis: ").append(stenografieSbis);
      }
      tabelleString2.append("</td></tr>\n");
    }

    if (StringUtils.isNotEmpty(weiteresFach1)) {
      tabelleString2.append("<tr><td align=\"left\">").append(de.knightsoft.common.StringToHtml.convert(weiteresFach1))
          .append(":</td><td align=\"left\">");
      if (StringUtils.isNotEmpty(weiteresFach1Jvon)) {
        tabelleString2.append("Jahrgang von: ").append(weiteresFach1Jvon).append(" bis: ").append(weiteresFach1Jbis)
            .append("   ");
      }

      if (StringUtils.isNotEmpty(weiteresFach1Svon)) {
        tabelleString2.append("Semester von: ").append(weiteresFach1Svon).append(" bis: ").append(weiteresFach1Sbis);
      }
      tabelleString2.append("</td></tr>\n");
    }

    if (StringUtils.isNotEmpty(weiteresFach2)) {
      tabelleString2.append("<tr><td align=\"left\">").append(de.knightsoft.common.StringToHtml.convert(weiteresFach2))
          .append(":</td><td align=\"left\">");
      if (StringUtils.isNotEmpty(weiteresFach2Jvon)) {
        tabelleString2.append("Jahrgang von: ").append(weiteresFach2Jvon).append(" bis: ").append(weiteresFach2Jbis)
            .append("   ");
      }

      if (StringUtils.isNotEmpty(weiteresFach2Svon)) {
        tabelleString2.append("Semester von: ").append(weiteresFach2Svon).append(" bis: ").append(weiteresFach2Sbis);
      }
      tabelleString2.append("</td></tr>\n");
    }

    if (StringUtils.isNotEmpty(weiteresFach3)) {
      tabelleString2.append("<tr><td align=\"left\">").append(de.knightsoft.common.StringToHtml.convert(weiteresFach3))
          .append(":</td><td align=\"left\">");
      if (StringUtils.isNotEmpty(weiteresFach3Jvon)) {
        tabelleString2.append("Jahrgang von: ").append(weiteresFach3Jvon).append(" bis: ").append(weiteresFach3Jbis)
            .append("   ");
      }

      if (StringUtils.isNotEmpty(weiteresFach3Svon)) {
        tabelleString2.append("Semester von: ").append(weiteresFach3Svon).append(" bis: ").append(weiteresFach3Sbis);
      }
      tabelleString2.append("</td></tr>\n");
    }

    if (StringUtils.isNotEmpty(preis)) {
      tabelleString2.append("<tr valign=\"top\"><td align=\"left\">Preisvorgaben:</td><td align=\"left\">")
          .append(de.knightsoft.common.StringToHtml.convert(preis)).append("</td></tr>\n");
    }

    return this.htmlTabelle(tabelleString1, tabelleString2.toString(), search, "Nachhilfe");
  }

  @Override
  public String htmlHidden() {

    return super.htmlHidden() + "<input type=\"hidden\" name=\"Vorwahl\" value=\""
        + de.knightsoft.common.StringToHtml.convert(vorwahl) + "\">\n" + "<input type=\"hidden\" name=\"Geburtsjahr\" value=\""
        + de.knightsoft.common.StringToHtml.convert(geburtsjahr) + "\">\n" + "<input type=\"hidden\" name=\"Biologie\" value=\""
        + de.knightsoft.common.StringToHtml.convert(biologie) + "\">\n"
        + "<input type=\"hidden\" name=\"Biologie_J_von\" value=\"" + de.knightsoft.common.StringToHtml.convert(biologieJvon)
        + "\">\n" + "<input type=\"hidden\" name=\"Biologie_J_bis\" value=\""
        + de.knightsoft.common.StringToHtml.convert(biologieJbis) + "\">\n"
        + "<input type=\"hidden\" name=\"Biologie_S_von\" value=\"" + de.knightsoft.common.StringToHtml.convert(biologieSvon)
        + "\">\n" + "<input type=\"hidden\" name=\"Biologie_S_bis\" value=\""
        + de.knightsoft.common.StringToHtml.convert(biologieSbis) + "\">\n" + "<input type=\"hidden\" name=\"BWL\" value=\""
        + de.knightsoft.common.StringToHtml.convert(bwl) + "\">\n" + "<input type=\"hidden\" name=\"BWL_J_von\" value=\""
        + de.knightsoft.common.StringToHtml.convert(bwlJvon) + "\">\n" + "<input type=\"hidden\" name=\"BWL_J_bis\" value=\""
        + de.knightsoft.common.StringToHtml.convert(bwlJbis) + "\">\n" + "<input type=\"hidden\" name=\"BWL_S_von\" value=\""
        + de.knightsoft.common.StringToHtml.convert(bwlSvon) + "\">\n" + "<input type=\"hidden\" name=\"BWL_S_bis\" value=\""
        + de.knightsoft.common.StringToHtml.convert(bwlSbis) + "\">\n" + "<input type=\"hidden\" name=\"Chemie\" value=\""
        + de.knightsoft.common.StringToHtml.convert(chemie) + "\">\n" + "<input type=\"hidden\" name=\"Chemie_J_von\" value=\""
        + de.knightsoft.common.StringToHtml.convert(chemieJvon) + "\">\n"
        + "<input type=\"hidden\" name=\"Chemie_J_bis\" value=\"" + de.knightsoft.common.StringToHtml.convert(chemieJbis)
        + "\">\n" + "<input type=\"hidden\" name=\"Chemie_S_von\" value=\""
        + de.knightsoft.common.StringToHtml.convert(chemieSvon) + "\">\n"
        + "<input type=\"hidden\" name=\"Chemie_S_bis\" value=\"" + de.knightsoft.common.StringToHtml.convert(chemieSbis)
        + "\">\n" + "<input type=\"hidden\" name=\"Deutsch\" value=\"" + de.knightsoft.common.StringToHtml.convert(deutsch)
        + "\">\n" + "<input type=\"hidden\" name=\"Deutsch_J_von\" value=\""
        + de.knightsoft.common.StringToHtml.convert(deutschJvon) + "\">\n"
        + "<input type=\"hidden\" name=\"Deutsch_J_bis\" value=\"" + de.knightsoft.common.StringToHtml.convert(deutschJbis)
        + "\">\n" + "<input type=\"hidden\" name=\"Deutsch_S_von\" value=\""
        + de.knightsoft.common.StringToHtml.convert(deutschSvon) + "\">\n"
        + "<input type=\"hidden\" name=\"Deutsch_S_bis\" value=\"" + de.knightsoft.common.StringToHtml.convert(deutschSbis)
        + "\">\n" + "<input type=\"hidden\" name=\"Englisch\" value=\"" + de.knightsoft.common.StringToHtml.convert(englisch)
        + "\">\n" + "<input type=\"hidden\" name=\"Englisch_J_von\" value=\""
        + de.knightsoft.common.StringToHtml.convert(englischJvon) + "\">\n"
        + "<input type=\"hidden\" name=\"Englisch_J_bis\" value=\"" + de.knightsoft.common.StringToHtml.convert(englischJbis)
        + "\">\n" + "<input type=\"hidden\" name=\"Englisch_S_von\" value=\""
        + de.knightsoft.common.StringToHtml.convert(englischSvon) + "\">\n"
        + "<input type=\"hidden\" name=\"Englisch_S_bis\" value=\"" + de.knightsoft.common.StringToHtml.convert(englischSbis)
        + "\">\n" + "<input type=\"hidden\" name=\"Erdkunde\" value=\"" + de.knightsoft.common.StringToHtml.convert(erdkunde)
        + "\">\n" + "<input type=\"hidden\" name=\"Erdkunde_J_von\" value=\""
        + de.knightsoft.common.StringToHtml.convert(erdkundeJvon) + "\">\n"
        + "<input type=\"hidden\" name=\"Erdkunde_J_bis\" value=\"" + de.knightsoft.common.StringToHtml.convert(erdkundeJbis)
        + "\">\n" + "<input type=\"hidden\" name=\"Erdkunde_S_von\" value=\""
        + de.knightsoft.common.StringToHtml.convert(erdkundeSvon) + "\">\n"
        + "<input type=\"hidden\" name=\"Erdkunde_S_bis\" value=\"" + de.knightsoft.common.StringToHtml.convert(erdkundeSbis)
        + "\">\n" + "<input type=\"hidden\" name=\"Franzoesisch\" value=\""
        + de.knightsoft.common.StringToHtml.convert(franzoesisch) + "\">\n"
        + "<input type=\"hidden\" name=\"Franzoesisch_J_von\" value=\""
        + de.knightsoft.common.StringToHtml.convert(franzoesischJvon) + "\">\n"
        + "<input type=\"hidden\" name=\"Franzoesisch_J_bis\" value=\""
        + de.knightsoft.common.StringToHtml.convert(franzoesischJbis) + "\">\n"
        + "<input type=\"hidden\" name=\"Franzoesisch_S_von\" value=\""
        + de.knightsoft.common.StringToHtml.convert(franzoesischSvon) + "\">\n"
        + "<input type=\"hidden\" name=\"Franzoesisch_S_bis\" value=\""
        + de.knightsoft.common.StringToHtml.convert(franzoesischSbis) + "\">\n"
        + "<input type=\"hidden\" name=\"Geschichte\" value=\"" + de.knightsoft.common.StringToHtml.convert(geschichte)
        + "\">\n" + "<input type=\"hidden\" name=\"Geschichte_J_von\" value=\""
        + de.knightsoft.common.StringToHtml.convert(geschichteJvon) + "\">\n"
        + "<input type=\"hidden\" name=\"Geschichte_J_bis\" value=\""
        + de.knightsoft.common.StringToHtml.convert(geschichteJbis) + "\">\n"
        + "<input type=\"hidden\" name=\"Geschichte_S_von\" value=\""
        + de.knightsoft.common.StringToHtml.convert(geschichteSvon) + "\">\n"
        + "<input type=\"hidden\" name=\"Geschichte_S_bis\" value=\""
        + de.knightsoft.common.StringToHtml.convert(geschichteSbis) + "\">\n"
        + "<input type=\"hidden\" name=\"Griechisch\" value=\"" + de.knightsoft.common.StringToHtml.convert(griechisch)
        + "\">\n" + "<input type=\"hidden\" name=\"Griechisch_J_von\" value=\""
        + de.knightsoft.common.StringToHtml.convert(griechischJvon) + "\">\n"
        + "<input type=\"hidden\" name=\"Griechisch_J_bis\" value=\""
        + de.knightsoft.common.StringToHtml.convert(griechischJbis) + "\">\n"
        + "<input type=\"hidden\" name=\"Griechisch_S_von\" value=\""
        + de.knightsoft.common.StringToHtml.convert(griechischSvon) + "\">\n"
        + "<input type=\"hidden\" name=\"Griechisch_S_bis\" value=\""
        + de.knightsoft.common.StringToHtml.convert(griechischSbis) + "\">\n"
        + "<input type=\"hidden\" name=\"Informatik\" value=\"" + de.knightsoft.common.StringToHtml.convert(informatik)
        + "\">\n" + "<input type=\"hidden\" name=\"Informatik_J_von\" value=\""
        + de.knightsoft.common.StringToHtml.convert(informatikJvon) + "\">\n"
        + "<input type=\"hidden\" name=\"Informatik_J_bis\" value=\""
        + de.knightsoft.common.StringToHtml.convert(informatikJbis) + "\">\n"
        + "<input type=\"hidden\" name=\"Informatik_S_von\" value=\""
        + de.knightsoft.common.StringToHtml.convert(informatikSvon) + "\">\n"
        + "<input type=\"hidden\" name=\"Informatik_S_bis\" value=\""
        + de.knightsoft.common.StringToHtml.convert(informatikSbis) + "\">\n"
        + "<input type=\"hidden\" name=\"Latein\" value=\"" + de.knightsoft.common.StringToHtml.convert(latein) + "\">\n"
        + "<input type=\"hidden\" name=\"Latein_J_von\" value=\"" + de.knightsoft.common.StringToHtml.convert(lateinJvon)
        + "\">\n" + "<input type=\"hidden\" name=\"Latein_J_bis\" value=\""
        + de.knightsoft.common.StringToHtml.convert(lateinJbis) + "\">\n"
        + "<input type=\"hidden\" name=\"Latein_S_von\" value=\"" + de.knightsoft.common.StringToHtml.convert(lateinSvon)
        + "\">\n" + "<input type=\"hidden\" name=\"Latein_S_bis\" value=\""
        + de.knightsoft.common.StringToHtml.convert(lateinSbis) + "\">\n"
        + "<input type=\"hidden\" name=\"Maschineschreiben\" value=\""
        + de.knightsoft.common.StringToHtml.convert(maschineschreiben) + "\">\n"
        + "<input type=\"hidden\" name=\"Maschineschreiben_J_von\" value=\""
        + de.knightsoft.common.StringToHtml.convert(maschineschreibenJvon) + "\">\n"
        + "<input type=\"hidden\" name=\"Maschineschreiben_J_bis\" value=\""
        + de.knightsoft.common.StringToHtml.convert(maschineschreibenJbis) + "\">\n"
        + "<input type=\"hidden\" name=\"Maschineschreiben_S_von\" value=\""
        + de.knightsoft.common.StringToHtml.convert(maschineschreibenSvon) + "\">\n"
        + "<input type=\"hidden\" name=\"Maschineschreiben_S_bis\" value=\""
        + de.knightsoft.common.StringToHtml.convert(maschineschreibenSbis) + "\">\n"
        + "<input type=\"hidden\" name=\"Mathematik\" value=\"" + de.knightsoft.common.StringToHtml.convert(mathematik)
        + "\">\n" + "<input type=\"hidden\" name=\"Mathematik_J_von\" value=\""
        + de.knightsoft.common.StringToHtml.convert(mathematikJvon) + "\">\n"
        + "<input type=\"hidden\" name=\"Mathematik_J_bis\" value=\""
        + de.knightsoft.common.StringToHtml.convert(mathematikJbis) + "\">\n"
        + "<input type=\"hidden\" name=\"Mathematik_S_von\" value=\""
        + de.knightsoft.common.StringToHtml.convert(mathematikSvon) + "\">\n"
        + "<input type=\"hidden\" name=\"Mathematik_S_bis\" value=\""
        + de.knightsoft.common.StringToHtml.convert(mathematikSbis) + "\">\n"
        + "<input type=\"hidden\" name=\"Physik\" value=\"" + de.knightsoft.common.StringToHtml.convert(physik) + "\">\n"
        + "<input type=\"hidden\" name=\"Physik_J_von\" value=\"" + de.knightsoft.common.StringToHtml.convert(physikJvon)
        + "\">\n" + "<input type=\"hidden\" name=\"Physik_J_bis\" value=\""
        + de.knightsoft.common.StringToHtml.convert(physikJbis) + "\">\n"
        + "<input type=\"hidden\" name=\"Physik_S_von\" value=\"" + de.knightsoft.common.StringToHtml.convert(physikSvon)
        + "\">\n" + "<input type=\"hidden\" name=\"Physik_S_bis\" value=\""
        + de.knightsoft.common.StringToHtml.convert(physikSbis) + "\">\n"
        + "<input type=\"hidden\" name=\"Sozialkunde\" value=\"" + de.knightsoft.common.StringToHtml.convert(sozialkunde)
        + "\">\n" + "<input type=\"hidden\" name=\"Sozialkunde_J_von\" value=\""
        + de.knightsoft.common.StringToHtml.convert(sozialkundeJvon) + "\">\n"
        + "<input type=\"hidden\" name=\"Sozialkunde_J_bis\" value=\""
        + de.knightsoft.common.StringToHtml.convert(sozialkundeJbis) + "\">\n"
        + "<input type=\"hidden\" name=\"Sozialkunde_S_von\" value=\""
        + de.knightsoft.common.StringToHtml.convert(sozialkundeSvon) + "\">\n"
        + "<input type=\"hidden\" name=\"Sozialkunde_S_bis\" value=\""
        + de.knightsoft.common.StringToHtml.convert(sozialkundeSbis) + "\">\n"
        + "<input type=\"hidden\" name=\"Stenografie\" value=\"" + de.knightsoft.common.StringToHtml.convert(stenografie)
        + "\">\n" + "<input type=\"hidden\" name=\"Stenografie_J_von\" value=\""
        + de.knightsoft.common.StringToHtml.convert(stenografieJvon) + "\">\n"
        + "<input type=\"hidden\" name=\"Stenografie_J_bis\" value=\""
        + de.knightsoft.common.StringToHtml.convert(stenografieJbis) + "\">\n"
        + "<input type=\"hidden\" name=\"Stenografie_S_von\" value=\""
        + de.knightsoft.common.StringToHtml.convert(stenografieSvon) + "\">\n"
        + "<input type=\"hidden\" name=\"Stenografie_S_bis\" value=\""
        + de.knightsoft.common.StringToHtml.convert(stenografieSbis) + "\">\n"
        + "<input type=\"hidden\" name=\"weiteres_fach1\" value=\"" + de.knightsoft.common.StringToHtml.convert(weiteresFach1)
        + "\">\n" + "<input type=\"hidden\" name=\"weiteres_fach1_J_von\" value=\""
        + de.knightsoft.common.StringToHtml.convert(weiteresFach1Jvon) + "\">\n"
        + "<input type=\"hidden\" name=\"weiteres_fach1_J_bis\" value=\""
        + de.knightsoft.common.StringToHtml.convert(weiteresFach1Jbis) + "\">\n"
        + "<input type=\"hidden\" name=\"weiteres_fach1_S_von\" value=\""
        + de.knightsoft.common.StringToHtml.convert(weiteresFach1Svon) + "\">\n"
        + "<input type=\"hidden\" name=\"weiteres_fach1_S_bis\" value=\""
        + de.knightsoft.common.StringToHtml.convert(weiteresFach1Sbis) + "\">\n"
        + "<input type=\"hidden\" name=\"weiteres_fach2\" value=\"" + de.knightsoft.common.StringToHtml.convert(weiteresFach2)
        + "\">\n" + "<input type=\"hidden\" name=\"weiteres_fach2_J_von\" value=\""
        + de.knightsoft.common.StringToHtml.convert(weiteresFach2Jvon) + "\">\n"
        + "<input type=\"hidden\" name=\"weiteres_fach2_J_bis\" value=\""
        + de.knightsoft.common.StringToHtml.convert(weiteresFach2Jbis) + "\">\n"
        + "<input type=\"hidden\" name=\"weiteres_fach2_S_von\" value=\""
        + de.knightsoft.common.StringToHtml.convert(weiteresFach2Svon) + "\">\n"
        + "<input type=\"hidden\" name=\"weiteres_fach2_S_bis\" value=\""
        + de.knightsoft.common.StringToHtml.convert(weiteresFach2Sbis) + "\">\n"
        + "<input type=\"hidden\" name=\"weiteres_fach3\" value=\"" + de.knightsoft.common.StringToHtml.convert(weiteresFach3)
        + "\">\n" + "<input type=\"hidden\" name=\"weiteres_fach3_J_von\" value=\""
        + de.knightsoft.common.StringToHtml.convert(weiteresFach3Jvon) + "\">\n"
        + "<input type=\"hidden\" name=\"weiteres_fach3_J_bis\" value=\""
        + de.knightsoft.common.StringToHtml.convert(weiteresFach3Jbis) + "\">\n"
        + "<input type=\"hidden\" name=\"weiteres_fach3_S_von\" value=\""
        + de.knightsoft.common.StringToHtml.convert(weiteresFach3Svon) + "\">\n"
        + "<input type=\"hidden\" name=\"weiteres_fach3_S_bis\" value=\""
        + de.knightsoft.common.StringToHtml.convert(weiteresFach3Sbis) + "\">\n"
        + "<input type=\"hidden\" name=\"Preis\" value=\"" + de.knightsoft.common.StringToHtml.convert(preis) + "\">\n";
  }
}
