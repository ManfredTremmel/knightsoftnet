/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.knightsoftnet.assistance;

import de.knightsoft.common.AbstractVisualDb;
import de.knightsoft.knightsoftnet.common.KnConst;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * The <code>Search</code> class is based on the abstract class de.knightsoft.common.AbstractVisualDb and implements the search
 * function for the KnightSoft Nachhilfe-Vermittlung. It's no longer a real servlet, only a part of one.
 *
 *
 * @author Manfred Tremmel
 * @version 2.0.0, 19.08.2006
 */
public class Search extends de.knightsoft.common.AbstractVisualDb {

  private static final String SEARCH_SQL_ONE = //
      "SELECT     * " //
          + "FROM       KnightSoft_Nachhilfe " //
          + "WHERE      zaehler = ? " //
          + " AND       eintrag_gesperrt = 'N' ";

  /**
   * Constructor
   *
   * @param thisDatabase Connection to database
   * @param servletname Name of the Servlet
   * @exception SQLException if a database access error occurs
   * @exception de.knightsoft.common.TextException to give out a text message
   * @see de.knightsoft.common.TextException
   */
  public Search(final Connection thisDatabase, final String servletname)
      throws java.sql.SQLException, de.knightsoft.common.TextException {
    super(thisDatabase, KnConst.HTML_BASE, servletname, KnConst.GIF_URL + "16x16/find.png", "Suchen", "Suchen", null, null, 0,
        null);
  }

  /**
   * The Method <code>SuchergebnisUebersicht</code> generates the html output of the search overview.
   *
   * @param res HttpServletResponse from the Servlet
   * @param result ResultSet to get the entries from
   * @param land country code
   * @param bietesuche search for "Bieter" or "Sucher"
   * @param vorwahl first part of the phone number
   * @param fach subject
   * @param fachAlter alternativ subject selection
   * @param jahrSemSel semester or class
   * @param jahrSem year of semester or class
   * @return HTML code of the formular
   */
  private String suchergebnisUebersicht(final HttpServletResponse res, final ResultSet result, final String land,
      final String bietesuche, final String vorwahl, final String fach, final String fachAlter, final String jahrSemSel,
      final String jahrSem) throws SQLException {
    int anzahlGefunden = 0;
    String emailText = StringUtils.EMPTY;
    final StringBuilder inhalt = new StringBuilder(2500);

    inhalt.append("        <div style=\"text-align:center;\">\n");

    while (result.next()) {
      // E-Mail Anschrift aufbereiten
      if (result.getString("geschlecht").equals("W")) {
        emailText = "Sehr geehrte Frau " + result.getString("name") + ",";
      } else {
        emailText = "Sehr geehrter Herr " + result.getString("name") + ",";
      }
      if (anzahlGefunden == 0) {
        inhalt.append("        <div style=\"text-align:center;\">\n"
            + "        <table style=\"margin-left:auto; margin-right:auto;\" border=\"0\" "
            + "cellspacing=\"5\" cellpadding=\"0\">\n" + "            <tr style=\"background-color:#CCCCCC;\">\n"
            + "                <th style=\"text-align:left;\">Name<BR>E-Mail</th>\n"
            + "                <th style=\"text-align:left;\">Strasse<BR>Ort</th>\n"
            + "                <th style=\"text-align:left;\">Telefon<BR>&nbsp;</th>\n"
            + "                <th style=\"text-align:left;\">Details<BR>&nbsp;</th>\n" + "            </tr>\n");
      }

      if (anzahlGefunden % 2 == 0) {
        inhalt.append("            <tr style=\"background-color:#FFFFFF;\">\n");
      } else {
        inhalt.append("            <tr style=\"background-color:#CCCCCC;\">\n");
      }
      inhalt.append("                <td style=\"text-align:left;\">"
          + de.knightsoft.common.StringToHtml.convert(result.getString("name")) + ", "
          + de.knightsoft.common.StringToHtml.convert(result.getString("vorname")) + "<br>" + "<a href=\"MAILTO:"
          + result.getString("email") + "?subject=Anfrage bezueglich Ihres Nachhilfe-Angebots im KnightSoft-Net" + "&amp;body="
          + emailText + "\"> " + de.knightsoft.common.StringToHtml.convert(result.getString("email")) + "</a></td>\n"
          + "                <td style=\"text-align:left;\">"
          + de.knightsoft.common.StringToHtml.convert(result.getString("strasse")) + "<br>" + result.getString("plz") + " "
          + de.knightsoft.common.StringToHtml.convert(result.getString("ort")) + "</td>\n");
      if (result.getString("telefonnummer2") == null) {
        inhalt.append("                <td style=\"text-align:left;\">"
            + de.knightsoft.common.StringToHtml.convert(result.getString("telefonnummer1")) + "<br>&nbsp;</td>\n");
      } else {
        inhalt.append("                <td style=\"text-align:left;\">"
            + de.knightsoft.common.StringToHtml.convert(result.getString("telefonnummer1")) + "<br>"
            + de.knightsoft.common.StringToHtml.convert(result.getString("telefonnummer2")) + "</td>\n");
      }
      inhalt.append("                <td style=\"text-align:left;\">\n" + "                    <form action=\""
          + res.encodeURL(KnConst.HTML_BASE + "Nachhilfe/suchen.html")
          + "\" method=\"post\" enctype=\"application/x-www-form-urlencoded; charset=utf-8\" " + "accept-charset=\"utf-8\">\n"
          + "                        <p>\n" + "                            <input type=\"hidden\" name=\"Stufe\" value=\"3\">\n"
          + "                            <input type=\"hidden\" name=\"land\" value=\""
          + de.knightsoft.common.StringToHtml.convert(land) + "\">\n"
          + "                            <input type=\"hidden\" name=\"bietesuche\" value=\""
          + de.knightsoft.common.StringToHtml.convert(bietesuche) + "\">\n"
          + "                            <input type=\"hidden\" name=\"Vorwahl\" value=\""
          + de.knightsoft.common.StringToHtml.convert(vorwahl) + "\">\n"
          + "                            <input type=\"hidden\" name=\"Fach\" value=\""
          + de.knightsoft.common.StringToHtml.convert(fach) + "\">\n"
          + "                            <input type=\"hidden\" name=\"FachAlter\" value=\""
          + de.knightsoft.common.StringToHtml.convert(fachAlter) + "\">\n"
          + "                            <input type=\"hidden\" name=\"JahrSemSel\" value=\""
          + de.knightsoft.common.StringToHtml.convert(jahrSemSel) + "\">\n"
          + "                            <input type=\"hidden\" name=\"JahrSem\" value=\""
          + de.knightsoft.common.StringToHtml.convert(jahrSem) + "\">\n"
          + "                            <input type=\"hidden\" name=\"detailnr\" value=\""
          + de.knightsoft.common.StringToHtml.convert(result.getString("zaehler")) + "\">\n"
          + "                            <input type=\"submit\" name=\"abschicken\" " + "value=\"Details\">\n"
          + "                        </p>\n" + "                    </form>\n" + "                </td>\n"
          + "            </tr>\n");

      anzahlGefunden++;
    }

    switch (anzahlGefunden) {
      case 0:
        inhalt.append("        <p>Es wurde leider kein Nachhilfeangebot gefunden.</p>\n");
        break;
      case 1:
        inhalt.append("        </table>\n" + "        </div>\n" + "        <p>Es wurde ein Nachhilfeangebot gefunden.</p>\n");
        break;
      default:
        inhalt.append("        </table>\n" + "        </div>\n" + "        <p>Es wurden " + anzahlGefunden
            + " Nachhilfeangebote gefunden.</p>\n");
        break;
    }

    inhalt.append("        <p><a href=\"" + res.encodeURL(KnConst.HTML_BASE + "Nachhilfe/suchen.html")
        + "\">Erneute Suche</a></p>\n" + "        </div>\n");

    return inhalt.toString();
  }

  /**
   * The Method <code>SuchergebnisDetail</code> generates the html output of the detailed search.
   *
   * @param res HttpServletResponse from the Servlet
   * @param nachhilfeDaten Nachhilfe data from the database
   * @param land country code
   * @param bietesuche search for "Bieter" or "Sucher"
   * @param vorwahl first part of the phone number
   * @param fach subject
   * @param fachAlter alternativ subject selection
   * @param jahrSemSel semester or class
   * @param jahrSem year of semester or class
   * @return HTML code of the formular
   */
  private String suchergebnisDetail(final HttpServletResponse res,
      final de.knightsoft.knightsoftnet.assistance.AssistCheck nachhilfeDaten, final String land, final String bietesuche,
      final String vorwahl, final String fach, final String fachAlter, final String jahrSemSel, final String jahrSem) {
    return "        <div style=\"text-align:center;\">\n" + "        <form action=\""
        + res.encodeURL(KnConst.HTML_BASE + "Nachhilfe/suchen.html")
        + "\" method=\"post\" enctype=\"application/x-www-form-urlencoded; charset=utf-8\" " + "accept-charset=\"utf-8\">\n"
        + "            <p><input type=\"hidden\" name=\"Stufe\" value=\"1\">\n"
        + "            <input type=\"hidden\" name=\"land\" value=\"" + de.knightsoft.common.StringToHtml.convert(land)
        + "\">\n" + "            <input type=\"hidden\" name=\"bietesuche\" value=\""
        + de.knightsoft.common.StringToHtml.convert(bietesuche) + "\">\n"
        + "            <input type=\"hidden\" name=\"Vorwahl\" value=\"" + de.knightsoft.common.StringToHtml.convert(vorwahl)
        + "\">\n" + "            <input type=\"hidden\" name=\"Fach\" value=\""
        + de.knightsoft.common.StringToHtml.convert(fach) + "\">\n"
        + "            <input type=\"hidden\" name=\"FachAlter\" value=\""
        + de.knightsoft.common.StringToHtml.convert(fachAlter) + "\">\n"
        + "            <input type=\"hidden\" name=\"JahrSemSel\" value=\""
        + de.knightsoft.common.StringToHtml.convert(jahrSemSel) + "\">\n"
        + "            <input type=\"hidden\" name=\"JahrSem\" value=\"" + de.knightsoft.common.StringToHtml.convert(jahrSem)
        + "\"></p>\n" + nachhilfeDaten.htmlTabelle(true)
        + "        <p><input type=\"submit\" name=\"submit\" value=\"Zur&uuml;ck\"></p>\n" + "        </form>\n"
        + "        </div>\n";
  }

  /**
   * The Method <code>verarbeitungSucheKompakt</code> is used for a simple search.
   *
   * @param res HttpServletResponse from the Servlet
   * @param land country code
   * @param bietesuche search for "Bieter" or "Sucher"
   * @param vorwahl first part of the phone number
   * @param fach subject
   * @param fachAlter alternativ subject selection
   * @param jahrSemSel semester or class
   * @param jahrSem year of semester or class
   * @return HTML code of the formular
   */
  private String verarbeitungSucheKompakt(final HttpServletResponse res, final String land, final String bietesuche,
      final String vorwahl, final String fach, final String fachAlter, final String jahrSemSel, final String jahrSem)
      throws de.knightsoft.common.TextException {
    String fachmerker = null;
    if (vorwahl.length() < 2) {
      throw new de.knightsoft.common.TextException("Die Vorwahl ist nicht korrekt gefüllt, eine Suche ist nicht möglich!");
    }

    if ("bitte wählen Sie".equals(fach) && StringUtils.isEmpty(fachAlter)
        || "anderes Fach".equals(fach) && StringUtils.isEmpty(fachAlter)) {
      throw new de.knightsoft.common.TextException("Es wurde kein Fach vorgegeben, eine Suche ist nicht möglich!");
    }

    if (StringUtils.isNotEmpty(jahrSem) && !StringUtils.isNumeric(jahrSem)) {
      throw new de.knightsoft.common.TextException(
          "Das gewählte " + jahrSemSel + " ist nicht nummerisch, eine Suche ist nicht möglich!");
    }

    final StringBuilder sqlString = new StringBuilder(256);

    sqlString.append("SELECT * FROM KnightSoft_Nachhilfe WHERE land=" + de.knightsoft.common.StringToSql.convertString(land)
        + " AND bietesuche=" + de.knightsoft.common.StringToSql.convertString(bietesuche)
        + " AND eintrag_gesperrt='N' AND (vorwahl='alle' OR "
        + de.knightsoft.common.StringToSql.searchString("vorwahl", vorwahl) + ")");

    if ("anderes Fach".equals(fach) || "bitte wählen Sie".equals(fach)) {
      fachmerker = null;
      switch (fachAlter.toLowerCase()) {
        case "biologie":
        case "bio":
          fachmerker = "biologie";
          break;
        case "bwl":
        case "betriebswirtschaftslehre":
          fachmerker = "bwl";
          break;
        case "deutsch":
          fachmerker = "deutsch";
          break;
        case "englisch":
        case "english":
          fachmerker = "englisch";
          break;
        case "erdkunde":
          fachmerker = "erdkunde";
          break;
        case "französisch":
        case "franzoesisch":
          fachmerker = "franzoesisch";
          break;
        case "geschichte":
          fachmerker = "geschichte";
          break;
        case "griechisch":
          fachmerker = "griechisch";
          break;
        case "informatik":
        case "edv":
        case "computer":
        case "info":
          fachmerker = "informatik";
          break;
        case "latein":
        case "lateinisch":
          fachmerker = "latein";
          break;
        case "maschineschreiben":
        case "maschinenschreiben":
        case "maschine":
        case "schreibmaschine":
          fachmerker = "mschineschreiben";
          break;
        case "mathematik":
        case "mathe":
        case "rechnen":
          fachmerker = "mathematik";
          break;
        case "physik":
          fachmerker = "physik";
          break;
        case "sozialkunde":
        case "sozi":
          fachmerker = "sozialkunde";
          break;
        case "stenografie":
        case "stenographie":
        case "steno":
          fachmerker = "stenografie";
          break;
        default:
          break;
      }

      if (StringUtils.isEmpty(jahrSem)) {
        if (fachmerker == null) {
          sqlString.append(" AND (");
        } else {
          sqlString.append(" AND (" + fachmerker + "='J' OR ");
        }
        sqlString.append(de.knightsoft.common.StringToSql.searchString("weiteres_fach1", "*" + fachAlter + "*") + " OR "
            + de.knightsoft.common.StringToSql.searchString("weiteres_fach2", "*" + fachAlter + "*") + " OR "
            + de.knightsoft.common.StringToSql.searchString("weiteres_fach3", "*" + fachAlter + "*") + ")");
      } else {
        if ("Jahrgang:".equals(jahrSemSel)) {
          if (fachmerker == null) {
            sqlString.append(" AND (");
          } else {
            sqlString.append(" AND ((" + fachmerker + "='J' AND " + fachmerker + "_j_von <= " + jahrSem + " AND " + fachmerker
                + "_j_bis >= " + jahrSem + ") OR ");
          }
          sqlString.append("(" + de.knightsoft.common.StringToSql.searchString("weiteres_fach1", "*" + fachAlter + "*")
              + " AND weiteres_fach1_j_von <= " + jahrSem + " AND weiteres_fach1_j_bis >= " + jahrSem + ") OR " + "("
              + de.knightsoft.common.StringToSql.searchString("weiteres_fach2", "*" + fachAlter + "*")
              + " AND weiteres_fach2_j_von <= " + jahrSem + " AND weiteres_fach2_j_bis >= " + jahrSem + ") OR " + "("
              + de.knightsoft.common.StringToSql.searchString("weiteres_fach3", "*" + fachAlter + "*")
              + " AND weiteres_fach3_j_von <= " + jahrSem + " AND weiteres_fach3_j_bis >= " + jahrSem + ") " + ")");
        } else {
          if (fachmerker == null) {
            sqlString.append(" AND (");
          } else {
            sqlString.append(" AND ((" + fachmerker + "='J' AND " + fachmerker + "_s_von <= " + jahrSem + " AND " + fachmerker
                + "_s_bis >= " + jahrSem + ") OR ");
          }
          sqlString.append("(" + de.knightsoft.common.StringToSql.searchString("weiteres_fach1", "*" + fachAlter + "*")
              + " AND weiteres_fach1_s_von <= " + jahrSem + " AND weiteres_fach1_s_bis >= " + jahrSem + ") OR " + "("
              + de.knightsoft.common.StringToSql.searchString("weiteres_fach2", "*" + fachAlter + "*")
              + " AND weiteres_fach2_s_von <= " + jahrSem + " AND weiteres_fach2_s_bis >= " + jahrSem + ") OR " + "("
              + de.knightsoft.common.StringToSql.searchString("weiteres_fach3", "*" + fachAlter + "*")
              + " AND weiteres_fach3_s_von <= " + jahrSem + " AND weiteres_fach3_s_bis >= " + jahrSem + ") " + ")");
        }
      }
    } else {
      if ("Französisch".equals(fach)) {
        fachmerker = "franzoesisch";
      } else {
        fachmerker = fach.toLowerCase();
      }

      sqlString.append(" AND ").append(fachmerker).append("='J'");
      if (StringUtils.isNotEmpty(jahrSem)) {
        if ("Jahrgang:".equals(jahrSemSel)) {
          sqlString.append(" AND ").append(fachmerker).append("_j_von <= ").append(jahrSem).append(" AND ").append(fachmerker)
              .append("_j_bis >= ").append(jahrSem);
        } else {
          sqlString.append(" AND ").append(fachmerker).append("_s_von <= ").append(jahrSem).append(" AND ").append(fachmerker)
              .append("_s_bis >= ").append(jahrSem);
        }
      }
    }

    sqlString.append(" ORDER BY plz, name");

    try (final ResultSet result = myDataBase.createStatement().executeQuery(sqlString.toString())) {
      // Ausgabe der Suchergebnisseite
      return suchergebnisUebersicht(res, result, land, bietesuche, vorwahl, fach, fachAlter, jahrSemSel, jahrSem);
    } catch (final java.sql.SQLException e) {
      throw new de.knightsoft.common.TextException("Fehler beim Datenbanksuchen: " + e.toString(), e);
    }
  }

  /**
   * The Method <code>verarbeitungSucheKomplex</code> is used for detailed search. Not implemented here, its just a call to
   * verarbeitungSucheKompakt
   *
   * @param res HttpServletResponse from the Servlet
   * @param land country code
   * @param bietesuche search for "Bieter" or "Sucher"
   * @param vorwahl first part of the phone number
   * @param fach subject
   * @param fachAlter alternativ subject selection
   * @param jahrSemSel semester or class
   * @param jahrSem year of semester or class
   * @return HTML code of the formular
   */
  private String verarbeitungSucheKomplex(final HttpServletResponse res, final String land, final String bietesuche,
      final String vorwahl, final String fach, final String fachAlter, final String jahrSemSel, final String jahrSem)
      throws de.knightsoft.common.TextException {
    return verarbeitungSucheKompakt(res, land, bietesuche, vorwahl, fach, fachAlter, jahrSemSel, jahrSem);
  }

  /**
   * The Method <code>verarbeitungDetailansicht</code> is used for to display the detailed search results.
   *
   * @param req HttpServletRequest from the Servlet
   * @param res HttpServletResponse from the Servlet
   * @param land country code
   * @param bietesuche search for "Bieter" or "Sucher"
   * @param vorwahl first part of the phone number
   * @return HTML code of the formular
   */
  private String verarbeitungDetailansicht(final HttpServletRequest req, final HttpServletResponse res, final String land,
      final String bietesuche, final String vorwahl, final String fach, final String fachAlter, final String jahrSemSel,
      final String jahrSem) throws de.knightsoft.common.TextException {
    final String dbNr = req.getParameter("detailnr");

    try (final PreparedStatement searchSQLOneStatement = myDataBase.prepareStatement(Search.SEARCH_SQL_ONE)) {
      // Lesezugriff auf die Datenbank
      de.knightsoft.knightsoftnet.assistance.AssistCheck nachhilfeDaten = null;
      searchSQLOneStatement.clearParameters();
      searchSQLOneStatement.setInt(1, Integer.parseInt(dbNr));
      try (final ResultSet result = searchSQLOneStatement.executeQuery()) {
        if (!result.next()) {
          throw new de.knightsoft.common.TextException("Diese Eintragung existiert nicht mehr!");
        }
        nachhilfeDaten = new de.knightsoft.knightsoftnet.assistance.AssistCheck(result);
      }

      // Ausgabe der Suchergebnisseite
      return suchergebnisDetail(res, nachhilfeDaten, land, bietesuche, vorwahl, fach, fachAlter, jahrSemSel, jahrSem);
    } catch (final java.sql.SQLException e) {
      throw new de.knightsoft.common.TextException("Fehler beim Datenbankzugriff für Detailansicht: " + e.toString(), e);
    }
  }

  /**
   * The Method <code>HTMLPage</code> generates the html formular.
   *
   * @param res HttpServletResponse from the Servlet
   * @param hint Hint to be displayed on the screen
   * @param session The Data of the current Session
   * @param navTyp Navigation type
   * @return HTML code of the formular
   */
  @Override
  protected String htmlPage(final HttpServletResponse res, final String hint, final HttpSession session, final String navTyp) {
    session.setAttribute(servletName + AbstractVisualDb.SELECTED_FIELD_TEXT + serviceName, "Vorwahl");
    return "        <p>F&uuml;r Sch&auml;den, die im Rahmen des von uns vermittelten "
        + "Vertragsverh&auml;ltnisses entstehen, k&ouml;nnen wir keine Haftung " + "&uuml;bernehmen.</p>\n" + "        <p>\n"
        + "            Hier k&ouml;nnen Sie unsere Datenbank nach geeigneten Nachhilfelehrern " + "durchsuchen.\n"
        + "            Dazu geben Sie bitte ihre Telefonvorwahl, das Fach und die Jahrgangsstufe "
        + "(letztere kann weggelassen werden) in die Suchfelder ein und dr&uuml;cken den " + "Suchen-Knopf.\n"
        + "        </p>\n" + "        <p>\n" + "            <b>Hinweis:</b> Sie k&ouml;nnen ab der dritten Stelle der Vorwahl "
        + "den Platzhalter * verwenden, um einen gr&ouml;&szlig;eren Bereich abzudecken.<br>\n"
        + "            Das Fach kann entweder aus dem Popupmen&uuml; ausgew&auml;hlt, oder in "
        + "das Textfeld darunter eingegeben werden.<br>\n"
        + "            Jahrgang oder Semester m&uuml;ssen nicht angegeben werden. Weitere " + "Hinweise auf der <a href=\""
        + KnConst.HTML_BASE + "Nachhilfe/faq.html\">FAQ-Seite</a>\n" + "        </p>\n"
        + "        <div style=\"text-align:center;\">\n"
        + (hint == null ? "" : "    <h2>" + de.knightsoft.common.StringToHtml.convert(hint) + "</h2>\n")
        + "            <form action=\"" + res.encodeURL(KnConst.HTML_BASE + "Nachhilfe/suchen.html")
        + "\" method=\"POST\" enctype=\"application/x-www-form-urlencoded; charset=utf-8\" "
        + "accept-charset=\"utf-8\" OnSubmit=\"return chkSuchen()\">\n"
        + "                <table style=\"margin-left:auto; margin-right:auto;\" border=\"0\">\n" + "                    <tr>\n"
        + "                        <td style=\"text-align:left;\">Land:</td>\n"
        + "                        <td style=\"text-align:left;\">\n"
        + "                            <input type=\"hidden\" name=\"Stufe\" value=\"1\">\n"
        + "                            <select name=\"land\" size=\"1\">\n"
        + "                                <option value=\"de\">Deutschland</option>\n"
        + "                                <option value=\"at\">&Ouml;sterreich</option>\n"
        + "                                <option value=\"ch\">Schweiz</option>\n" + "                            </select>\n"
        + "                        </td>\n" + "                    </tr>\n" + "                    <tr>\n"
        + "                        <td style=\"text-align:left;\">Biete/Suche:</td>\n"
        + "                        <td style=\"text-align:left;\">\n"
        + "                            <select name=\"bietesuche\" size=\"1\">\n"
        + "                                <option value=\"b\">Ich suche jemanden der mir " + "Nachhilfe erteilt</option>\n"
        + "                                <option value=\"s\">Ich suche jemanden der Nachhilfe " + "ben&ouml;tigt</option>\n"
        + "                            </select>\n" + "                        </td>\n" + "                    </tr>\n"
        + "                    <tr>\n" + "                        <td style=\"text-align:left;\">Telefonvorwahl:</td>\n"
        + "                        <td style=\"text-align:left;\"><input type=\"Text\" "
        + "name=\"Vorwahl\" size=\"6\" maxlength=\"6\"></td>\n" + "                    </tr>\n" + "                    <tr>\n"
        + "                        <td style=\"text-align:left; vertical-align:top;\">Fach:</td>\n"
        + "                        <td style=\"text-align:left;\">\n"
        + "                            <select name=\"Fach\" size=\"1\" >\n"
        + "                                <option>bitte w&auml;hlen Sie</option>\n"
        + "                                <option>Biologie</option>\n"
        + "                                <option>BWL</option>\n" + "                                <option>Chemie</option>\n"
        + "                                <option>Deutsch</option>\n"
        + "                                <option>Englisch</option>\n"
        + "                                <option>Erdkunde</option>\n"
        + "                                <option>Franz&ouml;sisch</option>\n"
        + "                                <option>Geschichte</option>\n"
        + "                                <option>Griechisch</option>\n"
        + "                                <option>Informatik</option>\n"
        + "                                <option>Latein</option>\n"
        + "                                <option>Maschineschreiben</option>\n"
        + "                                <option>Mathematik</option>\n"
        + "                                <option>Physik</option>\n"
        + "                                <option>Sozialkunde</option>\n"
        + "                                <option>Stenografie</option>\n"
        + "                                <option>anderes Fach</option>\n" + "                            </select><br>\n"
        + "                            <input type=\"Text\" name=\"FachAlter\" size=\"20\" " + "maxlength=\"30\">\n"
        + "                        </td>\n" + "                    </tr>\n" + "                    <tr>\n"
        + "                        <td style=\"text-align:left;\">\n"
        + "                            <select name=\"JahrSemSel\" size=\"1\" >\n"
        + "                                <option>Jahrgang:</option>\n"
        + "                                <option>Semester:</option>\n" + "                            </select>\n"
        + "                        </td>\n" + "                        <td style=\"text-align:left;\"><input type=\"Text\" "
        + "name=\"JahrSem\" size=\"2\" maxlength=\"2\"></td>\n" + "                    </tr>\n" + "                    <tr>\n"
        + "                        <td style=\"text-align:center;\" colspan=\"2\">"
        + "<input type=submit value=\"Suchen\"> <input type=reset value=\"Abbrechen\"></td>\n" + "                    </tr>\n"
        + "                </table>\n" + "            </form>\n" + "        </div>\n";
  }

  /**
   * The Method <code>doTheWork</code> coordinates all the work in this class
   *
   * @param req HttpServletRequest from the Servlet
   * @param res HttpServletResponse from the Servlet
   * @param session The Data of the current Session
   * @return html code of the formular
   * @exception de.knightsoft.common.TextException when a error in checking the passwords occours
   */
  @Override
  public String doTheWork(final HttpServletRequest req, final HttpServletResponse res, final HttpSession session)
      throws de.knightsoft.common.TextException {

    final String stufe = req.getParameter("Stufe");
    final String land = req.getParameter("land");
    final String bietesuche = req.getParameter("bietesuche");
    final String vorwahl = req.getParameter("Vorwahl");
    final String fach = req.getParameter("Fach");
    final String fachAlter = req.getParameter("FachAlter");
    final String jahrSemSel = req.getParameter("JahrSemSel");
    final String jahrSem = req.getParameter("JahrSem");
    final String navTyp = "suchen";
    String formular = null;

    try {
      session.removeAttribute(servletName + AbstractVisualDb.SELECTED_FIELD_TEXT + serviceName);
      if (stufe == null) {
        formular = htmlPage(res, null, session, navTyp);
        // Stufe 1 = Suche kompakt (nur Eingabe der Vorwahl)
      } else if ("1".equals(stufe)) {
        formular = verarbeitungSucheKompakt(res, land, bietesuche, vorwahl, fach, fachAlter, jahrSemSel, jahrSem);
        // Stufe 2 = Suche detailliert (komplexe Selektionsmöglichkeiten
      } else if ("2".equals(stufe)) {
        formular = verarbeitungSucheKomplex(res, land, bietesuche, vorwahl, fach, fachAlter, jahrSemSel, jahrSem);
        // Stufe 3 = Bestätigung oder Ablehnung der Löschbestätigung
      } else if ("3".equals(stufe)) {
        formular = verarbeitungDetailansicht(req, res, land, bietesuche, vorwahl, fach, fachAlter, jahrSemSel, jahrSem);
      } else {
        formular = htmlPage(res, null, session, navTyp);
      }
    } catch (final de.knightsoft.common.TextException e) {
      formular = htmlPage(res, e.toString(), session, navTyp);
    }

    return formular;
  }

  /**
   * The Method <code>allowedToSee</code> tells you if the currently logged in user is allowed to access this application.
   *
   * @param session Data of the current session
   * @return true if it is allowed for this user
   */
  @Override
  public boolean allowedToSee(final HttpSession session) {
    return true;
  }

  /**
   * The Method <code>allowedToChange</code> tells you if the currently logged in user is allowed to change data in this
   * application.
   *
   * @param session Data of the current session
   * @return true if it is allowed for this user
   */
  @Override
  public boolean allowedToChange(final HttpSession session) {
    return true;
  }
}
