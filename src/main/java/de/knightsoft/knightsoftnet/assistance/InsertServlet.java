/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.knightsoftnet.assistance;

import de.knightsoft.knightsoftnet.common.KnConst;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import java.io.IOException;
import java.sql.Connection;

public class InsertServlet extends HttpServlet {

  private static final long serialVersionUID = 4511309674365968234L;

  /**
   * Nach der Eingabe, Daten auf Gültigkeit prüfen und zur Bestätigung durch den Benutzer nochmals ausgeben.
   */
  private void verarbeitungStufe1(final HttpServletRequest req, final HttpServletResponse res, final ServletOutputStream out,
      final de.knightsoft.knightsoftnet.assistance.AssistCheck nachhilfeDaten)
      throws de.knightsoft.common.TextException, IOException {
    try {
      nachhilfeDaten.checkDaten();

      final String inhalt = "<div style=\"text-align:center;\"><h2>Pr&uuml;fen Sie die eingegebenen Daten, "
          + "dr&uuml;cken Sie den \"Best&auml;tigen\" Knopf wenn die Daten korrekt sind und "
          + "Sie mit den Konditionen einverstanden sind</h2></div>\n" + "<p>&nbsp;</p>\n" + "<form action=\""
          + res.encodeURL(KnConst.SERVLET_URL + "KnightSoftNachhilfeInsert")
          + "\" method=\"POST\" enctype=\"application/x-www-form-urlencoded; charset=utf-8\" " + "accept-charset=\"utf-8\">\n"
          + "<input type=\"hidden\" name=\"Stufe\" value=\"2\">\n" + nachhilfeDaten.htmlHidden() + "\n"
          + nachhilfeDaten.htmlTabelle(false) + "\n" + de.knightsoft.knightsoftnet.common.KnConst.HINWEIS_REGISTER + "\n"
          + "<p style=\"text-align:center;\"><input type=\"submit\" name=\"Submittype\" "
          + "value=\"Best&auml;tigen\"><input type=\"submit\" name=\"Submittype\" " + "value=\"Abbrechen\"></p>\n"
          + "</form>\n";

      final String htmlSeite = de.knightsoft.knightsoftnet.common.SeitenTemplate.htmlSeite(req, "KnightSoftNachhilfeInsert",
          "Daten werden eingef&uuml;gt", "", true, de.knightsoft.knightsoftnet.common.KnConst.NACHHILFE_FARBE,
          de.knightsoft.knightsoftnet.common.KnConst.NACHHILFE_TEXT, "Prüfung der Eingaben", inhalt,
          de.knightsoft.knightsoftnet.assistance.Navigation.NACHHILFE_NAV);

      // HTML-Datei für Insertbestätigung schreiben
      out.println(htmlSeite);

    } catch (final de.knightsoft.common.TextException e) {
      final de.knightsoft.knightsoftnet.assistance.ChangeServlet MyChangeServlet =
          new de.knightsoft.knightsoftnet.assistance.ChangeServlet();

      MyChangeServlet.setService("Nachhilfe");
      MyChangeServlet.setWbildReg(de.knightsoft.knightsoftnet.common.KnConst.WBILD_NACH_REG);
      MyChangeServlet.setWurlReg(de.knightsoft.knightsoftnet.common.KnConst.WURL_NACH_REG);

      if (nachhilfeDaten.biologie == null) {
        nachhilfeDaten.biologie = "N";
      }
      if (nachhilfeDaten.bwl == null) {
        nachhilfeDaten.bwl = "N";
      }
      if (nachhilfeDaten.chemie == null) {
        nachhilfeDaten.chemie = "N";
      }
      if (nachhilfeDaten.deutsch == null) {
        nachhilfeDaten.deutsch = "N";
      }
      if (nachhilfeDaten.englisch == null) {
        nachhilfeDaten.englisch = "N";
      }
      if (nachhilfeDaten.erdkunde == null) {
        nachhilfeDaten.erdkunde = "N";
      }
      if (nachhilfeDaten.franzoesisch == null) {
        nachhilfeDaten.franzoesisch = "N";
      }
      if (nachhilfeDaten.geschichte == null) {
        nachhilfeDaten.geschichte = "N";
      }
      if (nachhilfeDaten.griechisch == null) {
        nachhilfeDaten.griechisch = "N";
      }
      if (nachhilfeDaten.informatik == null) {
        nachhilfeDaten.informatik = "N";
      }
      if (nachhilfeDaten.latein == null) {
        nachhilfeDaten.latein = "N";
      }
      if (nachhilfeDaten.maschineschreiben == null) {
        nachhilfeDaten.maschineschreiben = "N";
      }
      if (nachhilfeDaten.mathematik == null) {
        nachhilfeDaten.mathematik = "N";
      }
      if (nachhilfeDaten.physik == null) {
        nachhilfeDaten.physik = "N";
      }
      if (nachhilfeDaten.sozialkunde == null) {
        nachhilfeDaten.sozialkunde = "N";
      }
      if (nachhilfeDaten.stenografie == null) {
        nachhilfeDaten.stenografie = "N";
      }
      if (nachhilfeDaten.news == null) {
        nachhilfeDaten.news = "J";
      }

      out.println(MyChangeServlet.aendernHtml(req, res, de.knightsoft.knightsoftnet.common.KnConst.JAVA_SCRIPT_NH_URL,
          "Fehler beim Einfügen: " + e.getMessage(), nachhilfeDaten, "KnightSoftNachhilfeInsert"));
    }
  }

  /**
   * Die geprüften Daten in die Datenbank sichern und eine email an den Benutzer verschicken.
   */
  private void verarbeitungStufe2(final HttpServletRequest req, final HttpServletResponse res, final ServletOutputStream out,
      final de.knightsoft.knightsoftnet.assistance.AssistCheck nachhilfeDaten)
      throws de.knightsoft.common.TextException, IOException, ServletException {
    String sqlString = null;
    InitialContext ic = null;

    try {
      ic = new InitialContext();
      final DataSource lDataSource = (DataSource) ic.lookup("java:comp/env/jdbc/db_knightsoft");
      try (final Connection myDataBase = lDataSource.getConnection()) {

        sqlString = nachhilfeDaten.insertVorbereiten();
        myDataBase.createStatement().executeUpdate("INSERT INTO KnightSoft_Nachhilfe " + sqlString);
      }

      String inhalt = "<div style=\"text-align:center;\"><h2>Sie wurden als Nachhilfe in unsere Datenbank "
          + "aufgenommen.</h2>\n" + "<p>Eine E-Mail mit dem Passwort zum &auml;ndern oder l&ouml;schen Ihrer "
          + "Eintragung wird Ihnen automatisch zugesendet.</p>\n"
          + "<p>Bitte beachten Sie, dass Ihr Eintrag gesperrt bleibt, bis Sie sich einmal "
          + "mit dem zugesandten Passwort erfolgreich eingeloggt haben.</p>\n" + "<a href=\""
          + res.encodeURL(KnConst.SERVLET_URL + "KnightSoftNachhilfePflege?Stufe=0") + "\"> zur&uuml;ck</a></div>\n";

      // E-Mail versenden
      try {
        sendPasswort(nachhilfeDaten.email, nachhilfeDaten.passwort, nachhilfeDaten.geschlecht, nachhilfeDaten.name);
      } catch (final IOException e) {
        inhalt += "Fehler beim Versenden der E-Mail: " + e.toString() + "\n"; // NOPMD
      }

      out.println(de.knightsoft.knightsoftnet.common.SeitenTemplate.htmlSeite(req, "KnightSoftNachhilfeInsert",
          "Die Aufnahme als Nachhilfe war erfolgreich", "", true, de.knightsoft.knightsoftnet.common.KnConst.NACHHILFE_FARBE,
          de.knightsoft.knightsoftnet.common.KnConst.NACHHILFE_TEXT, "Registrierung war erfolgreich", inhalt,
          de.knightsoft.knightsoftnet.assistance.Navigation.NACHHILFE_NAV));
    } catch (final java.sql.SQLException e) {
      throw new ServletException(e);
    } catch (final javax.naming.NamingException e) {
      throw new ServletException(e);
    } finally {
      if (ic != null) {
        try {
          ic.close();
        } catch (final NamingException e) {
          e.printStackTrace();
        }
      }
    }
  }

  /**
   * Nach der Eingabe, Daten auf Gültigkeit prüfen und zur Bestätigung durch den Benutzer nochmals ausgeben.
   *
   * @param req HttpServletRequest
   * @param res HttpServletResponse
   * @param out ServletOutputStream
   */
  private void verarbeitungAbbruch(final HttpServletRequest req, final HttpServletResponse res, final ServletOutputStream out)
      throws IOException {
    out.println(de.knightsoft.knightsoftnet.common.SeitenTemplate.htmlSeite(req, "KnightSoftNachhilfeInsert",
        "Abbruch der Nachhilfeeintragung", "", true, de.knightsoft.knightsoftnet.common.KnConst.NACHHILFE_FARBE,
        de.knightsoft.knightsoftnet.common.KnConst.NACHHILFE_TEXT, "Abbruch der Eintragung",
        "<div style=\"text-align:center;\"><h2>Sie haben den Abbruch-Knopf bet&auml;tigt und wurden nicht in unsere Datenbank aufgenommen.</h2>\n<a href=\""
            + res.encodeURL(KnConst.SERVLET_URL + "KnightSoftNachhilfePflege?Stufe=0") + "\"> zur&uuml;ck</a></div>\n",
        de.knightsoft.knightsoftnet.assistance.Navigation.NACHHILFE_NAV));
  }

  /**
   * Nach der Eingabe, Daten auf Gültigkeit prüfen und zur Bestätigung durch den Benutzer nochmals ausgeben.
   *
   * @param empfaengerEMail receiver mail address
   * @param passwort password
   * @param geschlecht sex
   * @param name user name
   */
  private void sendPasswort(final String empfaengerEMail, final String passwort, final String geschlecht, final String name)
      throws IOException {
    final String emailText =
        de.knightsoft.knightsoftnet.common.KnConst.registerEmail(name, geschlecht, empfaengerEMail, passwort, "Nachhilfe");

    new de.knightsoft.common.SendEMail(de.knightsoft.knightsoftnet.common.KnConst.EMAIL_NH,
        de.knightsoft.common.Constants.ORGANISATION, empfaengerEMail,
        "Registrierung in der " + de.knightsoft.common.Constants.COMPANY + " Nachhilfe-Vermittlung", emailText);
  }

  /**
   * Auslesen der gePOSTeden Suchkriterien, zugriff auf die Datenbank und Einfügen des Eintrags, wenn möglich.
   */
  @Override
  public void doPost(final HttpServletRequest req, final HttpServletResponse res) throws ServletException, IOException {
    // Setze als erstes den "content type" Header der Antwortseite auf HTML
    res.setContentType("text/html");

    // Writer für die HTML-Datei holen
    final ServletOutputStream out = res.getOutputStream();

    // Eingaben einlesen
    final de.knightsoft.knightsoftnet.assistance.AssistCheck NachhilfeDaten =
        new de.knightsoft.knightsoftnet.assistance.AssistCheck(req);

    try {
      if ("1".equals(NachhilfeDaten.stufe)) {
        verarbeitungStufe1(req, res, out, NachhilfeDaten);
      } else {
        final String typ = req.getParameter("Submittype");

        if ("Abbrechen".equals(typ)) {
          verarbeitungAbbruch(req, res, out);
        } else {
          verarbeitungStufe2(req, res, out, NachhilfeDaten);
        }
      }
      out.close();
    } catch (final de.knightsoft.common.TextException e) {
      // HTML-Datei für Fehlermeldung schreiben
      out.println(de.knightsoft.knightsoftnet.common.SeitenTemplate.htmlSeite(req, "KnightSoftNachhilfeInsert",
          "Fehler bei Datenbank-Insert", "", true, KnConst.NACHHILFE_FARBE, KnConst.NACHHILFE_TEXT,
          "Fehler beim Anlegen der Daten",
          "<div style=\"text-align:center;\"><h2>" + e.toHtml() + "</h2>\n<a href=\""
              + res.encodeURL(KnConst.SERVLET_URL + "KnightSoftNachhilfePflege?Stufe=0") + "\"> zur&uuml;ck</a></div>\n",
          de.knightsoft.knightsoftnet.assistance.Navigation.NACHHILFE_NAV));
      out.close();
    }
  }

  @Override
  public String getServletInfo() {
    return "Insert a new assistance teacher into the database\n\n" + de.knightsoft.common.Constants.COPYRIGHT;
  }
}
