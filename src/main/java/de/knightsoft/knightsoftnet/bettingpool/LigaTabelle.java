/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.knightsoftnet.bettingpool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * The <code>LigaTabelle</code> class generates a html version from the table of the current matchday.
 *
 * @author Manfred Tremmel
 * @version 2.0.1, 02.01.2010
 */
public class LigaTabelle {

  private final Connection tippDb;
  private int seasonLeague;
  private String name;
  private int numTeams;
  private int numTop1;
  private int numTop2;
  private int numDown1;
  private int numDown2;
  private String[] tabColor;
  private static final String[] TABELLEN_FARBEN = {"#2ced1d", "#5acc29", "#8aa936", "#bd8444", "#eb6351"};
  private static final String TABELLEN_SQL = "SELECT    tl.bezeichnung AS name,"
      + "            tl.anz_manschaften AS anz_manschaften," + "            tl.anz_tab_oben1 AS anz_tab_oben1,"
      + "            tl.anz_tab_oben2 AS anz_tab_oben2," + "            tl.anz_tab_unten1 AS anz_tab_unten1,"
      + "            tl.anz_tab_unten2 AS anz_tab_unten2," + "            tm.bezeichnung AS mannschafts_bez,"
      + "            SUM(if (ts.tore_heim IS NOT NULL, 1, 0)) AS anz_spiele," + "            SUM(if (ts.tore_heim IS NOT NULL, "
      + "                    if (ts.mannschaft_heim=tlm.lfd_liga_ms, "
      + "                        ts.tore_heim, ts.tore_ausw), 0)) AS tore," + "            SUM(if (ts.tore_heim IS NOT NULL, "
      + "                    if (ts.mannschaft_heim=tlm.lfd_liga_ms, "
      + "                        ts.tore_ausw, ts.tore_heim), 0)) AS gegentore,"
      + "            SUM(if (    ts.mannschaft_heim=tlm.lfd_liga_ms,"
      + "                    (if (ts.tore_heim IS NULL, 0, ts.tore_heim) - "
      + "                     if (ts.tore_ausw IS NULL, 0, ts.tore_ausw)),"
      + "                    (if (ts.tore_ausw IS NULL, 0, ts.tore_ausw) - "
      + "                     if (ts.tore_heim IS NULL, 0, ts.tore_heim))" + "                )"
      + "            ) AS torverhaeltnis," + "            SUM(" + "                    if (ts.tore_heim IS NOT NULL,"
      + "                        if (ts.mannschaft_heim=tlm.lfd_liga_ms,"
      + "                            if (ts.tore_heim > ts.tore_ausw," + "                                tl.punkte_sieg,"
      + "                                if (ts.tore_heim = ts.tore_ausw,"
      + "                                    tl.punkte_unent," + "                                    tl.punkte_nieder"
      + "                                    )" + "                                ),"
      + "                            if (ts.tore_ausw > ts.tore_heim," + "                                tl.punkte_sieg,"
      + "                                if (ts.tore_ausw = ts.tore_heim,"
      + "                                    tl.punkte_unent," + "                                    tl.punkte_nieder"
      + "                                    )" + "                                )" + "                            ),"
      + "                            0" + "                        )" + "                ) AS punkte," + "            SUM("
      + "                    if (ts.tore_heim IS NOT NULL," + "                        if (ts.mannschaft_heim=tlm.lfd_liga_ms,"
      + "                            if (ts.tore_heim > ts.tore_ausw," + "                                1,"
      + "                                0" + "                                ),"
      + "                            if (ts.tore_ausw > ts.tore_heim," + "                                1,"
      + "                                0" + "                                )" + "                            ),"
      + "                            0" + "                        )" + "                ) AS siege," + "            SUM("
      + "                    if (ts.tore_heim IS NOT NULL," + "                        if (ts.mannschaft_heim=tlm.lfd_liga_ms,"
      + "                            if (ts.tore_heim = ts.tore_ausw," + "                                1,"
      + "                                0" + "                                ),"
      + "                            if (ts.tore_ausw = ts.tore_heim," + "                                1,"
      + "                                0" + "                                )" + "                            ),"
      + "                            0" + "                        )" + "                ) AS unentschieden,"
      + "            SUM(" + "                    if (ts.tore_heim IS NOT NULL,"
      + "                        if (ts.mannschaft_heim=tlm.lfd_liga_ms,"
      + "                            if (ts.tore_heim < ts.tore_ausw," + "                                1,"
      + "                                0" + "                                ),"
      + "                            if (ts.tore_ausw < ts.tore_heim," + "                                1,"
      + "                                0" + "                                )" + "                            ),"
      + "                            0" + "                        )" + "                ) AS niederlagen,"
      + "            SUM(if (ts.tore_heim IS NOT NULL, "
      + "                    if (ts.mannschaft_heim=tlm.lfd_liga_ms, 1, 0), 0)" + "            ) AS anz_heimspiele, "
      + "            SUM(if (ts.tore_heim IS NOT NULL, "
      + "                    if (ts.mannschaft_heim=tlm.lfd_liga_ms, ts.tore_heim, 0), 0)" + "            ) AS heim_tore, "
      + "            SUM(if (ts.tore_heim IS NOT NULL, "
      + "                    if (ts.mannschaft_heim=tlm.lfd_liga_ms, ts.tore_ausw, 0), 0)" + "            ) AS heim_gegentore, "
      + "            SUM(" + "                    if (ts.tore_heim IS NOT NULL, "
      + "                        if (ts.mannschaft_heim=tlm.lfd_liga_ms, "
      + "                            if (ts.tore_heim > ts.tore_ausw, " + "                                tl.punkte_sieg, "
      + "                                if (ts.tore_heim = ts.tore_ausw, "
      + "                                    tl.punkte_unent, " + "                                    tl.punkte_nieder"
      + "                                    ) " + "                                ), " + "                            0 "
      + "                            ), " + "                        0 " + "                        ) "
      + "                ) AS heim_punkte, " + "            SUM(" + "                    if (ts.tore_heim IS NOT NULL, "
      + "                        if (ts.mannschaft_heim=tlm.lfd_liga_ms, "
      + "                            if (ts.tore_heim > ts.tore_ausw, " + "                                1, "
      + "                                0 " + "                                ), " + "                            0 "
      + "                            ), " + "                        0 " + "                        ) "
      + "                ) AS heim_siege, " + "            SUM(" + "                    if (ts.tore_heim IS NOT NULL, "
      + "                        if (ts.mannschaft_heim=tlm.lfd_liga_ms, "
      + "                            if (ts.tore_heim = ts.tore_ausw, " + "                                1, "
      + "                                0 " + "                                ), " + "                            0 "
      + "                            ), " + "                        0 " + "                        ) "
      + "                ) AS heim_unentschieden, " + "            SUM(" + "                    if (ts.tore_heim IS NOT NULL, "
      + "                        if (ts.mannschaft_heim=tlm.lfd_liga_ms, "
      + "                            if (ts.tore_heim < ts.tore_ausw, " + "                                1, "
      + "                                0 " + "                                ), " + "                            0 "
      + "                            ), " + "                        0 " + "                        ) "
      + "                ) AS heim_niederlagen, " + "            SUM(if (ts.tore_heim IS NOT NULL, "
      + "                    if (ts.mannschaft_heim=tlm.lfd_liga_ms, 0, 1), 0)" + "            ) AS anz_auswspiele, "
      + "            SUM(if (ts.tore_heim IS NOT NULL, "
      + "                    if (ts.mannschaft_heim=tlm.lfd_liga_ms, 0, ts.tore_ausw), 0)" + "            ) AS ausw_tore, "
      + "            SUM(if (ts.tore_heim IS NOT NULL, "
      + "                    if (ts.mannschaft_heim=tlm.lfd_liga_ms, 0, ts.tore_heim), 0)" + "            ) AS ausw_gegentore, "
      + "            SUM(" + "                    if (ts.mannschaft_heim=tlm.lfd_liga_ms, " + "                        0, "
      + "                        if (ts.tore_heim IS NOT NULL, "
      + "                            if (ts.tore_ausw > ts.tore_heim, " + "                                tl.punkte_sieg, "
      + "                                if (ts.tore_ausw = ts.tore_heim, "
      + "                                    tl.punkte_unent, " + "                                    tl.punkte_nieder"
      + "                                    ) " + "                                ), " + "                            0 "
      + "                            ) " + "                        ) " + "                ) AS ausw_punkte, "
      + "            SUM(" + "                    if (ts.mannschaft_heim=tlm.lfd_liga_ms, " + "                        0, "
      + "                        if (ts.tore_heim IS NOT NULL, "
      + "                            if (ts.tore_ausw > ts.tore_heim, " + "                                1, "
      + "                                0 " + "                                ), " + "                            0 "
      + "                            ) " + "                        ) " + "                ) AS ausw_siege, "
      + "            SUM(" + "                    if (ts.mannschaft_heim=tlm.lfd_liga_ms, " + "                        0, "
      + "                        if (ts.tore_heim IS NOT NULL, "
      + "                            if (ts.tore_ausw = ts.tore_heim, " + "                                1, "
      + "                                0 " + "                                ), " + "                            0 "
      + "                            ) " + "                        ) " + "                ) AS ausw_unentschieden, "
      + "            SUM(" + "                    if (ts.mannschaft_heim=tlm.lfd_liga_ms, " + "                        0, "
      + "                        if (ts.tore_heim IS NOT NULL, "
      + "                            if (ts.tore_ausw < ts.tore_heim, " + "                                1, "
      + "                                0 " + "                                ), " + "                            0 "
      + "                            ) " + "                        ) " + "                ) AS ausw_niederlagen "
      + "FROM        KnightSoft_TippLigaMannschaft            AS tlm "
      + "    LEFT JOIN    KnightSoft_TippSpiele                AS ts "
      + "    ON        (tlm.Mandator=ts.Mandator                AND "
      + "             tlm.saison_liga=ts.saison_liga            AND "
      + "             (    tlm.lfd_liga_ms=ts.mannschaft_heim    OR " + "                tlm.lfd_liga_ms=ts.mannschaft_ausw)) "
      + "    LEFT JOIN    KnightSoft_TippMannschaften            AS tm "
      + "    ON        (tlm.Mandator=tm.Mandator                AND " + "             tlm.zaehler=tm.zaehler) "
      + "    LEFT JOIN    KnightSoft_TippLiga                    AS tl"
      + "    ON        (tlm.Mandator=tl.Mandator                AND " + "             tlm.saison_liga=tl.saison_liga) "
      + "WHERE        tlm.Mandator = ? AND" + "            tlm.saison_liga= ? AND " + "            ts.spieltag<= ?    "
      + "GROUP BY    tm.zaehler " + "ORDER BY    punkte DESC, torverhaeltnis DESC, tore DESC, mannschafts_bez";

  /**
   * Constructor.
   *
   * @param thisDatabase Connection to database
   */
  public LigaTabelle(final Connection thisDatabase) {
    tippDb = thisDatabase;
    seasonLeague = -1;
  }

  /**
   * The Method <code>getAnzMannschaften</code> returns the number of teams which are playing in this league.
   *
   * @return teamcount
   */
  public int getAnzMannschaften() {
    return numTeams;
  }

  /**
   * The Method <code>getLigabezeichnung</code> returns the name of this league.
   *
   * @return name of the league
   */
  public String getLigabezeichnung() {
    return name;
  }

  /**
   * The Method <code>HTML_LigaTabelle</code> returns the results of one matchday as html table.
   *
   * @param mandator mandator number
   * @param saisonLigaUeb number of the liga
   * @param spieltag number of the matchday
   * @return html table
   */
  public String htmlLigaTabelle(final int mandator, final int saisonLigaUeb, final int spieltag) {
    final StringBuilder sb = new StringBuilder(5000);
    int posI;
    int posJ;
    int toreEigene = 0;
    int toreGegner = 0;
    int toreEigeneHeim = 0;
    int toreGegnerHeim = 0;
    int toreEigeneAusw = 0;
    int toreGegnerAusw = 0;

    try {
      posI = 0;
      sb.append("        <h3>Tabelle&nbsp;&sup2;)</h3>\n" + "        <table style=\"margin-left:auto; margin-right:auto;\" "
          + "border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" + "            <tr style=\"background-color:#aed2ff;\">\n"
          + "                <th colspan=\"13\"><p class=\"thr\">&nbsp;</p></th>\n"
          + "                <th style=\"background-color:white;\"><p class=\"tptc\">&nbsp;</p>" + "                </th>\n"
          + "                <th colspan=\"11\"><p class=\"thc\">Heimspiele</p></th>\n"
          + "                <th style=\"background-color:white;\"><p class=\"tptc\">&nbsp;</p>" + "                </th>\n"
          + "                <th colspan=\"11\"><p class=\"thc\">Ausw&auml;rtsspiele</p></th>\n" + "            </tr>\n"
          + "            <tr style=\"background-color:#aed2ff;\">\n" + "                <th><p class=\"thr\">Pos</p></th>\n"
          + "                <th><p class=\"thl\">Mannschaft</p></th>\n" + "                <th><p class=\"thr\">Sp</p></th>\n"
          + "                <th><p class=\"thr\">Pkt</p></th>\n" + "                <th><p class=\"thr\">S</p></th>\n"
          + "                <th><p class=\"thr\">U</p></th>\n" + "                <th><p class=\"thr\">N</p></th>\n"
          + "                <th colspan=\"6\"><p class=\"thc\">Tore</p></th>\n"
          + "                <th style=\"background-color:white;\"><p class=\"tptc\">&nbsp;</p>" + "                </th>\n"
          + "                <th><p class=\"thr\">Sp</p></th>\n" + "                <th><p class=\"thr\">Pkt</p></th>\n"
          + "                <th><p class=\"thr\">S</p></th>\n" + "                <th><p class=\"thr\">U</p></th>\n"
          + "                <th><p class=\"thr\">N</p></th>\n"
          + "                <th colspan=\"6\"><p class=\"thc\">Tore</p></th>\n"
          + "                <th style=\"background-color:white;\"><p class=\"tptc\">&nbsp;</p>" + "                </th>\n"
          + "                <th><p class=\"thr\">Sp</p></th>\n" + "                <th><p class=\"thr\">Pkt</p></th>\n"
          + "                <th><p class=\"thr\">S</p></th>\n" + "                <th><p class=\"thr\">U</p></th>\n"
          + "                <th><p class=\"thr\">N</p></th>\n"
          + "                <th colspan=\"6\"><p class=\"thc\">Tore</p></th>\n" + "            </tr>\n");

      try (final PreparedStatement ligaStatement = tippDb.prepareStatement(LigaTabelle.TABELLEN_SQL)) {
        ligaStatement.clearParameters();
        ligaStatement.setInt(1, mandator);
        ligaStatement.setInt(2, saisonLigaUeb);
        ligaStatement.setInt(3, spieltag);
        try (ResultSet result = ligaStatement.executeQuery()) {
          while (result.next()) {
            if (posI == 0 && seasonLeague != saisonLigaUeb) {
              seasonLeague = saisonLigaUeb;
              name = result.getString("name");
              numTeams = result.getInt("anz_manschaften");
              numTop1 = result.getInt("anz_tab_oben1");
              numTop2 = result.getInt("anz_tab_oben2");
              numDown1 = result.getInt("anz_tab_unten1");
              numDown2 = result.getInt("anz_tab_unten2");
              tabColor = new String[numTeams];

              for (posJ = 0; posJ < numTop1; tabColor[posJ++] = LigaTabelle.TABELLEN_FARBEN[0]) {
                ;
              }

              for (; posJ < numTop1 + numTop2; tabColor[posJ++] = LigaTabelle.TABELLEN_FARBEN[1]) {
                ;
              }

              for (; posJ < numTeams - numDown2 - numDown1; tabColor[posJ++] = LigaTabelle.TABELLEN_FARBEN[2]) {
                ;
              }

              for (; posJ < numTeams - numDown1; tabColor[posJ++] = LigaTabelle.TABELLEN_FARBEN[3]) {
                ;
              }

              for (; posJ < numTeams; tabColor[posJ++] = //
                  LigaTabelle.TABELLEN_FARBEN[4]) {
                ;
              }
            }

            toreEigene = result.getInt("tore");
            toreGegner = result.getInt("gegentore");
            toreEigeneHeim = result.getInt("heim_tore");
            toreGegnerHeim = result.getInt("heim_gegentore");
            toreEigeneAusw = result.getInt("ausw_tore");
            toreGegnerAusw = result.getInt("ausw_gegentore");

            sb.append("            <tr style=\"background-color:" + tabColor[posI] + ";\">\n"
                + "                <td><p class=\"tpr\">" + (posI + 1) + "&nbsp;.</p></td>\n"
                + "                <td><p class=\"tpl\">"
                + de.knightsoft.common.StringToHtml.convert(result.getString("mannschafts_bez"), true) + "</p></td>\n"
                + "                <td><p class=\"tpr\">" + Integer.toString(result.getInt("anz_spiele")) + "</p></td>\n"
                + "                <td><p class=\"tpr\">" + Integer.toString(result.getInt("punkte")) + "</p></td>\n"
                + "                <td><p class=\"tpr\">" + Integer.toString(result.getInt("siege")) + "</p></td>\n"
                + "                <td><p class=\"tpr\">" + Integer.toString(result.getInt("unentschieden")) + "</p></td>\n"
                + "                <td><p class=\"tpr\">" + Integer.toString(result.getInt("niederlagen")) + "</p></td>\n"
                + "                <td><p class=\"tpsc\">&nbsp;</p></td>\n" + "                <td><p class=\"tpsr\">"
                + Integer.toString(toreEigene) + "</p></td>\n" + "                <td><p class=\"tpsc\">:</p></td>\n"
                + "                <td><p class=\"tpsr\">" + Integer.toString(toreGegner) + "</p></td>\n"
                + "                <td><p class=\"tpsc\">&nbsp;</p></td>\n" + "                <td><p class=\"tpsr\">("
                + Integer.toString(result.getInt("torverhaeltnis")) + ")</p></td>\n"
                + "                <td style=\"background-color:white;\">" + "<p class=\"tptc\">&nbsp;</p></td>\n"
                + "                <td><p class=\"tpr\">" + Integer.toString(result.getInt("anz_heimspiele")) + "</p></td>\n"
                + "                <td><p class=\"tpr\">" + Integer.toString(result.getInt("heim_punkte")) + "</p></td>\n"
                + "                <td><p class=\"tpr\">" + Integer.toString(result.getInt("heim_siege")) + "</p></td>\n"
                + "                <td><p class=\"tpr\">" + Integer.toString(result.getInt("heim_unentschieden"))
                + "</p></td>\n" + "                <td><p class=\"tpr\">" + Integer.toString(result.getInt("heim_niederlagen"))
                + "</p></td>\n" + "                <td><p class=\"tpsc\">&nbsp;</p></td>\n"
                + "                <td><p class=\"tpsr\">" + Integer.toString(toreEigeneHeim) + "</p></td>\n"
                + "                <td><p class=\"tpsc\">:</p></td>\n" + "                <td><p class=\"tpsr\">"
                + Integer.toString(toreGegnerHeim) + "</p></td>\n" + "                <td><p class=\"tpsc\">&nbsp;</p></td>\n"
                + "                <td><p class=\"tpsr\">(" + Integer.toString(toreEigeneHeim - toreGegnerHeim) + ")</p></td>\n"
                + "                <td style=\"background-color:white;\">" + "<p class=\"tptc\">&nbsp;</p></td>\n"
                + "                <td><p class=\"tpr\">" + Integer.toString(result.getInt("anz_auswspiele")) + "</p></td>\n"
                + "                <td><p class=\"tpr\">" + Integer.toString(result.getInt("ausw_punkte")) + "</p></td>\n"
                + "                <td><p class=\"tpr\">" + Integer.toString(result.getInt("ausw_siege")) + "</p></td>\n"
                + "                <td><p class=\"tpr\">" + Integer.toString(result.getInt("ausw_unentschieden"))
                + "</p></td>\n" + "                <td><p class=\"tpr\">" + Integer.toString(result.getInt("ausw_niederlagen"))
                + "</p></td>\n" + "                <td><p class=\"tpsc\">&nbsp;</p></td>\n"
                + "                <td><p class=\"tpsr\">" + Integer.toString(toreEigeneAusw) + "</p></td>\n"
                + "                <td><p class=\"tpsc\">:</p></td>\n" + "                <td><p class=\"tpsr\">"
                + Integer.toString(toreGegnerAusw) + "</p></td>\n" + "                <td><p class=\"tpsc\">&nbsp;</p></td>\n"
                + "                <td><p class=\"tpsr\">(" + Integer.toString(toreEigeneAusw - toreGegnerAusw) + ")</p></td>\n"
                + "            </tr>\n");
            posI++;
          }
        }
      }

      if (posI == 0) {
        sb.append("        <h3>Die Tabelle steht noch nicht zur Verf&uuml;gung</h3>\n");
      } else {
        sb.append("        </table>\n" + "        <p class=\"anmerkung\">&sup2;)&nbsp;Beachten Sie, dass die Tabelle "
            + "aktuell aus den Spielergebnissen ermittelt wird, eventuelle Punktabz&uuml;ge "
            + "sind nicht ber&uuml;cksichtigt!</p>\n" + "        <hr>\n");
      }
    } catch (final java.sql.SQLException e) {
      sb.append("<h3>Fehler beim Datenbankzugriff (Liga-Tabelle): " + e.toString() + "</h3>");
    }

    return sb.toString();
  }
}
