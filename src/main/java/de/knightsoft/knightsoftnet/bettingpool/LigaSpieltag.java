/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.knightsoftnet.bettingpool;

import javax.servlet.http.HttpSession;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * The <code>LigaSpieltag</code> class generates a table with the results of one matchday.
 *
 * @author Manfred Tremmel
 * @version 2.0.1, 02.01.2010
 */
public class LigaSpieltag {

  private static final String SPIELTAG_SQL = //
      "SELECT            ts.lfd_spiele_nr + 1 as spiel_nr, " //
          + "                tm1.bezeichnung as heim_bez, " //
          + "                tm2.bezeichnung as ausw_bez, " //
          + "                if (ts.tore_heim IS NOT NULL, ts.tore_heim, null) as heim_tore, " //
          + "                if (ts.tore_heim IS NOT NULL, ts.tore_ausw, null) as ausw_tore, " //
          + "                DATE_FORMAT(ts.spielbeginn,'%d.%m.%Y %H:%i') as spielbeginn, " //
          + "                ts.spiel_beendet AS spiel_beendet " //
          + "FROM            KnightSoft_TippSpiele as ts " //
          + "    LEFT JOIN    KnightSoft_TippLigaMannschaft AS tlm1 " //
          + "    ON            (tlm1.Mandator=ts.Mandator AND " //
          + "                 tlm1.saison_liga=ts.saison_liga AND " //
          + "                 tlm1.lfd_liga_ms=ts.mannschaft_heim) " //
          + "    LEFT JOIN    KnightSoft_TippMannschaften AS tm1 " //
          + "    ON            (tlm1.Mandator=tm1.Mandator AND " //
          + "                 tlm1.zaehler=tm1.zaehler) " //
          + "    LEFT JOIN    KnightSoft_TippLigaMannschaft AS tlm2 " //
          + "    ON            (tlm2.Mandator=ts.Mandator AND " //
          + "                 tlm2.saison_liga=ts.saison_liga AND " //
          + "                 tlm2.lfd_liga_ms=ts.mannschaft_ausw) " //
          + "    LEFT JOIN    KnightSoft_TippMannschaften AS tm2 " //
          + "    ON            (tlm2.Mandator=tm2.Mandator AND " //
          + "                 tlm2.zaehler=tm2.zaehler) " //
          + "WHERE            ts.Mandator = ? AND " //
          + "                ts.saison_liga = ? AND " //
          + "                ts.spieltag = ? " //
          + "ORDER BY        ts.spielbeginn, spiel_nr";

  private final Connection tippDb;

  /**
   * Constructor.
   *
   * @param pdatabase Connection to database
   */
  public LigaSpieltag(final Connection pdatabase) {
    tippDb = pdatabase;
  }

  /**
   * The Method <code>HTML_LigaSpieltag</code> returns the results of one matchday as html table.
   *
   * @param session session data
   * @param saisonLiga number of the liga
   * @param spieltag number of the matchday
   * @return html table
   */
  public String htmlLigaSpieltag(final HttpSession session, final int mandator, final String servletname, final int saisonLiga,
      final int spieltag) {
    boolean farbig = false;
    final StringBuilder formular = new StringBuilder(1024);
    String heimTore = null;
    String auswTore = null;
    int spiel = 0;
    boolean finished = true;

    try {
      formular.append("        <h3>Ergebnisse</h3>\n" + "        <div style=\"text-align:center;\">\n"
          + "        <table style=\"margin-left:auto; margin-right:auto;\" border=\"0\" "
          + "cellspacing=\"0\" cellpadding=\"0\">\n" + "            <tr style=\"background-color:#5ea6ff;\">\n"
          + "                <th><p class=\"shc\">Spiel</p></th>\n"
          + "                <th><p class=\"shl\">Heimmannschaft</p></th>\n"
          + "                <th><p class=\"shl\">Ausw&auml;rtsmannschaft</p></th>\n"
          + "                <th><p class=\"shr\">Ergebnis</p></th>\n"
          + "                <th><p class=\"shl\">Spielbeginn</p></th>\n" + "            </tr>\n");
      spiel = 0;

      try (final PreparedStatement spieltagStatement = tippDb.prepareStatement(LigaSpieltag.SPIELTAG_SQL)) {
        spieltagStatement.clearParameters();
        spieltagStatement.setInt(1, mandator);
        spieltagStatement.setInt(2, saisonLiga);
        spieltagStatement.setInt(3, spieltag);
        try (final ResultSet result = spieltagStatement.executeQuery()) {
          while (result.next()) {
            spiel++;

            if (farbig) {
              formular.append("            <tr style=\"background-color:#5ea6ff;\">\n");
            } else {
              formular.append("            <tr style=\"background-color:#aed2ff;\">\n");
            }
            farbig = !farbig;

            formular.append(
                "                <td><p class=\"spc\">" + spiel + "</p></td>\n" + "                <td><p class=\"spl\">"
                    + de.knightsoft.common.StringToHtml.convert(result.getString("heim_bez"), true) + "</p></td>\n"
                    + "                <td><p class=\"spl\">"
                    + de.knightsoft.common.StringToHtml.convert(result.getString("ausw_bez"), true) + "</p></td>\n");
            heimTore = result.getString("heim_tore");
            auswTore = result.getString("ausw_tore");
            if (heimTore == null) {
              formular.append("                <td><p class=\"spc\">steht aus</p></td>\n");
              finished = false;
            } else {
              if (result.getBoolean("spiel_beendet")) {
                formular
                    .append("                <td><p class=\"spc\">" + heimTore + "&nbsp;:&nbsp;" + auswTore + "</p></td>\n");
              } else {
                formular
                    .append("                <td><p class=\"spco\">" + heimTore + "&nbsp;:&nbsp;" + auswTore + "</p></td>\n");
              }
            }
            formular.append("                <td><p class=\"spl\">"
                + de.knightsoft.common.StringToHtml.convert(result.getString("spielbeginn"), true) + "</p></td>\n"
                + "            </tr>\n");
          }
        }
      }

      formular.append("        </table>\n" + "        </div>\n");
      session.setAttribute(servletname + "LigaStatistikFinished", Boolean.valueOf(finished));

    } catch (final java.sql.SQLException e) {
      formular.append("<h3>Fehler beim Datenbankzugriff (Liga-Spieltag): " + e.toString() + "</h3>\n");
    }

    return formular.toString();
  }
}
