/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.knightsoftnet.bettingpool;

import de.knightsoft.common.AbstractVisualDb;
import de.knightsoft.common.TextException;
import de.knightsoft.common.field.NumberField;
import de.knightsoft.common.field.PopupField;
import de.knightsoft.knightsoftnet.common.KnConst;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * The <code>AdminLigaMannschaft</code> class is based on the abstract class de.knightsoft.common.AbstractVisualDb and
 * implements the administration of the Mannchaft &lt;-&gt; Liga connection in the KnightSoft Tipprunde.
 *
 * @author Manfred Tremmel
 * @version 2.0.0, 06.08.2006
 */
public class AdminLigaMannschaft extends de.knightsoft.common.AbstractVisualDb {

  private static final String GET_ANZ_TEAMS_LIGA_SQL = "SELECT        anz_manschaften " + "FROM        KnightSoft_TippLiga "
      + "WHERE        Mandator = ? AND" + "            saison_liga = ? ";
  private static final String GET_ANZ_TEAMS_SQL = "SELECT        COUNT(*) AS anz "
      + "FROM        KnightSoft_TippLigaMannschaft " + "WHERE        Mandator = ? AND" + "            saison_liga = ? ";
  private static final String INSERT_TEAM_SQL = "INSERT INTO KnightSoft_TippLigaMannschaft "
      + "            (Mandator, saison_liga, lfd_liga_ms, zaehler) " + "            VALUES (?, ?, ?, ?)";

  /**
   * Constructor.
   *
   * @param pdatabase Connection to database
   * @param pservletname Name of the Servlet
   * @exception SQLException if a database access error occurs
   * @exception TextException to give out a text message
   * @see TextException
   */
  public AdminLigaMannschaft(final Connection pdatabase, final String pservletname) throws SQLException, TextException {
    super(pdatabase, KnConst.HTML_BASE, pservletname, KnConst.GIF_URL + "16x16/mannschaften.png", "LigaMannschaften",
        "KnightSoft-Net Tipprundenadministration - Liga <-> Mannschaften", "KnightSoft_TippLiga",
        new de.knightsoft.common.field.AbstractBaseField[] {
            new NumberField(pservletname, null, "saison_liga", "saison_liga", 9, 9, true)},
        0, null, "KnightSoft_TippLigaMannschaft",
        new de.knightsoft.common.field.AbstractBaseField[] {
            new NumberField(pservletname, null, "saison_liga", "saison_liga", 9, 9, true),
            new NumberField(pservletname, null, "lfd_liga_ms", "lfd_liga_ms", 9, 9, true), new PopupField(pservletname,
                "Mannschaft", "zaehler", "zaehler", 1, 1, true, "SELECT zaehler AS value, Bezeichnung AS valuedisplay "
                    + "FROM KnightSoft_TippMannschaften " + "WHERE Mandator = ? " + "ORDER BY valuedisplay")},
        1, 0, false, false);
  }

  /**
   * The Method <code>HTML_Navigator</code> Creates the navigation part of the HTML page
   *
   * @param session The Data of the current Session
   * @param navTyp Navigation type
   * @return HTML code of the navigation part
   * @exception java.sql.SQLException
   */
  @Override
  protected String htmlNavigator(final HttpSession session, final String navTyp) throws java.sql.SQLException {
    return this.htmlNavigator(session, navTyp, false, true, false, false, true, false, false, false, false, false, false);
  }

  /**
   * The Method <code>fillMinMax</code> reads the min and max keys in the database.
   *
   * @param session Data of the current session
   * @exception java.sql.SQLException
   */
  @Override
  protected void fillMinMax(final HttpSession session) throws java.sql.SQLException {
    // nothing to do
  }

  /**
   * The Method <code>doTheWork</code> coordinates all the work in this class.
   *
   * @param req HttpServletRequest from the Servlet
   * @param res HttpServletResponse from the Servlet
   * @param session The Data of the current Session
   * @return html code of the formular
   * @exception TextException when a error in checking the passwords occours
   */
  @Override
  public String doTheWork(final HttpServletRequest req, final HttpServletResponse res, final HttpSession session)
      throws TextException {
    final String saisonLiga = req.getParameter("saison_liga");
    if (saisonLiga != null) {
      final Integer mandatorInteger =
          (Integer) session.getAttribute(servletName + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_MANDATOR);
      final int mandator = mandatorInteger == null ? 1 : mandatorInteger.intValue();
      int anzManschaftenLiga = 0;
      int anzManschaftenExists = 0;

      session.setAttribute(servletName + dataBaseTable + "." + keyField, saisonLiga);
      session.removeAttribute(servletName + AbstractVisualDb.NUMBER_POS_TEXT + serviceName);
      session.setAttribute(servletName + AbstractVisualDb.DBMIN_TEXT + serviceName, saisonLiga);
      session.setAttribute(servletName + AbstractVisualDb.DBMAX_TEXT + serviceName, saisonLiga);

      try (
          final PreparedStatement getAnzTeamsLigaSQLStatement =
              myDataBase.prepareStatement(AdminLigaMannschaft.GET_ANZ_TEAMS_LIGA_SQL);
          final PreparedStatement getAnzTeamsSQLStatement = myDataBase.prepareStatement(AdminLigaMannschaft.GET_ANZ_TEAMS_SQL);
          final PreparedStatement insertTeamSQLStatement = myDataBase.prepareStatement(AdminLigaMannschaft.INSERT_TEAM_SQL);) {
        synchronized (getAnzTeamsLigaSQLStatement) {
          getAnzTeamsLigaSQLStatement.clearParameters();
          getAnzTeamsLigaSQLStatement.setInt(1, mandator);
          getAnzTeamsLigaSQLStatement.setString(2, saisonLiga);
          try (final ResultSet resultAnzTeamsLiga = getAnzTeamsLigaSQLStatement.executeQuery()) {
            if (resultAnzTeamsLiga.next()) {
              anzManschaftenLiga = resultAnzTeamsLiga.getInt("anz_manschaften");
            }
          }
        }

        synchronized (getAnzTeamsSQLStatement) {
          getAnzTeamsSQLStatement.clearParameters();
          getAnzTeamsSQLStatement.setInt(1, mandator);
          getAnzTeamsSQLStatement.setString(2, saisonLiga);
          try (final ResultSet resultAnzTeams = getAnzTeamsSQLStatement.executeQuery()) {
            if (resultAnzTeams.next()) {
              anzManschaftenExists = resultAnzTeams.getInt("anz");
            }
          }
        }

        if (anzManschaftenLiga > anzManschaftenExists) {
          for (int i = anzManschaftenExists; i < anzManschaftenLiga; i++) {
            synchronized (insertTeamSQLStatement) {
              insertTeamSQLStatement.setInt(1, mandator);
              insertTeamSQLStatement.setString(2, saisonLiga);
              insertTeamSQLStatement.setInt(3, i);
              insertTeamSQLStatement.setInt(4, 1);
              insertTeamSQLStatement.executeUpdate();
            }
          }
        }
      } catch (final java.sql.SQLException e) {
        throw new TextException("SQL-Fehler:\n" + e.toString(), e);
      }
    }

    return super.doTheWork(req, res, session);
  }

  /**
   * The Method <code>allowedToSee</code> tells you if the currently logged in user is allowed to access this application.
   *
   * @param session Data of the current session
   * @return true if it is allowed for this user
   */
  @Override
  public boolean allowedToSee(final HttpSession session) {
    boolean returnwert = false;
    if (session != null) {
      if (session.getAttribute(servletName + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_USER
          + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_USER) != null) {
        final int stufe = ((Integer) session.getAttribute(servletName + KnConst.SESSION_STUFE_FT)).intValue();
        returnwert = stufe > 5;
      }
    }
    return returnwert;
  }

  /**
   * The Method <code>allowedToChange</code> tells you if the currently logged in user is allowed to change data in this
   * application.
   *
   * @param session Data of the current session
   * @return true if it is allowed for this user
   */
  @Override
  public boolean allowedToChange(final HttpSession session) {
    boolean returnwert = false;
    if (session != null) {
      if (session.getAttribute(servletName + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_USER
          + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_USER) != null) {
        final int stufe = ((Integer) session.getAttribute(servletName + KnConst.SESSION_STUFE_FT)).intValue();
        returnwert = stufe > 5;
      }
    }
    return returnwert;
  }
}
