/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.knightsoftnet.bettingpool;

import de.knightsoft.common.TextException;
import de.knightsoft.knightsoftnet.common.KnConst;

import javax.servlet.http.HttpSession;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * The <code>AdminLiga</code> class is based on the abstract class de.knightsoft.common.AbstractVisualDb and implements the
 * administration of the liga in the KnightSoft Tipprunde.
 *
 * @author Manfred Tremmel
 * @version 2.0.1, 06.07.2019
 */
public class AdminLiga extends de.knightsoft.common.AbstractVisualDb {

  /**
   * Constructor.
   *
   * @param thisDatabase Connection to database
   * @param servletname Name of the Servlet
   * @exception SQLException if a database access error occurs
   * @exception TextException to give out a text message
   * @see TextException
   */
  public AdminLiga(final Connection thisDatabase, final String servletname) throws SQLException, TextException {
    super(thisDatabase, KnConst.HTML_BASE, servletname, KnConst.GIF_URL + "16x16/liga.png", "Liga",
        "KnightSoft-Net Tipprundenadministration - Liga", "KnightSoft_TippLiga",
        new de.knightsoft.common.field.AbstractBaseField[] {
            new de.knightsoft.common.field.NumberField(servletname, "Saison/Liga", "saison_liga", "saison_liga", 9, 9, true,
                Long.valueOf(199800001L), Long.valueOf(999999999L)),
            new de.knightsoft.common.field.CheckBoxField(servletname, "Liga gesperrt", "eintrag_gesperrt", "eintrag_gesperrt",
                1, 1, true),
            new de.knightsoft.common.field.TextField(servletname, "Bezeichnung", "bezeichnung", "bezeichnung", 50, 30, true),
            new de.knightsoft.common.field.NumberField(servletname, "Anzahl Mannschaften", "anz_manschaften", "anz_manschaften",
                2, 2, true, Long.valueOf(2L), Long.valueOf(127L)),
            new de.knightsoft.common.field.NumberField(servletname, "Besondere Tab.pos. oben 1", "anz_tab_oben1",
                "anz_tab_oben1", 2, 2, true, Long.valueOf(0L), Long.valueOf(99L)),
            new de.knightsoft.common.field.NumberField(servletname, "Besondere Tab.pos. oben 2", "anz_tab_oben2",
                "anz_tab_oben2", 2, 2, true, Long.valueOf(0L), Long.valueOf(99L)),
            new de.knightsoft.common.field.NumberField(servletname, "Besondere Tab.pos. unten 1", "anz_tab_unten1",
                "anz_tab_unten1", 2, 2, true, Long.valueOf(0L), Long.valueOf(99L)),
            new de.knightsoft.common.field.NumberField(servletname, "Besondere Tab.pos. unten 2", "anz_tab_unten2",
                "anz_tab_unten2", 2, 2, true, Long.valueOf(0L), Long.valueOf(99L)),
            new de.knightsoft.common.field.NumberField(servletname, "Liga Punkte Sieg", "punkte_sieg", "punkte_sieg", 2, 2,
                true, Long.valueOf(0L), Long.valueOf(99L)),
            new de.knightsoft.common.field.NumberField(servletname, "Liga Punkte Unentschieden", "punkte_unent", "punkte_unent",
                2, 2, true, Long.valueOf(0L), Long.valueOf(99L)),
            new de.knightsoft.common.field.NumberField(servletname, "Liga Punkte Niederlage", "punkte_nieder", "punkte_nieder",
                2, 2, true, Long.valueOf(0L), Long.valueOf(99L)),
            new de.knightsoft.common.field.DummyField(servletname, null, null, 0, 0, false),
            new de.knightsoft.common.field.NumberField(servletname, "Tipprunde Punkte Exakt", "tr_punkte_exakt",
                "tr_punkte_exakt", 2, 2, true, Long.valueOf(0L), Long.valueOf(99L)),
            new de.knightsoft.common.field.NumberField(servletname, "Tipprunde Punkte Torverhältnis", "tr_punkte_torver",
                "tr_punkte_torver", 2, 2, true, Long.valueOf(0L), Long.valueOf(99L)),
            new de.knightsoft.common.field.NumberField(servletname, "Tipprunde Punkte Toto", "tr_punkte_toto", "tr_punkte_toto",
                2, 2, true, Long.valueOf(0L), Long.valueOf(99L)),
            new de.knightsoft.common.field.NumberField(servletname, "Tipprunde Punkte Falsch", "tr_punkte_falsch",
                "tr_punkte_falsch", 2, 2, true, Long.valueOf(0L), Long.valueOf(99L))},
        0);
  }

  /**
   * The Method <code>allowedToSee</code> tells you if the currently logged in user is allowed to access this application.
   *
   * @param session Data of the current session
   * @return true if it is allowed for this user
   */
  @Override
  public boolean allowedToSee(final HttpSession session) {
    boolean returnwert = false;
    if (session != null) {
      if (session.getAttribute(servletName + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_USER
          + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_USER) != null) {
        final int stufe = ((Integer) session.getAttribute(servletName + KnConst.SESSION_STUFE_FT)).intValue();
        returnwert = stufe > 5;
      }
    }
    return returnwert;
  }

  /**
   * The Method <code>allowedToChange</code> tells you if the currently logged in user is allowed to change data in this
   * application.
   *
   * @param session Data of the current session
   * @return true if it is allowed for this user
   */
  @Override
  public boolean allowedToChange(final HttpSession session) {
    boolean returnwert = false;
    if (session != null) {
      if (session.getAttribute(servletName + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_USER
          + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_USER) != null) {
        final int stufe = ((Integer) session.getAttribute(servletName + KnConst.SESSION_STUFE_FT)).intValue();
        returnwert = stufe > 5;
      }
    }
    return returnwert;
  }
}
