/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.knightsoftnet.bettingpool;

import de.knightsoft.common.TextException;
import de.knightsoft.knightsoftnet.common.KnConst;

import javax.servlet.http.HttpSession;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * The <code>AdminMannschaft</code> class is based on the abstract class de.knightsoft.common.AbstractVisualDb and implements
 * the administratio of the Teams in the KnightSoft Tipprunde.
 *
 * @author Manfred Tremmel
 * @version 2.0.0, 06.08.2006
 */
public class AdminMannschaft extends de.knightsoft.common.AbstractVisualDb {

  /**
   * Constructor.
   *
   * @param thisDatabase Connection to database
   * @param servletname Name of the Servlet
   * @exception SQLException if a database access error occurs
   * @exception TextException to give out a text message
   * @see TextException
   */
  public AdminMannschaft(final Connection thisDatabase, final String servletname) throws SQLException, TextException {
    super(thisDatabase, KnConst.HTML_BASE, servletname, KnConst.GIF_URL + "16x16/mannschaften.png", "Mannschaften",
        "KnightSoft-Net Tipprundenadministration - Mannschaften", "KnightSoft_TippMannschaften",
        new de.knightsoft.common.field.AbstractBaseField[] {
            new de.knightsoft.common.field.TextField(servletname, "Bezeichnung", "bezeichnung", "bezeichnung", 50, 50, true),
            new de.knightsoft.common.field.DummyField(servletname, "zaehler", "zaehler", 0, 0, false)},
        1);
  }

  /**
   * The Method <code>allowedToSee</code> tells you if the currently logged in user is allowed to access this application.
   *
   * @param session Data of the current session
   * @return true if it is allowed for this user
   */
  @Override
  public boolean allowedToSee(final HttpSession session) {
    boolean returnwert = false;
    if (session != null) {
      if (session.getAttribute(servletName + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_USER
          + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_USER) != null) {
        final int stufe = ((Integer) session.getAttribute(servletName + KnConst.SESSION_STUFE_FT)).intValue();
        returnwert = stufe > 5;
      }
    }
    return returnwert;
  }

  /**
   * The Method <code>allowedToChange</code> tells you if the currently logged in user is allowed to change data in this
   * application.
   *
   * @param session Data of the current session
   * @return true if it is allowed for this user
   */
  @Override
  public boolean allowedToChange(final HttpSession session) {
    boolean returnwert = false;
    if (session != null) {
      if (session.getAttribute(servletName + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_USER
          + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_USER) != null) {
        final int stufe = ((Integer) session.getAttribute(servletName + KnConst.SESSION_STUFE_FT)).intValue();
        returnwert = stufe > 5;
      }
    }
    return returnwert;
  }
}
