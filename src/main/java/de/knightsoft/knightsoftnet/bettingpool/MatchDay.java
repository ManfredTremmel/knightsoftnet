/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.knightsoftnet.bettingpool;

import java.util.Date;

public class MatchDay {
  private final int matchNumber;
  private final Date matchStart;
  private final Integer homeTeam;
  private final Integer otherTeam;
  private final Integer homeGoals;
  private final Integer otherGoals;
  private final boolean finished;

  /**
   * constructor initializing fields.
   *
   * @param pmatchNumber match number
   * @param pmatchStart match start DateField/TimeField
   * @param phomeTeam home team
   * @param potherTeam other team
   * @param phomeGoals home team goals
   * @param potherGoals other team goals
   * @param pfinished match has finished
   */
  public MatchDay(final int pmatchNumber, final Date pmatchStart, final Integer phomeTeam, final Integer potherTeam,
      final Integer phomeGoals, final Integer potherGoals, final boolean pfinished) {
    super();
    matchNumber = pmatchNumber;
    matchStart = pmatchStart;
    homeTeam = phomeTeam;
    otherTeam = potherTeam;
    homeGoals = phomeGoals;
    otherGoals = potherGoals;
    finished = pfinished;
  }

  public final int getMatchNumber() {
    return matchNumber;
  }

  public final Date getMatchStart() {
    return matchStart;
  }

  public final Integer getHomeTeam() {
    return homeTeam;
  }

  public final Integer getOtherTeam() {
    return otherTeam;
  }

  public final Integer getHomeGoals() {
    return homeGoals;
  }

  public final Integer getOtherGoals() {
    return otherGoals;
  }

  public final boolean isFinished() {
    return finished;
  }

  @Override
  public int hashCode() {
    return matchNumber;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (this.getClass() != obj.getClass()) {
      return false;
    }
    final MatchDay other = (MatchDay) obj;
    return matchNumber == other.matchNumber;
  }
}
