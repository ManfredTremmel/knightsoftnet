/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.knightsoftnet.bettingpool;

import de.knightsoft.common.StringToHtml;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.NumberFormat;

/**
 * The <code>LigaTippSchnittStatistik</code> class generates a html table with the avarage tipps of the players.
 *
 * @author Manfred Tremmel
 * @version 2.0.0, 06.08.2006
 */
public class LigaTippSchnittStatistik {

  private static final String ABFRAGE_SQL = "SELECT            tst.lfd_spiele_nr + 1 AS lfd_spiele_nr, "
      + "                tm1.bezeichnung AS heimmannschaft, " + "                tm2.bezeichnung AS auswmannschaft, "
      + "                SUM(tst.tore_heim) AS heimtore, " + "                SUM(tst.tore_ausw) AS auswtore, "
      + "                SUM(" + "                    if (tst.tore_heim is null, " + "                        0, "
      + "                        1 " + "                        ) " + "                    ) AS anz_tipps, "
      + "                SUM(" + "                    if (tst.tore_heim > tst.tore_ausw, " + "                        1, "
      + "                        0 " + "                        ) " + "                    ) AS anz_heimsieg_tipps, "
      + "                SUM(" + "                    if (tst.tore_heim < tst.tore_ausw, " + "                        1, "
      + "                        0 " + "                        ) " + "                    ) AS anz_auswsieg_tipps, "
      + "                SUM(" + "                    if (tst.tore_heim != tst.tore_ausw OR tst.tore_heim is null, "
      + "                        0, " + "                        1 " + "                        ) "
      + "                    ) AS anz_unent_tipps " + "FROM            KnightSoft_TippSpiele AS ts "
      + "    LEFT JOIN    KnightSoft_TippLigaMannschaft AS tlm1 " + "    ON            (tlm1.Mandator=ts.Mandator AND "
      + "                 tlm1.saison_liga=ts.saison_liga AND " + "                 tlm1.lfd_liga_ms=ts.mannschaft_heim) "
      + "    LEFT JOIN    KnightSoft_TippMannschaften AS tm1 " + "    ON            (tlm1.Mandator=tm1.Mandator AND "
      + "                 tlm1.zaehler=tm1.zaehler) " + "    LEFT JOIN    KnightSoft_TippLigaMannschaft AS tlm2 "
      + "    ON            (tlm2.Mandator=ts.Mandator AND " + "                 tlm2.saison_liga=ts.saison_liga AND "
      + "                 tlm2.lfd_liga_ms=ts.mannschaft_ausw) " + "    LEFT JOIN    KnightSoft_TippMannschaften AS tm2 "
      + "    ON            (tlm2.Mandator=tm2.Mandator AND " + "                 tlm2.zaehler=tm2.zaehler) "
      + "    LEFT JOIN    KnightSoft_TippSpieleTipp AS tst " + "    ON            (ts.Mandator=tst.Mandator AND "
      + "                 ts.saison_liga=tst.saison_liga AND " + "                 ts.spieltag=tst.spieltag AND "
      + "                 ts.lfd_spiele_nr=tst.lfd_spiele_nr) " + "WHERE            ts.Mandator = ? AND "
      + "                ts.saison_liga = ? AND " + "                ts.spieltag = ? " + "GROUP BY        tst.lfd_spiele_nr "
      + "ORDER BY        ts.spielbeginn, ts.lfd_spiele_nr";

  private final Connection tippDb;

  /**
   * Constructor.
   *
   * @param thisDatabase Connection to database
   */
  public LigaTippSchnittStatistik(final Connection thisDatabase) {
    tippDb = thisDatabase;
  }

  /**
   * The Method <code>HTML_LigaSpieltag</code> returns the avarage tipps of the players as html table.
   *
   * @param mandator mandator number
   * @param saisonLiga number of the liga
   * @param spieltag number of the matchday
   * @return html table
   */
  public String htmlLigaSpieltag(final int mandator, final int saisonLiga, final int spieltag) {
    boolean farbig = false;
    String formular = null;
    final StringBuilder formularT2 = new StringBuilder(512);
    String heimmannschaft;
    String auswmannschaft;
    int heimtore;
    int auswtore;
    int anzTipps;
    int anzHeimsiegTipps;
    int anzAuswsiegTipps;
    double heimtoreSchnitt;
    double auswtoreSchnitt;
    double heimsiegtippsProz;
    double auswsiegtippsProz;
    double unenttippsProz;

    int pos;

    final NumberFormat formProzent = NumberFormat.getPercentInstance(java.util.Locale.GERMAN);
    final NumberFormat formZahl = NumberFormat.getInstance(java.util.Locale.GERMAN);
    formZahl.setMinimumFractionDigits(2);
    formZahl.setMaximumFractionDigits(2);

    try {
      pos = 0;
      try (final PreparedStatement abfrageStatement = tippDb.prepareStatement(LigaTippSchnittStatistik.ABFRAGE_SQL)) {
        abfrageStatement.clearParameters();
        abfrageStatement.setInt(1, mandator);
        abfrageStatement.setInt(2, saisonLiga);
        abfrageStatement.setInt(3, spieltag);
        try (final ResultSet result = abfrageStatement.executeQuery()) {

          while (result.next()) {
            anzTipps = result.getInt("anz_tipps");

            if (anzTipps > 0) {
              pos++;

              heimmannschaft = result.getString("heimmannschaft");
              auswmannschaft = result.getString("auswmannschaft");
              heimtore = result.getInt("heimtore");
              auswtore = result.getInt("auswtore");
              anzHeimsiegTipps = result.getInt("anz_heimsieg_tipps");
              anzAuswsiegTipps = result.getInt("anz_auswsieg_tipps");
              heimtoreSchnitt = (double) heimtore / (double) anzTipps;
              auswtoreSchnitt = (double) auswtore / (double) anzTipps;
              if (anzHeimsiegTipps > 0) {
                heimsiegtippsProz = Math.round((double) (anzHeimsiegTipps * 100) / (double) anzTipps) / 100.0;
              } else {
                heimsiegtippsProz = 0.0;
              }
              if (anzAuswsiegTipps > 0) {
                auswsiegtippsProz = Math.round((double) (anzAuswsiegTipps * 100) / (double) anzTipps) / 100.0;
              } else {
                auswsiegtippsProz = 0.0;
              }
              unenttippsProz = 1.0 - (heimsiegtippsProz + auswsiegtippsProz);
              if (unenttippsProz < 0.0) {
                unenttippsProz = 0.0;
              }

              if (farbig) {
                formularT2.append("            <tr style=\"background-color:#5ea6ff;\">\n");
              } else {
                formularT2.append("            <tr style=\"background-color:#aed2ff;\">\n");
              }
              farbig = !farbig;

              formularT2.append(
                  "                <td><p class=\"spr\">" + pos + "&nbsp;.</p></td>\n" + "                <td><p class=\"spl\">"
                      + StringToHtml.convert(heimmannschaft, true) + "</p></td>\n" + "                <td><p class=\"spl\">"
                      + StringToHtml.convert(auswmannschaft, true) + "</p></td>\n" + "                <td><p class=\"spr\">"
                      + StringToHtml.convert(formZahl.format(heimtoreSchnitt)) + "</p></td>\n"
                      + "                <td><p class=\"spr\">" + StringToHtml.convert(formZahl.format(auswtoreSchnitt))
                      + "</p></td>\n" + "                <td><p class=\"spr\">"
                      + StringToHtml.convert(formProzent.format(heimsiegtippsProz)) + "</p></td>\n"
                      + "                <td><p class=\"spr\">" + StringToHtml.convert(formProzent.format(unenttippsProz))
                      + "</p></td>\n" + "                <td><p class=\"spr\">"
                      + StringToHtml.convert(formProzent.format(auswsiegtippsProz)) + "</p></td>\n" + "            </tr>\n");
            }
          }
        }
      }

      if (pos > 0) {
        formular = "        <h3>Durchschnittliche Tipps des Spieltages</h3>\n"
            + "        <table style=\"margin-left:auto; margin-right:auto;\" border=\"0\" "
            + "cellspacing=\"0\" cellpadding=\"0\">\n" + "            <tr style=\"background-color:#5ea6ff;\">\n"
            + "                <th><p class=\"shr\">Sp.</p></th>\n"
            + "                <th><p class=\"shl\">Heimmannschaft</p></th>\n"
            + "                <th><p class=\"shl\">Ausw&auml;rtsmannschaft</p></th>\n"
            + "                <th><p class=\"shr\">&Oslash;&nbsp;H.Tore</p></th>\n"
            + "                <th><p class=\"shr\">&Oslash;&nbsp;A.Tore</p></th>\n"
            + "                <th><p class=\"shr\">H.Sieg</p></th>\n"
            + "                <th><p class=\"shr\">Unent.</p></th>\n"
            + "                <th><p class=\"shr\">A.Sieg</p></th>\n" + "            </tr>\n" + formularT2.toString()
            + "        </table>\n";
      }
    } catch (final java.sql.SQLException e) {
      formular = "<h2>Fehler beim Datenbankzugriff (Tipp-Schnitt): " + e.toString() + "</h2>\n";
    }

    return formular;
  }
}
