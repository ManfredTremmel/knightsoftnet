/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.knightsoftnet.bettingpool;

import de.knightsoft.common.AbstractVisualDb;
import de.knightsoft.common.field.CheckBoxField;
import de.knightsoft.common.field.DateField;
import de.knightsoft.common.field.DummyField;
import de.knightsoft.common.field.EmailField;
import de.knightsoft.common.field.PasswordField;
import de.knightsoft.common.field.RadioLabledField;
import de.knightsoft.common.field.TextField;
import de.knightsoft.knightsoftnet.common.KnConst;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

/**
 * The <code>Mitspielerbereich</code> class is based on the abstract class de.knightsoft.common.AbstractVisualDb and implements
 * the management of the data of the player
 *
 *
 * @author Manfred Tremmel
 * @version 2.0.0, 06.08.2006
 */
public class Mitspielerbereich extends de.knightsoft.common.AbstractVisualDb {

  private static final String[] GESCHLECHT = {"W", "M"};
  private static final String[] GESCHLECHT_DISPLAY = {"weiblich", "männlich"};

  /**
   * Constructor
   *
   * @param thisDatabase Connection to database
   * @param servletname Name of the Servlet
   * @exception SQLException if a database access error occurs
   * @exception de.knightsoft.common.TextException to give out a text message
   * @see de.knightsoft.common.TextException
   */
  public Mitspielerbereich(final Connection thisDatabase, final String servletname)
      throws SQLException, de.knightsoft.common.TextException {
    super(thisDatabase, KnConst.HTML_BASE, servletname, KnConst.GIF_URL + "16x16/daten_aendern.png", "Mitspieler", "Mitspieler",
        "KnightSoft_TippMitspieler",
        new de.knightsoft.common.field.AbstractBaseField[] {
            new TextField(servletname, "Spitzname/Benutzer", "spitzname", "spitzname", //
                50, 30, true),
            new DateField(servletname, null, "datum_erstellt", "datum_erstellt", true),
            new PasswordField(servletname, "Passwort", "passwort", "passwort", 50, 30, true),
            new DateField(servletname, null, "datum_geaendert", "datum_geaendert", false),
            new EmailField(servletname, "E-Mail", "email", "email", 50, 30, true),
            new DummyField(servletname, null, null, 0, 0, false),
            new RadioLabledField(servletname, "Geschlecht", "geschlecht", "geschlecht", 1, 1, true,
                Mitspielerbereich.GESCHLECHT, Mitspielerbereich.GESCHLECHT_DISPLAY),
            new DummyField(servletname, null, null, 0, 0, false),
            new TextField(servletname, "Nachname", "nname", "name", 50, 30, true),
            new DummyField(servletname, null, null, 0, 0, false),
            new TextField(servletname, "Vorname", "vorname", "vorname", 50, 30, false),
            new DummyField(servletname, null, null, 0, 0, false),
            new TextField(servletname, "Straße", "strasse", "strasse", 50, 30, false),
            new DummyField(servletname, null, null, 0, 0, false),
            new TextField(servletname, "Postleitzahl", "plz", "plz", 5, 5, false),
            new DummyField(servletname, null, null, 0, 0, false),
            new TextField(servletname, "ORT", "ort", "ort", 50, 30, false),
            new DummyField(servletname, null, null, 0, 0, false), new CheckBoxField(servletname,
                "Ich möchte über Neuerungen per E-Mail informiert werden", "sende_infos", "sende_infos", 1, 1, true)},
        0);
  }

  /**
   * The Method <code>HTML_Navigator</code> Creates the navigation part of the HTML page
   *
   * @param session The Data of the current Session
   * @param navTyp Navigation type
   * @return HTML code of the navigation part
   * @exception java.sql.SQLException
   */
  @Override
  protected String htmlNavigator(final HttpSession session, final String navTyp) throws java.sql.SQLException {
    return this.htmlNavigator(session, navTyp, false, true, false, false, true, false, false, false, false, false, false);
  }

  /**
   * The Method <code>fillMinMax</code> reads the min and max keys in the database.
   *
   * @param session Data of the current session
   * @exception java.sql.SQLException
   */
  @Override
  protected void fillMinMax(final HttpSession session) throws java.sql.SQLException {
    if (session != null) {
      final String User = (String) session.getAttribute(servletName + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_USER
          + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_USER);
      session.setAttribute(servletName + dataBaseTable + "." + keyField, User);
      session.removeAttribute(servletName + AbstractVisualDb.NUMBER_POS_TEXT + serviceName);
      session.setAttribute(servletName + AbstractVisualDb.DBMIN_TEXT + serviceName, User);
      session.setAttribute(servletName + AbstractVisualDb.DBMAX_TEXT + serviceName, User);
    }
  }

  /**
   * The Method <code>preparePage</code> fills up the window with current data
   *
   * @param req HttpServletRequest from the Servlet
   * @param res HttpServletResponse from the Servlet
   * @param phint Text to be displayed
   * @param session The Data of the current Session
   * @param navTyp Navigation type
   * @return HTML code of the formular
   * @exception de.knightsoft.common.TextException text of the error
   */
  @Override
  protected String preparePage(final HttpServletRequest req, final HttpServletResponse res, final String phint,
      final HttpSession session, final String navTyp) throws de.knightsoft.common.TextException {
    String hint = phint;
    final SimpleDateFormat myDf = new java.text.SimpleDateFormat(DateField.DATE_FORMAT);
    int pos;
    final String dbmax = (String) session.getAttribute(servletName + AbstractVisualDb.DBMAX_TEXT + serviceName);
    final String dbnumber = (String) session.getAttribute(servletName + dataBaseTable + "." + keyField);
    final java.util.Date now = new java.util.Date();
    final java.sql.Timestamp nowSql = new java.sql.Timestamp(now.getTime());

    if (dbFields == null) {
      return null;
    }
    for (pos = 0; pos < dbFields.length; dbFields[pos++].initField(session)) {
      ;
    }
    dbFields[1].setField(session, myDf.format(new java.util.Date(nowSql.getTime())));
    dbFields[6].setField(session, Mitspielerbereich.GESCHLECHT[1]);

    try {
      // Datensätze vorhanden?
      if (dbnumber == null) {
        if (dbmax == null) {
          hint = "Geben Sie Ihre Daten zur Registrierung ein, mit dem Diskettensymbol" + " werden sie gespeichert.";
        } else {
          hint = StringUtils.EMPTY;
        }
      } else {
        // Lesezugriff auf die Datenbank
        final Integer MandatorInteger =
            (Integer) session.getAttribute(servletName + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_MANDATOR);
        try (final PreparedStatement preparedStatement = myDataBase.prepareStatement(readEntrySql)) {
          preparedStatement.clearParameters();
          preparedStatement.setInt(1, MandatorInteger == null ? 1 : MandatorInteger.intValue());
          preparedStatement.setString(2, dbnumber);
          try (final ResultSet result = preparedStatement.executeQuery()) {
            if (result.next() && dbFields != null) {
              for (pos = 0; pos < dbFields.length; dbFields[pos++].sqlRead(result, session)) {
                ;
              }
            }
          }
        }
        dbFields[3].setField(session, myDf.format(new java.util.Date(nowSql.getTime())));
      }

      return htmlPage(res, hint, session, navTyp);
    } catch (final java.sql.SQLException e) {
      // Um die Fehlerbehandlung nicht zweimal zu machen, schicken
      // wir die Meldung einfach weiter
      throw new de.knightsoft.common.TextException("SQL-Fehler in preparePage()\n" + e.toString(), e);
    }
  }

  /**
   * The Method <code>allowedToSee</code> tells you if the currently logged in user is allowed to access this application.
   *
   * @param session Data of the current session
   * @return true if it is allowed for this user
   */
  @Override
  public boolean allowedToSee(final HttpSession session) {
    return true;
  }

  /**
   * The Method <code>allowedToChange</code> tells you if the currently logged in user is allowed to change data in this
   * application.
   *
   * @param session Data of the current session
   * @return true if it is allowed for this user
   */
  @Override
  public boolean allowedToChange(final HttpSession session) {
    return true;
  }

}
