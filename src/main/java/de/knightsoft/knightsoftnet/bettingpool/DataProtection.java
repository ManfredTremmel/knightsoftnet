/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.knightsoftnet.bettingpool;

import de.knightsoft.common.TextException;
import de.knightsoft.knightsoftnet.common.KnConst;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * The <code>DataProtection</code> class is based on the abstract class de.knightsoft.common.AbstractVisualDb and implements the
 * data protection page of the KnightSoft Tipprunde.
 *
 *
 * @author Manfred Tremmel
 * @version 1.0.0, 28.07.2018
 */
public class DataProtection extends de.knightsoft.common.AbstractVisualDb {
  /**
   * Constructor.
   *
   * @param thisDatabase Connection to database
   * @param servletname Name of the Servlet
   * @exception SQLException if a database access error occurs
   * @exception TextException to give out a text message
   * @see TextException
   */
  public DataProtection(final Connection thisDatabase, final String servletname) throws SQLException, TextException {
    super(thisDatabase, KnConst.HTML_BASE, servletname, KnConst.GIF_URL + "16x16/impressum.png", "Datenschutz", "Datenschutz");
  }

  /**
   * The Method <code>HTMLPage</code> generates the html formular.
   *
   * @param res HttpServletResponse from the Servlet
   * @param hint Hint to be displayed on the screen
   * @param session The Data of the current Session
   * @param navTyp Navigation type
   * @return HTML code of the formular
   */
  @Override
  protected String htmlPage(final HttpServletResponse res, final String hint, final HttpSession session, final String navTyp) {
    return "<h2>Datenschutzerkl&auml;rung</h2><h3 id=\"dsg-general-intro\"></h3><p>Diese Datenschutzerkl&auml;rung kl&auml;rt Sie &uuml;ber die "
        + "Art, den Umfang und Zweck der Verarbeitung von personenbezogenen Daten (nachfolgend kurz &bdquo;Daten&ldquo;) innerhalb "
        + "unseres Onlineangebotes und der mit ihm verbundenen Webseiten, Funktionen und Inhalte sowie externen "
        + "Onlinepr&auml;senzen, wie z.B. unser Social Media Profile auf (nachfolgend gemeinsam bezeichnet als &bdquo;Onlineangebot&ldquo;). "
        + "Im Hinblick auf die verwendeten Begrifflichkeiten, wie z.B. &bdquo;Verarbeitung&ldquo; oder &bdquo;Verantwortlicher&ldquo; verweisen wir "
        + "auf die Definitionen im Art. 4 der Datenschutzgrundverordnung (DSGVO).<br>\n" + "<br>\n"
        + "</p><h3 id=\"dsg-general-controller\">Verantwortlicher</h3><p><span class=\"tsmcontroller\">Manfred Tremmel<br>\n"
        + "Marschalkstr. 20<br>\n" + "84419 Schwindegg<br>\n" + "E-Mailadresse: MTremmel@KnightSoft-Net.de<br>\n"
        + "Link zum Impressum: <a href=\"" + res.encodeURL(KnConst.HTML_BASE + "/Tipprunde/impressum.html") + "\">"
        + "https://www.knightsoft-net.de/Tipprunde/impressum.html</a><br>\n"
        + "</span></p><h3 id=\"dsg-general-datatype\">Arten der verarbeiteten Daten:</h3><p>"
        + "- Bestandsdaten (z.B., Namen, Adressen).<br>\n" + "- Kontaktdaten (z.B., E-Mail, Telefonnummern).<br>\n"
        + "- Inhaltsdaten (z.B., Texteingaben, Fotografien, Videos).<br>\n"
        + "- Nutzungsdaten (z.B., besuchte Webseiten, Interesse an Inhalten, Zugriffszeiten).<br>\n"
        + "- Meta-/Kommunikationsdaten (z.B., Ger&auml;te-Informationen, IP-Adressen).<br>\n"
        + "</p><h3 id=\"dsg-general-datasubjects\">Kategorien betroffener Personen</h3>"
        + "<p>Besucher und Nutzer des Onlineangebotes (Nachfolgend bezeichnen wir die betroffenen Personen zusammenfassend "
        + "auch als &bdquo;Nutzer&ldquo;).<br>\n"
        + "</p><h3 id=\"dsg-general-purpose\">Zweck der Verarbeitung</h3><p>- Zurverf&uuml;gungstellung des Onlineangebotes, "
        + "seiner Funktionen und  Inhalte.<br>\n" + "- Beantwortung von Kontaktanfragen und Kommunikation mit Nutzern.<br>\n"
        + "- Sicherheitsma&szlig;nahmen.<br>\n"
        + "<span class=\"tsmcom\"></span></p><h3 id=\"dsg-general-terms\">Verwendete Begrifflichkeiten </h3>"
        + "<p>&bdquo;Personenbezogene Daten&ldquo; sind alle Informationen, die sich auf eine identifizierte oder identifizierbare "
        + "nat&uuml;rliche Person (im Folgenden &bdquo;betroffene Person&ldquo;) beziehen; als identifizierbar wird eine nat&uuml;rliche Person "
        + "angesehen, die direkt oder indirekt, insbesondere mittels Zuordnung zu einer Kennung wie einem Namen, zu einer "
        + "Kennnummer, zu Standortdaten, zu einer Online-Kennung (z.B. Cookie) oder zu einem oder mehreren besonderen "
        + "Merkmalen identifiziert werden kann, die Ausdruck der physischen, physiologischen, genetischen, psychischen, "
        + "wirtschaftlichen, kulturellen oder sozialen Identit&auml;t dieser nat&uuml;rlichen Person sind.<br>\n" + "<br>\n"
        + "&bdquo;Verarbeitung&ldquo; ist jeder mit oder ohne Hilfe automatisierter Verfahren ausgef&uuml;hrte Vorgang oder jede solche "
        + "Vorgangsreihe im Zusammenhang mit personenbezogenen Daten. Der Begriff reicht weit und umfasst praktisch jeden "
        + "Umgang mit Daten.<br>\n" + "<br>\n"
        + "&bdquo;Pseudonymisierung&ldquo; die Verarbeitung personenbezogener Daten in einer Weise, dass die personenbezogenen Daten "
        + "ohne Hinzuziehung zus&auml;tzlicher Informationen nicht mehr einer spezifischen betroffenen Person zugeordnet werden "
        + "k&ouml;nnen, sofern diese zus&auml;tzlichen Informationen gesondert aufbewahrt werden und technischen und organisatorischen "
        + "Ma&szlig;nahmen unterliegen, die gew&auml;hrleisten, dass die personenbezogenen Daten nicht einer identifizierten oder "
        + "identifizierbaren nat&uuml;rlichen Person zugewiesen werden.<br>\n" + "<br>\n"
        + "&bdquo;Profiling&ldquo; jede Art der automatisierten Verarbeitung personenbezogener Daten, die darin besteht, dass diese "
        + "personenbezogenen Daten verwendet werden, um bestimmte pers&ouml;nliche Aspekte, die sich auf eine nat&uuml;rliche Person "
        + "beziehen, zu bewerten, insbesondere um Aspekte bez&uuml;glich Arbeitsleistung, wirtschaftliche Lage, Gesundheit, "
        + "pers&ouml;nliche Vorlieben, Interessen, Zuverl&auml;ssigkeit, Verhalten, Aufenthaltsort oder Ortswechsel dieser nat&uuml;rlichen "
        + "Person zu analysieren oder vorherzusagen.<br>\n" + "<br>\n"
        + "Als &bdquo;Verantwortlicher&ldquo; wird die nat&uuml;rliche oder juristische Person, Beh&ouml;rde, Einrichtung oder andere Stelle, die "
        + "allein oder gemeinsam mit anderen &uuml;ber die Zwecke und Mittel der Verarbeitung von personenbezogenen Daten "
        + "entscheidet, bezeichnet.<br>\n" + "<br>\n"
        + "&bdquo;Auftragsverarbeiter&ldquo; eine nat&uuml;rliche oder juristische Person, Beh&ouml;rde, Einrichtung oder andere Stelle, die "
        + "personenbezogene Daten im Auftrag des Verantwortlichen verarbeitet.<br>\n"
        + "</p><h3 id=\"dsg-general-legalbasis\">Ma&szlig;gebliche Rechtsgrundlagen</h3><p>Nach Ma&szlig;gabe des Art. 13 DSGVO teilen "
        + "wir Ihnen die Rechtsgrundlagen unserer Datenverarbeitungen mit. Sofern die Rechtsgrundlage in der "
        + "Datenschutzerkl&auml;rung nicht genannt wird, gilt Folgendes: Die Rechtsgrundlage f&uuml;r die Einholung von Einwilligungen "
        + "ist Art. 6 Abs. 1 lit. a und Art. 7 DSGVO, die Rechtsgrundlage f&uuml;r die Verarbeitung zur Erf&uuml;llung unserer "
        + "Leistungen und Durchf&uuml;hrung vertraglicher Ma&szlig;nahmen sowie Beantwortung von Anfragen ist Art. 6 Abs. 1 lit. b "
        + "DSGVO, die Rechtsgrundlage f&uuml;r die Verarbeitung zur Erf&uuml;llung unserer rechtlichen Verpflichtungen ist Art. 6 "
        + "Abs. 1 lit. c DSGVO, und die Rechtsgrundlage f&uuml;r die Verarbeitung zur Wahrung unserer berechtigten Interessen ist "
        + "Art. 6 Abs. 1 lit. f DSGVO. F&uuml;r den Fall, dass lebenswichtige Interessen der betroffenen Person oder einer anderen "
        + "nat&uuml;rlichen Person eine Verarbeitung personenbezogener Daten erforderlich machen, dient Art. 6 Abs. 1 lit. d DSGVO "
        + "als Rechtsgrundlage.</p><h3 id=\"dsg-general-securitymeasures\">Sicherheitsma&szlig;nahmen</h3><p>Wir treffen nach "
        + "Ma&szlig;gabe des Art. 32 DSGVO unter Ber&uuml;cksichtigung des Stands der Technik, der Implementierungskosten und der Art, "
        + "des Umfangs, der Umst&auml;nde und der Zwecke der Verarbeitung sowie der unterschiedlichen Eintrittswahrscheinlichkeit "
        + "und Schwere des Risikos f&uuml;r die Rechte und Freiheiten nat&uuml;rlicher Personen, geeignete technische und "
        + "organisatorische Ma&szlig;nahmen, um ein dem Risiko angemessenes Schutzniveau zu gew&auml;hrleisten.<br>\n"
        + "<br>\n"
        + "Zu den Ma&szlig;nahmen geh&ouml;ren insbesondere die Sicherung der Vertraulichkeit, Integrit&auml;t und Verf&uuml;gbarkeit von Daten "
        + "durch Kontrolle des physischen Zugangs zu den Daten, als auch des sie betreffenden Zugriffs, der Eingabe, "
        + "Weitergabe, der Sicherung der Verf&uuml;gbarkeit und ihrer Trennung. Des Weiteren haben wir Verfahren eingerichtet, "
        + "die eine Wahrnehmung von Betroffenenrechten, L&ouml;schung von Daten und Reaktion auf Gef&auml;hrdung der Daten "
        + "gew&auml;hrleisten. Ferner ber&uuml;cksichtigen wir den Schutz personenbezogener Daten bereits bei der Entwicklung, bzw. "
        + "Auswahl von Hardware, Software sowie Verfahren, entsprechend dem Prinzip des Datenschutzes durch Technikgestaltung "
        + "und durch datenschutzfreundliche Voreinstellungen (Art. 25 DSGVO).<br>\n"
        + "</p><h3 id=\"dsg-general-coprocessing\">Zusammenarbeit mit Auftragsverarbeitern und Dritten</h3>"
        + "<p>Sofern wir im Rahmen unserer Verarbeitung Daten gegen&uuml;ber anderen Personen und Unternehmen "
        + "(Auftragsverarbeitern oder Dritten) offenbaren, sie an diese &uuml;bermitteln oder ihnen sonst Zugriff auf die Daten "
        + "gew&auml;hren, erfolgt dies nur auf Grundlage einer gesetzlichen Erlaubnis (z.B. wenn eine &Uuml;bermittlung der Daten an "
        + "Dritte, wie an Zahlungsdienstleister, gem. Art. 6 Abs. 1 lit. b DSGVO zur Vertragserf&uuml;llung erforderlich ist), "
        + "Sie eingewilligt haben, eine rechtliche Verpflichtung dies vorsieht oder auf Grundlage unserer berechtigten "
        + "Interessen (z.B. beim Einsatz von Beauftragten, Webhostern, etc.). <br>\n" + "<br>\n"
        + "Sofern wir Dritte mit der Verarbeitung von Daten auf Grundlage eines sog. &bdquo;Auftragsverarbeitungsvertrages&ldquo; "
        + "beauftragen, geschieht dies auf Grundlage des Art. 28 DSGVO.</p><h3 id=\"dsg-general-thirdparty\">&Uuml;bermittlungen "
        + "in Drittl&auml;nder</h3><p>Es erfolgt keine &Uuml;bermittlung von Daten in ein Drittland (d.h. au&szlig;erhalb der Europ&auml;ischen "
        + "Union (EU) oder des Europ&auml;ischen Wirtschaftsraums (EWR))!</p><h3 id=\"dsg-general-rightssubject\">Rechte der "
        + "betroffenen Personen</h3><p>Sie haben das Recht, eine Best&auml;tigung dar&uuml;ber zu verlangen, ob betreffende Daten "
        + "verarbeitet werden und auf Auskunft &uuml;ber diese Daten sowie auf weitere Informationen und Kopie der Daten "
        + "entsprechend Art. 15 DSGVO.<br>\n" + "<br>\n"
        + "Sie haben entsprechend. Art. 16 DSGVO das Recht, die Vervollst&auml;ndigung der Sie betreffenden Daten oder die "
        + "Berichtigung der Sie betreffenden unrichtigen Daten zu verlangen.<br>\n" + "<br>\n"
        + "Sie haben nach Ma&szlig;gabe des Art. 17 DSGVO das Recht zu verlangen, dass betreffende Daten unverz&uuml;glich gel&ouml;scht "
        + "werden, bzw. alternativ nach Ma&szlig;gabe des Art. 18 DSGVO eine Einschr&auml;nkung der Verarbeitung der Daten zu "
        + "verlangen.<br>\n" + "<br>\n"
        + "Sie haben das Recht zu verlangen, dass die Sie betreffenden Daten, die Sie uns bereitgestellt haben nach Ma&szlig;gabe "
        + "des Art. 20 DSGVO zu erhalten und deren &Uuml;bermittlung an andere Verantwortliche zu fordern. <br>\n" + "<br>\n"
        + "Sie haben ferner gem. Art. 77 DSGVO das Recht, eine Beschwerde bei der zust&auml;ndigen Aufsichtsbeh&ouml;rde einzureichen."
        + "</p><h3 id=\"dsg-general-revokeconsent\">Widerrufsrecht</h3>"
        + "<p>Sie haben das Recht, erteilte Einwilligungen gem. Art. 7 Abs. 3 DSGVO mit Wirkung f&uuml;r die Zukunft zu "
        + "widerrufen</p>" + "<h3 id=\"dsg-general-object\">Widerspruchsrecht</h3>"
        + "<p>Sie k&ouml;nnen der k&uuml;nftigen Verarbeitung der Sie betreffenden Daten nach Ma&szlig;gabe des Art. 21 DSGVO jederzeit"
        + " widersprechen. Der Widerspruch kann insbesondere gegen die Verarbeitung f&uuml;r Zwecke der Direktwerbung erfolgen.</p>"
        + "<h3 id=\"dsg-general-cookies\">Cookies und Widerspruchsrecht bei Direktwerbung</h3>"
        + "<p>Als &bdquo;Cookies&ldquo; werden kleine Dateien bezeichnet, die auf Rechnern der Nutzer gespeichert werden. Innerhalb der "
        + "Cookies k&ouml;nnen unterschiedliche Angaben gespeichert werden. Ein Cookie dient prim&auml;r dazu, die Angaben zu einem "
        + "Nutzer (bzw. dem Ger&auml;t auf dem das Cookie gespeichert ist) w&auml;hrend oder auch nach seinem Besuch innerhalb eines "
        + "Onlineangebotes zu speichern. Als tempor&auml;re Cookies, bzw. &bdquo;Session-Cookies&ldquo; oder &bdquo;transiente Cookies&ldquo;, werden "
        + "Cookies bezeichnet, die gel&ouml;scht werden, nachdem ein Nutzer ein Onlineangebot verl&auml;sst und seinen Browser "
        + "schlie&szlig;t. In einem solchen Cookie kann z.B. der Inhalt eines Warenkorbs in einem Onlineshop oder ein Login-Status "
        + "gespeichert werden. Als &bdquo;permanent&ldquo; oder &bdquo;persistent&ldquo; werden Cookies bezeichnet, die auch nach dem Schlie&szlig;en des "
        + "Browsers gespeichert bleiben. So kann z.B. der Login-Status gespeichert werden, wenn die Nutzer diese nach "
        + "mehreren Tagen aufsuchen. Ebenso k&ouml;nnen in einem solchen Cookie die Interessen der Nutzer gespeichert werden, "
        + "die f&uuml;r Reichweitenmessung oder Marketingzwecke verwendet werden. Als &bdquo;Third-Party-Cookie&ldquo; werden Cookies "
        + "bezeichnet, die von anderen Anbietern als dem Verantwortlichen, der das Onlineangebot betreibt, angeboten "
        + "werden (andernfalls, wenn es nur dessen Cookies sind spricht man von &bdquo;First-Party Cookies&ldquo;).<br>\n"
        + "<br>\n"
        + "Wir setzen ausschlie&szlig;lich tempor&auml;re Cookies zum speichern der aktuellen Sitzungsdaten (angemeldeter Benutzer, "
        + "aktuell verarbeiteter Datensatz) ein, die nach schlie&szlig;en des Browsers von diesem gel&ouml;scht werden sollten.<br>\n"
        + "<br>\n"
        + "Es werden keine Cookies zum Zwecken des Onlinemarketing oder User-Trackings verwendet. Es werden keine permanenten "
        + "Cookies gesetzt und der Zugriff auf die tempor&auml;ren Cookies ist auf die eigene Seite begrenzt.</p>"
        + "<h3 id=\"dsg-general-erasure\">L&ouml;schung von Daten</h3>"
        + "<p>Die von uns verarbeiteten Daten werden nach Ma&szlig;gabe der Art. 17 und 18 DSGVO gel&ouml;scht oder in ihrer "
        + "Verarbeitung eingeschr&auml;nkt. Sofern nicht im Rahmen dieser Datenschutzerkl&auml;rung ausdr&uuml;cklich angegeben, werden die "
        + "bei uns gespeicherten Daten gel&ouml;scht, sobald sie f&uuml;r ihre Zweckbestimmung nicht mehr erforderlich sind und der "
        + "L&ouml;schung keine gesetzlichen Aufbewahrungspflichten entgegenstehen. Sofern die Daten nicht gel&ouml;scht werden, weil "
        + "sie f&uuml;r andere und gesetzlich zul&auml;ssige Zwecke erforderlich sind, wird deren Verarbeitung eingeschr&auml;nkt. D.h. die "
        + "Daten werden gesperrt und nicht f&uuml;r andere Zwecke verarbeitet. Das gilt z.B. f&uuml;r Daten, die aus handels- oder "
        + "steuerrechtlichen Gr&uuml;nden aufbewahrt werden m&uuml;ssen.<br>\n" + "<br>\n"
        + "Nach gesetzlichen Vorgaben in Deutschland, erfolgt die Aufbewahrung insbesondere f&uuml;r 10 Jahre gem&auml;&szlig; &sect;&sect; 147 Abs. 1 "
        + "AO, 257 Abs. 1 Nr. 1 und 4, Abs. 4 HGB (B&uuml;cher, Aufzeichnungen, Lageberichte, Buchungsbelege, Handelsb&uuml;cher, f&uuml;r "
        + "Besteuerung relevanter Unterlagen, etc.) und 6 Jahre gem&auml;&szlig; &sect; 257 Abs. 1 Nr. 2 und 3, Abs. 4 HGB "
        + "(Handelsbriefe).<br>\n" + "<br>\n"
        + "Nach gesetzlichen Vorgaben in &Ouml;sterreich erfolgt die Aufbewahrung insbesondere f&uuml;r 7 J gem&auml;&szlig; &sect; 132 Abs. 1 BAO "
        + "(Buchhaltungsunterlagen, Belege/Rechnungen, Konten, Belege, Gesch&auml;ftspapiere, Aufstellung der Einnahmen und "
        + "Ausgaben, etc.), f&uuml;r 22 Jahre im Zusammenhang mit Grundst&uuml;cken und f&uuml;r 10 Jahre bei Unterlagen im Zusammenhang "
        + "mit elektronisch erbrachten Leistungen, Telekommunikations-, Rundfunk- und Fernsehleistungen, die an "
        + "Nichtunternehmer in EU-Mitgliedstaaten erbracht werden und f&uuml;r die der Mini-One-Stop-Shop (MOSS) in Anspruch "
        + "genommen wird.</p><p></p><h3 id=\"dsg-registration\">Registrierfunktion</h3><p></p>"
        + "<p><span class=\"ts-muster-content\">Nutzer k&ouml;nnen ein Nutzerkonto anlegen. Im Rahmen der Registrierung werden "
        + "die erforderlichen Pflichtangaben den Nutzern mitgeteilt und auf Grundlage des Art. 6 Abs. 1 lit. b DSGVO zu "
        + "Zwecken der Bereitstellung des Nutzerkontos verarbeitet. Zu den verarbeiteten Daten geh&ouml;ren insbesondere die "
        + "Login-Informationen (Name, Passwort sowie eine E-Mailadresse). Die im Rahmen der Registrierung eingegebenen "
        + "Daten werden f&uuml;r die Zwecke der Nutzung des Nutzerkontos und dessen Zwecks verwendet. <br>\n" + "<br>\n"
        + "Die Nutzer k&ouml;nnen &uuml;ber Informationen, die f&uuml;r deren Nutzerkonto relevant sind, wie z.B. technische &Auml;nderungen, "
        + "per E-Mail informiert werden. Wenn Nutzer ihr Nutzerkonto gek&uuml;ndigt haben, werden deren Daten im Hinblick auf das "
        + "Nutzerkonto, vorbehaltlich einer gesetzlichen Aufbewahrungspflicht, gel&ouml;scht. Es obliegt den Nutzern, ihre Daten "
        + "bei erfolgter K&uuml;ndigung vor dem Vertragsende zu sichern. Wir sind berechtigt, s&auml;mtliche w&auml;hrend der Vertragsdauer "
        + "gespeicherten Daten des Nutzers unwiederbringlich zu l&ouml;schen.<br>\n" + "<br>\n"
        + "Im Rahmen der Inanspruchnahme unserer Registrierungs- und Anmeldefunktionen sowie der Nutzung des Nutzerkontos, "
        + "speichern wir die IP-Adresse und den Zeitpunkt der jeweiligen Nutzerhandlung. Die Speicherung erfolgt auf "
        + "Grundlage unserer berechtigten Interessen, als auch der Nutzer an Schutz vor Missbrauch und sonstiger unbefugter "
        + "Nutzung. Eine Weitergabe dieser Daten an Dritte erfolgt grunds&auml;tzlich nicht, au&szlig;er sie ist zur Verfolgung unserer "
        + "Anspr&uuml;che erforderlich oder es besteht hierzu besteht eine gesetzliche Verpflichtung gem. Art. 6 Abs. 1 lit. c. "
        + "DSGVO. Die IP-Adressen werden sp&auml;testens nach 7 Tagen anonymisiert oder gel&ouml;scht.<br>\n"
        + "</span></p><p></p><h3 id=\"dsg-newsletter-de\">Newsletter</h3>"
        + "<p></p><p><span class=\"ts-muster-content\">Mit den nachfolgenden Hinweisen informieren wir Sie &uuml;ber die Inhalte "
        + "unseres Newsletters sowie das Anmelde-, Versand- und das statistische Auswertungsverfahren sowie Ihre "
        + "Widerspruchsrechte auf. Indem Sie unseren Newsletter abonnieren, erkl&auml;ren Sie sich mit dem Empfang und den "
        + "beschriebenen Verfahren einverstanden.<br>\n" + "<br>\n"
        + "Inhalt des Newsletters: Wir versenden Newsletter, E-Mails und weitere elektronische Benachrichtigungen mit "
        + "aktuellen Informationen zu dem registrierten Service (nachfolgend &bdquo;Newsletter&ldquo;) nur mit der Einwilligung der "
        + "Empf&auml;nger oder einer gesetzlichen Erlaubnis. Sofern im Rahmen einer Anmeldung zum Newsletter dessen Inhalte "
        + "konkret umschrieben werden, sind sie f&uuml;r die Einwilligung der Nutzer ma&szlig;geblich. Im &Uuml;brigen enthalten unsere "
        + "Newsletter Informationen zu unseren Leistungen und uns.<br>\n" + "<br>\n"
        + "Double-Opt-In und Protokollierung: Die Anmeldung zu unserem Newsletter erfolgt im Rahmen des "
        + "Registrierungsprozesses im Double-Opt-In-Verfahren. D.h. Sie erhalten nach der Anmeldung eine E-Mail, in der Sie "
        + "um die Best&auml;tigung Ihrer Anmeldung gebeten werden. Diese Best&auml;tigung ist notwendig, damit sich niemand mit fremden "
        + "E-Mailadressen anmelden kann. Die Anmeldungen zum Newsletter werden protokolliert, um den Anmeldeprozess "
        + "entsprechend den rechtlichen Anforderungen nachweisen zu k&ouml;nnen. Hierzu geh&ouml;rt die Speicherung des Anmelde- und "
        + "des Best&auml;tigungszeitpunkts, als auch der IP-Adresse. Ebenso werden die &Auml;nderungen Ihrer bei dem "
        + "Versanddienstleister gespeicherten Daten protokolliert.<br>\n" + "<br>\n"
        + "Anmeldedaten: Die Anmeldung zum Newsletter ist nur im Rahmen einer Registrierung f&uuml;r den Service m&ouml;glich, es "
        + "werden keine weiteren Informationen erhoben.<br>\n" + "<br>\n"
        + "Der Versand des Newsletters und die mit ihm verbundene Erfolgsmessung erfolgen auf Grundlage einer Einwilligung "
        + "der Empf&auml;nger gem. Art. 6 Abs. 1 lit. a, Art. 7 DSGVO i.V.m &sect; 7 Abs. 2 Nr. 3 UWG oder falls eine Einwilligung "
        + "nicht erforderlich ist, auf Grundlage unserer berechtigten Interessen am Direktmarketing gem. Art. 6 Abs. 1 lt. f. "
        + "DSGVO i.V.m. &sect; 7 Abs. 3 UWG. <br>\n" + "<br>\n"
        + "Die Protokollierung des Anmeldeverfahrens erfolgt auf Grundlage unserer berechtigten Interessen gem. Art. 6 Abs. "
        + "1 lit. f DSGVO. Unser Interesse richtet sich auf den Einsatz eines nutzerfreundlichen sowie sicheren "
        + "Newslettersystems, das sowohl unseren gesch&auml;ftlichen Interessen dient, als auch den Erwartungen der Nutzer "
        + "entspricht und uns ferner den Nachweis von Einwilligungen erlaubt.<br>\n" + "<br>\n"
        + "K&uuml;ndigung/Widerruf - Sie k&ouml;nnen den Empfang unseres Newsletters jederzeit k&uuml;ndigen, d.h. Ihre Einwilligungen "
        + "widerrufen. Die K&uuml;ndigung erfolgt &uuml;ber die Daten &auml;ndern Funktion. Wir k&ouml;nnen die ausgetragenen E-Mailadressen "
        + "bis zu drei Jahren auf Grundlage unserer berechtigten Interessen speichern bevor wir sie l&ouml;schen, um eine ehemals "
        + "gegebene Einwilligung nachweisen zu k&ouml;nnen. Die Verarbeitung dieser Daten wird auf den Zweck einer m&ouml;glichen "
        + "Abwehr von Anspr&uuml;chen beschr&auml;nkt. Ein individueller L&ouml;schungsantrag ist jederzeit m&ouml;glich, sofern zugleich das "
        + "ehemalige Bestehen einer Einwilligung best&auml;tigt wird.</span></p><p></p><h3 id=\"dsg-hostingprovider\">Hosting und "
        + "E-Mail-Versand</h3><p></p><p><span class=\"ts-muster-content\">Die von uns in Anspruch genommenen "
        + "Hosting-Leistungen dienen der Zurverf&uuml;gungstellung der folgenden Leistungen: Infrastruktur- und "
        + "Plattformdienstleistungen, Rechenkapazit&auml;t, Speicherplatz und Datenbankdienste, E-Mail-Versand, "
        + "Sicherheitsleistungen sowie technische Wartungsleistungen, die wir zum Zwecke des Betriebs dieses Onlineangebotes "
        + "einsetzen. <br>\n" + "<br>\n"
        + "Hierbei verarbeiten wir, bzw. unser Hostinganbieter Bestandsdaten, Kontaktdaten, Inhaltsdaten, Vertragsdaten, "
        + "Nutzungsdaten, Meta- und Kommunikationsdaten von Kunden, Interessenten und Besuchern dieses Onlineangebotes auf "
        + "Grundlage unserer berechtigten Interessen an einer effizienten und sicheren Zurverf&uuml;gungstellung dieses "
        + "Onlineangebotes gem. Art. 6 Abs. 1 lit. f DSGVO i.V.m. Art. 28 DSGVO (Abschluss Auftragsverarbeitungsvertrag)."
        + "</span></p><p></p><h3 id=\"dsg-logfiles\">Erhebung von Zugriffsdaten und Logfiles</h3><p></p><p>"
        + "<span class=\"ts-muster-content\">Wir, bzw. unser Hostinganbieter, erhebt auf Grundlage unserer berechtigten "
        + "Interessen im Sinne des Art. 6 Abs. 1 lit. f. DSGVO Daten &uuml;ber jeden Zugriff auf den Server, auf dem sich dieser "
        + "Dienst befindet (sogenannte Serverlogfiles). Zu den Zugriffsdaten geh&ouml;ren Name der abgerufenen Webseite, Datei, "
        + "Datum und Uhrzeit des Abrufs, &uuml;bertragene Datenmenge, Meldung &uuml;ber erfolgreichen Abruf, Browsertyp nebst Version, "
        + "das Betriebssystem des Nutzers, Referrer URL (die zuvor besuchte Seite), IP-Adresse und der anfragende Provider."
        + "<br>\n" + "<br>\n"
        + "Logfile-Informationen werden aus Sicherheitsgr&uuml;nden (z.B. zur Aufkl&auml;rung von Missbrauchs- oder Betrugshandlungen) "
        + "f&uuml;r die Dauer von maximal 7 Tagen gespeichert und danach gel&ouml;scht. Daten, deren weitere Aufbewahrung zu "
        + "Beweiszwecken erforderlich ist, sind bis zur endg&uuml;ltigen Kl&auml;rung des jeweiligen Vorfalls von der L&ouml;schung "
        + "ausgenommen.</span></p>"
        + "<a href=\"https://datenschutz-generator.de\" class=\"dsg1-5\" rel=\"nofollow\" target=\"_blank\">Erstellt mit "
        + "Datenschutz-Generator.de von RA Dr. Thomas Schwenke</a>\n";
  }

  /**
   * The Method <code>preventFromCache</code> tells you if this page should be cached or not.
   *
   * @param session Data of the current session
   * @return don't cache this site = true
   */
  @Override
  public boolean preventFromCache(final HttpSession session) {
    return false;
  }

  /**
   * The Method <code>allowedToSee</code> tells you if the currently logged in user is allowed to access this application.
   *
   * @param session Data of the current session
   * @return true if it is allowed for this user
   */
  @Override
  public boolean allowedToSee(final HttpSession session) {
    return true;
  }

  /**
   * The Method <code>allowedToChange</code> tells you if the currently logged in user is allowed to change data in this
   * application.
   *
   * @param session Data of the current session
   * @return true if it is allowed for this user
   */
  @Override
  public boolean allowedToChange(final HttpSession session) {
    return true;
  }

}
