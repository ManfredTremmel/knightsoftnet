/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.knightsoftnet.bettingpool;

import de.knightsoft.common.AbstractVisualDb;
import de.knightsoft.common.TextException;
import de.knightsoft.common.field.AbstractBaseField;
import de.knightsoft.common.field.CheckBoxField;
import de.knightsoft.common.field.DateTimeField;
import de.knightsoft.common.field.NumberField;
import de.knightsoft.common.field.PopupField;
import de.knightsoft.knightsoftnet.common.KnConst;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Map;

/**
 * The <code>AdminSpiele</code> class is based on the abstract class de.knightsoft.common.AbstractVisualDb and implements the
 * administration of the Games in the KnightSoft Tipprunde.
 *
 * @author Manfred Tremmel
 * @version 2.0.0, 06.08.2006
 */
public class AdminSpiele extends de.knightsoft.common.AbstractVisualDb {

  private static final String POP_UP_SESSION_SPIELE_VALUE = "PopUpSessionSpieleValue";
  private static final String POP_UP_SESSION_SPIELE_VALUE_DISPLAY = "PopUpSessionSpieleValueDisplay";
  private static final String POP_UP_SQL_STRING = //
      "SELECT       tab1.lfd_liga_ms AS value, " //
          + "             tab2.zaehler AS mannschafts_nummer, " //
          + "             tab2.bezeichnung AS valuedisplay " //
          + "FROM         KnightSoft_TippLigaMannschaft AS tab1 " //
          + "   LEFT JOIN KnightSoft_TippMannschaften AS tab2 " //
          + "    ON       tab1.Mandator = tab2.Mandator " + //
          "    AND      tab1.zaehler = tab2.zaehler " //
          + "WHERE        tab1.Mandator = ? " //
          + "    AND      tab1.saison_liga = ? " //
          + "ORDER BY     valuedisplay";

  private final String readCurrentSql;
  private final String readPosCountRealSql;
  private final String readUserSql;

  /**
   * Constructor.
   *
   * @param thisDatabase Connection to database
   * @param servletname Name of the Servlet
   * @exception SQLException if a database access error occurs
   * @exception TextException to give out a text message
   * @see TextException
   */
  public AdminSpiele(final Connection thisDatabase, final String servletname) throws SQLException, TextException {
    super(thisDatabase, KnConst.HTML_BASE, servletname, KnConst.GIF_URL + "16x16/spieltag.png", "Spieltag",
        "KnightSoft-Net Tipprundenadministration - Spiele", "KnightSoft_TippLiga",
        new de.knightsoft.common.field.AbstractBaseField[] {
            new NumberField(servletname, null, "saison_liga", "saison_liga", 9, 9, true)},
        0, "Spiele übernehmen",

        "KnightSoft_TippSpiele",
        new de.knightsoft.common.field.AbstractBaseField[] {
            new NumberField(servletname, null, "saison_liga", "saison_liga", 9, 9, true, Long.valueOf(199800001L),
                Long.valueOf(999999999L)),
            new NumberField(servletname, null, "spieltag", "spieltag", 9, 9, true, Long.valueOf(1L), Long.valueOf(255L)),
            new NumberField(servletname, null, "lfd_spiele_nr", "lfd_spiele_nr", 9, 9, true, Long.valueOf(0L),
                Long.valueOf(255L)),
            new PopupField(servletname, "Heimmannschaft", "mannschaft_heim", "mannschaft_heim", 1, 1, true,
                servletname + AdminSpiele.POP_UP_SESSION_SPIELE_VALUE,
                servletname + AdminSpiele.POP_UP_SESSION_SPIELE_VALUE_DISPLAY),
            new PopupField(servletname, "Auswärtsmannschaft", "mannschaft_ausw", "mannschaft_ausw", 1, 1, true,
                servletname + AdminSpiele.POP_UP_SESSION_SPIELE_VALUE,
                servletname + AdminSpiele.POP_UP_SESSION_SPIELE_VALUE_DISPLAY),
            new NumberField(servletname, "Heimtore", "tore_heim", "tore_heim", 2, 3, false, Long.valueOf(0L),
                Long.valueOf(255L)),
            new NumberField(servletname, "Ausw.Tore", "tore_ausw", "tore_ausw", 2, 3, false, Long.valueOf(0L),
                Long.valueOf(255L)),
            new DateTimeField(servletname, "Spielbeginn", "spielbeginn", "spielbeginn", true),
            new CheckBoxField(servletname, "Beendet", "spiel_beendet", "spiel_beendet", //
                1, 1, true)},
        1, 0, false, false);

    // Find Min and Max Entry of the DataBase
    readMinMaxSql = "SELECT 1 AS dbmin, " + "       ((anz_manschaften - 1) * 2) AS dbmax " + "FROM   " + dataBaseTable + " "
        + "WHERE  Mandator = ? " + " AND   " + keyField + " = ? ";

    // Find Entry to work with, when no one is selected
    readCurrentSql = "SELECT spieltag AS aktspieltag " + "FROM   KnightSoft_TippSpiele " + "WHERE  Mandator = ? "
        + " AND   saison_liga = ? " + " AND   spiel_beendet = ? " + "ORDER BY spielbeginn LIMIT 1";

    // Position count
    readPosCountSql = "SELECT CEIL(anz_manschaften / 2) AS num_positions " + "FROM   " + dataBaseTable + " "
        + "WHERE  Mandator =  ? " + " AND   " + positionDbFields[positionNumberReferenceField].getdbfieldname() + " = ? ";

    // really count positions
    readPosCountRealSql = "SELECT COUNT(*) AS num_positions " + "FROM   " + positionDataBaseTable + " "
        + "WHERE  Mandator =  ? " + " AND   " + positionDbFields[positionNumberReferenceField].getdbfieldname() + " = ? "
        + " AND   " + positionDbFields[positionNumberKeyField].getdbfieldname() + " = ? ";

    // read all positions
    readAllPosSql = "SELECT * " + "FROM   " + positionDataBaseTable + " " + "WHERE  Mandator =  ? " + " AND   "
        + positionDbFields[positionNumberReferenceField].getdbfieldname() + " = ? " + " AND   "
        + positionDbFields[positionNumberKeyField].getdbfieldname() + " = ? " + "ORDER BY "
        + positionDbFields[positionNumberKeyField + 1].getdbfieldname();

    // delete a position entry
    deletePositionSql = "DELETE FROM " + positionDataBaseTable + " " + "WHERE  Mandator = ? " + " AND   "
        + positionDbFields[positionNumberReferenceField].getdbfieldname() + " = ? " + " AND   "
        + positionDbFields[positionNumberKeyField].getdbfieldname() + " = ? " + " AND   "
        + positionDbFields[positionNumberKeyField + 1].getdbfieldname() + " = ? ";

    readUserSql = "SELECT   stufe " //
        + "FROM     KnightSoft_TippMitspieler " //
        + "WHERE    Mandator = 1 AND " //
        + "         spitzname = ? AND " //
        + "         passwort = " + myDataBaseDepending.getSqlPassword(" ? ");

    // insert a new position
    final StringBuilder insertPositionSqlA = new StringBuilder();
    final StringBuilder insertPositionSqlB = new StringBuilder();
    insertPositionSqlA.append("Mandator");
    insertPositionSqlB.append('?');
    for (final AbstractBaseField positionDbField : positionDbFields) {
      if (positionDbField.getdbfieldname() != null) {
        positionDbField.sqlInsertNames(insertPositionSqlA);
        positionDbField.sqlPrepareInsertItems(insertPositionSqlB);
      }
    }
    insertPositionSql = "INSERT INTO " + positionDataBaseTable + " (" + insertPositionSqlA.toString() + ") VALUES ("
        + insertPositionSqlB.toString() + ")";

    // update a position
    final StringBuilder updatePositionSb = new StringBuilder();
    for (int i = 3; i < positionDbFields.length; i++) {
      positionDbFields[i].sqlUpdateName(updatePositionSb);
    }
    updatePositionSql = "UPDATE " + positionDataBaseTable + " " //
        + "SET    " + updatePositionSb.toString() //
        + " " + "WHERE  Mandator = ? " //
        + " AND   " + positionDbFields[positionNumberReferenceField].getdbfieldname() + " = ? " + " AND   "
        + positionDbFields[positionNumberKeyField].getdbfieldname() + " = ? " //
        + " AND   " + positionDbFields[positionNumberKeyField + 1].getdbfieldname() + " = ? ";

    // read a position
    readPositionSql = "SELECT * " + "FROM   " + positionDataBaseTable + " " + "WHERE  Mandator = ? " + " AND   "
        + positionDbFields[positionNumberReferenceField].getdbfieldname() + " = ? " + " AND   "
        + positionDbFields[positionNumberKeyField].getdbfieldname() + " = ? " + " AND   "
        + positionDbFields[positionNumberKeyField + 1].getdbfieldname() + " = ? ";
  }

  /**
   * The Method <code>fillMinMax</code> reads the min and max keys in the database.
   *
   * @param session Data of the current session
   * @exception java.sql.SQLException
   */
  @Override
  protected void fillMinMax(final HttpSession session) throws java.sql.SQLException {
    if (session != null && readMinMaxSql != null) {

      final String saisonLiga = (String) session.getAttribute(servletName + dataBaseTable + "." + keyField);
      final Integer mandatorInteger =
          (Integer) session.getAttribute(servletName + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_MANDATOR);
      final int mandator = mandatorInteger == null ? 1 : mandatorInteger.intValue();
      String dbmin = null;
      String dbmax = null;
      String dbnumber = null;

      try (final PreparedStatement preparedStatement = myDataBase.prepareStatement(readMinMaxSql)) {

        preparedStatement.clearParameters();
        preparedStatement.setInt(1, mandator);
        preparedStatement.setString(2, saisonLiga);
        try (final ResultSet resultMinMax = preparedStatement.executeQuery()) {
          if (resultMinMax.next()) {
            dbmin = Integer.toString(resultMinMax.getInt("dbmin"));
            dbmax = Integer.toString(resultMinMax.getInt("dbmax"));
            dbnumber = (String) session.getAttribute(servletName + positionDataBaseTable + "." + saisonLiga + "."
                + positionDbFields[positionNumberKeyField].getdbfieldname());
          }
        }
      }
      if (StringUtils.isEmpty(dbnumber)) {
        try (final PreparedStatement preparedStatement = myDataBase.prepareStatement(readCurrentSql)) {
          preparedStatement.clearParameters();
          preparedStatement.setInt(1, mandator);
          preparedStatement.setString(2, saisonLiga);
          preparedStatement.setBoolean(3, false);
          try (final ResultSet resultCurrent = preparedStatement.executeQuery()) {
            if (resultCurrent.next()) {
              dbnumber = resultCurrent.getString("aktspieltag");
            } else {
              dbnumber = dbmin;
            }
          }
        }
        setDbNumber(session, dbnumber);
      }
      session.setAttribute(servletName + AbstractVisualDb.DBMIN_TEXT + serviceName, dbmin);
      session.setAttribute(servletName + AbstractVisualDb.DBMAX_TEXT + serviceName, dbmax);
    }
  }

  /**
   * The Method <code>setDBNumber</code> switches to a new DataBases number.
   *
   * @param session The Data of the current Session
   * @param number The new key/number of the DataBase
   */
  @Override
  public void setDbNumber(final HttpSession session, final String number) {
    if (session != null) {
      final String SaisonLiga = (String) session.getAttribute(servletName + dataBaseTable + "." + keyField);

      session.setAttribute(servletName + positionDataBaseTable + "." + SaisonLiga + "."
          + positionDbFields[positionNumberKeyField].getdbfieldname(), number);
      session.removeAttribute(servletName + AbstractVisualDb.NUMBER_POS_TEXT + serviceName);
    }
  }

  /**
   * The Method <code>readDBNumber</code> reads the last used DB-Key-Field as current number.
   *
   * @param session The Data of the current Session
   */
  @Override
  public void readDbNumber(final HttpSession session) {
    if (session != null) {
      final String SaisonLiga = (String) session.getAttribute(servletName + dataBaseTable + "." + keyField);
      final String SessionKey =
          positionDataBaseTable + "." + SaisonLiga + "." + positionDbFields[positionNumberKeyField].getdbfieldname();

      String dbnumber = (String) session.getAttribute(servletName + SessionKey);
      if (dbnumber == null) {
        dbnumber = (String) session.getAttribute(servletName + AbstractVisualDb.DBMAX_TEXT + serviceName);
        session.setAttribute(servletName + SessionKey, dbnumber);
        session.removeAttribute(servletName + AbstractVisualDb.NUMBER_POS_TEXT + serviceName);
      }
    }
  }

  /**
   * The Method <code>readDBNumberWOOF</code> reads the last used DB-Key-Field as current number without Other Field.
   *
   * @param session The Data of the current Session
   */
  @Override
  public String readDbNumberWoof(final HttpSession session) {
    String dbnumber = null;
    if (session != null) {
      final String SaisonLiga = (String) session.getAttribute(servletName + dataBaseTable + "." + keyField);
      final String SessionKey =
          positionDataBaseTable + "." + SaisonLiga + "." + positionDbFields[positionNumberKeyField].getdbfieldname();
      dbnumber = (String) session.getAttribute(servletName + SessionKey);
    }
    return dbnumber;
  }

  /**
   * The Method <code>nextDBNumber</code> jumps to the next entry in DataBase.
   *
   * @param session The Data of the current Session
   * @exception TextException Textmessage, when something's going wrong
   * @see TextException
   */
  @Override
  protected void nextDbNumber(final HttpSession session) throws TextException {
    if (session != null) {
      final String SaisonLiga = (String) session.getAttribute(servletName + dataBaseTable + "." + keyField);
      final String SessionKey =
          positionDataBaseTable + "." + SaisonLiga + "." + positionDbFields[positionNumberKeyField].getdbfieldname();

      String dbnumber = (String) session.getAttribute(servletName + SessionKey);
      if (dbnumber == null || dbnumber.equals(StringUtils.EMPTY)) {
        dbnumber = (String) session.getAttribute(servletName + AbstractVisualDb.DBMIN_TEXT + serviceName);
      } else {
        try {
          final int dbnumint = Integer.parseInt(dbnumber);
          dbnumber = Integer.toString(dbnumint + 1);
        } catch (final Exception e) {
          throw new TextException("Keyfeld nicht nummerisch:\n" + e.toString(), e);
        }
      }
      session.setAttribute(servletName + SessionKey, dbnumber);
      session.removeAttribute(servletName + AbstractVisualDb.NUMBER_POS_TEXT + serviceName);
    }
  }

  /**
   * The Method <code>previousDBNumber</code> jumps to the previous entry in DataBase.
   *
   * @param session The Data of the current Session
   * @exception TextException Textmessage, when something's going wrong
   * @see TextException
   */
  @Override
  protected void previousDbNumber(final HttpSession session) throws TextException {
    if (session != null) {
      final String SaisonLiga = (String) session.getAttribute(servletName + dataBaseTable + "." + keyField);
      final String SessionKey =
          positionDataBaseTable + "." + SaisonLiga + "." + positionDbFields[positionNumberKeyField].getdbfieldname();

      String dbnumber = (String) session.getAttribute(servletName + SessionKey);
      if (dbnumber == null || dbnumber.equals(StringUtils.EMPTY)) {
        dbnumber = (String) session.getAttribute(servletName + AbstractVisualDb.DBMIN_TEXT + serviceName);
      } else {
        try {
          final int dbnumint = Integer.parseInt(dbnumber);
          dbnumber = Integer.toString(dbnumint - 1);
        } catch (final Exception e) {
          throw new TextException("Keyfeld nicht nummerisch:\n" + e.toString(), e);
        }
      }
      session.setAttribute(servletName + SessionKey, dbnumber);
      session.removeAttribute(servletName + AbstractVisualDb.NUMBER_POS_TEXT + serviceName);
    }
  }

  /**
   * The Method <code>HTML_Navigator</code> Creates the navigation part of the HTML page
   *
   * @param psession The Data of the current Session
   * @param pnavTyp Navigation type
   * @return HTML code of the navigation part
   * @exception java.sql.SQLException
   */
  @Override
  protected String htmlNavigator(final HttpSession psession, final String pnavTyp) throws java.sql.SQLException {
    return this.htmlNavigator(psession, pnavTyp, false, true, false, false, true, true, true, true, true, true, true);
  }

  /**
   * The Method <code>HTMLPage</code> generates the html formular.
   *
   * @param res HttpServletResponse from the Servlet
   * @param hint Hint to be displayed on the screen
   * @param session The Data of the current Session
   * @param navTyp Navigation type
   * @return HTML code of the formular
   */
  @Override
  protected String htmlPage(final HttpServletResponse res, final String hint, final HttpSession session, final String navTyp) {
    int pos = 0;
    int tabpos = 1;
    final StringBuilder sb = new StringBuilder(512);
    final String selectedFieldDefault = "tore_heim0";
    String field = "x";
    final String dbnumber = (String) session.getAttribute(servletName + dataBaseTable + "." + keyField);
    final String spielNummer = readDbNumberWoof(session);
    final String debugText = (String) session.getAttribute(servletName + AbstractVisualDb.DEBUG_TEXT_TEXT + serviceName);
    String selectedField = null;
    final Integer mandatorInteger =
        (Integer) session.getAttribute(servletName + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_MANDATOR);
    final int mandator = mandatorInteger == null ? 1 : mandatorInteger.intValue();

    try {
      if (!allowedToSee(session)) {
        return "<h1>Sie sind nicht berechtigt, diese Anwendung aufzurufen!</h1>\n";
      }
      if (allowedToChange(session)) {
        sb.append("        <form action=\"" + res.encodeURL(KnConst.HTML_BASE + "Tipprunde/" + dbnumber + "/Spieltag.html")
            + "\" method=\"POST\" enctype=\"application/x-www-form-urlencoded; charset=utf-8\" "
            + "accept-charset=\"utf-8\" OnSubmit=\"return chkForm();\">\n" + this.htmlNavigator(session, navTyp));
      } else {
        sb.append("        <form action=\"" + res.encodeURL(KnConst.HTML_BASE + "Tipprunde/" + dbnumber + "/Spieltag.html")
            + "\" method=\"POST\" enctype=\"application/x-www-form-urlencoded; charset=utf-8\" " + "accept-charset=\"utf-8\">\n"
            + this.htmlNavigator(session, navTyp));
      }
    } catch (final java.sql.SQLException e) {
      return "SQL-Fehler in HTMLPage()\n" + e.toString();
    }

    if (debugText != null) {
      sb.append("                    <p>" + de.knightsoft.common.StringToHtml.convert(debugText) + "</p>\n");
      session.removeAttribute(servletName + AbstractVisualDb.DEBUG_TEXT_TEXT + serviceName);
    }
    if (hint == null) {
      if (allowedToChange(session)) {
        sb.append("            <p>Geben Sie die gew&uuml;nschten Daten ein.</p>\n");
      } else {
        sb.append("            <p>Keine &Auml;nderungsberechtigung, Daten werden nur " + "angezeigt.</p>\n");
      }
    } else {
      sb.append("            <p><b>" + de.knightsoft.common.StringToHtml.convert(hint) + "</b></p>\n");
    }

    if (positionDataBaseTable == null || positionDbFields == null || dbnumber == null) {
      session.removeAttribute(servletName + AbstractVisualDb.NUMBER_POS_TEXT + serviceName);
    } else {
      final Integer numPosInteger =
          (Integer) session.getAttribute(servletName + AbstractVisualDb.NUMBER_POS_TEXT + serviceName);
      int numPos = 0;

      sb.append("            <table class=\"fullborder\">\n").append(headerlinePosition(session));

      if (numPosInteger == null) {
        // read data from database
        try {
          int posJ = 0;
          try (final PreparedStatement preparedStatement = myDataBase.prepareStatement(readPosCountSql)) {
            preparedStatement.clearParameters();
            preparedStatement.setInt(1, mandator);
            preparedStatement.setString(2, dbnumber);
            try (final ResultSet resultPosCount = preparedStatement.executeQuery()) {
              if (resultPosCount.next()) {
                numPos = resultPosCount.getInt("num_positions");
              }
            }
          }

          int numPosReal = -1;
          try (final PreparedStatement preparedStatement = myDataBase.prepareStatement(readPosCountRealSql)) {
            preparedStatement.clearParameters();
            preparedStatement.setInt(1, mandator);
            preparedStatement.setString(2, dbnumber);
            preparedStatement.setString(3, spielNummer);
            try (final ResultSet resultPosCountReal = preparedStatement.executeQuery()) {
              if (resultPosCountReal.next()) {
                numPosReal = resultPosCountReal.getInt("num_positions");
              }
            }
          }
          if (numPosReal < numPos) {
            int yearInt = 9999;
            if (dbnumber != null && dbnumber.length() > 4) {
              final String Jahr = dbnumber.substring(0, 4);
              yearInt = Integer.parseInt(Jahr);
            }
            final java.util.Calendar myCalE = java.util.Calendar.getInstance();
            myCalE.set(yearInt, 11, 31, 15, 30, 00);
            final java.sql.Timestamp tsSql = new java.sql.Timestamp(myCalE.getTimeInMillis());
            try (final PreparedStatement preparedStatement = myDataBase.prepareStatement(insertPositionSql)) {
              preparedStatement.clearParameters();
              preparedStatement.setInt(1, mandator);
              preparedStatement.setString(2, dbnumber);
              preparedStatement.setString(3, spielNummer);
              preparedStatement.setInt(5, 0);
              preparedStatement.setInt(6, 0);
              preparedStatement.setNull(7, Types.INTEGER);
              preparedStatement.setNull(8, Types.INTEGER);
              preparedStatement.setTimestamp(9, tsSql);
              preparedStatement.setBoolean(10, false);
              for (pos = numPosReal; pos < numPos; pos++) {
                preparedStatement.setInt(4, pos);
                preparedStatement.executeUpdate();
              }
            }
          }

          try (final PreparedStatement preparedStatement = myDataBase.prepareStatement(readAllPosSql)) {
            preparedStatement.clearParameters();
            preparedStatement.setInt(1, mandator);
            preparedStatement.setString(2, dbnumber);
            preparedStatement.setString(3, spielNummer);
            try (final ResultSet resultAllPos = preparedStatement.executeQuery()) {
              for (pos = 0; pos < numPos; pos++) {
                resultAllPos.next();
                for (posJ = 0; posJ < positionDbFields.length; posJ++) {
                  positionDbFields[posJ].sqlRead(resultAllPos, session, pos);
                }
              }
            }
          }
          for (posJ = 0; posJ < positionDbFields.length; posJ++) {
            if (posJ == positionNumberKeyField && positionDbFields[posJ].getEnchentment() == null) {
              if (numPos == 0) {
                positionDbFields[posJ].setField(session, "0", numPos);
              } else {
                field = positionDbFields[posJ].getContents(session, numPos - 1);
                if (field == null) {
                  pos = numPos - 1;
                } else {
                  pos = Integer.parseInt(field);
                }
                field = Integer.toString(pos + 1);
                positionDbFields[posJ].setField(session, field, numPos);
              }
            } else if (posJ == positionNumberReferenceField) {
              positionDbFields[posJ].setField(session, dbFields[numberKeyField].getContents(session), numPos);
            } else {
              positionDbFields[posJ].initField(session, numPos);
            }
          }
        } catch (final java.sql.SQLException e) {
          sb.append("SQL-Fehler in HTML_Formular(), bitte EDV verst&auml;ndigen:\n").append(e.toString());
        }
      } else {
        // we already know all data
        numPos = numPosInteger.intValue();
      }

      // show all lines
      for (pos = 0; pos < numPos; pos++) {
        session.setAttribute(servletName + AbstractVisualDb.SELECTED_FIELD_TEXT + serviceName, selectedField);
        sb.append(inputlinePosition(session, pos, numPos, tabpos));
        tabpos += positionDbFields.length;
        selectedField = (String) session.getAttribute(servletName + AbstractVisualDb.SELECTED_FIELD_TEXT + serviceName);
      }

      // another line for inserts
      if (positionAddRemove && allowedToChange(session)) {
        session.setAttribute(servletName + AbstractVisualDb.SELECTED_FIELD_TEXT + serviceName, selectedField);
        sb.append(inputlinePosition(session, numPos, numPos, tabpos));
        selectedField = (String) session.getAttribute(servletName + AbstractVisualDb.SELECTED_FIELD_TEXT + serviceName);
      }

      sb.append("            </table>\n");

      session.setAttribute(servletName + AbstractVisualDb.NUMBER_POS_TEXT + serviceName, Integer.valueOf(numPos));
    }

    sb.append(
        "            <p class=\"anmerkung\">&sup1;: Zwangsfelder, diese Felder m&uuml;ssen" + " immer gef&uuml;llt werden</p>\n"
            + "            <p class=\"anmerkung\">&sup2;: Wahlfreie Felder, diese Felder k&ouml;nnen"
            + " leer gelassen werden</p>\n" + "        </form>\n");

    if (allowedToChange(session) && selectedField == null) {
      selectedField = selectedFieldDefault;
    }
    session.setAttribute(servletName + AbstractVisualDb.SELECTED_FIELD_TEXT + serviceName, selectedField);

    return sb.toString();
  }

  /**
   * The Method <code>preparePage</code> fills up the window with current data.
   *
   * @param req HttpServletRequest from the Servlet
   * @param res HttpServletResponse from the Servlet
   * @param hint Text to be displayed
   * @param session The Data of the current Session
   * @param navTyp Navigation type
   * @return HTML code of the formular
   * @exception TextException text of the error
   */
  @Override
  protected String preparePage(final HttpServletRequest req, final HttpServletResponse res, final String hint,
      final HttpSession session, final String navTyp) throws TextException {
    return htmlPage(res, hint, session, navTyp);
  }

  /**
   * The Method <code>doReplacePosition</code> replaces a Item in the database.
   *
   * @param req HttpServletRequest from the Servlet
   * @param session The Data of the current Session
   * @param entry Name of the entry
   * @return insert ok (true/false)
   */
  @Override
  protected boolean doReplacePosition(final HttpServletRequest req, final HttpSession session, final int entry) {
    boolean allok = false;
    final String dbnumber = (String) session.getAttribute(servletName + dataBaseTable + "." + keyField);

    try {
      int posJ = 1;
      try (final PreparedStatement preparedStatement = myDataBase.prepareStatement(updatePositionSql)) {
        preparedStatement.clearParameters();
        for (int i = 0; i < positionDbFields.length; i++) {
          if (i != positionNumberReferenceField && i != positionNumberKeyField && i != positionNumberKeyField + 1) {
            posJ = positionDbFields[i].sqlPrepareItems(preparedStatement, myDataBase, posJ, session, entry);
          }
        }
        final Integer MandatorInteger =
            (Integer) session.getAttribute(servletName + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_MANDATOR);
        preparedStatement.setInt(posJ++, MandatorInteger == null ? 1 : MandatorInteger.intValue());
        preparedStatement.setString(posJ++, dbnumber);
        preparedStatement.setString(posJ++, positionDbFields[positionNumberKeyField].getContents(session, entry));
        preparedStatement.setString(posJ++, positionDbFields[positionNumberKeyField + 1].getContents(session, entry));

        if (preparedStatement.executeUpdate() > 0) {
          allok = true;
        }
      }
    } catch (final java.sql.SQLException e) {
      // Um die Fehlerbehandlung nicht zweimal zu machen, schicken
      // wir die Meldung einfach weiter
      allok = false;
      session.setAttribute(servletName + AbstractVisualDb.DEBUG_TEXT_TEXT + serviceName,
          "SQL-Fehler:\n" + e.toString() + "\nbei doReplacePosition");
    }

    return allok;
  }

  /**
   * The Method <code>checkPositionHasChanged</code> checks out if the position has changed or not.
   *
   * @param req HttpServletRequest from the Servlet
   * @param session The Data of the current Session
   * @param pentry Name of the entry
   * @return has changed (true/false)
   */
  @Override
  protected boolean checkPositionHasChanged(final HttpServletRequest req, final HttpSession session, final int pentry) {
    for (int i = 0; i < positionDbFields.length; i++) {
      if (i != positionNumberKeyField && i != positionNumberKeyField + 1 && i != positionNumberReferenceField) {
        positionDbFields[i].htmlRead(req, session, pentry);
      }
    }
    return true;
  }

  /**
   * The Method <code>checkDBKey</code> is used for manuel switching to another entry.
   *
   * @param newDbKey New Database-Key to change to
   * @param session The Data of the current Session
   * @return null if the new database key exists, otherwise the error text
   */
  @Override
  protected String checkDbKey(final String newDbKey, final HttpSession session) {
    String hint = null;

    final String dbmin = (String) session.getAttribute(servletName + AbstractVisualDb.DBMIN_TEXT + serviceName);
    final String dbmax = (String) session.getAttribute(servletName + AbstractVisualDb.DBMAX_TEXT + serviceName);
    final String saisonLiga = (String) session.getAttribute(servletName + dataBaseTable + "." + keyField);
    int dbminInt = 0;
    int dbmaxInt = 0;
    int newDbKeyInt = 0;

    try {
      dbminInt = Integer.parseInt(dbmin);
      dbmaxInt = Integer.parseInt(dbmax);
      newDbKeyInt = Integer.parseInt(newDbKey);
      if (newDbKeyInt < dbminInt) {
        hint = "Eingegebener Key: \"" + newDbKey + "\", ist zu klein";
      } else if (newDbKeyInt > dbmaxInt) {
        hint = "Eingegebener Key: \"" + newDbKey + "\", ist zu groß";
      } else {
        session.setAttribute(servletName + positionDataBaseTable + "." + saisonLiga + "."
            + positionDbFields[positionNumberKeyField].getdbfieldname(), newDbKey);
        session.removeAttribute(servletName + AbstractVisualDb.NUMBER_POS_TEXT + serviceName);
      }
    } catch (final Exception e) {
      // Um die Fehlerbehandlung nicht zweimal zu machen, schicken
      // wir die Meldung einfach weiter
      hint = "Eingabe der Spieltagsnummer nicht nummerisch\n" + e.toString();
    }
    return hint;
  }

  /**
   * The Method <code>doOtherOptions</code> generates the background automaticly.
   *
   * @param req HttpServletRequest from the Servlet
   * @param res HttpServletResponse from the Servlet
   * @param session The Data of the current Session
   * @param navTyp Navigation type
   * @return HTML code of the formular
   * @exception TextException text of the error
   */
  @Override
  protected String doOtherOptions(final HttpServletRequest req, final HttpServletResponse res, final HttpSession session,
      final String navTyp) throws TextException {
    final String saisonLiga = (String) session.getAttribute(servletName + dataBaseTable + "." + keyField);
    final Integer mandatorInteger =
        (Integer) session.getAttribute(servletName + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_MANDATOR);
    final int mandator = mandatorInteger == null ? 1 : mandatorInteger.intValue();
    return this.doOtherOptions(req, res, session, navTyp, mandator, saisonLiga);
  }

  /**
   * The Method <code>doOtherOptions</code> generates the background automaticly.
   *
   * @param req HttpServletRequest from the Servlet
   * @param res HttpServletResponse from the Servlet
   * @param session The Data of the current Session
   * @param navTyp Navigation type
   * @param mandator mandator number
   * @param saisonLiga season ligue
   * @return HTML code of the formular
   * @exception TextException text of the error
   */
  protected String doOtherOptions(final HttpServletRequest req, final HttpServletResponse res, final HttpSession session,
      final String navTyp, final int mandator, final String saisonLiga) throws TextException {
    String returnwert = null;
    int numPos = 0;

    // Werte neuer Spieltag
    int ligue = 0;

    try {
      if (saisonLiga != null && saisonLiga.length() > 4) {
        ligue = Integer.parseInt(saisonLiga);
      }

      try (final PreparedStatement preparedStatement = myDataBase.prepareStatement(readPosCountRealSql)) {
        preparedStatement.clearParameters();
        preparedStatement.setInt(1, mandator);
        preparedStatement.setInt(2, ligue);
        preparedStatement.setInt(3, 2);
        try (final ResultSet resultPosCount = preparedStatement.executeQuery()) {
          if (resultPosCount.next()) {
            numPos = resultPosCount.getInt("num_positions");
          }
        }
      }

      final Map<String, Integer> teamNumberMap = ScanBundesliga.readTeamMap(mandator, ligue, myDataBase);
      final String baseUrl = ScanBundesliga.getUrlForSeasonLeague(ligue);
      if (numPos > 0) {
        ScanBundesliga.updateMatchdays(mandator, ligue, baseUrl, teamNumberMap, myDataBase);
        returnwert = preparePage(req, res, "Spiele wurden aktualisiert", session, navTyp);
      } else {
        ScanBundesliga.createMatchdays(mandator, ligue, baseUrl, teamNumberMap, myDataBase);
        returnwert = preparePage(req, res, "Spiele wurden generiert", session, navTyp);
      }
    } catch (final Exception e) {
      // Um die Fehlerbehandlung nicht zweimal zu machen, schicken wir die Meldung einfach weiter
      returnwert = preparePage(req, res, "Fehler beim generieren/update der Daten:\n" + e.toString(), session, navTyp);
    }
    return returnwert;
  }

  /**
   * The Method <code>doTheWork</code> coordinates all the work in this class.
   *
   * @param req HttpServletRequest from the Servlet
   * @param res HttpServletResponse from the Servlet
   * @param session The Data of the current Session
   * @return html code of the formular
   * @exception TextException when a error in checking the passwords occours
   */
  @Override
  public String doTheWork(final HttpServletRequest req, final HttpServletResponse res, final HttpSession session)
      throws TextException {
    final String saisonLiga = req.getParameter("saison_liga");

    if (saisonLiga != null) {
      final String user = req.getParameter("user");
      final String password = req.getParameter("password");
      try {
        if (user == null || StringUtils.EMPTY.equals(user) || password == null || StringUtils.EMPTY.equals(password)) {
          session.setAttribute(servletName + dataBaseTable + "." + keyField, saisonLiga);

          final Integer MandatorInteger =
              (Integer) session.getAttribute(servletName + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_MANDATOR);
          final java.util.Vector<String> PreselectsVector = new java.util.Vector<>();
          final java.util.Vector<String> PreselectsViewVector = new java.util.Vector<>();
          int pos = 0;
          try (final PreparedStatement preparedStatement = myDataBase.prepareStatement(AdminSpiele.POP_UP_SQL_STRING)) {
            preparedStatement.clearParameters();
            preparedStatement.setInt(1, MandatorInteger == null ? 1 : MandatorInteger.intValue());
            preparedStatement.setString(2, saisonLiga);
            try (final ResultSet result = preparedStatement.executeQuery()) {
              while (result.next()) {
                PreselectsVector.add(result.getString("value"));
                PreselectsViewVector.add(result.getString("valuedisplay"));
                pos++;
              }
            }
          }
          if (pos > 0) {
            final String[] db_PreselectName = new String[pos];
            final String[] db_PreselectNameDisplay = new String[pos];
            for (int j = 0; j < PreselectsVector.size(); j++) {
              db_PreselectName[j] = PreselectsVector.get(j);
              db_PreselectNameDisplay[j] = PreselectsViewVector.get(j);
            }
            session.setAttribute(servletName + AdminSpiele.POP_UP_SESSION_SPIELE_VALUE, db_PreselectName);
            session.setAttribute(servletName + AdminSpiele.POP_UP_SESSION_SPIELE_VALUE_DISPLAY, db_PreselectNameDisplay);
          } else {
            final String[] db_PreselectName = new String[1];
            final String[] db_PreselectNameDisplay = new String[1];
            db_PreselectName[0] = "1";
            db_PreselectNameDisplay[0] = "Bitte Mannschaften erfassen";
            session.setAttribute(servletName + AdminSpiele.POP_UP_SESSION_SPIELE_VALUE, db_PreselectName);
            session.setAttribute(servletName + AdminSpiele.POP_UP_SESSION_SPIELE_VALUE_DISPLAY, db_PreselectNameDisplay);
          }

          fillMinMax(session);

        } else {
          int stufe = 0;
          try (final PreparedStatement preparedStatement = myDataBase.prepareStatement(readUserSql)) {
            preparedStatement.clearParameters();
            preparedStatement.setString(1, user);
            preparedStatement.setString(2, password);
            try (final ResultSet resultHead = preparedStatement.executeQuery()) {
              if (resultHead.next()) {
                stufe = resultHead.getInt("stufe");
              }
            }
          }

          if (stufe <= 5) {
            return "<h2>Sie haben keine Berechtigung f&uuml;r die Spiele&uuml;bername</h2>\n";
          }
          return this.doOtherOptions(req, res, session, StringUtils.EMPTY, 1, saisonLiga);
        }
      } catch (final java.sql.SQLException e) {
        throw new TextException("SQL-Fehler:\nSaison/Liga:" + saisonLiga + "\n" + e.toString(), e);
      }
    }

    return super.doTheWork(req, res, session);
  }

  /**
   * The Method <code>allowedToSee</code> tells you if the currently logged in user is allowed to access this application.
   *
   * @param session Data of the current session
   * @return true if it is allowed for this user
   */
  @Override
  public boolean allowedToSee(final HttpSession session) {
    boolean returnwert = false;
    if (session != null) {
      if (session.getAttribute(servletName + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_USER
          + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_USER) != null) {
        final int stufe = ((Integer) session.getAttribute(servletName + KnConst.SESSION_STUFE_FT)).intValue();
        returnwert = stufe > 5;
      }
    }
    return returnwert;
  }

  /**
   * The Method <code>allowedToChange</code> tells you if the currently logged in user is allowed to change data in this
   * application.
   *
   * @param session Data of the current session
   * @return true if it is allowed for this user
   */
  @Override
  public boolean allowedToChange(final HttpSession session) {
    boolean returnwert = false;
    if (session != null) {
      if (session.getAttribute(servletName + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_USER
          + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_USER) != null) {
        final int stufe = ((Integer) session.getAttribute(servletName + KnConst.SESSION_STUFE_FT)).intValue();
        returnwert = stufe > 5;
      }
    }
    return returnwert;
  }
}
