/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.knightsoftnet.bettingpool;

import de.knightsoft.common.NavTabStrukt;
import de.knightsoft.knightsoftnet.common.KnConst;

import javax.servlet.http.HttpSession;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * The <code>Navigation</code> class generates the html navitagion panel of the KnightSoft-Net Tipprunde.
 *
 * @author Manfred Tremmel
 * @version 2.0.1, 28.07.2018
 */
public class Navigation {

  private final Connection tippDb;

  private static final String SQL_STRING = //
      "SELECT            tl.saison_liga AS saison_liga, " //
          + "                tl.bezeichnung AS bezeichnung, " //
          + "                CONCAT(TRUNCATE(tl.saison_liga/100000,0), '/', " //
          + "                       (TRUNCATE(tl.saison_liga / 100000,0) + 1)) as saison, " //
          + "                (0 - TRUNCATE(tl.saison_liga/100000,0)) AS sort_sl, " //
          + "                if (MAX(ts.spielbeginn)>=NOW(),'J','N') AS liga_aktuell " //
          + "FROM            KnightSoft_TippLiga AS tl " //
          + "    LEFT JOIN    KnightSoft_TippSpiele AS ts " //
          + "    ON            tl.Mandator = ts.Mandator AND " //
          + "                tl.saison_liga = ts.saison_liga " //
          + "WHERE            tl.Mandator = ? AND " //
          + "                tl.eintrag_gesperrt = ? " //
          + "GROUP BY        sort_sl, saison_liga";

  private final de.knightsoft.common.NavTabStrukt fixNavTypeHead;
  private final de.knightsoft.common.NavTabStrukt fixNavTypeStart;
  private final de.knightsoft.common.NavTabStrukt fixNavTypeRegeln;
  private final de.knightsoft.common.NavTabStrukt fixNavTypeDataProtection;
  private final de.knightsoft.common.NavTabStrukt fixNavTypeImpres;
  private final de.knightsoft.common.NavTabStrukt fixNavTypeFaq;
  private final de.knightsoft.common.NavTabStrukt fixNavTypeLigaStart;
  private final de.knightsoft.common.NavTabStrukt fixNavTypeLigaStop;
  private final de.knightsoft.common.NavTabStrukt fixNewTypeRegister;
  private final de.knightsoft.common.NavTabStrukt fixNewTypeDaten;
  private final de.knightsoft.common.NavTabStrukt fixNavAdminLiga;
  private final de.knightsoft.common.NavTabStrukt fixNavAdminManschaften;
  private final de.knightsoft.common.NavTabStrukt fixNavAdminEMail;
  private final de.knightsoft.common.NavTabStrukt fixNavAdminDbAdmin;

  /**
   * Constructor.
   *
   * @param pdatabase Connection to database
   */
  public Navigation(final Connection pdatabase) {
    tippDb = pdatabase;
    fixNavTypeHead = new NavTabStrukt(0, KnConst.GIF_URL + "16x16/Tipprunde_nav.png", "Tipprunde", //
        null, false);
    fixNavTypeStart =
        new NavTabStrukt(3, KnConst.GIF_URL + "16x16/gohome.png", "Index", KnConst.HTML_BASE + "Tipprunde/index.html", false);
    fixNavTypeDataProtection = new NavTabStrukt(3, KnConst.GIF_URL + "16x16/impressum.png", "Datenschutz",
        KnConst.HTML_BASE + "Tipprunde/datenschutz.html", false);
    fixNavTypeImpres = new NavTabStrukt(3, KnConst.GIF_URL + "16x16/impressum.png", "Impressum",
        KnConst.HTML_BASE + "Tipprunde/impressum.html", false);
    fixNavTypeRegeln = new NavTabStrukt(3, KnConst.GIF_URL + "16x16/spielregeln.png", "Spielregeln",
        KnConst.HTML_BASE + "Tipprunde/spielregeln.html", false);
    fixNavTypeFaq =
        new NavTabStrukt(3, KnConst.GIF_URL + "16x16/help.png", "FAQ", KnConst.HTML_BASE + "Tipprunde/faq.html", false);
    fixNavTypeLigaStart = new NavTabStrukt(1, KnConst.GIF_URL + "16x16/folder.png", "Tippligen", null, true);
    fixNavTypeLigaStop = new NavTabStrukt(2, null, null, null, true);
    fixNewTypeRegister = new NavTabStrukt(3, KnConst.GIF_URL + "16x16/registrieren.png", "Registrieren",
        KnConst.HTML_BASE + "Tipprunde/registrieren.html", false);
    fixNewTypeDaten = new NavTabStrukt(3, KnConst.GIF_URL + "16x16/daten_aendern.png", "Daten&nbsp;&auml;ndern",
        KnConst.HTML_BASE + "Tipprunde/datenaendern.html", false);
    fixNavAdminLiga =
        new NavTabStrukt(3, KnConst.GIF_URL + "16x16/liga.png", "Liga", KnConst.HTML_BASE + "Tipprunde/adminliga.html", false);
    fixNavAdminManschaften = new NavTabStrukt(3, KnConst.GIF_URL + "16x16/mannschaften.png", "Mannschaften",
        KnConst.HTML_BASE + "Tipprunde/adminmannschaften.html", false);
    fixNavAdminEMail =
        new NavTabStrukt(3, KnConst.GIF_URL + "16x16/email.png", "E-Mail", KnConst.HTML_BASE + "Tipprunde/email.html", false);
    fixNavAdminDbAdmin =
        new NavTabStrukt(3, KnConst.GIF_URL + "16x16/db.png", "DBAdmin", KnConst.HTML_BASE + "Tipprunde/dbadmin.html", false);
  }

  /**
   * The Method <code>getNavTabStrukt</code> returns the navigation panel structure.
   *
   * @return navigation panel
   */
  public NavTabStrukt[] getNavTabStrukt() {
    return this.getNavTabStrukt(null, "XXXXXXX", null);
  }

  /**
   * The Method <code>getNavTabStrukt</code> returns the navigation panel structure.
   *
   * @param saisonLigaOpen String with the number of the opened league
   * @param session session data
   * @return navigation panel
   */
  public NavTabStrukt[] getNavTabStrukt(final String servletname, final String saisonLigaOpen, final HttpSession session) {
    int dynamicSize = 8;
    int position = 0;
    int mandator = 1;
    boolean isOpen = true;
    boolean binEingeloggt = false;
    boolean binAdmin = false;
    String saisonLiga = null;
    String bezeichnung = null;
    String saison = null;
    String ligaAktuell = null;
    String saisonMerker = null;
    String loginlogout = "Login";
    NavTabStrukt[] myNavTab = null;

    try {
      if (session != null) {
        if ((String) session.getAttribute(servletname + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_USER
            + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_USER) != null) {
          final int stufe = ((Integer) session.getAttribute(servletname + KnConst.SESSION_STUFE_FT)).intValue();
          if (stufe > 0) {
            binEingeloggt = true;
            loginlogout = "Logout";
            if (stufe > 5) {
              binAdmin = true;
            }
          }
        }
        final Integer MandatorInteger =
            (Integer) session.getAttribute(servletname + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_MANDATOR);
        if (MandatorInteger != null) {
          mandator = MandatorInteger.intValue();
        }
      }

      try (final PreparedStatement sqlStatement = tippDb.prepareStatement(Navigation.SQL_STRING)) {
        sqlStatement.clearParameters();
        sqlStatement.setInt(1, mandator);
        sqlStatement.setBoolean(2, false);
        try (final ResultSet result = sqlStatement.executeQuery()) {

          // How many entries are in the resultset?
          result.last();
          dynamicSize = result.getRow();

          // back to first entry
          result.beforeFirst();

          if (binAdmin) {
            dynamicSize += dynamicSize * 12 + 4;
          } else if (binEingeloggt) {
            dynamicSize += dynamicSize * 11;
          } else {
            dynamicSize += dynamicSize * 10;
          }

          myNavTab = new NavTabStrukt[dynamicSize];

          myNavTab[position++] = fixNavTypeHead;
          myNavTab[position++] = fixNavTypeStart;
          myNavTab[position++] = fixNavTypeImpres;
          myNavTab[position++] = fixNavTypeDataProtection;
          myNavTab[position++] = fixNavTypeRegeln;
          myNavTab[position++] = fixNavTypeFaq;
          myNavTab[position++] = new NavTabStrukt(3, KnConst.GIF_URL + "16x16/login.png", loginlogout,
              KnConst.HTML_BASE + "Tipprunde/" + loginlogout + ".html", false);

          if (binEingeloggt) {
            myNavTab[position++] = fixNewTypeDaten;
          } else {
            myNavTab[position++] = fixNewTypeRegister;
          }

          if (binAdmin) {
            myNavTab[position++] = fixNavAdminEMail;
            myNavTab[position++] = fixNavAdminDbAdmin;
            myNavTab[position++] = fixNavAdminLiga;
            myNavTab[position++] = fixNavAdminManschaften;
          }

          myNavTab[position++] = fixNavTypeLigaStart;

          while (result.next()) {
            saisonLiga = result.getString("saison_liga");
            bezeichnung = result.getString("bezeichnung");
            saison = result.getString("saison");
            ligaAktuell = result.getString("liga_aktuell");

            if (saisonMerker == null) {
              myNavTab[position++] =
                  new NavTabStrukt(1, KnConst.GIF_URL + "16x16/folder.png", "Saison&nbsp;" + saison, null, isOpen);
              saisonMerker = saison;
            } else {
              if (!saison.equals(saisonMerker)) {
                if (saison.substring(0, 4).equals(saisonLigaOpen.substring(0, 4)) || "J".equals(ligaAktuell)) {
                  isOpen = true;
                } else {
                  isOpen = false;
                }
                myNavTab[position++] = fixNavTypeLigaStop;
                myNavTab[position++] =
                    new NavTabStrukt(1, KnConst.GIF_URL + "16x16/folder.png", "Saison&nbsp;" + saison, null, isOpen);
                saisonMerker = saison;
              }
            }

            if (saisonLiga.equals(saisonLigaOpen) || "J".equals(ligaAktuell)) {
              isOpen = true;
            } else {
              isOpen = false;
            }
            myNavTab[position++] = new NavTabStrukt(1, KnConst.GIF_URL + "16x16/dfb.png",
                de.knightsoft.common.StringToHtml.convert(bezeichnung, true), null, true);
            myNavTab[position++] = new NavTabStrukt(3, KnConst.GIF_URL + "16x16/ligastatistik.png", "Ligastatistik",
                KnConst.HTML_BASE + "Tipprunde/" + saisonLiga + "/Ligastatistik.html", false);
            myNavTab[position++] = new NavTabStrukt(3, KnConst.GIF_URL + "16x16/tippstatistik.png", "Tippstatistik",
                KnConst.HTML_BASE + "Tipprunde/" + saisonLiga + "/Tippstatistik.html", false);
            if (binEingeloggt) {
              myNavTab[position++] = new NavTabStrukt(3, KnConst.GIF_URL + "16x16/tippen.png", "Tippen",
                  KnConst.HTML_BASE + "Tipprunde/" + saisonLiga + "/Tippen.html", false);
            }
            if (binAdmin) {
              myNavTab[position++] = new NavTabStrukt(3, KnConst.GIF_URL + "16x16/ligamannschaften.png", "LigaMannschaften",
                  KnConst.HTML_BASE + "Tipprunde/" + saisonLiga + "/LigaMannschaften.html", false);
              myNavTab[position++] = new NavTabStrukt(3, KnConst.GIF_URL + "16x16/spieltag.png", "Spieltag",
                  KnConst.HTML_BASE + "Tipprunde/" + saisonLiga + "/Spieltag.html", false);
            }
            myNavTab[position++] = fixNavTypeLigaStop;
          }
        }
      }
    } catch (final java.sql.SQLException e) {
      // dumm gelaufen, aber um den weiteren Ablauf zu ermöglichen, kein Abbruch!
      e.printStackTrace();
    }

    if (myNavTab != null) {
      if (saisonMerker != null) {
        myNavTab[position++] = fixNavTypeLigaStop;
      }

      myNavTab[position++] = fixNavTypeLigaStop;

      for (; position < dynamicSize; myNavTab[position++] = null) {
        ;
      }
    }

    return myNavTab;
  }
}
