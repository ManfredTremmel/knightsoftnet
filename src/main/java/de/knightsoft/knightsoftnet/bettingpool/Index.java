/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.knightsoftnet.bettingpool;

import de.knightsoft.common.TextException;
import de.knightsoft.knightsoftnet.common.KnConst;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * The <code>Index</code> class is based on the abstract class de.knightsoft.common.AbstractVisualDb and implements the index
 * page of the KnightSoft Tipprunde.
 *
 *
 * @author Manfred Tremmel
 * @version 2.0.5, 24.02.2013
 */
public class Index extends de.knightsoft.common.AbstractVisualDb {
  /**
   * Constructor.
   *
   * @param thisDatabase Connection to database
   * @param servletname Name of the Servlet
   * @exception SQLException if a database access error occurs
   * @exception TextException to give out a text message
   * @see TextException
   */
  public Index(final Connection thisDatabase, final String servletname) throws SQLException, TextException {
    super(thisDatabase, KnConst.HTML_BASE, servletname, KnConst.GIF_URL + "16x16/gohome.png", "Index",
        "Willkommen bei der Tipprunde");
  }

  /**
   * The Method <code>HTMLPage</code> generates the html formular.
   *
   * @param res HttpServletResponse from the Servlet
   * @param hint Hint to be displayed on the screen
   * @param session The Data of the current Session
   * @param navTyp Navigation type
   * @return HTML code of the formular
   */
  @Override
  protected String htmlPage(final HttpServletResponse res, final String hint, final HttpSession session, final String navTyp) {
    return "<p>Wenn Sie glauben, ein wenig (oder viel) Erfahrung mit Fu&szlig;ball zu haben, " + "sind Sie hier richtig.</p>\n"
        + "<p>Sie k&ouml;nnen zu jedem Spieltag einen Tipp abgeben, erhalten entsprechend der <a href=\""
        + res.encodeURL(KnConst.HTML_BASE + "Tipprunde/spielregeln.html") + "\">Spielregeln</a> Punkte f&uuml;r Ihre Tipps,\n"
        + "sobald das jeweilige Spiel abgeschlossen ist.</p>\n"
        + "<p>Unter der Liga Statistik zur entsprechenden Liga, k&ouml;nnen Sie die Ergebnisse "
        + "der jeweiligen Spieltage und die daraus errechnete Tabelle nachlesen und unter\n"
        + "Tipp Statistik finden Sie die besten Tipps des Spieltages, sowie &uuml;ber die Saison " + "hinweg.</p>\n"
        + "<p>Wenn Sie teilnehmen wollen, f&uuml;llen Sie das Registrierungsformular zur "
        + "entsprechenden Liga aus und melden sich dann per Login auf der entsprechenden " + "Seite an.\n"
        + "<p>Ich w&uuml;nsche Ihnen viel Spa&szlig; und Erfolg mit Ihren Tipps.</p>\n"
        + "<p>Beachten Sie auch die Vermittlungen f&uuml;r <a href=\""
        + res.encodeURL(KnConst.HTML_BASE + "Babysitter/index.html") + "\">Babysitter</a>, <a href=\""
        + res.encodeURL(KnConst.HTML_BASE + "Nachhilfe/index.html") + "\">Nachhilfelehrer(innen)</a> und <a href=\""
        + res.encodeURL(KnConst.HTML_BASE + "Fahrgemeinschaft/index.html")
        + "\">Fahrgemeinschaften / Mitfahrgelegenheiten</a>.</p>\n"
        + "<p>Wenn Sie Kontakt mit mir aufnehmen wollen, finden Sie die Adresse auf der <a href=\""
        + res.encodeURL(KnConst.HTML_BASE + "Tipprunde/impressum.html") + "\">Impressum-Seite</a>.</p>\n"
        + "<div class=\"up\">Letztes Update: 20.01.2024</div>\n";
  }

  /**
   * The Method <code>preventFromCache</code> tells you if this page should be cached or not.
   *
   * @param session Data of the current session
   * @return don't cache this site = true
   */
  @Override
  public boolean preventFromCache(final HttpSession session) {
    return false;
  }

  /**
   * The Method <code>allowedToSee</code> tells you if the currently logged in user is allowed to access this application.
   *
   * @param session Data of the current session
   * @return true if it is allowed for this user
   */
  @Override
  public boolean allowedToSee(final HttpSession session) {
    return true;
  }

  /**
   * The Method <code>allowedToChange</code> tells you if the currently logged in user is allowed to change data in this
   * application.
   *
   * @param session Data of the current session
   * @return true if it is allowed for this user
   */
  @Override
  public boolean allowedToChange(final HttpSession session) {
    return true;
  }

}
