/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.knightsoftnet.bettingpool;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * The <code>Tippen</code> class is based on the abstract class de.knightsoft.common.AbstractVisualDb and implements the
 * formular to add the tipps for the KnightSoft Tipprunde. It's no longer a real servlet, only a part of one.
 *
 *
 * @author Manfred Tremmel
 * @version 2.0.1, 29.04.2010
 */
public class Tippen extends de.knightsoft.common.AbstractVisualDb {

  private final de.knightsoft.knightsoftnet.bettingpool.LigaSpieltagNavigation myStNavigation;

  private static final String ERMITTLE_SPIELTAG_A = "SELECT        MAX(spieltag)+1 AS nr_spieltag "
      + "FROM        KnightSoft_TippSpieleTipp " + "WHERE        Mandator = ? AND" + "            saison_liga = ? AND "
      + "            spitzname = ? AND " + "            tore_heim IS NOT NULL";
  private static final String ERMITTLE_SPIELTAG_B =
      "SELECT        MIN(spieltag) AS nr_spieltag " + "FROM        KnightSoft_TippSpiele " + "WHERE        Mandator = ? AND"
          + "            saison_liga= ? AND " + "            spiel_beendet = '0'";
  private static final String ABFRAGE_A = "SELECT        bezeichnung, " + "            anz_manschaften "
      + "FROM        KnightSoft_TippLiga " + "WHERE        Mandator = ? AND" + "            saison_liga = ? ";
  private static final String ABFRAGE_B = "SELECT        ts.lfd_spiele_nr + 1 AS spiel_nr, "
      + "            tm1.bezeichnung AS heim_bez, " + "            tm2.bezeichnung AS ausw_bez, "
      + "            IF (ts.tore_heim IS NOT NULL, ts.tore_heim, null) AS heim_tore, "
      + "            IF (ts.tore_ausw IS NOT NULL, ts.tore_ausw, null) AS ausw_tore, "
      + "            tst.tore_heim AS heim_tore_tipp, " + "            tst.tore_ausw AS ausw_tore_tipp, "
      + "            IF (ts.tore_heim IS NULL OR tst.tore_heim IS NULL, " + "                0, "
      + "                IF (ts.tore_heim = tst.tore_heim AND ts.tore_ausw = tst.tore_ausw, "
      + "                    tl.tr_punkte_exakt, "
      + "                    IF ((CAST(ts.tore_heim AS signed) - CAST(ts.tore_ausw AS signed)) "
      + "                      = (CAST(tst.tore_heim AS signed) - CAST(tst.tore_ausw AS signed)), "
      + "                        tl.tr_punkte_torver, " + "                        IF (((ts.tore_heim  = ts.tore_ausw) AND "
      + "                             (tst.tore_heim = tst.tore_ausw)) OR "
      + "                            ((ts.tore_heim  > ts.tore_ausw) AND "
      + "                             (tst.tore_heim > tst.tore_ausw)) OR "
      + "                            ((ts.tore_heim  < ts.tore_ausw) AND "
      + "                             (tst.tore_heim < tst.tore_ausw)), " + "                            tl.tr_punkte_toto, "
      + "                            tl.tr_punkte_falsch " + "                            ) " + "                        ) "
      + "                    ) " + "                ) AS punkte, "
      + "            IF (ts.spielbeginn > NOW(), '1', '0') AS eingabe_erlaubt, "
      + "            DATE_FORMAT(ts.spielbeginn,'%d.%m.%Y %H:%i') AS spielbeginn, "
      + "            ts.spiel_beendet AS spiel_beendet " + "FROM        KnightSoft_TippSpiele AS ts "
      + "    LEFT JOIN    KnightSoft_TippLigaMannschaft AS tlm1 " + "    ON            (tlm1.Mandator=ts.Mandator AND "
      + "                 tlm1.saison_liga=ts.saison_liga AND " + "                 tlm1.lfd_liga_ms=ts.mannschaft_heim) "
      + "    LEFT JOIN    KnightSoft_TippMannschaften AS tm1 " + "    ON            (tlm1.Mandator=tm1.Mandator AND "
      + "                 tlm1.zaehler=tm1.zaehler) " + "    LEFT JOIN    KnightSoft_TippLigaMannschaft AS tlm2 "
      + "    ON            (tlm2.Mandator=ts.Mandator AND " + "                 tlm2.saison_liga=ts.saison_liga AND "
      + "                 tlm2.lfd_liga_ms=ts.mannschaft_ausw) " + "    LEFT JOIN    KnightSoft_TippMannschaften AS tm2 "
      + "    ON            (tlm2.Mandator=tm2.Mandator AND " + "                 tlm2.zaehler=tm2.zaehler) "
      + "    LEFT JOIN    KnightSoft_TippLiga AS tl " + "    ON            (ts.Mandator=tl.Mandator AND "
      + "                 ts.saison_liga=tl.saison_liga) " + "    LEFT JOIN    KnightSoft_TippSpieleTipp AS tst "
      + "    ON            (ts.Mandator=tst.Mandator AND " + "                 ts.saison_liga=tst.saison_liga AND "
      + "                 ts.spieltag=tst.spieltag AND " + "                 ts.lfd_spiele_nr=tst.lfd_spiele_nr AND"
      + "                 tst.spitzname = ?) " + "WHERE            ts.Mandator = ? AND"
      + "                ts.saison_liga = ? AND " + "                ts.spieltag = ? "
      + "ORDER BY        ts.spielbeginn, spiel_nr";
  private static final String ABFRAGE_INSERT =
      "INSERT INTO    KnightSoft_TippSpieleTipp" + "                (Mandator, saison_liga, spieltag, lfd_spiele_nr, "
          + "spitzname, tore_heim, tore_ausw)" + "                VALUES(?, ?, ?, ?, ?, ?, ?)";
  private static final String EINGABE_LESEN = "SELECT            IF (spielbeginn > NOW(), '1', '0') AS update_erlaubt "
      + "FROM            KnightSoft_TippSpiele " + "WHERE            Mandator = ? AND " + "                saison_liga= ? AND "
      + "                spieltag = ? AND " + "                lfd_spiele_nr = ? ";
  private static final String EINGABE_SCHREIBEN =
      "UPDATE            KnightSoft_TippSpieleTipp " + "SET            tore_heim = ? ," + "                tore_ausw = ? "
          + "WHERE            Mandator = ? AND " + "                saison_liga = ? AND " + "                spieltag = ?  AND "
          + "                lfd_spiele_nr = ? AND " + "                spitzname = ? ";

  /**
   * Constructor.
   *
   * @param thisDatabase Connection to database
   * @param servletname Name of the Servlet
   * @exception SQLException if a database access error occurs
   * @exception de.knightsoft.common.TextException to give out a text message
   * @see de.knightsoft.common.TextException
   */
  public Tippen(final Connection thisDatabase, final String servletname)
      throws java.sql.SQLException, de.knightsoft.common.TextException {
    super(thisDatabase, de.knightsoft.common.Constants.HTML_BASE, servletname,
        de.knightsoft.common.Constants.GIF_URL + "16x16/tippen.png", "Tippen", "Tippen", null, null, 0, null);
    myStNavigation = new de.knightsoft.knightsoftnet.bettingpool.LigaSpieltagNavigation("Tippen");
  }

  /**
   * build java script for this site.
   *
   * @return javaScript string
   * @throws SQLException if db call fails
   */
  @Override
  protected String initJavaScript() throws SQLException {
    return super.initJavaScript() //
        + "        <script type=\"text/javascript\">\n" //
        + "            <!--\n" //
        + "                function chkTipp(eingabefeld) {\n" //
        + "                    if (eingabefeld.value.length > 0) {\n" //
        + "                        chkZ = 1;\n" //
        + "                        for (pos=0;pos<eingabefeld.value.length;++pos) {\n" //
        + "                            if (eingabefeld.value.charAt(pos) < \"0\" ||\n" //
        + "                                eingabefeld.value.charAt(pos) > \"9\") {\n" //
        + "                                  chkZ = -1;\n" //
        + "                            }\n" //
        + "                        }\n" //
        + "                        if (chkZ == -1) {\n" //
        + "                            alert(\"Bitte den Tipp nummerisch eingeben!\");\n" //
        + "                            eingabefeld.focus();\n" //
        + "                        }\n" //
        + "                    }\n" //
        + "                }\n" //
        + "            //-->\n" //
        + "        </script>\n";
  }

  /**
   * The Method <code>ermittleLigaSpieltag</code> returns the next matchday which is not filled out until now.
   *
   * @param saisonLiga number of the liga
   * @param spieltageMax available matchdays in this league
   * @param session The Data of the current Session
   * @return number of the matchday
   */
  private int ermittleLigaSpieltag(final int saisonLiga, final int spieltageMax, final HttpSession session) {
    int nrSpieltag = spieltageMax;
    final String spitzname = (String) session.getAttribute(servletName + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_USER
        + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_USER);
    final Integer mandatorInteger =
        (Integer) session.getAttribute(servletName + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_MANDATOR);
    final int mandator = mandatorInteger == null ? 1 : mandatorInteger.intValue();

    try (final PreparedStatement ermittleSpieltagaStatement = myDataBase.prepareStatement(Tippen.ERMITTLE_SPIELTAG_A);
        final PreparedStatement ermittleSpieltagbStatement = myDataBase.prepareStatement(Tippen.ERMITTLE_SPIELTAG_B)) {
      ermittleSpieltagaStatement.clearParameters();
      ermittleSpieltagaStatement.setInt(1, mandator);
      ermittleSpieltagaStatement.setInt(2, saisonLiga);
      ermittleSpieltagaStatement.setString(3, spitzname);
      try (final ResultSet resultNrA = ermittleSpieltagaStatement.executeQuery()) {
        if (resultNrA.next()) {
          nrSpieltag = resultNrA.getInt("nr_spieltag");
        } else {
          nrSpieltag = 0;
        }
      }

      if (nrSpieltag == 0) {
        ermittleSpieltagbStatement.clearParameters();
        ermittleSpieltagbStatement.setInt(1, mandator);
        ermittleSpieltagbStatement.setInt(2, saisonLiga);
        try (final ResultSet resultNrB = ermittleSpieltagbStatement.executeQuery()) {
          if (resultNrB.next()) {
            nrSpieltag = resultNrB.getInt("nr_spieltag");
          }
        }
      }

      if (nrSpieltag > spieltageMax) {
        nrSpieltag = spieltageMax;
      }
      if (nrSpieltag < 1) {
        nrSpieltag = 1;
      }
    } catch (final java.sql.SQLException e) {
      e.printStackTrace();
    }

    return nrSpieltag;
  }

  /**
   * The Method <code>HTMLPage</code> generates the html formular.
   *
   * @param res HttpServletResponse from the Servlet
   * @param hint Hint to be displayed on the screen
   * @param session The Data of the current Session
   * @param navTyp Navigation type
   * @param saisonLiga number of the season and league
   * @param pspieltag number of the matchday
   * @return HTML code of the formular
   */
  protected String htmlPage(final HttpServletResponse res, final StringBuilder hint, final HttpSession session,
      final String navTyp, final int saisonLiga, final int pspieltag) {
    boolean farbig = false;
    boolean submit = false;
    final StringBuilder formular = new StringBuilder(1280);
    final StringBuilder formularT2 = new StringBuilder(1024);
    int posI;
    int posJ;
    int anzMannschaften = 0;
    String ligabezeichnung = StringUtils.EMPTY;
    int punkteGesamt = 0;

    int spielNr;
    String heimBez;
    String auswBez;
    String heimTore;
    String auswTore;
    String heimToreTipp;
    String auswToreTipp;
    int punkte;
    boolean eingabeErlaubt;
    String spielbeginn;
    final String spitzname = (String) session.getAttribute(servletName + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_USER
        + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_USER);
    final Integer mandatorInteger =
        (Integer) session.getAttribute(servletName + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_MANDATOR);
    final int mandator = mandatorInteger == null ? 1 : mandatorInteger.intValue();
    String selectedField = null;

    try (final PreparedStatement abfrageaStatement = myDataBase.prepareStatement(Tippen.ABFRAGE_A)) {
      abfrageaStatement.clearParameters();
      abfrageaStatement.setInt(1, mandator);
      abfrageaStatement.setInt(2, saisonLiga);
      try (final ResultSet resultAbfA = abfrageaStatement.executeQuery()) {

        if (resultAbfA.next()) {
          anzMannschaften = resultAbfA.getInt("anz_manschaften");
          ligabezeichnung = resultAbfA.getString("bezeichnung");
        }
      }
    } catch (final java.sql.SQLException e) {
      e.printStackTrace();
    }

    final int spieltag;
    if (pspieltag == 0) {
      spieltag = ermittleLigaSpieltag(saisonLiga, (anzMannschaften - 1) * 2, session);
    } else {
      spieltag = pspieltag;
    }

    try (final PreparedStatement abfragebStatement = myDataBase.prepareStatement(Tippen.ABFRAGE_B)) {
      posI = 0;

      while (posI == 0) {
        posJ = 0;
        abfragebStatement.clearParameters();
        abfragebStatement.setString(1, spitzname);
        abfragebStatement.setInt(2, mandator);
        abfragebStatement.setInt(3, saisonLiga);
        abfragebStatement.setInt(4, spieltag);
        try (final ResultSet resultAbfB = abfragebStatement.executeQuery()) {

          while (resultAbfB.next()) {
            posI++;

            spielNr = resultAbfB.getInt("spiel_nr");
            heimBez = resultAbfB.getString("heim_bez");
            auswBez = resultAbfB.getString("ausw_bez");
            heimTore = resultAbfB.getString("heim_tore");
            auswTore = resultAbfB.getString("ausw_tore");
            heimToreTipp = resultAbfB.getString("heim_tore_tipp");
            if (heimToreTipp == null) {
              heimToreTipp = StringUtils.EMPTY;
            }
            auswToreTipp = resultAbfB.getString("ausw_tore_tipp");
            if (auswToreTipp == null) {
              auswToreTipp = StringUtils.EMPTY;
            }
            punkte = resultAbfB.getInt("punkte");
            eingabeErlaubt = resultAbfB.getBoolean("eingabe_erlaubt");
            spielbeginn = resultAbfB.getString("spielbeginn");

            submit |= eingabeErlaubt;

            if (farbig) {
              formularT2.append("            <tr style=\"background-color:#5ea6ff;\">\n");
            } else {
              formularT2.append("            <tr style=\"background-color:#aed2ff;\">\n");
            }
            farbig = !farbig;

            formularT2.append("                <td><p class=\"spr\">" + posI + "</p></td>\n"
                + "                <td><p class=\"spl\">" + de.knightsoft.common.StringToHtml.convert(heimBez, true)
                + "</p></td>\n" + "                <td><p class=\"spl\">"
                + de.knightsoft.common.StringToHtml.convert(auswBez, true) + "</p></td>\n");
            if (heimTore == null) {
              formularT2.append("                <td><p class=\"spr\">steht aus</p></td>\n");
            } else {
              eingabeErlaubt = false;
              if (resultAbfB.getBoolean("spiel_beendet")) {
                formularT2
                    .append("                <td><p class=\"spc\">" + heimTore + "&nbsp;:&nbsp;" + auswTore + "</p></td>\n");
              } else {
                formularT2
                    .append("                <td><p class=\"spco\">" + heimTore + "&nbsp;:&nbsp;" + auswTore + "</p></td>\n");
              }
            }

            if (eingabeErlaubt) {
              if (selectedField == null) {
                selectedField = "heim_tore_tipp_" + posJ;
              }
              formularT2.append("                <td><p class=\"spr\"><input name=\"heim_tore_tipp_" + posJ
                  + "\" min=\"0\" max=\"99\" size=\"3\" type=\"number\" value=\"" + heimToreTipp
                  + "\" onChange=\"chkTipp(document.forms[0].heim_tore_tipp_" + posJ + ")\"><input name=\"lfd_spiele_nr_" + posJ
                  + "\" type=\"hidden\" value=\"" + (spielNr - 1) + "\"></p></td>\n"
                  + "                <td><p class=\"spr\"><input name=\"ausw_tore_tipp_" + posJ
                  + "\" min=\"0\" max=\"99\" size=\"3\" type=\"number\" value=\"" + auswToreTipp
                  + "\" onChange=\"chkTipp(document.forms[0].ausw_tore_tipp_" + posJ + ")\"></p></td>\n");
              posJ++;
            } else {
              if (StringUtils.isEmpty(heimToreTipp)) {
                formularT2.append("                <td colspan=\"2\"><p class=\"spc\">----</p></td>\n");
              } else {
                formularT2.append("                <td colspan=\"2\"><p class=\"spc\">" + heimToreTipp + "&nbsp;:&nbsp;"
                    + auswToreTipp + "</p></td>\n");
              }
            }

            formularT2.append(
                "                <td><p class=\"spr\">" + punkte + "</p></td>\n" + "                <td><p class=\"spl\">"
                    + de.knightsoft.common.StringToHtml.convert(spielbeginn, true) + "</p></td>\n" + "            </tr>\n");
            punkteGesamt += punkte;
          }
        }

        if (farbig) {
          formularT2.append("            <tr style=\"background-color:#5ea6ff;\">\n");
        } else {
          formularT2.append("            <tr style=\"background-color:#aed2ff;\">\n");
        }
        farbig = !farbig;

        formularT2.append("                <td colspan=\"6\"><p class=\"spl\">Punkte gesamt:</p></td>\n"
            + "                <td><p class=\"spr\">" + punkteGesamt + "</p></td>\n"
            + "                <td><p class=\"spl\">&nbsp;</p></td>\n" + "            </tr>\n");
      }
    } catch (final java.sql.SQLException e) {
      formularT2.append("                <td colspan=\"8\"><p class=\"spc\">Datenbankfehler: " + e.toString() + "</p></td>\n");
    }

    formular.append(myStNavigation.htmlLigaSpielNavigation(res, saisonLiga, (anzMannschaften - 1) * 2, session,
        de.knightsoft.common.Constants.HTML_BASE + "Tipprunde/" + saisonLiga + "/" + spieltag + "/Tippzettel.pdf"));

    formular.append("        <h2>Tipp " + spieltag + ". Spieltag der Liga: \""
        + de.knightsoft.common.StringToHtml.convert(ligabezeichnung) + "\"</h2>\n");

    if (hint != null) {
      formular.append("        <h3>" + de.knightsoft.common.StringToHtml.convert(hint.toString()) + "</h3>\n");
    }
    formular.append("        <form action=\""
        + res.encodeURL(
            de.knightsoft.knightsoftnet.common.KnConst.HTML_BASE + "Tipprunde/" + saisonLiga + "/" + spieltag + "/Tippen.html")
        + "\" method=\"POST\" enctype=\"application/x-www-form-urlencoded; charset=utf-8\" " //
        + "accept-charset=\"utf-8\">\n" //
        + "            <div style=\"text-align:center;\">\n" //
        + "            <input type=\"hidden\" name=\"saison_liga\" value=\"" + saisonLiga + "\">\n" //
        + "            <input type=\"hidden\" name=\"spieltag\" value=\"" + spieltag + "\">\n" //
        + "            <table style=\"margin-left:auto; margin-right:auto;\" " //
        + "border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" //
        + "                <tr style=\"background-color:#5ea6ff;\">\n" //
        + "                    <th><p class=\"shr\">Spiel</p></th>\n" //
        + "                    <th><p class=\"shl\">Heimmannschaft</p></th>\n" //
        + "                    <th><p class=\"shl\">Ausw&auml;rtsmannschaft</p></th>\n" //
        + "                    <th><p class=\"shr\">Ergebnis</p></th>\n" //
        + "                    <th colspan=\"2\"><p class=\"shc\">Tipp</p></th>\n" //
        + "                    <th><p class=\"shr\">Punkte</p></th>\n" //
        + "                    <th><p class=\"shl\">Spielbeginn</p></th>\n" //
        + "                </tr>\n" + formularT2.toString() //
        + "            </table>\n");

    if (submit) {
      formular.append("            <input type=\"submit\" name=\"submittype\" "
          + "value=\"Tipp abgeben/&auml;ndern\"> <input type=\"reset\" " + "value=\"Zur&uuml;cksetzen\">");
    }

    formular.append("            </div>\n" + "        </form>\n");
    session.setAttribute(servletName + de.knightsoft.common.AbstractVisualDb.SELECTED_FIELD_TEXT + serviceName, selectedField);

    return formular.toString();
  }

  /**
   * The Method <code>pdfFormular</code> generates the pdf code of the formular.
   *
   * @param document a opend iText document
   * @param saisonLiga number of the liga
   * @param spieltag number of the matchday
   * @param session The Data of the current Session
   */
  private void pdfFormular(final Document document, final int saisonLiga, final int spieltag, final HttpSession session)
      throws DocumentException, de.knightsoft.common.TextException {
    String ligabezeichnung = StringUtils.EMPTY;
    int anzMannschaften = 0;
    int spielNr;
    String heimBez;
    String auswBez;
    String heimToreTipp;
    String auswToreTipp;
    String heimTore;
    String auswTore;
    int punkte;
    int punkteGesamt = 0;
    boolean showPunkteGesamt = false;
    String punkteString;
    String spielbeginn;
    final String spitzname = (String) session.getAttribute(servletName + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_USER
        + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_USER);
    final Integer mandatorInteger =
        (Integer) session.getAttribute(servletName + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_MANDATOR);
    final int mandator = mandatorInteger == null ? 1 : mandatorInteger.intValue();

    try (final PreparedStatement abfrageaStatement = myDataBase.prepareStatement(Tippen.ABFRAGE_A);
        final PreparedStatement abfragebStatement = myDataBase.prepareStatement(Tippen.ABFRAGE_B)) {
      abfrageaStatement.clearParameters();
      abfrageaStatement.setInt(1, mandator);
      abfrageaStatement.setInt(2, saisonLiga);
      try (final ResultSet resultA = abfrageaStatement.executeQuery()) {

        if (resultA.next()) {
          anzMannschaften = resultA.getInt("anz_manschaften");
          ligabezeichnung = resultA.getString("bezeichnung");
        }
      }

      if ((anzMannschaften - 1) * 2 >= spieltag && spieltag >= 1) {
        final Paragraph Headerline = new Paragraph("Tipp " + spieltag + ". Spieltag der " + ligabezeichnung + " - " + spitzname,
            new Font(Font.FontFamily.TIMES_ROMAN, 18));
        Headerline.setAlignment(Element.ALIGN_CENTER);

        document.add(Headerline);
        document.add(new Paragraph(" ", new Font(Font.FontFamily.TIMES_ROMAN, 10)));

        final Font fontUeb = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
        final Font fontPos = new Font(Font.FontFamily.TIMES_ROMAN, 12);
        final float[] widths = {0.06f, 0.28f, 0.28f, 0.04f, 0.04f, 0.18f, 0.04f, 0.04f, 0.04f};
        final PdfPTable table = new PdfPTable(widths);
        table.setWidthPercentage(100);
        table.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(new PdfPCell(new Paragraph("Spiel", fontUeb)));
        table.addCell(new PdfPCell(new Paragraph("Heimmannschaft", fontUeb)));
        table.addCell(new PdfPCell(new Paragraph("Auswärtsmannschaft", fontUeb)));
        final PdfPCell cellTipp = new PdfPCell(new Paragraph("Tipp", fontUeb));
        cellTipp.setColspan(2);
        cellTipp.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cellTipp);
        table.addCell(new PdfPCell(new Paragraph("Spielbeginn", fontUeb)));
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
        final PdfPCell cellErg = new PdfPCell(new Paragraph("Ergeb.", fontUeb));
        cellErg.setColspan(2);
        cellErg.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cellErg);
        table.addCell(new PdfPCell(new Paragraph("P", fontUeb)));

        spielNr = 0;
        abfragebStatement.clearParameters();
        abfragebStatement.setString(1, spitzname);
        abfragebStatement.setInt(2, mandator);
        abfragebStatement.setInt(3, saisonLiga);
        abfragebStatement.setInt(4, spieltag);
        try (final ResultSet resultB = abfragebStatement.executeQuery()) {

          while (resultB.next()) {
            spielNr++;
            heimBez = resultB.getString("heim_bez");
            auswBez = resultB.getString("ausw_bez");
            heimToreTipp = resultB.getString("heim_tore_tipp");
            if (heimToreTipp == null) {
              heimToreTipp = StringUtils.EMPTY;
            }
            auswToreTipp = resultB.getString("ausw_tore_tipp");
            if (auswToreTipp == null) {
              auswToreTipp = StringUtils.EMPTY;
            }
            punkte = resultB.getInt("punkte");
            punkteGesamt += punkte;
            heimTore = resultB.getString("heim_tore");
            if (heimTore == null) {
              heimTore = StringUtils.EMPTY;
            }
            auswTore = resultB.getString("ausw_tore");
            if (auswTore == null) {
              auswTore = StringUtils.EMPTY;
              punkteString = StringUtils.EMPTY;
            } else {
              punkteString = Integer.toString(punkte);
              showPunkteGesamt = true;
            }
            spielbeginn = resultB.getString("spielbeginn");

            table.addCell(new PdfPCell(new Paragraph(Integer.toString(spielNr), fontPos)));
            table.addCell(new PdfPCell(new Paragraph(heimBez, fontPos)));
            table.addCell(new PdfPCell(new Paragraph(auswBez, fontPos)));
            final PdfPCell cell_ht = new PdfPCell(new Paragraph(heimToreTipp, fontPos));
            cell_ht.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(cell_ht);
            final PdfPCell cell_at = new PdfPCell(new Paragraph(auswToreTipp, fontPos));
            cell_at.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(cell_at);
            table.addCell(new PdfPCell(new Paragraph(spielbeginn, fontPos)));
            final PdfPCell cell_h = new PdfPCell(new Paragraph(heimTore, fontPos));
            cell_h.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(cell_h);
            final PdfPCell cell_a = new PdfPCell(new Paragraph(auswTore, fontPos));
            cell_a.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(cell_a);
            final PdfPCell cell_p = new PdfPCell(new Paragraph(punkteString, fontPos));
            cell_p.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(cell_p);
          }
        }
        final PdfPCell cellResultText = new PdfPCell(new Paragraph("Punkte gesamt:", fontPos));
        cellResultText.setColspan(6);
        cellResultText.setBorderColorLeft(new BaseColor(255, 255, 255));
        cellResultText.setBorderColorBottom(new BaseColor(255, 255, 255));
        table.addCell(cellResultText);
        if (showPunkteGesamt) {
          punkteString = Integer.toString(punkteGesamt);
        } else {
          punkteString = StringUtils.EMPTY;
        }
        final PdfPCell cell_pg = new PdfPCell(new Paragraph(punkteString, fontPos));
        cell_pg.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell_pg.setColspan(3);
        table.addCell(cell_pg);
        document.add(table);
      }
    } catch (final java.sql.SQLException e) {
      throw new de.knightsoft.common.TextException(e.getLocalizedMessage(), e);
    }
  }

  /**
   * The Method <code>eingabeVerarb</code> checks the input of the html formular and saves tipps to database when everything is
   * ok.
   *
   * @param req HTTP Servlet request
   * @param res HTTP Servlet response
   * @param saisonLiga number of the liga
   * @param spieltag number of the matchday
   * @param session The Data of the current Session
   * @param navTyp Navigation type
   * @return html input formular
   */
  public String eingabeVerarb(final HttpServletRequest req, final HttpServletResponse res, final int saisonLiga,
      final int spieltag, final HttpSession session, final String navTyp) {
    final StringBuilder hint = new StringBuilder(256);
    boolean schonBegonnen = false;
    int anzUpdates = 0;
    int anzFehlerhaft = 0;
    int posI = 0;
    int posJ = 0;
    String heimToreTipp = StringUtils.EMPTY;
    String auswToreTipp = StringUtils.EMPTY;
    String lfdSpieleNr = StringUtils.EMPTY;
    final String spitzname = (String) session.getAttribute(servletName + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_USER
        + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_USER);
    final Integer mandatorInteger =
        (Integer) session.getAttribute(servletName + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_MANDATOR);
    final int mandator = mandatorInteger == null ? 1 : mandatorInteger.intValue();

    try (final PreparedStatement eingabeLesenStatement = myDataBase.prepareStatement(Tippen.EINGABE_LESEN);
        final PreparedStatement eingabeSchreibenStatement = myDataBase.prepareStatement(Tippen.EINGABE_SCHREIBEN);
        final PreparedStatement abfrageInsertStatement = myDataBase.prepareStatement(Tippen.ABFRAGE_INSERT)) {
      for (posI = 0; lfdSpieleNr != null; posI++) {
        heimToreTipp = req.getParameter("heim_tore_tipp_" + posI);
        auswToreTipp = req.getParameter("ausw_tore_tipp_" + posI);
        lfdSpieleNr = req.getParameter("lfd_spiele_nr_" + posI);
        if (lfdSpieleNr != null) {
          try {

            if (StringUtils.isBlank(heimToreTipp)) {
              heimToreTipp = null;
            } else {
              for (posJ = 0; posJ < heimToreTipp.length(); posJ++) {
                if (!Character.isDigit(heimToreTipp.charAt(posJ))) {
                  throw new de.knightsoft.common.TextException("Das Feld 'Heim-Tore-Tipp' ist nicht nummerisch!");
                }
              }
            }

            if (StringUtils.isBlank(auswToreTipp)) {
              auswToreTipp = null;
            } else {
              for (posJ = 0; posJ < auswToreTipp.length(); posJ++) {
                if (!Character.isDigit(auswToreTipp.charAt(posJ))) {
                  throw new de.knightsoft.common.TextException("Das Feld 'Auswärts-Tore-Tipp' ist nicht nummerisch!");
                }
              }
            }

            eingabeLesenStatement.clearParameters();
            eingabeLesenStatement.setInt(1, mandator);
            eingabeLesenStatement.setInt(2, saisonLiga);
            eingabeLesenStatement.setInt(3, spieltag);
            eingabeLesenStatement.setInt(4, Integer.parseInt(lfdSpieleNr));
            try (final ResultSet result = eingabeLesenStatement.executeQuery()) {
              if (result.next()) {
                if (result.getBoolean("update_erlaubt")) {
                  schonBegonnen = false;
                } else {
                  schonBegonnen = true;
                }
              }
            }
            if (!schonBegonnen) {
              eingabeSchreibenStatement.clearParameters();
              if (heimToreTipp == null) {
                eingabeSchreibenStatement.setString(1, null);
              } else {
                eingabeSchreibenStatement.setInt(1, Integer.parseInt(heimToreTipp));
              }
              if (auswToreTipp == null) {
                eingabeSchreibenStatement.setString(2, null);
              } else {
                eingabeSchreibenStatement.setInt(2, Integer.parseInt(auswToreTipp));
              }
              eingabeSchreibenStatement.setInt(3, mandator);
              eingabeSchreibenStatement.setInt(4, saisonLiga);
              eingabeSchreibenStatement.setInt(5, spieltag);
              eingabeSchreibenStatement.setString(7, spitzname);
              eingabeSchreibenStatement.setInt(6, Integer.parseInt(lfdSpieleNr));
              posJ = eingabeSchreibenStatement.executeUpdate();
              if (posJ == -1) {
                if (heimToreTipp == null) {
                  eingabeSchreibenStatement.setString(1, null);
                } else {
                  eingabeSchreibenStatement.setInt(1, Integer.parseInt(heimToreTipp));
                }
                if (auswToreTipp == null) {
                  eingabeSchreibenStatement.setString(2, null);
                } else {
                  eingabeSchreibenStatement.setInt(2, Integer.parseInt(auswToreTipp));
                }
                eingabeSchreibenStatement.setInt(3, mandator);
                eingabeSchreibenStatement.setInt(4, saisonLiga);
                eingabeSchreibenStatement.setInt(5, spieltag);
                eingabeSchreibenStatement.setInt(6, Integer.parseInt(lfdSpieleNr));
                eingabeSchreibenStatement.setString(7, spitzname);
                posJ = eingabeSchreibenStatement.executeUpdate();
              }
            }
            if (posJ == 0) {
              abfrageInsertStatement.clearParameters();
              abfrageInsertStatement.setInt(1, mandator);
              abfrageInsertStatement.setInt(2, saisonLiga);
              abfrageInsertStatement.setInt(3, spieltag);
              abfrageInsertStatement.setInt(4, Integer.parseInt(lfdSpieleNr));
              abfrageInsertStatement.setString(5, spitzname);
              if (heimToreTipp == null) {
                abfrageInsertStatement.setString(6, null);
              } else {
                abfrageInsertStatement.setInt(6, Integer.parseInt(heimToreTipp));
              }
              if (auswToreTipp == null) {
                abfrageInsertStatement.setString(7, null);
              } else {
                abfrageInsertStatement.setInt(7, Integer.parseInt(auswToreTipp));
              }
              posJ = abfrageInsertStatement.executeUpdate();
            }
            anzUpdates += posJ;
          } catch (final java.sql.SQLException e) {
            hint.append("Datenbankfehler, Ihre Änderung konnte nicht (vollständig) " + "durchgeführt werden");
            lfdSpieleNr = null;
          } catch (final de.knightsoft.common.TextException e) {
            anzFehlerhaft++;
          }
        }
      }

      if (hint.length() == 0) {
        hint.append("Es wurden " + anzUpdates + " Spieletipps geändert.");

        switch (anzFehlerhaft) {
          case 0:
            break;
          case 1:
            hint.append(" Ein falscher Tipp wurde nicht übernommen.");
            break;
          default:
            hint.append(" " + anzFehlerhaft + " falsche Tipps wurden nicht übernommen.");
            break;
        }

        if (schonBegonnen) {
          hint.append(" Bereits angelaufene Spiel konnten nicht geändert werden.");
        }
      }
    } catch (final java.sql.SQLException e) {
      hint.append("Datenbankfehler, Ihre Änderung konnte nicht (vollständig) durchgeführt werden");
    }

    return this.htmlPage(res, hint, session, navTyp, saisonLiga, spieltag);
  }

  /**
   * The Method <code>doTheWork</code> coordinates all the work in this class
   *
   * @param req HttpServletRequest from the Servlet
   * @param res HttpServletResponse from the Servlet
   * @param session The Data of the current Session
   * @return html code of the formular
   * @exception de.knightsoft.common.TextException when a error in checking the passwords occours
   */
  @Override
  public String doTheWork(final HttpServletRequest req, final HttpServletResponse res, final HttpSession session)
      throws de.knightsoft.common.TextException {

    final String saisonLigaParm = req.getParameter("saison_liga");
    int saisonLigaInt = 0;
    final String spieltagParm = req.getParameter("spieltag");
    int spieltagInt = 0;
    final String submittype = req.getParameter("submittype");
    String formular = null;

    if (spieltagParm == null) {
      spieltagInt = 0;
    } else {
      spieltagInt = Integer.parseInt(spieltagParm);
    }

    if (saisonLigaParm == null) {
      saisonLigaInt = 0;
    } else {
      saisonLigaInt = Integer.parseInt(saisonLigaParm);
    }

    if (session.getAttribute(servletName + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_USER
        + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_USER) == null) {
      formular = "<h2>Sie sind nicht angemeldet, bitte per &quot;Login&quot; anmelden um zu tippen</h2>\n";
    } else {
      if (submittype == null || "Tippen".equals(submittype)) {
        formular = this.htmlPage(res, null, session, "Tippen", saisonLigaInt, spieltagInt);
      } else if ("Tipp abgeben/ändern".equals(submittype)) {
        formular = eingabeVerarb(req, res, saisonLigaInt, spieltagInt, session, "Tippen");
      } else if ("Tippzettel".equals(submittype)) {
        formular = StringUtils.EMPTY;

        final Document document = new Document(PageSize.A4);

        res.setContentType("application/pdf");

        try {
          PdfWriter.getInstance(document, res.getOutputStream());

          spieltagInt = Integer.parseInt(spieltagParm);
          saisonLigaInt = Integer.parseInt(saisonLigaParm);

          int pos = (spieltagInt - 1) / 3 * 3 + 1;

          document.addTitle("Tippzettel Saison-Liga " + saisonLigaParm + ", Spieltag " + Integer.toString(pos) + " bis "
              + Integer.toString(pos + 2));
          document.addSubject("Tippzettel für die KnightSoft-Net Tipprunde");
          document.addKeywords("Fußball, Tippen, KnightSoft");
          document.addCreator("KnightSoft-Net Tipprunde mit iText");
          document.addAuthor("Manfred Tremmel");

          document.open();

          pdfFormular(document, saisonLigaInt, pos++, session);
          document.add(new Paragraph(" ", new Font(Font.FontFamily.TIMES_ROMAN, 20)));
          pdfFormular(document, saisonLigaInt, pos++, session);
          document.add(new Paragraph(" ", new Font(Font.FontFamily.TIMES_ROMAN, 20)));
          pdfFormular(document, saisonLigaInt, pos++, session);
        } catch (final DocumentException de) {
          throw new de.knightsoft.common.TextException("document: " + de.getMessage(), de);
        } catch (final java.io.IOException ie) {
          throw new de.knightsoft.common.TextException("IO-Exception: " + ie.getMessage(), ie);
        }

        document.close();
      }
    }

    return formular;
  }

  /**
   * The Method <code>allowedToSee</code> tells you if the currently logged in user is allowed to access this application.
   *
   * @param session Data of the current session
   * @return true if it is allowed for this user
   */
  @Override
  public boolean allowedToSee(final HttpSession session) {
    return session.getAttribute(servletName + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_USER
        + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_USER) != null;
  }

  /**
   * The Method <code>allowedToChange</code> tells you if the currently logged in user is allowed to change data in this
   * application.
   *
   * @param session Data of the current session
   * @return true if it is allowed for this user
   */
  @Override
  public boolean allowedToChange(final HttpSession session) {
    return session.getAttribute(servletName + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_USER
        + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_USER) != null;
  }

}
