/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.knightsoftnet.bettingpool;

import de.knightsoft.knightsoftnet.common.KnConst;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * The <code>Faq</code> class is based on the abstract class de.knightsoft.common.AbstractVisualDb and implements the faq page
 * of the KnightSoft Tipprunde.
 *
 *
 * @author Manfred Tremmel
 * @version 2.0.0, 06.08.2006
 */
public class Faq extends de.knightsoft.common.AbstractVisualDb {
  /**
   * Konstruktor.
   *
   * @param thisDatabase Connection to database
   * @param servletname Name of the Servlet
   * @exception SQLException if a database access error occurs
   * @exception de.knightsoft.common.TextException to give out a text message
   * @see de.knightsoft.common.TextException
   */
  public Faq(final Connection thisDatabase, final String servletname) throws SQLException, de.knightsoft.common.TextException {
    super(thisDatabase, KnConst.HTML_BASE, servletname, KnConst.GIF_URL + "16x16/faq.png", "FAQ",
        "Tipprunden-FAQ (Frequently Asked Questions)");
  }

  /**
   * The Method <code>HTMLPage</code> generates the html formular.
   *
   * @param res HttpServletResponse from the Servlet
   * @param hint Hint to be displayed on the screen
   * @param session The Data of the current Session
   * @param navTyp Navigation type
   * @return HTML code of the formular
   */
  @Override
  protected String htmlPage(final HttpServletResponse res, final String hint, final HttpSession session, final String navTyp) {
    return "<h1><a name=\"inhalt\">Diese FAQ enth&auml;lt Fragen und Antworten zu folgenden " + "Punkten:</a></h1>\n" + "<ol>\n"
        + "    <li><a href=\"#reg\">Fragen zur Anmeldung/Registrierung</a>\n" + "        <ul>\n"
        + "            <li><a href=\"#reg_emaileingabe\">Ich will die E-Mail Infos nicht, weshalb mu&szlig; ich trotzdem eine E-Mail-Adresse angeben?</a></li>\n"
        + "        </ul>\n" + "    </li>\n" + "    <li><a href=\"#esta\">Fragen zur eigenen Statistik</a>\n" + "        <ul>\n"
        + "            <li><a href=\"#esta_wo\">Wo kann ich nachsehen, wieviele Punkte ich erhalten habe, wenn ich nicht in der Top 10 bin?</a></li>\n"
        + "        </ul>\n" + "    </li>\n" + "</ol>\n" + "<h2><a name=\"reg\">Fragen zur Anmeldung/Registrierung</a></h2>\n"
        + "<h4><a name=\"reg_emaileingabe\">Ich will die E-Mail Infos nicht, weshalb "
        + "mu&szlig; ich trotzdem eine E-Mail-Adresse angeben?</a></h4>\n"
        + "<p>Neben dem Versand der Informationen (Newsletter), wird die E-Mail-Adresse "
        + "auch zum Versand eines neuen Passwortes verwendet, sollte dieses vergessen " + "worden sein.</p>\n"
        + "<p style=\"text-align:right\"><a href=\"#inhalt\">Zur&uuml;ck</a></p>\n"
        + "<h2><a name=\"esta\">Fragen zur eigenen Statistik</a></h2>\n"
        + "<h4><a name=\"esta_wo\">Wo kann ich nachsehen, wieviele Punkte ich erhalten "
        + "habe, wenn ich nicht in der Top 10 bin?</a></h4>\n"
        + "<p>Nach einem Login ist die Limitierung auf zehn Eintr&auml;ge aufgehoben und "
        + "Ihr eigenes Ergebnis wird farblich hervorgehoben.</p>\n"
        + "<p style=\"text-align:right\"><a href=\"#inhalt\">Zur&uuml;ck</a></p>\n";
  }

  /**
   * The Method <code>preventFromCache</code> tells you if this page should be cached or not.
   *
   * @param session Data of the current session
   * @return don't cache this site = true
   */
  @Override
  public boolean preventFromCache(final HttpSession session) {
    return false;
  }

  /**
   * The Method <code>allowedToSee</code> tells you if the currently logged in user is allowed to access this application.
   *
   * @param session Data of the current session
   * @return true if it is allowed for this user
   */
  @Override
  public boolean allowedToSee(final HttpSession session) {
    return true;
  }

  /**
   * The Method <code>allowedToChange</code> tells you if the currently logged in user is allowed to change data in this
   * application.
   *
   * @param session Data of the current session
   * @return true if it is allowed for this user
   */
  @Override
  public boolean allowedToChange(final HttpSession session) {
    return true;
  }

}
