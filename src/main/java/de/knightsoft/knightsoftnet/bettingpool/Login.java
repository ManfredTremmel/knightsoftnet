/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.knightsoftnet.bettingpool;

import de.knightsoft.common.AbstractVisualDb;
import de.knightsoft.common.DataBaseDepending;
import de.knightsoft.common.NavTabStrukt;
import de.knightsoft.common.SendEMail;
import de.knightsoft.knightsoftnet.common.KnConst;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * The <code>Login</code> class is used for login and logout into the KnightSoft Net Tipprunde.
 *
 * @author Manfred Tremmel
 * @version 2.0.0, 06.08.2006
 */
public class Login extends de.knightsoft.common.AbstractVisualDb {

  protected static final int MAX_LOGIN = 5;

  private String passwordSql;
  private String passwordSqlOld;
  private String readUserSql;
  private String updateErrorSql;
  private String updateOkSql;
  private String readUserMailSql;
  private String updateMailSql;

  private Navigation thisNavigation;

  /**
   * Constructor
   *
   * @param thisDatabase Connection to database
   * @param servletname Name of the Servlet
   * @exception SQLException if a database access error occurs
   * @exception de.knightsoft.common.TextException to give out a text message
   * @see de.knightsoft.common.TextException
   */
  public Login(final Connection thisDatabase, final String servletname)
      throws java.sql.SQLException, de.knightsoft.common.TextException {
    super(thisDatabase, KnConst.HTML_BASE, servletname, KnConst.GIF_URL + "16x16/login.png", "Login",
        "Login für die Tipprunde");

    try {
      final DataBaseDepending myDataBaseDepending = new DataBaseDepending(thisDatabase.getMetaData().getDatabaseProductName());

      if (!myDataBaseDepending.getSqlPassword(StringUtils.EMPTY).equals(StringUtils.EMPTY)) {
        passwordSql = "SELECT     " + myDataBaseDepending.getSqlPassword(" ? ");
        passwordSqlOld = "SELECT     " + myDataBaseDepending.getSqlOldPassword(" ? ");
      }
      readUserSql = "SELECT        passwort, " + "                " + myDataBaseDepending.getSqlDiffFromNow("datum_fehllogin")
          + " AS waiting_period_run_off, " + "                mandator, " + "                spitzname, "
          + "                anzahl_fehllogin, " + "                stufe " + "FROM            KnightSoft_TippMitspieler "
          + "WHERE        Mandator = ? AND " + "                spitzname = ? ";

      updateErrorSql = "UPDATE        KnightSoft_TippMitspieler " + "SET            anzahl_fehllogin = ? , "
          + "                datum_fehllogin = " + myDataBaseDepending.getSqlTimeNow() + " " + "WHERE        Mandator = ? AND "
          + "                spitzname = ? ";

      updateOkSql = "UPDATE        KnightSoft_TippMitspieler " //
          + "SET            datum_login = " + myDataBaseDepending.getSqlTimeNow() + ", " //
          + "                passwort = " + myDataBaseDepending.getSqlPassword("?") + ", " //
          + "                anzahl_fehllogin = 0, " //
          + "                datum_fehllogin = NULL " //
          + "WHERE        Mandator = ? AND " //
          + "                spitzname = ? ";

      readUserMailSql = "SELECT        geschlecht, " + "                name, " + "                email "
          + "FROM            KnightSoft_TippMitspieler " + "WHERE        Mandator = ? AND " + "                spitzname = ? ";

      updateMailSql = "UPDATE        KnightSoft_TippMitspieler " //
          + "SET            datum_geaendert = " + myDataBaseDepending.getSqlTimeNow() + ", " //
          + "                passwort = " + myDataBaseDepending.getSqlPassword("?") + " " //
          + "WHERE        Mandator = ? AND " //
          + "                spitzname = ? ";

      thisNavigation = new Navigation(thisDatabase);
    } catch (final Exception e) {
      // shouldn't happen, ignore it ;-)
      e.printStackTrace();
    }
  }

  /**
   * build java script for this site.
   *
   * @return javaScript string
   */
  @Override
  protected String initJavaScript() throws SQLException {
    return super.initJavaScript() + "        <script type=\"text/javascript\">\n" + "            <!--\n"
        + "                PWCheck=0;\n\n" + "                function chkLogin()\n" + "                {\n"
        + "                    if (document.forms[0].spitzname.value.length == 0)\n" + "                    {\n"
        + "                        alert(\"Bitte geben Sie Ihren bei der Registrierung angegebenen "
        + "Spitznamen/ihren Benutzernamen ein\");\n" + "                        document.forms[0].spitzname.focus();\n"
        + "                        return false;\n" + "                    }\n\n" + "                    if (PWCheck == 1)\n"
        + "                    {\n" + "                        if (document.forms[0].passwort.value.length == 0)\n"
        + "                        {\n" + "                            alert(\"Bitte geben Sie Ihr bei der Registrierung "
        + "angegebenes Passwort ein\");\n" + "                            document.forms[0].passwort.focus();\n"
        + "                            return false;\n" + "                        }\n" + "                    }\n\n"
        + "                    return true;\n" + "                }\n" + "            //-->\n" + "        </script>\n";
  }

  /**
   * The Method <code>HTMLPage</code> generates the html formular.
   *
   * @param res HttpServletResponse from the Servlet
   * @param phint Hint to be displayed on the screen
   * @param session The Data of the current Session
   * @param navTyp Navigation type
   * @param user Name of the user
   * @param password password of the user
   * @return HTML code of the formular
   */
  protected String htmlPage(final HttpServletResponse res, final String phint, final HttpSession session, final String navTyp,
      final String user, final String password) {

    String hint = phint;
    if (hint == null) {
      hint = "Bitte geben Sie Ihren Benutzernamen (Spitzname) und Ihr Passwort ein";
    }

    session.setAttribute(servletName + AbstractVisualDb.SELECTED_FIELD_TEXT + serviceName, "spitzname");

    return "            <h2>" + de.knightsoft.common.StringToHtml.convert(hint) + "</h2>\n" + "            <form action=\""
        + res.encodeURL(KnConst.HTML_BASE + "Tipprunde/Login.html")
        + "\" method=\"POST\" enctype=\"application/x-www-form-urlencoded; charset=utf-8\" "
        + "accept-charset=\"utf-8\" OnSubmit=\"return chkLogin()\">\n"
        + "                <div><input type=\"hidden\" name=\"Mandator\" value=\"1\"></div>\n"
        + "                <table class=\"login\">\n"
        // + " <tr>\n"
        // + " <td align=\"left\">Mandant:</td>\n"
        // + " <td align=\"left\"><input type=\"Text\" name=\"Mandator\"
        // + size=\"5\" maxlength=\"5\" value=\""
        // + java.lang.Integer.toString(Mandator) + "\"></td>\n"
        // + " </tr>\n"
        + "                    <tr>\n" + "                        <td class=\"left\">Spitzname/Benutzer:</td>\n"
        + "                        <td class=\"left\"><input type=\"Text\" name=\"spitzname\" "
        + "size=\"30\" maxlength=\"50\" value=\"" + de.knightsoft.common.StringToHtml.convert(StringUtils.defaultString(user))
        + "\"></td>\n" + "                    </tr>\n" + "                    <tr>\n"
        + "                        <td class=\"left\">Passwort:</td>\n"
        + "                        <td class=\"left\"><input type=\"password\" name=\"passwort\" "
        + "size=\"30\" maxlength=\"30\" value=\""
        + de.knightsoft.common.StringToHtml.convert(StringUtils.defaultString(password)) + "\"></td>\n"
        + "                    </tr>\n" + "                    <tr>\n"
        + "                        <td colspan=\"2\" class=\"center\"><input type=\"submit\" "
        + "name=\"Submittype\" value=\"Absenden\" onClick=\"javascript:PWCheck=1\">"
        + "<input type=\"reset\" value=\"Zur&uuml;cksetzen\">&nbsp;&nbsp;&nbsp;"
        + "<input type=\"submit\" name=\"Submittype\" value=\"Passwort vergessen *\" "
        + "onClick=\"javascript:PWCheck=0\"></td>\n" + "                    </tr>\n" + "                </table>\n"
        + "            </form>\n" + "            <p class=\"anmerkung\">*) Sollten Sie Ihr Passwort vergessen haben, "
        + "w&auml;hlen sie die Liga, f&uuml;r die Sie sich angemeldet haben, geben Sie Ihren "
        + "Spitznamen bzw. Ihre Benutzerkennung ein und klicken Sie auf \"Passwort vergessen "
        + "*\". Ein Passwort wird per Zufallsgenerator erstellt und Ihnen umgehend per E-Mail " + "zugeschickt.</p>\n";
  }

  /**
   * The Method <code>HTMLPage</code> generates the html formular.
   *
   * @param res HttpServletResponse from the Servlet
   * @param hint Hint to be displayed on the screen
   * @param session The Data of the current Session
   * @param navTyp Navigation type
   * @return HTML code of the formular
   */
  @Override
  protected String htmlPage(final HttpServletResponse res, final String hint, final HttpSession session, final String navTyp) {

    return this.htmlPage(res, hint, session, navTyp, "", "");
  }

  /**
   * The Method <code>HTMLPageLogon</code> generates the html formular.
   *
   * @param res HttpServletResponse from the Servlet
   * @param hint Hint to be displayed on the screen
   * @param session The Data of the current Session
   * @param navTyp Navigation type
   * @return HTML code of the formular
   */
  protected String htmlPageLogon(final HttpServletResponse res, final String hint, final HttpSession session,
      final String navTyp) {
    return "<h2>Willkommen &quot;"
        + de.knightsoft.common.StringToHtml.convert((String) session.getAttribute(servletName
            + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_USER + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_USER))
        + "&quot; bei der KnightSoft-Net Tipprunde.</h2>";
  }

  /**
   * The <code>logonok</code> class checks, if the logon is ok or not.
   *
   * @param result ResultSet of the SQL request
   * @param user Username given in
   * @param password Password given in
   * @param session data of this session
   * @return true if logon is ok
   * @exception de.knightsoft.common.TextException when a error in checking the passwords occours
   */
  protected boolean logonok(final ResultSet result, final String user, final String password, final HttpSession session)
      throws de.knightsoft.common.TextException {
    final boolean isok = true;
    int mandator = 1;
    int stufe = 0;
    String dbUser = null;

    try (final PreparedStatement updateOKSQLStatement = myDataBase.prepareStatement(updateOkSql)) {
      mandator = result.getInt("Mandator");
      stufe = result.getInt("stufe");
      dbUser = result.getString("spitzname");
      session.setAttribute(servletName + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_USER
          + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_USER, dbUser);

      session.setAttribute(servletName + KnConst.SESSION_STUFE_FT, Integer.valueOf(stufe));

      final NavTabStrukt[] myNavTabStrukt = thisNavigation.getNavTabStrukt(servletName, "xxxxxx", session);
      session.setAttribute(servletName + KnConst.SESSION_NAVI_FT, myNavTabStrukt);

      updateOKSQLStatement.clearParameters();
      updateOKSQLStatement.setString(1, password);
      updateOKSQLStatement.setInt(2, mandator);
      updateOKSQLStatement.setString(3, user);
      updateOKSQLStatement.executeUpdate();
    } catch (final java.sql.SQLException e) {
      throw new de.knightsoft.common.TextException("Fehler beim Datenbankzugriff:\n" + e.toString(), e);
    }

    return isok;
  }

  /**
   * The <code>doLogout</code> class logs out the system.
   *
   * @param session Session data
   */
  public void doLogout(final HttpSession session) {
    session.setAttribute(
        servletName + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_USER + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_USER,
        null);
    session.setAttribute(servletName + KnConst.SESSION_STUFE_FT, Integer.valueOf(0));

    final NavTabStrukt[] myNavTabStrukt = thisNavigation.getNavTabStrukt();
    session.setAttribute(servletName + KnConst.SESSION_NAVI_FT, myNavTabStrukt);
  }

  /**
   * The <code>doLogon</code> class checks, if the user is already logged on and if he isn't, it checks the given parameter.
   *
   * @param req HttpServletRequest from the servlet
   * @param res HttpServletRespons from the servlet
   * @param session Session data
   * @return true if logon is ok or user is already logged on
   * @exception java.sql.SQLException
   * @exception de.knightsoft.common.TextException
   */
  public boolean doLogon(final HttpServletRequest req, final HttpServletResponse res, final HttpSession session)
      throws java.sql.SQLException, de.knightsoft.common.TextException {
    int posI = 0;
    int posJ = 0;
    final String mandatorString = req.getParameter("Mandator");
    int mandator = 1;
    final String user = req.getParameter("spitzname");
    final String password = req.getParameter("passwort");
    String passwordEncrypted = password;
    String passwordEncryptedOld = password;
    boolean logonok = false;

    if (mandatorString != null) {
      mandator = java.lang.Integer.parseInt(mandatorString);
    }

    if (passwordSql != null && passwordSqlOld != null && password != null) {
      try (final PreparedStatement passwordSQLStatement = myDataBase.prepareStatement(passwordSql);
          final PreparedStatement oldPasswordSQLStatement = myDataBase.prepareStatement(passwordSqlOld)) {
        passwordSQLStatement.clearParameters();
        passwordSQLStatement.setString(1, password);
        try (final ResultSet resultPWD = passwordSQLStatement.executeQuery()) {
          if (resultPWD.next()) {
            passwordEncrypted = resultPWD.getString(1);
          }
        }
        oldPasswordSQLStatement.clearParameters();
        oldPasswordSQLStatement.setString(1, password);
        try (final ResultSet resultPWD = oldPasswordSQLStatement.executeQuery()) {
          if (resultPWD.next()) {
            passwordEncryptedOld = resultPWD.getString(1);
          }
        }
      }
    }
    if (StringUtils.isEmpty(user) || StringUtils.isEmpty(password)) {
      throw new de.knightsoft.common.TextException("Bitte geben Sie Ihren Usernamen und Ihr Passwort ein");
    } else {
      try (final PreparedStatement readUserSqlStatement = myDataBase.prepareStatement(readUserSql)) {
        readUserSqlStatement.clearParameters();
        readUserSqlStatement.setInt(1, mandator);
        readUserSqlStatement.setString(2, user);
        try (final ResultSet result = readUserSqlStatement.executeQuery()) {
          while (!logonok && result.next()) {
            posJ++;
            posI = result.getInt("anzahl_fehllogin");
            if (posI >= Login.MAX_LOGIN) {
              if (result.getInt("waiting_period_run_off") < 1) {
                throw new de.knightsoft.common.TextException(
                    "Es gab heute bereits " + Login.MAX_LOGIN + " Fehlversuche, versuchen Sie es morgen wieder");
              } else {
                posI = 0;
              }
            }

            final String TransformedPasswordSaved = result.getString("passwort");
            if (passwordEncrypted != null && TransformedPasswordSaved != null
                && (TransformedPasswordSaved.equalsIgnoreCase(passwordEncrypted)
                    || TransformedPasswordSaved.equalsIgnoreCase(passwordEncryptedOld))) {
              logonok = logonok(result, user, password, session);
            }
          }
        }
      }

      if (posJ == 0) {
        throw new de.knightsoft.common.TextException("Die eingegebene Userkennung existiert nicht");
      } else if (!logonok) {
        posI++;

        try (final PreparedStatement updateErrorSqlStatement = myDataBase.prepareStatement(updateErrorSql)) {
          updateErrorSqlStatement.clearParameters();
          updateErrorSqlStatement.setInt(1, posI);
          updateErrorSqlStatement.setInt(2, mandator);
          updateErrorSqlStatement.setString(3, user);
          updateErrorSqlStatement.executeUpdate();
        }

        if (posI >= Login.MAX_LOGIN) {
          throw new de.knightsoft.common.TextException(
              "Dies war Ihr " + posI + ". Fehlversuch, der Zugang wird bis morgen gesperrt!");
        } else {
          throw new de.knightsoft.common.TextException("Dies war Ihr " + posI + ". Fehlversuch, nach " + Login.MAX_LOGIN
              + " wird der Zugang für heute gesperrt, Sie haben noch " + (Login.MAX_LOGIN - posI) + " Versuche");
        }
      }
    }
    return logonok;
  }

  /**
   * The <code>newPassword</code> method sends a new Password to the user.
   *
   * @param req HttpServletRequest from the servlet
   * @param res HttpServletRespons from the servlet
   * @exception java.sql.SQLException
   * @exception de.knightsoft.common.TextException
   */
  protected String newPassword(final HttpServletRequest req, final HttpServletResponse res)
      throws java.sql.SQLException, de.knightsoft.common.TextException, IOException {

    final String mandatorString = req.getParameter("Mandator");
    final String user = req.getParameter("spitzname");
    int mandator = 1;

    if (mandatorString != null) {
      mandator = java.lang.Integer.parseInt(mandatorString);
    }

    String geschlecht = null;
    String name = null;
    String email = null;
    try (final PreparedStatement readUserMailSqlStatement = myDataBase.prepareStatement(readUserMailSql)) {
      readUserMailSqlStatement.clearParameters();
      readUserMailSqlStatement.setInt(1, mandator);
      readUserMailSqlStatement.setString(2, user);
      try (final ResultSet result = readUserMailSqlStatement.executeQuery()) {
        if (result.next()) {
          geschlecht = result.getString("geschlecht");
          name = result.getString("name");
          email = result.getString("email");
        }
      }
    }

    if (name == null) {
      return "Der Benutzer/Spitzname existiert in der gewählten Liga nicht, "
          + "es konnte kein neues Passwort vergeben werden!";
    } else {
      final String passwort = de.knightsoft.common.Constants.generatePassword("KSFT");
      String emailStart = null;
      String emailText = null;

      if ("W".equals(geschlecht)) {
        emailStart = "Sehr geehrte Frau ";
      } else {
        emailStart = "Sehr geehrter Herr ";
      }

      emailText = emailStart + name + ",\n\n" + "für Ihren Eintrag mit der Benutzerkennung bzw.\n" + "dem Spitznamen '" + user
          + "' wurde ein\n" + "neues Passwort angefordert. Unser Zufallsgenerator\n" + "hat folgendes Passwort generiert:\n\n"
          + passwort + "\n\n" + "Bitte beachten Sie Groß- und Kleinschreibung bei\n"
          + "der Eingabe. Das bisherige Passwort ist nicht mehr\n" + "gültig!\n"
          + "Bei Fragen oder Problemen stehen wir Ihnen jeder-\n" + "zeit mir Rat und Tat zur Seite.\n\n"
          + "Mit freundlichen Grüssen,\n\n" + "Manfred Tremmel";

      new SendEMail(KnConst.EMAIL_FT, de.knightsoft.common.Constants.ORGANISATION, email, "Neue Passwortanforderung",
          emailText);

      try (final PreparedStatement updateMailSqlStatement = myDataBase.prepareStatement(updateMailSql)) {
        updateMailSqlStatement.clearParameters();
        updateMailSqlStatement.setString(1, passwort);
        updateMailSqlStatement.setInt(2, mandator);
        updateMailSqlStatement.setString(3, user);
        updateMailSqlStatement.executeUpdate();
      }

      return "Ein neues Passwort wurde Ihnen per E-Mail zugesandt!";
    }
  }

  /**
   * The Method <code>doTheWork</code> coordinates all the work in this class
   *
   * @param req HttpServletRequest from the Servlet
   * @param res HttpServletResponse from the Servlet
   * @param session The Data of the current Session
   * @return html code of the formular
   * @exception de.knightsoft.common.TextException when a error in checking the passwords occours
   */
  @Override
  public String doTheWork(final HttpServletRequest req, final HttpServletResponse res, final HttpSession session)
      throws de.knightsoft.common.TextException {
    String returnwert = null;
    final String navTyp = req.getParameter("Submittype");

    if (navTyp == null) {
      returnwert = this.htmlPage(res, null, session, navTyp);
    } else if ("Logout".equals(navTyp)) {
      doLogout(session);
      returnwert = this.htmlPage(res, null, session, navTyp);
    } else if ("Absenden".equals(navTyp)) {
      final String user = req.getParameter("spitzname");
      final String password = req.getParameter("passwort");
      try {
        if (doLogon(req, res, session)) {
          returnwert = htmlPageLogon(res, null, session, null);
        } else {
          returnwert = this.htmlPage(res, "Logon fehlgeschlagen", session, navTyp, user, password);
        }
      } catch (final java.sql.SQLException e) {
        returnwert = this.htmlPage(res, "Fehler beim Datenbankzugriff:\n" + e.toString(), session, navTyp, user, password);
      } catch (final de.knightsoft.common.TextException e) {
        returnwert = this.htmlPage(res, e.toString(), session, navTyp, user, password);
      }

    } else if ("Passwort vergessen *".equals(navTyp)) {
      String hint = null;
      try {
        hint = newPassword(req, res);
      } catch (final java.sql.SQLException e) {
        hint = "Fehler beim Datenbankzugriff: " + e.toString();
      } catch (final java.io.IOException e) {
        hint = "Fehler beim versenden der E-Mail, bitte nochmals versuchen: " + e.toString();
      }
      returnwert = this.htmlPage(res, hint, session, navTyp);
    } else {
      returnwert = this.htmlPage(res, null, session, navTyp);
    }
    return returnwert;
  }

  /**
   * The Method <code>changesNavagation</code> tells you if this Class can change navigation (must be read again from the
   * session).
   *
   * @param session Data of the current session
   * @return true if it is allowed for this user
   */
  @Override
  public boolean changesNavigation(final HttpSession session) {
    return true;
  }

  /**
   * The Method <code>allowedToSee</code> tells you if the currently logged in user is allowed to access this application.
   *
   * @param session Data of the current session
   * @return true if it is allowed for this user
   */
  @Override
  public boolean allowedToSee(final HttpSession session) {
    return true;
  }

  /**
   * The Method <code>allowedToChange</code> tells you if the currently logged in user is allowed to change data in this
   * application.
   *
   * @param session Data of the current session
   * @return true if it is allowed for this user
   */
  @Override
  public boolean allowedToChange(final HttpSession session) {
    return true;
  }
}
