/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.knightsoftnet.bettingpool;

import de.knightsoft.common.StringToHtml;

import org.apache.commons.lang3.StringUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.NumberFormat;

/**
 * The <code> LigaTippStatistik</code> class generates different statistics for a matchday.
 *
 * @author Manfred Tremmel
 * @version 2.0.1, 02.01.2010
 */
public class LigaTippStatistik {

  private static final String[] ABFRAGE_SQL = { //
      "SELECT            ts.spieltag AS spieltag, " //
          + "                tst.spitzname AS spitzname, " //
          + "                COUNT(if (ts.tore_heim IS NOT NULL, 0, 1)" //
          + "                ) AS anz_gewertete_spiele, " //
          + "                SUM(" //
          + "                    if (ts.tore_heim = tst.tore_heim " //
          + "                    AND ts.tore_ausw = tst.tore_ausw, " //
          + "                        tl.tr_punkte_exakt, " //
          + "                        if ((CAST(ts.tore_heim AS signed) - " //
          + "                             CAST(ts.tore_ausw AS signed)) = " //
          + "                            (CAST(tst.tore_heim AS signed) - " //
          + "                             CAST(tst.tore_ausw AS signed)), " //
          + "                            tl.tr_punkte_torver, " //
          + "                            if (((ts.tore_heim  > ts.tore_ausw) AND " //
          + "                                 (tst.tore_heim > tst.tore_ausw)) OR " //
          + "                                ((ts.tore_heim  < ts.tore_ausw) AND " //
          + "                                 (tst.tore_heim < tst.tore_ausw)), " //
          + "                                tl.tr_punkte_toto, " //
          + "                                tl.tr_punkte_falsch " //
          + "                                ) " //
          + "                            ) " //
          + "                        ) " //
          + "                    ) AS punkte " //
          + "FROM            KnightSoft_TippSpiele AS ts " //
          + "    LEFT JOIN    KnightSoft_TippSpieleTipp AS tst " //
          + "    ON            (tst.Mandator=ts.Mandator AND " //
          + "                 tst.saison_liga=ts.saison_liga AND " //
          + "                 tst.spieltag=ts.spieltag AND " //
          + "                 tst.lfd_spiele_nr=ts.lfd_spiele_nr) " //
          + "    LEFT JOIN    KnightSoft_TippLiga AS tl " //
          + "    ON            (ts.Mandator=tl.Mandator AND " //
          + "                 ts.saison_liga=tl.saison_liga) " //
          + "WHERE            ts.Mandator = ? AND " //
          + "                ts.saison_liga = ? AND " //
          + "                ts.spieltag = ? AND " //
          + "                ts.tore_heim IS NOT NULL AND " //
          + "                tst.tore_heim IS NOT NULL " //
          + "GROUP BY        spitzname " //
          + "ORDER BY        punkte DESC, spitzname " //
          + "LIMIT            ?",

      "SELECT            ts.spieltag AS spieltag, " //
          + "                tst.spitzname AS spitzname, " //
          + "                COUNT(if (ts.tore_heim IS NOT NULL, 0, 1)" //
          + "                ) AS anz_gewertete_spiele, " //
          + "                SUM(" //
          + "                    if (ts.tore_heim = tst.tore_heim" //
          + "                    AND ts.tore_ausw = tst.tore_ausw, " //
          + "                        tl.tr_punkte_exakt, " //
          + "                        if ((CAST(ts.tore_heim AS signed) - " //
          + "                             CAST(ts.tore_ausw AS signed)) = " //
          + "                            (CAST(tst.tore_heim AS signed) - " //
          + "                             CAST(tst.tore_ausw AS signed)), " //
          + "                            tl.tr_punkte_torver, " //
          + "                            if (((ts.tore_heim  > ts.tore_ausw) AND " //
          + "                                 (tst.tore_heim > tst.tore_ausw)) OR " //
          + "                                ((ts.tore_heim  < ts.tore_ausw) AND " //
          + "                                 (tst.tore_heim < tst.tore_ausw)), " //
          + "                                tl.tr_punkte_toto, " //
          + "                                tl.tr_punkte_falsch " //
          + "                                ) " //
          + "                            ) " //
          + "                        ) " //
          + "                    ) AS punkte " //
          + "FROM            KnightSoft_TippSpiele AS ts " //
          + "    LEFT JOIN    KnightSoft_TippSpieleTipp AS tst " //
          + "    ON            (tst.Mandator=ts.Mandator AND " //
          + "                 tst.saison_liga=ts.saison_liga AND " //
          + "                 tst.spieltag=ts.spieltag AND " //
          + "                 tst.lfd_spiele_nr=ts.lfd_spiele_nr) " //
          + "    LEFT JOIN    KnightSoft_TippLiga AS tl " //
          + "    ON            (ts.Mandator=tl.Mandator AND " //
          + "                 ts.saison_liga=tl.saison_liga) " //
          + "WHERE            ts.Mandator = ? AND " //
          + "                ts.saison_liga = ? AND " //
          + "                ts.spieltag <= ? AND " //
          + "                ts.tore_heim IS NOT NULL AND " //
          + "                tst.tore_heim IS NOT NULL " //
          + "GROUP BY        spitzname " //
          + "ORDER BY        punkte DESC, spitzname " //
          + "LIMIT            ?",

      "SELECT            ts.spieltag AS spieltag, " //
          + "                tst.spitzname AS spitzname, " //
          + "                COUNT(if (ts.tore_heim IS NOT NULL, 0, 1)" //
          + "                ) AS anz_gewertete_spiele, " //
          + "                SUM(" //
          + "                    if (ts.tore_heim = tst.tore_heim " //
          + "                    AND ts.tore_ausw = tst.tore_ausw, " //
          + "                        tl.tr_punkte_exakt, " //
          + "                        if ((CAST(ts.tore_heim AS signed) - " //
          + "                             CAST(ts.tore_ausw AS signed)) = " //
          + "                            (CAST(tst.tore_heim AS signed) - " //
          + "                             CAST(tst.tore_ausw AS signed)), " //
          + "                            tl.tr_punkte_torver, " //
          + "                            if (((ts.tore_heim  > ts.tore_ausw) AND " //
          + "                                 (tst.tore_heim > tst.tore_ausw)) OR " //
          + "                                ((ts.tore_heim  < ts.tore_ausw) AND " //
          + "                                 (tst.tore_heim < tst.tore_ausw)), " //
          + "                                tl.tr_punkte_toto, " //
          + "                                tl.tr_punkte_falsch " //
          + "                                ) " //
          + "                            ) " //
          + "                        ) " //
          + "                    ) AS punkte " //
          + "FROM            KnightSoft_TippSpiele AS ts " //
          + "    LEFT JOIN    KnightSoft_TippSpieleTipp AS tst " //
          + "    ON            (tst.Mandator=ts.Mandator AND " //
          + "                 tst.saison_liga=ts.saison_liga AND " //
          + "                 tst.spieltag=ts.spieltag AND " //
          + "                 tst.lfd_spiele_nr=ts.lfd_spiele_nr) " //
          + "    LEFT JOIN    KnightSoft_TippLiga AS tl " //
          + "    ON            (ts.Mandator=tl.Mandator AND " //
          + "                 ts.saison_liga=tl.saison_liga) " //
          + "WHERE            ts.Mandator = ? AND " //
          + "                ts.saison_liga = ? AND " //
          + "                ts.spieltag <= ? AND " //
          + "                ts.tore_heim IS NOT NULL AND " //
          + "                tst.tore_heim IS NOT NULL " //
          + "GROUP BY        spitzname, spieltag " //
          + "ORDER BY        punkte DESC, spitzname " //
          + "LIMIT            ?",

      "SELECT            ts.spieltag AS spieltag, " //
          + "                tst.spitzname AS spitzname, " //
          + "                COUNT(if (ts.tore_heim IS NOT NULL, 0, 1)" //
          + "                ) AS anz_gewertete_spiele, " //
          + "                SUM(" //
          + "                    if (ts.tore_heim = tst.tore_heim " //
          + "                    AND ts.tore_ausw = tst.tore_ausw, " //
          + "                        tl.tr_punkte_exakt, " //
          + "                        if ((CAST(ts.tore_heim AS signed) - " //
          + "                             CAST(ts.tore_ausw AS signed)) = " //
          + "                            (CAST(tst.tore_heim AS signed) - " //
          + "                             CAST(tst.tore_ausw AS signed)), " //
          + "                            tl.tr_punkte_torver, " //
          + "                            if (((ts.tore_heim  > ts.tore_ausw) AND " //
          + "                                 (tst.tore_heim > tst.tore_ausw)) OR " //
          + "                                ((ts.tore_heim  < ts.tore_ausw) AND " //
          + "                                 (tst.tore_heim < tst.tore_ausw)), " //
          + "                                tl.tr_punkte_toto, " //
          + "                                tl.tr_punkte_falsch " //
          + "                                ) " //
          + "                            ) " //
          + "                        ) " //
          + "                    ) / " //
          + "                COUNT(if (ts.tore_heim IS NOT NULL, 0, 1)) AS punkte_je_spiel " //
          + "FROM            KnightSoft_TippSpiele AS ts " //
          + "    LEFT JOIN    KnightSoft_TippSpieleTipp AS tst " //
          + "    ON             (tst.Mandator=ts.Mandator AND " //
          + "                  tst.saison_liga=ts.saison_liga AND " //
          + "                 tst.spieltag=ts.spieltag AND " //
          + "                 tst.lfd_spiele_nr=ts.lfd_spiele_nr) " //
          + "    LEFT JOIN    KnightSoft_TippLiga AS tl " //
          + "    ON            (ts.Mandator=tl.Mandator AND " //
          + "                 ts.saison_liga=tl.saison_liga) " //
          + "WHERE            ts.Mandator = ? AND " //
          + "                ts.saison_liga = ? AND " //
          + "                ts.spieltag <= ? AND " //
          + "                ts.tore_heim IS NOT NULL AND " //
          + "                tst.tore_heim IS NOT NULL " //
          + "GROUP BY        spitzname " //
          + "ORDER BY        punkte_je_spiel DESC, spitzname " //
          + "LIMIT            ?"};

  private final Connection tippDb;
  private final int anzListe;
  private final NumberFormat formZahl;

  /**
   * Constructor.
   *
   * @param thisDatabase Connection to database
   * @param anzListeUeb number results of every statistic
   */
  public LigaTippStatistik(final Connection thisDatabase, final int anzListeUeb) {
    anzListe = anzListeUeb;
    tippDb = thisDatabase;
    formZahl = NumberFormat.getInstance(java.util.Locale.GERMAN);
  }

  /**
   * The Method <code>HTML_LigaSpieltag</code> returns the statistic as html table.
   *
   * @param mandator mandator number
   * @param saisonLiga number of the liga
   * @param spieltag number of the matchday
   * @param typ statistic typ you want to get 0 - Top x of the matchday y 1 - Top x league of the players until the matchday y 2
   *        - Top x results of all tipps until matchday y 3 - Top x best result/matchday until matchday y
   * @param loginuser name of the loged in user, if none, enter null
   * @return html table
   */
  public String htmlLigaSpieltag(final int mandator, final int saisonLiga, final int spieltag, final int typ,
      final String loginuser) {
    boolean farbig = false;
    String formular = null;
    final StringBuilder formularT2 = new StringBuilder(512);
    String spitzname = null;
    String cssclass = StringUtils.EMPTY;
    String dummy1 = StringUtils.EMPTY;
    String dummy2 = StringUtils.EMPTY;
    String dummy3 = StringUtils.EMPTY;
    String dummy4 = StringUtils.EMPTY;
    int anzGewerteteSpiele;
    int pos;
    int anzListeAkt;

    try {
      if (loginuser == null || typ == 2) {
        anzListeAkt = anzListe;
      } else {
        anzListeAkt = 100;
      }

      pos = 0;
      try (final PreparedStatement abfrageStatement = tippDb.prepareStatement(LigaTippStatistik.ABFRAGE_SQL[typ])) {
        abfrageStatement.clearParameters();
        abfrageStatement.setInt(1, mandator);
        abfrageStatement.setInt(2, saisonLiga);
        abfrageStatement.setInt(3, spieltag);
        abfrageStatement.setInt(4, anzListeAkt);
        try (ResultSet result = abfrageStatement.executeQuery()) {
          while (result.next()) {
            anzGewerteteSpiele = result.getInt("anz_gewertete_spiele");
            spitzname = result.getString("spitzname");

            if (loginuser != null && loginuser.equalsIgnoreCase(spitzname)) {
              cssclass = "log";
            } else {
              cssclass = StringUtils.EMPTY;
            }

            pos++;

            if (farbig) {
              formularT2.append("            <tr style=\"background-color:#5ea6ff;\">\n");
            } else {
              formularT2.append("            <tr style=\"background-color:#aed2ff;\">\n");
            }
            farbig = !farbig;

            formularT2.append("                <td><p class=\"spr" + cssclass + "\">" + pos + "</p></td>\n");

            if (typ == 2) {
              formularT2.append(
                  "                <td><p class=\"spr" + cssclass + "\">" + result.getString("spieltag") + "</p></td>\n");
            }

            formularT2.append("                <td><p class=\"spl" + cssclass + "\">" + StringToHtml.convert(spitzname, true)
                + "</p></td>\n");

            if (typ == 3) {
              formularT2.append("                <td><p class=\"spr" + cssclass + "\">"
                  + StringToHtml.convert(formZahl.format(result.getDouble("punkte_je_spiel"))) + "</p></td>\n");
            } else {
              formularT2
                  .append("                <td><p class=\"spr" + cssclass + "\">" + result.getString("punkte") + "</p></td>\n");
            }

            formularT2.append("                <td><p class=\"spr" + cssclass + "\">" + anzGewerteteSpiele + "</p></td>\n"
                + "            </tr>\n");
          }
        }
      }

      if (pos > 0) {
        switch (typ) {
          case 0:
            dummy1 = "        <h3>Die " + pos + " besten Tipps des Spieltages</h3>\n";
            break;
          case 1:
            dummy1 = "        <h3>Die " + pos + " besten Gesamtergebnisse bis zum aktuellen Spieltag</h3>\n";
            break;
          case 2:
            dummy1 = "        <h3>Die " + pos + " besten Tipps bis zum aktuellen Spieltag</h3>\n";
            break;
          default:
            dummy1 = "        <h3>Die Spieler mit den " + pos + " besten \"Punkte je Spiel\"-Quoten</h3>\n";
            break;
        }

        if (typ == 2) {
          dummy2 = "                <th><p class=\"shr\">Spieltag</p></th>\n";
        } else {
          dummy2 = StringUtils.EMPTY;
        }

        dummy3 = "                <th><p class=\"shl\">Mitspieler</p></th>\n";

        if (typ == 3) {
          dummy4 = "                <th><p class=\"shr\">Punkte je Spiel</p></th>\n";
        } else {
          dummy4 = "                <th><p class=\"shr\">Punkte</p></th>\n";
        }

        formular = dummy1 + "        <table style=\"margin-left:auto; margin-right:auto;\" border=\"0\" " //
            + "cellspacing=\"0\" cellpadding=\"0\">\n" //
            + "            <tr style=\"background-color:#5ea6ff;\">\n" //
            + "                <th><p class=\"shr\">Position</p></th>\n" //
            + dummy2 + dummy3 + dummy4 //
            + "                <th><p class=\"shr\">Anzahl gewerteter Spiele</p></th>\n" //
            + "            </tr>\n" + formularT2.toString() //
            + "        </table>\n";
      }
    } catch (final java.sql.SQLException e) {
      formular = "<h2>Fehler beim Datenbankzugriff (Liga-Spieltag): " + e.toString() + "</h2>\n";
    }

    return formular;
  }
}
