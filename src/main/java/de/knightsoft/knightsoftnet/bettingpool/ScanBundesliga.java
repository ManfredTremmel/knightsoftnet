/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.knightsoftnet.bettingpool;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class ScanBundesliga {

  /**
   * text which describes the arguments.
   */
  private static final String HINT = //
      "Usage:\n" + "java de.knightsoft.knightsoftnet.bettingpool.ScanBundesliga <arguments>\n\n" //
          + "-dbdriver <name>      name of the jdbc-driver (must be filled)\n" //
          + "-dbdatabase <name>    connection string to database (must be filled)\n" //
          + "-dbuser <name>        db user to connect to database (must be filled)\n" //
          + "-dbpassword <name>    password to connect to database (must be filled)\n" //
          + "-mandator <number>    mandator number (must be filled)\n" //
          + "-league <number>      number of the league (must be filled)\n" //
          + "-create               create new games\n" //
          + "-update               update existing games\n";

  /**
   * read teams with numbers.
   */
  private static final String SQL_GET_TEAMS = //
      "SELECT TM.bezeichnung, LM.lfd_liga_ms " //
          + "FROM KnightSoft_TippLigaMannschaft AS LM " //
          + " LEFT JOIN KnightSoft_TippMannschaften AS TM " //
          + " ON (TM.Mandator = LM.Mandator " //
          + " AND TM.zaehler = LM.zaehler) " //
          + "WHERE LM.Mandator = ? " //
          + " AND LM.saison_liga = ? ";

  /**
   * insert a match.
   */
  private static final String SQL_INSERT_MATCH = //
      "INSERT INTO KnightSoft_TippSpiele VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

  /**
   * update a match.
   */
  private static final String SQL_UPDATE_MATCH = //
      "UPDATE KnightSoft_TippSpiele " //
          + "SET tore_heim = ?, tore_ausw = ?, spielbeginn = ?, spiel_beendet = ? " //
          + "WHERE Mandator = ? " //
          + "  AND saison_liga = ? " //
          + "  AND spieltag = ? " //
          + "  AND mannschaft_heim = ? " //
          + "  AND mannschaft_ausw = ? ";

  /**
   * team names to replace.
   */
  private static final Map<String, String> TEAM_REPLACEMENTS = new HashMap<>();

  static {
    ScanBundesliga.TEAM_REPLACEMENTS.put("1. FC Nürnberg II", "1. FC Nürnberg");
    ScanBundesliga.TEAM_REPLACEMENTS.put("SC Paderborn 07 U 19", "SC Paderborn 07");
  }


  /**
   * season league to url map.
   */
  private static final Map<Integer, String> SEASON_LEAGUE_URL_MAP = new HashMap<>();

  static {
    ScanBundesliga.SEASON_LEAGUE_URL_MAP.put(Integer.valueOf(201500001),
        "https://www.dfb.de/bundesliga/spieltagtabelle/?spieledb_path=%%2Fcompetitions"
            + "%%2F12%%2Fseasons%%2F17399%%2Fmatchday%%2F%d");
    ScanBundesliga.SEASON_LEAGUE_URL_MAP.put(Integer.valueOf(201500002),
        "https://www.dfb.de/2-bundesliga/spieltagtabelle/?spieledb_path=%%2Fcompetitions"
            + "%%2F3%%2Fseasons%%2F17402%%2Fmatchday%%2F%d");
    ScanBundesliga.SEASON_LEAGUE_URL_MAP.put(Integer.valueOf(201600001),
        "https://www.dfb.de/bundesliga/spieltagtabelle/?spieledb_path=%%2Fcompetitions"
            + "%%2F12%%2Fseasons%%2F17444%%2Fmatchday%%2F%d");
    ScanBundesliga.SEASON_LEAGUE_URL_MAP.put(Integer.valueOf(201600002),
        "https://www.dfb.de/2-bundesliga/spieltagtabelle/?spieledb_path=%%2Fcompetitions"
            + "%%2F3%%2Fseasons%%2F17551%%2Fmatchday%%2F%d");
    ScanBundesliga.SEASON_LEAGUE_URL_MAP.put(Integer.valueOf(201700001),
        "https://www.dfb.de/bundesliga/spieltagtabelle/?no_cache=1&spieledb_path=%%2F"
            + "competitions%%2F12%%2Fseasons%%2F17683%%2Fmatchday%%2F%d");
    ScanBundesliga.SEASON_LEAGUE_URL_MAP.put(Integer.valueOf(201700002),
        "https://www.dfb.de/2-bundesliga/spieltagtabelle/?no_cache=1&spieledb_path=%%2F"
            + "competitions%%2F3%%2Fseasons%%2F17676%%2Fmatchday%%2F%d");
    ScanBundesliga.SEASON_LEAGUE_URL_MAP.put(Integer.valueOf(201800001),
        "https://www.dfb.de/bundesliga/spieltagtabelle/?no_cache=1&spieledb_path=%%2F"
            + "competitions%%2F12%%2Fseasons%%2F17820%%2Fmatchday%%2F%d");
    ScanBundesliga.SEASON_LEAGUE_URL_MAP.put(Integer.valueOf(201800002),
        "https://www.dfb.de/2-bundesliga/spieltagtabelle/?no_cache=1&spieledb_path=%%2F"
            + "competitions%%2F3%%2Fseasons%%2F17821%%2Fmatchday%%2F%d");
    ScanBundesliga.SEASON_LEAGUE_URL_MAP.put(Integer.valueOf(201900001),
        "https://www.dfb.de/bundesliga/spieltagtabelle/?no_cache=1&spieledb_path=%%2F"
            + "competitions%%2F12%%2Fseasons%%2F18010%%2Fmatchday%%2F%d");
    ScanBundesliga.SEASON_LEAGUE_URL_MAP.put(Integer.valueOf(201900002),
        "https://www.dfb.de/2-bundesliga/spieltagtabelle/?no_cache=1&spieledb_path=%%2F"
            + "competitions%%2F3%%2Fseasons%%2F18011%%2Fmatchday%%2F%d");
    ScanBundesliga.SEASON_LEAGUE_URL_MAP.put(Integer.valueOf(202000001),
        "https://www.dfb.de/bundesliga/spieltagtabelle/?spieledb_path=/datencenter/bundesliga/"
            + "2020-21/current&spieledb_path=%%2Fdatencenter%%2Fbundesliga%%2F2020-21%%2F%d");
    ScanBundesliga.SEASON_LEAGUE_URL_MAP.put(Integer.valueOf(202000002),
        "https://www.dfb.de/2-bundesliga/spieltagtabelle/?no_cache=1&spieledb_path=%%2Fde%%2F"
            + "competitions%%2F2-bundesliga%%2Fseasons%%2F2020-21%%2Fmatchday%%2F%d");
    ScanBundesliga.SEASON_LEAGUE_URL_MAP.put(Integer.valueOf(202100001),
        "https://www.dfb.de/bundesliga/spieltagtabelle/?spieledb_path=/datencenter/bundesliga/"
            + "2021-22/current&spieledb_path=%%2Fdatencenter%%2Fbundesliga%%2F2021-22%%2F%d");
    ScanBundesliga.SEASON_LEAGUE_URL_MAP.put(Integer.valueOf(202100002),
        "https://www.dfb.de/2-bundesliga/spieltagtabelle/?no_cache=1&spieledb_path=%%2Fde%%2F"
            + "competitions%%2F2-bundesliga%%2Fseasons%%2F2021-22%%2Fmatchday%%2F%d");
    ScanBundesliga.SEASON_LEAGUE_URL_MAP.put(Integer.valueOf(202200001),
        "https://www.dfb.de/bundesliga/spieltagtabelle/?spieledb_path=/datencenter/bundesliga/"
            + "2022-23/current&spieledb_path=%%2Fdatencenter%%2Fbundesliga%%2F2022-23%%2F%d");
    ScanBundesliga.SEASON_LEAGUE_URL_MAP.put(Integer.valueOf(202200002),
        "https://www.dfb.de/2-bundesliga/spieltagtabelle/?no_cache=1&spieledb_path=%%2Fde%%2F"
            + "competitions%%2F2-bundesliga%%2Fseasons%%2F2022-23%%2Fmatchday%%2F%d");
    ScanBundesliga.SEASON_LEAGUE_URL_MAP.put(Integer.valueOf(202300001),
        "https://www.dfb.de/bundesliga/spieltagtabelle/?spieledb_path=/datencenter/bundesliga/2023-24/%d");
    ScanBundesliga.SEASON_LEAGUE_URL_MAP.put(Integer.valueOf(202300002),
        "https://www.dfb.de/2-bundesliga/spieltagtabelle/?spieledb_path=/datencenter/2-bundesliga/2023-24/%d");
  }

  /**
   * main method used in batch mode.
   *
   * @param args parameters given on command line
   * @throws IOException if io operation fails
   * @throws ParseException if parsing of something fails
   */
  public static void main(final String[] args) throws IOException, ParseException {
    int mandator = 0; // param
    int league = 0; // param
    String dbDriver = null; // param
    String dbDataBase = null; // param
    String dbUser = null; // param
    String dbPassword = null; // param
    boolean create = false; // param
    boolean update = false; // param

    try {
      for (int i = 0; i < args.length; i++) {
        if ("-mandator".equals(args[i])) {
          mandator = Integer.parseInt(args[++i]);
        } else if ("-league".equals(args[i])) {
          league = Integer.parseInt(args[++i]);
        } else if ("-dbdriver".equals(args[i])) {
          dbDriver = args[++i];
        } else if ("-dbdatabase".equals(args[i])) {
          dbDataBase = args[++i];
        } else if ("-dbuser".equals(args[i])) {
          dbUser = args[++i];
        } else if ("-dbpassword".equals(args[i])) {
          dbPassword = args[++i];
        } else if ("-create".equals(args[i])) {
          create = true;
        } else if ("-update".equals(args[i])) {
          update = true;
        }
      }

      if (dbDriver == null || dbDataBase == null //
          || dbUser == null || dbPassword == null //
          || mandator == 0 || league == 0 || !create && !update) {
        System.out.println(ScanBundesliga.HINT);
      } else {
        // connect to database
        Class.forName(dbDriver);
        try (final Connection thisDataBase = DriverManager.getConnection(dbDataBase, dbUser, dbPassword)) {
          final Map<String, Integer> teamNumberMap = //
              ScanBundesliga.readTeamMap(mandator, league, thisDataBase);

          final String baseUrl = ScanBundesliga.getUrlForSeasonLeague(league);
          if (create) {
            ScanBundesliga.createMatchdays(mandator, league, baseUrl, teamNumberMap, thisDataBase);
          } else if (update) {
            ScanBundesliga.updateMatchdays(mandator, league, baseUrl, teamNumberMap, thisDataBase);
          }
        } catch (final InterruptedException e) {
          e.printStackTrace();
        }
      }
    } catch (final SQLException se) {
      se.printStackTrace();
    } catch (final ClassNotFoundException e) {
      e.printStackTrace();
    }
  }

  /**
   * get url to scan for a given season league value.
   *
   * @param pseasonLeague keyfield which qualifies the season and league
   * @return url of the page to scan
   */
  public static String getUrlForSeasonLeague(final int pseasonLeague) {
    return ScanBundesliga.SEASON_LEAGUE_URL_MAP.get(Integer.valueOf(pseasonLeague));
  }

  /**
   * read teams as map.
   *
   * @param pmandator mandator
   * @param pleague league number
   * @param pdataBase database connection
   * @return map with teams
   * @throws SQLException if sql access fails
   */
  public static Map<String, Integer> readTeamMap( //
      final int pmandator, final int pleague, final Connection pdataBase) throws SQLException {
    final Map<String, Integer> teamMap = new HashMap<>();
    try (PreparedStatement readTeams = pdataBase.prepareStatement(ScanBundesliga.SQL_GET_TEAMS)) {
      readTeams.clearParameters();
      readTeams.setInt(1, pmandator);
      readTeams.setInt(2, pleague);
      try (final ResultSet resultTeams = readTeams.executeQuery()) {
        while (resultTeams.next()) {
          teamMap.put(resultTeams.getString("bezeichnung"), //
              Integer.valueOf(resultTeams.getInt("lfd_liga_ms")));
        }
      }
    }
    return teamMap;
  }

  /**
   * create a new matchday.
   *
   * @param pmandator mandator
   * @param pleague league number
   * @param purlPattern url pattern
   * @param pteamNumberMap team number map
   * @param pdataBase database connection
   * @throws IOException if io operation failes
   * @throws ParseException if parsing fails
   * @throws SQLException if sql operation fails
   * @throws InterruptedException if sleep is interrupted
   */
  public static void createMatchdays( //
      final int pmandator, final int pleague, final String purlPattern, //
      final Map<String, Integer> pteamNumberMap, //
      final Connection pdataBase) throws IOException, ParseException, SQLException, InterruptedException {
    try (final PreparedStatement insertMatch = pdataBase.prepareStatement(ScanBundesliga.SQL_INSERT_MATCH)) {
      for (int i = 1; i <= 34; i++) {
        List<MatchDay> matchesOnDay = null;
        while (matchesOnDay == null) {
          try {
            matchesOnDay = //
                ScanBundesliga.parseMatchDay(purlPattern, pteamNumberMap, i);
          } catch (final IOException e) {
            matchesOnDay = null;
            Thread.sleep(5000);
          }
        }
        for (final MatchDay matchDay : matchesOnDay) {
          insertMatch.clearParameters();
          int parm = 1;
          insertMatch.setInt(parm++, pmandator);
          insertMatch.setInt(parm++, pleague);
          insertMatch.setInt(parm++, i);
          insertMatch.setInt(parm++, matchDay.getMatchNumber());
          if (matchDay.getHomeTeam() == null) {
            insertMatch.setNull(parm++, 0);
          } else {
            insertMatch.setInt(parm++, matchDay.getHomeTeam().intValue());
          }
          if (matchDay.getOtherTeam() == null) {
            insertMatch.setNull(parm++, 0);
          } else {
            insertMatch.setInt(parm++, matchDay.getOtherTeam().intValue());
          }
          if (matchDay.getHomeGoals() == null) {
            insertMatch.setNull(parm++, 0);
          } else {
            insertMatch.setInt(parm++, matchDay.getHomeGoals().intValue());
          }
          if (matchDay.getOtherGoals() == null) {
            insertMatch.setNull(parm++, 0);
          } else {
            insertMatch.setInt(parm++, matchDay.getOtherGoals().intValue());
          }
          insertMatch.setTimestamp(parm++, new Timestamp(matchDay.getMatchStart().getTime()));
          insertMatch.setBoolean(parm++, matchDay.isFinished());
          insertMatch.executeUpdate();
        }
        // wait to not kill page
        Thread.sleep(2000);
      }
    }
  }

  /**
   * update existing match day.
   *
   * @param pmandator mandator
   * @param pleague league number
   * @param purlPattern url pattern
   * @param pteamNumberMap team number map
   * @param pdataBase database connection
   * @throws IOException if io operation failes
   * @throws ParseException if parsing fails
   * @throws SQLException if sql operation fails
   * @throws InterruptedException if sleep is interrupted
   */
  public static void updateMatchdays( //
      final int pmandator, final int pleague, final String purlPattern, //
      final Map<String, Integer> pteamNumberMap, //
      final Connection pdataBase) throws IOException, ParseException, SQLException, InterruptedException {
    try (final PreparedStatement updateMatch = pdataBase.prepareStatement(ScanBundesliga.SQL_UPDATE_MATCH)) {
      for (int i = 1; i <= 34; i++) {
        List<MatchDay> matchesOnDay = null;
        while (matchesOnDay == null) {
          try {
            matchesOnDay = //
                ScanBundesliga.parseMatchDay(purlPattern, pteamNumberMap, i);
          } catch (final IOException e) {
            matchesOnDay = null;
            Thread.sleep(5000);
          }
        }
        for (final MatchDay matchDay : matchesOnDay) {
          updateMatch.clearParameters();
          int parm = 1;
          if (matchDay.getHomeGoals() == null) {
            updateMatch.setNull(parm++, 0);
          } else {
            updateMatch.setInt(parm++, matchDay.getHomeGoals().intValue());
          }
          if (matchDay.getOtherGoals() == null) {
            updateMatch.setNull(parm++, 0);
          } else {
            updateMatch.setInt(parm++, matchDay.getOtherGoals().intValue());
          }
          updateMatch.setTimestamp(parm++, new Timestamp(matchDay.getMatchStart().getTime()));
          updateMatch.setBoolean(parm++, matchDay.isFinished());
          updateMatch.setInt(parm++, pmandator);
          updateMatch.setInt(parm++, pleague);
          updateMatch.setInt(parm++, i);
          if (matchDay.getHomeTeam() == null) {
            updateMatch.setNull(parm++, 0);
          } else {
            updateMatch.setInt(parm++, matchDay.getHomeTeam().intValue());
          }
          if (matchDay.getOtherTeam() == null) {
            updateMatch.setNull(parm++, 0);
          } else {
            updateMatch.setInt(parm++, matchDay.getOtherTeam().intValue());
          }
          updateMatch.executeUpdate();
        }
        // wait to not kill page
        Thread.sleep(2000);
      }
    }
  }

  private static List<MatchDay> parseMatchDay( //
      final String purlPattern, //
      final Map<String, Integer> pteamNumberMap, //
      final int pmatchday) throws IOException, ParseException {
    final SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.GERMANY);
    final String url = String.format(purlPattern, pmatchday);
    final Document doc = Jsoup.connect(url).get();
    final String intro = doc.select(".headline-small").html();
    final String matchGlobalStartString = intro.substring(intro.indexOf('(') + 1, intro.indexOf('(') + 11);
    final Date globalStart = formatter.parse(matchGlobalStartString + " 15:30");
    final Elements matchDay = doc.select(".table-match-comparison");
    final Elements matches = matchDay.get(0).getElementsByTag("tbody").get(0).getElementsByTag("tr");
    final List<MatchDay> matchesOnDay = new ArrayList<>(matches.size());
    int matchNumber = 0;
    for (final Element match : matches) {
      final String matchStartString = match.select(".column-date").html();
      final String[] matchStartTab = StringUtils.defaultString(matchStartString).split("<br>");
      final Date matchStart;
      if (matchStartTab.length == 3) {
        matchStart =
            formatter.parse(StringUtils.remove(matchStartTab[1] + " " + StringUtils.substring(matchStartTab[2], 0, 6), '\n'));
      } else {
        matchStart = globalStart;
      }

      final Elements matchTeams = match.select(".column-team-title");
      final String homeTeam =
          StringEscapeUtils.unescapeHtml4(StringUtils.trim(matchTeams.get(0).getElementsByTag("a").get(0).html()));
      final String homeTeamFixed;
      if (ScanBundesliga.TEAM_REPLACEMENTS.containsKey(homeTeam)) {
        homeTeamFixed = ScanBundesliga.TEAM_REPLACEMENTS.get(homeTeam);
      } else {
        homeTeamFixed = homeTeam;
      }
      final String otherTeam =
          StringEscapeUtils.unescapeHtml4(StringUtils.trim(matchTeams.get(1).getElementsByTag("a").get(0).html()));
      final String otherTeamFixed;
      if (ScanBundesliga.TEAM_REPLACEMENTS.containsKey(otherTeam)) {
        otherTeamFixed = ScanBundesliga.TEAM_REPLACEMENTS.get(otherTeam);
      } else {
        otherTeamFixed = otherTeam;
      }
      final String[] result = match.select(".column-score").get(0).getElementsByTag("a").get(0).html().split(" : ");
      Integer homeGoals = null;
      Integer otherGoals = null;
      if (result.length == 2 && StringUtils.isNumeric(StringUtils.trim(result[0]))
          && StringUtils.isNumeric(StringUtils.trim(result[1]))) {
        try {
          homeGoals = Integer.valueOf(Integer.parseInt(StringUtils.trim(result[0])));
          otherGoals = Integer.valueOf(Integer.parseInt(StringUtils.trim(result[1])));
        } catch (final Exception e) {
          homeGoals = null;
          otherGoals = null;
        }
      }

      matchesOnDay.add(new MatchDay(matchNumber, matchStart, pteamNumberMap.get(homeTeamFixed), //
          pteamNumberMap.get(otherTeamFixed), homeGoals, otherGoals, homeGoals != null));
      matchNumber++;
    }
    return matchesOnDay;
  }
}
