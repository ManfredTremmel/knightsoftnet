/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.knightsoftnet.bettingpool;

import de.knightsoft.knightsoftnet.common.KnConst;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * The <code>LigaSpieltagNavigation</code> class generates a html navigation pannel to navigate between the matchdays of one
 * season.
 *
 * @author Manfred Tremmel
 * @version 2.0.0, 06.08.2006
 */
public class LigaSpieltagNavigation {

  private final String serviceHtmlAlias;

  /**
   * Constructor.
   *
   */
  public LigaSpieltagNavigation() {
    serviceHtmlAlias = StringUtils.EMPTY;
  }

  /**
   * Constructor.
   *
   * @param pserviceHtmlAlias name of the virtual html page to linke too
   */
  public LigaSpieltagNavigation(final String pserviceHtmlAlias) {
    serviceHtmlAlias = pserviceHtmlAlias;
  }

  /**
   * The Method <code>HTML_LigaSpielNavigation</code> returns the navigation panel.
   *
   * @param res HTTP Servlet response
   * @param saisonLiga number of the liga
   * @param anzSpieltage number of the matchdays
   * @return html navigation panel
   */
  public String htmlLigaSpielNavigation(final HttpServletResponse res, final int saisonLiga, final int anzSpieltage) {
    return this.htmlLigaSpielNavigation(res, saisonLiga, anzSpieltage, null, null);
  }

  /**
   * The Method <code>HTML_LigaSpielNavigation</code> returns the navigation panel.
   *
   * @param res HTTP Servlet response
   * @param saisonLiga number of the liga
   * @param anzSpieltage number of the matchdays
   * @param session HTTP session data to encode URLs with
   * @return html navigation panel
   */
  public String htmlLigaSpielNavigation(final HttpServletResponse res, final int saisonLiga, final int anzSpieltage,
      final HttpSession session) {
    return this.htmlLigaSpielNavigation(res, saisonLiga, anzSpieltage, session, null);
  }

  /**
   * The Method <code>HTML_LigaSpielNavigation</code> returns the navigation panel.
   *
   * @param res HTTP Servlet response
   * @param saisonLiga number of the liga
   * @param anzSpieltage number of the matchdays
   * @param session HTTP session data to encode URLs with
   * @param pdfLink add a Link to a PDF version
   * @return html navigation panel
   */
  public String htmlLigaSpielNavigation(final HttpServletResponse res, final int saisonLiga, final int anzSpieltage,
      final HttpSession session, final String pdfLink) {
    final int anzSpieltageHalbe = anzSpieltage / 2;
    int pos;

    final StringBuilder formular = new StringBuilder(512);

    formular.append("        <hr>\n" + "        <table style=\"margin-left:auto; margin-right:auto;\" border=\"0\" "
        + "cellspacing=\"2\" cellpadding=\"0\">\n" + "            <tr>\n"
        + "                <th style=\"text-align:center;\" rowspan=\"2\">Spieltage:</th>\n");

    if (session == null) {
      for (pos = 1; pos <= anzSpieltageHalbe; pos++) {
        formular.append("                    <td><a class=\"liganav\" href=\"" + KnConst.HTML_BASE + "Tipprunde/" + saisonLiga
            + "/" + pos + "/" + serviceHtmlAlias + ".html\">" + pos + "</a></td>\n");
      }
      if (pdfLink != null) {
        formular.append("                <td style=\"text-align:center;\" rowspan=\"2\"><a class=\"nav\" href=\"" + pdfLink
            + "\"><img src=\"" + KnConst.GIF_URL + "32x32/pdf.png\" srcset=\"" + KnConst.GIF_URL
            + "64x64/pdf.png 2x\" width=\"32\" height=\"32\" alt=\"PDF Version\"></a></th>\n");

      }
      formular.append("            </tr>\n" + "            <tr>\n");
      for (pos = 1 + anzSpieltageHalbe; pos <= anzSpieltage; pos++) {
        formular.append("                <td><a class=\"liganav\" href=\"" + KnConst.HTML_BASE + "Tipprunde/" + saisonLiga + "/"
            + pos + "/" + serviceHtmlAlias + ".html\">" + pos + "</a></td>\n");
      }
    } else {
      for (pos = 1; pos <= anzSpieltageHalbe; pos++) {
        formular.append("                <td><a class=\"liganav\" href=\""
            + res.encodeURL(KnConst.HTML_BASE + "Tipprunde/" + saisonLiga + "/" + pos + "/" + serviceHtmlAlias + ".html")
            + "\">" + pos + "</a></td>\n");
      }
      if (pdfLink != null) {
        formular.append("                <td style=\"text-align:center;\" rowspan=\"2\"><a class=\"nav\" href=\""
            + res.encodeURL(pdfLink) + "\"><img src=\"" + KnConst.GIF_URL + "32x32/pdf.png\" srcset=\"" + KnConst.GIF_URL
            + "64x64/pdf.png 2x\" width=\"32\" height=\"32\" alt=\"PDF Version\"></a></th>\n");

      }
      formular.append("            </tr>\n" + "            <tr>\n");
      for (pos = 1 + anzSpieltageHalbe; pos <= anzSpieltage; pos++) {
        formular.append("                <td><a class=\"liganav\" href=\""
            + res.encodeURL(KnConst.HTML_BASE + "Tipprunde/" + saisonLiga + "/" + pos + "/" + serviceHtmlAlias + ".html")
            + "\">" + pos + "</a></td>\n");
      }
    }
    formular.append("            </tr>\n" + "        </table>\n" + "        <hr>\n");

    return formular.toString();
  }
}
