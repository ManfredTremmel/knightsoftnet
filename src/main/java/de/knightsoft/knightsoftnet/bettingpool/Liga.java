/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.knightsoftnet.bettingpool;

import de.knightsoft.common.TextException;
import de.knightsoft.knightsoftnet.common.KnConst;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * The <code>Liga</code> class is based on the abstract class de.knightsoft.common.AbstractVisualDb and implements the resulsts
 * and table statistic page of the KnightSoft Tipprunde.
 *
 *
 * @author Manfred Tremmel
 * @version 2.0.1, 29.04.2010
 */
public class Liga extends de.knightsoft.common.AbstractVisualDb {

  private static final String SPIELTAG_SQL = //
      "SELECT        MAX(spieltag) AS aktspieltag " //
          + "FROM        KnightSoft_TippSpiele " //
          + "WHERE        Mandator = ? " //
          + "  AND        saison_liga = ? " //
          + "  AND        tore_heim IS NOT NULL ";

  private final de.knightsoft.knightsoftnet.bettingpool.LigaSpieltagNavigation mySpielNavi;
  private final de.knightsoft.knightsoftnet.bettingpool.LigaSpieltag mySpieltag;
  private final de.knightsoft.knightsoftnet.bettingpool.LigaTabelle myTabelle;

  /**
   * Constructor.
   *
   * @param thisDatabase Connection to database
   * @param servletname Name of the Servlet
   * @exception SQLException if a database access error occurs
   * @exception TextException to give out a text message
   * @see TextException
   */
  public Liga(final Connection thisDatabase, final String servletname) throws SQLException, TextException {
    super(thisDatabase, KnConst.HTML_BASE, servletname, KnConst.GIF_URL + "16x16/ligastatistik.png", "Ligastatistik",
        "Ligastatistik", null, null, 0, null);

    mySpielNavi = new de.knightsoft.knightsoftnet.bettingpool.LigaSpieltagNavigation("Ligastatistik");
    mySpieltag = new de.knightsoft.knightsoftnet.bettingpool.LigaSpieltag(thisDatabase);
    myTabelle = new de.knightsoft.knightsoftnet.bettingpool.LigaTabelle(thisDatabase);
  }

  /**
   * The Method <code>ermittleSpieltag</code> finds out the current matchday.
   *
   * @param saisonLiga number of the liga
   * @param session session data
   * @return number of the matchday
   */
  private int ermittleSpieltag(final int saisonLiga, final HttpSession session) {
    int aktSpieltag = 1;
    final Integer mandatorInteger =
        (Integer) session.getAttribute(servletName + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_MANDATOR);
    final int mandator = mandatorInteger == null ? 1 : mandatorInteger.intValue();

    try {
      try (final PreparedStatement spieltagStatement = myDataBase.prepareStatement(Liga.SPIELTAG_SQL)) {
        spieltagStatement.clearParameters();
        spieltagStatement.setInt(1, mandator);
        spieltagStatement.setInt(2, saisonLiga);
        try (final ResultSet result = spieltagStatement.executeQuery()) {

          if (result.next()) {
            aktSpieltag = result.getInt("aktspieltag");
          }
        }
      }
    } catch (final java.sql.SQLException e) {
      e.printStackTrace();
    }

    if (aktSpieltag <= 0) {
      aktSpieltag = 1;
    }

    return aktSpieltag;
  }

  /**
   * The Method <code>doTheWork</code> coordinates all the work in this class
   *
   * @param req HttpServletRequest from the Servlet
   * @param res HttpServletResponse from the Servlet
   * @param session The Data of the current Session
   * @return html code of the formular
   * @exception de.knightsoft.common.TextException when a error in checking the passwords occours
   */
  @Override
  public String doTheWork(final HttpServletRequest req, final HttpServletResponse res, final HttpSession session)
      throws de.knightsoft.common.TextException {

    final String saisonLigaParm = req.getParameter("saison_liga");
    int saisonLigaInt = 0;
    final String spieltagParm = req.getParameter("spieltag");
    int spieltagInt = 0;
    String inhalt = null;
    int anzMannschaften = 0;
    String buffer = null;
    String ligabezeichnung = null;
    final Integer mandatorInteger =
        (Integer) session.getAttribute(servletName + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_MANDATOR);
    final int mandator = mandatorInteger == null ? 1 : mandatorInteger.intValue();

    // Liga angegeben
    if (saisonLigaParm != null) {
      saisonLigaInt = Integer.parseInt(saisonLigaParm);

      if (spieltagParm == null) {
        spieltagInt = ermittleSpieltag(saisonLigaInt, session);
      } else {
        spieltagInt = Integer.parseInt(spieltagParm);
      }
      buffer = myTabelle.htmlLigaTabelle(mandator, saisonLigaInt, spieltagInt);

      ligabezeichnung = myTabelle.getLigabezeichnung();
      anzMannschaften = myTabelle.getAnzMannschaften();
      session.setAttribute(servletName + "LigaStatistikTitle",
          "Ligastatistik - " + spieltagInt + ". Spieltag der \"" + ligabezeichnung + "\"");

      inhalt = mySpielNavi.htmlLigaSpielNavigation(res, saisonLigaInt, (anzMannschaften - 1) * 2, session)
          + mySpieltag.htmlLigaSpieltag(session, mandator, servletName, saisonLigaInt, spieltagInt) + buffer;

    }
    return inhalt;
  }

  /**
   * The Method <code>getTitle</code> gives back the Title of the application.
   *
   * @param session Data of the current session
   * @return Name of the application
   */
  @Override
  public String getTitle(final HttpSession session) {
    String title = (String) session.getAttribute(servletName + "LigaStatistikTitle");
    if (title == null) {
      title = serviceNameLocal;
    }
    return title;
  }

  /**
   * The Method <code>preventFromCache</code> tells you if this page should be cached or not.
   *
   * @param session Data of the current session
   * @return don't cache this site = true
   */
  @Override
  public boolean preventFromCache(final HttpSession session) {
    final Boolean finished = (Boolean) session.getAttribute(servletName + "LigaStatistikFinished");
    boolean finishedB = false;
    if (finished != null) {
      finishedB = finished.booleanValue();
    }
    return !finishedB;
  }

  /**
   * The Method <code>allowedToSee</code> tells you if the currently logged in user is allowed to access this application.
   *
   * @param session Data of the current session
   * @return true if it is allowed for this user
   */
  @Override
  public boolean allowedToSee(final HttpSession session) {
    return true;
  }

  /**
   * The Method <code>allowedToChange</code> tells you if the currently logged in user is allowed to change data in this
   * application.
   *
   * @param session Data of the current session
   * @return true if it is allowed for this user
   */
  @Override
  public boolean allowedToChange(final HttpSession session) {
    return true;
  }

}
