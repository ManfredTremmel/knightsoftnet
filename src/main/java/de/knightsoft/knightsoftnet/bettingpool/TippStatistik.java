/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.knightsoftnet.bettingpool;

import de.knightsoft.knightsoftnet.common.KnConst;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * The <code>TippStatistik</code> class is based on the abstract class de.knightsoft.common.AbstractVisualDb and implements the
 * results and table tipp statistic page of the KnightSoft Tipprunde. It's no longer a real servlet, only a part of one.
 *
 *
 * @author Manfred Tremmel
 * @version 2.0.1, 29.04.2010
 */
public class TippStatistik extends de.knightsoft.common.AbstractVisualDb {

  private static final String SPIELTAG_SQL = //
      "SELECT        MAX(spieltag) AS aktspieltag " //
          + "FROM        KnightSoft_TippSpiele " //
          + "WHERE        Mandator = ? " //
          + "  AND        saison_liga = ? " //
          + "  AND        tore_heim IS NOT NULL";
  private static final String NAVIGATION_SQL = //
      "SELECT        bezeichnung, anz_manschaften " //
          + "FROM        KnightSoft_TippLiga " //
          + "WHERE        Mandator = ? " //
          + "  AND        saison_liga = ? ";

  private final de.knightsoft.knightsoftnet.bettingpool.LigaSpieltagNavigation mySpielNavi;
  private final de.knightsoft.knightsoftnet.bettingpool.LigaTippStatistik myStatistik;

  private final de.knightsoft.knightsoftnet.bettingpool.LigaTippSchnittStatistik mySchnittStatistik;

  /**
   * Constructor.
   *
   * @param thisDatabase Connection to database
   * @param servletname Name of the Servlet
   * @exception SQLException if a database access error occurs
   * @exception de.knightsoft.common.TextException to give out a text message
   * @see de.knightsoft.common.TextException
   */
  public TippStatistik(final Connection thisDatabase, final String servletname)
      throws SQLException, de.knightsoft.common.TextException {
    super(thisDatabase, KnConst.HTML_BASE, servletname, KnConst.GIF_URL + "16x16/tippstatistik.png", "Tippstatistik",
        "Tippstatistik", null, null, 0, null);
    mySpielNavi = new de.knightsoft.knightsoftnet.bettingpool.LigaSpieltagNavigation("Tippstatistik");
    myStatistik = new de.knightsoft.knightsoftnet.bettingpool.LigaTippStatistik(thisDatabase, 10);

    mySchnittStatistik = new de.knightsoft.knightsoftnet.bettingpool.LigaTippSchnittStatistik(thisDatabase);
  }

  /**
   * The Method <code>ermittleSpieltag</code> finds out the current matchday.
   *
   * @param saisonLiga number of the liga
   * @param mandator mandator number
   * @return number of the matchday
   */
  private int ermittleSpieltag(final int saisonLiga, final int mandator) {
    int aktSpieltag = 1;

    try (final PreparedStatement spieltagStatement = myDataBase.prepareStatement(TippStatistik.SPIELTAG_SQL)) {
      spieltagStatement.clearParameters();
      spieltagStatement.setInt(1, mandator);
      spieltagStatement.setInt(2, saisonLiga);
      try (final ResultSet result = spieltagStatement.executeQuery()) {
        if (result.next()) {
          aktSpieltag = result.getInt("aktspieltag");
        }
      }
    } catch (final java.sql.SQLException e) {
      e.printStackTrace();
    }

    if (aktSpieltag <= 0) {
      aktSpieltag = 1;
    }

    return aktSpieltag;
  }

  /**
   * The Method <code>doTheWork</code> coordinates all the work in this class
   *
   * @param req HttpServletRequest from the Servlet
   * @param res HttpServletResponse from the Servlet
   * @param session The Data of the current Session
   * @return html code of the formular
   * @exception de.knightsoft.common.TextException when a error in checking the passwords occours
   */
  @Override
  public String doTheWork(final HttpServletRequest req, final HttpServletResponse res, final HttpSession session)
      throws de.knightsoft.common.TextException {

    final String saisonLigaParm = req.getParameter("saison_liga");
    int saisonLigaInt = 0;
    final String spieltagParm = req.getParameter("spieltag");
    int spieltagInt = 0;
    final StringBuilder inhalt = new StringBuilder();
    int anzMannschaften = 0;
    String buffer = null;
    String buffer2 = null;
    String ligaBezeichnung = null;
    String cookieUser = null;
    final Integer mandatorInteger =
        (Integer) session.getAttribute(servletName + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_MANDATOR);
    final int mandator = mandatorInteger == null ? 1 : mandatorInteger.intValue();

    // Liga angegeben
    if (saisonLigaParm != null) {
      saisonLigaInt = Integer.parseInt(saisonLigaParm);

      if (spieltagParm == null) {
        spieltagInt = ermittleSpieltag(saisonLigaInt, mandator);
      } else {
        spieltagInt = Integer.parseInt(spieltagParm);
      }

      try (final PreparedStatement navigationStatement = myDataBase.prepareStatement(TippStatistik.NAVIGATION_SQL)) {
        navigationStatement.clearParameters();
        navigationStatement.setInt(1, mandator);
        navigationStatement.setInt(2, saisonLigaInt);
        try (final ResultSet result = navigationStatement.executeQuery()) {
          if (result.next()) {
            ligaBezeichnung = result.getString("bezeichnung");
            anzMannschaften = result.getInt("anz_manschaften");
          }
        }
      } catch (final java.sql.SQLException e) {
        e.printStackTrace();
      }

      cookieUser = (String) session.getAttribute(servletName + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_USER
          + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_USER);

      session.setAttribute(servletName + "TippStatistikTitle",
          "Tippstatistik - " + spieltagInt + ". Spieltag der \"" + ligaBezeichnung + "\"");

      inhalt.append(mySpielNavi.htmlLigaSpielNavigation(res, saisonLigaInt, (anzMannschaften - 1) * 2, session));

      buffer2 = mySchnittStatistik.htmlLigaSpieltag(mandator, saisonLigaInt, spieltagInt);

      buffer = myStatistik.htmlLigaSpieltag(mandator, saisonLigaInt, spieltagInt, 1, cookieUser);

      if (buffer == null) {
        if (buffer2 == null) {
          inhalt.append("            <h3>Es liegen keine Statistiken vor</h3>\n");
        } else {
          inhalt.append(buffer2);
        }
      } else {
        if (buffer2 != null) {
          inhalt.append(buffer2);
        }

        inhalt.append(buffer);
        buffer = myStatistik.htmlLigaSpieltag(mandator, saisonLigaInt, spieltagInt, 0, cookieUser);
        if (buffer != null) {
          inhalt.append(buffer);
        }

        buffer = myStatistik.htmlLigaSpieltag(mandator, saisonLigaInt, spieltagInt, 2, cookieUser);
        if (buffer != null) {
          inhalt.append(buffer);
        }

        buffer = myStatistik.htmlLigaSpieltag(mandator, saisonLigaInt, spieltagInt, 3, cookieUser);
        if (buffer != null) {
          inhalt.append(buffer);
        }
      }
    }
    return inhalt.toString();
  }

  /**
   * The Method <code>getTitle</code> gives back the Title of the application.
   *
   * @param session Data of the current session
   * @return Name of the application
   */
  @Override
  public String getTitle(final HttpSession session) {
    String title = (String) session.getAttribute(servletName + "TippStatistikTitle");
    if (title == null) {
      title = serviceNameLocal;
    }
    return title;
  }

  /**
   * The Method <code>allowedToSee</code> tells you if the currently logged in user is allowed to access this application.
   *
   * @param session Data of the current session
   * @return true if it is allowed for this user
   */
  @Override
  public boolean allowedToSee(final HttpSession session) {
    return true;
  }

  /**
   * The Method <code>allowedToChange</code> tells you if the currently logged in user is allowed to change data in this
   * application.
   *
   * @param session Data of the current session
   * @return true if it is allowed for this user
   */
  @Override
  public boolean allowedToChange(final HttpSession session) {
    return true;
  }

}
