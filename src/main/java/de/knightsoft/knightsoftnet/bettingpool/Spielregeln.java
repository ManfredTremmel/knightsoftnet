/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.knightsoftnet.bettingpool;

import de.knightsoft.knightsoftnet.common.KnConst;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * The <code>Spielregeln</code> class is based on the abstract class de.knightsoft.common.AbstractVisualDb and implements the
 * Spielregeln page of the KnightSoft Tipprunde.
 *
 *
 * @author Manfred Tremmel
 * @version 2.0.0, 06.08.2006
 */
public class Spielregeln extends de.knightsoft.common.AbstractVisualDb {

  private static final String LIGEN_SQL = //
      "SELECT   CONCAT(CONCAT(TRUNCATE(saison_liga/100000,0),'/'), " //
          + "                TRUNCATE(saison_liga/100000,0) + 1) AS saison, " //
          + "         bezeichnung, " //
          + "         saison_liga, " //
          + "         tr_punkte_exakt, " //
          + "         tr_punkte_torver, " //
          + "         tr_punkte_toto, " //
          + "         tr_punkte_falsch, " //
          + "         (0 - TRUNCATE(saison_liga/100000,0)) AS sort_sl " //
          + "FROM     KnightSoft_TippLiga " //
          + "WHERE    Mandator = ? " //
          + "  AND    eintrag_gesperrt = ? " //
          + "ORDER BY sort_sl, saison_liga";

  /**
   * Constructor
   *
   * @param thisDatabase Connection to database
   * @param servletname Name of the Servlet
   * @exception SQLException if a database access error occurs
   * @exception de.knightsoft.common.TextException to give out a text message
   * @see de.knightsoft.common.TextException
   */
  public Spielregeln(final Connection thisDatabase, final String servletname)
      throws SQLException, de.knightsoft.common.TextException {
    super(thisDatabase, KnConst.HTML_BASE, servletname, KnConst.GIF_URL + "16x16/impressum.png", "Spielregeln",
        "Spielregeln für die KnightSoft-Net-Tipprunde");
  }

  /**
   * The Method <code>HTMLPage</code> generates the html formular.
   *
   * @param res HttpServletResponse from the Servlet
   * @param hint Hint to be displayed on the screen
   * @param session The Data of the current Session
   * @param navTyp Navigation type
   * @return HTML code of the formular
   */
  @Override
  protected String htmlPage(final HttpServletResponse res, final String hint, final HttpSession session, final String navTyp) {
    boolean farbig = false;
    final StringBuilder inhalt = new StringBuilder(4096);

    inhalt.append("        <ol>\n" + "            <li><div class=\"norm\">\n"
        + "                Die Teilnahme steht jedem offen\n" + "            </div></li>\n"
        + "            <li><div class=\"norm\">\n" + "                Zur Teilnahme m&uuml;ssen Sie sich &uuml;ber die "
        + "Registrierungsseite der jeweiligen Liga anmelden.\n" + "            </div></li>\n"
        + "            <li><div class=\"norm\">\n" + "                Tipps zu den einzelnen Spielen k&ouml;nnen bis zum "
        + "Spielgeginn im Mitspielerbereich der jeweiligen Liga abgegeben werden. "
        + "Die Pr&uuml;fung erfolgt bei der Aufbereitung des Eingabeformulars, als " + "auch beim Abschicken der Tipps.\n"
        + "            </div></li>\n" + "            <li><div class=\"norm\">\n"
        + "                Die Ergebnisse der eigenen Tipps k&ouml;nnen nach Eingabe "
        + "der Spielergebnisse durch KnightSoft im Mitspielerbereich der Liga des "
        + "jeweiligen Spieltags nachgelesen werden.\n" + "            </div></li>\n" + "            <li><div class=\"norm\">\n"
        + "                Die besten Tipp-Ergebnisse eines Spieltages,sowie &uuml;ber "
        + "die Saison hinweg, sind in der Tipp Statistik nachzulesen.\n" + "            </div></li>\n"
        + "            <li><div class=\"norm\">\n"
        + "                Die Spiel-Ergebnisse und die aktuelle Liga-Tabelle sind in " + "der Liga Statistik nachzulesen.\n"
        + "            </div></li>\n" + "            <li><div class=\"norm\">\n"
        + "                Die Punkte f&uuml;r die Tipps werden wie folgt vergeben:</div>\n");

    try (final PreparedStatement ligenStatement = myDataBase.prepareStatement(Spielregeln.LIGEN_SQL)) {
      final Integer mandatorInteger =
          (Integer) session.getAttribute(servletName + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_MANDATOR);
      inhalt.append("                <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n"
          + "                    <tr style=\"background-color:#5ea6ff;\">\n"
          + "                        <th><p class=\"shl\">Saison</p></th>\n"
          + "                        <th><p class=\"shl\">Ligabezeichnung</p></th>\n"
          + "                        <th><p class=\"shr\">E</p></th>\n"
          + "                        <th><p class=\"shr\">T</p></th>\n"
          + "                        <th><p class=\"shr\">P</p></th>\n"
          + "                        <th><p class=\"shr\">F</p></th>\n" + "                    </tr>\n");

      ligenStatement.clearParameters();
      ligenStatement.setInt(1, mandatorInteger == null ? 1 : mandatorInteger.intValue());
      ligenStatement.setBoolean(2, false);
      try (final ResultSet result = ligenStatement.executeQuery()) {
        while (result.next()) {
          if (farbig) {
            inhalt.append("                    <tr style=\"background-color:#5ea6ff;\">\n");
          } else {
            inhalt.append("                    <tr style=\"background-color:#aed2ff;\">\n");
          }
          farbig = !farbig;

          inhalt.append("                        <td><p class=\"spl\">" + result.getString("saison") + "</p></td>\n"
              + "                        <td><p class=\"spl\">"
              + de.knightsoft.common.StringToHtml.convert(result.getString("bezeichnung")) + "</p></td>\n"
              + "                        <td><p class=\"spr\">" + result.getString("tr_punkte_exakt") + "</p></td>\n"
              + "                        <td><p class=\"spr\">" + result.getString("tr_punkte_torver") + "</p></td>\n"
              + "                        <td><p class=\"spr\">" + result.getString("tr_punkte_toto") + "</p></td>\n"
              + "                        <td><p class=\"spr\">" + result.getString("tr_punkte_falsch") + "</p></td>\n"
              + "                    </tr>\n");
        }
      }

      inhalt.append("                </table>\n" + "                <div class=\"norm\"><b>Anmerkungen:</b><br>\n"
          + "                <b>E:</b> Exakter Tipp (z.B. Ergebnis 3:1 und Tipp 3:1)<br>\n"
          + "                <b>T:</b> Kein exakter Tipp, aber korrektes Torverh&auml;ltnis "
          + "(z.B. Ergebnis 3:1 und Tipp 2:0)<br>\n"
          + "                <b>P:</b> Das Ergebnis und das Torverh&auml;ltnis stimmen nicht "
          + "exakt, aber Sieg, Unentschieden bzw. Niederlage wurden korrekt getippt (z.B. " + "Ergebnis 3:1 und Tipp 1:0)<br>\n"
          + "                <b>F:</b> Komplett falscher Tipp, keines der oben genannten "
          + "Ereignisse trifft zu (z.B. Ergebnis 3:1 und Tipp 0:1)\n");
    } catch (final java.sql.SQLException e) {
      inhalt.append("<h2>Fehler beim Datenbankzugriff: " + e.toString() + "</h2>\n");
    }

    inhalt.append("            </div></li>\n" + "        </ol>\n");

    return inhalt.toString();
  }

  /**
   * The Method <code>preventFromCache</code> tells you if this page should be cached or not.
   *
   * @param session Data of the current session
   * @return don't cache this site = true
   */
  @Override
  public boolean preventFromCache(final HttpSession session) {
    return false;
  }

  /**
   * The Method <code>allowedToSee</code> tells you if the currently logged in user is allowed to access this application.
   *
   * @param session Data of the current session
   * @return true if it is allowed for this user
   */
  @Override
  public boolean allowedToSee(final HttpSession session) {
    return true;
  }

  /**
   * The Method <code>allowedToChange</code> tells you if the currently logged in user is allowed to change data in this
   * application.
   *
   * @param session Data of the current session
   * @return true if it is allowed for this user
   */
  @Override
  public boolean allowedToChange(final HttpSession session) {
    return true;
  }

}
