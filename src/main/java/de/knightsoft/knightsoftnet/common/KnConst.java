/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.knightsoftnet.common;

import org.apache.commons.lang3.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class KnConst extends de.knightsoft.common.Constants {
  public static final String HTML_BASE = "/";
  public static final String HTML_URL = KnConst.HTML_BASE;
  public static final String CSS_URL = KnConst.HTML_BASE + "css/";
  public static final String GIF_URL = KnConst.HTML_BASE + "pics/";
  public static final String SERVLET_URL = KnConst.HTML_BASE + "servlet/";

  public static final String HTML_BS_URL = KnConst.HTML_BASE + "Babysitter/";
  public static final String JAVA_SCRIPT_URL = KnConst.HTML_BS_URL + "js/";
  public static final String EMAIL_BS = "Babysitter@" + KnConst.COMPANY + "-net.de";
  public static final String SESSION_BS = "KSBS_Session";
  public static final String SESSION_NAVI_BS = "KSBS_Navigation";

  public static final String HTML_NH_URL = KnConst.HTML_BASE + "Nachhilfe/";
  public static final String JAVA_SCRIPT_NH_URL = KnConst.HTML_NH_URL + "js/";
  public static final String EMAIL_NH = "Nachhilfe@" + KnConst.COMPANY + "-net.de";
  public static final String SESSION_NH = "KSNH_Session";
  public static final String SESSION_NAVI_NH = "KSNH_Navigation";

  public static final String HTML_FG_URL = KnConst.HTML_BASE + "Fahrgemeinschaft/";
  public static final String JAVA_SCRIPT_FG_URL = KnConst.HTML_FG_URL + "js/";
  public static final String EMAIL_FG = "Fahrgemeinschaft@" + KnConst.COMPANY + "-net.de";
  public static final String SESSION_FG = "KSFG_Session";
  public static final String SESSION_NAVI_FG = "KSFG_Navigation";

  public static final String HTML_FT_URL = KnConst.HTML_BASE + "Tipprunde/";
  public static final String JAVA_SCRIPT_FT_URL = KnConst.HTML_FT_URL + "js/";
  public static final String EMAIL_FT = "Fussballtipp@" + KnConst.COMPANY + "-net.de";
  public static final String SESSION_USER_FT = "KSFTUser_";
  public static final String SESSION_COUNT_FT = "KSFTCount_";
  public static final String SESSION_FT = "KSFT_Session";
  public static final String SESSION_DATA_FT = "KSFT_SessionData";
  public static final String SESSION_STUFE_FT = "KSFT_Stufe";
  public static final String SESSION_NAVI_FT = "KSFT_Navigation";

  // private static final String WBild_welt_weit = GIF_URL + "WWW_468.gif";
  // private static final String WURL_welt_weit =
  // "http://www.Welt-Weit-Weg.de/kunden/redirect.cgi?partner=knight";

  private static final String WBILD_PERF_ORD = null;
  private static final String WURL_PERF_ORD =
      "<!-- BEGIN PARTNER PROGRAM - DO NOT CHANGE THE PARAMETERS OF THE HYPERLINK -->\n<A HREF=\"http://partners.webmasterplan.com/click.asp?ref=4166&amp;site=235&amp;type=b1\">\n<IMG SRC=\"http://www.sunrose.de/grafik/banner.gif\" BORDER=\"0\" ALT=\"Perfekte Ordnung - leicht gemacht!\" WIDTH=\"468\" HEIGHT=\"60\"></A><br>\n<!-- END PARTNER PROGRAM -->";

  private static final String WBILD_LINK_ORD = null; // www.linkstation.de Bannertauschprogramm
  private static final String WURL_LINK_ORD =
      "<!-- BEGIN LINKSTATION - PRO-BANNER -->\n<A HREF=\"http://www.linkstation.de/cgi-bin/click/L08764AD\">\n<IMG SRC=\"http://www.linkstation.de/cgi-bin/show/L08764AD\" ISMAP BORDER=1 WIDTH=468 HEIGHT=60 ALT=\"Linkstation-PRO-Ads\"></A>\n<!-- END LINKSTATION - PRO-BANNER -->\n";

  // private static final String WBild_BU_VERS = null; // BU Versicherungen
  // private static final String WURL_BU_VERS =
  // "<!-- BEGIN PARTNER PROGRAM - DO NOT CHANGE THE PARAMETERS OF THE HYPERLINK -->\n <A
  // HREF=\"http://partners.webmasterplan.com/click.asp?ref=4250&amp;site=379&amp;type=b2\"> <IMG
  // SRC=\"http://mitglied.tripod.de/pmaruegen/banner/bu_bann5.gif\" BORDER=\"0\"
  // ALT=\"BU-Versicherung ohne
  // Gesundheitsprüfung\" WIDTH=\"468\" HEIGHT=\"60\"></A>\n <!-- END PARTNER PROGRAM -->";

  // private static final String WBild_BU_VERS2 = null; // BU Versicherungen
  // private static final String WURL_BU_VERS2 =
  // "<!-- BEGIN PARTNER PROGRAM - DO NOT CHANGE THE PARAMETERS OF THE HYPERLINK -->\n <A
  // HREF=\"http://partners.webmasterplan.com/click.asp?ref=4166&amp;site=379&amp;type=b2\"> <IMG
  // SRC=\"http://mitglied.tripod.de/pmaruegen/banner/bu_bann5.gif\" BORDER=\"0\"
  // ALT=\"BU-Versicherung ohne
  // Gesundheitsprüfung\" WIDTH=\"468\" HEIGHT=\"60\"></A>\n <!-- END PARTNER PROGRAM -->";

  public static final String WBILD = null;
  public static final String WURL =
      "<!-- BEGIN LINKSTATION - PRO-BANNER -->\n<A HREF=\"http://www.linkstation.de/cgi-bin/click/L08764XY\">\n"
          + "<IMG SRC=\"http://www.linkstation.de/cgi-bin/show/L08764XY\" ISMAP BORDER=1 WIDTH=468 HEIGHT=60 ALT=\"Linkstation-PRO-Ads\"></A>\n<!-- END LINKSTATION - PRO-BANNER -->\n";

  public static final String[] WBILD_RANDOM =
      {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null};
  public static final String[] WURL_RANDOM = {
      "<A HREF=\"http://tracker.tradedoubler.com/tracker.asp?code=249762&amp;pid=3&amp;"
          + "url=http://www.auktionen.de\"><IMG SRC=\"http://ads.auktionen.de/"
          + "de_primusauktionprivat_13_anim_156x60_mot3.gif\" width=\"156\" "
          + "height=\"60\" alt=\"Geld sparen bei Auktionen.de! Wenn Sie hochwertige "
          + "Produkte zu Scnäppchenpreisen suchen sind Sie hier genau richtig!\"></A>\n",
      "<A HREF=\"http://tracker.tradedoubler.com/tracker.asp?code=249782&amp;pid=3\">"
          + "<IMG SRC=\"http://ads.powershopping.de/100x70_hell_5.gif\" width=\"100\" "
          + "height=\"70\" alt=\"www.primuspowershopping.de\"></A>\n",
      "<A HREF=\"http://tracker.tradedoubler.com/tracker.asp?code=410082&amp;pid=34&amp;"
          + "url=http://www.flipside.de/?theme=defaults&amp;partner=tradedoubler\">"
          + "<IMG SRC=\"http://www.flipside.de/commun/articles_picts/de/affiliate/creatives/"
          + "flipside_vaio.gif\" border=\"0\" width=\"468\" height=\"60\" alt=\"Kostenlose "
          + "Onlinespiele und attraktive Preise warten auf Dich! Jetzt bei Flipside.de spielen " + "und gewinnen.\"></A>\n",
      "<A HREF=\"http://tracker.tradedoubler.com/tracker.asp?code=249761&amp;pid=16&amp;"
          + "url=http://www.primusauktion.de\"><IMG SRC=\"http://ads.primus-auktion.de/"
          + "n02_468x60_02.gif\" width=\"468\" height=\"60\" alt=\"www.primusauktion.de\"></A>\n",
      "<A HREF=\"http://tracker.tradedoubler.com/tracker.asp?code=410082&amp;pid=38&amp;"
          + "url=http://www.flipside.de/?theme=defaults&amp;partner=tradedoubler\">"
          + "<IMG SRC=\"http://www.flipside.de/commun/articles_picts/de/affiliate/creatives/"
          + "ostern001.gif\" border=\"0\" width=\"468\" height=\"60\" alt=\"Kostenlose "
          + "Onlinespiele und attraktive Preise warten auf Dich! Jetzt bei Flipside.de " + "spielen und gewinnen.\"></A>\n"};

  public static final String WBILD_GEWINN = null;
  public static final String WURL_GEWINN =
      "<!-- BEGIN LINKSTATION - PRO-BANNER -->\n<A HREF=\"http://www.linkstation.de/cgi-bin/click/L08764XX\">\n<IMG SRC=\"http://www.linkstation.de/cgi-bin/show/L08764XX\" ISMAP BORDER=1 WIDTH=468 HEIGHT=60 ALT=\"Linkstation-PRO-Ads\"></A>\n<!-- END LINKSTATION - PRO-BANNER -->\n";

  public static final String WBILD_GEWINN_B = KnConst.WBILD_LINK_ORD; // Werbebanner für
  public static final String WURL_GEWINN_B = KnConst.WURL_LINK_ORD; // das Gewinnspiel -
                                                                    // Bestätigungsseite

  public static final String WBILD_BABY_REG = null;
  public static final String WURL_BABY_REG =
      "<!-- BEGIN LINKSTATION - PRO-BANNER -->\n<A HREF=\"http://www.linkstation.de/cgi-bin/click/L08764BD\">\n<IMG SRC=\"http://www.linkstation.de/cgi-bin/show/L08764BD\" ISMAP BORDER=1 WIDTH=468 HEIGHT=60 ALT=\"Linkstation-PRO-Ads\"></A>\n<!-- END LINKSTATION - PRO-BANNER -->\n";
  public static final String WBILD_BABY_CHANGE = null;
  public static final String WURL_BABY_CHANGE = KnConst.WURL_BABY_REG;
  public static final String WBILD_BABY_SEARCH = null;
  public static final String WURL_BABY_SEARCH =
      "<!-- BEGIN LINKSTATION - PRO-BANNER -->\n<A HREF=\"http://www.linkstation.de/cgi-bin/click/L08764BO\">\n<IMG SRC=\"http://www.linkstation.de/cgi-bin/show/L08764BO\" ISMAP BORDER=1 WIDTH=468 HEIGHT=60 ALT=\"Linkstation-PRO-Ads\"></A>\n<!-- END LINKSTATION - PRO-BANNER -->\n";

  public static final String WBILD_NACH_REG = null;
  public static final String WURL_NACH_REG = KnConst.WURL_PERF_ORD;
  public static final String WBILD_NACH_CHANGE = null;
  public static final String WURL_NACH_CHANGE =
      "<!-- BEGIN LINKSTATION - PRO-BANNER -->\n<A HREF=\"http://www.linkstation.de/cgi-bin/click/L08764CA\">\n<IMG SRC=\"http://www.linkstation.de/cgi-bin/show/L08764CA\" ISMAP BORDER=1 WIDTH=468 HEIGHT=60 ALT=\"Linkstation-PRO-Ads\"></A>\n<!-- END LINKSTATION - PRO-BANNER -->\n";
  public static final String WBILD_NACH_SEARCH = KnConst.WBILD_PERF_ORD;
  public static final String WURL_NACH_SEARCH = KnConst.WURL_PERF_ORD;

  public static final String WBILD_FAHR_REG = null;
  public static final String WURL_FAHR_REG =
      "<!-- BEGIN LINKSTATION - PRO-BANNER -->\n<A HREF=\"http://www.linkstation.de/cgi-bin/click/L08764DA\">\n<IMG SRC=\"http://www.linkstation.de/cgi-bin/show/L08764DA\" ISMAP BORDER=1 WIDTH=468 HEIGHT=60 ALT=\"Linkstation-PRO-Ads\"></A>\n<!-- END LINKSTATION - PRO-BANNER -->\n";
  public static final String WBILD_FAHR_CHANGE = null;
  public static final String WURL_FAHR_CHANGE = KnConst.WURL_FAHR_REG;
  public static final String WBILD_FAHR_SEARCH = null;
  public static final String WURL_FAHR_SEARCH =
      "<A HREF=\"http://tracker.tradedoubler.com/tracker.asp?code=249761&amp;pid=16&amp;url=http://www.primusauktion.de\"><IMG SRC=\"http://ads.primus-auktion.de/n02_468x60_02.gif\" width=\"468\" height=\"60\" alt=\"www.primusauktion.de\"></A>\n";

  public static final String WBILD_TIPP_REG = null;
  public static final String WURL_TIPP_REG =
      "<!-- BEGIN LINKSTATION - PRO-BANNER -->\n<A HREF=\"http://www.linkstation.de/cgi-bin/click/L08764AO\">\n<IMG SRC=\"http://www.linkstation.de/cgi-bin/show/L08764AO\" ISMAP BORDER=1 WIDTH=468 HEIGHT=60 ALT=\"Linkstation-PRO-Ads\"></A>\n<!-- END LINKSTATION - PRO-BANNER -->\n";
  public static final String WBILD_TIPP_CHANGE = null;
  public static final String WURL_TIPP_CHANGE =
      "<!-- BEGIN LINKSTATION - PRO-BANNER -->\n<A HREF=\"http://www.linkstation.de/cgi-bin/click/L08764AP\">\n<IMG SRC=\"http://www.linkstation.de/cgi-bin/show/L08764AP\" ISMAP BORDER=1 WIDTH=468 HEIGHT=60 ALT=\"Linkstation-PRO-Ads\"></A>\n<!-- END LINKSTATION - PRO-BANNER -->\n";
  public static final String WBILD_TIPP_SEARCH = null;
  public static final String WURL_TIPP_SEARCH =
      "<!-- BEGIN LINKSTATION - PRO-BANNER -->\n<A HREF=\"http://www.linkstation.de/cgi-bin/click/L08764AQ\">\n<IMG SRC=\"http://www.linkstation.de/cgi-bin/show/L08764AQ\" ISMAP BORDER=1 WIDTH=468 HEIGHT=60 ALT=\"Linkstation-PRO-Ads\"></A>\n<!-- END LINKSTATION - PRO-BANNER -->\n";

  public static final String HINWEIS_REGISTER =
      "<div style=\"text-align:center;\"><h2>Beachten Sie bitte unsere Bedingungen f&uuml;r "
          + "die Aufnahme in unsere Datenbank</h2></div>\n" //
          + "<ul>\n" //
          + "<li><div class=\"norm\">" //
          + KnConst.ORGANISATION //
          + " tritt nur als Vermittler auf, es kann nicht garantiert werden, dass sich " //
          + "Interessenten entsprechend Ihrer Vorstellung bei Ihnen melden.</div></li>\n" //
          + "<li><div class=\"norm\">Vertr&auml;ge kommen ausschliesslich zwischen Ihnen " //
          + "und der suchenden Person zustande, f&uuml;r Sch&auml;den, die im Rahmen durch die " //
          + "Vermittlung entstehen, schliesse ich jede Haftung aus.</div></li>\n" //
          + "<li><div class=\"norm\">In Folge Ihrer Eintragung erhalten Sie per E-Mail ein " //
          + "Passwort zugesandt, um falsche Eintragungen zu vermeiden, bleibt Ihr Eintrag " //
          + "gesperrt, bis Sie sich einmal mit diesem Passwort angemeldet (Login-Knopf) " //
          + "haben.</div></li>\n" //
          + "<li><div class=\"norm\">Ich behalte mir vor, Ihren Eintrag zu l&ouml;schen, " //
          + "wenn rechtliche Verst&ouml;sse, vermehrte Beschwerden, oder andere schwerwiegende " //
          + "Gr&uuml;nde bekannt werden.</div></li>\n" //
          + "</ul>";

  public static final String BABY_SITTER_FARBE = "#f9657d";
  public static final String BABY_SITTER_TEXT = "Babysitter-<br>Vermittlung";

  public static final String FAHRGEMEINSCHAFT_FARBE = "#f60b32";
  public static final String FAHRGEMEINSCHAFT_TEXT = "Mitfahr-<br>Vermittlung";

  public static final String NACHHILFE_FARBE = "#000000";
  public static final String NACHHILFE_TEXT = "Nachhilfe-<br>Vermittlung";

  public static final String TIPPRUNDE_FARBE = "#15a800";
  public static final String TIPPRUNDE_TEXT = "Fu&szlig;ball-<br>Tipprunde";

  /**
   * generate start of the page.
   *
   * @param servletname name of the servlet
   * @param seitentitel title of the page
   * @param zusatzdaten additional data
   * @param bild picture
   * @param url url
   * @param ptitle title text
   * @param htmlVerzeichnis html directory
   * @param jsVerzeichnis js directory
   * @param buttonName name of the button
   * @param nocache don't cache the page
   * @return html text
   */
  public static final String htmlStart(final String servletname, final String seitentitel, final String zusatzdaten,
      final String bild, final String url, final String ptitle, final String htmlVerzeichnis, final String jsVerzeichnis,
      final String buttonName, final boolean nocache) {
    final Date today = new Date();
    final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss z", Locale.GERMANY);

    final StringBuilder htmlStartString = new StringBuilder(2048);

    htmlStartString.append("<!DOCTYPE html>\n" //
        + "<html><!-- " + servletname + " -->\n" + "    <head>\n"
        + "        <meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\">\n"
        + "        <meta http-equiv=\"Content-Style-Type\" content=\"text/css\">\n"
        + "        <meta http-equiv=\"Content-Script-Type\" content=\"text/javascript\">\n"
        + "        <meta http-equiv=\"content-language\" content=\"de\">\n");
    if (nocache) {
      htmlStartString.append("        <meta http-equiv=\"expires\" content=\"0\">\n"
          + "        <meta http-equiv=\"pragma\" content=\"no-cache\">\n"
          + "        <meta name=\"robots\" content=\"noindex\">\n");
    }
    htmlStartString.append("        <meta name=\"MSSmartTagsPreventParsing\" content=\"TRUE\">\n"
        + "        <meta name=\"description\" content=\"" + seitentitel + "\">\n" + "        <meta name=\"date\" content=\""
        + formatter.format(today) + "\">\n" + "        <!-- " + de.knightsoft.common.Constants.COPYRIGHT + " -->\n"
        + "        <title>" + seitentitel + "</title>\n" + "        <link rev=\"made\" href=\"mailto:"
        + de.knightsoft.common.Constants.EMAIL + "\">\n" + "        <link rel=\"stylesheet\" type=\"text/css\" href=\""
        + KnConst.CSS_URL + "default.css\">\n" + "        <link rel=\"start\" href=\"/index.html\">\n");

    if (jsVerzeichnis != null) {
      htmlStartString.append("        <script src=\"" + jsVerzeichnis + "umleit.js\" type=\"text/javascript\"></script>\n"
          + "        <script src=\"" + jsVerzeichnis + "buttons.js\" type=\"text/javascript\"></script>\n");
    }

    if (zusatzdaten != null) {
      htmlStartString.append(zusatzdaten);
    }

    htmlStartString.append("    </head>\n" + "    <body onload=\"javascript:AktiviereSeite('" + buttonName + "');\">\n");

    // Ohne Vorgaben, Zufalls-Werbebanner ermitteln
    // if (URL == null ||
    // URL.equals("") ||
    // URL.equals(""))
    // {
    // byte pos = (byte)((java.lang.Math.random() * (WURL_random.length - 1)));
    // URL = WURL_random[pos];
    // Bild = WBild_random[pos];
    // }

    String title = ptitle;
    if (StringUtils.isEmpty(title)) {
      title = de.knightsoft.common.Constants.ORGANISATION;
    }

    // HTMLStartString.append( " <table width=\"100%\">\n" +
    // " <tr>\n" +
    // " <td rowspan=\"2\">&nbsp;</td>\n" +
    // " <td style=\"width:468px;\"><p class=\"anzeige\">Anzeige</p></td>\n" +
    // " <td rowspan=\"2\">&nbsp;</td>\n" +
    // " </tr>\n" +
    // " <tr>\n" +
    // " <td>\n");

    // if (URL.substring(0,1).equals("<"))
    // HTMLStartString.append( " " + URL + "\n");
    // else
    // HTMLStartString.append( " <a href=\"" + URL + "\"><img src=\"" + Bild +
    // "\" border=\"0\" ALT=\"Banner\" width=\"468\" height=\"60\"></a>\n");
    htmlStartString.append( // " </td>\n" +
        // " </tr>\n" +
        // " </table>\n" +
        "        <table width=\"100%\">\n" + "            <tr>\n" + "                <td class=\"ueb\">" + title + "</td>\n"
            + "            </tr>\n" + "        </table>\n");

    return htmlStartString.toString();
  }

  /**
   * create register mail.
   *
   * @param name name of the user
   * @param geschlecht sex of the user
   * @param user user name
   * @param passwort password to send
   * @param service service name
   * @return string email text
   */
  public static final String registerEmail(final String name, final String geschlecht, final String user, final String passwort,
      final String service) {
    final StringBuilder eMailText = new StringBuilder(1024);

    if ("W".equals(geschlecht)) {
      eMailText.append("Sehr geehrte Frau ");
    } else {
      eMailText.append("Sehr geehrter Herr ");
    }

    eMailText.append(name);
    eMailText.append(",\n\nvielen Dank für die Eintragung bei meiner kostenlosen\n");
    eMailText.append(service);
    eMailText.append("-Vermittlung.\nUm Ihre Daten zu ändern, oder Ihre Eintragung zu löschen,\n"
        + "verwenden Sie bitte folgendes Passwort:\n\n");
    eMailText.append(passwort);
    eMailText.append("\n\nAchten Sie auf korrekte Klein- und Grossschreibung bei\n" + "Eingabe des Passworts.\n\n");
    eMailText
        .append("Sie können den Eintrag auch dirket durch Aufruf der nach-\n" + "folgenden URL (anklicken) freischalten:\n\n");
    eMailText.append("http://www." + de.knightsoft.common.Constants.COMPANY + "-net.de" + KnConst.SERVLET_URL
        + "de.KnightSoft.KnightSoftNet." + service + ".ChangeServlet?Stufe=1&User=" + user + "&Password=" + passwort + "\n\n");
    eMailText.append("Bitte beachten Sie auch, dass Ihre Eintragung gesperrt\n"
        + "bleibt, bis Sie sich einmal erfolgreich angemeldet haben.\n"
        + "Einträge, die nach einem Monat noch nicht freigeschalten\n" + "wurden, werden von mir automatisch gelöscht.\n");
    eMailText.append("Bei Fragen oder Problemen stehe ich Ihnen jederzeit mit\n" + "Rat und Tat zur Seite.\n\n");
    eMailText.append("Mit freundlichen Grüssen,\n\nManfred Tremmel");

    return eMailText.toString();
  }

  /**
   * create mail with new password.
   *
   * @param name name of the user
   * @param geschlecht sex of the user
   * @param passwort password to send
   * @param service service name
   * @return string email text
   */
  public static final String neuesPasswortEmail(final String name, final String geschlecht, final String passwort,
      final String service) {
    final StringBuilder eMailText = new StringBuilder(512);

    if ("W".equals(geschlecht)) {
      eMailText.append("Sehr geehrte Frau ");
    } else {
      eMailText.append("Sehr geehrter Herr ");
    }

    eMailText.append(name);
    eMailText.append(",\n\nSie haben ein neues Passwort für unseren ");
    eMailText.append(service);
    eMailText.append("-\nService angefordert, es lautet:\n\n");
    eMailText.append(passwort);
    eMailText.append("\n\nAchten Sie auf korrekte Klein- und Grossschreibung bei\n"
        + "Eingabe des Passworts, Ihr altes Passwort ist nicht mehr\ngültig!\n");
    eMailText.append("Bei Fragen oder Problemen stehe ich Ihnen jederzeit mit\n" + "Rat und Tat zur Seite.\n\n");
    eMailText.append("Mit freundlichen Grüssen,\n\nManfred Tremmel");

    return eMailText.toString();
  }
}
