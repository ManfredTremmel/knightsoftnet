/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.knightsoftnet.common;

import de.knightsoft.common.StringToHtml;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.text.NumberFormat;

public class DbStatistik {
  // Versenden Statistik an die KnightSoft-Leutchen
  static final String htmlDbStatistic(final Connection myDataBase, final String datumGestern, final int panzGeloeschtBs,
      final int anzGeloeschtNh, final int anzGeloeschtFg) throws java.sql.SQLException, IOException {
    de.knightsoft.common.Browserdetection myBrowserdetection;
    final NumberFormat formZahl = NumberFormat.getInstance(java.util.Locale.GERMANY);
    final NumberFormat formProzent = NumberFormat.getPercentInstance(java.util.Locale.GERMANY);

    int anzGeloeschtBs = panzGeloeschtBs;
    final String datum_gestern_formatiert =
        datumGestern.substring(8, 10) + "." + datumGestern.substring(5, 7) + "." + datumGestern.substring(0, 4);
    final StringBuilder htmlText = new StringBuilder(5500);
    final StringBuilder gewinnText1 = new StringBuilder(512);
    final StringBuilder gewinnText2 = new StringBuilder(512);
    final StringBuilder gewinnText3 = new StringBuilder(512);
    final StringBuilder bannerText1 = new StringBuilder(1024);
    final StringBuilder bannerText2 = new StringBuilder(1024);
    long anzEintraegeBs = 0L;
    long anzEintraegeNh = 0L;
    long anzEintraegeFg = 0L;
    long anzEintraegeTr = 0L;
    long anzEintraegeGs = 0L;
    long anzEintraegeWbs = 0L;
    long anzEintraegeWnh = 0L;
    long anzEintraegeWfg = 0L;
    long anzEintraegeWtr = 0L;
    long anzEintraegeWgs = 0L;
    long anzEintraegeHeuteBs = 0L;
    long anzEintraegeHeuteNh = 0L;
    long anzEintraegeHeuteFg = 0L;
    long anzEintraegeHeuteTr = 0L;
    long anzEintraegeHeuteGs = 0L;
    long anzEintraegeFreiBs = 0L;
    long anzEintraegeFreiNh = 0L;
    long anzEintraegeFreiFg = 0L;
    long anzEintraegeFreiTr = 0L;
    long anzEintraegeHeuteFreiBs = 0L;
    long anzEintraegeHeuteFreiNh = 0L;
    long anzEintraegeHeuteFreiFg = 0L;
    long anzEintraegeHeuteFreiTr = 0L;
    long anzGewinnspielerBs = 0L;
    long anzGewinnspielerNh = 0L;
    long anzGewinnspielerFg = 0L;
    long anzDoppeltEintraegeBs = 0L;
    long anzDoppeltEintraegeNh = 0L;
    long anzDoppeltEintraegeFg = 0L;
    long anzDoppeltEintraegeTr = 0L;
    long anzEinzEintraegeGs = 0L;
    long anzBannerklicks = 0L;
    long anzBannerklicksWin = 0L;
    long anzBannerklicksMac = 0L;
    long anzBannerklicksOs2 = 0L;
    long anzBannerklicksUnix = 0L;
    long anzBannerklicksVms = 0L;
    long anzBannerklicksAmiga = 0L;
    long anzBannerklicksOs = 0L;
    long anzBannerklicksNetscape = 0L;
    long anzBannerklicksIe = 0L;
    long anzBannerklicksAol = 0L;
    long anzBannerklicksOpera = 0L;
    long anzBannerklicksKonqueror = 0L;
    long anzBannerklicksBrowser = 0L;

    boolean fullPage = true;

    if (anzGeloeschtBs < 0) {
      fullPage = false;
      anzGeloeschtBs = 0;
    }

    try (final ResultSet myresult = myDataBase.createStatement()
        .executeQuery("SELECT        COUNT(*) AS anz_eintr, " + "            SUM(if (eintrag_gesperrt='N',1,0)) AS anz_frei, "
            + "            SUM(if (datum_erstellt='" + datumGestern + "' ,1,0)) AS eintr_heute, "
            + "            SUM(if (datum_geaendert='" + datumGestern + "' AND eintrag_gesperrt='N',1,0)) AS geaendert_heute, "
            + "            SUM(if (geschlecht='W',1,0)) AS anz_weibl " + "FROM        KnightSoft_BabySitter "
            + "WHERE        datum_erstellt<='" + datumGestern + "'")) {
      if (myresult.next()) {
        anzEintraegeBs = myresult.getLong("anz_eintr");
        anzEintraegeFreiBs = myresult.getLong("anz_frei");
        anzEintraegeHeuteBs = myresult.getLong("eintr_heute");
        anzEintraegeHeuteFreiBs = myresult.getLong("geaendert_heute");
        anzEintraegeWbs = myresult.getLong("anz_weibl");
      }
    }

    try (final ResultSet myresult = myDataBase.createStatement()
        .executeQuery("SELECT        COUNT(*) AS anz_eintr, " + "            SUM(if (eintrag_gesperrt='N',1,0)) AS anz_frei, "
            + "            SUM(if (datum_erstellt='" + datumGestern + "' ,1,0)) AS eintr_heute, "
            + "            SUM(if (datum_geaendert='" + datumGestern + "' AND eintrag_gesperrt='N',1,0)) AS geaendert_heute, "
            + "            SUM(if (geschlecht='W',1,0)) AS anz_weibl " + "FROM        KnightSoft_Nachhilfe "
            + "WHERE        datum_erstellt<='" + datumGestern + "'")) {
      if (myresult.next()) {
        anzEintraegeNh = myresult.getLong("anz_eintr");
        anzEintraegeFreiNh = myresult.getLong("anz_frei");
        anzEintraegeHeuteNh = myresult.getLong("eintr_heute");
        anzEintraegeHeuteFreiNh = myresult.getLong("geaendert_heute");
        anzEintraegeWnh = myresult.getLong("anz_weibl");
      }
    }

    try (final ResultSet myresult = myDataBase.createStatement()
        .executeQuery("SELECT        COUNT(*) AS anz_eintr, " + "            SUM(if (eintrag_gesperrt='N',1,0)) AS anz_frei, "
            + "            SUM(if (datum_erstellt='" + datumGestern + "' ,1,0)) AS eintr_heute, "
            + "            SUM(if (datum_geaendert='" + datumGestern + "' AND eintrag_gesperrt='N',1,0)) AS geaendert_heute, "
            + "            SUM(if (geschlecht='W',1,0)) AS anz_weibl " + "FROM        KnightSoft_Fahrgemeinschaft "
            + "WHERE        datum_erstellt<='" + datumGestern + "'")) {
      if (myresult.next()) {
        anzEintraegeFg = myresult.getLong("anz_eintr");
        anzEintraegeFreiFg = myresult.getLong("anz_frei");
        anzEintraegeHeuteFg = myresult.getLong("eintr_heute");
        anzEintraegeHeuteFreiFg = myresult.getLong("geaendert_heute");
        anzEintraegeWfg = myresult.getLong("anz_weibl");
      }
    }

    try (final ResultSet myresult = myDataBase.createStatement()
        .executeQuery("SELECT        COUNT(*) AS anz_eintr, " + "            COUNT(*) AS anz_frei, "
            + "            SUM(if (datum_erstellt='" + datumGestern + "' ,1,0)) AS eintr_heute, "
            + "            SUM(if (datum_geaendert='" + datumGestern + "' AND eintrag_gesperrt='N',1,0)) AS geaendert_heute, "
            + "            SUM(if (geschlecht='W',1,0)) AS anz_weibl " + "FROM        KnightSoft_TippMitspieler "
            + "WHERE        datum_erstellt<='" + datumGestern + "'")) {
      if (myresult.next()) {
        anzEintraegeTr = myresult.getLong("anz_eintr");
        anzEintraegeFreiTr = myresult.getLong("anz_frei");
        anzEintraegeHeuteTr = myresult.getLong("eintr_heute");
        anzEintraegeHeuteFreiTr = myresult.getLong("geaendert_heute");
        anzEintraegeWtr = myresult.getLong("anz_weibl");
      }
    }

    try (final ResultSet myresult = myDataBase.createStatement()
        .executeQuery("SELECT        COUNT(DB1.name) AS mehrfachmalzwei " + "FROM        KnightSoft_BabySitter AS DB1, "
            + "            KnightSoft_BabySitter AS DB2 " + "WHERE        DB1.zaehler!=DB2.zaehler AND "
            + "            DB1.datum_erstellt<='" + datumGestern + "' AND " + "            DB2.datum_erstellt<='" + datumGestern
            + "' AND " + "            DB1.name=DB2.name AND " + "            DB1.vorname=DB2.vorname AND "
            + "            DB1.email=DB2.email")) {
      if (myresult.next()) {
        anzDoppeltEintraegeBs = myresult.getLong("mehrfachmalzwei") / 2;
      }
    }

    try (final ResultSet myresult = myDataBase.createStatement()
        .executeQuery("SELECT        COUNT(DB1.name) AS mehrfachmalzwei " + "FROM        KnightSoft_Nachhilfe AS DB1, "
            + "            KnightSoft_Nachhilfe AS DB2 " + "WHERE        DB1.zaehler!=DB2.zaehler AND "
            + "            DB1.datum_erstellt<='" + datumGestern + "' AND " + "            DB2.datum_erstellt<='" + datumGestern
            + "' AND " + "            DB1.name=DB2.name AND " + "            DB1.vorname=DB2.vorname AND "
            + "            DB1.email=DB2.email")) {
      if (myresult.next()) {
        anzDoppeltEintraegeNh = myresult.getLong("mehrfachmalzwei") / 2;
      }
    }

    try (final ResultSet myresult = myDataBase.createStatement()
        .executeQuery("SELECT        COUNT(DB1.name) AS mehrfachmalzwei " + "FROM        KnightSoft_Fahrgemeinschaft AS DB1, "
            + "            KnightSoft_Fahrgemeinschaft AS DB2 " + "WHERE        DB1.zaehler!=DB2.zaehler AND "
            + "            DB1.datum_erstellt<='" + datumGestern + "' AND " + "            DB2.datum_erstellt<='" + datumGestern
            + "' AND " + "            DB1.name=DB2.name AND " + "            DB1.vorname=DB2.vorname AND "
            + "            DB1.email=DB2.email")) {
      if (myresult.next()) {
        anzDoppeltEintraegeFg = myresult.getLong("mehrfachmalzwei") / 2;
      }
    }

    try (final ResultSet myresult = myDataBase.createStatement()
        .executeQuery("SELECT        COUNT(DB1.name) AS mehrfachmalzwei " + "FROM        KnightSoft_TippMitspieler AS DB1, "
            + "            KnightSoft_TippMitspieler AS DB2 " + "WHERE        DB1.Mandator=DB2.Mandator AND "
            + "            DB1.spitzname!=DB2.spitzname AND " + "            DB1.datum_erstellt<='" + datumGestern + "' AND "
            + "            DB2.datum_erstellt<='" + datumGestern + "' AND " + "            DB1.name=DB2.name AND "
            + "            DB1.vorname=DB2.vorname AND " + "            DB1.email=DB2.email")) {
      if (myresult.next()) {
        anzDoppeltEintraegeTr = myresult.getLong("mehrfachmalzwei") / 2;
      }
    }

    long anzGewinnspielerA = 0L;
    long anzGewinnspielerFa = 0L;
    try (final ResultSet myresult = myDataBase.createStatement()
        .executeQuery("SELECT sende_infos FROM KnightSoft_GewinnspielTeilnehmer WHERE datum_erstellt<='" + datumGestern
            + "' GROUP BY name, vorname, email")) {
      while (myresult.next()) {
        anzGewinnspielerA++;
        if (myresult.getString("sende_infos").equals("J")) {
          anzGewinnspielerFa++;
        }
      }
    }

    int anzGewinnspiele = 0;
    try (final ResultSet myresult = myDataBase.createStatement().executeQuery("SELECT COUNT(*) FROM KnightSoft_Gewinnspiel")) {
      myresult.next();
      anzGewinnspiele = myresult.getInt("COUNT(*)");
    }
    for (int pos = 1; pos <= anzGewinnspiele; pos++) {
      try (final ResultSet myresult = myDataBase.createStatement()
          .executeQuery("SELECT COUNT(*) FROM KnightSoft_GewinnspielTeilnehmer WHERE gewinnspiel_nr=" + pos
              + " AND datum_erstellt<='" + datumGestern + "'")) {
        myresult.next();
        anzEintraegeGs = myresult.getLong("COUNT(*)");
      }

      try (final ResultSet myresult = myDataBase.createStatement()
          .executeQuery("SELECT COUNT(*) FROM KnightSoft_GewinnspielTeilnehmer WHERE gewinnspiel_nr=" + pos
              + " AND geschlecht='W' AND datum_erstellt<='" + datumGestern + "'")) {
        myresult.next();
        anzEintraegeWgs = myresult.getLong("COUNT(*)");
      }

      try (final ResultSet myresult = myDataBase.createStatement()
          .executeQuery("SELECT COUNT(*) FROM KnightSoft_GewinnspielTeilnehmer WHERE gewinnspiel_nr=" + pos
              + " AND datum_erstellt='" + datumGestern + "'")) {
        myresult.next();
        anzEintraegeHeuteGs = myresult.getLong("COUNT(*)");
      }

      try (final ResultSet myresult = myDataBase.createStatement()
          .executeQuery("SELECT COUNT(*) FROM KnightSoft_GewinnspielTeilnehmer WHERE gewinnspiel_nr=" + pos
              + " AND datum_erstellt<='" + datumGestern + "' AND system='BabySitter'")) {
        myresult.next();
        anzGewinnspielerBs = myresult.getLong("COUNT(*)");
      }

      try (final ResultSet myresult = myDataBase.createStatement()
          .executeQuery("SELECT COUNT(*) FROM KnightSoft_GewinnspielTeilnehmer WHERE gewinnspiel_nr=" + pos
              + " AND datum_erstellt<='" + datumGestern + "' AND system='Nachhilfe'")) {
        myresult.next();
        anzGewinnspielerNh = myresult.getLong("COUNT(*)");
      }

      try (final ResultSet myresult = myDataBase.createStatement()
          .executeQuery("SELECT COUNT(*) FROM KnightSoft_GewinnspielTeilnehmer " //
              + "WHERE gewinnspiel_nr=" + pos //
              + " AND datum_erstellt<='" + datumGestern + "' " //
              + " AND system='Fahrgemeinschaften'")) {
        myresult.next();
        anzGewinnspielerFg = myresult.getLong("COUNT(*)");
      }

      try (final ResultSet myresult =
          myDataBase.createStatement().executeQuery("SELECT zaehler FROM KnightSoft_GewinnspielTeilnehmer WHERE gewinnspiel_nr="
              + pos + " AND datum_erstellt<='" + datumGestern + "' GROUP BY name, vorname, email")) {
        anzEinzEintraegeGs = 0L;
        while (myresult.next()) {
          anzEinzEintraegeGs++;
        }
      }

      gewinnText1.append("            <tr>\n" + "                <td style=\"background-color:#FFFF00;\">Gewinnspiel "
          + StringToHtml.convert(formZahl.format(pos)) + "</td>\n" + "                <td style=\"text-align:right;\">"
          + StringToHtml.convert(formZahl.format(anzEintraegeGs)) + "</td>\n" + "                <td>&nbsp;</td>\n"
          + "                <td>&nbsp;</td>\n" + "                <td style=\"text-align:right;\">"
          + StringToHtml.convert(formZahl.format(anzEintraegeHeuteGs)) + "</td>\n" + "                <td>&nbsp;</td>\n"
          + "                <td>&nbsp;</td>\n" + "                <td style=\"text-align:right;\">"
          + StringToHtml.convert(formZahl.format(anzEintraegeGs - anzEinzEintraegeGs)) + "</td>\n" + "            </tr>\n");
      if (anzEintraegeGs > 0L) {
        gewinnText2.append(
            "            <tr>\n" + "                <td style=\"background-color:#FFFF00;\">Gewinnspiel " + pos + "</td>\n"
                + "                <td style=\"text-align:right;\">" + StringToHtml.convert(formZahl.format(anzEintraegeWgs))
                + "</td>\n" + "                <td style=\"text-align:right;\">"
                + StringToHtml.convert(formProzent.format((double) anzEintraegeWgs / (double) anzEintraegeGs)) + "</td>\n"
                + "                <td style=\"text-align:right;\">"
                + StringToHtml.convert(formZahl.format(anzEintraegeGs - anzEintraegeWgs)) + "</td>\n"
                + "                <td style=\"text-align:right;\">"
                + StringToHtml
                    .convert(formProzent.format((double) (anzEintraegeGs - anzEintraegeWgs) / (double) anzEintraegeGs))
                + "</td>\n" + "            </tr>\n");

        gewinnText3.append(
            "            <tr>\n" + "                <td style=\"background-color:#FFFF00;\">Gewinnspiel " + pos + "</td>\n"
                + "                <td style=\"text-align:right;\">" + StringToHtml.convert(formZahl.format(anzGewinnspielerBs))
                + "</td>\n" + "                <td style=\"text-align:right;\">"
                + StringToHtml.convert(formProzent.format((double) anzGewinnspielerBs / (double) anzEintraegeGs)) + "</td>\n"
                + "                <td style=\"text-align:right;\">" + StringToHtml.convert(formZahl.format(anzGewinnspielerNh))
                + "</td>\n" + "                <td style=\"text-align:right;\">"
                + StringToHtml.convert(formProzent.format((double) anzGewinnspielerNh / (double) anzEintraegeGs)) + "</td>\n"
                + "                <td style=\"text-align:right;\">" + StringToHtml.convert(formZahl.format(anzGewinnspielerFg))
                + "</td>\n" + "                <td style=\"text-align:right;\">"
                + StringToHtml.convert(formProzent.format((double) anzGewinnspielerFg / (double) anzEintraegeGs)) + "</td>\n"
                + "            </tr>\n");
      }
    }

    if (fullPage) {
      htmlText.append("<!DOCTYPE html>\n" //
          + "<html>\n" + "    <head>\n" + "        <meta http-equiv=\"content-type\" content=\"text/html; "
          + "charset=UTF-8\">\n" + "        <meta name=\"MSSmartTagsPreventParsing\" content=\"TRUE\">\n"
          + "        <title>Statistik vom " + datum_gestern_formatiert + "</title>\n" + "    </head>\n"
          + "    <body style=\"background: white; color: black\">\n");
    }
    htmlText.append("        <div style=\"text-align:center;\"><h2>KnightSoft-DB Statistik</h2>\n" + "        <p>\n"
        + "        <div style=\"text-align:center;\">\n"
        + "        <table style=\"margin-left:auto; margin-right:auto;\" border=\"3\">\n"
        + "            <tr style=\"background-color:#FFFF00;\">\n" + "                <th valign=\"TOP\">DB-Eintragungen</th>\n"
        + "                <th>eingetragen<br>gesamt</th>\n" + "                <th colspan=\"2\">frei<br>gesamt</th>\n"
        + "                <th>eingetragen<br>" + datum_gestern_formatiert + "</th>\n"
        + "                <th>frei/ge&auml;ndert<br>" + datum_gestern_formatiert + "</th>\n"
        + "                <th>gel&ouml;scht<br>" + datum_gestern_formatiert + "</th>\n"
        + "                <th>mehrfache<br>Eintragungen</th>\n" + "            </tr>\n" + "            <tr>\n"
        + "                <td style=\"background-color:#FFFF00;\">Babysitter</td>\n"
        + "                <td style=\"text-align:right;\">" + StringToHtml.convert(formZahl.format(anzEintraegeBs)) + "</td>\n"
        + "                <td style=\"text-align:right;\">" + StringToHtml.convert(formZahl.format(anzEintraegeFreiBs))
        + "</td>\n");
    if (anzEintraegeBs == 0L) {
      htmlText.append("                <td>&nbsp;</td>\n");
    } else {
      htmlText.append("                <td style=\"text-align:right;\">"
          + StringToHtml.convert(formProzent.format((double) anzEintraegeFreiBs / (double) anzEintraegeBs)) + "</td>\n");
    }
    htmlText.append("                <td style=\"text-align:right;\">"
        + StringToHtml.convert(formZahl.format(anzEintraegeHeuteBs)) + "</td>\n"
        + "                <td style=\"text-align:right;\">" + StringToHtml.convert(formZahl.format(anzEintraegeHeuteFreiBs))
        + "</td>\n" + "                <td style=\"text-align:right;\">" + StringToHtml.convert(formZahl.format(anzGeloeschtBs))
        + "</td>\n" + "                <td style=\"text-align:right;\">"
        + StringToHtml.convert(formZahl.format(anzDoppeltEintraegeBs)) + "</td>\n" + "            </tr>\n"
        + "            <tr>\n" + "                <td style=\"background-color:#FFFF00;\">Nachhilfe</td>\n"
        + "                <td style=\"text-align:right;\">" + StringToHtml.convert(formZahl.format(anzEintraegeNh)) + "</td>\n"
        + "                <td style=\"text-align:right;\">" + StringToHtml.convert(formZahl.format(anzEintraegeFreiNh))
        + "</td>\n");
    if (anzEintraegeNh == 0L) {
      htmlText.append("                <td>&nbsp;</td>\n");
    } else {
      htmlText.append("                <td style=\"text-align:right;\">"
          + StringToHtml.convert(formProzent.format((double) anzEintraegeFreiNh / (double) anzEintraegeNh)) + "</td>\n");
    }
    htmlText.append("                <td style=\"text-align:right;\">"
        + StringToHtml.convert(formZahl.format(anzEintraegeHeuteNh)) + "</td>\n"
        + "                <td style=\"text-align:right;\">" + StringToHtml.convert(formZahl.format(anzEintraegeHeuteFreiNh))
        + "</td>\n" + "                <td style=\"text-align:right;\">" + StringToHtml.convert(formZahl.format(anzGeloeschtNh))
        + "</td>\n" + "                <td style=\"text-align:right;\">"
        + StringToHtml.convert(formZahl.format(anzDoppeltEintraegeNh)) + "</td>\n" + "            </tr>\n"
        + "            <tr>\n" + "                <td style=\"background-color:#FFFF00;\">Fahrgemeinschaften</td>\n"
        + "                <td style=\"text-align:right;\">" + StringToHtml.convert(formZahl.format(anzEintraegeFg)) + "</td>\n"
        + "                <td style=\"text-align:right;\">" + StringToHtml.convert(formZahl.format(anzEintraegeFreiFg))
        + "</td>\n");
    if (anzEintraegeFg == 0L) {
      htmlText.append("                <td>&nbsp;</td>\n");
    } else {
      htmlText.append("                <td style=\"text-align:right;\">"
          + StringToHtml.convert(formProzent.format((double) anzEintraegeFreiFg / (double) anzEintraegeFg)) + "</td>\n");
    }
    htmlText.append("                <td style=\"text-align:right;\">"
        + StringToHtml.convert(formZahl.format(anzEintraegeHeuteFg)) + "</td>\n"
        + "                <td style=\"text-align:right;\">" + StringToHtml.convert(formZahl.format(anzEintraegeHeuteFreiFg))
        + "</td>\n" + "                <td style=\"text-align:right;\">" + StringToHtml.convert(formZahl.format(anzGeloeschtFg))
        + "</td>\n" + "                <td style=\"text-align:right;\">"
        + StringToHtml.convert(formZahl.format(anzDoppeltEintraegeFg)) + "</td>\n" + "            </tr>\n");
    // 02.08.2000: Tipprunde neu
    htmlText.append("            <tr>\n" + "                <td style=\"background-color:#FFFF00;\">Tipprunde</td>\n"
        + "                <td style=\"text-align:right;\">" + StringToHtml.convert(formZahl.format(anzEintraegeTr)) + "</td>\n"
        + "                <td style=\"text-align:right;\">" + StringToHtml.convert(formZahl.format(anzEintraegeFreiTr))
        + "</td>\n");
    if (anzEintraegeTr == 0L) {
      htmlText.append("                <td>&nbsp;</td>\n");
    } else {
      htmlText.append("                <td style=\"text-align:right;\">"
          + StringToHtml.convert(formProzent.format((double) anzEintraegeFreiTr / (double) anzEintraegeTr)) + "</td>\n");
    }
    htmlText.append("                <td style=\"text-align:right;\">"
        + StringToHtml.convert(formZahl.format(anzEintraegeHeuteTr)) + "</td>\n"
        + "                <td style=\"text-align:right;\">" + StringToHtml.convert(formZahl.format(anzEintraegeHeuteFreiTr))
        + "</td>\n" + "                <td style=\"text-align:right;\">&nbsp;</td>\n"
        + "                <td style=\"text-align:right;\">" + StringToHtml.convert(formZahl.format(anzDoppeltEintraegeTr))
        + "</td>\n" + "            </tr>\n");
    if (anzGewinnspielerA > 0L) {
      htmlText
          .append("            <tr>\n" + "                <td style=\"background-color:#FFFF00;\">Gewinnspieladressen</td>\n"
              + "                <td style=\"text-align:right;\">" + StringToHtml.convert(formZahl.format(anzGewinnspielerA))
              + "</td>\n" + "                <td style=\"text-align:right;\">"
              + StringToHtml.convert(formZahl.format(anzGewinnspielerFa)) + "</td>\n"
              + "                <td style=\"text-align:right;\">"
              + StringToHtml.convert(formProzent.format((double) anzGewinnspielerFa / (double) anzGewinnspielerA)) + "</td>\n"
              + "                <td>&nbsp;</td>\n" + "                <td>&nbsp;</td>\n" + "                <td>&nbsp;</td>\n"
              + "                <td>&nbsp;</td>\n" + "            </tr>\n");
    }

    htmlText.append(gewinnText1.toString());
    htmlText.append("        </table>\n" + "        </div>\n" + "        </p>\n" + "        <hr>\n" + "        <p>\n"
        + "        <div style=\"text-align:center;\">\n"
        + "        <table style=\"margin-left:auto; margin-right:auto;\" border=\"3\">\n"
        + "            <tr style=\"background-color:#FFFF00;\">\n" + "                <th>Geschlechterverteilung</th>\n"
        + "                <th colspan=\"2\">weiblich</th>\n" + "                <th colspan=\"2\">m&auml;nnlich</th>\n"
        + "            </tr>\n");
    if (anzEintraegeBs > 0L) {
      htmlText.append("            <tr>\n" + "                <td style=\"background-color:#FFFF00;\">Babysitter</td>\n"
          + "                <td style=\"text-align:right;\">" + StringToHtml.convert(formZahl.format(anzEintraegeWbs))
          + "</td>\n" + "                <td style=\"text-align:right;\">"
          + StringToHtml.convert(formProzent.format((double) anzEintraegeWbs / (double) anzEintraegeBs)) + "</td>\n"
          + "                <td style=\"text-align:right;\">"
          + StringToHtml.convert(formZahl.format(anzEintraegeBs - anzEintraegeWbs)) + "</td>\n"
          + "                <td style=\"text-align:right;\">"
          + StringToHtml.convert(formProzent.format((double) (anzEintraegeBs - anzEintraegeWbs) / (double) anzEintraegeBs))
          + "</td>\n" + "            </tr>\n");
    }
    if (anzEintraegeNh > 0L) {
      htmlText.append("            <tr>\n" + "                <td style=\"background-color:#FFFF00;\">Nachhilfe</td>\n"
          + "                <td style=\"text-align:right;\">" + StringToHtml.convert(formZahl.format(anzEintraegeWnh))
          + "</td>\n" + "                <td style=\"text-align:right;\">"
          + StringToHtml.convert(formProzent.format((double) anzEintraegeWnh / (double) anzEintraegeNh)) + "</td>\n"
          + "                <td style=\"text-align:right;\">"
          + StringToHtml.convert(formZahl.format(anzEintraegeNh - anzEintraegeWnh)) + "</td>\n"
          + "                <td style=\"text-align:right;\">"
          + StringToHtml.convert(formProzent.format((double) (anzEintraegeNh - anzEintraegeWnh) / (double) anzEintraegeNh))
          + "</td>\n" + "            </tr>\n");
    }
    if (anzEintraegeFg > 0L) {
      htmlText.append("            <tr>\n" + "                <td style=\"background-color:#FFFF00;\">Fahrgemeinschaften</td>\n"
          + "                <td style=\"text-align:right;\">" + StringToHtml.convert(formZahl.format(anzEintraegeWfg))
          + "</td>\n" + "                <td style=\"text-align:right;\">"
          + StringToHtml.convert(formProzent.format((double) anzEintraegeWfg / (double) anzEintraegeFg)) + "</td>\n"
          + "                <td style=\"text-align:right;\">"
          + StringToHtml.convert(formZahl.format(anzEintraegeFg - anzEintraegeWfg)) + "</td>\n"
          + "                <td style=\"text-align:right;\">"
          + StringToHtml.convert(formProzent.format((anzEintraegeFg - (double) anzEintraegeWfg) / anzEintraegeFg)) + "</td>\n"
          + "            </tr>\n");
    }
    // 02.08.2000: Tipprunde
    if (anzEintraegeTr > 0L) {
      htmlText.append("            <tr>\n" + "                <td style=\"background-color:#FFFF00;\">Tipprunde</td>\n"
          + "                <td style=\"text-align:right;\">" + StringToHtml.convert(formZahl.format(anzEintraegeWtr))
          + "</td>\n" + "                <td style=\"text-align:right;\">"
          + StringToHtml.convert(formProzent.format((double) anzEintraegeWtr / (double) anzEintraegeTr)) + "</td>\n"
          + "                <td style=\"text-align:right;\">"
          + StringToHtml.convert(formZahl.format(anzEintraegeTr - anzEintraegeWtr)) + "</td>\n"
          + "                <td style=\"text-align:right;\">"
          + StringToHtml.convert(formProzent.format((double) (anzEintraegeTr - anzEintraegeWtr) / (double) anzEintraegeTr))
          + "</td>\n" + "            </tr>\n");
    }
    htmlText.append(gewinnText2);
    htmlText.append("        </table>\n" + "        </div>\n" + "        </p>\n");
    if (!gewinnText3.toString().equals(StringUtils.EMPTY)) {
      htmlText.append("        <hr>\n" + "        <p>\n" + "        <div style=\"text-align:center;\">\n"
          + "        <table style=\"margin-left:auto; margin-right:auto;\" border=\"3\">\n"
          + "            <tr style=\"background-color:#FFFF00;\">\n" + "                <th>Verteilung Gewinnspieler</th>\n"
          + "                <th colspan=\"2\">Babysitter</th>\n" + "                <th colspan=\"2\">Nachhilfen</th>\n"
          + "                <th colspan=\"2\">Fahrgemeinschaften</th>\n" + "            </tr>\n");
      htmlText.append(gewinnText3);
      htmlText.append("        </table>\n" + "        </div>\n" + "        </p>\n");
    }

    // 05.02.2001: Komplett neu
    long anzBannerpartner = 0;
    try (final ResultSet myresult = myDataBase.createStatement().executeQuery("SELECT COUNT(*) " //
        + "FROM KnightSoft_Bannerpartner")) {
      myresult.next();
      anzBannerpartner = myresult.getLong("COUNT(*)");
    }
    for (int pos = 0; pos <= anzBannerpartner; pos++) {
      try (final ResultSet myresult =
          myDataBase.createStatement().executeQuery("SELECT useragent FROM KnightSoft_Bannerklicks where bannerpartner=" + pos
              + " AND datum_zeit<='" + datumGestern + " 23:59:59'")) {
        while (myresult.next()) {
          myBrowserdetection = new de.knightsoft.common.Browserdetection(myresult.getString("useragent"));

          anzBannerklicks++;

          if (myBrowserdetection.isNav) {
            anzBannerklicksNetscape++;
          } else if (myBrowserdetection.isIe) {
            anzBannerklicksIe++;
          } else if (myBrowserdetection.isAol) {
            anzBannerklicksAol++;
          } else if (myBrowserdetection.isOpera) {
            anzBannerklicksOpera++;
          } else if (myBrowserdetection.isKonq) {
            anzBannerklicksKonqueror++;
          } else {
            anzBannerklicksBrowser++;
          }

          if (myBrowserdetection.isWin) {
            anzBannerklicksWin++;
          } else if (myBrowserdetection.isOS2) {
            anzBannerklicksOs2++;
          } else if (myBrowserdetection.isMac) {
            anzBannerklicksMac++;
          } else if (myBrowserdetection.isUnix) {
            anzBannerklicksUnix++;
          } else if (myBrowserdetection.isVms) {
            anzBannerklicksVms++;
          } else if (myBrowserdetection.isAmiga) {
            anzBannerklicksAmiga++;
          } else {
            anzBannerklicksOs++;
          }
        }
      }

      if (anzBannerklicks > 0) {
        bannerText1.append("            <tr>\n");
        if (pos == 0) {
          bannerText1.append("                <td style=\"background-color:#FFFF00;\">" + "Nicht zuordenbare Klicks</td>\n");
        } else {
          bannerText1.append("                <td style=\"background-color:#FFFF00;\">Klicks Partner " + pos + "</td>\n");
        }
        bannerText1.append("                <td style=\"text-align:right;\">"
            + StringToHtml.convert(formZahl.format(anzBannerklicks)) + "</td>\n"
            + "                <td style=\"text-align:right;\">" + StringToHtml.convert(formZahl.format(anzBannerklicksWin))
            + "</td>\n" + "                <td style=\"text-align:right;\">"
            + StringToHtml.convert(formProzent.format((double) anzBannerklicksWin / (double) anzBannerklicks)) + "</td>\n"
            + "                <td style=\"text-align:right;\">" + StringToHtml.convert(formZahl.format(anzBannerklicksOs2))
            + "</td>\n" + "                <td style=\"text-align:right;\">"
            + StringToHtml.convert(formProzent.format((double) anzBannerklicksOs2 / (double) anzBannerklicks)) + "</td>\n"
            + "                <td style=\"text-align:right;\">" + StringToHtml.convert(formZahl.format(anzBannerklicksMac))
            + "</td>\n" + "                <td style=\"text-align:right;\">"
            + StringToHtml.convert(formProzent.format((double) anzBannerklicksMac / (double) anzBannerklicks)) + "</td>\n"
            + "                <td style=\"text-align:right;\">" + StringToHtml.convert(formZahl.format(anzBannerklicksUnix))
            + "</td>\n" + "                <td style=\"text-align:right;\">"
            + StringToHtml.convert(formProzent.format((double) anzBannerklicksUnix / (double) anzBannerklicks)) + "</td>\n"
            + "                <td style=\"text-align:right;\">" + StringToHtml.convert(formZahl.format(anzBannerklicksVms))
            + "</td>\n" + "                <td style=\"text-align:right;\">"
            + StringToHtml.convert(formProzent.format((double) anzBannerklicksVms / (double) anzBannerklicks)) + "</td>\n"
            + "                <td style=\"text-align:right;\">" + StringToHtml.convert(formZahl.format(anzBannerklicksAmiga))
            + "</td>\n" + "                <td style=\"text-align:right;\">"
            + StringToHtml.convert(formProzent.format((double) anzBannerklicksAmiga / (double) anzBannerklicks)) + "</td>\n"
            + "                <td style=\"text-align:right;\">" + StringToHtml.convert(formZahl.format(anzBannerklicksOs))
            + "</td>\n" + "                <td style=\"text-align:right;\">"
            + StringToHtml.convert(formProzent.format((double) anzBannerklicksOs / (double) anzBannerklicks)) + "</td>\n"
            + "            </tr>\n");

        bannerText2.append("            <tr>\n");
        if (pos == 0) {
          bannerText2.append("                <td style=\"background-color:#FFFF00;\">" + "Nicht zuordenbare Klicks</td>\n");
        } else {
          bannerText2.append("                <td style=\"background-color:#FFFF00;\">Klicks Partner " + pos + "</td>\n");
        }
        bannerText2.append("                <td style=\"text-align:right;\">"
            + StringToHtml.convert(formZahl.format(anzBannerklicks)) + "</td>\n"
            + "                <td style=\"text-align:right;\">"
            + StringToHtml.convert(formZahl.format(anzBannerklicksNetscape)) + "</td>\n"
            + "                <td style=\"text-align:right;\">"
            + StringToHtml.convert(formProzent.format((double) anzBannerklicksNetscape / (double) anzBannerklicks)) + "</td>\n"
            + "                <td style=\"text-align:right;\">" + StringToHtml.convert(formZahl.format(anzBannerklicksIe))
            + "</td>\n" + "                <td style=\"text-align:right;\">"
            + StringToHtml.convert(formProzent.format((double) anzBannerklicksIe / (double) anzBannerklicks)) + "</td>\n"
            + "                <td style=\"text-align:right;\">" + StringToHtml.convert(formZahl.format(anzBannerklicksAol))
            + "</td>\n" + "                <td style=\"text-align:right;\">"
            + StringToHtml.convert(formProzent.format((double) anzBannerklicksAol / (double) anzBannerklicks)) + "</td>\n"
            + "                <td style=\"text-align:right;\">" + StringToHtml.convert(formZahl.format(anzBannerklicksOpera))
            + "</td>\n" + "                <td style=\"text-align:right;\">"
            + StringToHtml.convert(formProzent.format((double) anzBannerklicksOpera / (double) anzBannerklicks)) + "</td>\n"
            + "                <td style=\"text-align:right;\">"
            + StringToHtml.convert(formZahl.format(anzBannerklicksKonqueror)) + "</td>\n"
            + "                <td style=\"text-align:right;\">"
            + StringToHtml.convert(formProzent.format((double) anzBannerklicksKonqueror / (double) anzBannerklicks)) + "</td>\n"
            + "                <td style=\"text-align:right;\">" + StringToHtml.convert(formZahl.format(anzBannerklicksBrowser))
            + "</td>\n" + "                <td style=\"text-align:right;\">"
            + StringToHtml.convert(formProzent.format((double) anzBannerklicksBrowser / (double) anzBannerklicks)) + "</td>\n"
            + "            </tr>\n");
      }
    }

    if (!bannerText1.toString().equals(StringUtils.EMPTY)) {
      htmlText.append("        <hr>\n" + "        <p>\n" + "        <div style=\"text-align:center;\">\n"
          + "        <table style=\"margin-left:auto; margin-right:auto;\" border=\"3\">\n"
          + "            <tr style=\"background-color:#FFFF00;\">\n" + "                <th>Bannerklicks nach OS</th>\n"
          + "                <th>gesamt</th>\n" + "                <th COLSPAN=2>Windows</th>\n"
          + "                <th COLSPAN=2>OS/2</th>\n" + "                <th COLSPAN=2>Macintosh</th>\n"
          + "                <th COLSPAN=2>Unix/Linux</th>\n" + "                <th COLSPAN=2>VMS</th>\n"
          + "                <th COLSPAN=2>Amiga</th>\n" + "                <th COLSPAN=2>Sonstige</th>\n"
          + "            </tr>\n");
      htmlText.append(bannerText1.toString());
      htmlText.append("        </table>\n" + "        </div>\n" + "        </p>\n");
    }

    if (!bannerText2.toString().equals(StringUtils.EMPTY)) {
      htmlText.append("        <hr>\n" + "        <p>\n" + "        <div style=\"text-align:center;\">\n"
          + "        <table style=\"margin-left:auto; margin-right:auto;\" border=\"3\">\n"
          + "            <tr style=\"background-color:#FFFF00;\">\n" + "                <th>Bannerklicks nach Browser</th>\n"
          + "                <th>gesamt</th>\n" + "                <th colspan=\"2\">Netscape / Mozilla</th>\n"
          + "                <th colspan=\"2\">MS IE</th>\n" + "                <th colspan=\"2\">AOL</th>\n"
          + "                <th colspan=\"2\">Opera</th>\n" + "                <th colspan=\"2\">Konqueror</th>\n"
          + "                <th colspan=\"2\">Sonstige</th>\n" + "            </tr>\n");
      htmlText.append(bannerText2.toString());
      htmlText.append("        </table>\n" + "        </div>\n" + "        </p>\n");
    }

    htmlText.append("        </div>\n");
    if (fullPage) {
      htmlText.append("    </body>\n" + "</html>");
    }

    return htmlText.toString();
  }
}
