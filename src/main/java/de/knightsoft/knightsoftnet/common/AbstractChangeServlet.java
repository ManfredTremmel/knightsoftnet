/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.knightsoftnet.common;

import de.knightsoft.common.NavTabStrukt;
import de.knightsoft.common.TextException;

import javax.naming.InitialContext;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class AbstractChangeServlet extends HttpServlet {
  private static final long serialVersionUID = 4092247097002754100L;
  protected Character einzelzeichen;
  protected Integer integerZeichen;
  protected String service;
  protected String servlet;
  protected String servletReg;
  protected String dbTabelle;
  protected String wbild;
  protected String wurl;
  protected String wbildReg;
  protected String wurlReg;
  protected String loginCount;
  protected String loginDbNummer;
  protected String homeUrl;
  protected String emailAb;
  protected String passwortStart;
  protected String logoTextFarbe;
  protected String logoText;
  protected String homePage;
  protected NavTabStrukt[] thisNav;
  protected Connection myDataBase;
  protected de.knightsoft.knightsoftnet.common.KnLogin myLogin;

  /**
   * init-Methode.
   */
  @Override
  public void init(final ServletConfig config) throws ServletException {
    super.init(config);

    try {
      final InitialContext ic = new InitialContext();
      final DataSource lDataSource = (DataSource) ic.lookup("java:comp/env/jdbc/db_knightsoft");
      myDataBase = lDataSource.getConnection();
      ic.close();
    } catch (final java.sql.SQLException e) {
      throw new ServletException(e);
    } catch (final javax.naming.NamingException e) {
      throw new ServletException(e);
    }

    myLogin = new de.knightsoft.knightsoftnet.common.KnLogin(servlet, "KnightSoft-Net-" + service + " Login", service,
        dbTabelle, myDataBase, wbild, wurl, homeUrl, emailAb, passwortStart, loginDbNummer, logoTextFarbe, logoText, thisNav);
  }

  /**
   * destroy method closes database connection
   *
   * @see javax.servlet.GenericServlet#destroy()
   */
  @Override
  public void destroy() {
    // Clean up.
    try {
      if (myDataBase != null) {
        myDataBase.close();
      }
    } catch (final SQLException ignored) {
      ignored.printStackTrace();
    }
  }

  public void setService(final String newService) {
    service = newService;
  }

  public void setWbildReg(final String pwbildReg) {
    wbildReg = pwbildReg;
  }

  public void setWurlReg(final String pwurlReg) {
    wurlReg = pwurlReg;
  }

  protected ResultSet leseDbSatz(final long pdbnummer) throws TextException, SQLException {
    // Datenbankzugriff um zu sehen, ob der Eintrag wirklich existiert
    final ResultSet result =
        myDataBase.createStatement().executeQuery("SELECT * FROM " + dbTabelle + " WHERE zaehler=" + pdbnummer);
    if (!result.next()) {
      throw new de.knightsoft.common.TextException("Ihre Eintragung existiert nicht mehr!");
    }
    return result;
  }

  protected String aendernHtml(final HttpServletRequest req, final HttpServletResponse res, final String javaScriptServiceUrl,
      final String hinweis, final de.knightsoft.knightsoftnet.common.Check pcheck, final String seitenspezifisch,
      final String servletName) throws de.knightsoft.common.TextException {
    String stufe = "1";
    final StringBuilder inhalt = new StringBuilder(5000);

    inhalt.append("        <div style=\"text-align:center;\">\n");
    if (hinweis != null) {
      inhalt.append("        <h2>" + de.knightsoft.common.StringToHtml.convert(hinweis) + "</h2>\n");
    }
    if (servletName.equals(servlet)) {
      stufe = "2";
    }
    inhalt.append("        <form action=\"" + res.encodeURL(KnConst.SERVLET_URL + servletName)
        + "\" method=\"POST\" enctype=\"application/x-www-form-urlencoded; charset=utf-8\" "
        + "accept-charset=\"utf-8\" OnSubmit=\"javascript:return chkRegister();\">\n"
        + "            <p><input type=\"hidden\" name=\"Stufe\" value=\"" + stufe
        + "\">Die mit grauem Text und &sup1;) gekennzeichneten Eingaben sind optional "
        + "und k&ouml;nnen auf Wunsch weggelassen werden.</p>\n" + "            <table border=\"0\">\n"
        + "                <tr>\n" + "                    <td style=\"text-align:left;\">Land:</td>\n"
        + "                    <td style=\"text-align:left;\">\n"
        + "                        <select name=\"land\" size=\"1\">\n");
    if ("de".equals(pcheck.land)) {
      inhalt.append("                            <option value=\"de\" selected>" + "Deutschland</option>\n");
    } else {
      inhalt.append("                            <option value=\"de\">Deutschland</option>\n");
    }
    if ("at".equals(pcheck.land)) {
      inhalt.append("                            <option value=\"at\" selected>" + "&Ouml;sterreich</option>\n");
    } else {
      inhalt.append("                            <option value=\"at\">&Ouml;sterreich</option>\n");
    }
    if ("ch".equals(pcheck.land)) {
      inhalt.append("                            <option value=\"ch\" selected>Schweiz</option>\n");
    } else {
      inhalt.append("                            <option value=\"ch\">Schweiz</option>\n");
    }
    inhalt.append("                        </select>\n" + "                    </td>\n" + "                </tr>\n"
        + "                <tr>\n" + "                    <td style=\"text-align:left;\">Biete/Suche:</td>\n"
        + "                    <td style=\"text-align:left;\">\n"
        + "                        <select name=\"bietesuche\" size=\"1\">\n");
    if ("b".equals(pcheck.bietesuche)) {
      inhalt.append("                            <option value=\"b\" selected>Ich biete den "
          + de.knightsoft.common.StringToHtml.convert(service) + "-Service an</option>\n");
    } else {
      inhalt.append("                            <option value=\"b\">Ich biete den "
          + de.knightsoft.common.StringToHtml.convert(service) + "-Service an</option>\n");
    }
    if ("s".equals(pcheck.bietesuche)) {
      inhalt.append("                            <option value=\"s\" selected>Ich suche einen "
          + de.knightsoft.common.StringToHtml.convert(service) + "-Service</option>\n");
    } else {
      inhalt.append("                            <option value=\"s\">Ich suche einen "
          + de.knightsoft.common.StringToHtml.convert(service) + "-Service</option>\n");
    }
    inhalt.append("                        </select>\n" + "                    </td>\n" + "                </tr>\n"
        + "                <tr>\n" + "                    <td style=\"text-align:left;\" valign=\"top\">E-Mail:</td>\n"
        + "                    <td style=\"text-align:left;\"><input type=\"email\" "
        + "name=\"email\" size=\"30\" maxlength=\"50\" value=\"" + de.knightsoft.common.StringToHtml.convert(pcheck.email)
        + "\" autofocus pattern=\"^[a-zA-Z0-9.!#$%&´*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*\\.[a-zA-Z]{2,6}$\"></td>\n"
        + "                </tr>\n" + "                <tr>\n"
        + "                    <td style=\"text-align:left;\">Geschlecht:</td>\n");
    if ("W".equals(pcheck.geschlecht)) {
      inhalt.append("                    <td style=\"text-align:left;\"><input type=\"radio\""
          + " name=\"Geschlecht\" checked value=\"W\">weiblich <input type=\"radio\""
          + " name=\"Geschlecht\" value=\"M\">m&auml;nnlich</td>\n");
    } else {
      inhalt.append("                    <td style=\"text-align:left;\"><input type=\"radio\" "
          + "name=\"Geschlecht\" value=\"W\">weiblich <input type=\"radio\" "
          + "name=\"Geschlecht\" checked value=\"M\">m&auml;nnlich</td>\n");
    }
    inhalt.append("                </tr>\n" + "                <tr>\n"
        + "                    <td style=\"text-align:left;\">Nachname:</td>\n"
        + "                    <td style=\"text-align:left;\"><input type=\"text\" "
        + "name=\"Nname\" size=\"30\" maxlength=\"50\" value=\"" + de.knightsoft.common.StringToHtml.convert(pcheck.name)
        + "\" required=\"required\"></td>\n" + "                </tr>\n" + "                <tr>\n"
        + "                    <td style=\"text-align:left;\">Vorname:</td>\n"
        + "                    <td style=\"text-align:left;\"><input type=\"text\" "
        + "name=\"Vorname\" size=\"30\" maxlength=\"50\" value=\"" + de.knightsoft.common.StringToHtml.convert(pcheck.vorname)
        + "\" required=\"required\"></td>\n" + "                </tr>\n" + "                <tr>\n"
        + "                    <td style=\"text-align:left;\">Stra&szlig;e:</td>\n"
        + "                    <td style=\"text-align:left;\"><input type=\"text\" "
        + "name=\"Strasse\" size=\"30\" maxlength=\"50\" value=\"" + de.knightsoft.common.StringToHtml.convert(pcheck.strasse)
        + "\" required=\"required\"></td>\n" + "                </tr>\n" + "                <tr>\n"
        + "                    <td style=\"text-align:left;\">PLZ, Ort:</td>\n"
        + "                    <td style=\"text-align:left;\"><input type=\"text\" "
        + "name=\"PLZ\" size=\"5\" maxlength=\"5\" value=\"" + de.knightsoft.common.StringToHtml.convert(pcheck.plz)
        + "\" required=\"required\"><input type=\"Text\" name=\"Ort\" size=\"23\" maxlength=\"50\" value=\""
        + de.knightsoft.common.StringToHtml.convert(pcheck.ort) + "\" required=\"required\"></td>\n" + "                </tr>\n"
        + "                <tr>\n" + "                    <td style=\"text-align:left;\">Telefonnummer:<br>"
        + "<div class=\"small\">(mit Vorwahl)</div></td>\n"
        + "                    <td style=\"text-align:left;\"><input type=\"tel\" "
        + "name=\"Rufnummer\" size=\"20\" maxlength=\"20\" value=\""
        + de.knightsoft.common.StringToHtml.convert(pcheck.rufnummer) + "\" required=\"required\"></td>\n"
        + "                </tr>\n" + "                <tr>\n"
        + "                    <td style=\"text-align:left; color:gray;\" valign=\"top\"> "
        + "&sup1;) weitere Telefonnummern:<br><div class=\"small\">(mit Vorwahl)</div></td>\n"
        + "                    <td style=\"text-align:left;\"><input type=\"tel\" "
        + "name=\"weitereNummer1\" size=\"20\" maxlength=\"20\" value=\""
        + de.knightsoft.common.StringToHtml.convert(pcheck.weitereNummer1)
        + "\"><BR><input type=\"tel\" name=\"weitereNummer2\" size=\"20\" " + "maxlength=\"20\" value=\""
        + de.knightsoft.common.StringToHtml.convert(pcheck.weitereNummer2) + "\"></td>\n" + "                </tr>\n");

    if (seitenspezifisch != null) {
      inhalt.append(seitenspezifisch);
    }

    inhalt.append("                <tr>\n" + "                    <td style=\"text-align:left; color:gray;\" "
        + "valign=\"top\"> &sup1;) Bemerkung:</td>\n" + "                    <td style=\"text-align:left;\">"
        + "<textarea name=\"Bemerkung\" rows=3 cols=30>"
        + de.knightsoft.common.StringToHtml.convert(pcheck.bemerkung, false, false) + "</textarea></td>\n"
        + "                </tr>\n" + "                <tr>\n"
        + "                    <td style=\"text-align:left; color:gray;\"> &sup1;) " + "Homepage:</td>\n"
        + "                    <td style=\"text-align:left;\"><input type=\"Text\" "
        + "name=\"url\" size=\"30\" maxlength=\"200\" value=\"" + de.knightsoft.common.StringToHtml.convert(pcheck.url)
        + "\"></td>\n" + "                </tr>\n" + "                <tr>\n"
        + "                    <td style=\"text-align:left;\">Ich m&ouml;chte den "
        + "KnightSoft-Newsletter<BR>per E-Mail erhalten</td>\n");
    if ("J".equals(pcheck.news)) {
      inhalt.append(
          "                    <td style=\"text-align:left;\">" + "<input type=\"Checkbox\" name=\"News\" checked></td>\n");
    } else {
      inhalt.append("                    <td style=\"text-align:left;\">" + "<input type=\"Checkbox\" name=\"News\"></td>\n");
    }
    inhalt.append("                </tr>\n" + "                <tr>\n");

    if (servletName.equals(servlet)) {
      inhalt.append("                    <td colspan=\"2\" style=\"text-align:center;\">"
          + "<input type=\"submit\" NAME=\"Submittype\" value=\"&Auml;ndern\"> "
          + "<input type=\"submit\" NAME=\"Submittype\" value=\"L&ouml;schen\"> "
          + "<input type=\"submit\" NAME=\"Submittype\" value=\"Logout\"> " + "<input type=reset value=\"Abbrechen\"></td>\n");
    } else {
      inhalt.append("                    <td colspan=\"2\" style=\"text-align:center;\">"
          + "<input type=\"submit\" NAME=\"Submittype\" value=\"Absenden\"> "
          + "<input type=reset value=\"Abbrechen\"></td>\n");
    }

    inhalt.append("                </tr>\n" + "            </table>\n" + "        </form>\n" + "        </div>\n");

    String htmlString = null;
    if (servletName.equals(servlet)) {
      htmlString = de.knightsoft.knightsoftnet.common.SeitenTemplate.htmlSeite(req, servlet,
          "KnightSoft-Net-" + service + " Daten-&Auml;ndern",
          "        <script src=\"" + javaScriptServiceUrl
              + "chkRegister.js\" type=\"text/javascript\"></script>\n        <script src=\""
              + de.knightsoft.knightsoftnet.common.KnConst.JAVA_SCRIPT_URL
              + "chkRegister.js\" type=\"text/javascript\"></script>\n",
          true, logoTextFarbe, logoText, "Ändern oder löschen Sie hier Ihre Daten", inhalt.toString(), thisNav);
    } else {
      htmlString = de.knightsoft.knightsoftnet.common.SeitenTemplate.htmlSeite(req, servlet,
          "KnightSoft-Net-" + service + " Registrierung",
          "        <script src=\"" + javaScriptServiceUrl
              + "chkRegister.js\" type=\"text/javascript\"></script>\n        <script src=\""
              + de.knightsoft.knightsoftnet.common.KnConst.JAVA_SCRIPT_URL
              + "chkRegister.js\" type=\"text/javascript\"></script>\n",
          true, logoTextFarbe, logoText, "Registrieren eines " + service + "-Services", inhalt.toString(), thisNav);
    }

    return htmlString;
  }

  /**
   * Löschen, nochmal nachfragen.
   *
   * @param req HttpServletRequest
   * @param res HttpServletResponse
   * @param out stream to write to
   */
  protected void verarbeitungLoeschenVorbereiten(final HttpServletRequest req, final HttpServletResponse res,
      final ServletOutputStream out) throws TextException, IOException {
    final String inhalt = "<div style=\"text-align:center;\"><h2>Sind Sie sicher, dass Sie Ihre Eintragung "
        + "l&ouml;schen wollen? Die Daten k&ouml;nnen nicht wieder hergestellt werden!</h2>\n" + "<form action=\""
        + res.encodeURL(KnConst.SERVLET_URL + servlet)
        + "\" method=\"POST\" enctype=\"application/x-www-form-urlencoded; charset=utf-8\" " + "accept-charset=\"utf-8\">\n"
        + "    <p><input type=\"hidden\" name=\"Stufe\" value=\"3\"> <input type=\"submit\" "
        + "name=\"Submittype\" value=\"L&ouml;schen\"> <input type=\"submit\" "
        + "name=\"Submittype\" value=\"Abbrechen\"></p>\n" + "</form>\n</div>\n";
    final String htmlString = de.knightsoft.knightsoftnet.common.SeitenTemplate.htmlSeite(req, servlet,
        "KnightSoft-" + service + "Wollen Sie wirklich l&ouml;schen?", "", true, logoTextFarbe, logoText, "Löschbestätigung",
        inhalt, thisNav);

    out.println(htmlString);
  }

  /**
   * Löschung durchführen.
   *
   * @param req HttpServletRequest
   * @param res HttpServletResponse
   * @param out stream to write to
   */
  protected void verarbeitungLoeschen(final HttpServletRequest req, final HttpServletResponse res,
      final ServletOutputStream out) throws de.knightsoft.common.TextException, IOException {
    final HttpSession session = req.getSession(true);
    long cookieDbNr = 0;
    String inhalt = null;
    String htmlString = null;

    // Datensatznummer dem Cookiewerte entnehmen
    if (!session.isNew()) {
      final Long cookieDbNrC = (Long) session.getAttribute(servlet + loginDbNummer);
      if (cookieDbNrC != null) {
        cookieDbNr = cookieDbNrC.longValue();
      }
    }

    // Prüfen, ob auch wirklich eine Datenbanknummer im Cookie steckt
    if (cookieDbNr == 0) {
      throw new de.knightsoft.common.TextException("Es ist kein Datensatz zur Löschung ausgewählt");
    }

    try {
      // Datenbankzugriff um zu sehen, ob der Eintrag wirklich existiert
      final int result =
          myDataBase.createStatement().executeUpdate("DELETE FROM " + dbTabelle + " WHERE zaehler=" + cookieDbNr);

      switch (result) {
        case 0:
          throw new de.knightsoft.common.TextException("Der Eintrag ist bereits gelöscht!");
          // break;
        case 1:
          inhalt = "<div style=\"text-align:center;\"><h2>Ihre Eintragung im " + service
              + "-Service wurde gel&ouml;scht!</h2>\n" + "<a href=\"" + res.encodeURL(homePage) + "\">zur&uuml;ck</a></div>";

          htmlString = de.knightsoft.knightsoftnet.common.SeitenTemplate.htmlSeite(req, servlet,
              "KnightSoft-" + service + "Die L&ouml;schung wurde durchgef&uuml;hrt", "", true, logoTextFarbe, logoText,
              "Eintrag wurde gelöscht", inhalt, thisNav);
          out.println(htmlString);
          break;
        default:
          throw new de.knightsoft.common.TextException("Wenn das hier wirklich passiert, erschiesst mich!");
          // break;
      }
    } catch (final java.sql.SQLException e) {
      throw new de.knightsoft.common.TextException("Fehler beim Datenbanklöschen: " + e.toString(), e);
    }
  }

  protected void upDateDbSatz(final HttpServletRequest req, final HttpServletResponse res, final ServletOutputStream out,
      final String sqlString, final long dbNummer) throws de.knightsoft.common.TextException, IOException {
    try {
      // Datenbankzugriff um zu sehen, ob der Eintrag wirklich existiert
      myDataBase.createStatement().executeUpdate("UPDATE " + dbTabelle + " SET " + sqlString + " WHERE zaehler = " + dbNummer);

      // Na, war das nicht ein feines Update? Erfolgsmeldung auf den Schirm!
      out.println(de.knightsoft.knightsoftnet.common.SeitenTemplate.htmlSeite(req, servlet,
          "KnightSoft-" + service + " Die &Auml;nderung wurde durchgef&uuml;hrt", "", true, logoTextFarbe, logoText,
          "Änderung erfolgreich",
          "<div style=\"text-align:center;\"><h2>Ihre Eintragung im " + service
              + "-Service wurde ge&auml;ndert!</h2>\n<a href=\"" + res.encodeURL(homePage) + "\">zur&uuml;ck</a></div>",
          thisNav));
    } catch (final java.sql.SQLException e) {
      throw new de.knightsoft.common.TextException(
          "Fehler beim Datenbankändern: " + e.toString() + "\n\nDatenbank: " + dbTabelle + "\n\n" + sqlString, e);
    }
  }

  /**
   * Ändern des ausgewählten Datensatzes.
   *
   * @param req HttpServletRequest
   * @param res HttpServletResponse
   * @param out stream to write to
   * @return number of the database entry
   */
  protected long verarbeitungAendern(final HttpServletRequest req, final HttpServletResponse res, final ServletOutputStream out)
      throws de.knightsoft.common.TextException, IOException {
    final HttpSession session = req.getSession(true);

    // Datensatznummer dem Cookiewerte entnehmen
    long cookieDbNr = 0;

    // Datensatznummer dem Cookiewerte entnehmen
    if (!session.isNew()) {
      cookieDbNr = ((Long) session.getAttribute(servlet + loginDbNummer)).longValue();
    }

    // Prüfen, ob auch wirklich eine Datenbanknummer im Cookie steckt
    if (cookieDbNr == 0) {
      throw new de.knightsoft.common.TextException("Es ist kein Datensatz zur Änderung ausgewählt");
    }

    return cookieDbNr;
  }

  /**
   * Die doGet Methode wird nur bei Direkter Eingabe/Link des Servlets im Browser angesprochen.
   */
  @Override
  public void doGet(final HttpServletRequest req, final HttpServletResponse res) throws ServletException, IOException {
    // Sei faul und mach nie was doppelt, also deligiere
    doPost(req, res);
  }

  @Override
  public void doPost(final HttpServletRequest req, final HttpServletResponse res) throws ServletException, IOException {
    final String stufe = req.getParameter("Stufe");
    final String typ = req.getParameter("Submittype");
    final HttpSession session = req.getSession(true);

    // Setze als erstes den "content type" Header der Antwortseite auf HTML
    res.setContentType("text/html");

    // Writer für die HTML-Datei holen
    final ServletOutputStream out = res.getOutputStream();

    try {
      // Stufe 0 = Registrierungsfenster darstellen
      if ("0".equals(stufe)) {
        out.println(changeHtml(req, res, "Bitte füllen Sie das Formular aus, um sich zu Registrieren."));
        // Stufe 1 = Login-Fenster, bzw. wenn die Zugangsdaten OK sind, das Änderungsfenster
      } else if ("1".equals(stufe)) {
        if (myLogin.process(req, res, out, session)) {
          changeWindow(req, res, out, myLogin.getStep(), myLogin.getHint());
          // Stufe 2 = Ändern oder Löschen Button auf der Änderungsseite gedrückt
        }
      } else if ("2".equals(stufe)) {
        if ("Löschen".equals(typ)) {
          verarbeitungLoeschenVorbereiten(req, res, out);
        } else if ("Logout".equals(typ)) {
          myLogin.processLogout(req, res, out);
        } else {
          verarbeitungAendern(req, res, out);
          // Stufe 3 = Bestätigung oder Ablehnung der Löschbestätigung
        }
      } else if ("3".equals(stufe)) {
        if ("Löschen".equals(typ)) {
          verarbeitungLoeschen(req, res, out);
        } else {
          myLogin.process(req, res, out, session);
        }
      } else {
        throw new de.knightsoft.common.TextException("Unbekannte Verarbeitungsstufe!");
      }
      out.close();
    } catch (final de.knightsoft.common.TextException e) {
      // HTML-Datei für Fehlermeldung schreiben
      out.println(de.knightsoft.knightsoftnet.common.SeitenTemplate.htmlSeite(req, servlet,
          "KnightSoft-" + service + " Fehler bei Datenbank-&Auml;nderung", "", true, logoTextFarbe, logoText,
          "Ein Fehler ist aufgetreten", "<div style=\"text-align:center;\"><h2>" + e.toHtml() + "</h2>\n<a href=\""
              + res.encodeURL(homePage) + "\">zur&uuml;ck</a></div>",
          thisNav));
      out.close();
    }
  }

  /**
   * Abstrakte Klassen, die erst in den abgeleiteten Klassen wirklich declariert werden.
   *
   * @param req HttpServletRequest
   * @param res HttpServletResponse
   * @param out stream to write to
   * @param cookieDbNr database number
   * @param hint hint text
   */
  protected abstract void changeWindow(HttpServletRequest req, HttpServletResponse res, ServletOutputStream out,
      long cookieDbNr, String hint) throws de.knightsoft.common.TextException, IOException;

  /**
   * change html.
   *
   * @param req HttpServletRequest
   * @param res HttpServletResponse
   * @param hint hint text
   * @return changed html
   */
  protected abstract String changeHtml(HttpServletRequest req, HttpServletResponse res, //
      String hint) throws de.knightsoft.common.TextException;
}
