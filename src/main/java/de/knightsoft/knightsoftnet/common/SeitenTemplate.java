/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.knightsoftnet.common;

import de.knightsoft.common.NavTabStrukt;
import de.knightsoft.common.PageTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class SeitenTemplate extends de.knightsoft.common.PageTemplate {

  public static final String htmlSeite(final HttpServletRequest req, final String servletname, final String seitentitel,
      final String zusatzdaten, final boolean nocache, final String logoTextFarbe, final String logoText, final String titel,
      final String body, final NavTabStrukt[] myNavStrukt) {
    return SeitenTemplate.htmlSeite(req, servletname, seitentitel, zusatzdaten, nocache, logoTextFarbe, logoText, titel, body,
        myNavStrukt, null, null, null);
  }

  public static final String htmlSeite(final HttpServletRequest req, final String servletname, final String seitentitel,
      final String zusatzdaten, final boolean nocache, final String logoTextFarbe, final String logoText, final String titel,
      final String body, final NavTabStrukt[] myNavStrukt, final HttpSession session, final HttpServletResponse res) {
    return SeitenTemplate.htmlSeite(req, servletname, seitentitel, zusatzdaten, nocache, logoTextFarbe, logoText, titel, body,
        myNavStrukt, null, session, res);
  }

  /**
   * generate html page.
   *
   * @param req HttpServletRequest
   * @param servletname servlet name
   * @param seitentitel page title
   * @param zusatzdaten additional data
   * @param nocache don't cache the page
   * @param logoTextFarbe color of the logo
   * @param logoText text of the logo
   * @param titel page title
   * @param body page body
   * @param myNavStrukt navigation structure
   * @param selectedField selected field
   * @param session session data
   * @param res HttpServletResponse
   * @return html code of the page
   */
  public static final String htmlSeite(final HttpServletRequest req, final String servletname, final String seitentitel,
      final String zusatzdaten, final boolean nocache, final String logoTextFarbe, final String logoText, final String titel,
      final String body, final NavTabStrukt[] myNavStrukt, final String selectedField, final HttpSession session,
      final HttpServletResponse res) {
    return PageTemplate.htmlPage(req, KnConst.HTML_BASE, servletname, seitentitel, zusatzdaten, nocache, titel, body,
        selectedField, myNavStrukt, session, res, "2002-2024 by<br>Manfred Tremmel", de.knightsoft.common.Constants.EMAIL,
        de.knightsoft.common.Constants.EMAIL, logoText, logoTextFarbe, KnConst.GIF_URL + "logo_klein.png",
        KnConst.GIF_URL + "logo.png", "100", "74", "KnightSoft-Net Startseite");
  }
}
