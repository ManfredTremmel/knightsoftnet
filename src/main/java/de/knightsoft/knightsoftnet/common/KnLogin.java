/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.knightsoftnet.common;

import de.knightsoft.common.NavTabStrukt;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;

public class KnLogin extends de.knightsoft.common.Login {
  protected String wbild;
  protected String wurl;
  protected String homeUrl;
  protected String homeUrlJs;
  protected String emailAb;
  protected String passwortStart;
  protected String loginDbNummer;
  protected String logoTextFarbe;
  protected String logoText;
  protected NavTabStrukt[] thisNav;

  /**
   * Constructor.
   *
   * @param servletname name of the servlet
   * @param titel title of the page
   * @param service service name
   * @param dbTable database table
   * @param database database connection
   * @param wbild picture
   * @param wurl url
   * @param homeUrl home url
   * @param emailAb email address
   * @param passwortStart password start
   * @param loginDbNummer login database number
   * @param logoTextFarbe logo text color
   * @param logoText logo text
   * @param thisNav navigation structore
   */
  public KnLogin(final String servletname, final String titel, final String service, final String dbTable,
      final Connection database, final String wbild, final String wurl, final String homeUrl, final String emailAb,
      final String passwortStart, final String loginDbNummer, final String logoTextFarbe, final String logoText,
      final NavTabStrukt[] thisNav) {
    super(servletname, KnConst.HTML_BASE, titel, service, null, database, dbTable, "email", "passwort", null, "zaehler",
        ", eintrag_gesperrt");

    this.wbild = wbild;
    this.wurl = wurl;
    this.homeUrl = homeUrl;
    homeUrlJs = homeUrl + "js/";
    this.emailAb = emailAb;
    this.passwortStart = passwortStart;
    this.loginDbNummer = loginDbNummer;
    this.logoTextFarbe = logoTextFarbe;
    this.logoText = logoText;
    this.thisNav = thisNav;
  }

  @Override
  protected void htmlFormular(final HttpServletRequest req, final HttpServletResponse res, final ServletOutputStream out,
      final String user, final String password, final String passwordNew, final String passwordRep) throws IOException {
    final StringBuilder inhalt = new StringBuilder(3082);

    inhalt.append("        <div style=\"text-align:center;\">\n");

    if (hint != null) {
      inhalt.append("        <p>" + de.knightsoft.common.StringToHtml.convert(hint) + "</p>\n");
    }
    inhalt.append("        <p>\n" + "        <form action=\"" + res.encodeURL(KnConst.SERVLET_URL + servletName)
        + "\" method=\"POST\" enctype=\"application/x-www-form-urlencoded; charset=utf-8\" "
        + "accept-charset=\"utf-8\" OnSubmit=\"return chkLogin()\">\n" + "            <div style=\"text-align:center;\">\n"
        + "            <input type=\"hidden\" name=\"Stufe\" value=\"1\">\n"
        + "            <table style=\"margin-left:auto; margin-right:auto;\" border=\"0\">\n" + "                <tr>\n"
        + "                    <td align=\"left\">E-Mail:</td>\n");
    if (user == null) {
      inhalt.append("                    <td align=\"left\"><input type=\"Text\" "
          + "name=\"User\" size=\"30\" maxlength=\"50\"></td>\n");
    } else {
      inhalt.append(
          "                    <td align=\"left\"><input type=\"Text\" " + "name=\"User\" size=\"30\" maxlength=\"50\" value=\""
              + de.knightsoft.common.StringToHtml.convert(user) + "\"></td>\n");
    }
    inhalt.append(
        "                </tr>\n" + "                <tr>\n" + "                    <td align=\"left\">Passwort:</td>\n");
    if (password == null) {
      inhalt.append("                    <td align=\"left\"><input type=\"password\" "
          + "name=\"Password\" size=\"30\" maxlength=\"30\"></td>\n");
    } else {
      inhalt.append("                    <td align=\"left\"><input type=\"password\" "
          + "name=\"Password\" size=\"30\" maxlength=\"30\" value=\"" + de.knightsoft.common.StringToHtml.convert(password)
          + "\"></td>\n");
    }
    inhalt.append("                </tr>\n" + "                <tr>\n"
        + "                    <td colspan=\"2\" style=\"text-align:center;\">"
        + "Wenn Sie Ihr Passwort &auml;ndern wollen, tragen Sie bitte in beiden folgenden "
        + "Feldern das neue Passwort ein.</td>\n" + "                </tr>\n" + "                <tr>\n"
        + "                    <td align=\"left\">Passwort neu:</td>\n");
    if (passwordNew == null) {
      inhalt.append("                    <td align=\"left\"><input type=\"password\" "
          + "name=\"Password_new\" size=\"30\" maxlength=\"30\"></td>\n");
    } else {
      inhalt.append("                    <td align=\"left\"><input type=\"password\" "
          + "name=\"Password_new\" size=\"30\" maxlength=\"30\" value=\""
          + de.knightsoft.common.StringToHtml.convert(passwordNew) + "\"></td>\n");
    }
    inhalt.append("                </tr>\n" + "                <tr>\n"
        + "                    <td align=\"left\">Passwort best&auml;tigen:</td>\n");
    if (passwordRep == null) {
      inhalt.append("                    <td align=\"left\"><input type=\"password\" "
          + "name=\"Password_rep\" size=\"30\" maxlength=\"30\"></td>\n");
    } else {
      inhalt.append("                    <td align=\"left\"><input type=\"password\" "
          + "name=\"Password_rep\" size=\"30\" maxlength=\"30\" value=\""
          + de.knightsoft.common.StringToHtml.convert(passwordRep) + "\"></td>\n");
    }
    inhalt.append("                </tr>\n" + "                <tr>\n"
        + "                    <td colspan=\"2\" style=\"text-align:center;\">"
        + "                      <input type=\"submit\" name=\"Submittype\" "
        + "value=\"Absenden\" onClick=\"javascript:PWCheck=1\">"
        + "                      <input type=\"submit\" name=\"Submittype\" "
        + "value=\"Passwort vergessen *\" onClick=\"javascript:PWCheck=0\">"
        + "                      <input type=reset value=\"Abbrechen\">" + "                    </td>\n"
        + "                </tr>\n" + "            </table>\n" + "            </div>\n" + "        </form>\n"
        + "        <p>*) Sollten Sie sich eingetragen haben, aber kein Passwort per E-Mail "
        + "erhalten haben, oder aber Ihr Passwort nicht mehr wissen, tragen Sie bitte Ihre "
        + "E-Mail ein und bet&auml;tigen dann den \"Passwort vergessen *\" Knopf.\n"
        + "        <br>Ein neues Passwort wird generiert und Ihnen per E-Mail zugeschickt, "
        + "das bisherige Passwort wird unwirksam!</p>\n" + "        </div>\n");

    final String htmlPage =
        de.knightsoft.knightsoftnet.common.SeitenTemplate.htmlSeite(req, servletName, "KnightSoft-" + service + " KnLogin",
            "        <script src=\"" + de.knightsoft.knightsoftnet.common.KnConst.JAVA_SCRIPT_URL
                + "chkLogin.js\" type=\"text/javascript\"></script>\n",
            true, logoTextFarbe, logoText, service + " KnLogin", inhalt.toString(), thisNav);

    out.println(htmlPage);
  }

  @Override
  protected boolean loginok(final ResultSet result, final String user, final String passwort, final String passwordNew,
      final String passwordRep, final HttpSession session) throws de.knightsoft.common.TextException {
    final boolean isok = true;
    String sqlString = StringUtils.EMPTY;

    final Long cookieDbNr = Long.valueOf(step);

    // Datenbanknummer in die Session speichern
    session.setAttribute(servletName + loginDbNummer, cookieDbNr);

    try {
      if (result.getString("eintrag_gesperrt").equals("J")) {
        if (hint == null || hint.equals(StringUtils.EMPTY)) {
          hint = "Ihr Eintrag wurde freigeschalten!";
        } else {
          hint += "\nIhr Eintrag wurde freigeschalten!";
        }

        sqlString = "UPDATE    " + dbTable + " " + "SET        datum_login=NOW()," + "            anzahl_fehllogin=0,"
            + "            datum_fehllogin=NULL, " + "            eintrag_gesperrt='N', "
            + "            datum_geaendert=CURDATE()" + passwortaenderung(passwort, passwordNew, passwordRep)
            + "WHERE    zaehler=" + step;
      } else {
        sqlString = "UPDATE    " + dbTable + " " + "SET        datum_login=NOW()," + "            anzahl_fehllogin=0,"
            + "            datum_fehllogin=NULL " + passwortaenderung(passwort, passwordNew, passwordRep) + "WHERE    zaehler="
            + step;
      }

      thisDatabase.createStatement().executeUpdate(sqlString);
    } catch (final java.sql.SQLException e) {
      throw new de.knightsoft.common.TextException(
          "Fehler beim Datenbankzugriff: " + e.toString() + "\n\nSQL-String:\n" + sqlString, e);
    }

    return isok;
  }

  /**
   * Nach der Eingabe, Daten auf Gültigkeit prüfen und zur Bestätigung durch den Benutzer nochmals ausgeben.
   */
  @Override
  public boolean process(final HttpServletRequest req, final HttpServletResponse res, final ServletOutputStream out,
      final HttpSession session) throws IOException {
    String submittype = req.getParameter("Submittype");
    boolean loginOk = false;
    if (submittype == null) {
      submittype = "Absenden";
    }

    if ("Absenden".equals(submittype)) {
      loginOk = super.process(req, res, out, session);
    } else {
      try {
        verarbeitungVergessen(req, res, out);
      } catch (final de.knightsoft.common.TextException e) {
        hint = e.toString();
        htmlFormular(req, res, out, req.getParameter("User"), req.getParameter("Password"), req.getParameter("Password_new"),
            req.getParameter("Password_rep"));
      }
    }

    return loginOk;
  }

  /**
   * Nach der Eingabe, Daten auf Gültigkeit prüfen und zur bestätigung durch den Benutzer nochmals ausgeben.
   *
   * @param req HttpServletRequest
   * @param res HttpServletResponse
   * @param out ServletOutputStream
   */
  protected void verarbeitungVergessen(final HttpServletRequest req, final HttpServletResponse res,
      final ServletOutputStream out) throws de.knightsoft.common.TextException, IOException {
    final String user = req.getParameter("User");
    int zaehler = 0;
    long dbSatz = 0;
    final String emailAb = de.knightsoft.common.Constants.EMAIL;
    String passwort = null;
    String geschlecht = null;
    String name = null;
    String emailText = null;

    if (user == null || user.equals(StringUtils.EMPTY)) {
      throw new de.knightsoft.common.TextException("Bitte geben Sie Ihre E-Mail Adresse ein");
    }

    try (final ResultSet result = thisDatabase.createStatement().executeQuery("SELECT zaehler, geschlecht, name FROM " + dbTable
        + " WHERE email=" + de.knightsoft.common.StringToSql.convert(user))) {
      // Datenbankzugriff um zu sehen, ob der Eintrag wirklich existiert

      while (result.next()) {
        dbSatz = result.getLong("zaehler");
        geschlecht = result.getString("geschlecht");
        name = result.getString("name");

        passwort = de.knightsoft.common.Constants.generatePassword(passwortStart);

        emailText = de.knightsoft.knightsoftnet.common.KnConst.neuesPasswortEmail(name, geschlecht, passwort, service);

        new de.knightsoft.common.SendEMail(emailAb, de.knightsoft.common.Constants.ORGANISATION, user,
            "Neue Passwortanforderung", emailText);

        thisDatabase.createStatement().executeUpdate("UPDATE " + dbTable + " SET datum_geaendert=CURDATE(), passwort=password("
            + de.knightsoft.common.StringToSql.convert(passwort) + ")" + " WHERE zaehler = " + dbSatz);
        zaehler++;
      }
      switch (zaehler) {
        case 0:
          throw new de.knightsoft.common.TextException("Es existiert keine Eintragung für diese E-Mail-Adresse.");
        // break;
        case 1:
          hint = "Ein neues Passwort wurde generiert und Ihnen per E-Mail zugeschickt";
          break;
        default:
          hint = "Es wurden " + zaehler + " Passwörter generiert und Ihnen per E-Mail zugeschickt";
          break;
      }
      final String Inhalt = "        <div style=\"text-align:center;\">\n" + "        <form action=\""
          + res.encodeURL(KnConst.SERVLET_URL + servletName)
          + "\" method=\"POST\" enctype=\"application/x-www-form-urlencoded; charset=utf-8\" " + "accept-charset=\"utf-8\">\n"
          + "            <input type=\"hidden\" name=\"Stufe\" value=\"1\">\n" + "            <p>" + hint + "</p>\n"
          + "            <input type=submit name=\"Submittype\" value=\"Zur&uuml;ck\">" + "        </form>\n"
          + "        </div>\n";
      out.println(
          de.knightsoft.knightsoftnet.common.SeitenTemplate.htmlSeite(req, servletName, "KnightSoft-" + service + " KnLogin",
              StringUtils.EMPTY, true, logoTextFarbe, logoText, service + " KnLogin", Inhalt, thisNav));
    } catch (final java.sql.SQLException e) {
      throw new de.knightsoft.common.TextException("Fehler beim KnLogin-Datenbankzugriff: " + e.toString(), e);
    }
  }

  /**
   * Passwort vorprüfung.
   */
  @Override
  public String checkPassword(final String ppwd) {
    String pwd = ppwd;
    if (pwd != null && pwd.length() > 14 && pwd.substring(0, 4).equals(passwortStart)) {
      pwd = pwd.substring(0, 14);
    }
    return pwd;
  }

}
