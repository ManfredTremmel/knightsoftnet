/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.knightsoftnet.common;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;

public class DbSynchron {
  protected static Connection fromDataBase;
  protected static Connection toDataBase;

  private static String dbString(final ResultSet result, final String field) throws java.sql.SQLException {
    return de.knightsoft.common.StringToSql.convert(result.getString(field));
  }

  /**
   * Main Prozedur, Datenbanken öffnen, alte Werte löschen und Funktion zur Datenübertragung aufrufen.
   *
   * @param args commandline arguments
   */
  public static void main(final String[] args) {
    boolean upDateBabysitter = true;
    boolean upDateFahrgemeinschaft = true;
    boolean upDateNachhilfe = true;
    String zwDatumGst = null;
    String zwDatumBk = null;
    String zwSaisonMin = null;
    String jdbcDriver = null;
    String jdbcDatabase = null;
    String jdbcUser = null;
    String jdbcPassword = null;
    String jdbcDriverTarget = null;
    String jdbcDatabaseTarget = null;
    String jdbcUserTarget = null;
    String jdbcPasswordTarget = null;

    try {
      for (int i = 0; i < args.length; i++) {
        if (args[i].equals("-dbdriver")) {
          jdbcDriver = args[++i];
        } else if (args[i].equals("-dbdatabase")) {
          jdbcDatabase = args[++i];
        } else if (args[i].equals("-dbuser")) {
          jdbcUser = args[++i];
        } else if (args[i].equals("-dbpassword")) {
          jdbcPassword = args[++i];
        } else if (args[i].equals("-dbdrivertarget")) {
          jdbcDriverTarget = args[++i];
        } else if (args[i].equals("-dbdatabasetarget")) {
          jdbcDatabaseTarget = args[++i];
        } else if (args[i].equals("-dbusertarget")) {
          jdbcUserTarget = args[++i];
        } else if (args[i].equals("-dbpasswordtarget")) {
          jdbcPasswordTarget = args[++i];
        }
      }

      Class.forName(jdbcDriver);
      DbSynchron.fromDataBase = DriverManager.getConnection(jdbcDatabase, jdbcUser, jdbcPassword);

      Class.forName(jdbcDriverTarget);
      DbSynchron.toDataBase = DriverManager.getConnection(jdbcDatabaseTarget, jdbcUserTarget, jdbcPasswordTarget);

      // Prüfen, ob Updates notwendig sind
      try (
          final ResultSet result = DbSynchron.toDataBase.createStatement()
              .executeQuery("SELECT COUNT(*) AS anz, " + "MAX(datum_erstellt) AS dat_e, MAX(datum_geaendert) AS dat_g "
                  + "FROM KnightSoft_BabySitter");
          final ResultSet result2 = DbSynchron.fromDataBase.createStatement().executeQuery("SELECT COUNT(*) AS anz, "
              + "MAX(datum_erstellt) AS dat_e, MAX(datum_geaendert) AS dat_g " + "FROM KnightSoft_BabySitter");) {
        if (result.next() && result2.next()) {
          if (result.getString("dat_e") != null && result.getString("dat_g") != null) {
            if (result.getInt("anz") == result2.getInt("anz") && result.getString("dat_e").equals(result2.getString("dat_e"))
                && result.getString("dat_g").equals(result2.getString("dat_g"))) {
              upDateBabysitter = false;
            }
          }
        }
      }

      try (
          final ResultSet result = DbSynchron.toDataBase.createStatement()
              .executeQuery("SELECT COUNT(*) AS anz, " + "MAX(datum_erstellt) AS dat_e, MAX(datum_geaendert) AS dat_g "
                  + "FROM KnightSoft_Fahrgemeinschaft");
          final ResultSet result2 = DbSynchron.fromDataBase.createStatement().executeQuery("SELECT COUNT(*) AS anz, "
              + "MAX(datum_erstellt) AS dat_e, MAX(datum_geaendert) AS dat_g " + "FROM KnightSoft_Fahrgemeinschaft");) {
        if (result.next() && result2.next()) {
          if (result.getString("dat_e") == null || result.getString("dat_g") == null) {
            upDateFahrgemeinschaft = true;
          } else {
            if (result.getInt("anz") == result2.getInt("anz") && result.getString("dat_e").equals(result2.getString("dat_e"))
                && result.getString("dat_g").equals(result2.getString("dat_g"))) {
              upDateFahrgemeinschaft = false;
            } else {
              upDateFahrgemeinschaft = true;
            }
          }
        }
      }

      try (
          final ResultSet result = DbSynchron.toDataBase.createStatement()
              .executeQuery("SELECT COUNT(*) AS anz, MAX(datum_erstellt) AS dat_e, MAX(datum_geaendert) AS dat_g "
                  + "FROM KnightSoft_Nachhilfe");
          final ResultSet result2 = DbSynchron.fromDataBase.createStatement().executeQuery("SELECT COUNT(*) AS anz, "
              + "MAX(datum_erstellt) AS dat_e, MAX(datum_geaendert) AS dat_g " + "FROM KnightSoft_Nachhilfe");) {
        if (result.next() && result2.next()) {
          if (result.getString("dat_e") == null || result.getString("dat_g") == null) {
            upDateNachhilfe = true;
          } else {
            if (result.getInt("anz") == result2.getInt("anz") && result.getString("dat_e").equals(result2.getString("dat_e"))
                && result.getString("dat_g").equals(result2.getString("dat_g"))) {
              upDateNachhilfe = false;
            } else {
              upDateNachhilfe = true;
            }
          }
        }
      }

      // heimische Datenbank löschen
      if (upDateBabysitter) {
        DbSynchron.toDataBase.createStatement().executeUpdate("DELETE FROM KnightSoft_BabySitter");
      }
      if (upDateFahrgemeinschaft) {
        DbSynchron.toDataBase.createStatement().executeUpdate("DELETE FROM KnightSoft_Fahrgemeinschaft");
      }
      if (upDateNachhilfe) {
        DbSynchron.toDataBase.createStatement().executeUpdate("DELETE FROM KnightSoft_Nachhilfe");
      }
      DbSynchron.toDataBase.createStatement().executeUpdate("DELETE FROM KnightSoft_Gewinnspiel");
      DbSynchron.toDataBase.createStatement().executeUpdate("DELETE FROM KnightSoft_Banner");
      DbSynchron.toDataBase.createStatement().executeUpdate("DELETE FROM KnightSoft_Bannerpartner");
      DbSynchron.toDataBase.createStatement().executeUpdate("DELETE FROM KnightSoft_Mail");

      // höchstes Datum der Gewinnspiel-Teilnehmer ermitteln
      try (final ResultSet result = DbSynchron.toDataBase.createStatement()
          .executeQuery("SELECT MAX(datum_erstellt) FROM KnightSoft_GewinnspielTeilnehmer")) {
        if (result.next()) {
          zwDatumGst = result.getString("MAX(datum_erstellt)");
          if (zwDatumGst == null) {
            zwDatumGst = "1999-01-01";
          }
        } else {
          zwDatumGst = "1999-01-01";
        }
      }

      // höchstes Datum der Bannerklicks ermitteln
      try (final ResultSet result = DbSynchron.toDataBase.createStatement().executeQuery(
          "SELECT DATE_FORMAT(MAX(datum_zeit),'%Y-%m-%d 00:00:00') AS maxdatum " + "FROM KnightSoft_Bannerklicks")) {
        if (result.next()) {
          zwDatumBk = result.getString("maxdatum");
          if (zwDatumBk == null) {
            zwDatumBk = "1999-01-01 00:00:00";
          }
        } else {
          zwDatumBk = "1999-01-01 00:00:00";
        }
      }

      // höchste Bundesligasaison ermitteln
      try (final ResultSet result = DbSynchron.toDataBase.createStatement()
          .executeQuery("SELECT MAX(saison_liga) AS max_saison_liga FROM KnightSoft_TippLiga")) {
        if (result.next()) {
          zwSaisonMin = result.getString("max_saison_liga");
          if (zwSaisonMin == null) {
            zwSaisonMin = "199900000";
          } else {
            zwSaisonMin = zwSaisonMin.substring(0, 4) + "00000";
          }
        } else {
          zwSaisonMin = "199900000";
        }
      }

      // Einträge ab diesem Datum werden gelöscht
      DbSynchron.toDataBase.createStatement()
          .executeUpdate("DELETE FROM KnightSoft_GewinnspielTeilnehmer WHERE datum_erstellt>="
              + de.knightsoft.common.StringToSql.convert(zwDatumGst));
      // abfrage auf ZW_DATUM_BK geändert
      // ToDataBase.executeUpdate("DELETE FROM KnightSoft_Bannerklicks WHERE datum_zeit>='" +
      // ZW_Datum_GST + " 00:00:00'");
      DbSynchron.toDataBase.createStatement().executeUpdate(
          "DELETE FROM KnightSoft_Bannerklicks WHERE datum_zeit>=" + de.knightsoft.common.StringToSql.convert(zwDatumBk));

      // Tipprunde, Tabellen leeren
      DbSynchron.toDataBase.createStatement().executeUpdate("DELETE FROM KnightSoft_TippLiga WHERE saison_liga>" + zwSaisonMin);
      DbSynchron.toDataBase.createStatement()
          .executeUpdate("DELETE FROM KnightSoft_TippLigaMannschaft WHERE saison_liga>" + zwSaisonMin);
      DbSynchron.toDataBase.createStatement().executeUpdate("DELETE FROM KnightSoft_TippMannschaften");
      DbSynchron.toDataBase.createStatement()
          .executeUpdate("DELETE FROM KnightSoft_TippMitspieler WHERE saison_liga>" + zwSaisonMin);
      DbSynchron.toDataBase.createStatement()
          .executeUpdate("DELETE FROM KnightSoft_TippSpiele WHERE saison_liga>" + zwSaisonMin);
      DbSynchron.toDataBase.createStatement()
          .executeUpdate("DELETE FROM KnightSoft_TippSpieleTipp WHERE saison_liga>" + zwSaisonMin);

      // Berechtigungstabelle leeren
      DbSynchron.toDataBase.createStatement().executeUpdate("DELETE FROM KnightSoft_Berechtigungen");

      // Lesezugriff auf die Datenbank (BabySitter-Tabelle)
      if (upDateBabysitter) {
        try (final ResultSet result =
            DbSynchron.fromDataBase.createStatement().executeQuery("SELECT * FROM KnightSoft_BabySitter ORDER BY zaehler")) {
          while (result.next()) {
            DbSynchron.toDataBase.createStatement()
                .executeUpdate("INSERT INTO KnightSoft_BabySitter VALUES(" + result.getString("zaehler") + ", "
                    + DbSynchron.dbString(result, "sende_infos") + ", " + DbSynchron.dbString(result, "eintrag_gesperrt") + ", "
                    + DbSynchron.dbString(result, "datum_erstellt") + ", " + DbSynchron.dbString(result, "datum_geaendert")
                    + ", " + DbSynchron.dbString(result, "geschlecht") + ", " + DbSynchron.dbString(result, "name") + ", "
                    + DbSynchron.dbString(result, "vorname") + ", " + DbSynchron.dbString(result, "strasse") + ", "
                    + DbSynchron.dbString(result, "plz") + ", " + DbSynchron.dbString(result, "ort") + ", "
                    + DbSynchron.dbString(result, "email") + ", " + DbSynchron.dbString(result, "vorwahl") + ", "
                    + DbSynchron.dbString(result, "telefonnummer1") + ", " + DbSynchron.dbString(result, "telefonnummer2")
                    + ", " + DbSynchron.dbString(result, "telefonnummer3") + ", " + DbSynchron.dbString(result, "passwort")
                    + ", " + DbSynchron.dbString(result, "geburtsjahr") + ", " + DbSynchron.dbString(result, "mehrere_kinder")
                    + ", " + DbSynchron.dbString(result, "koerper_behinderte") + ", "
                    + DbSynchron.dbString(result, "geistig_behinderte") + ", " + DbSynchron.dbString(result, "preis_text")
                    + ", " + DbSynchron.dbString(result, "bemerkung") + ", " + DbSynchron.dbString(result, "url") + ", "
                    + DbSynchron.dbString(result, "land") + ", " + DbSynchron.dbString(result, "bietesuche") + ", "
                    + result.getString("anzahl_fehllogin") + ", " + DbSynchron.dbString(result, "datum_fehllogin") + ", "
                    + DbSynchron.dbString(result, "datum_login") + ")");
          }
        }
      }

      // Lesezugriff auf die Datenbank (Fahrgemeinschaft-Tabelle)
      if (upDateFahrgemeinschaft) {
        try (final ResultSet result = DbSynchron.fromDataBase.createStatement()
            .executeQuery("SELECT * FROM KnightSoft_Fahrgemeinschaft ORDER BY zaehler")) {
          while (result.next()) {
            DbSynchron.toDataBase.createStatement()
                .executeUpdate("INSERT INTO KnightSoft_Fahrgemeinschaft VALUES(" + result.getString("zaehler") + ", "
                    + DbSynchron.dbString(result, "sende_infos") + ", " + DbSynchron.dbString(result, "eintrag_gesperrt") + ", "
                    + DbSynchron.dbString(result, "datum_erstellt") + ", " + DbSynchron.dbString(result, "datum_geaendert")
                    + ", " + DbSynchron.dbString(result, "geschlecht") + ", " + DbSynchron.dbString(result, "name") + ", "
                    + DbSynchron.dbString(result, "vorname") + ", " + DbSynchron.dbString(result, "strasse") + ", "
                    + DbSynchron.dbString(result, "plz") + ", " + DbSynchron.dbString(result, "ort") + ", "
                    + DbSynchron.dbString(result, "email") + ", " + DbSynchron.dbString(result, "telefonnummer1") + ", "
                    + DbSynchron.dbString(result, "telefonnummer2") + ", " + DbSynchron.dbString(result, "telefonnummer3")
                    + ", " + DbSynchron.dbString(result, "passwort") + ", " + DbSynchron.dbString(result, "einmalige_fahrt")
                    + ", " + DbSynchron.dbString(result, "abfahrtsdatum") + ", " + DbSynchron.dbString(result, "abfahrtsplz")
                    + ", " + DbSynchron.dbString(result, "abfahrtsort") + ", " + DbSynchron.dbString(result, "abfahrtsstrasse")
                    + ", " + DbSynchron.dbString(result, "abfahrtszeit") + ", " + DbSynchron.dbString(result, "zielplz") + ", "
                    + DbSynchron.dbString(result, "zielort") + ", " + DbSynchron.dbString(result, "zielstrasse") + ", "
                    + DbSynchron.dbString(result, "ankunftszeit") + ", " + DbSynchron.dbString(result, "bemerkung") + ", "
                    + DbSynchron.dbString(result, "url") + ", " + DbSynchron.dbString(result, "rueckfahrt") + ", "
                    + DbSynchron.dbString(result, "rueckfahrtsdatum") + ", " + DbSynchron.dbString(result, "rueckAbfahrtszeit")
                    + ", " + DbSynchron.dbString(result, "rueckAnkunftszeit") + ", " + DbSynchron.dbString(result, "land")
                    + ", " + DbSynchron.dbString(result, "bietesuche") + ", " + result.getString("anzahl_fehllogin") + ", "
                    + DbSynchron.dbString(result, "datum_fehllogin") + ", " + DbSynchron.dbString(result, "datum_login") + ")");
          }
        }
      }

      // Lesezugriff auf die Datenbank (Nachhilfe-Tabelle)
      if (upDateNachhilfe) {
        try (final ResultSet result =
            DbSynchron.fromDataBase.createStatement().executeQuery("SELECT * FROM KnightSoft_Nachhilfe ORDER BY zaehler")) {
          while (result.next()) {
            DbSynchron.toDataBase.createStatement().executeUpdate("INSERT INTO KnightSoft_Nachhilfe VALUES("
                + result.getString("zaehler") + ", " + DbSynchron.dbString(result, "sende_infos") + ", "
                + DbSynchron.dbString(result, "eintrag_gesperrt") + ", " + DbSynchron.dbString(result, "datum_erstellt") + ", "
                + DbSynchron.dbString(result, "datum_geaendert") + ", " + DbSynchron.dbString(result, "geschlecht") + ", "
                + DbSynchron.dbString(result, "name") + ", " + DbSynchron.dbString(result, "vorname") + ", "
                + DbSynchron.dbString(result, "strasse") + ", " + DbSynchron.dbString(result, "plz") + ", "
                + DbSynchron.dbString(result, "ort") + ", " + DbSynchron.dbString(result, "email") + ", "
                + DbSynchron.dbString(result, "vorwahl") + ", " + DbSynchron.dbString(result, "telefonnummer1") + ", "
                + DbSynchron.dbString(result, "telefonnummer2") + ", " + DbSynchron.dbString(result, "telefonnummer3") + ", "
                + DbSynchron.dbString(result, "passwort") + ", " + DbSynchron.dbString(result, "geburtsjahr") + ", "
                + DbSynchron.dbString(result, "biologie") + ", " + DbSynchron.dbString(result, "biologie_j_von") + ", "
                + DbSynchron.dbString(result, "biologie_j_bis") + ", " + DbSynchron.dbString(result, "biologie_s_von") + ", "
                + DbSynchron.dbString(result, "biologie_s_bis") + ", " + DbSynchron.dbString(result, "bwl") + ", "
                + DbSynchron.dbString(result, "bwl_j_von") + ", " + DbSynchron.dbString(result, "bwl_j_bis") + ", "
                + DbSynchron.dbString(result, "bwl_s_von") + ", " + DbSynchron.dbString(result, "bwl_s_bis") + ", "
                + DbSynchron.dbString(result, "chemie") + ", " + DbSynchron.dbString(result, "chemie_j_von") + ", "
                + DbSynchron.dbString(result, "chemie_j_bis") + ", " + DbSynchron.dbString(result, "chemie_s_von") + ", "
                + DbSynchron.dbString(result, "chemie_s_bis") + ", " + DbSynchron.dbString(result, "deutsch") + ", "
                + DbSynchron.dbString(result, "deutsch_j_von") + ", " + DbSynchron.dbString(result, "deutsch_j_bis") + ", "
                + DbSynchron.dbString(result, "deutsch_s_von") + ", " + DbSynchron.dbString(result, "deutsch_s_bis") + ", "
                + DbSynchron.dbString(result, "englisch") + ", " + DbSynchron.dbString(result, "englisch_j_von") + ", "
                + DbSynchron.dbString(result, "englisch_j_bis") + ", " + DbSynchron.dbString(result, "englisch_s_von") + ", "
                + DbSynchron.dbString(result, "englisch_s_bis") + ", " + DbSynchron.dbString(result, "erdkunde") + ", "
                + DbSynchron.dbString(result, "erdkunde_j_von") + ", " + DbSynchron.dbString(result, "erdkunde_j_bis") + ", "
                + DbSynchron.dbString(result, "erdkunde_s_von") + ", " + DbSynchron.dbString(result, "erdkunde_s_bis") + ", "
                + DbSynchron.dbString(result, "franzoesisch") + ", " + DbSynchron.dbString(result, "franzoesisch_j_von") + ", "
                + DbSynchron.dbString(result, "franzoesisch_j_bis") + ", " + DbSynchron.dbString(result, "franzoesisch_s_von")
                + ", " + DbSynchron.dbString(result, "franzoesisch_s_bis") + ", " + DbSynchron.dbString(result, "geschichte")
                + ", " + DbSynchron.dbString(result, "geschichte_j_von") + ", "
                + DbSynchron.dbString(result, "geschichte_j_bis") + ", " + DbSynchron.dbString(result, "geschichte_s_von")
                + ", " + DbSynchron.dbString(result, "geschichte_s_bis") + ", " + DbSynchron.dbString(result, "griechisch")
                + ", " + DbSynchron.dbString(result, "griechisch_j_von") + ", "
                + DbSynchron.dbString(result, "griechisch_j_bis") + ", " + DbSynchron.dbString(result, "griechisch_s_von")
                + ", " + DbSynchron.dbString(result, "griechisch_s_bis") + ", " + DbSynchron.dbString(result, "informatik")
                + ", " + DbSynchron.dbString(result, "informatik_j_von") + ", "
                + DbSynchron.dbString(result, "informatik_j_bis") + ", " + DbSynchron.dbString(result, "informatik_s_von")
                + ", " + DbSynchron.dbString(result, "informatik_s_bis") + ", " + DbSynchron.dbString(result, "latein") + ", "
                + DbSynchron.dbString(result, "latein_j_von") + ", " + DbSynchron.dbString(result, "latein_j_bis") + ", "
                + DbSynchron.dbString(result, "latein_s_von") + ", " + DbSynchron.dbString(result, "latein_s_bis") + ", "
                + DbSynchron.dbString(result, "maschineschreiben") + ", "
                + DbSynchron.dbString(result, "maschineschreiben_j_von") + ", "
                + DbSynchron.dbString(result, "maschineschreiben_j_bis") + ", "
                + DbSynchron.dbString(result, "maschineschreiben_s_von") + ", "
                + DbSynchron.dbString(result, "maschineschreiben_s_bis") + ", " + DbSynchron.dbString(result, "mathematik")
                + ", " + DbSynchron.dbString(result, "mathematik_j_von") + ", "
                + DbSynchron.dbString(result, "mathematik_j_bis") + ", " + DbSynchron.dbString(result, "mathematik_s_von")
                + ", " + DbSynchron.dbString(result, "mathematik_s_bis") + ", " + DbSynchron.dbString(result, "physik") + ", "
                + DbSynchron.dbString(result, "physik_j_von") + ", " + DbSynchron.dbString(result, "physik_j_bis") + ", "
                + DbSynchron.dbString(result, "physik_s_von") + ", " + DbSynchron.dbString(result, "physik_s_bis") + ", "
                + DbSynchron.dbString(result, "sozialkunde") + ", " + DbSynchron.dbString(result, "sozialkunde_j_von") + ", "
                + DbSynchron.dbString(result, "sozialkunde_j_bis") + ", " + DbSynchron.dbString(result, "sozialkunde_s_von")
                + ", " + DbSynchron.dbString(result, "sozialkunde_s_bis") + ", " + DbSynchron.dbString(result, "stenografie")
                + ", " + DbSynchron.dbString(result, "stenografie_j_von") + ", "
                + DbSynchron.dbString(result, "stenografie_j_bis") + ", " + DbSynchron.dbString(result, "stenografie_s_von")
                + ", " + DbSynchron.dbString(result, "stenografie_s_bis") + ", " + DbSynchron.dbString(result, "weiteres_fach1")
                + ", " + DbSynchron.dbString(result, "weiteres_fach1_j_von") + ", "
                + DbSynchron.dbString(result, "weiteres_fach1_j_bis") + ", "
                + DbSynchron.dbString(result, "weiteres_fach1_s_von") + ", "
                + DbSynchron.dbString(result, "weiteres_fach1_s_bis") + ", " + DbSynchron.dbString(result, "weiteres_fach2")
                + ", " + DbSynchron.dbString(result, "weiteres_fach2_j_von") + ", "
                + DbSynchron.dbString(result, "weiteres_fach2_j_bis") + ", "
                + DbSynchron.dbString(result, "weiteres_fach2_s_von") + ", "
                + DbSynchron.dbString(result, "weiteres_fach2_s_bis") + ", " + DbSynchron.dbString(result, "weiteres_fach3")
                + ", " + DbSynchron.dbString(result, "weiteres_fach3_j_von") + ", "
                + DbSynchron.dbString(result, "weiteres_fach3_j_bis") + ", "
                + DbSynchron.dbString(result, "weiteres_fach3_s_von") + ", "
                + DbSynchron.dbString(result, "weiteres_fach3_s_bis") + ", " + DbSynchron.dbString(result, "preis_text") + ", "
                + DbSynchron.dbString(result, "bemerkung") + ", " + DbSynchron.dbString(result, "url") + ", "
                + DbSynchron.dbString(result, "land") + ", " + DbSynchron.dbString(result, "bietesuche") + ", "
                + result.getString("anzahl_fehllogin") + ", " + DbSynchron.dbString(result, "datum_fehllogin") + ", "
                + DbSynchron.dbString(result, "datum_login") + ")");
          }
        }
      }

      // Lesezugriff auf die Datenbank (Gewinnspiel-Tabelle)
      try (final ResultSet result =
          DbSynchron.fromDataBase.createStatement().executeQuery("SELECT * FROM KnightSoft_Gewinnspiel ORDER BY zaehler")) {
        while (result.next()) {
          DbSynchron.toDataBase.createStatement()
              .executeUpdate("INSERT INTO KnightSoft_Gewinnspiel VALUES(" + result.getString("zaehler") + ", "
                  + DbSynchron.dbString(result, "eintrag_gesperrt") + ", " + DbSynchron.dbString(result, "datum_von") + ", "
                  + DbSynchron.dbString(result, "datum_bis") + ", " + DbSynchron.dbString(result, "datum_ziehung") + ", "
                  + DbSynchron.dbString(result, "anzahl_preise") + ", " + DbSynchron.dbString(result, "system") + ", "
                  + DbSynchron.dbString(result, "ausgabetext") + ", " + DbSynchron.dbString(result, "reihenfolge_wichtig")
                  + ", " + DbSynchron.dbString(result, "formular_generieren") + ", " + DbSynchron.dbString(result, "frage1")
                  + ", " + DbSynchron.dbString(result, "frage2") + ", " + DbSynchron.dbString(result, "frage3") + ", "
                  + DbSynchron.dbString(result, "frage4") + ", " + DbSynchron.dbString(result, "frage5") + ", "
                  + DbSynchron.dbString(result, "frage6") + ", " + DbSynchron.dbString(result, "frage7") + ", "
                  + DbSynchron.dbString(result, "frage8") + ", " + DbSynchron.dbString(result, "frage9") + ", "
                  + DbSynchron.dbString(result, "frage10") + ", " + DbSynchron.dbString(result, "antwort1") + ", "
                  + DbSynchron.dbString(result, "antwort2") + ", " + DbSynchron.dbString(result, "antwort3") + ", "
                  + DbSynchron.dbString(result, "antwort4") + ", " + DbSynchron.dbString(result, "antwort5") + ", "
                  + DbSynchron.dbString(result, "antwort6") + ", " + DbSynchron.dbString(result, "antwort7") + ", "
                  + DbSynchron.dbString(result, "antwort8") + ", " + DbSynchron.dbString(result, "antwort9") + ", "
                  + DbSynchron.dbString(result, "antwort10") + ", " + DbSynchron.dbString(result, "bild") + ", "
                  + DbSynchron.dbString(result, "url") + ", " + DbSynchron.dbString(result, "gewinnerliste") + ")");
        }
      }

      // Lesezugriff auf die Datenbank (GewinnspielTeilnehmer-Tabelle)
      try (final ResultSet result = DbSynchron.fromDataBase.createStatement()
          .executeQuery("SELECT * FROM KnightSoft_GewinnspielTeilnehmer WHERE datum_erstellt>="
              + de.knightsoft.common.StringToSql.convert(zwDatumGst) + " ORDER BY zaehler")) {
        while (result.next()) {
          DbSynchron.toDataBase.createStatement()
              .executeUpdate("INSERT INTO KnightSoft_GewinnspielTeilnehmer VALUES(" + result.getString("zaehler") + ", "
                  + result.getString("gewinnspiel_nr") + ", " + result.getString("gewinnspiel_reihung") + ", "
                  + DbSynchron.dbString(result, "sende_infos") + ", " + DbSynchron.dbString(result, "eintrag_gesperrt") + ", "
                  + DbSynchron.dbString(result, "antwort_korrekt") + ", " + DbSynchron.dbString(result, "datum_erstellt") + ", "
                  + DbSynchron.dbString(result, "system") + ", " + DbSynchron.dbString(result, "geschlecht") + ", "
                  + DbSynchron.dbString(result, "name") + ", " + DbSynchron.dbString(result, "vorname") + ", "
                  + DbSynchron.dbString(result, "strasse") + ", " + DbSynchron.dbString(result, "plz") + ", "
                  + DbSynchron.dbString(result, "ort") + ", " + DbSynchron.dbString(result, "email") + ", "
                  + DbSynchron.dbString(result, "antwort1") + ", " + DbSynchron.dbString(result, "antwort2") + ", "
                  + DbSynchron.dbString(result, "antwort3") + ", " + DbSynchron.dbString(result, "antwort4") + ", "
                  + DbSynchron.dbString(result, "antwort5") + ", " + DbSynchron.dbString(result, "antwort6") + ", "
                  + DbSynchron.dbString(result, "antwort7") + ", " + DbSynchron.dbString(result, "antwort8") + ", "
                  + DbSynchron.dbString(result, "antwort9") + ", " + DbSynchron.dbString(result, "antwort10") + ", "
                  + DbSynchron.dbString(result, "gewinnerliste") + ")");
        }
      }

      // Lesezugriff auf die Datenbank (Banner-Tabelle)
      try (final ResultSet result =
          DbSynchron.fromDataBase.createStatement().executeQuery("SELECT * FROM KnightSoft_Banner ORDER BY zaehler")) {
        while (result.next()) {
          DbSynchron.toDataBase.createStatement()
              .executeUpdate("INSERT INTO KnightSoft_Banner VALUES(" + result.getString("zaehler") + ", "
                  + DbSynchron.dbString(result, "banner_html") + ", " + DbSynchron.dbString(result, "url") + ")");
        }
      }

      // Lesezugriff auf die Datenbank (Bannerpartner-Tabelle)
      try (final ResultSet result =
          DbSynchron.fromDataBase.createStatement().executeQuery("SELECT * FROM KnightSoft_Bannerpartner ORDER BY zaehler")) {
        while (result.next()) {
          DbSynchron.toDataBase.createStatement()
              .executeUpdate("INSERT INTO KnightSoft_Bannerpartner VALUES(" + result.getString("zaehler") + ", "
                  + result.getString("banner") + ", " + DbSynchron.dbString(result, "anrede") + ", "
                  + DbSynchron.dbString(result, "firma1") + ", " + DbSynchron.dbString(result, "firma2") + ", "
                  + DbSynchron.dbString(result, "name") + ", " + DbSynchron.dbString(result, "vorname") + ", "
                  + DbSynchron.dbString(result, "strasse") + ", " + DbSynchron.dbString(result, "plz") + ", "
                  + DbSynchron.dbString(result, "ort") + ", " + DbSynchron.dbString(result, "telefon") + ", "
                  + DbSynchron.dbString(result, "email") + ", " + DbSynchron.dbString(result, "bank") + ", "
                  + DbSynchron.dbString(result, "blz") + ", " + DbSynchron.dbString(result, "konto") + ", "
                  + DbSynchron.dbString(result, "user") + ", " + DbSynchron.dbString(result, "passwort") + ")");
        }
      }

      // Lesezugriff auf die Datenbank (Bannerklicks-Tabelle)
      try (final ResultSet result =
          DbSynchron.fromDataBase.createStatement().executeQuery("SELECT * FROM KnightSoft_Bannerklicks WHERE datum_zeit>="
              + de.knightsoft.common.StringToSql.convert(zwDatumBk) + " ORDER BY zaehler")) {
        while (result.next()) {
          DbSynchron.toDataBase.createStatement()
              .executeUpdate("INSERT INTO KnightSoft_Bannerklicks VALUES(" + result.getString("zaehler") + ", "
                  + DbSynchron.dbString(result, "bannerpartner") + ", " + DbSynchron.dbString(result, "useragent") + ", "
                  + DbSynchron.dbString(result, "ip") + ", " + DbSynchron.dbString(result, "datum_zeit") + ")");
        }
      }

      // Lesezugriff auf die Datenbank (Mail-Tabelle)
      try (final ResultSet result =
          DbSynchron.fromDataBase.createStatement().executeQuery("SELECT * FROM KnightSoft_Mail ORDER BY zaehler")) {
        while (result.next()) {
          DbSynchron.toDataBase.createStatement()
              .executeUpdate("INSERT INTO KnightSoft_Mail VALUES(" + result.getString("zaehler") + ", "
                  + DbSynchron.dbString(result, "provider") + ", " + DbSynchron.dbString(result, "email") + ")");
        }
      }

      // Lesezugriff auf die Datenbank (TippLiga-Tabelle)
      try (final ResultSet result = DbSynchron.fromDataBase.createStatement()
          .executeQuery("SELECT * FROM KnightSoft_TippLiga WHERE saison_liga>" + zwSaisonMin + " ORDER BY saison_liga")) {
        while (result.next()) {
          DbSynchron.toDataBase.createStatement()
              .executeUpdate("INSERT INTO KnightSoft_TippLiga VALUES(" + result.getString("saison_liga") + ", "
                  + DbSynchron.dbString(result, "eintrag_gesperrt") + ", " + DbSynchron.dbString(result, "bezeichnung") + ", "
                  + result.getString("anz_manschaften") + ", " + result.getString("anz_tab_oben1") + ", "
                  + result.getString("anz_tab_oben2") + ", " + result.getString("anz_tab_unten1") + ", "
                  + result.getString("anz_tab_unten2") + ", " + result.getString("punkte_sieg") + ", "
                  + result.getString("punkte_unent") + ", " + result.getString("punkte_nieder") + ", "
                  + result.getString("tr_punkte_exakt") + ", " + result.getString("tr_punkte_torver") + ", "
                  + result.getString("tr_punkte_toto") + ", " + result.getString("tr_punkte_falsch") + ")");
        }
      }

      // Lesezugriff auf die Datenbank (TippLigaMannschaften-Tabelle)
      try (final ResultSet result = DbSynchron.fromDataBase.createStatement()
          .executeQuery("SELECT * FROM KnightSoft_TippLigaMannschaft WHERE saison_liga>" + zwSaisonMin
              + " ORDER BY saison_liga, lfd_liga_ms")) {
        while (result.next()) {
          DbSynchron.toDataBase.createStatement()
              .executeUpdate("INSERT INTO KnightSoft_TippLigaMannschaft VALUES(" + result.getString("saison_liga") + ", "
                  + result.getString("lfd_liga_ms") + ", " + result.getString("zaehler") + ")");
        }
      }

      // Lesezugriff auf die Datenbank (TippMannschaften-Tabelle)
      try (final ResultSet result = DbSynchron.fromDataBase.createStatement()
          .executeQuery("SELECT * FROM KnightSoft_TippMannschaften ORDER BY zaehler")) {
        while (result.next()) {
          DbSynchron.toDataBase.createStatement().executeUpdate("INSERT INTO KnightSoft_TippMannschaften VALUES("
              + result.getString("zaehler") + ", " + DbSynchron.dbString(result, "bezeichnung") + ")");
        }
      }

      // Lesezugriff auf die Datenbank (TippSpiel-Tabelle)
      try (final ResultSet result =
          DbSynchron.fromDataBase.createStatement().executeQuery("SELECT * FROM KnightSoft_TippSpiele WHERE saison_liga>"
              + zwSaisonMin + " ORDER BY saison_liga, spieltag, lfd_spiele_nr")) {
        while (result.next()) {
          DbSynchron.toDataBase.createStatement()
              .executeUpdate("INSERT INTO KnightSoft_TippSpiele VALUES(" + result.getString("saison_liga") + ", "
                  + result.getString("spieltag") + ", " + result.getString("lfd_spiele_nr") + ", "
                  + result.getString("mannschaft_heim") + ", " + result.getString("mannschaft_ausw") + ", "
                  + result.getString("tore_heim") + ", " + result.getString("tore_ausw") + ", "
                  + DbSynchron.dbString(result, "spielbeginn") + ", " + DbSynchron.dbString(result, "spiel_beendet") + ")");
        }
      }

      // Lesezugriff auf die Datenbank (TippMitspieler-Tabelle)
      try (final ResultSet result = DbSynchron.fromDataBase.createStatement().executeQuery(
          "SELECT * FROM KnightSoft_TippMitspieler WHERE saison_liga>" + zwSaisonMin + " ORDER BY saison_liga, spitzname")) {
        while (result.next()) {
          DbSynchron.toDataBase.createStatement()
              .executeUpdate("INSERT INTO KnightSoft_TippMitspieler VALUES(" + result.getString("saison_liga") + ", "
                  + DbSynchron.dbString(result, "spitzname") + ", " + DbSynchron.dbString(result, "passwort") + ", "
                  + DbSynchron.dbString(result, "sende_infos") + ", " + DbSynchron.dbString(result, "eintrag_gesperrt") + ", "
                  + DbSynchron.dbString(result, "datum_erstellt") + ", " + DbSynchron.dbString(result, "datum_geaendert") + ", "
                  + DbSynchron.dbString(result, "geschlecht") + ", " + DbSynchron.dbString(result, "name") + ", "
                  + DbSynchron.dbString(result, "vorname") + ", " + DbSynchron.dbString(result, "strasse") + ", "
                  + DbSynchron.dbString(result, "plz") + ", " + DbSynchron.dbString(result, "ort") + ", "
                  + DbSynchron.dbString(result, "email") + ", " + result.getString("anzahl_fehllogin") + ", "
                  + DbSynchron.dbString(result, "datum_fehllogin") + ", " + DbSynchron.dbString(result, "datum_login") + ")");
        }
      }

      // Lesezugriff auf die Datenbank (TippSpielTipp-Tabelle)
      try (final ResultSet result =
          DbSynchron.fromDataBase.createStatement().executeQuery("SELECT * FROM KnightSoft_TippSpieleTipp WHERE saison_liga>"
              + zwSaisonMin + " ORDER BY saison_liga, spieltag, spitzname, lfd_spiele_nr")) {
        while (result.next()) {
          DbSynchron.toDataBase.createStatement()
              .executeUpdate("INSERT INTO KnightSoft_TippSpieleTipp VALUES(" + result.getString("saison_liga") + ", "
                  + result.getString("spieltag") + ", " + result.getString("lfd_spiele_nr") + ", "
                  + DbSynchron.dbString(result, "spitzname") + ", " + result.getString("tore_heim") + ", "
                  + result.getString("tore_ausw") + ")");
        }
      }

      // Lesezugriff auf die Datenbank (Berechtigungen-Tabelle)
      try (final ResultSet result = DbSynchron.fromDataBase.createStatement()
          .executeQuery("SELECT * FROM KnightSoft_Berechtigungen ORDER BY system, user")) {
        while (result.next()) {
          DbSynchron.toDataBase.createStatement()
              .executeUpdate("INSERT INTO KnightSoft_Berechtigungen VALUES(" + DbSynchron.dbString(result, "system") + ", "
                  + DbSynchron.dbString(result, "user") + ", " + DbSynchron.dbString(result, "passwort") + ", "
                  + result.getString("stufe") + ", " + result.getString("anzahl_fehllogin") + ", "
                  + DbSynchron.dbString(result, "datum_fehllogin") + ", " + DbSynchron.dbString(result, "datum_login") + ")");
        }
      }
    } catch (final ClassNotFoundException e) {
      System.err.println("de.knightsoft.common.TextException: " + e.toString());
    } catch (final java.sql.SQLException e) {
      System.err.println("SQL-Fehler: " + e.toString());
    }
  }
}
