/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.knightsoftnet.common;

import de.knightsoft.common.DataBaseDepending;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.SimpleEmail;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

/**
 * The <code>LoescheUnfreie</code> class is based on the abstract class de.knightsoft.common.AbstractVisualDb and implements
 * some functions used by the cron job: - nach 10 Tagen noch nicht freigeschalten, erhält der Eintragende eine E-Mail mit der
 * Aufforderung, die Eintragung vorzunehmen. - nach 20 Tagen wird nochmals eine E-Mail versendet mit deutlicherer Betonung, dass
 * die Eintragung nach 30 Tagen gelöscht wird. - nach 30 Tagen wird die Eintragung gelöscht und eine E-Mail an den Eintragenden
 * verschickt, dass die Eintragung entfernt wurde. Desweiteren wird eine statistische Auswertung der KnightSoft-Datenbank an die
 * KnightSoft Geschäfts- führer. Zuguterletzt werden per de.knightsoft.common.SendNewsServlet erstellte E-Mail, die bis dahin
 * nur in einer zwi- schenspeicher-DB lagen, tatsächlich verschickt. Je Provider werden dabei maximal 20 Mails pro Tag
 * verschickt, um nicht in Spamingfilter zu geraten.
 *
 * @author Manfred Tremmel
 * @version 2.0.0, 15.08.2006
 */

public class LoescheUnfreie extends de.knightsoft.common.AbstractVisualDb {

  private String readUserSql;

  /**
   * Constructor.
   *
   * @param thisDatabase Connection to database
   * @param servletname Name of the Servlet
   * @exception SQLException if a database access error occurs
   * @exception de.knightsoft.common.TextException to give out a text message
   * @see de.knightsoft.common.TextException
   */
  public LoescheUnfreie(final Connection thisDatabase, final String servletname)
      throws java.sql.SQLException, de.knightsoft.common.TextException {
    super(thisDatabase, KnConst.HTML_BASE, servletname, KnConst.GIF_URL + "16x16/db.png", "LoescheUnfreie", "LoescheUnfreie");
    try {
      final DataBaseDepending myDataBaseDepending = new DataBaseDepending(myDataBase.getMetaData().getDatabaseProductName());

      readUserSql = "SELECT        stufe " //
          + "FROM            KnightSoft_TippMitspieler " //
          + "WHERE        Mandator = 1 AND " //
          + "                spitzname = ? AND " //
          + "                passwort = " + myDataBaseDepending.getSqlPassword(" ? ");
    } catch (final Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * The Method <code>sendEMailHinweis2</code> is called when entries are made and 10 days later no one has freed the entry.
   *
   * @param result resultset to read sql data from
   * @param service Service name to do it for
   */
  private void sendEMailHinweis1(final ResultSet result, final String service) throws java.sql.SQLException, IOException {
    final StringBuilder emailText = new StringBuilder(512);
    final String geschlecht = result.getString("geschlecht");
    final String name = result.getString("name");
    final String eMail = result.getString("email");
    final String datumEintrag = result.getString("datum_erstellt");

    if ("W".equals(geschlecht)) {
      emailText.append("Sehr geehrte Frau ");
    } else {
      emailText.append("Sehr geehrter Herr ");
    }

    emailText.append(name + ",\n\n" + "Sie haben sich am " + datumEintrag.substring(8, 10) + "." + datumEintrag.substring(5, 7)
        + "." + datumEintrag.substring(0, 4) + " in unseren " + service + "-Service\n"
        + "eingetragen, dies liegt nun zehn Tage zurück. Ihr Eintrag ist nach\n"
        + "wie vor gesperrt. Bitte schalten Sie Ihren Eintrag mit dem zuge-\n" + "sandten Passwort frei.\n"
        + "Sollten Sie kein Passwort erhalten haben, können Sie ein neues\n" + "auf der \"Login\"-Seite anfordern.\n"
        + "Sollten Sie Probleme bei der Freischaltung haben, wenden Sie\n"
        + "sich bitte an uns. Wir stehen Ihnen gerne zur Verfügung.\n\n" + "Mit freundlichen Grüssen\n\n"
        + "Ihr KnightSoft-Team");

    new de.knightsoft.common.SendEMail(de.knightsoft.common.Constants.EMAIL, de.knightsoft.common.Constants.ORGANISATION, eMail,
        "Ihre Eintragung bei unserem " + service + "-Service unter www.knightsoft-net.de", emailText.toString());
  }

  /**
   * The Method <code>sendEMailHinweis2</code> is called when entries are made and 20 days later no one has freed the entry.
   *
   * @param result resultset to read sql data from
   * @param service Service name to do it for
   */
  private void sendEMailHinweis2(final ResultSet result, final String service) throws java.sql.SQLException, IOException {
    final StringBuilder emailText = new StringBuilder(768);
    final String geschlecht = result.getString("geschlecht");
    final String name = result.getString("name");
    final String eMail = result.getString("email");
    final String datumEintrag = result.getString("datum_erstellt");

    if ("W".equals(geschlecht)) {
      emailText.append("Sehr geehrte Frau ");
    } else {
      emailText.append("Sehr geehrter Herr ");
    }

    emailText.append(name + ",\n\n" + "Sie haben sich am " + datumEintrag.substring(8, 10) + "." + datumEintrag.substring(5, 7)
        + "." + datumEintrag.substring(0, 4) + " in unseren " + service + "-Service\n"
        + "eingetragen, dies liegt nun 20 Tage zurück. Ihr Eintrag ist nach\n"
        + "wie vor gesperrt. Bitte schalten Sie Ihren Eintrag mit dem zuge-\n"
        + "sandten Passwort frei. Beachten Sie bitte, dass Eintrag 30 Tage\n"
        + "nach Ihrer Erfassung gelöscht werden, sollten sie noch nicht frei-\n" + "geschalten sein.\n"
        + "Sollten Sie kein Passwort erhalten haben, können Sie ein neues\n" + "auf der \"Login\"-Seite anfordern.\n"
        + "Sollten Sie Probleme bei der Freischaltung haben, wenden Sie\n"
        + "sich bitte an uns. Wir stehen Ihnen gerne zur Verfügung.\n\n" + "Mit freundlichen Grüssen\n\n" + "Manfred Tremmel");

    new de.knightsoft.common.SendEMail(de.knightsoft.common.Constants.EMAIL, de.knightsoft.common.Constants.ORGANISATION, eMail,
        "Ihre Eintragung bei unserem " + service + "-Service unter www.knightsoft-net.de", emailText.toString());
  }

  /**
   * The Method <code>sendEMailHinweis3</code> is called when entries are made and no one has freed the entry, it is deleted
   * after 30 days.
   *
   * @param result resultset to read sql data from
   * @param service Service name to do it for
   */
  private void sendEMailHinweis3(final ResultSet result, final String service) throws java.sql.SQLException, IOException {
    final StringBuilder emailText = new StringBuilder(512);
    final String geschlecht = result.getString("geschlecht");
    final String name = result.getString("name");
    final String eMail = result.getString("email");
    final String datumEintrag = result.getString("datum_erstellt");

    if ("W".equals(geschlecht)) {
      emailText.append("Sehr geehrte Frau ");
    } else {
      emailText.append("Sehr geehrter Herr ");
    }

    emailText.append(name + ",\n\n" + "Sie haben sich am " + datumEintrag.substring(8, 10) + "." + datumEintrag.substring(5, 7)
        + "." + datumEintrag.substring(0, 4) + " in unseren " + service + "-Service\n"
        + "eingetragen, dies liegt nun 30 Tage zurück. Ihr Eintrag wurde\n"
        + "bis Heute nicht freigeschalten. Wir müssen also davon ausgehen,\n"
        + "dass die eingetragenen Daten fehlerhaft sind, oder kein Interesse\n" + "an unserem Service besteht.\n"
        + "Ihr Eintrag wurde deshalb aus unserer Datenbank gelöscht.\n\n" + "Mit freundlichen Grüssen\n\n" + "Manfred Tremmel");

    new de.knightsoft.common.SendEMail(de.knightsoft.common.Constants.EMAIL, de.knightsoft.common.Constants.ORGANISATION, eMail,
        "Loeschung Ihres Eintrags bei unserem " + service + "-Service unter www.knightsoft-net.de", emailText.toString());
  }

  /**
   * The Method <code>sendEMailHinweis4</code> is called for outdated entries in the fahrgemeinschaften database.
   *
   * @param result resultset to read sql data from
   * @param service Service name to do it for
   */
  private void sendEMailHinweis4(final ResultSet result, final String service) throws java.sql.SQLException, IOException {
    final StringBuilder emailText = new StringBuilder(512);
    final String geschlecht = result.getString("geschlecht");
    final String name = result.getString("name");
    final String eMail = result.getString("email");
    final String datumEintrag = result.getString("datum_erstellt");

    if ("W".equals(geschlecht)) {
      emailText.append("Sehr geehrte Frau ");
    } else {
      emailText.append("Sehr geehrter Herr ");
    }

    emailText.append(name + ",\n\n" + "Ihre am " + datumEintrag.substring(8, 10) + "." + datumEintrag.substring(5, 7) + "."
        + datumEintrag.substring(0, 4) + " in unseren " + service + "-Service\n"
        + "eingetragene, Mitfahrgelegenheit ist nicht mehr gültig und wurde\n" + "aus der Datenbank gelöscht.\n"
        + "Mit freundlichen Grüssen\n\n" + "Manfred Tremmel");

    new de.knightsoft.common.SendEMail(de.knightsoft.common.Constants.EMAIL, de.knightsoft.common.Constants.ORGANISATION, eMail,
        "Loeschung Ihres Eintrags bei unserem " + service + "-Service unter www.knightsoft-net.de", emailText.toString());
  }

  /**
   * The Method <code>sendEMailStatistik</code> creats a E-Mail with the database statistics and sends it to me.
   *
   * @param datumGestern string with the DateField of yesterday
   * @param anzGeloeschtBs deleted babysitter entries
   * @param anzGeloeschtNh deleted nachhilfe entries
   * @param anzGeloeschtFg deleted fahrgemeinschaften entries
   */
  private void sendEmailStatistik(final String datumGestern, final int anzGeloeschtBs, final int anzGeloeschtNh, //
      final int anzGeloeschtFg) throws java.sql.SQLException, IOException, EmailException {
    final String datumGesternFormatiert =
        datumGestern.substring(8, 10) + "." + datumGestern.substring(5, 7) + "." + datumGestern.substring(0, 4);

    final HtmlEmail email = new HtmlEmail();
    email.setFrom(de.knightsoft.common.Constants.EMAIL);
    email.addTo("MTremmel@knightsoft-net.de", "Manfred Tremmel");
    email.setCharset("iso-8859-1");
    email.setSubject("KnightSoft-DB-Statistik " + datumGesternFormatiert);
    email.setHtmlMsg(de.knightsoft.knightsoftnet.common.DbStatistik.htmlDbStatistic(myDataBase, datumGestern, anzGeloeschtBs,
        anzGeloeschtNh, anzGeloeschtFg));
    email.setTextMsg("Mail ohne HTML, sorry da is nix drin");
    new de.knightsoft.common.SendEMail(email);
  }

  /**
   * The Method <code>doTheWork</code> coordinates all the work in this class
   *
   * @param req HttpServletRequest from the Servlet
   * @param res HttpServletResponse from the Servlet
   * @param session The Data of the current Session
   * @return html code of the formular
   * @exception de.knightsoft.common.TextException when a error in checking the passwords occours
   */
  @Override
  public String doTheWork(final HttpServletRequest req, final HttpServletResponse res, final HttpSession session)
      throws de.knightsoft.common.TextException {

    String providerMerker = StringUtils.EMPTY;
    final String formular = StringUtils.EMPTY;
    String tagesdatumm1;
    String tagesdatumm10;
    String tagesdatumm20;
    String tagesdatumm30;
    int anzGeloeschtBs = 0;
    int anzGeloeschtNh = 0;
    int anzGeloeschtFg = 0;
    int pos = 0;
    final java.util.Date jetzt = new java.util.Date();

    try {
      final String user = req.getParameter("user");
      final String password = req.getParameter("password");
      int stufe = -1;

      if (user == null || StringUtils.EMPTY.equals(user) || password == null || StringUtils.EMPTY.equals(password)) {
        return "<h2>Sie haben keine Berechtigung f&uuml;r den DB-Admin-Bereich</h2>\n";
      }

      try (final PreparedStatement readUserSQLStatement = myDataBase.prepareStatement(readUserSql)) {
        readUserSQLStatement.clearParameters();
        readUserSQLStatement.setString(1, user);
        readUserSQLStatement.setString(2, password);
        try (final ResultSet resultHead = readUserSQLStatement.executeQuery()) {
          if (resultHead.next()) {
            stufe = resultHead.getInt("stufe");
          }
        }
      }

      if (stufe <= 5) {
        return "<h2>Sie haben keine Berechtigung f&uuml;r den DB-Admin-Bereich</h2>\n";
      } else {
        final GregorianCalendar myCalendar = new GregorianCalendar();
        myCalendar.setTimeZone(TimeZone.getTimeZone("ECT"));
        final SimpleDateFormat myDf = new java.text.SimpleDateFormat("yyyy-MM-dd");

        // Kalender auf aktuelle Uhrzeit/Datum setzen
        myCalendar.setTime(jetzt);
        // Datum um einen Tage verringern (Tagesdatum -1)
        myCalendar.add(Calendar.DATE, -1);
        tagesdatumm1 = myDf.format(myCalendar.getTime());

        // Datum um weitere 9 Tage verringern (Tagesdatum -10)
        myCalendar.add(Calendar.DATE, -9);
        tagesdatumm10 = myDf.format(myCalendar.getTime());

        // Datum um weitere 10 Tage verringern (Tagesdatum -20)
        myCalendar.add(Calendar.DATE, -10);
        tagesdatumm20 = myDf.format(myCalendar.getTime());

        // Datum um weitere 10 Tage verringern (Tagesdatum -30)
        myCalendar.add(Calendar.DATE, -10);
        tagesdatumm30 = myDf.format(myCalendar.getTime());

        // Lesezugriff auf die Datenbank alle 10 Tage alten gesperrten Eintragungen (Babysitter)
        try (
            final ResultSet result = myDataBase.createStatement().executeQuery("SELECT geschlecht, name, email, datum_erstellt "
                + "FROM KnightSoft_BabySitter " + "WHERE eintrag_gesperrt='J' AND datum_erstellt='" + tagesdatumm10 + "'")) {
          while (result.next()) {
            sendEMailHinweis1(result, "Babysitter");
          }
        }

        // Lesezugriff auf die Datenbank alle 10 Tage alten gesperrten Eintragungen (Nachhilfe)
        try (
            final ResultSet result = myDataBase.createStatement().executeQuery("SELECT geschlecht, name, email, datum_erstellt "
                + "FROM KnightSoft_Nachhilfe " + "WHERE eintrag_gesperrt='J' AND datum_erstellt='" + tagesdatumm10 + "'")) {
          while (result.next()) {
            sendEMailHinweis1(result, "Nachhilfe");
          }
        }

        // Lesezugriff auf die Datenbank alle 10 Tage alten gesperrten Eintragungen
        // (Fahrgemeinschaften)
        try (final ResultSet result = myDataBase.createStatement()
            .executeQuery("SELECT geschlecht, name, email, datum_erstellt " + "FROM KnightSoft_Fahrgemeinschaft "
                + "WHERE eintrag_gesperrt='J' AND datum_erstellt='" + tagesdatumm10 + "'")) {
          while (result.next()) {
            sendEMailHinweis1(result, "Fahrgemeinschaften");
          }
        }

        // Lesezugriff auf die Datenbank alle 20 Tage alten gesperrten Eintragungen (Babysitter)
        try (
            final ResultSet result = myDataBase.createStatement().executeQuery("SELECT geschlecht, name, email, datum_erstellt "
                + "FROM KnightSoft_BabySitter " + "WHERE eintrag_gesperrt='J' AND datum_erstellt='" + tagesdatumm20 + "'")) {
          while (result.next()) {
            sendEMailHinweis2(result, "Babysitter");
          }
        }

        // Lesezugriff auf die Datenbank alle 20 Tage alten gesperrten Eintragungen (Nachhilfe)
        try (
            final ResultSet result = myDataBase.createStatement().executeQuery("SELECT geschlecht, name, email, datum_erstellt "
                + "FROM KnightSoft_Nachhilfe " + "WHERE eintrag_gesperrt='J' AND datum_erstellt='" + tagesdatumm20 + "'")) {
          while (result.next()) {
            sendEMailHinweis2(result, "Nachhilfe");
          }
        }

        // Lesezugriff auf die Datenbank alle 20 Tage alten gesperrten Eintragungen
        // (Fahrgemeinschaften)
        try (final ResultSet result = myDataBase.createStatement()
            .executeQuery("SELECT geschlecht, name, email, datum_erstellt " + "FROM KnightSoft_Fahrgemeinschaft "
                + "WHERE eintrag_gesperrt='J' AND datum_erstellt='" + tagesdatumm20 + "'")) {
          while (result.next()) {
            sendEMailHinweis2(result, "Fahrgemeinschaften");
          }
        }

        // Lesezugriff auf die Datenbank alle 30 Tage alten gesperrten Eintragungen (Babysitter)
        try (
            final ResultSet result = myDataBase.createStatement().executeQuery("SELECT geschlecht, name, email, datum_erstellt "
                + "FROM KnightSoft_BabySitter " + "WHERE eintrag_gesperrt='J' AND datum_erstellt<='" + tagesdatumm30 + "'")) {
          while (result.next()) {
            sendEMailHinweis3(result, "Babysitter");
          }
        }
        anzGeloeschtBs = myDataBase.createStatement().executeUpdate(
            "DELETE FROM KnightSoft_BabySitter WHERE eintrag_gesperrt='J' AND datum_erstellt<='" + tagesdatumm30 + "'");

        // Lesezugriff auf die Datenbank alle 30 Tage alten gesperrten Eintragungen (Nachhilfe)
        try (
            final ResultSet result = myDataBase.createStatement().executeQuery("SELECT geschlecht, name, email, datum_erstellt "
                + "FROM KnightSoft_Nachhilfe WHERE eintrag_gesperrt='J' AND datum_erstellt<='" + tagesdatumm30 + "'")) {
          while (result.next()) {
            sendEMailHinweis3(result, "Nachhilfe");
          }
        }
        anzGeloeschtNh = myDataBase.createStatement().executeUpdate(
            "DELETE FROM KnightSoft_Nachhilfe WHERE eintrag_gesperrt='J' AND datum_erstellt<='" + tagesdatumm30 + "'");

        // Lesezugriff auf die Datenbank alle 30 Tage alten gesperrten Eintragungen
        // (Fahrgemeinschaften)
        try (final ResultSet result = myDataBase.createStatement()
            .executeQuery("SELECT geschlecht, name, email, datum_erstellt "
                + "FROM KnightSoft_Fahrgemeinschaft WHERE eintrag_gesperrt='J' " + "AND datum_erstellt<='" + tagesdatumm30
                + "'")) {
          while (result.next()) {
            sendEMailHinweis3(result, "Fahrgemeinschaften");
          }
        }
        anzGeloeschtFg = myDataBase.createStatement().executeUpdate("DELETE FROM KnightSoft_Fahrgemeinschaft "
            + "WHERE eintrag_gesperrt='J' AND datum_erstellt<='" + tagesdatumm30 + "'");

        // 12.03.2000: Einmalige Mitfahrgelegenheiten 10 Tage nach Ihrem Ablauf löschen
        try (final ResultSet result = myDataBase.createStatement()
            .executeQuery("SELECT * FROM KnightSoft_Fahrgemeinschaft "
                + "WHERE einmalige_fahrt='J' AND ((rueckfahrt='N' AND abfahrtsdatum<'" + tagesdatumm1
                + "') OR (rueckfahrt='J' AND rueckfahrtsdatum<'" + tagesdatumm1 + "'))")) {
          while (result.next()) {
            sendEMailHinweis4(result, "Fahrgemeinschaften");
          }
        }
        anzGeloeschtFg += myDataBase.createStatement()
            .executeUpdate("DELETE FROM KnightSoft_Fahrgemeinschaft WHERE einmalige_fahrt='J' "
                + "AND ((rueckfahrt='N' AND abfahrtsdatum<'" + tagesdatumm1 + "') OR (rueckfahrt='J' AND rueckfahrtsdatum<'"
                + tagesdatumm1 + "'))");

        // EMails aus der E-Mail-Datenbank verschicken
        try (final ResultSet result =
            myDataBase.createStatement().executeQuery("SELECT * FROM KnightSoft_Mail ORDER BY provider")) {
          while (result.next()) {
            if (providerMerker.equalsIgnoreCase(result.getString("provider"))) {
              pos++;
            } else {
              pos = 1;
              providerMerker = result.getString("provider");
            }
            if (pos <= 20) {
              new de.knightsoft.common.SendEMail(createEmailContextFromString(result.getString("email")));
              myDataBase.createStatement()
                  .executeUpdate("DELETE FROM KnightSoft_Mail WHERE zaehler=" + result.getString("zaehler"));
            }
          }
        }

        // E-Mail mit Statistik verschicken
        sendEmailStatistik(tagesdatumm1, anzGeloeschtBs, anzGeloeschtNh, anzGeloeschtFg);

        // 14.10.2000: Nicht gefüllte Tippeintragungen entfernen
        try (final ResultSet result = myDataBase.createStatement()
            .executeQuery("SELECT        saison_liga, " + "                spieltag, " + "                spitzname, "
                + "                SUM(if (tore_heim is null,0,1)) as anzahl_nicht_null "
                + "FROM            KnightSoft_TippSpieleTipp " + "GROUP BY    saison_liga, spieltag, spitzname")) {
          while (result.next()) {
            if (result.getInt("anzahl_nicht_null") == 0) {
              myDataBase.createStatement()
                  .executeUpdate("DELETE FROM    KnightSoft_TippSpieleTipp " + "WHERE            saison_liga="
                      + result.getString("saison_liga") + " AND " + "                spieltag=" + result.getString("spieltag")
                      + " AND " + "                spitzname='" + result.getString("spitzname") + "'");
            }
          }
        }
      }
    } catch (final SQLException e) {
      throw new de.knightsoft.common.TextException("SQL-Fehler:\n" + e.getMessage(), e);
    } catch (final IOException e) {
      throw new de.knightsoft.common.TextException("IO-Fehler:\n" + e.getMessage(), e);
    } catch (final EmailException e) {
      throw new de.knightsoft.common.TextException("eMail-Fehler:\n" + e.getMessage(), e);
    }

    return formular;
  }

  private Email createEmailContextFromString(final String mailString) throws EmailException {
    String from = "";
    String to = "";
    String subject = "";
    final StringBuilder mailtext = new StringBuilder();
    boolean mailtextStarted = false;
    for (final String line : StringUtils.split(mailString, '\n')) {
      if (mailtextStarted) {
        if (mailtext.length() > 0) {
          mailtext.append('\n');
        }
        mailtext.append(line);
      } else if (StringUtils.startsWith(line, "Content-Transfer-Encoding:")) {
        mailtextStarted = true;
      } else if (StringUtils.startsWith(line, "From: ")) {
        from = StringUtils.substring(line, 6);
      } else if (StringUtils.startsWith(line, "To: ")) {
        to = StringUtils.substring(line, 4);
      } else if (StringUtils.startsWith(line, "Subject: ")) {
        subject = StringUtils.substring(line, 9);
      }
    }
    final SimpleEmail email = new SimpleEmail();
    email.setFrom(from);
    email.addTo(to);
    email.setCharset("utf-8");
    email.setSubject(subject);
    email.setMsg(mailtext.toString());
    return email;
  }
}
