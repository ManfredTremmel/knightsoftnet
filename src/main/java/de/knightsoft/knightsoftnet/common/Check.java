/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.knightsoftnet.common;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class Check {
  public int pos;

  public String email;
  public String geschlecht;
  public String name;
  public String vorname;
  public String strasse;
  public String plz;
  public String ort;
  public String rufnummer;
  public String weitereNummer1;
  public String weitereNummer2;

  public String bemerkung;
  public String news;
  public String stufe;
  public String passwort;
  public String url;

  public String land;
  public String bietesuche;

  /**
   * Konstruktor aus dem Bild auslesen.
   */
  public Check() {
    email = StringUtils.EMPTY;
    passwort = StringUtils.EMPTY;
    geschlecht = "M";
    name = StringUtils.EMPTY;
    vorname = StringUtils.EMPTY;
    strasse = StringUtils.EMPTY;
    plz = StringUtils.EMPTY;
    ort = StringUtils.EMPTY;
    rufnummer = StringUtils.EMPTY;
    weitereNummer1 = StringUtils.EMPTY;
    weitereNummer2 = StringUtils.EMPTY;

    bemerkung = StringUtils.EMPTY;
    news = "N";
    stufe = "1";
    url = StringUtils.EMPTY;

    land = "de";
    bietesuche = "b";
  }

  /**
   * Konstruktor aus dem Bild auslesen.
   *
   * @param req HttpServletRequest
   */
  public Check(final HttpServletRequest req) {
    email = req.getParameter("email");
    passwort = req.getParameter("Passwort");
    geschlecht = req.getParameter("Geschlecht");
    name = req.getParameter("Nname");
    vorname = req.getParameter("Vorname");
    strasse = req.getParameter("Strasse");
    plz = req.getParameter("PLZ");
    ort = req.getParameter("Ort");
    rufnummer = req.getParameter("Rufnummer");
    weitereNummer1 = req.getParameter("weitereNummer1");
    weitereNummer2 = req.getParameter("weitereNummer2");

    bemerkung = req.getParameter("Bemerkung");
    news = req.getParameter("News");
    stufe = req.getParameter("Stufe");
    url = req.getParameter("url");

    land = req.getParameter("land");
    bietesuche = req.getParameter("bietesuche");
  }

  /**
   * Konstruktor aus der Datenbank auslesen.
   *
   * @param result ResultSet
   */
  public Check(final ResultSet result) throws SQLException {
    email = result.getString("email");
    geschlecht = result.getString("geschlecht");
    name = result.getString("name");
    vorname = result.getString("vorname");
    strasse = result.getString("strasse");
    plz = result.getString("plz");
    ort = result.getString("ort");
    rufnummer = result.getString("telefonnummer1");
    weitereNummer1 = result.getString("telefonnummer2");
    if (weitereNummer1 == null) {
      weitereNummer1 = StringUtils.EMPTY;
    }

    weitereNummer2 = result.getString("telefonnummer3");
    if (weitereNummer2 == null) {
      weitereNummer2 = StringUtils.EMPTY;
    }

    bemerkung = result.getString("bemerkung");
    if (bemerkung == null) {
      bemerkung = StringUtils.EMPTY;
    }

    url = result.getString("url");
    if (url == null) {
      url = StringUtils.EMPTY;
    }

    news = result.getString("sende_infos");

    land = result.getString("land");
    bietesuche = result.getString("bietesuche");
  }

  /**
   * Wert auf nummerisch Prüfen.
   *
   * @param value value to check
   * @return true if the value is numeric
   */
  public boolean checkNummerisch(final String value) {
    boolean nummerisch = true;

    for (pos = 0; pos < value.length() && nummerisch; nummerisch = Character.isDigit(value.charAt(pos++))) {
      ;
    }

    return nummerisch;
  }

  /**
   * J/N auf Ja/Nein umschlüsseln.
   *
   * @param wert the parameter to check
   * @return if parameter is "J" it returns "Ja" otherwise "Nein"
   */
  public String jnAusgabe(final String wert) {
    if ("J".equals(wert)) {
      return "Ja";
    } else {
      return "Nein";
    }
  }

  /**
   * W/M auf weiblich/männlich umschlüsseln.
   *
   * @param wert the parameter to check
   * @return if parameter is "M" it returns m&auml;nnlich, otherwise weiblich
   */
  public String wmAusgabe(final String wert) {
    if ("M".equals(wert)) {
      return "m&auml;nnlich";
    } else {
      return "weiblich";
    }
  }

  /**
   * b/s auf Biete/Suche umschlüsseln.
   *
   * @param wert the parameter to check
   * @return if parameter is "b" it returns "Ich biete an", otherwise "Ich bin auf der Suche"
   */
  public String bsAusgabe(final String wert) {
    if ("b".equals(wert)) {
      return "Ich biete an";
    } else {
      return "Ich bin auf der Suche";
    }
  }

  /**
   * Land umschluesseln.
   *
   * @param wert the parameter to check
   * @return if paramter is "at" it returns "&Ouml;sterreich", "ch" is transfered to "Schweiz", everything else is transfered to
   *         "Deutschland"
   */
  public String landAusgabe(final String wert) {
    if ("at".equals(wert)) {
      return "&Ouml;sterreich";
    } else if ("ch".equals(wert)) {
      return "Schweiz";
    } else {
      return "Deutschland";
    }
  }

  /**
   * Jahreseingabe Pruefen.
   *
   * @param pjahr year to check
   * @param zukuenftigeErlaubt future is allowed?
   * @param vergangenErlaubt past is allowed?
   * @return year as string
   */
  public String checkJahr(final String pjahr, final boolean zukuenftigeErlaubt, final boolean vergangenErlaubt)
      throws de.knightsoft.common.TextException {
    String jahr = pjahr;
    if (StringUtils.isNotEmpty(jahr)) {
      if (!checkNummerisch(jahr)) {
        throw new de.knightsoft.common.TextException("Das eingegebene Jahr ist nicht nummerisch!");
      }

      final GregorianCalendar myCalendar = new GregorianCalendar();
      myCalendar.setTimeZone(TimeZone.getTimeZone("ECT"));
      final java.util.Date jetzt = new java.util.Date();
      myCalendar.setTime(jetzt);
      final int diesesjahr = myCalendar.get(Calendar.YEAR);
      int eingabejahr = Integer.parseInt(jahr);

      switch (jahr.length()) {
        case 2:
          jahr = Integer.toString(diesesjahr).substring(0, 2) + jahr; // NOPMD
          eingabejahr = Integer.parseInt(jahr);
          if (eingabejahr > diesesjahr && !zukuenftigeErlaubt) {
            eingabejahr -= 100;
            jahr = Integer.toString(eingabejahr);
          }
          if (eingabejahr < diesesjahr && !vergangenErlaubt) {
            eingabejahr += 100;
            jahr = Integer.toString(eingabejahr);
          }
          break;
        case 4:
          if (eingabejahr > diesesjahr && !zukuenftigeErlaubt) {
            throw new de.knightsoft.common.TextException(
                "Das eingegebene Jahr liegt in der Vergangenheit, dies ist nicht erlaubt");
          }
          if (eingabejahr < diesesjahr && !vergangenErlaubt) {
            throw new de.knightsoft.common.TextException("Das eingegebene Jahr liegt in der Zukunft, dies ist nicht erlaubt");
          }
          break;
        default:
          throw new de.knightsoft.common.TextException("Das Jahr muss vierstellig angegeben werden!");
          // break;
      }
    }

    return jahr;
  }

  /**
   * Daten Pruefen.
   */
  public void checkDaten() throws de.knightsoft.common.TextException {
    int posI;
    int atCheck;
    int pointCheck;

    if (email == null || email.equals(StringUtils.EMPTY)) {
      throw new de.knightsoft.common.TextException("Sie müssen eine gültige E-Mail-Adresse angeben!");
    } else {
      atCheck = 0;
      pointCheck = 0;
      for (posI = 0; posI < email.length(); posI++) {
        if (email.charAt(posI) == '@') {
          atCheck++;
        } else if (email.charAt(posI) == '.' && atCheck != 0) {
          pointCheck++;
        }
      }
      if (atCheck != 1) {
        throw new de.knightsoft.common.TextException("Eine gültige E-Mail-Adresse darf exakt ein @-Zeichen enthalten");
      }
      if (pointCheck == 0) {
        throw new de.knightsoft.common.TextException(
            "Eine gültige E-Mail-Adresse muß mindestens einen Punkt nach dem @-Zeichen enthalten");
      }
    }

    if (name == null || name.equals(StringUtils.EMPTY)) {
      throw new de.knightsoft.common.TextException("Der Nachname ist nicht gefüllt!");
    }

    if (vorname == null || vorname.equals(StringUtils.EMPTY)) {
      throw new de.knightsoft.common.TextException("Der Vorname ist nicht gefüllt!");
    }

    if (strasse == null || strasse.equals(StringUtils.EMPTY)) {
      throw new de.knightsoft.common.TextException("Die Strasse ist nicht gefüllt!");
    }

    if (plz == null || plz.length() < 4) {
      throw new de.knightsoft.common.TextException("Die Postleitzahl ist nicht fünfstellig gefüllt!");
    } else {
      if (!checkNummerisch(plz)) {
        throw new de.knightsoft.common.TextException("Die Postleitzahl ist nicht nummerisch!");
      }
    }

    if (ort == null || ort.equals(StringUtils.EMPTY)) {
      throw new de.knightsoft.common.TextException("Der Ort ist nicht gefüllt!");
    }

    if (rufnummer == null || rufnummer.equals(StringUtils.EMPTY)) {
      throw new de.knightsoft.common.TextException("Die Telefonnumer ist nicht fünfstellig gefüllt!");
    } else {
      for (posI = 0; posI < rufnummer.length(); posI++) {
        if (!Character.isDigit(rufnummer.charAt(posI)) && rufnummer.charAt(posI) != '(' && rufnummer.charAt(posI) != ')'
            && rufnummer.charAt(posI) != '/' && rufnummer.charAt(posI) != '-') {
          throw new de.knightsoft.common.TextException("Die Telefonnummer darf nur Ziffern, \"/\" und \"-\" enthalten!");
        }
      }
    }

    if (StringUtils.isNotEmpty(weitereNummer1)) {
      for (posI = 0; posI < weitereNummer1.length(); posI++) {
        if (!Character.isDigit(weitereNummer1.charAt(posI)) && weitereNummer1.charAt(posI) != '('
            && weitereNummer1.charAt(posI) != ')' && weitereNummer1.charAt(posI) != '/' && weitereNummer1.charAt(posI) != '-') {
          throw new de.knightsoft.common.TextException("Die 2. Telefonnummer darf nur Ziffern, \"/\" und \"-\" enthalten!");
        }
      }
    }

    if (StringUtils.isNotEmpty(weitereNummer2)) {
      for (posI = 0; posI < weitereNummer2.length(); posI++) {
        if (!Character.isDigit(weitereNummer2.charAt(posI)) && weitereNummer2.charAt(posI) != '('
            && weitereNummer2.charAt(posI) != ')' && weitereNummer2.charAt(posI) != '/' && weitereNummer2.charAt(posI) != '-') {
          throw new de.knightsoft.common.TextException("Die 3. Telefonnummer darf nur Ziffern, \"/\" und \"-\" enthalten!");
        }
      }
    }

    if (news == null) {
      news = "N";
    } else {
      news = "J";
    }
  }

  /**
   * prepare date for insert.
   *
   * @param sqlStringA2 sql part one
   * @param sqlStringB2 sql part two
   * @param pwStart password start
   * @return created sql
   */
  public String prepareInsert(final String sqlStringA2, final String sqlStringB2, final String pwStart)
      throws de.knightsoft.common.TextException {
    final StringBuilder sqlStringA = new StringBuilder(256);
    final StringBuilder sqlStringB = new StringBuilder(256);

    sqlStringA.append("eintrag_gesperrt, land, bietesuche, email, geschlecht, name, vorname, strasse, " //
        + "plz, ort, telefonnummer1, sende_infos");
    sqlStringB.append("'J', " + de.knightsoft.common.StringToSql.convert(land) + ", "
        + de.knightsoft.common.StringToSql.convert(bietesuche) + ", " + de.knightsoft.common.StringToSql.convert(email) + ", "
        + de.knightsoft.common.StringToSql.convert(geschlecht) + ", " + de.knightsoft.common.StringToSql.convert(name) + ", "
        + de.knightsoft.common.StringToSql.convert(vorname) + ", " + de.knightsoft.common.StringToSql.convert(strasse) + ", "
        + de.knightsoft.common.StringToSql.convert(plz) + ", " + de.knightsoft.common.StringToSql.convert(ort) + ", "
        + de.knightsoft.common.StringToSql.convert(rufnummer) + ", " + de.knightsoft.common.StringToSql.convert(news));

    if (StringUtils.isNotEmpty(weitereNummer1)) {
      sqlStringA.append(", telefonnummer2");
      sqlStringB.append(", ").append(de.knightsoft.common.StringToSql.convert(weitereNummer1));
    }

    if (StringUtils.isNotEmpty(weitereNummer2)) {
      sqlStringA.append(", telefonnummer3");
      sqlStringB.append(", ").append(de.knightsoft.common.StringToSql.convert(weitereNummer2));
    }

    if (StringUtils.isNotEmpty(bemerkung)) {
      sqlStringA.append(", bemerkung");
      sqlStringB.append(", ").append(de.knightsoft.common.StringToSql.convert(bemerkung));
    }

    if (StringUtils.isNotEmpty(url)) {
      sqlStringA.append(", url");
      sqlStringB.append(", ").append(de.knightsoft.common.StringToSql.convert(url));
    }

    // aktuelles Datum ermitteln und passend aufbereiten
    sqlStringA.append(", datum_erstellt");
    sqlStringB.append(", CURDATE()");

    // Passwort für das Speichern in der Datenbank verschlüsseln
    passwort = de.knightsoft.common.Constants.generatePassword(pwStart);
    sqlStringA.append(", passwort");
    sqlStringB.append(", PASSWORD(" + de.knightsoft.common.StringToSql.convert(passwort) + ")");

    if (sqlStringA2 != null && sqlStringB2 != null) {
      sqlStringA.append(sqlStringA2);
      sqlStringB.append(sqlStringB2);
    }
    return "(" + sqlStringA.toString() + ") VALUES (" + sqlStringB.toString() + ")";
  }

  /**
   * prepare date for a update.
   *
   * @return update sql string
   */
  public String prepareUpdate() throws de.knightsoft.common.TextException {
    final StringBuilder sqlString = new StringBuilder(256);

    sqlString.append("land=" + de.knightsoft.common.StringToSql.convert(land) + ", bietesuche="
        + de.knightsoft.common.StringToSql.convert(bietesuche) + ", email=" + de.knightsoft.common.StringToSql.convert(email)
        + ", geschlecht=" + de.knightsoft.common.StringToSql.convert(geschlecht) + ", name="
        + de.knightsoft.common.StringToSql.convert(name) + ", vorname=" + de.knightsoft.common.StringToSql.convert(vorname)
        + ", strasse=" + de.knightsoft.common.StringToSql.convert(strasse) + ", plz="
        + de.knightsoft.common.StringToSql.convert(plz) + ", ort=" + de.knightsoft.common.StringToSql.convert(ort)
        + ", telefonnummer1=" + de.knightsoft.common.StringToSql.convert(rufnummer));
    if (weitereNummer1.equals(StringUtils.EMPTY)) {
      sqlString.append(", telefonnummer2=null");
    } else {
      sqlString.append(", telefonnummer2=").append(de.knightsoft.common.StringToSql.convert(weitereNummer1));
    }
    if (weitereNummer2.equals(StringUtils.EMPTY)) {
      sqlString.append(", telefonnummer3=null");
    } else {
      sqlString.append(", telefonnummer3=").append(de.knightsoft.common.StringToSql.convert(weitereNummer2));
    }
    if (bemerkung.equals(StringUtils.EMPTY)) {
      sqlString.append(", bemerkung=null");
    } else {
      sqlString.append(", bemerkung=").append(de.knightsoft.common.StringToSql.convert(bemerkung));
    }
    if (url.equals(StringUtils.EMPTY)) {
      sqlString.append(", url=null");
    } else {
      sqlString.append(", url=").append(de.knightsoft.common.StringToSql.convert(url));
    }
    sqlString.append(", sende_infos=").append(de.knightsoft.common.StringToSql.convert(news));

    if (StringUtils.isNotEmpty(passwort)) {
      sqlString.append(", passwort=PASSWORD(").append(de.knightsoft.common.StringToSql.convert(passwort)).append(')');
    }

    // aktuelles Datum ermitteln und passend aufbereiten
    sqlString.append(", datum_geaendert=CURDATE()");

    return sqlString.toString();
  }

  /**
   * create html table.
   *
   * @param tabelle1 table text part 1
   * @param tabelle2 table text part 2
   * @param suche search text
   * @param servicebez service text
   * @return created html table
   */
  public String htmlTabelle(final String tabelle1, final String tabelle2, final boolean suche, final String servicebez) {

    final StringBuilder tabelleString = new StringBuilder(2500);

    tabelleString.append("<div style=\"text-align:center;\">\n" + "    <table style=\"margin-left:auto; margin-right:auto;\">\n"
        + "        <tr>\n" + "            <td style=\"text-align:left;\">Land:</td>\n"
        + "            <td style=\"text-align:left;\">" + landAusgabe(land) + "</td>\n" + "        </tr>\n" + "        <tr>\n"
        + "            <td style=\"text-align:left;\">Biete/Suche:</td>\n" + "            <td style=\"text-align:left;\">"
        + bsAusgabe(bietesuche) + "</td>\n" + "        </tr>\n" + "        <tr>\n"
        + "            <td style=\"text-align:left;\">E-Mail:</td>\n" + "            <td style=\"text-align:left;\">"
        + "<a href=\"MAILTO:" + de.knightsoft.common.StringToHtml.convert(email));
    if (suche) {
      final String emailText; // E-Mail Text vorbereiten
      if ("W".equals(geschlecht)) {
        emailText = "Sehr geehrte Frau " + name + ",";
      } else {
        emailText = "Sehr geehrter Herr " + name + ",";
      }
      tabelleString
          .append("?subject=Anfrage bezueglich Ihres " + servicebez + "-Angebots im KnightSoft-Net" + "&amp;body=" + emailText);
    }
    tabelleString.append("\"> " + de.knightsoft.common.StringToHtml.convert(email) + "</a>" + "</td>\n" + "        </tr>\n");
    tabelleString.append("        <tr>\n" + "            <td style=\"text-align:left;\">Geschlecht:</td>\n"
        + "            <td style=\"text-align:left;\">" + wmAusgabe(geschlecht) + "</td>\n" + "        </tr>\n"
        + "        <tr>\n" + "            <td style=\"text-align:left;\">Name:</td>\n"
        + "            <td style=\"text-align:left;\">" + de.knightsoft.common.StringToHtml.convert(name) + "</td>\n"
        + "        </tr>\n" + "        <tr>\n" + "            <td style=\"text-align:left;\">Vorname:</td>\n"
        + "            <td style=\"text-align:left;\">" + de.knightsoft.common.StringToHtml.convert(vorname) + "</td>\n"
        + "        </tr>\n" + "        <tr>\n" + "            <td style=\"text-align:left;\">Stra&szlig;e:</td>\n"
        + "            <td style=\"text-align:left;\">" + de.knightsoft.common.StringToHtml.convert(strasse) + "</td>\n"
        + "        </tr>\n" + "        <tr>\n" + "            <td style=\"text-align:left;\">Postleitzahl:</td>\n"
        + "            <td style=\"text-align:left;\">" + de.knightsoft.common.StringToHtml.convert(plz) + "</td>\n"
        + "        </tr>\n" + "        <tr>\n" + "            <td style=\"text-align:left;\">Ort:</td>\n"
        + "            <td style=\"text-align:left;\">" + de.knightsoft.common.StringToHtml.convert(ort) + "</td>\n"
        + "        </tr>\n");
    if (tabelle1 != null) {
      tabelleString.append(tabelle1);
    }
    tabelleString.append("        <tr>\n" + "            <td style=\"text-align:left;\">Telefonnummer:</td>\n"
        + "            <td style=\"text-align:left;\">" + de.knightsoft.common.StringToHtml.convert(rufnummer) + "</td>\n"
        + "        </tr>\n");
    if (StringUtils.isNotEmpty(weitereNummer1)) {
      tabelleString.append("        <tr>\n" + "            <td style=\"text-align:left;\">1. weitere Telefonnummer:</td>\n"
          + "            <td style=\"text-align:left;\">" + de.knightsoft.common.StringToHtml.convert(weitereNummer1)
          + "</td>\n" + "        </tr>\n");
    }
    if (StringUtils.isNotEmpty(weitereNummer2)) {
      tabelleString.append("        <tr>\n" + "            <td style=\"text-align:left;\">2. weitere Telefonnummer:</td>\n"
          + "            <td style=\"text-align:left;\">" + de.knightsoft.common.StringToHtml.convert(weitereNummer2)
          + "</td>\n" + "        </tr>\n");
    }
    if (StringUtils.isNotEmpty(tabelle2)) {
      tabelleString.append(tabelle2);
    }
    if (StringUtils.isNotEmpty(bemerkung)) {
      tabelleString.append("        <tr valign=\"top\">\n" + "            <td style=\"text-align:left;\">Bemerkung:</td>\n"
          + "            <td style=\"text-align:left;\">" + de.knightsoft.common.StringToHtml.convert(bemerkung) + "</td>\n"
          + "        </tr>\n");
    }
    if (StringUtils.isNotEmpty(url)) {
      String urlplus;
      if (url.startsWith("http")) {
        urlplus = url;
      } else {
        urlplus = "http://" + url;
      }
      tabelleString.append("        <tr valign=\"top\">\n" + "            <td style=\"text-align:left;\">Homepage:</td>\n"
          + "            <td style=\"text-align:left;\"><a href=\"" + de.knightsoft.common.StringToHtml.convert(urlplus) + "\">"
          + de.knightsoft.common.StringToHtml.convert(url) + "</a></td>\n" + "        </tr>\n");
    }
    if (!suche) {
      tabelleString.append("        <tr>\n" + "            <td style=\"text-align:left;\">Bezug der KnightSoft-News:</td>\n"
          + "            <td style=\"text-align:left;\">" + jnAusgabe(news) + "</td>\n" + "        </tr>\n");
    }
    tabelleString.append("    </table>\n" + "</div>\n");

    return tabelleString.toString();
  }

  /**
   * add hidden input fields.
   *
   * @return html code of hidden fields
   */
  public String htmlHidden() {
    final String HiddenString = "<input type=\"hidden\" name=\"land\" value=\""
        + de.knightsoft.common.StringToHtml.convert(land) + "\">\n" + "<input type=\"hidden\" name=\"bietesuche\" value=\""
        + de.knightsoft.common.StringToHtml.convert(bietesuche) + "\">\n" + "<input type=\"hidden\" name=\"email\" value=\""
        + de.knightsoft.common.StringToHtml.convert(email) + "\">\n" + "<input type=\"hidden\" name=\"Geschlecht\" value=\""
        + de.knightsoft.common.StringToHtml.convert(geschlecht) + "\">\n" + "<input type=\"hidden\" name=\"Nname\" value=\""
        + de.knightsoft.common.StringToHtml.convert(name) + "\">\n" + "<input type=\"hidden\" name=\"Vorname\" value=\""
        + de.knightsoft.common.StringToHtml.convert(vorname) + "\">\n" + "<input type=\"hidden\" name=\"Strasse\" value=\""
        + de.knightsoft.common.StringToHtml.convert(strasse) + "\">\n" + "<input type=\"hidden\" name=\"PLZ\" value=\""
        + de.knightsoft.common.StringToHtml.convert(plz) + "\">\n" + "<input type=\"hidden\" name=\"Ort\" value=\""
        + de.knightsoft.common.StringToHtml.convert(ort) + "\">\n" + "<input type=\"hidden\" name=\"Rufnummer\" value=\""
        + de.knightsoft.common.StringToHtml.convert(rufnummer) + "\">\n"
        + "<input type=\"hidden\" name=\"weitereNummer1\" value=\"" + de.knightsoft.common.StringToHtml.convert(weitereNummer1)
        + "\">\n" + "<input type=\"hidden\" name=\"weitereNummer2\" value=\""
        + de.knightsoft.common.StringToHtml.convert(weitereNummer2) + "\">\n"
        + "<input type=\"hidden\" name=\"Bemerkung\" value=\"" + de.knightsoft.common.StringToHtml.convert(bemerkung) + "\">\n"
        + "<input type=\"hidden\" name=\"News\" value=\"" + de.knightsoft.common.StringToHtml.convert(news) + "\">\n"
        + "<input type=\"hidden\" name=\"url\" value=\"" + de.knightsoft.common.StringToHtml.convert(url) + "\">\n";

    return HiddenString;
  }
}
