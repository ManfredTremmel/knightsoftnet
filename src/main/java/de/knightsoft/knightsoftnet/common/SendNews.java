/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.knightsoftnet.common;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * The <code>SendNews</code> class is based on the abstract class de.knightsoft.common.AbstractVisualDb and implements a
 * function to send a e-mail letter to the users of KnightSoft-Net.
 *
 *
 * @author Manfred Tremmel
 * @version 2.0.0, 15.08.2006
 */
public class SendNews extends de.knightsoft.common.AbstractVisualDb {
  private static final String INSERT_SQL = "INSERT INTO KnightSoft_Mail " + "(provider, email) " + "VALUES(?, ?)";
  private static final String LESE_BABYSITTER_SQL =
      "SELECT    email, geschlecht, name, vorname " + "FROM    KnightSoft_BabySitter " + "WHERE    sende_infos='J'";
  private static final String LESE_NACHHILFE_SQL =
      "SELECT    email, geschlecht, name, vorname " + "FROM    KnightSoft_Nachhilfe " + "WHERE    sende_infos='J'";
  private static final String LESE_FAHRGEMEIN_SQL =
      "SELECT    email, geschlecht, name, vorname " + "FROM    KnightSoft_Fahrgemeinschaft " + "WHERE    sende_infos='J'";
  private static final String LESE_TIPPRUNDE_SQL = "SELECT    email, geschlecht, name, vorname "
      + "FROM    KnightSoft_TippMitspieler " + "WHERE    sende_infos = '1' " + "GROUP BY spitzname, name, email";
  private static final String LESE_GEWINN_SQL = "SELECT    email, geschlecht, name, vorname "
      + "FROM    KnightSoft_GewinnspielTeilnehmer " + "WHERE    sende_infos='J' " + "GROUP BY name, vorname, email";

  /**
   * Constructor.
   *
   * @param thisDatabase Connection to database
   * @param servletname Name of the Servlet
   * @exception SQLException if a database access error occurs
   * @exception de.knightsoft.common.TextException to give out a text message
   * @see de.knightsoft.common.TextException
   */
  public SendNews(final Connection thisDatabase, final String servletname)
      throws java.sql.SQLException, de.knightsoft.common.TextException {
    super(thisDatabase, KnConst.HTML_BASE, servletname, KnConst.GIF_URL + "16x16/email.png", "E-Mail", "E-Mail Newsletter");
  }

  /**
   * The Method <code>HTMLPage</code> generates the html formular.
   *
   * @param res HttpServletResponse from the Servlet
   * @param hint Hint to be displayed on the screen
   * @param session The Data of the current Session
   * @param navTyp Navigation type
   * @return HTML code of the formular
   */
  @Override
  protected String htmlPage(final HttpServletResponse res, final String hint, final HttpSession session, final String navTyp) {

    return "        <div style=\"text-align:center;\">\n" + "        <form name=\"SendNews\" action=\""
        + res.encodeURL(KnConst.SERVLET_URL + servletName)
        + "\" method=\"POST\" enctype=\"application/x-www-form-urlencoded; charset=utf-8\" "
        + "enctype=\"application/x-www-form-urlencoded; charset=utf-8\" accept-charset=\"utf-8\">\n"
        + "            <input type=\"hidden\" name=\"Stufe\" value=\"2\">\n" + "            <h2>"
        + (hint == null ? "Versand von Informationen an alle eingetragenen User"
            : de.knightsoft.common.StringToHtml.convert(hint))
        + "</h2>\n" + "            <strong>Betreff:</strong><br>\n"
        + "            <input type=\"text\" name=\"Betreff\" size=\"74\" maxlength=\"80\"><br>\n"
        + "            <strong>E-Mail-Text:</strong><br>\n"
        + "            <textarea name=\"eMailText\" rows=\"20\" cols=\"74\"></textarea><br>\n"
        + "            <strong>Zu versenden an die eingetragenen User der Services:</strong><br>\n"
        + "            <input type=\"checkbox\" name=\"BabySitter\" " + "value=\"BabySitter\">BabySitter<br>\n"
        + "            <input type=\"checkbox\" name=\"Nachhilfe\" " + "value=\"Nachhilfe\">Nachhilfe<br>\n"
        + "            <input type=\"checkbox\" name=\"Fahrgemeinschaften\" "
        + "value=\"Fahrgemeinschaften\">Fahrgemeinschaften<br>\n"
        + "            <input type=\"checkbox\" name=\"Fussballtipprunde\" "
        + "value=\"Fussballtipprunde\">Fu&szlig;balltipprunde<br>\n"
        + "            <input type=\"checkbox\" name=\"Gewinnspieler\" " + "value=\"Gewinnspieler\">Gewinnspieler<br>\n"
        + "            <input type=\"submit\" name=\"Absenden\" value=\"Absenden\">"
        + " <input type=\"reset\" name=\"Abbrechen\" value=\"Abbrechen\">\n" + "        </form>\n" + "        </div>\n";
  }

  /**
   * The Method <code>HTMLPageSendt</code> generates the html output after sending.
   *
   * @param count number sendt mails
   * @return HTML code of the result page
   */
  protected String htmlPageSendt(final long count) {
    return "        <h2>Es wurden " + Long.toString(count) + " E-Mail(s) versendet!</h2>\n";
  }

  /**
   * The Method <code>verarbeitungVersand</code> prepare the sending of the mails.
   *
   * @param req HttpServletRequest from the Servlet
   * @param res HttpServletResponse from the Servlet
   * @return HTML code of the formular
   */
  private String verarbeitungVersand(final HttpServletRequest req) throws de.knightsoft.common.TextException, IOException {

    final String betreff = req.getParameter("Betreff");
    final String eMailText = req.getParameter("eMailText");
    final String babySitter = req.getParameter("BabySitter");
    final String nachhilfe = req.getParameter("Nachhilfe");
    final String fahrgemeinschaften = req.getParameter("Fahrgemeinschaften");
    final String fussballtipprunde = req.getParameter("Fussballtipprunde");
    final String gewinnspieler = req.getParameter("Gewinnspieler");
    long anzahl = 0;

    if (betreff == null || betreff.equals(StringUtils.EMPTY)) {
      throw new de.knightsoft.common.TextException("Es wurde kein Betreff eingegeben");
    }

    if (eMailText == null || eMailText.equals(StringUtils.EMPTY)) {
      throw new de.knightsoft.common.TextException("Ich verschicke nichts nicht, E-Mail-Text eingeben!");
    }

    if (babySitter == null && nachhilfe == null && fahrgemeinschaften == null && fussballtipprunde == null
        && gewinnspieler == null) {
      throw new de.knightsoft.common.TextException("Es wurde keine Empfängergruppe ausgewählt");
    }

    try {
      if (babySitter != null) {
        // Lesezugriff auf die Datenbank
        try (final PreparedStatement leseBabySitterSQLStatement = myDataBase.prepareStatement(SendNews.LESE_BABYSITTER_SQL)) {
          try (final ResultSet result = leseBabySitterSQLStatement.executeQuery()) {

            anzahl = sendEmail(anzahl, result, betreff, eMailText);
          }
        }
      }

      if (nachhilfe != null) {
        // Lesezugriff auf die Datenbank
        try (final PreparedStatement leseNachhilfeSQLStatement = myDataBase.prepareStatement(SendNews.LESE_NACHHILFE_SQL)) {
          try (final ResultSet result = leseNachhilfeSQLStatement.executeQuery()) {

            anzahl = sendEmail(anzahl, result, betreff, eMailText);
          }
        }
      }

      if (fahrgemeinschaften != null) {
        // Lesezugriff auf die Datenbank
        try (final PreparedStatement leseFahrgemeinSQLStatement = myDataBase.prepareStatement(SendNews.LESE_FAHRGEMEIN_SQL)) {
          try (final ResultSet result = leseFahrgemeinSQLStatement.executeQuery()) {

            anzahl = sendEmail(anzahl, result, betreff, eMailText);
          }
        }
      }

      // 26.07.2000: Tipprunde freigegeben
      if (fussballtipprunde != null) {
        // Lesezugriff auf die Datenbank
        try (final PreparedStatement leseTipprundeSQLStatement = myDataBase.prepareStatement(SendNews.LESE_TIPPRUNDE_SQL)) {
          try (final ResultSet result = leseTipprundeSQLStatement.executeQuery()) {

            anzahl = sendEmail(anzahl, result, betreff, eMailText);
          }
        }
      }

      if (gewinnspieler != null) {
        // Lesezugriff auf die Datenbank
        try (final PreparedStatement leseGewinnSQLStatement = myDataBase.prepareStatement(SendNews.LESE_GEWINN_SQL)) {
          try (final ResultSet result = leseGewinnSQLStatement.executeQuery()) {

            anzahl = sendEmail(anzahl, result, betreff, eMailText);
          }
        }
      }

      return htmlPageSendt(anzahl);
    } catch (final java.sql.SQLException e) {
      throw new de.knightsoft.common.TextException("Fehler beim Datenbanklesen zum E-Mail-Empfäger ermitteln: " + e.toString(),
          e);
    }
  }

  /**
   * The Method <code>eMailsVerschicken</code> inserts the mails into the database for sending with cron job at night.
   *
   * @param pcount number of Mails sent until now
   * @param result resultset to read the target information from
   * @param betreff subject of the mail
   * @param emailText Navigation type
   * @return number of eMails sent (given "Anzahl" + new ones)
   */
  private long sendEmail(final long pcount, final ResultSet result, final String betreff, final String emailText)
      throws de.knightsoft.common.TextException, IOException, SQLException {
    long count = pcount;
    String email;
    String provider;
    StringBuilder emailTextKomplett;
    try (final PreparedStatement insertSQLStatement = myDataBase.prepareStatement(SendNews.INSERT_SQL)) {
      while (result.next()) {
        email = result.getString("email");
        provider = email.substring(email.indexOf('@') + 1);

        emailTextKomplett = new StringBuilder();

        emailTextKomplett.append("MAIL FROM: " + de.knightsoft.common.Constants.EMAIL + "\n" + "RCPT TO: " + email + "\n"
            + "DATA\n" + "From: " + de.knightsoft.common.Constants.EMAIL + "\nOrganization: "
            + de.knightsoft.common.Constants.ORGANISATION + "\nTo: " + email + "\nSubject: " + betreff
            + "\nContent-Type: text/plain; charset=\"iso-8859-1\"" + "\nContent-Transfer-Encoding: 8bit" + "\n");

        // 24.04.2000: wenn kein Name vorhanden, allgemeine Anrede verwenden
        if (result.getString("name") == null) {
          emailTextKomplett.append("Sehr geehrte Damen und Herren");
        } else {
          if (result.getString("geschlecht").equals("W")) {
            emailTextKomplett.append("Sehr geehrte Frau ");
          } else {
            emailTextKomplett.append("Sehr geehrter Herr ");
          }

          emailTextKomplett.append(result.getString("name"));
        }
        emailTextKomplett.append(",\n\n");
        emailTextKomplett.append(emailText);
        emailTextKomplett.append("\n\nMit freundlichen Grüssen\n\nManfred Tremmel");

        count++;

        insertSQLStatement.clearParameters();
        insertSQLStatement.setString(1, provider);
        insertSQLStatement.setString(2, emailTextKomplett.toString());
        insertSQLStatement.executeUpdate();
      }
    }

    return count;
  }

  /**
   * The Method <code>doTheWork</code> coordinates all the work in this class.
   *
   * @param req HttpServletRequest from the Servlet
   * @param res HttpServletResponse from the Servlet
   * @param session The Data of the current Session
   * @return html code of the formular
   * @exception de.knightsoft.common.TextException when a error in checking the passwords occours
   */
  @Override
  public String doTheWork(final HttpServletRequest req, final HttpServletResponse res, final HttpSession session)
      throws de.knightsoft.common.TextException {

    String formular = null;
    final String stufe = req.getParameter("Stufe");

    if (!allowedToChange(session)) {
      return "<h2>Sie haben keine Berechtigung E-Mails zu verschicken</h2>\n";
    }

    try {
      if (stufe != null && stufe.equals("2")) {
        // Stufe 2 = Prüfen und Abschicken der email
        formular = verarbeitungVersand(req);
      } else {
        formular = htmlPage(res, null, session, null);
      }
    } catch (final de.knightsoft.common.TextException e) {
      formular = htmlPage(res, e.toString(), session, null);
    } catch (final IOException e) {
      formular = htmlPage(res, e.getMessage(), session, null);
    }

    return formular;
  }

  /**
   * The Method <code>allowedToSee</code> tells you if the currently logged in user is allowed to access this application.
   *
   * @param session Data of the current session
   * @return true if it is allowed for this user
   */
  @Override
  public boolean allowedToSee(final HttpSession session) {
    boolean returnwert = false;
    if (session != null) {
      if (session.getAttribute(servletName + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_USER
          + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_USER) != null) {
        final int stufe = ((Integer) session.getAttribute(servletName + KnConst.SESSION_STUFE_FT)).intValue();
        returnwert = stufe > 5;
      }
    }
    return returnwert;
  }

  /**
   * The Method <code>allowedToChange</code> tells you if the currently logged in user is allowed to change data in this
   * application.
   *
   * @param session Data of the current session
   * @return true if it is allowed for this user
   */
  @Override
  public boolean allowedToChange(final HttpSession session) {
    boolean returnwert = false;
    if (session != null) {
      if (session.getAttribute(servletName + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_USER
          + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_USER) != null) {
        final int stufe = ((Integer) session.getAttribute(servletName + KnConst.SESSION_STUFE_FT)).intValue();
        returnwert = stufe > 5;
      }
    }
    return returnwert;
  }
}
