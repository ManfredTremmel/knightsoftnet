/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.knightsoftnet.common;

import de.knightsoft.common.Constants;
import de.knightsoft.common.TextException;

import org.apache.commons.lang3.StringUtils;

import javax.naming.InitialContext;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class BannerWeiterleitungServlet extends HttpServlet {

  private static final long serialVersionUID = -2201063038107305221L;
  private static final String URL_DEFAULT = "http://www.knightsoft-net.de/index.html";
  private static final String LESEN_SQL =
      "SELECT        bp.zaehler AS zaehler, " + "            b.url AS url " + "FROM        KnightSoft_Bannerpartner AS bp "
          + "LEFT JOIN    KnightSoft_Banner AS b " + "ON            bp.banner = b.zaehler " + "WHERE        bp.user = ?";
  private static final String SCHREIBEN_SQL =
      "INSERT INTO KnightSoft_Bannerklicks " + "(bannerpartner, useragent, ip, datum_zeit) " + "VALUES (?, ?, ?, NOW())";

  // Datenbankkonnecteion global definiert
  protected Connection myDataBase;

  /**
   * init-Methode.
   */
  @Override
  public void init(final ServletConfig config) throws ServletException {
    super.init(config);
    // Datenbank öffnen
    try {
      final InitialContext ic = new InitialContext();
      final DataSource lDataSource = (DataSource) ic.lookup("java:comp/env/jdbc/db_knightsoft");
      myDataBase = lDataSource.getConnection();
      ic.close();
    } catch (final java.sql.SQLException e) {
      throw new ServletException("SQL-Fehler beim Vorbereiten des Kommandos: " + e.toString(), e);
    } catch (final javax.naming.NamingException e) {
      throw new ServletException(e);
    }
  }

  /**
   * destroy method closes database connection
   *
   * @see javax.servlet.GenericServlet#destroy()
   */
  @Override
  public void destroy() {
    // Clean up.
    try {
      if (myDataBase != null) {
        myDataBase.close();
      }
    } catch (final SQLException ignored) {
      // ignore
      ignored.printStackTrace();
    }
  }

  private void verarbeitung(final HttpServletResponse res, final String user, final String remoteSw, final String remoteAddr)
      throws TextException, IOException {
    long dbNr = 0;
    String url = BannerWeiterleitungServlet.URL_DEFAULT;

    try {
      // sollte keine korrekte User-Angabe vorliegen, defaultwerte
      // auf unsere Seiten und Benutzernummer 0
      if (StringUtils.isNoneEmpty(user)) {
        // Zugriff auf die Bannerpartner-Tabelle
        try (final PreparedStatement lesenStatement = myDataBase.prepareStatement(BannerWeiterleitungServlet.LESEN_SQL)) {
          lesenStatement.clearParameters();
          lesenStatement.setString(1, de.knightsoft.common.StringToSql.convertStringPrepared(user));
          try (final ResultSet result = lesenStatement.executeQuery()) {
            if (result.next()) {
              // Partnernummer und Bannernummer ermitteln
              dbNr = result.getLong("zaehler");
              url = result.getString("url");
            }
          }
        }

      }
      // Eintrag in unsere Banner-Klick-Tabelle
      try (final PreparedStatement schreibenStatement = myDataBase.prepareStatement(BannerWeiterleitungServlet.SCHREIBEN_SQL)) {
        schreibenStatement.clearParameters();
        schreibenStatement.setLong(1, dbNr);
        schreibenStatement.setString(2, remoteSw);
        schreibenStatement.setString(3, remoteAddr);
        schreibenStatement.executeUpdate();
      }

      res.sendRedirect(url);
    } catch (final java.sql.SQLException e) {
      // Um die Fehlerbehandlung nicht zweimal zu machen, schicken
      // wir die Meldung einfach weiter
      throw new de.knightsoft.common.TextException(e.toString(), e);
    }
  }

  /**
   * Die doGet Methode wird nur bei Direkter Eingabe/Link des Servlets im Browser angesprochen.
   */
  @Override
  public void doGet(final HttpServletRequest req, final HttpServletResponse res) throws ServletException, IOException {
    final String user = req.getParameter("user");
    final String remoteSw = req.getHeader("user-agent");
    final String remoteAddr = req.getRemoteAddr();

    try {
      verarbeitung(res, user, remoteSw, remoteAddr);
    } catch (final de.knightsoft.common.TextException e) {
      // Setze als erstes den "content type" Header der Antwortseite auf HTML
      res.setContentType("text/html");

      // Writer für die HTML-Datei holen
      final ServletOutputStream out = res.getOutputStream();

      // HTML-Datei für Fehlermeldung schreiben
      out.println("<!DOCTYPE html>");
      out.println("<html>");
      out.println("    <head>");
      out.println("        <meta http-equiv=\"content-type\" content=\"text/html; " + "charset=UTF-8\">");
      out.println("        <meta name=\"MSSmartTagsPreventParsing\" content=\"TRUE\">");
      out.println("        <title>Fehler!!</title>");
      out.println("    </head>");
      out.println("    <body>");
      out.println("        <h2 style=\"text-align:center\">" + e.toHtml() + "</h2>");
      out.println("    </body>");
      out.println("</html>");
      out.close();
    }
  }

  @Override
  public String getServletInfo() {
    return "Count and redirect banner-clicks\n\n" + Constants.COPYRIGHT;
  }
}
