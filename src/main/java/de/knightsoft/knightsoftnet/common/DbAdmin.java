/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.knightsoftnet.common;

import de.knightsoft.common.DataBaseDepending;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

/**
 * The <code>DbAdmin</code> class is based on the abstract class de.knightsoft.common.AbstractVisualDb and implements functions
 * to backup the database and show a statistic.
 *
 * @author Manfred Tremmel
 * @version 2.0.0, 15.08.2006
 */

public class DbAdmin extends de.knightsoft.common.AbstractVisualDb {

  private static final String BABY_SITTER_SQL_DROP_STRING = "DROP TABLE IF EXISTS KnightSoft_BabySitter;\n";
  private static final String BABY_SITTER_SQL_STRING = "CREATE TABLE KnightSoft_BabySitter\n" + "(\n"
      + "    zaehler                  BIGINT    UNSIGNED NOT NULL AUTO_INCREMENT,\n"
      + "    sende_infos              ENUM(\"J\",\"N\") DEFAULT \"J\",\n"
      + "    eintrag_gesperrt         ENUM(\"J\",\"N\") DEFAULT \"N\",\n" + "    datum_erstellt           DATE,\n"
      + "    datum_geaendert          DATE,\n" + "    geschlecht               ENUM(\"W\",\"M\") DEFAULT \"W\",\n"
      + "    name                     VARCHAR(50),\n" + "    vorname                  VARCHAR(50),\n"
      + "    strasse                  VARCHAR(50),\n" + "    plz                      CHAR(5),\n"
      + "    ort                      VARCHAR(50),\n" + "    email                    VARCHAR(50) NOT NULL,\n"
      + "    vorwahl                  VARCHAR(10) NOT NULL,\n" + "    telefonnummer1           VARCHAR(30),\n"
      + "    telefonnummer2           VARCHAR(30),\n" + "    telefonnummer3           VARCHAR(30),\n"
      + "    passwort                 VARCHAR(50) NOT NULL,\n" + "    geburtsjahr              YEAR,\n"
      + "    mehrere_kinder           ENUM(\"J\",\"N\") DEFAULT \"N\",\n"
      + "    koerper_behinderte       ENUM(\"J\",\"N\") DEFAULT \"N\",\n"
      + "    geistig_behinderte       ENUM(\"J\",\"N\") DEFAULT \"N\",\n" + "    preis_text               LONGTEXT,\n"
      + "    bemerkung                LONGTEXT,\n" + "    url                      VARCHAR(200),\n"
      + "    land                     ENUM(\"de\",\"at\", \"ch\") DEFAULT \"de\" NOT NULL,\n"
      + "    bietesuche               ENUM(\"b\",\"s\") DEFAULT \"b\" NOT NULL,\n"
      + "    anzahl_fehllogin         TINYINT UNSIGNED DEFAULT 0 NOT NULL,\n" + "    datum_fehllogin          DATETIME,\n"
      + "    datum_login              DATETIME,\n" + "    PRIMARY KEY(zaehler),\n" + "    INDEX LOGIN (email, passwort),\n"
      + "    INDEX SEARCH (land, bietesuche, vorwahl)\n" + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;\n";
  private static final String NACHHILFE_SQL_DROP_STRING = "DROP TABLE IF EXISTS KnightSoft_Nachhilfe;\n";
  private static final String NACHHILFE_SQL_STRING = "CREATE TABLE KnightSoft_Nachhilfe\n" + "(\n"
      + "    zaehler                  BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,\n"
      + "    sende_infos              ENUM(\"J\",\"N\") DEFAULT \"J\",\n"
      + "    eintrag_gesperrt         ENUM(\"J\",\"N\") DEFAULT \"N\",\n" + "    datum_erstellt           DATE,\n"
      + "    datum_geaendert          DATE,\n" + "    geschlecht               ENUM(\"W\",\"M\") DEFAULT \"W\",\n"
      + "    name                     VARCHAR(50),\n" + "    vorname                  VARCHAR(50),\n"
      + "    strasse                  VARCHAR(50),\n" + "    plz                      CHAR(5),\n"
      + "    ort                      VARCHAR(50),\n" + "    email                    VARCHAR(50) NOT NULL,\n"
      + "    vorwahl                  VARCHAR(10) NOT NULL,\n" + "    telefonnummer1           VARCHAR(30),\n"
      + "    telefonnummer2           VARCHAR(30),\n" + "    telefonnummer3           VARCHAR(30),\n"
      + "    passwort                 VARCHAR(50) NOT NULL,\n" + "    geburtsjahr              YEAR,\n"
      + "    biologie                 ENUM(\"J\",\"N\") DEFAULT \"N\",\n" + "    biologie_j_von           TINYINT UNSIGNED,\n"
      + "    biologie_j_bis           TINYINT UNSIGNED,\n" + "    biologie_s_von           TINYINT UNSIGNED,\n"
      + "    biologie_s_bis           TINYINT UNSIGNED,\n" + "    bwl                      ENUM(\"J\",\"N\") DEFAULT \"N\",\n"
      + "    bwl_j_von                TINYINT UNSIGNED,\n" + "    bwl_j_bis                TINYINT UNSIGNED,\n"
      + "    bwl_s_von                TINYINT UNSIGNED,\n" + "    bwl_s_bis                TINYINT UNSIGNED,\n"
      + "    chemie                   ENUM(\"J\",\"N\") DEFAULT \"N\",\n" + "    chemie_j_von             TINYINT UNSIGNED,\n"
      + "    chemie_j_bis             TINYINT UNSIGNED,\n" + "    chemie_s_von             TINYINT UNSIGNED,\n"
      + "    chemie_s_bis             TINYINT UNSIGNED,\n"
      + "    deutsch                  ENUM(\"J\",\"N\")    DEFAULT \"N\",\n"
      + "    deutsch_j_von            TINYINT UNSIGNED,\n" + "    deutsch_j_bis            TINYINT UNSIGNED,\n"
      + "    deutsch_s_von            TINYINT UNSIGNED,\n" + "    deutsch_s_bis            TINYINT UNSIGNED,\n"
      + "    englisch                 ENUM(\"J\",\"N\") DEFAULT \"N\",\n" + "    englisch_j_von           TINYINT UNSIGNED,\n"
      + "    englisch_j_bis           TINYINT UNSIGNED,\n" + "    englisch_s_von           TINYINT UNSIGNED,\n"
      + "    englisch_s_bis           TINYINT UNSIGNED,\n" + "    erdkunde                 ENUM(\"J\",\"N\") DEFAULT \"N\",\n"
      + "    erdkunde_j_von           TINYINT UNSIGNED,\n" + "    erdkunde_j_bis           TINYINT UNSIGNED,\n"
      + "    erdkunde_s_von           TINYINT UNSIGNED,\n" + "    erdkunde_s_bis           TINYINT UNSIGNED,\n"
      + "    franzoesisch             ENUM(\"J\",\"N\") DEFAULT \"N\",\n" + "    franzoesisch_j_von       TINYINT UNSIGNED,\n"
      + "    franzoesisch_j_bis       TINYINT UNSIGNED,\n" + "    franzoesisch_s_von       TINYINT UNSIGNED,\n"
      + "    franzoesisch_s_bis       TINYINT UNSIGNED,\n" + "    geschichte               ENUM(\"J\",\"N\") DEFAULT \"N\",\n"
      + "    geschichte_j_von         TINYINT UNSIGNED,\n" + "    geschichte_j_bis         TINYINT UNSIGNED,\n"
      + "    geschichte_s_von         TINYINT UNSIGNED,\n" + "    geschichte_s_bis         TINYINT UNSIGNED,\n"
      + "    griechisch               ENUM(\"J\",\"N\") DEFAULT \"N\",\n" + "    griechisch_j_von         TINYINT UNSIGNED,\n"
      + "    griechisch_j_bis         TINYINT UNSIGNED,\n" + "    griechisch_s_von         TINYINT UNSIGNED,\n"
      + "    griechisch_s_bis         TINYINT UNSIGNED,\n" + "    informatik               ENUM(\"J\",\"N\") DEFAULT \"N\",\n"
      + "    informatik_j_von         TINYINT UNSIGNED,\n" + "    informatik_j_bis         TINYINT UNSIGNED,\n"
      + "    informatik_s_von         TINYINT UNSIGNED,\n" + "    informatik_s_bis         TINYINT UNSIGNED,\n"
      + "    latein                   ENUM(\"J\",\"N\") DEFAULT \"N\",\n" + "    latein_j_von             TINYINT UNSIGNED,\n"
      + "    latein_j_bis             TINYINT UNSIGNED,\n" + "    latein_s_von             TINYINT UNSIGNED,\n"
      + "    latein_s_bis             TINYINT UNSIGNED,\n" + "    maschineschreiben        ENUM(\"J\",\"N\") DEFAULT \"N\",\n"
      + "    maschineschreiben_j_von  TINYINT UNSIGNED,\n" + "    maschineschreiben_j_bis  TINYINT UNSIGNED,\n"
      + "    maschineschreiben_s_von  TINYINT UNSIGNED,\n" + "    maschineschreiben_s_bis  TINYINT UNSIGNED,\n"
      + "    mathematik               ENUM(\"J\",\"N\") DEFAULT \"N\",\n" + "    mathematik_j_von         TINYINT UNSIGNED,\n"
      + "    mathematik_j_bis         TINYINT UNSIGNED,\n" + "    mathematik_s_von         TINYINT UNSIGNED,\n"
      + "    mathematik_s_bis         TINYINT UNSIGNED,\n" + "    physik                   ENUM(\"J\",\"N\") DEFAULT \"N\",\n"
      + "    physik_j_von             TINYINT UNSIGNED,\n" + "    physik_j_bis             TINYINT UNSIGNED,\n"
      + "    physik_s_von             TINYINT UNSIGNED,\n" + "    physik_s_bis             TINYINT UNSIGNED,\n"
      + "    sozialkunde              ENUM(\"J\",\"N\") DEFAULT \"N\",\n" + "    sozialkunde_j_von        TINYINT UNSIGNED,\n"
      + "    sozialkunde_j_bis        TINYINT UNSIGNED,\n" + "    sozialkunde_s_von        TINYINT UNSIGNED,\n"
      + "    sozialkunde_s_bis        TINYINT UNSIGNED,\n" + "    stenografie              ENUM(\"J\",\"N\") DEFAULT \"N\",\n"
      + "    stenografie_j_von        TINYINT UNSIGNED,\n" + "    stenografie_j_bis        TINYINT UNSIGNED,\n"
      + "    stenografie_s_von        TINYINT UNSIGNED,\n" + "    stenografie_s_bis        TINYINT UNSIGNED,\n"
      + "    weiteres_fach1           VARCHAR(30),\n" + "    weiteres_fach1_j_von     TINYINT UNSIGNED,\n"
      + "    weiteres_fach1_j_bis     TINYINT UNSIGNED,\n" + "    weiteres_fach1_s_von     TINYINT UNSIGNED,\n"
      + "    weiteres_fach1_s_bis     TINYINT UNSIGNED,\n" + "    weiteres_fach2           VARCHAR(30),\n"
      + "    weiteres_fach2_j_von     TINYINT UNSIGNED,\n" + "    weiteres_fach2_j_bis     TINYINT UNSIGNED,\n"
      + "    weiteres_fach2_s_von     TINYINT UNSIGNED,\n" + "    weiteres_fach2_s_bis     TINYINT UNSIGNED,\n"
      + "    weiteres_fach3           VARCHAR(30),\n" + "    weiteres_fach3_j_von     TINYINT UNSIGNED,\n"
      + "    weiteres_fach3_j_bis     TINYINT UNSIGNED,\n" + "    weiteres_fach3_s_von     TINYINT UNSIGNED,\n"
      + "    weiteres_fach3_s_bis     TINYINT UNSIGNED,\n" + "    preis_text               LONGTEXT,\n"
      + "    bemerkung                LONGTEXT,\n" + "    url                      VARCHAR(200),\n"
      + "    land                     ENUM(\"de\",\"at\", \"ch\") DEFAULT \"de\" NOT NULL,\n"
      + "    bietesuche               ENUM(\"b\",\"s\") DEFAULT \"b\" NOT NULL,\n"
      + "    anzahl_fehllogin         TINYINT UNSIGNED DEFAULT 0 NOT NULL,\n" + "    datum_fehllogin          DATETIME,\n"
      + "    datum_login              DATETIME,\n" + "    PRIMARY KEY(zaehler),\n" + "    INDEX LOGIN (email, passwort),\n"
      + "    INDEX SEARCH (land, bietesuche, vorwahl)\n" + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;\n";
  private static final String FAHRGEMEINSCHAFT_SQL_DROP_STRING = "DROP TABLE IF EXISTS KnightSoft_Fahrgemeinschaft;\n";
  private static final String FAHRGEMEINSCHAFT_SQL_STRING = "CREATE TABLE KnightSoft_Fahrgemeinschaft\n" + "(\n"
      + "    zaehler                  BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,\n"
      + "    sende_infos              ENUM(\"J\",\"N\") DEFAULT \"J\",\n"
      + "    eintrag_gesperrt         ENUM(\"J\",\"N\") DEFAULT \"N\",\n" + "    datum_erstellt           DATE,\n"
      + "    datum_geaendert          DATE,\n" + "    geschlecht               ENUM(\"W\",\"M\") DEFAULT \"W\",\n"
      + "    name                     VARCHAR(50),\n" + "    vorname                  VARCHAR(50),\n"
      + "    strasse                  VARCHAR(50),\n" + "    plz                      CHAR(5),\n"
      + "    ort                      VARCHAR(50),\n" + "    email                    VARCHAR(50) NOT NULL,\n"
      + "    telefonnummer1           VARCHAR(30),\n" + "    telefonnummer2           VARCHAR(30),\n"
      + "    telefonnummer3           VARCHAR(30),\n" + "    passwort                 VARCHAR(50) NOT NULL,\n"
      + "    einmalige_fahrt          ENUM(\"J\",\"N\") DEFAULT \"J\",\n" + "    abfahrtsdatum            DATE,\n"
      + "    abfahrtsplz              CHAR(5) NOT NULL,\n" + "    abfahrtsort              VARCHAR(50) NOT NULL,\n"
      + "    abfahrtsstrasse          VARCHAR(50),\n" + "    abfahrtszeit             TIME,\n"
      + "    zielplz                  CHAR(5) NOT NULL,\n" + "    zielort                  VARCHAR(50) NOT NULL,\n"
      + "    zielstrasse              VARCHAR(50),\n" + "    ankunftszeit             TIME,\n"
      + "    bemerkung                LONGTEXT,\n" + "    url                      VARCHAR(200),\n"
      + "    rueckfahrt               ENUM(\"J\",\"N\") DEFAULT \"N\",\n" + "    rueckfahrtsdatum         DATE,\n"
      + "    rueck_abfahrtszeit       TIME,\n" + "    rueck_ankunftszeit       TIME,\n"
      + "    land                     ENUM(\"de\",\"at\", \"ch\") DEFAULT \"de\" NOT NULL,\n"
      + "    bietesuche               ENUM(\"b\",\"s\") DEFAULT \"b\" NOT NULL,\n"
      + "    anzahl_fehllogin         TINYINT UNSIGNED  DEFAULT 0 NOT NULL,\n" + "    datum_fehllogin          DATETIME,\n"
      + "    datum_login              DATETIME,\n" + "    PRIMARY KEY(zaehler),\n" + "    INDEX LOGIN (email, passwort),\n"
      + "    INDEX SEARCH (land, bietesuche, zielplz)\n" + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;\n";
  private static final String GEWINNSPIEL_SQL_DROP_STRING =
      "DROP TABLE IF EXISTS KnightSoft_GewinnspielTeilnehmer;\n" + "DROP TABLE IF EXISTS KnightSoft_Gewinnspiel;\n";
  private static final String GEWINNSPIEL_SQL_STRING = "CREATE TABLE KnightSoft_Gewinnspiel\n" + "(\n"
      + "    zaehler                  BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,\n"
      + "    eintrag_gesperrt         ENUM(\"J\",\"N\") DEFAULT \"N\",\n" + "    datum_von                DATE,\n"
      + "    datum_bis                DATE,\n" + "    datum_ziehung            DATE,\n"
      + "    anzahl_preise            INT UNSIGNED NOT NULL,\n" + "    system                   ENUM(\"allgemein\",\n"
      + "                                   \"BabySitter\",\n" + "                                   \"Nachhilfe\",\n"
      + "                                   \"Fahrgemeinschaften\",\n"
      + "                                   \"Fußballtipps\",\n"
      + "                                   \"GewinnspielWorld\") DEFAULT \"allgemein\",\n"
      + "    ausgabetext              LONGTEXT,\n" + "    reihenfolge_wichtig      ENUM(\"J\",\"N\") DEFAULT \"J\",\n"
      + "    formular_generieren      ENUM(\"J\",\"N\") DEFAULT \"J\",\n" + "    frage1                   VARCHAR(200),\n"
      + "    frage2                   VARCHAR(200),\n" + "    frage3                   VARCHAR(200),\n"
      + "    frage4                   VARCHAR(200),\n" + "    frage5                   VARCHAR(200),\n"
      + "    frage6                   VARCHAR(200),\n" + "    frage7                   VARCHAR(200),\n"
      + "    frage8                   VARCHAR(200),\n" + "    frage9                   VARCHAR(200),\n"
      + "    frage10                  VARCHAR(200),\n" + "    antwort1                 VARCHAR(50),\n"
      + "    antwort2                 VARCHAR(50),\n" + "    antwort3                 VARCHAR(50),\n"
      + "    antwort4                 VARCHAR(50),\n" + "    antwort5                 VARCHAR(50),\n"
      + "    antwort6                 VARCHAR(50),\n" + "    antwort7                 VARCHAR(50),\n"
      + "    antwort8                 VARCHAR(50),\n" + "    antwort9                 VARCHAR(50),\n"
      + "    antwort10                VARCHAR(50),\n" + "    bild                     VARCHAR(200),\n"
      + "    url                      MEDIUMTEXT,\n" + "    gewinnerliste            ENUM(\"J\",\"N\") DEFAULT \"N\",\n"
      + "    PRIMARY KEY(zaehler)\n" + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;\n\n"
      + "CREATE TABLE KnightSoft_GewinnspielTeilnehmer\n" + "(\n"
      + "    zaehler                  BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,\n"
      + "    gewinnspiel_nr           BIGINT UNSIGNED NOT NULL,\n" + "    gewinnspiel_reihung      BIGINT UNSIGNED,\n"
      + "    sende_infos              ENUM(\"J\",\"N\") DEFAULT \"J\",\n"
      + "    eintrag_gesperrt         ENUM(\"J\",\"N\") DEFAULT \"N\",\n"
      + "    antwort_korrekt          ENUM(\"J\",\"N\") DEFAULT \"N\",\n" + "    datum_erstellt           DATE,\n"
      + "    system                   ENUM(\"BabySitter\",\n" + "                                   \"Nachhilfe\",\n"
      + "                                   \"Fahrgemeinschaften\",\n"
      + "                                   \"Fußballtipps\",\n" + "                                   \"GewinnspielWorld\"),\n"
      + "    geschlecht               ENUM(\"W\",\"M\") DEFAULT \"W\",\n" + "    name                     VARCHAR(50),\n"
      + "    vorname                  VARCHAR(50),\n" + "    strasse                  VARCHAR(50),\n"
      + "    plz                      CHAR(5),\n" + "    ort                      VARCHAR(50),\n"
      + "    email                    VARCHAR(50) NOT NULL,\n" + "    antwort1                 VARCHAR(50),\n"
      + "    antwort2                 VARCHAR(50),\n" + "    antwort3                 VARCHAR(50),\n"
      + "    antwort4                 VARCHAR(50),\n" + "    antwort5                 VARCHAR(50),\n"
      + "    antwort6                 VARCHAR(50),\n" + "    antwort7                 VARCHAR(50),\n"
      + "    antwort8                 VARCHAR(50),\n" + "    antwort9                 VARCHAR(50),\n"
      + "    antwort10                VARCHAR(50),\n" + "    gewinnerliste            ENUM(\"J\",\"N\") DEFAULT \"N\",\n"
      + "    PRIMARY KEY(zaehler),\n" + "    CONSTRAINT gwstfk FOREIGN KEY (gewinnspiel_nr)\n"
      + "        REFERENCES KnightSoft_Gewinnspiel (zaehler)\n" + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;\n";
  private static final String BANNER_SQL_DROP_STRING = "DROP TABLE IF EXISTS KnightSoft_Bannerklicks;\n"
      + "DROP TABLE IF EXISTS KnightSoft_Bannerpartner;\n" + "DROP TABLE IF EXISTS KnightSoft_Banner;\n";
  private static final String BANNER_SQL_STRING =
      "CREATE TABLE KnightSoft_Banner\n" + "(\n" + "    zaehler                  BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,\n"
          + "    banner_html              MEDIUMTEXT,\n" + "    url                      VARCHAR(200),\n"
          + "    PRIMARY KEY(zaehler)\n" + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;\n" + "CREATE TABLE KnightSoft_Bannerpartner\n"
          + "(\n" + "    zaehler                  BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,\n"
          + "    banner                   BIGINT UNSIGNED NOT NULL,\n"
          + "    anrede                   ENUM(\"Firma\", \"Frau\", \"Herr\") DEFAULT \"Firma\",\n"
          + "    firma1                   VARCHAR(50),\n" + "    firma2                   VARCHAR(50),\n"
          + "    name                     VARCHAR(50),\n" + "    vorname                  VARCHAR(50),\n"
          + "    strasse                  VARCHAR(50) NOT NULL,\n" + "    plz                      CHAR(5) NOT NULL,\n"
          + "    ort                      VARCHAR(50) NOT NULL,\n" + "    telefon                  VARCHAR(30),\n"
          + "    email                    VARCHAR(50) NOT NULL,\n" + "    bank                     VARCHAR(50),\n"
          + "    blz                      CHAR(8),\n" + "    konto                    CHAR(10),\n"
          + "    user                     VARCHAR(20) NOT NULL,\n" + "    passwort                 VARCHAR(50) NOT NULL,\n"
          + "    PRIMARY KEY(zaehler),\n" + "     INDEX (user),\n" + "    INDEX (passwort),\n"
          + "    CONSTRAINT bpbafk FOREIGN KEY (banner)\n" + "        REFERENCES KnightSoft_Gewinnspiel (zaehler)\n"
          + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;\n" + "CREATE TABLE KnightSoft_Bannerklicks\n" + "(\n"
          + "    zaehler                  BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,\n"
          + "    bannerpartner            BIGINT UNSIGNED NOT NULL,\n" + "    useragent                VARCHAR(60) NOT NULL,\n"
          + "    ip                       VARCHAR(30) NOT NULL,\n" + "    datum_zeit               DATETIME NOT NULL,\n"
          + "    PRIMARY KEY(zaehler),\n" + "    CONSTRAINT bkbpfk FOREIGN KEY (bannerpartner)\n"
          + "        REFERENCES KnightSoft_Bannerpartner (zaehler)\n" + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;\n";
  private static final String MAIL_SQL_DROP_STRING = "DROP TABLE IF EXISTS KnightSoft_Mail;\n";
  private static final String MAIL_SQL_STRING =
      "CREATE TABLE KnightSoft_Mail\n" + "(\n" + "    zaehler                  BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,\n"
          + "    provider                 VARCHAR(50) NOT NULL,\n" + "    email                    LONGTEXT,\n"
          + "    PRIMARY KEY(zaehler)\n" + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;\n";
  private static final String TIPP_SQL_DROP_STRING =
      "DROP TABLE IF EXISTS KnightSoft_TippSpieleTipp;\n" + "DROP TABLE IF EXISTS KnightSoft_TippMitspieler;\n"
          + "DROP TABLE IF EXISTS KnightSoft_TippSpiele;\n" + "DROP TABLE IF EXISTS KnightSoft_TippLigaMannschaft;\n"
          + "DROP TABLE IF EXISTS KnightSoft_TippMannschaften;\n" + "DROP TABLE IF EXISTS KnightSoft_TippLiga;\n";
  private static final String TIPP_SQL_STRING = "CREATE TABLE KnightSoft_TippLiga\n" + "(\n"
      + "    Mandator                 SMALLINT UNSIGNED NOT NULL DEFAULT \'1\',\n"
      + "    saison_liga              INT UNSIGNED NOT NULL,\n" + "    eintrag_gesperrt         BOOL NOT NULL DEFAULT '0',\n"
      + "    bezeichnung              VARCHAR(50) NOT NULL,\n" + "    anz_manschaften          TINYINT UNSIGNED NOT NULL,\n"
      + "    anz_tab_oben1            TINYINT UNSIGNED NOT NULL,\n"
      + "    anz_tab_oben2            TINYINT UNSIGNED NOT NULL,\n"
      + "    anz_tab_unten1           TINYINT UNSIGNED NOT NULL,\n"
      + "    anz_tab_unten2           TINYINT UNSIGNED NOT NULL,\n"
      + "    punkte_sieg              TINYINT UNSIGNED NOT NULL,\n"
      + "    punkte_unent             TINYINT UNSIGNED NOT NULL,\n"
      + "    punkte_nieder            TINYINT UNSIGNED NOT NULL,\n"
      + "    tr_punkte_exakt          TINYINT UNSIGNED NOT NULL,\n"
      + "    tr_punkte_torver         TINYINT UNSIGNED NOT NULL,\n"
      + "    tr_punkte_toto           TINYINT UNSIGNED NOT NULL,\n"
      + "    tr_punkte_falsch         TINYINT UNSIGNED NOT NULL,\n" + "    PRIMARY KEY(Mandator, saison_liga)\n"
      + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;\n" + "CREATE TABLE KnightSoft_TippMannschaften\n" + "(\n"
      + "    Mandator                 SMALLINT UNSIGNED NOT NULL DEFAULT \'1\',\n"
      + "    zaehler                  INT UNSIGNED NOT NULL AUTO_INCREMENT,\n"
      + "    bezeichnung              VARCHAR(50) NOT NULL,\n" + "    PRIMARY KEY(zaehler),\n"
      + "    INDEX idx_manzaehl(Mandator, zaehler)\n" + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;\n"
      + "CREATE TABLE KnightSoft_TippLigaMannschaft\n" + "(\n"
      + "    Mandator                 SMALLINT UNSIGNED NOT NULL DEFAULT \'1\',\n"
      + "    saison_liga              INT UNSIGNED NOT NULL,\n" + "    lfd_liga_ms              TINYINT UNSIGNED NOT NULL,\n"
      + "    zaehler                  INT UNSIGNED NOT NULL,\n" + "    PRIMARY KEY(Mandator, saison_liga, lfd_liga_ms),\n"
      + "   CONSTRAINT tlmtlfk FOREIGN KEY (Mandator, saison_liga)\n"
      + "            REFERENCES KnightSoft_TippLiga (Mandator, saison_liga),\n"
      + "   CONSTRAINT tlmtmfk FOREIGN KEY (Mandator, zaehler)\n"
      + "            REFERENCES KnightSoft_TippMannschaften (Mandator, zaehler)\n" + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;\n"
      + "CREATE TABLE KnightSoft_TippSpiele\n" + "(\n"
      + "    Mandator                 SMALLINT UNSIGNED NOT NULL DEFAULT \'1\',\n"
      + "    saison_liga              INT UNSIGNED NOT NULL,\n" + "    spieltag                 TINYINT UNSIGNED NOT NULL,\n"
      + "    lfd_spiele_nr            TINYINT UNSIGNED NOT NULL,\n"
      + "    mannschaft_heim          TINYINT UNSIGNED NOT NULL,\n"
      + "    mannschaft_ausw          TINYINT UNSIGNED NOT NULL,\n" + "    tore_heim                TINYINT UNSIGNED,\n"
      + "    tore_ausw                TINYINT UNSIGNED,\n" + "    spielbeginn              DATETIME NOT NULL,\n"
      + "    spiel_beendet            BOOL NOT NULL DEFAULT '0',\n"
      + "    PRIMARY KEY(Mandator, saison_liga, spieltag, lfd_spiele_nr),\n"
      + "    CONSTRAINT tstlfk FOREIGN KEY (Mandator, saison_liga)\n"
      + "                    REFERENCES KnightSoft_TippLiga (Mandator, saison_liga),\n"
      + "    CONSTRAINT tstlmhfk FOREIGN KEY (Mandator, saison_liga, mannschaft_heim)\n"
      + "                    REFERENCES KnightSoft_TippLigaMannschaft " + "(Mandator, saison_liga, lfd_liga_ms),\n"
      + "    CONSTRAINT tstlmafk FOREIGN KEY (Mandator, saison_liga, mannschaft_ausw)\n"
      + "                    REFERENCES KnightSoft_TippLigaMannschaft " + "(Mandator, saison_liga, lfd_liga_ms)\n"
      + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;\n" + "CREATE TABLE KnightSoft_TippMitspieler\n" + "(\n"
      + "    Mandator                 SMALLINT UNSIGNED NOT NULL DEFAULT \'1\',\n"
      + "    spitzname                VARCHAR(50) NOT NULL,\n"
      + "    stufe                    TINYINT UNSIGNED DEFAULT 1 NOT NULL,\n"
      + "    passwort                 CHAR(50) NOT NULL,\n" + "    sende_infos              BOOL NOT NULL DEFAULT '1',\n"
      + "    eintrag_gesperrt         BOOL NOT NULL DEFAULT '0',\n" + "    datum_erstellt           DATE NOT NULL,\n"
      + "    datum_geaendert          DATE,\n" + "    geschlecht               ENUM(\"W\",\"M\") DEFAULT \"W\",\n"
      + "    name                     VARCHAR(50),\n" + "    vorname                  VARCHAR(50),\n"
      + "    strasse                  VARCHAR(50),\n" + "    plz                      CHAR(5),\n"
      + "    ort                      VARCHAR(50),\n" + "    email                    VARCHAR(50) NOT NULL,\n"
      + "    anzahl_fehllogin         TINYINT UNSIGNED DEFAULT 0 NOT NULL,\n" + "    datum_fehllogin          DATETIME,\n"
      + "    datum_login              DATETIME,\n" + "    PRIMARY KEY(Mandator, spitzname)\n"
      + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;\n" + "CREATE TABLE KnightSoft_TippSpieleTipp\n" + "(\n"
      + "    Mandator                 SMALLINT UNSIGNED NOT NULL DEFAULT \'1\',\n"
      + "    saison_liga              INT UNSIGNED NOT NULL,\n" + "    spieltag                 TINYINT UNSIGNED NOT NULL,\n"
      + "    lfd_spiele_nr            TINYINT UNSIGNED NOT NULL,\n" + "    spitzname                VARCHAR(50) NOT NULL,\n"
      + "    tore_heim                TINYINT UNSIGNED,\n" + "    tore_ausw                TINYINT UNSIGNED,\n"
      + "    PRIMARY KEY(Mandator, saison_liga, spieltag, lfd_spiele_nr, spitzname),\n"
      + "    CONSTRAINT tsttsfk FOREIGN KEY (Mandator, saison_liga, spieltag, lfd_spiele_nr)\n"
      + "                    REFERENCES KnightSoft_TippSpiele " + "(Mandator, saison_liga, spieltag, lfd_spiele_nr),\n"
      + "    CONSTRAINT tsttmfk FOREIGN KEY (Mandator, spitzname)\n"
      + "                    REFERENCES KnightSoft_TippMitspieler (Mandator, spitzname)\n"
      + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;\n";
  private static final String BERECHT_SQL_DROP_STRING = "DROP TABLE IF EXISTS KnightSoft_Berechtigungen;\n";
  private static final String BERECHT_SQL_STRING =
      "CREATE TABLE KnightSoft_Berechtigungen\n" + "(\n" + "    system                    VARCHAR(20)    NOT NULL,\n"
          + "    user                        VARCHAR(20)    NOT NULL,\n"
          + "    passwort                    CHAR(50)        NOT NULL,\n"
          + "    stufe                        INT            UNSIGNED NOT NULL,\n"
          + "    anzahl_fehllogin        TINYINT        UNSIGNED NOT NULL,\n" + "    datum_fehllogin        DATETIME,\n"
          + "    datum_login                DATETIME,\n" + "    PRIMARY KEY(system, user)\n"
          + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;\n";
  private String readUserSql;

  private DataBaseDepending myDataBaseDepending;

  /**
   * Constructor
   *
   * @param thisDatabase Connection to database
   * @param servletname Name of the Servlet
   * @exception SQLException if a database access error occurs
   * @exception de.knightsoft.common.TextException to give out a text message
   * @see de.knightsoft.common.TextException
   */
  public DbAdmin(final Connection thisDatabase, final String servletname)
      throws java.sql.SQLException, de.knightsoft.common.TextException {
    super(thisDatabase, KnConst.HTML_BASE, servletname, KnConst.GIF_URL + "16x16/db.png", "DBAdmin", "DBAdmin");
    try {
      myDataBaseDepending = new DataBaseDepending(myDataBase.getMetaData().getDatabaseProductName());

      readUserSql = "SELECT        stufe " //
          + "FROM            KnightSoft_TippMitspieler " //
          + "WHERE        Mandator = 1 AND " //
          + "                spitzname = ? AND " //
          + "                passwort = " + myDataBaseDepending.getSqlPassword(" ? ");
    } catch (final Exception e) {
      // shouldn't happen, ignore it ;-)
      e.printStackTrace();
    }
  }

  /**
   * The Method <code>HTMLPage</code> generates the html formular.
   *
   * @param res HttpServletResponse from the Servlet
   * @param hint Hint to be displayed on the screen
   * @param session The Data of the current Session
   * @param navTyp Navigation type
   * @return HTML code of the formular
   */
  @Override
  protected String htmlPage(final HttpServletResponse res, final String hint, final HttpSession session, final String navTyp) {

    return "        <div style=\"text-align:center;\">\n" + "        <form action=\""
        + res.encodeURL(KnConst.SERVLET_URL + servletName)
        + "\" method=\"POST\" enctype=\"application/x-www-form-urlencoded; charset=utf-8\" " + "accept-charset=\"utf-8\">"
        + "            <input type=\"hidden\" name=\"Stufe\" value=\"2\">"
        + "            <h2>Folgende Funktionen stehen zur Verf&uuml;gung</h2>" + "            <p>&nbsp;</p>"
        + "            <p><input type=\"submit\" name=\"submittype\" value=\"Statistik\"></p>"
        + "            <p><input type=\"submit\" name=\"submittype\" " + "value=\"SQL-Datei Babysitter\"></p>"
        + "            <p><input type=\"submit\" name=\"submittype\" " + "value=\"SQL-Datei Nachhilfe\"></p>"
        + "            <p><input type=\"submit\" name=\"submittype\" " + "value=\"SQL-Datei Fahrgemeinschaft\"></p>"
        + "            <p><input type=\"submit\" name=\"submittype\" " + "value=\"SQL-Datei Gewinnspiel\"></p>"
        + "            <p><input type=\"submit\" name=\"submittype\" " + "value=\"SQL-Datei Banner\"></p>"
        + "            <p><input type=\"submit\" name=\"submittype\" " + "value=\"SQL-Datei Mail\"></p>"
        + "            <p><input type=\"submit\" name=\"submittype\" " + "value=\"SQL-Datei Tipprunde\"></p>"
        + "            <p><input type=\"submit\" name=\"submittype\" " + "value=\"SQL-Datei Berechtigungen\"></p>"
        + "            <p><input type=\"submit\" name=\"submittype\" " + "value=\"SQL-Datei gesamt\"></p>" + "        </form>"
        + "        </div>";
  }

  /**
   * The Method <code>ausgabeStatistik</code> returns the database statistic as html page.
   *
   * @return HTML code of the statistic page
   */
  private String ausgabeStatistik() throws IOException, de.knightsoft.common.TextException {

    try {
      final java.util.Date jetzt = new java.util.Date();
      // Kalender auf aktuelle Uhrzeit/Datum setzen
      final GregorianCalendar myCalendar = new GregorianCalendar();
      myCalendar.setTimeZone(TimeZone.getTimeZone("ECT"));
      myCalendar.setTime(jetzt);

      // E-Mail mit Statistik verschicken, aktueller Tag und vortag
      final SimpleDateFormat myDf = new java.text.SimpleDateFormat("yyyy-MM-dd", Locale.GERMANY);
      return de.knightsoft.knightsoftnet.common.DbStatistik.htmlDbStatistic(myDataBase, myDf.format(myCalendar.getTime()), -1,
          0, 0);
    } catch (final IOException e) {
      throw new de.knightsoft.common.TextException("IOException: " + e.toString(), e);
    } catch (final java.sql.SQLException e) {
      throw new de.knightsoft.common.TextException("SQL-Fehler: " + e.toString(), e);
    }
  }

  /**
   * The Method <code>DBString</code> reads a String field from database and formats it as Database String.
   *
   * @param result resultset to get the data from
   * @param feld name of the Database field
   * @return SQL-String
   */
  private String dbString(final ResultSet result, final String feld) throws java.sql.SQLException {
    return de.knightsoft.common.StringToSql.convert(result.getString(feld));
  }

  /**
   * The Method <code>DBYear</code> reads a String field from database and formats it as Database String.
   *
   * @param result resultset to get the data from
   * @param feld name of the Database field
   * @return SQL-String
   */
  private String dbYear(final ResultSet result, final String feld) throws java.sql.SQLException {
    return de.knightsoft.common.StringToSql.convert(StringUtils.substring(result.getString(feld), 0, 4));
  }

  /**
   * The Method <code>ausgabeSQLBabysitter</code> writes the Babysitter database as SQL file.
   *
   * @param res HttpServletResponse from the Servlet
   */
  private void ausgabeSqlBabysitter(final HttpServletResponse res) throws IOException, de.knightsoft.common.TextException {
    res.setContentType("text/plain; charset=utf8");
    final java.io.PrintStream out = new java.io.PrintStream(res.getOutputStream(), true, "UTF-8");

    final java.util.Date jetzt = new java.util.Date();
    final String datum = DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL).format(jetzt);

    try {
      out.println("#");
      out.println("# Tabelle zur Erstellung der MySQL-Datenbank fuer die " + "\"KnightSoft-Babysittervermittlung\"");
      out.println("#");
      out.println("# " + de.knightsoft.common.Constants.COPYRIGHT + ", created by Manfred Tremmel");
      out.println("#");
      out.println("# Erstellt am: " + datum);
      out.println("#\n");
      out.println("SET NAMES utf8;\n");
      babySitterSql(out);

      out.close();
    } catch (final java.sql.SQLException e) {
      throw new de.knightsoft.common.TextException("Fehler beim Datenbanklesen zur Datenbanksicherung: " + e.toString(), e);
    }
  }

  /**
   * The Method <code>ausgabeSQLNachhilfe</code> writes the Nachhilfe database as SQL file.
   *
   * @param res HttpServletResponse from the Servlet
   */
  private void ausgabeSqlNachhilfe(final HttpServletResponse res) throws IOException, de.knightsoft.common.TextException {
    res.setContentType("text/plain; charset=utf8");
    final java.io.PrintStream out = new java.io.PrintStream(res.getOutputStream(), true, "UTF-8");

    final java.util.Date jetzt = new java.util.Date();
    final String datum = DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL).format(jetzt);

    try {
      out.println("#");
      out.println("# Tabelle zur Erstellung der MySQL-Datenbank fuer die " + "\"KnightSoft-Nachhilfevermittlung\"");
      out.println("#");
      out.println("# " + de.knightsoft.common.Constants.COPYRIGHT + ", created by Manfred Tremmel");
      out.println("#");
      out.println("# Erstellt am: " + datum);
      out.println("#\n");
      out.println("SET NAMES utf8;\n");
      nachhilfeSql(out);

      out.close();
    } catch (final java.sql.SQLException e) {
      throw new de.knightsoft.common.TextException("Fehler beim Datenbanklesen zur Datenbanksicherung: " + e.toString(), e);
    }
  }

  /**
   * The Method <code>ausgabeSQLFahrgemeinschaft</code> writes the Fahrgemeinschaft database as SQL file.
   *
   * @param res HttpServletResponse from the Servlet
   */
  private void ausgabeSqlFahrgemeinschaft(final HttpServletResponse res)
      throws IOException, de.knightsoft.common.TextException {
    res.setContentType("text/plain; charset=utf8");
    final java.io.PrintStream out = new java.io.PrintStream(res.getOutputStream(), true, "UTF-8");

    final java.util.Date jetzt = new java.util.Date();
    final String datum = DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL).format(jetzt);

    try {
      out.println("#");
      out.println("# Tabelle zur Erstellung der MySQL-Datenbank fuer die " + "\"KnightSoft-Fahrgemeinschaftenvermittlung\"");
      out.println("#");
      out.println("# " + de.knightsoft.common.Constants.COPYRIGHT + ", created by Manfred Tremmel");
      out.println("#");
      out.println("# Erstellt am: " + datum);
      out.println("#\n");
      out.println("SET NAMES utf8;\n");
      fahrgemeinschaftSql(out);

      out.close();
    } catch (final java.sql.SQLException e) {
      throw new de.knightsoft.common.TextException("Fehler beim Datenbanklesen zur Datenbanksicherung: " + e.toString(), e);
    }
  }

  /**
   * The Method <code>ausgabeSQLGewinnspiel</code> writes the Gewinnspiel database as SQL file.
   *
   * @param res HttpServletResponse from the Servlet
   */
  private void ausgabeSqlGewinnspiel(final HttpServletResponse res) throws IOException, de.knightsoft.common.TextException {
    res.setContentType("text/plain; charset=utf8");
    final java.io.PrintStream out = new java.io.PrintStream(res.getOutputStream(), true, "UTF-8");

    final java.util.Date jetzt = new java.util.Date();
    final String datum = DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL).format(jetzt);

    try {
      out.println("#");
      out.println("# Tabelle zur Erstellung der MySQL-Datenbank fuer das " + "\"KnightSoft-Gewinnspiel\"");
      out.println("#");
      out.println("# " + de.knightsoft.common.Constants.COPYRIGHT + ", created by Manfred Tremmel");
      out.println("#");
      out.println("# Erstellt am: " + datum);
      out.println("#\n");
      out.println("SET NAMES utf8;\n");
      gewinnspielSql(out);

      out.close();
    } catch (final java.sql.SQLException e) {
      throw new de.knightsoft.common.TextException("Fehler beim Datenbanklesen zur Datenbanksicherung: " + e.toString(), e);
    }
  }

  /**
   * The Method <code>ausgabeSQLBanner</code> writes the Banner database as SQL file.
   *
   * @param res HttpServletResponse from the Servlet
   */
  private void ausgabeSqlBanner(final HttpServletResponse res) throws IOException, de.knightsoft.common.TextException {
    res.setContentType("text/plain; charset=utf8");
    final java.io.PrintStream out = new java.io.PrintStream(res.getOutputStream(), true, "UTF-8");

    final java.util.Date jetzt = new java.util.Date();
    final String datum = DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL).format(jetzt);

    try {
      out.println("#");
      out.println("# Tabelle zur Erstellung der MySQL-Datenbank fuer die \"KnightSoft-Banner\"");
      out.println("#");
      out.println("# " + de.knightsoft.common.Constants.COPYRIGHT + ", created by Manfred Tremmel");
      out.println("#");
      out.println("# Erstellt am: " + datum);
      out.println("#\n");
      out.println("SET NAMES utf8;\n");
      bannerSql(out);

      out.close();
    } catch (final java.sql.SQLException e) {
      throw new de.knightsoft.common.TextException("Fehler beim Datenbanklesen zur Datenbanksicherung: " + e.toString(), e);
    }
  }

  /**
   * The Method <code>ausgabeSQLMail</code> writes the Mail database as SQL file.
   *
   * @param res HttpServletResponse from the Servlet
   */
  private void ausgabeSqlMail(final HttpServletResponse res) throws IOException, de.knightsoft.common.TextException {
    res.setContentType("text/plain; charset=utf8");
    final java.io.PrintStream out = new java.io.PrintStream(res.getOutputStream(), true, "UTF-8");

    final java.util.Date jetzt = new java.util.Date();
    final String datum = DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL).format(jetzt);

    try {
      out.println("#");
      out.println("# Tabelle zur Erstellung der MySQL-Datenbank fuer die " + "\"KnightSoft-Mailzwischenspeicherung\"");
      out.println("#");
      out.println("# " + de.knightsoft.common.Constants.COPYRIGHT + ", created by Manfred Tremmel");
      out.println("#");
      out.println("# Erstellt am: " + datum);
      out.println("#\n");
      out.println("SET NAMES utf8;\n");
      mailSql(out);

      out.close();
    } catch (final java.sql.SQLException e) {
      throw new de.knightsoft.common.TextException("Fehler beim Datenbanklesen zur Datenbanksicherung: " + e.toString(), e);
    }
  }

  /**
   * The Method <code>ausgabeSQLTipp</code> writes the Tipp database as SQL file.
   *
   * @param res HttpServletResponse from the Servlet
   */
  private void ausgabeSqlTipp(final HttpServletResponse res) throws IOException, de.knightsoft.common.TextException {
    res.setContentType("text/plain; charset=utf8");
    final java.io.PrintStream out = new java.io.PrintStream(res.getOutputStream(), true, "UTF-8");

    final java.util.Date jetzt = new java.util.Date();
    final String datum = DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL).format(jetzt);

    try {
      out.println("#");
      out.println("# Tabelle zur Erstellung der MySQL-Datenbank fuer die " + "\"KnightSoft-Tipprunden-DB\"");
      out.println("#");
      out.println("# " + de.knightsoft.common.Constants.COPYRIGHT + ", created by Manfred Tremmel");
      out.println("#");
      out.println("# Erstellt am: " + datum);
      out.println("#\n");
      out.println("SET NAMES utf8;\n");
      tippSql(out);

      out.close();
    } catch (final java.sql.SQLException e) {
      throw new de.knightsoft.common.TextException("Fehler beim Datenbanklesen zur Datenbanksicherung: " + e.toString(), e);
    }
  }

  /**
   * The Method <code>ausgabeSQLBerecht</code> writes the Berechtigungs database as SQL file.
   *
   * @param res HttpServletResponse from the Servlet
   */
  private void ausgabeSqlBerecht(final HttpServletResponse res) throws IOException, de.knightsoft.common.TextException {
    res.setContentType("text/plain; charset=utf8");
    final java.io.PrintStream out = new java.io.PrintStream(res.getOutputStream(), true, "UTF-8");

    final java.util.Date jetzt = new java.util.Date();
    final String datum = DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL).format(jetzt);

    try {
      out.println("#");
      out.println("# Tabelle zur Erstellung der MySQL-Datenbank fuer die " + "\"KnightSoft-Tipprunden-DB\"");
      out.println("#");
      out.println("# " + de.knightsoft.common.Constants.COPYRIGHT + ", created by Manfred Tremmel");
      out.println("#");
      out.println("# Erstellt am: " + datum);
      out.println("#\n");
      out.println("SET NAMES utf8;\n");
      berechtSql(out);

      out.close();
    } catch (final java.sql.SQLException e) {
      throw new de.knightsoft.common.TextException("Fehler beim Datenbanklesen zur Datenbanksicherung: " + e.toString(), e);
    }
  }

  /**
   * The Method <code>ausgabeSQLgesamt</code> writes the whole database as SQL file.
   *
   * @param res HttpServletResponse from the Servlet
   */
  private void ausgabeSqlgesamt(final HttpServletResponse res) throws IOException, de.knightsoft.common.TextException {
    res.setContentType("text/plain; charset=utf8");
    try (final java.io.PrintStream out = new java.io.PrintStream(res.getOutputStream(), true, "UTF-8")) {

      final java.util.Date jetzt = new java.util.Date();
      final String datum = DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL).format(jetzt);

      out.println("#");
      out.println("# Tabelle zur Erstellung der MySQL-Datenbank komplett");
      out.println("#");
      out.println("# " + de.knightsoft.common.Constants.COPYRIGHT + ", created by Manfred Tremmel");
      out.println("#");
      out.println("# Erstellt am: " + datum);
      out.println("#\n");
      out.println("SET NAMES utf8;\n");
      out.println(DbAdmin.BABY_SITTER_SQL_DROP_STRING);
      out.println(DbAdmin.NACHHILFE_SQL_DROP_STRING);
      out.println(DbAdmin.FAHRGEMEINSCHAFT_SQL_DROP_STRING);
      out.println(DbAdmin.BANNER_SQL_DROP_STRING);
      out.println(DbAdmin.GEWINNSPIEL_SQL_DROP_STRING);
      out.println(DbAdmin.MAIL_SQL_DROP_STRING);
      out.println(DbAdmin.TIPP_SQL_DROP_STRING);
      out.println(DbAdmin.BERECHT_SQL_DROP_STRING);
      babySitterSql(out);
      nachhilfeSql(out);
      fahrgemeinschaftSql(out);
      gewinnspielSql(out);
      bannerSql(out);
      mailSql(out);
      tippSql(out);
      berechtSql(out);

    } catch (final java.sql.SQLException e) {
      throw new de.knightsoft.common.TextException("Fehler beim Datenbanklesen zur Datenbanksicherung: " + e.toString(), e);
    }
  }

  /**
   * The Method <code>BabySitterSQL</code> writes the Babysitter database as SQL file.
   *
   * @param out PrintStream to write the Data to
   */
  private void babySitterSql(final java.io.PrintStream out) throws IOException, SQLException {
    // Setze als erstes den "content type" Header der Antwortseite auf Text
    out.print(DbAdmin.BABY_SITTER_SQL_STRING);

    // Lesezugriff auf die Datenbank
    try (final ResultSet result = myDataBase.createStatement().executeQuery("SELECT * FROM KnightSoft_BabySitter")) {
      while (result.next()) {
        out.print("INSERT INTO KnightSoft_BabySitter VALUES(" + result.getString("zaehler") + ", "
            + dbString(result, "sende_infos") + ", " + dbString(result, "eintrag_gesperrt") + ", "
            + dbString(result, "datum_erstellt") + ", " + dbString(result, "datum_geaendert") + ", "
            + dbString(result, "geschlecht") + ", " + dbString(result, "name") + ", " + dbString(result, "vorname") + ", "
            + dbString(result, "strasse") + ", " + dbString(result, "plz") + ", " + dbString(result, "ort") + ", "
            + dbString(result, "email") + ", " + dbString(result, "vorwahl") + ", " + dbString(result, "telefonnummer1") + ", "
            + dbString(result, "telefonnummer2") + ", " + dbString(result, "telefonnummer3") + ", "
            + dbString(result, "passwort") + ", " + dbYear(result, "geburtsjahr") + ", " + dbString(result, "mehrere_kinder")
            + ", " + dbString(result, "koerper_behinderte") + ", " + dbString(result, "geistig_behinderte") + ", "
            + dbString(result, "preis_text") + ", " + dbString(result, "bemerkung") + ", " + dbString(result, "url") + ", "
            + dbString(result, "land") + ", " + dbString(result, "bietesuche") + ", " + result.getString("anzahl_fehllogin")
            + ", " + dbString(result, "datum_fehllogin") + ", " + dbString(result, "datum_login") + ");\n");
      }
    }
  }

  /**
   * The Method <code>NachhilfeSQL</code> writes the Nachhilfe database as SQL file.
   *
   * @param out PrintStream to write the Data to
   */
  private void nachhilfeSql(final java.io.PrintStream out) throws IOException, SQLException {
    // Setze als erstes den "content type" Header der Antwortseite auf Text
    out.print(DbAdmin.NACHHILFE_SQL_STRING);

    // Lesezugriff auf die Datenbank
    try (final ResultSet result = myDataBase.createStatement().executeQuery("SELECT * FROM KnightSoft_Nachhilfe")) {
      while (result.next()) {
        out.print("INSERT INTO KnightSoft_Nachhilfe VALUES(" + result.getString("zaehler") + ", "
            + dbString(result, "sende_infos") + ", " + dbString(result, "eintrag_gesperrt") + ", "
            + dbString(result, "datum_erstellt") + ", " + dbString(result, "datum_geaendert") + ", "
            + dbString(result, "geschlecht") + ", " + dbString(result, "name") + ", " + dbString(result, "vorname") + ", "
            + dbString(result, "strasse") + ", " + dbString(result, "plz") + ", " + dbString(result, "ort") + ", "
            + dbString(result, "email") + ", " + dbString(result, "vorwahl") + ", " + dbString(result, "telefonnummer1") + ", "
            + dbString(result, "telefonnummer2") + ", " + dbString(result, "telefonnummer3") + ", "
            + dbString(result, "passwort") + ", " + dbYear(result, "geburtsjahr") + ", " + dbString(result, "biologie") + ", "
            + dbString(result, "biologie_j_von") + ", " + dbString(result, "biologie_j_bis") + ", "
            + dbString(result, "biologie_s_von") + ", " + dbString(result, "biologie_s_bis") + ", " + dbString(result, "bwl")
            + ", " + dbString(result, "bwl_j_von") + ", " + dbString(result, "bwl_j_bis") + ", " + dbString(result, "bwl_s_von")
            + ", " + dbString(result, "bwl_s_bis") + ", " + dbString(result, "chemie") + ", " + dbString(result, "chemie_j_von")
            + ", " + dbString(result, "chemie_j_bis") + ", " + dbString(result, "chemie_s_von") + ", "
            + dbString(result, "chemie_s_bis") + ", " + dbString(result, "deutsch") + ", " + dbString(result, "deutsch_j_von")
            + ", " + dbString(result, "deutsch_j_bis") + ", " + dbString(result, "deutsch_s_von") + ", "
            + dbString(result, "deutsch_s_bis") + ", " + dbString(result, "englisch") + ", "
            + dbString(result, "englisch_j_von") + ", " + dbString(result, "englisch_j_bis") + ", "
            + dbString(result, "englisch_s_von") + ", " + dbString(result, "englisch_s_bis") + ", "
            + dbString(result, "erdkunde") + ", " + dbString(result, "erdkunde_j_von") + ", "
            + dbString(result, "erdkunde_j_bis") + ", " + dbString(result, "erdkunde_s_von") + ", "
            + dbString(result, "erdkunde_s_bis") + ", " + dbString(result, "franzoesisch") + ", "
            + dbString(result, "franzoesisch_j_von") + ", " + dbString(result, "franzoesisch_j_bis") + ", "
            + dbString(result, "franzoesisch_s_von") + ", " + dbString(result, "franzoesisch_s_bis") + ", "
            + dbString(result, "geschichte") + ", " + dbString(result, "geschichte_j_von") + ", "
            + dbString(result, "geschichte_j_bis") + ", " + dbString(result, "geschichte_s_von") + ", "
            + dbString(result, "geschichte_s_bis") + ", " + dbString(result, "griechisch") + ", "
            + dbString(result, "griechisch_j_von") + ", " + dbString(result, "griechisch_j_bis") + ", "
            + dbString(result, "griechisch_s_von") + ", " + dbString(result, "griechisch_s_bis") + ", "
            + dbString(result, "informatik") + ", " + dbString(result, "informatik_j_von") + ", "
            + dbString(result, "informatik_j_bis") + ", " + dbString(result, "informatik_s_von") + ", "
            + dbString(result, "informatik_s_bis") + ", " + dbString(result, "latein") + ", " + dbString(result, "latein_j_von")
            + ", " + dbString(result, "latein_j_bis") + ", " + dbString(result, "latein_s_von") + ", "
            + dbString(result, "latein_s_bis") + ", " + dbString(result, "maschineschreiben") + ", "
            + dbString(result, "maschineschreiben_j_von") + ", " + dbString(result, "maschineschreiben_j_bis") + ", "
            + dbString(result, "maschineschreiben_s_von") + ", " + dbString(result, "maschineschreiben_s_bis") + ", "
            + dbString(result, "mathematik") + ", " + dbString(result, "mathematik_j_von") + ", "
            + dbString(result, "mathematik_j_bis") + ", " + dbString(result, "mathematik_s_von") + ", "
            + dbString(result, "mathematik_s_bis") + ", " + dbString(result, "physik") + ", " + dbString(result, "physik_j_von")
            + ", " + dbString(result, "physik_j_bis") + ", " + dbString(result, "physik_s_von") + ", "
            + dbString(result, "physik_s_bis") + ", " + dbString(result, "sozialkunde") + ", "
            + dbString(result, "sozialkunde_j_von") + ", " + dbString(result, "sozialkunde_j_bis") + ", "
            + dbString(result, "sozialkunde_s_von") + ", " + dbString(result, "sozialkunde_s_bis") + ", "
            + dbString(result, "stenografie") + ", " + dbString(result, "stenografie_j_von") + ", "
            + dbString(result, "stenografie_j_bis") + ", " + dbString(result, "stenografie_s_von") + ", "
            + dbString(result, "stenografie_s_bis") + ", " + dbString(result, "weiteres_fach1") + ", "
            + dbString(result, "weiteres_fach1_j_von") + ", " + dbString(result, "weiteres_fach1_j_bis") + ", "
            + dbString(result, "weiteres_fach1_s_von") + ", " + dbString(result, "weiteres_fach1_s_bis") + ", "
            + dbString(result, "weiteres_fach2") + ", " + dbString(result, "weiteres_fach2_j_von") + ", "
            + dbString(result, "weiteres_fach2_j_bis") + ", " + dbString(result, "weiteres_fach2_s_von") + ", "
            + dbString(result, "weiteres_fach2_s_bis") + ", " + dbString(result, "weiteres_fach3") + ", "
            + dbString(result, "weiteres_fach3_j_von") + ", " + dbString(result, "weiteres_fach3_j_bis") + ", "
            + dbString(result, "weiteres_fach3_s_von") + ", " + dbString(result, "weiteres_fach3_s_bis") + ", "
            + dbString(result, "preis_text") + ", " + dbString(result, "bemerkung") + ", " + dbString(result, "url") + ", "
            + dbString(result, "land") + ", " + dbString(result, "bietesuche") + ", " + result.getString("anzahl_fehllogin")
            + ", " + dbString(result, "datum_fehllogin") + ", " + dbString(result, "datum_login") + ");\n");
      }
    }
  }

  /**
   * The Method <code>FahrgemeinschaftSQL</code> writes the Fahrgemeinschaft database as SQL file.
   *
   * @param out PrintStream to write the Data to
   */
  private void fahrgemeinschaftSql(final java.io.PrintStream out) throws IOException, SQLException {
    // Setze als erstes den "content type" Header der Antwortseite auf Text
    out.print(DbAdmin.FAHRGEMEINSCHAFT_SQL_STRING);

    // Lesezugriff auf die Datenbank
    try (final ResultSet result = myDataBase.createStatement().executeQuery( //
        "SELECT * FROM KnightSoft_Fahrgemeinschaft")) {
      while (result.next()) {
        out.print("INSERT INTO KnightSoft_Fahrgemeinschaft VALUES(" + result.getString("zaehler") + ", "
            + dbString(result, "sende_infos") + ", " + dbString(result, "eintrag_gesperrt") + ", "
            + dbString(result, "datum_erstellt") + ", " + dbString(result, "datum_geaendert") + ", "
            + dbString(result, "geschlecht") + ", " + dbString(result, "name") + ", " + dbString(result, "vorname") + ", "
            + dbString(result, "strasse") + ", " + dbString(result, "plz") + ", " + dbString(result, "ort") + ", "
            + dbString(result, "email") + ", " + dbString(result, "telefonnummer1") + ", " + dbString(result, "telefonnummer2")
            + ", " + dbString(result, "telefonnummer3") + ", " + dbString(result, "passwort") + ", "
            + dbString(result, "einmalige_fahrt") + ", " + dbString(result, "abfahrtsdatum") + ", "
            + dbString(result, "abfahrtsplz") + ", " + dbString(result, "abfahrtsort") + ", "
            + dbString(result, "abfahrtsstrasse") + ", " + dbString(result, "abfahrtszeit") + ", " + dbString(result, "zielplz")
            + ", " + dbString(result, "zielort") + ", " + dbString(result, "zielstrasse") + ", "
            + dbString(result, "ankunftszeit") + ", " + dbString(result, "bemerkung") + ", " + dbString(result, "url") + ", "
            + dbString(result, "rueckfahrt") + ", " + dbString(result, "rueckfahrtsdatum") + ", "
            + dbString(result, "rueck_abfahrtszeit") + ", " + dbString(result, "rueck_ankunftszeit") + ", "
            + dbString(result, "land") + ", " + dbString(result, "bietesuche") + ", " + result.getString("anzahl_fehllogin")
            + ", " + dbString(result, "datum_fehllogin") + ", " + dbString(result, "datum_login") + ");\n");
      }
    }
  }

  /**
   * The Method <code>GewinnspielSQL</code> writes the Gewinnspiel database as SQL file.
   *
   * @param out PrintStream to write the Data to
   */
  private void gewinnspielSql(final java.io.PrintStream out) throws IOException, SQLException {
    // Setze als erstes den "content type" Header der Antwortseite auf Text
    out.print(DbAdmin.GEWINNSPIEL_SQL_STRING);

    // Lesezugriff auf die Datenbank
    try (final ResultSet resultGame = myDataBase.createStatement().executeQuery("SELECT * FROM KnightSoft_Gewinnspiel")) {
      while (resultGame.next()) {
        out.print("INSERT INTO KnightSoft_Gewinnspiel VALUES(" + resultGame.getString("zaehler") + ", "
            + dbString(resultGame, "eintrag_gesperrt") + ", " + dbString(resultGame, "datum_von") + ", "
            + dbString(resultGame, "datum_bis") + ", " + dbString(resultGame, "datum_ziehung") + ", "
            + dbString(resultGame, "anzahl_preise") + ", " + dbString(resultGame, "system") + ", "
            + dbString(resultGame, "ausgabetext") + ", " + dbString(resultGame, "reihenfolge_wichtig") + ", "
            + dbString(resultGame, "formular_generieren") + ", " + dbString(resultGame, "frage1") + ", "
            + dbString(resultGame, "frage2") + ", " + dbString(resultGame, "frage3") + ", " + dbString(resultGame, "frage4")
            + ", " + dbString(resultGame, "frage5") + ", " + dbString(resultGame, "frage6") + ", "
            + dbString(resultGame, "frage7") + ", " + dbString(resultGame, "frage8") + ", " + dbString(resultGame, "frage9")
            + ", " + dbString(resultGame, "frage10") + ", " + dbString(resultGame, "antwort1") + ", "
            + dbString(resultGame, "antwort2") + ", " + dbString(resultGame, "antwort3") + ", "
            + dbString(resultGame, "antwort4") + ", " + dbString(resultGame, "antwort5") + ", "
            + dbString(resultGame, "antwort6") + ", " + dbString(resultGame, "antwort7") + ", "
            + dbString(resultGame, "antwort8") + ", " + dbString(resultGame, "antwort9") + ", "
            + dbString(resultGame, "antwort10") + ", " + dbString(resultGame, "bild") + ", " + dbString(resultGame, "url")
            + ", " + dbString(resultGame, "gewinnerliste") + ");\n");
      }
    }

    // Lesezugriff auf die Datenbank
    try (final ResultSet resultUsers =
        myDataBase.createStatement().executeQuery("SELECT * FROM KnightSoft_GewinnspielTeilnehmer")) {
      while (resultUsers.next()) {
        out.print("INSERT INTO KnightSoft_GewinnspielTeilnehmer VALUES(" + resultUsers.getString("zaehler") + ", "
            + resultUsers.getString("gewinnspiel_nr") + ", " + resultUsers.getString("gewinnspiel_reihung") + ", "
            + dbString(resultUsers, "sende_infos") + ", " + dbString(resultUsers, "eintrag_gesperrt") + ", "
            + dbString(resultUsers, "antwort_korrekt") + ", " + dbString(resultUsers, "datum_erstellt") + ", "
            + dbString(resultUsers, "system") + ", " + dbString(resultUsers, "geschlecht") + ", "
            + dbString(resultUsers, "name") + ", " + dbString(resultUsers, "vorname") + ", " + dbString(resultUsers, "strasse")
            + ", " + dbString(resultUsers, "plz") + ", " + dbString(resultUsers, "ort") + ", " + dbString(resultUsers, "email")
            + ", " + dbString(resultUsers, "antwort1") + ", " + dbString(resultUsers, "antwort2") + ", "
            + dbString(resultUsers, "antwort3") + ", " + dbString(resultUsers, "antwort4") + ", "
            + dbString(resultUsers, "antwort5") + ", " + dbString(resultUsers, "antwort6") + ", "
            + dbString(resultUsers, "antwort7") + ", " + dbString(resultUsers, "antwort8") + ", "
            + dbString(resultUsers, "antwort9") + ", " + dbString(resultUsers, "antwort10") + ", "
            + dbString(resultUsers, "gewinnerliste") + ");\n");
      }
    }
  }

  /**
   * The Method <code>BannerSQL</code> writes the Banner database as SQL file.
   *
   * @param out PrintStream to write the Data to
   */
  private void bannerSql(final java.io.PrintStream out) throws IOException, SQLException {
    // Setze als erstes den "content type" Header der Antwortseite auf Text
    out.print(DbAdmin.BANNER_SQL_STRING);

    // Lesezugriff auf die Datenbank
    try (final ResultSet resultBanner = myDataBase.createStatement().executeQuery("SELECT * FROM KnightSoft_Banner")) {
      while (resultBanner.next()) {
        out.print("INSERT INTO KnightSoft_Banner VALUES(" + resultBanner.getString("zaehler") + ", "
            + dbString(resultBanner, "banner_html") + ", " + dbString(resultBanner, "url") + ");\n");
      }
    }

    // Lesezugriff auf die Datenbank
    try (final ResultSet resultPartner = myDataBase.createStatement().executeQuery("SELECT * FROM KnightSoft_Bannerpartner")) {
      while (resultPartner.next()) {
        out.print("INSERT INTO KnightSoft_Bannerpartner VALUES(" + resultPartner.getString("zaehler") + ", "
            + resultPartner.getString("banner") + ", " + dbString(resultPartner, "anrede") + ", "
            + dbString(resultPartner, "firma1") + ", " + dbString(resultPartner, "firma2") + ", "
            + dbString(resultPartner, "name") + ", " + dbString(resultPartner, "vorname") + ", "
            + dbString(resultPartner, "strasse") + ", " + dbString(resultPartner, "plz") + ", " + dbString(resultPartner, "ort")
            + ", " + dbString(resultPartner, "telefon") + ", " + dbString(resultPartner, "email") + ", "
            + dbString(resultPartner, "bank") + ", " + dbString(resultPartner, "blz") + ", " + dbString(resultPartner, "konto")
            + ", " + dbString(resultPartner, "user") + ", " + dbString(resultPartner, "passwort") + ");\n");
      }
    }

    // Lesezugriff auf die Datenbank
    try (final ResultSet resultBannerClicks =
        myDataBase.createStatement().executeQuery("SELECT * FROM KnightSoft_Bannerklicks")) {
      while (resultBannerClicks.next()) {
        out.print("INSERT INTO KnightSoft_Bannerklicks VALUES(" + resultBannerClicks.getString("zaehler") + ", "
            + resultBannerClicks.getString("bannerpartner") + ", " + dbString(resultBannerClicks, "useragent") + ", "
            + dbString(resultBannerClicks, "ip") + ", " + dbString(resultBannerClicks, "datum_zeit") + ");\n");
      }
    }
  }

  /**
   * The Method <code>MailSQL</code> writes the Mail database as SQL file.
   *
   * @param out PrintStream to write the Data to
   */
  private void mailSql(final java.io.PrintStream out) throws IOException, SQLException {
    // Setze als erstes den "content type" Header der Antwortseite auf Text
    out.print(DbAdmin.MAIL_SQL_STRING);

    // Lesezugriff auf die Datenbank
    try (final ResultSet result = myDataBase.createStatement().executeQuery("SELECT * FROM KnightSoft_Mail")) {
      while (result.next()) {
        out.print("INSERT INTO KnightSoft_Mail VALUES(" + result.getString("zaehler") + ", " + dbString(result, "provider")
            + ", " + dbString(result, "email") + ");\n");
      }
    }
  }

  /**
   * The Method <code>TippSQL</code> writes the Tipp database as SQL file.
   *
   * @param out PrintStream to write the Data to
   */
  private void tippSql(final java.io.PrintStream out) throws IOException, SQLException {
    // 1. Liga
    out.print(DbAdmin.TIPP_SQL_STRING);

    // Lesezugriff auf die Datenbank
    try (final ResultSet resultLeague =
        myDataBase.createStatement().executeQuery("SELECT * FROM KnightSoft_TippLiga ORDER BY Mandator, saison_liga")) {
      while (resultLeague.next()) {
        out.print("INSERT INTO KnightSoft_TippLiga VALUES(" + resultLeague.getString("Mandator") + ", "
            + resultLeague.getString("saison_liga") + ", "
            + myDataBaseDepending.getSqlBoolean(resultLeague.getBoolean("eintrag_gesperrt")) + ", "
            + dbString(resultLeague, "bezeichnung") + ", " + resultLeague.getString("anz_manschaften") + ", "
            + resultLeague.getString("anz_tab_oben1") + ", " + resultLeague.getString("anz_tab_oben2") + ", "
            + resultLeague.getString("anz_tab_unten1") + ", " + resultLeague.getString("anz_tab_unten2") + ", "
            + resultLeague.getString("punkte_sieg") + ", " + resultLeague.getString("punkte_unent") + ", "
            + resultLeague.getString("punkte_nieder") + ", " + resultLeague.getString("tr_punkte_exakt") + ", "
            + resultLeague.getString("tr_punkte_torver") + ", " + resultLeague.getString("tr_punkte_toto") + ", "
            + resultLeague.getString("tr_punkte_falsch") + ");\n");
      }
    }

    // 2. Mannschaften
    // Lesezugriff auf die Datenbank
    try (final ResultSet resultTeams =
        myDataBase.createStatement().executeQuery("SELECT * FROM KnightSoft_TippMannschaften ORDER BY Mandator, zaehler")) {
      while (resultTeams.next()) {
        out.print("INSERT INTO KnightSoft_TippMannschaften VALUES(" + resultTeams.getString("Mandator") + ", "
            + resultTeams.getString("zaehler") + ", " + dbString(resultTeams, "bezeichnung") + ");\n");
      }
    }

    // 3. Mannschaften -> Liga Zuordnung
    // Lesezugriff auf die Datenbank
    try (final ResultSet resultLeagueTeams = myDataBase.createStatement()
        .executeQuery("SELECT * FROM KnightSoft_TippLigaMannschaft " + "ORDER BY Mandator, saison_liga, lfd_liga_ms")) {
      while (resultLeagueTeams.next()) {
        out.print("INSERT INTO KnightSoft_TippLigaMannschaft VALUES(" + resultLeagueTeams.getString("Mandator") + ", "
            + resultLeagueTeams.getString("saison_liga") + ", " + resultLeagueTeams.getString("lfd_liga_ms") + ", "
            + resultLeagueTeams.getString("zaehler") + ");\n");
      }
    }

    // 4. Spiel
    // Lesezugriff auf die Datenbank
    try (final ResultSet resultMatch = myDataBase.createStatement()
        .executeQuery("SELECT * FROM KnightSoft_TippSpiele " + "ORDER BY Mandator, saison_liga, spieltag, lfd_spiele_nr")) {
      while (resultMatch.next()) {
        out.print("INSERT INTO KnightSoft_TippSpiele VALUES(" + resultMatch.getString("Mandator") + ", "
            + resultMatch.getString("saison_liga") + ", " + resultMatch.getString("spieltag") + ", "
            + resultMatch.getString("lfd_spiele_nr") + ", " + resultMatch.getString("mannschaft_heim") + ", "
            + resultMatch.getString("mannschaft_ausw") + ", " + resultMatch.getString("tore_heim") + ", "
            + resultMatch.getString("tore_ausw") + ", " + dbString(resultMatch, "spielbeginn") + ", "
            + myDataBaseDepending.getSqlBoolean(resultMatch.getBoolean("spiel_beendet")) + ");\n");
      }
    }

    // 5. Mitspieler (Tipper)
    // Lesezugriff auf die Datenbank
    try (final ResultSet resultUser =
        myDataBase.createStatement().executeQuery("SELECT * FROM KnightSoft_TippMitspieler ORDER BY Mandator, spitzname")) {
      while (resultUser.next()) {
        out.print("INSERT INTO KnightSoft_TippMitspieler VALUES(" + resultUser.getString("Mandator") + ", "
            + dbString(resultUser, "spitzname") + ", " + resultUser.getString("stufe") + ", " + dbString(resultUser, "passwort")
            + ", " + myDataBaseDepending.getSqlBoolean(resultUser.getBoolean("sende_infos")) + ", "
            + myDataBaseDepending.getSqlBoolean(resultUser.getBoolean("eintrag_gesperrt")) + ", "
            + dbString(resultUser, "datum_erstellt") + ", " + dbString(resultUser, "datum_geaendert") + ", "
            + dbString(resultUser, "geschlecht") + ", " + dbString(resultUser, "name") + ", " + dbString(resultUser, "vorname")
            + ", " + dbString(resultUser, "strasse") + ", " + dbString(resultUser, "plz") + ", " + dbString(resultUser, "ort")
            + ", " + dbString(resultUser, "email") + ", " + resultUser.getString("anzahl_fehllogin") + ", "
            + dbString(resultUser, "datum_fehllogin") + ", " + dbString(resultUser, "datum_login") + ");\n");
      }
    }

    // 6. Tipps der Mitspieler
    // Lesezugriff auf die Datenbank
    try (final ResultSet resultTipp = myDataBase.createStatement().executeQuery(
        "SELECT * FROM KnightSoft_TippSpieleTipp " + "ORDER BY Mandator, saison_liga, spieltag, spitzname, lfd_spiele_nr")) {
      while (resultTipp.next()) {
        out.print("INSERT INTO KnightSoft_TippSpieleTipp VALUES(" //
            + resultTipp.getString("Mandator") + ", " //
            + resultTipp.getString("saison_liga") + ", " //
            + resultTipp.getString("spieltag") + ", " //
            + resultTipp.getString("lfd_spiele_nr") + ", " //
            + dbString(resultTipp, "spitzname") + ", " //
            + resultTipp.getString("tore_heim") + ", " //
            + resultTipp.getString("tore_ausw") + ");\n");
      }
    }
  }

  /**
   * The Method <code>BerechtSQL</code> writes the Berechtigungs database as SQL file.
   *
   * @param out PrintStream to write the Data to
   */
  private void berechtSql(final java.io.PrintStream out) throws IOException, SQLException {
    // Setze als erstes den "content type" Header der Antwortseite auf Text
    out.print(DbAdmin.BERECHT_SQL_STRING);

    // Lesezugriff auf die Datenbank
    try (final ResultSet result = myDataBase.createStatement().executeQuery("SELECT * FROM KnightSoft_Berechtigungen")) {
      while (result.next()) {
        out.print("INSERT INTO KnightSoft_Berechtigungen VALUES(" + dbString(result, "system") + ", " + dbString(result, "user")
            + ", " + dbString(result, "passwort") + ", " + result.getString("stufe") + ", "
            + result.getString("anzahl_fehllogin") + ", " + dbString(result, "datum_fehllogin") + ", "
            + dbString(result, "datum_login") + ");\n");
      }
    }
  }

  /**
   * The Method <code>doTheWork</code> coordinates all the work in this class
   *
   * @param req HttpServletRequest from the Servlet
   * @param res HttpServletResponse from the Servlet
   * @param session The Data of the current Session
   * @return html code of the formular
   * @exception de.knightsoft.common.TextException when a error in checking the passwords occours
   */
  @Override
  public String doTheWork(final HttpServletRequest req, final HttpServletResponse res, final HttpSession session)
      throws de.knightsoft.common.TextException {

    String formular = StringUtils.EMPTY;

    try {
      if (!allowedToChange(session)) {
        final String user = req.getParameter("user");
        final String password = req.getParameter("password");

        if (StringUtils.isEmpty(user) || StringUtils.isEmpty(password)) {
          return "<h2>Sie haben keine Berechtigung f&uuml;r den DB-Admin-Bereich</h2>\n";
        }

        try (final PreparedStatement readUserSQLStatement = myDataBase.prepareStatement(readUserSql);) {
          readUserSQLStatement.clearParameters();
          readUserSQLStatement.setString(1, user);
          readUserSQLStatement.setString(2, password);
          try (final ResultSet result = readUserSQLStatement.executeQuery()) {

            if (result.next()) {
              final int stufe = result.getInt("stufe");
              if (stufe <= 5) {
                return "<h2>Sie haben keine Berechtigung f&uuml;r den DB-Admin-Bereich</h2>\n";
              } else {
                ausgabeSqlgesamt(res);
                return formular;
              }
            }
          }
        }
      }

      final String Typ = req.getParameter("submittype");

      switch (StringUtils.defaultString(Typ)) {
        case "Statistik":
          formular = ausgabeStatistik();
          break;
        case "SQL-Datei Babysitter":
          ausgabeSqlBabysitter(res);
          break;
        case "SQL-Datei Nachhilfe":
          ausgabeSqlNachhilfe(res);
          break;
        case "SQL-Datei Fahrgemeinschaft":
          ausgabeSqlFahrgemeinschaft(res);
          break;
        case "SQL-Datei Gewinnspiel":
          ausgabeSqlGewinnspiel(res);
          break;
        case "SQL-Datei Banner":
          ausgabeSqlBanner(res);
          break;
        case "SQL-Datei Mail":
          ausgabeSqlMail(res);
          break;
        case "SQL-Datei Tipprunde":
          ausgabeSqlTipp(res);
          break;
        case "SQL-Datei Berechtigungen":
          ausgabeSqlBerecht(res);
          break;
        case "SQL-Datei gesamt":
          ausgabeSqlgesamt(res);
          break;
        default:
          formular = htmlPage(res, null, session, null);
          break;
      }
    } catch (final SQLException e) {
      formular = htmlPage(res, e.getMessage(), session, null);
    } catch (final de.knightsoft.common.TextException e) {
      formular = htmlPage(res, e.toString(), session, null);
    } catch (final IOException e) {
      formular = htmlPage(res, e.getMessage(), session, null);
    }

    return formular;
  }

  /**
   * The Method <code>allowedToSee</code> tells you if the currently logged in user is allowed to access this application.
   *
   * @param session Data of the current session
   * @return true if it is allowed for this user
   */
  @Override
  public boolean allowedToSee(final HttpSession session) {
    boolean returnwert = false;
    if (session != null) {
      if (session.getAttribute(servletName + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_USER
          + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_USER) != null) {
        final int stufe = ((Integer) session.getAttribute(servletName + KnConst.SESSION_STUFE_FT)).intValue();
        returnwert = stufe > 5;
      }
    }
    return returnwert;
  }

  /**
   * The Method <code>allowedToChange</code> tells you if the currently logged in user is allowed to change data in this
   * application.
   *
   * @param session Data of the current session
   * @return true if it is allowed for this user
   */
  @Override
  public boolean allowedToChange(final HttpSession session) {
    boolean returnwert = false;
    if (session != null) {
      if (session.getAttribute(servletName + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_USER
          + de.knightsoft.common.Constants.DB_FIELD_GLOBAL_USER) != null) {
        final int stufe = ((Integer) session.getAttribute(servletName + KnConst.SESSION_STUFE_FT)).intValue();
        returnwert = stufe > 5;
      }
    }
    return returnwert;
  }
}
