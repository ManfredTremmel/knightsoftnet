/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.knightsoftnet.babysitter;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SitterCheck extends de.knightsoft.knightsoftnet.common.Check {
  String vorwahl;
  String geburtsjahr;

  String mehrereK;
  String koerpelK;
  String geistK;

  String preis;

  /**
   * Constructor leer.
   */
  public SitterCheck() {
    super();

    geschlecht = "W";
    vorwahl = StringUtils.EMPTY;
    geburtsjahr = StringUtils.EMPTY;

    mehrereK = "N";
    koerpelK = "N";
    geistK = "N";

    preis = StringUtils.EMPTY;
  }

  /**
   * Constructor aus dem Bild auslesen.
   *
   * @param req HttpServletRequest
   */
  public SitterCheck(final HttpServletRequest req) {
    super(req);

    vorwahl = req.getParameter("Vorwahl");
    geburtsjahr = req.getParameter("Geburtsjahr");

    mehrereK = req.getParameter("mehrereK");
    koerpelK = req.getParameter("koerpelK");
    geistK = req.getParameter("geistK");

    preis = req.getParameter("Preis");
  }

  /**
   * Constructor aus der Datenbank auslesen.
   *
   * @param result result set
   */
  public SitterCheck(final ResultSet result) throws SQLException {
    super(result);

    vorwahl = result.getString("vorwahl");
    geburtsjahr = result.getString("Geburtsjahr");
    if (geburtsjahr.length() == 10) {
      geburtsjahr = geburtsjahr.substring(0, 4);
    }

    mehrereK = result.getString("mehrere_kinder");
    koerpelK = result.getString("koerper_behinderte");
    geistK = result.getString("geistig_behinderte");

    preis = result.getString("preis_text");
    if (preis == null) {
      preis = StringUtils.EMPTY;
    }
  }

  /**
   * check data.
   */
  @Override
  public void checkDaten() throws de.knightsoft.common.TextException {
    super.checkDaten();

    if (vorwahl.length() < 2) {
      throw new de.knightsoft.common.TextException("Die Vorwahl ist nicht korrekt gefüllt!");
    } else {
      if (!StringUtils.isNumeric(vorwahl)) {
        throw new de.knightsoft.common.TextException("Die Telefonvorwahl ist nicht nummerisch!");
      }
    }

    if (StringUtils.isEmpty(geburtsjahr)) {
      throw new de.knightsoft.common.TextException("Bitte geben Sie Ihr Geburtsjahr ein!");
    } else {
      geburtsjahr = checkJahr(geburtsjahr, false, true);
    }

    if (mehrereK == null) {
      mehrereK = "N";
    } else {
      mehrereK = "J";
    }

    if (koerpelK == null) {
      koerpelK = "N";
    } else {
      koerpelK = "J";
    }

    if (geistK == null) {
      geistK = "N";
    } else {
      geistK = "J";
    }
  }

  /**
   * prepare insert.
   *
   * @return sql string
   */
  public String prepareInsert() throws de.knightsoft.common.TextException {
    final StringBuilder sqlStringA = new StringBuilder(128);
    final StringBuilder sqlStringB = new StringBuilder();

    sqlStringA.append(", vorwahl, geburtsjahr, mehrere_kinder, koerper_behinderte, geistig_behinderte");
    sqlStringB.append(", " + de.knightsoft.common.StringToSql.convert(vorwahl) + ", "
        + de.knightsoft.common.StringToSql.convert(geburtsjahr) + ", " + de.knightsoft.common.StringToSql.convert(mehrereK)
        + ", " + de.knightsoft.common.StringToSql.convert(koerpelK) + ", " + de.knightsoft.common.StringToSql.convert(geistK));

    if (StringUtils.isNotEmpty(preis)) {
      sqlStringA.append(", preis_text");
      sqlStringB.append(", ").append(de.knightsoft.common.StringToSql.convert(preis));
    }

    return this.prepareInsert(sqlStringA.toString(), sqlStringB.toString(), "KSBS");
  }

  @Override
  public String prepareUpdate() throws de.knightsoft.common.TextException {
    return super.prepareUpdate() + ", vorwahl=" + de.knightsoft.common.StringToSql.convert(vorwahl) + ", geburtsjahr="
        + de.knightsoft.common.StringToSql.convert(geburtsjahr) + ", preis_text="
        + de.knightsoft.common.StringToSql.convert(preis) + ", mehrere_kinder="
        + de.knightsoft.common.StringToSql.convert(mehrereK) + ", koerper_behinderte="
        + de.knightsoft.common.StringToSql.convert(koerpelK) + ", geistig_behinderte="
        + de.knightsoft.common.StringToSql.convert(geistK);
  }

  /**
   * create html table.
   *
   * @param search do search
   * @return html table
   */
  public String htmlTabelle(final boolean search) {
    final String tabelleString1 = "        <tr>\n" + "            <td align=\"left\">Vorwahl:</td>\n"
        + "            <td align=\"left\">" + de.knightsoft.common.StringToHtml.convert(vorwahl) + "</td>\n" + "        </tr>\n"
        + "        <tr>\n" + "            <td align=\"left\">Geburtsjahr:</td>\n" + "            <td align=\"left\">"
        + de.knightsoft.common.StringToHtml.convert(geburtsjahr) + "</td>\n" + "        </tr>\n";
    final StringBuilder tabelleString2 = new StringBuilder(600);

    tabelleString2.append("        <tr>\n" + "            <td align=\"left\">Betreuung mehrerer Kinder:</td>\n"
        + "            <td align=\"left\">" + jnAusgabe(mehrereK) + "</td>\n" + "        </tr>\n" + "        <tr>\n"
        + "            <td align=\"left\">Betreuung k&ouml;rperlich behinderter Kinder:</td>\n"
        + "            <td align=\"left\">" + jnAusgabe(koerpelK) + "</td>\n" + "        </tr>\n" + "        <tr>\n"
        + "            <td align=\"left\">Betreuung geistig behinderter Kinder:</td>\n" + "            <td align=\"left\">"
        + jnAusgabe(geistK) + "</td>\n" + "        </tr>\n");

    if (StringUtils.isNotEmpty(preis)) {
      tabelleString2.append("        <tr valign=\"top\">\n" + "            <td align=\"left\">Preisvorgaben:</td>\n"
          + "            <td align=\"left\">" + de.knightsoft.common.StringToHtml.convert(preis) + "</td>\n"
          + "        </tr>\n");
    }

    return this.htmlTabelle(tabelleString1, tabelleString2.toString(), search, "Babysitter");
  }

  @Override
  public String htmlHidden() {
    return super.htmlHidden() + "<input type=\"hidden\" name=\"Vorwahl\" value=\""
        + de.knightsoft.common.StringToHtml.convert(vorwahl) + "\">\n" + "<input type=\"hidden\" name=\"Geburtsjahr\" value=\""
        + de.knightsoft.common.StringToHtml.convert(geburtsjahr) + "\">\n" + "<input type=\"hidden\" name=\"mehrereK\" value=\""
        + de.knightsoft.common.StringToHtml.convert(mehrereK) + "\">\n" + "<input type=\"hidden\" name=\"koerpelK\" value=\""
        + de.knightsoft.common.StringToHtml.convert(koerpelK) + "\">\n" + "<input type=\"hidden\" name=\"geistK\" value=\""
        + de.knightsoft.common.StringToHtml.convert(geistK) + "\">\n" + "<input type=\"hidden\" name=\"Preis\" value=\""
        + de.knightsoft.common.StringToHtml.convert(preis) + "\">\n";
  }
}
