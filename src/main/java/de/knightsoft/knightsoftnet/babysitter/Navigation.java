/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.knightsoftnet.babysitter;

import de.knightsoft.common.NavTabStrukt;
import de.knightsoft.knightsoftnet.common.KnConst;

import javax.servlet.http.HttpSession;

/**
 * The <code>Navigation</code> class generates the html navitagion panel of the KnightSoft-Net Babysitter service.
 *
 * @author Manfred Tremmel
 * @version 2.0.1, 28.07.2018
 */
public class Navigation {

  public static final NavTabStrukt[] BABYSITTER_NAV = {
      new NavTabStrukt(0, KnConst.GIF_URL + "16x16/Babysitter_nav.png", "Babysittervermittlung", null, false),
      new NavTabStrukt(3, KnConst.GIF_URL + "16x16/gohome.png", "Index", KnConst.HTML_BASE + "Babysitter/index.html", false),
      new NavTabStrukt(3, KnConst.GIF_URL + "16x16/find.png", "Suchen", KnConst.HTML_BASE + "Babysitter/suchen.html", false),
      new NavTabStrukt(3, KnConst.GIF_URL + "16x16/registrieren.png", "Registrieren",
          KnConst.HTML_BASE + "Babysitter/registrieren.html", false),
      new NavTabStrukt(3, KnConst.GIF_URL + "16x16/login.png", "Login", KnConst.HTML_BASE + "Babysitter/login.html", false),
      new NavTabStrukt(3, KnConst.GIF_URL + "16x16/impressum.png", "Datenschutz",
          KnConst.HTML_BASE + "Babysitter/datenschutz.html", false),
      new NavTabStrukt(3, KnConst.GIF_URL + "16x16/impressum.png", "Impressum", KnConst.HTML_BASE + "Babysitter/impressum.html",
          false),
      new NavTabStrukt(3, KnConst.GIF_URL + "16x16/help.png", "FAQ", KnConst.HTML_BASE + "Babysitter/faq.html", false)};

  /**
   * The Method <code>getNavTabStrukt</code> returns the navigation panel structure.
   *
   * @return navigation panel
   */
  public NavTabStrukt[] getNavTabStrukt() {
    return this.getNavTabStrukt(null);
  }

  /**
   * The Method <code>getNavTabStrukt</code> returns the navigation panel structure.
   *
   * @param session session data
   * @return navigation panel
   */
  public NavTabStrukt[] getNavTabStrukt(final HttpSession session) {
    return Navigation.BABYSITTER_NAV;
  }

}
