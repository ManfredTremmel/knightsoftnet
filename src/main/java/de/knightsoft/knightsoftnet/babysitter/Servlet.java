/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.knightsoftnet.babysitter;

import de.knightsoft.common.NavTabStrukt;
import de.knightsoft.common.TextException;
import de.knightsoft.knightsoftnet.common.KnConst;

import org.apache.commons.lang3.StringUtils;

import javax.naming.InitialContext;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * The <code>Servlet</code> class is written to handle the complete the KnightSoft-Net "Babysitter-Vermittlung".
 *
 * @author Manfred Tremmel
 * @version 1.0.0, 17.08.2006
 */
public class Servlet extends HttpServlet {

  private static final long serialVersionUID = -5086221470815999449L;

  private String servletName;

  private Connection thisDataBase;

  private de.knightsoft.common.AbstractVisualDb[] thisServicesAdmin;

  private Navigation thisNavigation;

  /**
   * init Methode.
   *
   * @param config ServletConfig
   * @see javax.servlet.http.HttpServlet
   * @exception ServletException When something's going wrong
   */
  @Override
  public void init(final ServletConfig config) throws ServletException {
    super.init(config);

    try {
      servletName = "KnightSoftBabysitter";

      final InitialContext ic = new InitialContext();
      final DataSource lDataSource = (DataSource) ic.lookup("java:comp/env/jdbc/db_knightsoft");
      thisDataBase = lDataSource.getConnection();
      ic.close();

      final de.knightsoft.common.AbstractVisualDb[] TippServices = {new Index(thisDataBase, servletName),
          new Search(thisDataBase, servletName), new DataProtection(thisDataBase, servletName),
          new Impressum(thisDataBase, servletName), new Faq(thisDataBase, servletName)};
      thisServicesAdmin = TippServices;

      thisNavigation = new Navigation();
    } catch (final TextException e) {
      throw new ServletException(e);
    } catch (final java.sql.SQLException e) {
      throw new ServletException(e);
    } catch (final javax.naming.NamingException e) {
      throw new ServletException(e);
    }
  }

  /**
   * destroy method closes database connection
   *
   * @see javax.servlet.GenericServlet#destroy()
   */
  @Override
  public void destroy() {
    // Clean up.
    try {
      if (thisDataBase != null) {
        thisDataBase.close();
      }
    } catch (final SQLException ignored) {
      ignored.printStackTrace();
    }
  }

  /**
   * <code>doGet</code> just calls doPost.
   *
   * @param req HttpServletRequest from the servlet
   * @param res HttpServletRespons from the servlet
   * @exception ServletException when somethings going wrong
   * @exception IOException if writing the page doesn't work
   */
  @Override
  public void doGet(final HttpServletRequest req, final HttpServletResponse res) throws ServletException, IOException {
    // Sei faul und mach nie was doppelt, also deligiere
    doPost(req, res);
  }

  /**
   * <code>doPost</code> the main section of the Servlet, delegates the work to different methodes.
   *
   * @param req HttpServletRequest from the servlet
   * @param res HttpServletRespons from the servlet
   * @exception ServletException when somethings going wrong
   * @exception IOException if writing the page doesn't work
   */
  @Override
  public void doPost(final HttpServletRequest req, final HttpServletResponse res) throws ServletException, IOException {
    int pos;
    final HttpSession session = req.getSession(true);
    boolean gefunden = false;
    NavTabStrukt[] myNavTabStrukt = null;

    String typ = req.getParameter("service");
    String buffer = null;

    // Setze als erstes den "content type" Header der Antwortseite auf HTML
    res.setContentType("text/html");

    // Writer für die HTML-Datei holen
    final ServletOutputStream out = res.getOutputStream();

    try {
      if (!session.isNew()) {
        myNavTabStrukt = (NavTabStrukt[]) session.getAttribute(servletName + KnConst.SESSION_NAVI_BS);
      }
      if (myNavTabStrukt == null) {
        myNavTabStrukt = thisNavigation.getNavTabStrukt();
        session.setAttribute(servletName + KnConst.SESSION_NAVI_BS, myNavTabStrukt);
      }
      if (typ == null) {
        typ = (String) session.getAttribute(servletName + "Typ");
      }
      if (typ == null) {
        typ = "Index";
      }
      if ("showhidemenu".equals(typ)) {
        final String element = req.getParameter("element");
        final String show = req.getParameter("show");
        if (element != null && show != null && element.startsWith("nav_id")) {
          myNavTabStrukt[Integer.parseInt(element.substring(6))].setIsopen(Boolean.parseBoolean(show));
          session.setAttribute(servletName + KnConst.SESSION_NAVI_BS, myNavTabStrukt);
        }
        out.println("Dummy");
      } else {
        session.setAttribute(servletName + "Typ", typ);
        while (!gefunden) {
          for (pos = 0; pos < thisServicesAdmin.length && !gefunden; pos++) {
            if (typ.equals(thisServicesAdmin[pos].getServiceName())) {
              buffer = thisServicesAdmin[pos].doTheWork(req, res, session);
              if (thisServicesAdmin[pos].changesNavigation(session)) {
                myNavTabStrukt = (NavTabStrukt[]) session.getAttribute(servletName + KnConst.SESSION_NAVI_BS);
              }
              if (buffer == null) {
                typ = "Index";
              } else if (StringUtils.isNotEmpty(buffer)) {
                out.println(de.knightsoft.knightsoftnet.common.SeitenTemplate.htmlSeite(req, servletName,
                    thisServicesAdmin[pos].getTitle(session), thisServicesAdmin[pos].getJavaScript(session),
                    thisServicesAdmin[pos].preventFromCache(session), KnConst.BABY_SITTER_FARBE, KnConst.BABY_SITTER_TEXT,
                    thisServicesAdmin[pos].getTitle(session), buffer, myNavTabStrukt,
                    thisServicesAdmin[pos].getSelectedField(session), session, res));
              }
              gefunden = true;
            }
          }
          if (!gefunden) {
            typ = "Index";
          }
        }
      }
      out.close();
    } catch (final de.knightsoft.common.TextException e) {
      res.setContentType("text/html");
      final String errorMessage = "        <div align=\"center\">\n" + "            <h2>Ein Fehler ist aufgetreten:<br>"
          + e.toHtml() + "</h2>\n" + "            <form action=\"" + res.encodeURL(KnConst.HTML_BASE + servletName)
          + "\" method=\"POST\" enctype=\"application/x-www-form-urlencoded; charset=utf-8\" " + "accept-charset=\"utf-8\">\n"
          + "                <input type=\"submit\" name=\"service\" value=\"Zur&uuml;ck\">\n" + "            </form>\n"
          + "        </div>\n";
      out.println(de.knightsoft.knightsoftnet.common.SeitenTemplate.htmlSeite(req, servletName,
          "KnightSoft-Net Babysittervermittlung - Fehler", null, true, KnConst.BABY_SITTER_FARBE, KnConst.BABY_SITTER_TEXT,
          "KnightSoft-Net Babysittervermittlung - Fehler", errorMessage, myNavTabStrukt));
      out.close();
    }
  }

  /**
   * <code>getServletInfo</code> returns a little description of the servlet.
   *
   * @return description of the servlet
   */
  @Override
  public String getServletInfo() {
    return "KnightSoft-Net Babysittervermittlung";
  }
}
