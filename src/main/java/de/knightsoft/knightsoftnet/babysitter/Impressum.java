/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.knightsoftnet.babysitter;

import de.knightsoft.knightsoftnet.common.KnConst;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * The <code>Impressum</code> class is based on the abstract class de.knightsoft.common.AbstractVisualDb and implements the
 * impressum page of the KnightSoft Tipprunde.
 *
 *
 * @author Manfred Tremmel
 * @version 2.0.0, 17.08.2006
 */
public class Impressum extends de.knightsoft.common.AbstractVisualDb {

  /**
   * Constructor.
   *
   * @param thisDatabase Connection to database
   * @param servletname Name of the Servlet
   * @exception SQLException if a database access error occurs
   * @exception de.knightsoft.common.TextException to give out a text message
   * @see de.knightsoft.common.TextException
   */
  public Impressum(final Connection thisDatabase, final String servletname)
      throws SQLException, de.knightsoft.common.TextException {
    super(thisDatabase, KnConst.HTML_BASE, servletname, KnConst.GIF_URL + "16x16/impressum.png", "Impressum",
        "KnightSoft-Net Babysittervermittlung - Impressum");
  }

  /**
   * The Method <code>HTMLPage</code> generates the html formular.
   *
   * @param res HttpServletResponse from the Servlet
   * @param hint Hint to be displayed on the screen
   * @param session The Data of the current Session
   * @param navTyp Navigation type
   * @return HTML code of the formular
   */
  @Override
  protected String htmlPage(final HttpServletResponse res, final String hint, final HttpSession session, final String navTyp) {
    return "<p>\n" + "Manfred Tremmel<br>\n" + "Marschalkstra&szlig;e 20<br>&nbsp;<br>\n" + "84419 Schwindegg<br>&nbsp;<br>\n"
        + "Telefon: 0 80 82 / 91 11<br>&nbsp;<br>\n"
        + "E-Mail: <a href=\"mailto:MTremmel@KnightSoft-Net.de\">MTremmel@KnightSoft-Net.de</a><br>\n"
        + "E-Mail (Babysitter-Themen): <a href=\"mailto:Babysitter@KnightSoft-Net.de?subject=Anfrage zu Ihrer Babysittervermittlung\">Babysitter@KnightSoft-Net.de</a><br>\n"
        + "</p>\n";
  }

  /**
   * The Method <code>preventFromCache</code> tells you if this page should be cached or not.
   *
   * @param session Data of the current session
   * @return don't cache this site = true
   */
  @Override
  public boolean preventFromCache(final HttpSession session) {
    return false;
  }

  /**
   * The Method <code>allowedToSee</code> tells you if the currently logged in user is allowed to access this application.
   *
   * @param session Data of the current session
   * @return true if it is allowed for this user
   */
  @Override
  public boolean allowedToSee(final HttpSession session) {
    return true;
  }

  /**
   * The Method <code>allowedToChange</code> tells you if the currently logged in user is allowed to change data in this
   * application.
   *
   * @param session Data of the current session
   * @return true if it is allowed for this user
   */
  @Override
  public boolean allowedToChange(final HttpSession session) {
    return true;
  }

}
