/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.knightsoftnet.babysitter;

import de.knightsoft.common.AbstractVisualDb;
import de.knightsoft.knightsoftnet.common.KnConst;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * The <code>Search</code> class is based on the abstract class de.knightsoft.common.AbstractVisualDb and implements the search
 * function for the KnightSoft Babysitter-Vermittlung. It's no longer a real servlet, only a part of one
 *
 *
 * @author Manfred Tremmel
 * @version 2.0.0, 18.08.2006
 */
public class Search extends de.knightsoft.common.AbstractVisualDb {

  private static final String SEARCH_SQL_ONE =
      "SELECT     * " + "FROM       KnightSoft_BabySitter " + "WHERE      zaehler = ? " + " AND       eintrag_gesperrt = 'N' ";
  private static final String SEARCH_SQL_ALL_LIKE =
      "SELECT     * " + "FROM       KnightSoft_BabySitter " + "WHERE      land = ? " + " AND       bietesuche = ? "
          + " AND       eintrag_gesperrt = 'N' " + " AND       vorwahl like ? " + " ORDER BY  plz, name";

  /**
   * Constructor.
   *
   * @param thisDatabase Connection to database
   * @param servletname Name of the Servlet
   * @exception SQLException if a database access error occurs
   * @exception de.knightsoft.common.TextException to give out a text message
   * @see de.knightsoft.common.TextException
   */
  public Search(final Connection thisDatabase, final String servletname)
      throws java.sql.SQLException, de.knightsoft.common.TextException {
    super(thisDatabase, KnConst.HTML_BASE, servletname, KnConst.GIF_URL + "16x16/find.png", "Suchen", "Suchen", null, null, 0,
        null);
  }

  /**
   * The Method <code>SuchergebnisUebersicht</code> generates the html output of the search overview.
   *
   * @param req HttpServletRequest from the Servlet
   * @param res HttpServletResponse from the Servlet
   * @param result ResultSet to get the entries from
   * @param land country code
   * @param bietesuche search for "Bieter" or "Sucher"
   * @param vorwahl first part of the phone number
   * @return HTML code of the formular
   */
  private String suchergebnisUebersicht(final HttpServletResponse res, final ResultSet result, final String land,
      final String bietesuche, final String vorwahl) throws SQLException {
    int anzahlGefunden = 0;
    String emailText = StringUtils.EMPTY; // 12.03.2000 E-Mail Text vorbereiten
    final StringBuilder inhalt = new StringBuilder(2000);
    inhalt.append("        <div style=\"text-align:center;\">\n");

    while (result.next()) {
      // E-Mail Anschrift aufbereiten
      if (result.getString("geschlecht").equals("W")) {
        emailText = "Sehr geehrte Frau " + result.getString("name") + ",";
      } else {
        emailText = "Sehr geehrter Herr " + result.getString("name") + ",";
      }

      if (anzahlGefunden == 0) {
        inhalt.append("        <div style=\"text-align:center;\">\n"
            + "        <table style=\"margin-left:auto; margin-right:auto;\" border=\"0\" "
            + "cellspacing=\"5\" cellpadding=\"0\">\n" + "            <tr style=\"background-color:#CCCCCC;\">\n"
            + "                <th style=\"text-align:left;\">Name<br>E-Mail</th>\n"
            + "                <th style=\"text-align:left;\">Strasse<br>Ort</th>\n"
            + "                <th style=\"text-align:left;\">Telefon<br>&nbsp;</th>\n"
            + "                <th style=\"text-align:left;\">Details<br>&nbsp;</th>\n" + "            </tr>\n");
      }

      if (anzahlGefunden % 2 == 0) {
        inhalt.append("            <tr style=\"background-color:#FFFFFF;\">\n");
      } else {
        inhalt.append("            <tr style=\"background-color:#CCCCCC;\">\n");
      }
      inhalt.append("                <td style=\"text-align:left;\">"
          + de.knightsoft.common.StringToHtml.convert(result.getString("name")) + ", "
          + de.knightsoft.common.StringToHtml.convert(result.getString("vorname")) + "<br>" + "<a href=\"MAILTO:"
          + de.knightsoft.common.StringToHtml.convert(result.getString("email"))
          + "?subject=Anfrage bezueglich Ihres Babysitter-Angebots im KnightSoft-Net" + "&amp;body="
          + de.knightsoft.common.StringToHtml.convert(emailText) + "\"> "
          + de.knightsoft.common.StringToHtml.convert(result.getString("email")) + "</a></td>\n"
          + "                <td style=\"text-align:left;\">"
          + de.knightsoft.common.StringToHtml.convert(result.getString("strasse")) + "<br>"
          + de.knightsoft.common.StringToHtml.convert(result.getString("plz")) + " "
          + de.knightsoft.common.StringToHtml.convert(result.getString("ort")) + "</td>");
      if (result.getString("telefonnummer2") == null) {
        inhalt.append("                <td style=\"text-align:left;\">"
            + de.knightsoft.common.StringToHtml.convert(result.getString("telefonnummer1")) + "<br>&nbsp;</td>");
      } else {
        inhalt.append("                <td style=\"text-align:left;\">"
            + de.knightsoft.common.StringToHtml.convert(result.getString("telefonnummer1")) + "<br>"
            + de.knightsoft.common.StringToHtml.convert(result.getString("telefonnummer2")) + "</td>");
      }
      inhalt.append("                <td style=\"text-align:center;\">\n" + "                    <form action=\""
          + res.encodeURL(KnConst.HTML_BASE + "Babysitter/suchen.html")
          + "\" method=\"post\" enctype=\"application/x-www-form-urlencoded; charset=utf-8\" " + "accept-charset=\"utf-8\">\n"
          + "                        <p>\n" + "                            <input type=\"hidden\" name=\"Stufe\" value=\"3\">\n"
          + "                            <input type=\"hidden\" name=\"land\" value=\""
          + de.knightsoft.common.StringToHtml.convert(land) + "\">\n"
          + "                            <input type=\"hidden\" name=\"bietesuche\" value=\""
          + de.knightsoft.common.StringToHtml.convert(bietesuche) + "\">\n"
          + "                            <input type=\"hidden\" name=\"Suchstring\" value=\""
          + de.knightsoft.common.StringToHtml.convert(vorwahl) + "\">\n"
          + "                            <input type=\"hidden\" name=\"detailnr\" value=\""
          + de.knightsoft.common.StringToHtml.convert(result.getString("zaehler")) + "\">\n"
          + "                            <input type=\"submit\" name=\"abschicken\" " + "value=\"Details\">\n"
          + "                        </p>\n" + "                    </form>\n" + "                </td>\n"
          + "            </tr>");

      anzahlGefunden++;
    }

    switch (anzahlGefunden) {
      case 0:
        inhalt.append("        <p>Es wurde leider kein Babysitter gefunden.</p>\n");
        break;
      case 1:
        inhalt.append("        </table>\n" + "        </div>\n" + "        <p>Es wurde ein Babysitter gefunden.</p>\n");
        break;
      default:
        inhalt.append("        </table>\n" + "        </div>\n" + "        <p>Es wurden " + anzahlGefunden
            + " Babysitter gefunden.</p>\n");
        break;
    }

    inhalt.append("        <p><a href=\"" + res.encodeURL(KnConst.HTML_BASE + "Babysitter/suchen.html")
        + "\">Erneute Suche</a></p>\n" + "        </div>\n");

    return inhalt.toString();
  }

  /**
   * The Method <code>SuchergebnisDetail</code> generates the html output of the detailed search.
   *
   * @param res HttpServletResponse from the Servlet
   * @param babySitterDaten Babysitter data from the database
   * @param land country code
   * @param bietesuche search for "Bieter" or "Sucher"
   * @param vorwahl first part of the phone number
   * @return HTML code of the formular
   */
  private String suchergebnisDetail(final HttpServletResponse res,
      final de.knightsoft.knightsoftnet.babysitter.SitterCheck babySitterDaten, final String land, final String bietesuche,
      final String vorwahl) {
    return "        <div style=\"text-align:center;\">\n" + "        <form action=\""
        + res.encodeURL(KnConst.HTML_BASE + "Babysitter/suchen.html")
        + "\" method=\"post\" enctype=\"application/x-www-form-urlencoded; charset=utf-8\" " + "accept-charset=\"utf-8\">\n"
        + "            <p><input type=\"hidden\" name=\"Stufe\" value=\"1\">\n"
        + "            <input type=\"hidden\" name=\"land\" value=\"" + de.knightsoft.common.StringToHtml.convert(land)
        + "\">\n" + "            <input type=\"hidden\" name=\"bietesuche\" value=\""
        + de.knightsoft.common.StringToHtml.convert(bietesuche) + "\">\n"
        + "            <input type=\"hidden\" name=\"Suchstring\" value=\"" + de.knightsoft.common.StringToHtml.convert(vorwahl)
        + "\">\n</p>" + babySitterDaten.htmlTabelle(true)
        + "            <p><input type=\"submit\" name=\"submit\" value=\"Zur&uuml;ck\"></p>\n" + "        </form>\n"
        + "        </div>\n";
  }

  /**
   * The Method <code>verarbeitungSucheKompakt</code> is used for a simple search.
   *
   * @param res HttpServletResponse from the Servlet
   * @param land country code
   * @param bietesuche search for "Bieter" or "Sucher"
   * @param vorwahl first part of the phone number
   * @return HTML code of the formular
   */
  private String verarbeitungSucheKompakt(final HttpServletResponse res, final String land, final String bietesuche,
      final String vorwahl) throws de.knightsoft.common.TextException {
    if (vorwahl.length() < 2) {
      throw new de.knightsoft.common.TextException("Die Vorwahl ist nicht korrekt gefüllt, eine Suche ist nicht möglich!");
    }

    try {
      final String searchVorwahl = vorwahl.replace('?', '_').replace('*', '%');

      String resultString = null;
      try (final PreparedStatement searchSQLAllLikeStatement = myDataBase.prepareStatement(Search.SEARCH_SQL_ALL_LIKE)) {
        searchSQLAllLikeStatement.clearParameters();
        searchSQLAllLikeStatement.setString(1, land);
        searchSQLAllLikeStatement.setString(2, bietesuche);
        searchSQLAllLikeStatement.setString(3, searchVorwahl);
        try (final ResultSet result = searchSQLAllLikeStatement.executeQuery()) {
          // Ausgabe der Suchergebnisseite
          resultString = suchergebnisUebersicht(res, result, land, bietesuche, vorwahl);
        }
      }
      return resultString;
    } catch (final java.sql.SQLException e) {
      throw new de.knightsoft.common.TextException("Fehler beim Datenbanksuchen: " + e.toString(), e);
    }
  }

  /**
   * The Method <code>verarbeitungSucheKomplex</code> is used for detailed search. Not implemented here, its just a call to
   * verarbeitungSucheKompakt
   *
   * @param res HttpServletResponse from the Servlet
   * @param land country code
   * @param bietesuche search for "Bieter" or "Sucher"
   * @param vorwahl first part of the phone number
   * @return HTML code of the formular
   */
  private String verarbeitungSucheKomplex(final HttpServletResponse res, final String land, final String bietesuche,
      final String vorwahl) throws de.knightsoft.common.TextException {
    return verarbeitungSucheKompakt(res, land, bietesuche, vorwahl);
  }

  /**
   * The Method <code>verarbeitungDetailansicht</code> is used for to display the detailed search results.
   *
   * @param req HttpServletRequest from the Servlet
   * @param res HttpServletResponse from the Servlet
   * @param land country code
   * @param bietesuche search for "Bieter" or "Sucher"
   * @param vorwahl first part of the phone number
   * @return HTML code of the formular
   */
  private String verarbeitungDetailansicht(final HttpServletRequest req, final HttpServletResponse res, final String land,
      final String bietesuche, final String vorwahl) throws de.knightsoft.common.TextException {
    final String dbNr = req.getParameter("detailnr");

    try {
      // Lesezugriff auf die Datenbank
      de.knightsoft.knightsoftnet.babysitter.SitterCheck babySitterDaten = null;
      try (final PreparedStatement searchSQLOneStatement = myDataBase.prepareStatement(Search.SEARCH_SQL_ONE)) {
        searchSQLOneStatement.clearParameters();
        searchSQLOneStatement.setInt(1, Integer.parseInt(dbNr));
        try (final ResultSet result = searchSQLOneStatement.executeQuery()) {
          if (!result.next()) {
            throw new de.knightsoft.common.TextException("Diese Eintragung existiert nicht mehr!");
          }

          babySitterDaten = new de.knightsoft.knightsoftnet.babysitter.SitterCheck(result);
        }
      }

      // Ausgabe der Suchergebnisseite
      return suchergebnisDetail(res, babySitterDaten, land, bietesuche, vorwahl);
    } catch (final java.sql.SQLException e) {
      throw new de.knightsoft.common.TextException("Fehler beim Datenbankzugriff für Detailansicht: " + e.toString(), e);
    }
  }

  /**
   * The Method <code>HTMLPage</code> generates the html formular.
   *
   * @param res HttpServletResponse from the Servlet
   * @param hint Hint to be displayed on the screen
   * @param session The Data of the current Session
   * @param navTyp Navigation type
   * @return HTML code of the formular
   */
  @Override
  protected String htmlPage(final HttpServletResponse res, final String hint, final HttpSession session, final String navTyp) {
    session.setAttribute(servletName + AbstractVisualDb.SELECTED_FIELD_TEXT + serviceName, "Suchstring");
    return "<p>F&uuml;r Sch&auml;den, die im Rahmen des von uns vermittelten Vertragsver"
        + "h&auml;ltnisses entstehen, k&ouml;nnen wir keine Haftung &uuml;bernehmen.</p>\n"
        + "<p>Hier k&ouml;nnen Sie unsere Datenbank nach geeigneten Babysittern durchsuchen. "
        + "Dazu geben Sie bitte ihre Telefonvorwahl in das Suchfeld ein und dr&uuml;cken den " + "Suchen-Knopf.</p>\n"
        + "<p><b>Hinweise:</b> Sie k&ouml;nnen ab der dritten Stelle den Platzhalter * verwenden, "
        + "um einen gr&ouml;&szlig;eren Bereich abzudecken. Weitere Hinweise auf der <a href=\""
        + res.encodeURL(KnConst.HTML_BASE + "Babysitter/faq.html") + "\">FAQ-Seite</a>.</p>\n"
        + "<div style=\"text-align:center;\">\n"
        + (hint == null ? "" : "    <h2>" + de.knightsoft.common.StringToHtml.convert(hint) + "</h2>\n") + "    <form action=\""
        + res.encodeURL(KnConst.HTML_BASE + "Babysitter/suchen.html")
        + "\" method=\"POST\" enctype=\"application/x-www-form-urlencoded; charset=utf-8\" "
        + "accept-charset=\"utf-8\" OnSubmit=\"return chkSuchen()\">\n"
        + "        <table style=\"margin-left:auto; margin-right:auto;\" cellspacing=\"2\" "
        + "cellpadding=\"2\" border=\"0\">\n" + "            <tr>\n"
        + "                <td style=\"text-align:left;\">Land:</td>\n" + "                <td style=\"text-align:left;\">\n"
        + "                    <input type=\"hidden\" name=\"Stufe\" value=\"1\">\n"
        + "                    <select name=\"land\" size=\"1\">\n"
        + "                        <option value=\"de\">Deutschland</option>\n"
        + "                        <option value=\"at\">&Ouml;sterreich</option>\n"
        + "                        <option value=\"ch\">Schweiz</option>\n" + "                    </select>\n"
        + "                </td>\n" + "            </tr>\n" + "            <tr>\n"
        + "                <td style=\"text-align:left;\">Biete/Suche:</td>\n"
        + "                <td style=\"text-align:left;\">\n" + "                    <select name=\"bietesuche\" size=\"1\">\n"
        + "                        <option value=\"b\">Ich suche einen Babysitter</option>\n"
        + "                        <option value=\"s\">Ich suche Kinder zum babysitten</option>\n"
        + "                    </select>\n" + "                </td>\n" + "            </tr>\n" + "            <tr>\n"
        + "                <td style=\"text-align:left;\">Vorwahl:</td>\n"
        + "                <td style=\"text-align:left;\"><input type=\"Text\" name=\"Suchstring\""
        + " size=\"6\" maxlength=\"6\"></td>\n" + "            </tr>\n" + "            <tr>\n"
        + "                <td colspan=\"2\" style=\"text-align:center;\"><input type=\"submit\" " + "value=\"Suchen\"></td>\n"
        + "            </tr>\n" + "        </table>\n" + "    </form>\n" + "</div>\n";
  }

  /**
   * The Method <code>doTheWork</code> coordinates all the work in this class
   *
   * @param req HttpServletRequest from the Servlet
   * @param res HttpServletResponse from the Servlet
   * @param session The Data of the current Session
   * @return html code of the formular
   * @exception de.knightsoft.common.TextException when a error in checking the passwords occours
   */
  @Override
  public String doTheWork(final HttpServletRequest req, final HttpServletResponse res, final HttpSession session)
      throws de.knightsoft.common.TextException {

    final String stufe = req.getParameter("Stufe");
    final String land = req.getParameter("land");
    final String bietesuche = req.getParameter("bietesuche");
    final String vorwahl = req.getParameter("Suchstring");
    final String navTyp = "suchen";
    String formular = null;

    try {
      session.removeAttribute(servletName + AbstractVisualDb.SELECTED_FIELD_TEXT + serviceName);
      switch (StringUtils.defaultString(stufe)) {
        case "1":
          formular = verarbeitungSucheKompakt(res, land, bietesuche, vorwahl);
          break;
        // Stufe 2 = Suche detailliert (komplexe Selektionsmöglichkeiten
        case "2":
          formular = verarbeitungSucheKomplex(res, land, bietesuche, vorwahl);
          break;
        // Stufe 3 = Bestätigung oder Ablehnung der Löschbestätigung
        case "3":
          formular = verarbeitungDetailansicht(req, res, land, bietesuche, vorwahl);
          break;
        default:
          formular = htmlPage(res, null, session, navTyp);
          break;
      }
    } catch (final de.knightsoft.common.TextException e) {
      formular = htmlPage(res, e.toString(), session, navTyp);
    }

    return formular;
  }

  /**
   * The Method <code>allowedToSee</code> tells you if the currently logged in user is allowed to access this application.
   *
   * @param session Data of the current session
   * @return true if it is allowed for this user
   */
  @Override
  public boolean allowedToSee(final HttpSession session) {
    return true;
  }

  /**
   * The Method <code>allowedToChange</code> tells you if the currently logged in user is allowed to change data in this
   * application.
   *
   * @param session Data of the current session
   * @return true if it is allowed for this user
   */
  @Override
  public boolean allowedToChange(final HttpSession session) {
    return true;
  }
}
