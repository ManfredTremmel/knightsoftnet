/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.knightsoftnet.babysitter;

import de.knightsoft.knightsoftnet.common.KnConst;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * The <code>Index</code> class is based on the abstract class de.knightsoft.common.AbstractVisualDb and implements the index
 * page of the KnightSoft Babysitter service.
 *
 *
 * @author Manfred Tremmel
 * @version 2.0.4, 24.02.2013
 */
public class Index extends de.knightsoft.common.AbstractVisualDb {

  /**
   * Constructor.
   *
   * @param thisDatabase Connection to database
   * @param servletname Name of the Servlet
   * @exception SQLException if a database access error occurs
   * @exception de.knightsoft.common.TextException to give out a text message
   * @see de.knightsoft.common.TextException
   */
  public Index(final Connection thisDatabase, final String servletname)
      throws SQLException, de.knightsoft.common.TextException {
    super(thisDatabase, KnConst.HTML_BASE, servletname, KnConst.GIF_URL + "16x16/gohome.png", "Index",
        "Willkommen bei der Babysittervermittlung");
  }

  /**
   * The Method <code>HTMLPage</code> generates the html formular.
   *
   * @param res HttpServletResponse from the Servlet
   * @param hint Hint to be displayed on the screen
   * @param session The Data of the current Session
   * @param navTyp Navigation type
   * @return HTML code of the formular
   */
  @Override
  protected String htmlPage(final HttpServletResponse res, final String hint, final HttpSession session, final String navTyp) {
    return "<p>Wenn Sie einen Babysitter suchen, k&ouml;nnen Sie unsere Datenbank durchsuchen, "
        + "und bekommen dann eine Auflistung aller eingetragener Babysitter in Ihrer Umgebung." + "</p>\n"
        + "<p>Wenn Sie Sich als Babysitter registrieren wollen, klicken Sie auf den Button " + "<a href=\""
        + res.encodeURL(KnConst.HTML_BASE + "Babysitter/registrieren.html")
        + "\">Registrieren</a> und f&uuml;llen dort bitte das Formular aus. Sie bekommen dann " + "innerhalb weniger\n"
        + "Minuten eine E-Mail von uns, in der Sie Ihr Passwort finden. Mit diesem Passwort "
        + "k&ouml;nnen Sie Sich dann Einloggen und Ihre Registrierung freischalten.</p>\n\n"
        + "<p>Von Seite des Betreibers dieser Webseite wird keinerlei Haftung f&uuml;r die "
        + "eingetragenen Babysitter und deren Inhalt &uuml;bernommen.<br>\n"
        + "Ich behalte mir vor, Eintragungen ohne R&uuml;cksprache zu l&ouml;schen.</p>\n"
        + "<p>Beachten Sie auch die Vermittlungen f&uuml;r <a href=\""
        + res.encodeURL(KnConst.HTML_BASE + "Nachhilfe/index.html") + "\">Nachhilfe</a> und <a href=\""
        + res.encodeURL(KnConst.HTML_BASE + "Fahrgemeinschaft/index.html")
        + "\">Fahrgemeinschaften / Mitfahrgelegenheiten</a> sowie die <a href=\""
        + res.encodeURL(KnConst.HTML_BASE + "Tipprunde/index.html") + "\">Bundesliga-Tipprunde</a>.</p>\n\n"
        + "<p>Wenn Sie Kontakt mit mir aufnehmen wollen, finden Sie die Adresse auf der <a href=\""
        + res.encodeURL(KnConst.HTML_BASE + "Babysitter/impressum.html") + "\">Impressum-Seite</a>.</p>\n"
        + "<div class=\"up\">Letztes Update: 20.01.2024</div>\n";
  }

  /**
   * The Method <code>preventFromCache</code> tells you if this page should be cached or not.
   *
   * @param session Data of the current session
   * @return don't cache this site = true
   */
  @Override
  public boolean preventFromCache(final HttpSession session) {
    return false;
  }

  /**
   * The Method <code>allowedToSee</code> tells you if the currently logged in user is allowed to access this application.
   *
   * @param session Data of the current session
   * @return true if it is allowed for this user
   */
  @Override
  public boolean allowedToSee(final HttpSession session) {
    return true;
  }

  /**
   * The Method <code>allowedToChange</code> tells you if the currently logged in user is allowed to change data in this
   * application.
   *
   * @param session Data of the current session
   * @return true if it is allowed for this user
   */
  @Override
  public boolean allowedToChange(final HttpSession session) {
    return true;
  }

}
