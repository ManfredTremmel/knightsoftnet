/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.knightsoftnet.babysitter;

import de.knightsoft.common.Constants;
import de.knightsoft.common.StringToHtml;
import de.knightsoft.common.TextException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

public class ChangeServlet extends de.knightsoft.knightsoftnet.common.AbstractChangeServlet {

  private static final long serialVersionUID = 8301134354834905483L;

  /**
   * init-Methode.
   */
  @Override
  public void init(final ServletConfig config) throws ServletException {
    service = "Babysitter";
    servlet = "KnightSoftBabysitterPflege";
    servletReg = "KnightSoftBabysitterInsert";
    wbild = de.knightsoft.knightsoftnet.common.KnConst.WBILD_BABY_CHANGE;
    wurl = de.knightsoft.knightsoftnet.common.KnConst.WURL_BABY_CHANGE;
    wbildReg = de.knightsoft.knightsoftnet.common.KnConst.WBILD_BABY_REG;
    wurlReg = de.knightsoft.knightsoftnet.common.KnConst.WURL_BABY_REG;
    dbTabelle = "KnightSoft_BabySitter";
    loginCount = "BSLoginCounter";
    loginDbNummer = "BSLoginDBNummer";
    homeUrl = de.knightsoft.knightsoftnet.common.KnConst.HTML_BS_URL;
    emailAb = de.knightsoft.knightsoftnet.common.KnConst.EMAIL_BS;
    logoTextFarbe = de.knightsoft.knightsoftnet.common.KnConst.BABY_SITTER_FARBE;
    logoText = de.knightsoft.knightsoftnet.common.KnConst.BABY_SITTER_TEXT;
    thisNav = de.knightsoft.knightsoftnet.babysitter.Navigation.BABYSITTER_NAV;
    homePage = de.knightsoft.knightsoftnet.common.KnConst.HTML_BASE + "Babysitter/index.html";
    passwortStart = "KSBS";
    super.init(config);
  }

  /**
   * Ändern-Seite ausgeben.
   */
  @Override
  protected void changeWindow(final HttpServletRequest req, final HttpServletResponse res, final ServletOutputStream out,
      final long cookieDbNr, final String hinweis) throws IOException, de.knightsoft.common.TextException {
    de.knightsoft.knightsoftnet.babysitter.SitterCheck babySitterDaten = null;
    try {
      babySitterDaten = new de.knightsoft.knightsoftnet.babysitter.SitterCheck(leseDbSatz(cookieDbNr));
    } catch (final java.sql.SQLException e) {
      throw new TextException("Fehler beim Datenbankzugriff: " + e.toString(), e);
    }

    final String htmlSeite = this.changeHtml(req, res, de.knightsoft.knightsoftnet.common.KnConst.JAVA_SCRIPT_URL, hinweis,
        babySitterDaten, servlet);
    out.println(htmlSeite);
  }

  @Override
  protected String changeHtml(final HttpServletRequest req, final HttpServletResponse res, final String hinweis)
      throws de.knightsoft.common.TextException {
    final de.knightsoft.knightsoftnet.babysitter.SitterCheck aCheck = new de.knightsoft.knightsoftnet.babysitter.SitterCheck();
    return this.changeHtml(req, res, de.knightsoft.knightsoftnet.common.KnConst.JAVA_SCRIPT_URL, hinweis, aCheck, servletReg);
  }

  /**
   * change html.
   *
   * @param req HttpServletRequest
   * @param res HttpServletResponse
   * @param javaScriptServiceUrl java script service url
   * @param hinweis hint text
   * @param psitterCheck check
   * @param servletName servlet name
   * @return created html
   */
  public String changeHtml(final HttpServletRequest req, final HttpServletResponse res, final String javaScriptServiceUrl,
      final String hinweis, final SitterCheck psitterCheck, final String servletName)
      throws de.knightsoft.common.TextException {
    final StringBuilder sb = new StringBuilder(2000);
    sb.append("                <tr>\n" + "                    <td style=\"text-align:left;\">Vorwahl des Ortsbereiches:<br>"
        + "<div class=\"small\">(dient zur Suche, bitte keine Mobil-Vorwahl angeben)</div></td>\n"
        + "                    <td style=\"text-align:left;\"><input type=\"text\" "
        + "name=\"Vorwahl\" size=\"6\" maxlength=\"6\" value=\""
        + de.knightsoft.common.StringToHtml.convert(psitterCheck.vorwahl) + "\" required=\"required\"></td>\n"
        + "                </tr>\n" + "                <tr>\n"
        + "                    <td style=\"text-align:left;\">Geburtsjahr:</td>\n"
        + "                    <td style=\"text-align:left;\"><input type=\"number\" "
        + "name=\"Geburtsjahr\" min=\"1900\" max=\"3000\" value=\""
        + de.knightsoft.common.StringToHtml.convert(psitterCheck.geburtsjahr) + "\" required=\"required\"></td>\n"
        + "                </tr>\n" + "                <tr>\n"
        + "                    <td style=\"text-align:left;\">Ich betreue auch:</td>\n");
    if ("J".equals(psitterCheck.mehrereK)) {
      sb.append("                    <td style=\"text-align:left;\"><input type=\"Checkbox\" "
          + "name=\"mehrereK\" checked>mehrere Kinder</td>\n");
    } else {
      sb.append("                    <td style=\"text-align:left;\"><input type=\"Checkbox\" "
          + "name=\"mehrereK\">mehrere Kinder</td>\n");
    }
    sb.append("                </tr>\n");

    sb.append("                <tr>\n" + "                    <td style=\"text-align:left;\"></td>\n");
    if ("J".equals(psitterCheck.koerpelK)) {
      sb.append("                    <td style=\"text-align:left;\"><input type=\"Checkbox\" "
          + "name=\"koerpelK\" checked>k&ouml;rperlich behinderte Kinder</td>\n");
    } else {
      sb.append("                    <td style=\"text-align:left;\"><input type=\"Checkbox\" "
          + "name=\"koerpelK\">k&ouml;rperlich behinderte Kinder</td>\n");
    }
    sb.append("                </tr>\n");

    sb.append("                <tr>\n" + "                    <td style=\"text-align:left;\"></td>\n");
    if ("J".equals(psitterCheck.geistK)) {
      sb.append("                    <td style=\"text-align:left;\"><input type=\"Checkbox\" "
          + "name=\"geistK\" checked>geistig behinderte Kinder</td>\n");
    } else {
      sb.append("                    <td style=\"text-align:left;\"><input type=\"Checkbox\" "
          + "name=\"geistK\">geistig behinderte Kinder</td>\n");
    }
    sb.append("                </tr>\n");

    sb.append("                <tr>\n"
        + "                    <td style=\"text-align:left; color:gray;\" valign=\"top\"> &sup1;) " + "Preisvorstellung:</td>\n"
        + "                    <td style=\"text-align:left;\"><textarea name=\"Preis\" rows=\"3\" " + "cols=\"30\">"
        + StringToHtml.convert(psitterCheck.preis, false, false) + "</textarea></td>\n" + "                </tr>\n");

    return aendernHtml(req, res, javaScriptServiceUrl, hinweis, psitterCheck, sb.toString(), servletName);
  }

  /**
   * Ändern des ausgewählten Datensatzes.
   */
  @Override
  protected long verarbeitungAendern(final HttpServletRequest req, final HttpServletResponse res, final ServletOutputStream out)
      throws de.knightsoft.common.TextException, IOException {
    final long cookieDbNr = super.verarbeitungAendern(req, res, out);

    // Eingaben einlesen
    final de.knightsoft.knightsoftnet.babysitter.SitterCheck babySitterDaten =
        new de.knightsoft.knightsoftnet.babysitter.SitterCheck(req);

    // Die eingegebenen Daten prüfen
    babySitterDaten.checkDaten();

    upDateDbSatz(req, res, out, babySitterDaten.prepareUpdate(), cookieDbNr);

    return cookieDbNr;
  }

  @Override
  public String getServletInfo() {
    return "Change/update information about babysitters in database\n\n" + Constants.COPYRIGHT;
  }

}
