/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.knightsoftnet.babysitter;

import de.knightsoft.common.TextException;
import de.knightsoft.knightsoftnet.common.KnConst;

import org.apache.commons.lang3.StringUtils;

import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import java.io.IOException;
import java.sql.Connection;

public class InsertServlet extends HttpServlet {
  private static final long serialVersionUID = 8530385001236968716L;

  /**
   * Nach der Eingabe, Daten auf Gültigkeit prüfen und zur Bestätigung durch den Benutzer nochmals ausgeben.
   */
  private void verarbeitungStufe1(final HttpServletRequest req, final HttpServletResponse res, final ServletOutputStream out,
      final de.knightsoft.knightsoftnet.babysitter.SitterCheck babySitterDaten)
      throws de.knightsoft.common.TextException, IOException {
    try {
      babySitterDaten.checkDaten();

      final String inhalt = "<div style=\"text-align:center;\"><h2>Pr&uuml;fen Sie die eingegebenen Daten, "
          + "dr&uuml;cken Sie den \"Best&auml;tigen\" Knopf wenn die Daten korrekt sind "
          + "und Sie mit den Konditionen einverstanden sind</h2></div>\n" + "<p>&nbsp;</p>\n" + "<form action=\""
          + res.encodeURL(KnConst.SERVLET_URL) + "KnightSoftBabysitterInsert\" method=\"POST\" "
          + "enctype=\"application/x-www-form-urlencoded; charset=utf-8\" " + "accept-charset=\"utf-8\">\n"
          + "<input type=\"hidden\" name=\"Stufe\" value=\"2\">\n" + babySitterDaten.htmlHidden() + "\n"
          + babySitterDaten.htmlTabelle(false) + "\n" + de.knightsoft.knightsoftnet.common.KnConst.HINWEIS_REGISTER + "\n"
          + "<p style=\"text-align:center;\"><input type=\"submit\" name=\"Submittype\" "
          + "value=\"Best&auml;tigen\"><input type=\"submit\" name=\"Submittype\" " + "value=\"Abbrechen\"></p>\n"
          + "</form>\n";

      final String htmlSeite = de.knightsoft.knightsoftnet.common.SeitenTemplate.htmlSeite(req, "KnightSoftBabysitterInsert",
          "Daten werden eingef&uuml;gt", "", true, de.knightsoft.knightsoftnet.common.KnConst.BABY_SITTER_FARBE,
          de.knightsoft.knightsoftnet.common.KnConst.BABY_SITTER_TEXT, "Prüfung der Eingaben", inhalt,
          de.knightsoft.knightsoftnet.babysitter.Navigation.BABYSITTER_NAV);

      // HTML-Datei für Insertbestätigung schreiben
      out.println(htmlSeite);

    } catch (final de.knightsoft.common.TextException e) {
      final de.knightsoft.knightsoftnet.babysitter.ChangeServlet MyChangeServlet =
          new de.knightsoft.knightsoftnet.babysitter.ChangeServlet();

      MyChangeServlet.setService("Babysitter");
      MyChangeServlet.setWbildReg(de.knightsoft.knightsoftnet.common.KnConst.WBILD_BABY_REG);
      MyChangeServlet.setWurlReg(de.knightsoft.knightsoftnet.common.KnConst.WURL_BABY_REG);
      if (babySitterDaten.mehrereK == null) {
        babySitterDaten.mehrereK = "N";
      }
      if (babySitterDaten.koerpelK == null) {
        babySitterDaten.koerpelK = "N";
      }
      if (babySitterDaten.geistK == null) {
        babySitterDaten.geistK = "N";
      }
      if (babySitterDaten.news == null) {
        babySitterDaten.news = "J";
      }
      out.println(MyChangeServlet.changeHtml(req, res, de.knightsoft.knightsoftnet.common.KnConst.JAVA_SCRIPT_URL,
          "Fehler beim Einfügen: " + e.getMessage(), babySitterDaten, //
          "KnightSoftBabysitterInsert"));
    }
  }

  /**
   * Die geprüften Daten in die Datenbank sichern und eine email an den Benutzer verschicken.
   */
  private void verarbeitungStufe2(final HttpServletRequest req, final HttpServletResponse res, final ServletOutputStream out,
      final de.knightsoft.knightsoftnet.babysitter.SitterCheck babySitterDaten)
      throws IOException, ServletException, TextException {
    String sqlString = StringUtils.EMPTY;

    try {
      final InitialContext ic = new InitialContext();
      final DataSource lDataSource = (DataSource) ic.lookup("java:comp/env/jdbc/db_knightsoft");
      try (final Connection myDataBase = lDataSource.getConnection()) {

        sqlString = babySitterDaten.prepareInsert();
        myDataBase.createStatement().executeUpdate("INSERT INTO KnightSoft_BabySitter " + sqlString);

        String content = "<div style=\"text-align:center;\"><h2>Sie wurden als Babysitter in unsere "
            + "Datenbank aufgenommen.</h2>\n" + "<p>Eine E-Mail mit dem Passwort zum &auml;ndern oder l&ouml;schen Ihrer "
            + "Eintragung wird Ihnen automatisch zugesendet.</p>\n"
            + "<p>Bitte beachten Sie, dass Ihr Eintrag gesperrt bleibt, bis Sie sich einmal "
            + "mit dem zugesandten Passwort erfolgreich eingeloggt haben.</p>\n" + "<a href=\""
            + res.encodeURL(KnConst.SERVLET_URL + "KnightSoftBabysitterPflege?Stufe=0") + "\"> zur&uuml;ck</a></div>\n";

        // E-Mail versenden
        try {
          sendPasswort(babySitterDaten.email, babySitterDaten.passwort, babySitterDaten.geschlecht, babySitterDaten.name);
        } catch (final IOException e) {
          content += "Fehler beim Versenden der E-Mail: " + e.toString() + "\n"; // NOPMD
        }

        out.println(de.knightsoft.knightsoftnet.common.SeitenTemplate.htmlSeite(req, "KnightSoftBabysitterInsert",
            "Aufnahme als Babysitter war erfolgreich", "", true, de.knightsoft.knightsoftnet.common.KnConst.BABY_SITTER_FARBE,
            de.knightsoft.knightsoftnet.common.KnConst.BABY_SITTER_TEXT, "Registrierung war erfolgreich", content,
            de.knightsoft.knightsoftnet.babysitter.Navigation.BABYSITTER_NAV));
      } finally {
        ic.close();
      }
    } catch (final java.sql.SQLException e) {
      throw new ServletException(e);
    } catch (final javax.naming.NamingException e) {
      throw new ServletException(e);
    }
  }

  /**
   * Nach der Eingabe, Daten auf Gültigkeit prüfen und zur Bestätigung durch den Benutzer nochmals ausgeben.
   */
  private void verarbeitungAbbruch(final HttpServletRequest req, final HttpServletResponse res, final ServletOutputStream out)
      throws IOException {
    out.println(de.knightsoft.knightsoftnet.common.SeitenTemplate.htmlSeite(req, "KnightSoftBabysitterInsert",
        "Abbruch der Babysittereintragung", "", true, KnConst.BABY_SITTER_FARBE, KnConst.BABY_SITTER_TEXT,
        "Abbruch der Eintragung",
        "<div style=\"text-align:center;\"><h2>Sie haben den Abbruch-Knopf bet&auml;tigt "
            + "und wurden nicht in unsere Datenbank aufgenommen.</h2>\n<a href=\""
            + res.encodeURL(KnConst.SERVLET_URL + "KnightSoftBabysitterPflege?Stufe=0") + "\"> zur&uuml;ck</a></div>\n",
        de.knightsoft.knightsoftnet.babysitter.Navigation.BABYSITTER_NAV));
  }

  /**
   * Nach der Eingabe, Daten auf Gültigkeit prüfen und zur Bestätigung durch den Benutzer nochmals ausgeben.
   */
  private void sendPasswort(final String empfaengerEMail, final String passwort, final String geschlecht, final String name)
      throws IOException {
    final String eMailText = KnConst.registerEmail(name, geschlecht, empfaengerEMail, passwort, "Babysitter");

    new de.knightsoft.common.SendEMail(KnConst.EMAIL_BS, de.knightsoft.common.Constants.ORGANISATION, empfaengerEMail,
        "Registrierung in der " + de.knightsoft.common.Constants.COMPANY + " Babysitter-Vermittlung", eMailText);
  }

  /**
   * Auslesen der gePOSTeden Suchkriterien, zugriff auf die Datenbank und Einfügen des Eintrags, wenn möglich.
   */
  @Override
  public void doPost(final HttpServletRequest req, final HttpServletResponse res) throws ServletException, IOException {
    // Setze als erstes den "content type" Header der Antwortseite auf HTML
    res.setContentType("text/html");

    // Writer für die HTML-Datei holen
    final ServletOutputStream out = res.getOutputStream();

    // Eingaben einlesen
    final de.knightsoft.knightsoftnet.babysitter.SitterCheck babySitterDaten =
        new de.knightsoft.knightsoftnet.babysitter.SitterCheck(req);

    try {
      if ("1".equals(babySitterDaten.stufe)) {
        verarbeitungStufe1(req, res, out, babySitterDaten);
      } else {
        final String typ = req.getParameter("Submittype");

        if ("Abbrechen".equals(typ)) {
          verarbeitungAbbruch(req, res, out);
        } else {
          verarbeitungStufe2(req, res, out, babySitterDaten);
        }
      }
      out.close();
    } catch (final de.knightsoft.common.TextException e) {
      // HTML-Datei für Fehlermeldung schreiben
      out.println(de.knightsoft.knightsoftnet.common.SeitenTemplate.htmlSeite(req, "KnightSoftBabysitterInsert",
          "Fehler bei Datenbank-Insert", "", true, KnConst.BABY_SITTER_FARBE, KnConst.BABY_SITTER_TEXT,
          "Fehler beim Anlegen der Daten",
          "<div style=\"text-align:center;\"><h2>" + e.toHtml() + "</h2>\n<a href=\""
              + res.encodeURL(KnConst.SERVLET_URL + "KnightSoftBabysitterPflege?Stufe=0") + "\"> zur&uuml;ck</a></div>\n",
          de.knightsoft.knightsoftnet.babysitter.Navigation.BABYSITTER_NAV));
      out.close();
    }
  }

  @Override
  public String getServletInfo() {
    return "Insert a new babysitter into the database\n\n" + de.knightsoft.common.Constants.COPYRIGHT;
  }
}
