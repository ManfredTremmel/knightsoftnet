/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.common;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.StringTokenizer;

/**
 * <code>CsvKonvert</code> is used to im- and export csv lists
 *
 * @author Manfred Tremmel
 * @version 1.0.3, 06.08.2005
 */
public class CsvKonvert {

  private static final char DEFAULT_SEPERATOR = ';';
  private final char seperator;

  private String[] csvElements;
  public String[] header;

  /**
   * Constructor.
   *
   * @param header table of header fields
   */
  public CsvKonvert(final String[] header) {
    this(header, CsvKonvert.DEFAULT_SEPERATOR);
  }

  /**
   * Constructor.
   *
   * @param header table of header fields
   * @param splitchar character to split the lines at.
   */
  public CsvKonvert(final String[] header, final char splitchar) {
    this.header = header;
    csvElements = new String[header.length];
    seperator = splitchar;
  }

  /**
   * Constructor.
   *
   * @param headerString haederline that has to be splitted in separate fields
   */
  public CsvKonvert(final String headerString) {
    this(headerString, CsvKonvert.DEFAULT_SEPERATOR);
  }

  /**
   * Constructor.
   *
   * @param haederString haederline that has to be splitted in separate fields
   * @param splitchar character to split the lines at.
   */
  public CsvKonvert(final String haederString, final char splitchar) {
    seperator = splitchar;
    initHeader(haederString);
  }

  /**
   * The <code>SplittCSVString</code> method splitts a csv line into separate fields.
   *
   * @param ueberschriftString header line to split into separate fields
   */
  private void initHeader(final String ueberschriftString) {
    final StringTokenizer st = new StringTokenizer(ueberschriftString, String.valueOf(seperator), false);
    final int i = st.countTokens();
    if (i > 0) {
      header = new String[i];
      csvElements = new String[i];
      splittCsvString(ueberschriftString);
      for (int j = 0; j < i; j++) {
        if (csvElements[j] == null) {
          header[j] = StringUtils.EMPTY;
        } else {
          header[j] = csvElements[j].trim();
        }
        csvElements[j] = null;
      }
    } else {
      header = null;
      csvElements = null;
    }
  }

  /**
   * The <code>splittCSVString</code> method splitts a csv line into separate fields.
   *
   * @param csvString line to split into separate fields
   * @return true, if line is splitted complete, false, if it's a incomplete line
   */
  public final boolean splittCsvString(final String csvString) {
    final char[] csvCharTab = csvString.toCharArray();
    int tmpCharTabPos = 0;
    final char[] tmpCharTab = new char[csvString.length()];
    boolean stringOpen = false;
    int pos = 0;
    int count = 0;

    if (csvElements != null) {
      // Zunächst mal alle Felder löschen
      Arrays.fill(csvElements, null);

      // Den String Durchrattern
      for (count = 0; count < csvCharTab.length && pos < csvElements.length; count++) {
        // Einzelne Zeichen prüfen
        if (csvCharTab[count] == '\"') {
          // Das Anführungszeichen dient zur Kennzeichnung eines Strings,
          // kann aber auch im String vorkommen, wird dann aber maskiert,
          // sprich es sind zwei aufeinander folgende Anführungszeichen
          // Voraussetzung dafür ist, dass bereits am Anfang des Strings
          // ein Anführungszeichen stand.
          if (stringOpen) {
            if (count + 1 < csvCharTab.length && csvCharTab[count + 1] == csvCharTab[count]) {
              count++;
              tmpCharTab[tmpCharTabPos++] = csvCharTab[count];
            } else {
              stringOpen = false;
            }
          } else {
            if (count == 0 || csvCharTab[count - 1] == seperator) {
              stringOpen = true;
            } else {
              tmpCharTab[tmpCharTabPos++] = csvCharTab[count];
            }
          }
        } else {
          // alle anderen Zeichen werden einfach so übernommen
          // Semicolon (Trennzeichen), dient zum Trennen der einzelnen
          // Felder, kann aber auch innerhalb eines Strings vorkommen,
          // dieser muß allerdings dann zu beginn ein Anführungszeichen
          // haben.
          if (csvCharTab[count] == seperator) {
            if (stringOpen) {
              tmpCharTab[tmpCharTabPos++] = csvCharTab[count];
            } else {
              csvElements[pos++] = new String(tmpCharTab, 0, tmpCharTabPos);
              tmpCharTabPos = 0;
            }
          } else {
            tmpCharTab[tmpCharTabPos++] = csvCharTab[count];
          }
        }
      }
      if (pos < csvElements.length) {
        csvElements[pos++] = new String(tmpCharTab, 0, tmpCharTabPos);
      }
    }
    return !stringOpen;
  }

  /**
   * The <code>JoinCSVElements</code> method creates a csv string out of a table of strings.
   *
   * @param csvElement table of strings to build a csv line out of
   * @return build csv string
   */
  private String joinCsvElements(final String[] csvElement) {
    char[] tmpCharTab;
    int tmpCharTabPos = 0;

    final int csvCharTabMax = 65536;
    int csvCharTabPos = 0;
    final char[] csvCharTab = new char[csvCharTabMax];

    boolean addAnfuehr = false;

    if (csvElements == null) {
      return StringUtils.EMPTY;
    } else {
      for (int i = 0; i < csvElement.length && csvCharTabPos < csvCharTabMax; i++) {
        if (csvElement[i] != null) {
          tmpCharTab = csvElement[i].toCharArray();

          addAnfuehr = false;
          for (tmpCharTabPos = 0; tmpCharTabPos < tmpCharTab.length //
              && !addAnfuehr; tmpCharTabPos++) {
            if (tmpCharTab[tmpCharTabPos] == '\"' || tmpCharTab[tmpCharTabPos] == seperator) {
              addAnfuehr = true;
            }
          }
          if (addAnfuehr) {
            csvCharTab[csvCharTabPos++] = '\"';
            for (tmpCharTabPos = 0; tmpCharTabPos < tmpCharTab.length && csvCharTabPos < csvCharTabMax; tmpCharTabPos++) {
              if (tmpCharTab[tmpCharTabPos] == '\"') {
                csvCharTab[csvCharTabPos++] = tmpCharTab[tmpCharTabPos];
              }
              csvCharTab[csvCharTabPos++] = tmpCharTab[tmpCharTabPos];
            }
            if (csvCharTabPos < csvCharTabMax) {
              csvCharTab[csvCharTabPos++] = '\"';
            }
          } else {
            for (tmpCharTabPos = 0; tmpCharTabPos < tmpCharTab.length
                && csvCharTabPos < csvCharTabMax; csvCharTab[csvCharTabPos++] = tmpCharTab[tmpCharTabPos++]) {
              ;
            }
          }
        }

        if (i + 1 < csvElement.length) {
          csvCharTab[csvCharTabPos++] = seperator;
        }
      }

      return new String(csvCharTab, 0, csvCharTabPos);
    }
  }

  /**
   * The <code>joinCSVUeb</code> method creates a csv header string.
   *
   * @return build csv haeder string
   */
  public String joinCsvUeb() {
    return joinCsvElements(header);
  }

  /**
   * The <code>joinCSVString</code> method creates a csv position string.
   *
   * @return build csv string
   */
  public String joinCsvString() {
    return joinCsvElements(csvElements);
  }

  /**
   * The <code>findElementPos</code> method finds the column number of a header name.
   *
   * @param tabHeaderName string contains the haeder name
   * @return number of the column
   */
  public int findElementPos(final String tabHeaderName) {
    int returnValue = -1;

    if (tabHeaderName != null && header != null) {
      int pos = 0;
      for (; pos < header.length && !tabHeaderName.equals(header[pos]); pos++) {
        ;
      }
      if (pos < header.length) {
        returnValue = pos;
      }
    }
    return returnValue;
  }

  /**
   * The <code>getElement</code> method givs back the column entry of a splitted csv line.
   *
   * @param tabPos number of the column
   * @return entry of the column
   */
  public String getElement(final int tabPos) {
    String returnValue = null;

    if (csvElements != null) {
      if (tabPos >= 0 && tabPos < csvElements.length) {
        returnValue = csvElements[tabPos];
      }
    }

    return returnValue;
  }

  /**
   * The <code>getElement</code> method givs back the column entry of a splitted csv line.
   *
   * @param tabHeaderName haeder name of the column
   * @return entry of the column
   */
  public String getElement(final String tabHeaderName) {
    return this.getElement(findElementPos(tabHeaderName));
  }

  /**
   * The <code>setElement</code> changes a entry of the csv line.
   *
   * @param tabPos number of the column
   * @param newEntry new entry of the column
   */
  public boolean setElement(final int tabPos, final String newEntry) {
    boolean returnValue = false;

    if (csvElements != null) {
      if (tabPos >= 0 && tabPos < csvElements.length) {
        returnValue = true;
        csvElements[tabPos] = newEntry;
      }
    }

    return returnValue;
  }

  /**
   * The <code>setElement</code> changes a entry of the csv line.
   *
   * @param tabHeaderName haeder name of the column
   * @param newEntry new entry of the column
   */
  public boolean setElement(final String tabHeaderName, final String newEntry) {
    return this.setElement(findElementPos(tabHeaderName), newEntry);
  }
}
