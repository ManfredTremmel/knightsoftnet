/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.common;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

/**
 * The <code>NavTabStrukt</code> declares a structure that's used to create a navigation menu.
 *
 * @author Manfred Tremmel
 * @version 1.0.1, 06.08.2005
 */
public class NavTabStrukt implements Serializable {

  /**
   * Serial version id.
   */
  private static final long serialVersionUID = -4749305533022328473L;

  /**
   * type of ths structure. 0 = start entry 1 = directory start 2 = directory end 3 = link
   */
  private final int type;
  /**
   * picture for menu entry.
   */
  private final String picture;
  /**
   * picture for menu entry for HiDPI displays.
   */
  private final String pictureHiDpi;
  /**
   * name of the entry.
   */
  private final String entry;
  /**
   * link to use.
   */
  private final String link;
  /**
   * directory is open or not.
   */
  private boolean isopen;

  /**
   * constructor.
   *
   * @param ptyp type to set
   * @param ppicture picture to display
   * @param pentry name of the entry
   * @param plink lint to set
   * @param pisOpen is open
   */
  public NavTabStrukt(final int ptyp, final String ppicture, final String pentry, final String plink, final boolean pisOpen) {
    this(ptyp, ppicture, StringUtils.replace(ppicture, "16x16", "32x32"), pentry, plink, pisOpen);
  }

  /**
   * constructor.
   *
   * @param ptyp type to set
   * @param ppicture picture to display
   * @param ppictureHiDpi picture to display for HiDPI displays
   * @param pentry name of the entry
   * @param plink lint to set
   * @param pisOpen is open
   */
  public NavTabStrukt(final int ptyp, final String ppicture, final String ppictureHiDpi, final String pentry,
      final String plink, final boolean pisOpen) {
    type = ptyp;
    picture = ppicture;
    pictureHiDpi = ppictureHiDpi;
    entry = pentry;
    link = plink;
    setIsopen(pisOpen);
  }

  /**
   * getter for type.
   *
   * @return the type
   */
  public int getType() {
    return type;
  }

  /**
   * getter for picture.
   *
   * @return the picture
   */
  public String getPicture() {
    return picture;
  }

  /**
   * getter for pictureHiDpi.
   *
   * @return the pictureHiDpi
   */
  public String getPictureHiDpi() {
    return pictureHiDpi;
  }

  /**
   * getter for entry.
   *
   * @return the entry
   */
  public String getEntry() {
    return entry;
  }

  /**
   * getter for isopen.
   *
   * @return the isopen
   */
  public boolean isIsopen() {
    return isopen;
  }

  /**
   * setter for isopen.
   *
   * @param setIsopen the isopen to set
   */
  public void setIsopen(final boolean setIsopen) {
    isopen = setIsopen;
  }

  /**
   * getter for link.
   *
   * @return the link
   */
  public String getLink() {
    return link;
  }
}
