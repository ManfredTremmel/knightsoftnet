/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.common;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import java.io.IOException;
import java.util.List;

/**
 * <code>SendEMail</code> is a class to send emails.
 *
 * @author Manfred Tremmel
 * @version 1.0.6, 03.06.2008
 */
public class SendEMail {

  /**
   * SMTP port.
   */
  public static final int SMTP_PORT = 25;

  /**
   * ip address of LOCALHOST.
   */
  public static final String LOCALHOST = "127.0.0.1"; // NOPMD

  /**
   * Constructor.
   *
   * @param from EmailField of the sender
   * @param organization organization of the sender
   * @param to EmailField of the receiver
   * @param replyTo EmailField where answers should be sent to
   * @param subject subject of the EmailField
   * @param mailtext text of the EmailField
   * @param smtpServer server to send the mail
   * @param smtpAuth true if using SMTP authentication
   * @param smtpUser user to authenticate
   * @param smtpPassword password to authenticate
   * @param smtpPort port of the server to send the mail
   * @throws IOException when sending fails
   */
  public SendEMail(final String from, final String organization, final String to, final String replyTo, final String subject,
      final String mailtext, final String smtpServer, final boolean smtpAuth, final String smtpUser, final String smtpPassword,
      final int smtpPort) throws IOException {
    try {
      sendEMailSendmail(createMailText(from, organization, to, replyTo, subject, mailtext), smtpServer, smtpAuth, smtpUser,
          smtpPassword, smtpPort);
    } catch (AddressException | EmailException e) {
      throw new IOException(e);
    }
  }

  /**
   * Constructor.
   *
   * @param from EmailField of the sender
   * @param organization organization of the sender
   * @param to EmailField of the receiver
   * @param replyTo EmailField where answers should be sent to
   * @param subject subject of the EmailField
   * @param mailtext text of the EmailField
   * @param smtpServer server to send the mail
   * @throws IOException when sending fails
   */
  public SendEMail(final String from, final String organization, final String to, final String replyTo, final String subject,
      final String mailtext, final String smtpServer) throws IOException {
    try {
      sendEMailSendmail(createMailText(from, organization, to, replyTo, subject, mailtext), smtpServer);
    } catch (AddressException | EmailException e) {
      throw new IOException(e);
    }
  }

  private Email createMailText(final String from, final String organization, final String to, final String replyTo,
      final String subject, final String mailtext) throws EmailException, AddressException {
    final SimpleEmail email = new SimpleEmail();
    email.setFrom(from);
    email.addTo(to);
    email.setCharset("iso-8859-1");
    email.setSubject(subject);
    email.setMsg(mailtext);
    if (StringUtils.isNotEmpty(replyTo)) {
      email.setReplyTo(List.of(new InternetAddress(replyTo)));
    }
    return email;
  }

  /**
   * Constructor.
   *
   * @param from EmailField of the sender
   * @param organization organization of the sender
   * @param to EmailField of the receiver
   * @param subject subject of the EmailField
   * @param mailtext text of the EmailField
   * @param smtpServer server to send the mail
   * @throws IOException when sending fails
   */
  public SendEMail(final String from, final String organization, final String to, final String subject, final String mailtext,
      final String smtpServer) throws IOException {
    this(from, organization, to, null, subject, mailtext, smtpServer);
  }

  /**
   * Constructor.
   *
   * @param from EmailField of the sender
   * @param organization organization of the sender
   * @param to EmailField of the receiver
   * @param subject subject of the EmailField
   * @param mailtext text of the EmailField
   * @throws IOException when sending fails
   */
  public SendEMail(final String from, final String organization, final String to, final String subject, final String mailtext)
      throws IOException {

    this(from, organization, to, null, subject, mailtext, null);
  }

  /**
   * Constructor.
   *
   * @param email the complete e-mail
   * @throws IOException when sending fails
   */
  public SendEMail(final Email email) throws IOException {
    this(email, null);
  }

  /**
   * Constructor.
   *
   * @param email the complete e-mail
   * @param smtpServer server to send the mail
   * @throws IOException when sending fails
   */
  public SendEMail(final Email email, final String smtpServer) throws IOException {

    sendEMailSendmail(email, smtpServer);
  }

  /**
   * The <code>SendEMailSendmail</code> method is sending the e-mail.
   *
   * @param email the complete e-mail
   * @param smtpServer server to send the mail
   * @throws IOException when sending fails
   */
  protected final void sendEMailSendmail(final Email email, final String smtpServer) throws IOException {

    final boolean smtpAuth;
    final String smtpServerWork;
    final String smtpUser;
    final String smtpPassword;
    final int smtpPort;
    try {
      final InitialContext ic = new InitialContext();
      smtpAuth = ((Boolean) ic.lookup("java:/comp/env/smtp_auth")).booleanValue();
      smtpServerWork = StringUtils
          .defaultString(StringUtils.defaultString((String) ic.lookup("java:/comp/env/smtp_server"), smtpServer), LOCALHOST);
      smtpUser = (String) ic.lookup("java:/comp/env/smtp_user");
      smtpPassword = (String) ic.lookup("java:/comp/env/smtp_password");
      smtpPort = (Integer) ic.lookup("java:/comp/env/smtp_port");
      ic.close();
    } catch (final NamingException e) {
      throw new IOException(e.getMessage());
    }
    sendEMailSendmail(email, smtpServerWork, smtpAuth, smtpUser, smtpPassword, smtpPort);
  }

  /**
   * The <code>SendEMailSendmail</code> method is sending the e-mail.
   *
   * @param email the complete e-mail
   * @param smtpServer server to send the mail
   * @param smtpAuth true if using SMTP authentication
   * @param smtpUser user to authenticate
   * @param smtpPassword password to authenticate
   * @param smtpPort port of the server to send the mail
   * @throws IOException when sending fails
   */
  protected final void sendEMailSendmail(final Email email, final String smtpServer, final boolean smtpAuth,
      final String smtpUser, final String smtpPassword, final int smtpPort) throws IOException {

    if (smtpUser != null && smtpPassword != null) {
      email.setAuthenticator(new DefaultAuthenticator(smtpUser, smtpPassword));
      email.setSSLOnConnect(true);
    }
    email.setHostName(smtpServer);
    email.setSmtpPort(smtpPort);
    try {
      email.send();
    } catch (final EmailException e) {
      throw new IOException(e);
    }
  }
}
