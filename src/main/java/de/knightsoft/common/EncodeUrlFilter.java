/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.common;

import javax.servlet.Filter;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import java.io.IOException;

/**
 *
 * <code>EncodeUrlFilter</code> is Wrapper to exclude Bots from URL rewrite.
 *
 * @author Manfred Tremmel
 * @version 1.0.0, 24.02.2013
 */
public class EncodeUrlFilter implements Filter {

  /**
   * servlet context.
   */
  ServletContext sc = null;

  @Override
  public final void init(final FilterConfig filterConfig) throws ServletException {
    // reference the context
    sc = filterConfig.getServletContext();
  }

  /**
   * filter request.
   *
   * @param request servlet request
   * @param response servlet response
   * @param chain FilterChain
   */
  @Override
  public void doFilter(final ServletRequest request, final ServletResponse response, final javax.servlet.FilterChain chain)
      throws ServletException, IOException {
    if (request instanceof HttpServletRequest && response instanceof HttpServletResponse) {
      final String userAgent = ((HttpServletRequest) request).getHeader("User-Agent");
      boolean doFilter = false;
      if (userAgent != null) {
        final String userAgentLower = userAgent.toLowerCase();
        if (userAgentLower.contains("googlebot") || userAgentLower.contains("ezooms") || userAgentLower.contains("baiduspider")
            || userAgentLower.contains("careerbot") || userAgentLower.contains("slurp") || userAgentLower.contains("mj12bot")
            || userAgentLower.contains("pixray-seeker") || userAgentLower.contains("yandexbot")
            || userAgentLower.contains("yahooseeker") || userAgentLower.contains("bingbot")
            || userAgentLower.contains("voilabot") || userAgentLower.contains("msnbot")) {
          doFilter = true;
        }
      }
      if (doFilter) {
        chain.doFilter(request, new HttpServletResponseWrapper((HttpServletResponse) response) {
          @Override
          public final String encodeURL(final String url) {
            return url;
          }
        });
      } else {
        chain.doFilter(request, response);
      }
    } else {
      chain.doFilter(request, response);
    }
  }

  @Override
  public void destroy() {
    // nothing to do
  }
}
