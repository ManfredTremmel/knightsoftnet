/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.common.field;

import de.knightsoft.common.Constants;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpSession;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * The <code>RadioField</code> class is based on the abstract AbstractBaseField class and implement the RadioField-button.
 *
 * @author Manfred Tremmel
 * @version 1.2.0, 22.07.2008
 */
public class RadioField extends de.knightsoft.common.field.AbstractBaseField {

  protected final String[] values;
  protected final String[] displayvalues;
  protected final String sqlString;
  protected String sqlWorkString;

  /**
   * Constructor, used for fix RadioField entries.
   *
   * @param servletname Name of the servlet
   * @param enchentment Label, displayed in the HTML-site
   * @param fieldname Fieldname, used in the HTML-site
   * @param dbfieldname Fieldname, used in the database
   * @param length Not used in this field-type
   * @param fieldlength Not used in this field-type
   * @param complusion Must be filled or not
   * @param values Values should be the same as in database
   * @param displayvalues Values to display, can be more readable
   */
  public RadioField(final String servletname, final String enchentment, final String fieldname, final String dbfieldname,
      final int length, final int fieldlength, final boolean complusion, final String[] values, final String[] displayvalues) {
    super(servletname, enchentment, fieldname, dbfieldname, length, fieldlength, complusion);
    this.values = values;
    this.displayvalues = displayvalues;
    myDataBase = null;
    sqlString = null;
  }

  /**
   * Constructor, used for flexible RadioField entries, taken from the database
   *
   * @param servletname Name of the servlet
   * @param enchentment Label, displayed in the HTML-site
   * @param fieldname Fieldname, used in the HTML-site
   * @param dbfieldname Fieldname, used in the database
   * @param length Not used in this field-type
   * @param fieldlength Not used in this field-type
   * @param complusion Must be filled or not
   * @param sqlString Use a prepared SQL statement that gives back to fields, value and displayvalue. They contain the entries
   *        of the Field.
   */
  public RadioField(final String servletname, final String enchentment, final String fieldname, final String dbfieldname,
      final int length, final int fieldlength, final boolean complusion, final String sqlString) {
    super(servletname, enchentment, fieldname, dbfieldname, length, fieldlength, complusion);
    values = null;
    displayvalues = null;
    this.sqlString = sqlString;
  }

  /**
   * Setup a new DataBase connection.
   *
   * @param pdataBase Database connection
   */
  @Override
  public void setNewDataBase(final Connection pdataBase) throws java.sql.SQLException {
    super.setNewDataBase(pdataBase);

    if (sqlString != null) {
      try {
        final de.knightsoft.common.DataBaseDepending MyDataBaseDepending =
            new de.knightsoft.common.DataBaseDepending(pdataBase.getMetaData().getDatabaseProductName());
        sqlWorkString = sqlString.replaceAll("NOW\\(\\)", MyDataBaseDepending.getSqlTimeNow());
      } catch (final Exception e) {
        sqlWorkString = sqlString;
      }
    }
  }

  /**
   * The method <code>htmlInputField</code> returns the input field for html formulars.
   *
   * @param ptabpos positionnummber of the field
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @param type action type to handle when this field is changed at least
   * @return html inputfield
   */
  @Override
  public String htmlInputField(final int ptabpos, final HttpSession session, final int posnumber, final String type) {
    int tabpos = ptabpos;
    if (enchentment == null) {
      return StringUtils.EMPTY;
    } else {
      final String htmlfieldname = de.knightsoft.common.StringToHtml.convert(this.getfieldname(posnumber), false, true, true);
      final StringBuilder sb = new StringBuilder(128);
      String contents = this.getContents(session, posnumber);

      String value;
      String selected;

      if (sqlString == null) {
        if (contents == null) {
          contents = values[0];
        }
        for (final String value2 : values) {
          if (value2 == null) {
            sb.append("<td>&nbsp;</td>");
          } else {
            if (contents != null && contents.equals(value2)) {
              selected = " checked";
            } else {
              selected = StringUtils.EMPTY;
            }
            sb.append("<td><input type=\"radio\" name=\"" + htmlfieldname + "\" value=\""
                + de.knightsoft.common.StringToHtml.convert(value2, false, true, true) + "\"" + selected + " tabindex=\""
                + tabpos++ + "\" onBlur=\"return checkRegEx('" + htmlfieldname + "', /" + regEx + "/, " + complusion + ", '"
                + type + "', false);\"></td>");
          }
        }
      } else {
        try {
          final Integer mandatorInteger = (Integer) session.getAttribute(servletname + Constants.DB_FIELD_GLOBAL_MANDATOR);
          try (final PreparedStatement sqlStatement = myDataBase.prepareStatement(sqlWorkString)) {
            sqlStatement.clearParameters();
            sqlStatement.setInt(1, mandatorInteger == null ? 1 : mandatorInteger.intValue());
            try (final ResultSet result = sqlStatement.executeQuery()) {

              while (result.next()) {
                value = result.getString("value");
                if (contents == null) {
                  contents = value;
                }
                if (value == null) {
                  sb.append("<td>&nbsp;</td>");
                } else {
                  if (contents != null && contents.equals(value)) {
                    selected = " checked";
                  } else {
                    selected = StringUtils.EMPTY;
                  }
                  sb.append("<td><input type=\"radio\" name=\"" + htmlfieldname + "\" value=\""
                      + de.knightsoft.common.StringToHtml.convert(value, false, true, true) + "\"" + selected + " tabindex=\""
                      + tabpos++ + "\" onChange=\"return checkRegEx('" + htmlfieldname + "', /" + regEx + "/, " + complusion
                      + ", '" + type + "', false);\"></td>");
                }
              }
            }
          }
        } catch (final java.sql.SQLException e) {
          System.err.println("SQL-Fehler beim ermitteln der select-Inhalte: " + e.toString());
        }
      }

      return sb.toString();
    }
  }

  /**
   * The method <code>htmlInputTableSegment</code> returns the input field including enchantement and table structure for html
   * formulars.
   *
   * @param tabpos positionnummber of the field
   * @param session The Data of the current Session
   * @return html inputfield
   */
  @Override
  public String htmlInputTableSegment(final int tabpos, final HttpSession session) {
    String returnstring;
    if (enchentment == null) {
      returnstring = StringUtils.EMPTY;
    } else if (complusion) {
      returnstring =
          "                    <td><label class=\"complusion\">" + de.knightsoft.common.StringToHtml.convert(enchentment)
              + "&nbsp;&sup1;</label></td>\n" + "                    " + this.htmlInputField(tabpos, session) + "\n";
    } else {
      returnstring = "                    <td><label class=\"free\">" + de.knightsoft.common.StringToHtml.convert(enchentment)
          + "&nbsp;&sup2;</label></td>\n" + "                    " + this.htmlInputField(tabpos, session) + "\n";
    }
    return returnstring;
  }

  /**
   * The method <code>htmlInputTableSegment</code> returns the input field including enchantement and table structure for html
   * formulars.
   *
   * @param tabpos positionnummber of the field
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @param type action type to handle when this field is changed at least
   * @return html inputfield
   */
  @Override
  public String htmlInputTableSegment(final int tabpos, final HttpSession session, final int posnumber, final String type) {
    if (enchentment == null) {
      return StringUtils.EMPTY;
    } else {
      return "                    " + this.htmlInputField(tabpos, session, posnumber, type) + "\n";
    }
  }

  /**
   * The method <code>htmlViewField</code> returns the html view field.
   *
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @return html viewfield
   */
  @Override
  public String htmlViewField(final HttpSession session, final int posnumber) {
    if (enchentment == null) {
      return StringUtils.EMPTY;
    } else {
      final String contents = this.getContents(session, posnumber);
      final StringBuilder sb = new StringBuilder();
      String value;
      if (sqlString == null) {
        for (final String value2 : values) {
          if (value2 == null) {
            sb.append("<td>&nbsp;</td>");
          } else {
            if (contents != null && contents.equals(value2)) {
              sb.append("<td>X</td>");
            } else {
              sb.append("<td>-</td>");
            }
          }
        }
      } else {
        try {
          final Integer MandatorInteger = (Integer) session.getAttribute(servletname + Constants.DB_FIELD_GLOBAL_MANDATOR);
          try (final PreparedStatement sqlStatement = myDataBase.prepareStatement(sqlWorkString)) {
            sqlStatement.clearParameters();
            sqlStatement.setInt(1, MandatorInteger == null ? 1 : MandatorInteger.intValue());
            try (final ResultSet result = sqlStatement.executeQuery()) {

              while (result.next()) {
                value = result.getString("value");
                if (value == null) {
                  sb.append("<td>&nbsp;</td>");
                } else {
                  if (contents != null && contents.equals(value)) {
                    sb.append("<td>X</td>");
                  } else {
                    sb.append("<td>-</td>");
                  }
                }
              }
            }
          }
        } catch (final java.sql.SQLException e) {
          System.err.println("SQL-Fehler beim ermitteln der select-Inhalte: " + e.toString());
        }
      }

      return sb.toString();
    }
  }

  /**
   * The method <code>htmlViewTableSegment</code> returns the html view field including enchantement and table structure.
   *
   * @param session The Data of the current Session
   * @return html viewfield
   */
  @Override
  public String htmlViewTableSegment(final HttpSession session) {
    if (enchentment == null) {
      return StringUtils.EMPTY;
    } else {
      return "                    <td>" + de.knightsoft.common.StringToHtml.convert(enchentment) + "</td>\n"
          + "                    " + this.htmlViewField(session) + "\n";
    }
  }

  /**
   * The method <code>htmlViewTableSegment</code> returns the html view field including enchantement and table structure.
   *
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @return html viewfield
   */
  @Override
  public String htmlViewTableSegment(final HttpSession session, final int posnumber) {
    if (enchentment == null) {
      return StringUtils.EMPTY;
    } else {
      return "                    " + this.htmlViewField(session, posnumber) + "\n";
    }
  }

  /**
   * The method <code>javascriptCheck</code> returns javascript code to check the input of the field.
   *
   * @param posnumber number of the position or -1 for headerfields
   * @param type action type to handle when this field is changed at least
   * @return html inputfield
   */
  @Override
  public String javascriptCheck(final int posnumber, final String type) {
    return StringUtils.EMPTY;
  }

  /**
   * The method <code>checkField</code> checks the contents of the field.
   *
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @return true if the contents is correct
   */
  @Override
  public boolean checkField(final HttpSession session, final int posnumber) {
    return true;
  }
}
