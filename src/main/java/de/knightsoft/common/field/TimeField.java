/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.common.field;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpSession;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * The <code>TimeField</code> class is a text input field with check TimeField input.
 *
 * @author Manfred Tremmel
 * @version 1.3.0, 28.07.2018
 */
public class TimeField extends de.knightsoft.common.field.AbstractBaseField {

  private static final String TIME_FORMAT = "HH:mm:ss";

  /**
   * Constructor.
   *
   * @param servletname Name of the servlet
   * @param enchentment Label, displayed in the HTML-site
   * @param fieldname Fieldname, used in the HTML-site
   * @param dbfieldname Fieldname, used in the database
   * @param complusion Must be filled or not
   */
  public TimeField(final String servletname, final String enchentment, final String fieldname, final String dbfieldname,
      final boolean complusion) {
    super(servletname, enchentment, fieldname, dbfieldname, 8, 9, complusion, null, "time");
  }

  /**
   * The method <code>sqlRead</code> reads out the SQL result Data (SQL select).
   *
   * @param result ResultSet of the SQL read
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @exception SQLException if a database access error occurs
   */
  @Override
  public void sqlRead(final ResultSet result, final HttpSession session, final int posnumber) throws java.sql.SQLException {
    if (fieldname != null && dbfieldname != null) {
      final Time timeSql = result.getTime(dbfieldname);

      if (timeSql == null) {
        session.removeAttribute(servletname + this.getfieldname(posnumber));
      } else {
        session.setAttribute(servletname + this.getfieldname(posnumber),
            new SimpleDateFormat(TimeField.TIME_FORMAT).format(new Date(timeSql.getTime())));
      }
    }
  }

  /**
   * The method <code>sqlPrepareItems</code> adds the entry to the fields of a prepared statement
   *
   * @param myPreparedStatement prepared statement
   * @param thisDatabase the database connection
   * @param pentryNumber number of the field
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @return new EntryNumber
   * @exception java.sql.SQLException
   */
  @Override
  public int sqlPrepareItems(final PreparedStatement myPreparedStatement, final Connection thisDatabase, final int pentryNumber,
      final HttpSession session, //
      final int posnumber) throws SQLException {
    int entryNumber = pentryNumber;
    if (fieldname != null && dbfieldname != null) {
      final String contents;
      if (StringUtils.length(this.getContents(session, posnumber)) == 5) {
        contents = this.getContents(session, posnumber) + ":00";
      } else {
        contents = this.getContents(session, posnumber);
      }
      Time contentsSdf = null;
      if (contents != null) {
        try {
          contentsSdf = new Time(new SimpleDateFormat(TimeField.TIME_FORMAT).parse(contents).getTime());
        } catch (final Exception e) {
          contentsSdf = null;
        }
      }

      myPreparedStatement.setTime(entryNumber, contentsSdf);

      entryNumber++;
    }
    return entryNumber;
  }
}
