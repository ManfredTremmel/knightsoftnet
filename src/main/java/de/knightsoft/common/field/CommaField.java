/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.common.field;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;

/**
 * The <code>CommaField</code> class is a text input field with check numeric input (double)
 *
 * @author Manfred Tremmel
 * @version 1.2.0 22.07.2008
 */
public class CommaField extends de.knightsoft.common.field.AbstractBaseField {

  /**
   * Constructor.
   *
   * @param servletname Name of the servlet
   * @param enchentment Label, displayd in the HTML-site
   * @param fieldname Fieldname, used in the HTML-site
   * @param dbfieldname Fieldname, used in the database
   * @param length maximum length of the number
   * @param fieldlength field length
   * @param complusion Must be filled or not
   */
  public CommaField(final String servletname, final String enchentment, final String fieldname, final String dbfieldname,
      final int length, final int fieldlength, final boolean complusion) {
    super(servletname, enchentment, fieldname, dbfieldname, length, fieldlength, complusion, "^\\d*($|,\\d*$)");
  }

  /**
   * Constructor.
   *
   * @param servletname Name of the servlet
   * @param enchentment Label, displayd in the HTML-site
   * @param fieldname Fieldname, used in the HTML-site
   * @param dbfieldname Fieldname, used in the database
   * @param fieldlength field length
   * @param complusion Must be filled or not
   * @param minimumIntegerDigits Minumum digits before CommaField
   * @param maximumIntegerDigits Maximum digits before CommaField
   * @param maximumFractionDigits Maximum digits after CommaField
   */
  public CommaField(final String servletname, final String enchentment, final String fieldname, final String dbfieldname,
      final int fieldlength, final boolean complusion, final int minimumIntegerDigits, final int maximumIntegerDigits,
      final int maximumFractionDigits) {
    super(servletname, enchentment, fieldname, dbfieldname, maximumIntegerDigits + maximumFractionDigits + 1, fieldlength,
        complusion, "^\\d{" + Integer.toString(minimumIntegerDigits) + "," + Integer.toString(maximumIntegerDigits)
            + "}($|,\\d{1," + Integer.toString(maximumFractionDigits) + "}$)");
  }

  /**
   * The method <code>htmlRead</code> reads out the HTML request Data (input of the html form).
   *
   * @param req Request data from the servlet
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   */
  @Override
  public void htmlRead(final HttpServletRequest req, final HttpSession session, //
      final int posnumber) {
    if (fieldname != null && enchentment != null) {
      final String thisFieldName = this.getfieldname(posnumber);
      String contents = req.getParameter(thisFieldName);
      if (contents == null) {
        contents = StringUtils.EMPTY;
      } else {
        try {
          final NumberFormat myNf = NumberFormat.getInstance(java.util.Locale.GERMANY);
          myNf.setMinimumIntegerDigits(0);
          myNf.setMaximumIntegerDigits(length);
          myNf.setMinimumFractionDigits(0);
          myNf.setMaximumFractionDigits(length - 2);
          myNf.setGroupingUsed(false);
          contents = myNf.format(myNf.parse(contents).doubleValue());
        } catch (final java.text.ParseException e) { // NOPMD
          // do nothing, contents keeps unchanged
        }
      }
      session.setAttribute(servletname + thisFieldName, contents);
    }
  }

  /**
   * The method <code>sqlRead</code> reads out the SQL result Data (SQL select).
   *
   * @param result ResultSet of the SQL read
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @exception SQLException if a database access error occurs
   */
  @Override
  public void sqlRead(final ResultSet result, final HttpSession session, final int posnumber) throws SQLException {
    if (fieldname != null && dbfieldname != null) {
      final double contentsDouble = result.getDouble(dbfieldname);
      final NumberFormat myNf = NumberFormat.getInstance(java.util.Locale.GERMANY);
      myNf.setMinimumIntegerDigits(0);
      myNf.setMaximumIntegerDigits(length);
      myNf.setMinimumFractionDigits(0);
      myNf.setMaximumFractionDigits(length - 2);
      myNf.setGroupingUsed(false);
      final String contents = myNf.format(contentsDouble);
      session.setAttribute(servletname + this.getfieldname(posnumber), contents);
    }
  }

  /**
   * The method <code>htmlInputField</code> returns the input field for html formulars.
   *
   * @param tabpos positionnummber of the field
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @param type action type to handle when this field is changed at least
   * @return html inputfield
   */
  @Override
  public String htmlInputField(final int tabpos, final HttpSession session, final int posnumber, final String type) {
    if (enchentment == null) {
      return StringUtils.EMPTY;
    } else if (fieldname == null) {
      return "&nbsp;";
    } else {
      final String htmlfieldname = de.knightsoft.common.StringToHtml.convert(this.getfieldname(posnumber), false, true, true);
      final String classstring = this.checkField(session, posnumber) ? "default" : "faulty";
      final String returnstring = "<input type=\"text\" class=\"" + classstring + "\" style=\"text-align:right;\" id=\""
          + htmlfieldname + "\" name=\"" + htmlfieldname + "\" size=\"" + Integer.toString(fieldlength) + "\" maxlength=\""
          + Integer.toString(length) + "\" value=\"" + this.htmlStringContents(session, posnumber) + "\" tabindex=\""
          + Integer.toString(tabpos) + "\" onBlur=\"return checkRegEx('" + htmlfieldname + "', /" + regEx + "/, " + complusion
          + ", '" + type + "', false);\">";
      return returnstring;
    }
  }

  /**
   * The method <code>sqlPrepareItems</code> adds the entry to the fields of a prepared statement.
   *
   * @param myPreparedStatement prepared statement
   * @param thisDatabase the database connection
   * @param pentryNumber number of the field
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @return new EntryNumber
   * @exception java.sql.SQLException
   */
  @Override
  public int sqlPrepareItems(final PreparedStatement myPreparedStatement, final Connection thisDatabase, final int pentryNumber,
      final HttpSession session, //
      final int posnumber) throws java.sql.SQLException {
    int entryNumber = pentryNumber;
    if (fieldname != null && dbfieldname != null) {
      final String contents = this.getContents(session, posnumber);
      double contentsDouble = 0.0;
      try {
        final NumberFormat myNf = NumberFormat.getInstance(java.util.Locale.GERMANY);
        myNf.setMinimumIntegerDigits(0);
        myNf.setMaximumIntegerDigits(length);
        myNf.setMinimumFractionDigits(0);
        myNf.setMaximumFractionDigits(length - 2);
        myNf.setGroupingUsed(false);
        contentsDouble = myNf.parse(contents).doubleValue();
      } catch (final java.text.ParseException e) {
        contentsDouble = 0.0;
      }

      myPreparedStatement.setDouble(entryNumber, contentsDouble);

      entryNumber++;
    }
    return entryNumber;
  }

}
