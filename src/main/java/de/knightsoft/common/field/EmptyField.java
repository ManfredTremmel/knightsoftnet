/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.common.field;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpSession;

/**
 * The <code>EmptyField</code> class is a placeholder and dose nothing.
 *
 * @author Manfred Tremmel
 * @version 1.2.0 22.07.2008
 */
public class EmptyField extends de.knightsoft.common.field.DummyField {

  /**
   * Constructor.
   *
   * @param servletname Name of the servlet
   * @param fieldname Fieldname, used in the HTML-site
   * @param dbfieldname Fieldname, used in the database
   * @param length Not used in this field-type
   * @param fieldlength Not used in this field-type
   * @param complusion Must be filled or not
   */
  public EmptyField(final String servletname, final String fieldname, final String dbfieldname, final int length,
      final int fieldlength, final boolean complusion) {
    super(servletname, fieldname, dbfieldname, length, fieldlength, complusion);
  }

  /**
   * Constructor.
   */
  public EmptyField() {
    super();
  }

  /**
   * The method <code>htmlInputTableSegment</code> returns the input field including enchantment and table structure for html
   * formulars.
   *
   * @param tabpos positionnummber of the field
   * @param session The Data of the current Session
   * @return html inputfield
   */
  @Override
  public String htmlInputTableSegment(final int tabpos, final HttpSession session) {
    return StringUtils.EMPTY;
  }

  /**
   * The method <code>numTDs</code> returns the number td tags the htmlInputTableSegment method creates.
   *
   * @return number TD tags
   */
  @Override
  public int numTDs() {
    return 0;
  }
}
