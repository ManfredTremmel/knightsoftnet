/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.common.field;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpSession;

/**
 * The <code>TextAreaField</code> class is for text input fields with more then one line.
 *
 * @author Manfred Tremmel
 * @version 1.2.0, 22.07.2008
 */
public class TextAreaField extends de.knightsoft.common.field.AbstractBaseField {

  protected final int fieldheight; // Zeilenzahl Ausgabefeld

  /**
   * Constructor.
   *
   * @param servletname Name of the servlet
   * @param enchentment Label, displayed in the HTML-site
   * @param fieldname Fieldname, used in the HTML-site
   * @param dbfieldname Fieldname, used in the database
   * @param length Not used in this field-type
   * @param fieldlength Not used in this field-type
   * @param complusion Must be filled or not
   */
  public TextAreaField(final String servletname, final String enchentment, final String fieldname, final String dbfieldname,
      final int length, final int fieldlength, final boolean complusion) {
    super(servletname, enchentment, fieldname, dbfieldname, length, fieldlength, complusion);
    fieldheight = 5;
  }

  /**
   * Constructor.
   *
   * @param servletname Name of the servlet
   * @param enchentment Label, displayed in the HTML-site
   * @param fieldname Fieldname, used in the HTML-site
   * @param dbfieldname Fieldname, used in the database
   * @param length Not used in this field-type
   * @param fieldlength Not used in this field-type
   * @param complusion Must be filled or not
   * @param fieldheight Number of lines
   */
  public TextAreaField(final String servletname, final String enchentment, final String fieldname, final String dbfieldname,
      final int length, final int fieldlength, final boolean complusion, final int fieldheight) {
    super(servletname, enchentment, fieldname, dbfieldname, length, fieldlength, complusion);
    this.fieldheight = fieldheight;
  }

  /**
   * The method <code>javascriptCheck</code> returns javascript code to check the input of the field.
   *
   * @param posnumber number of the position or -1 for headerfields
   * @param type action type to handle when this field is changed at least
   * @return html inputfield
   */
  @Override
  public String javascriptCheck(final int posnumber, final String type) {
    return StringUtils.EMPTY;
  }

  /**
   * The method <code>checkField</code> checks the contents of the field.
   *
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @return true if the contents is correct
   */
  @Override
  public boolean checkField(final HttpSession session, final int posnumber) {
    final String contents = this.getContents(session, posnumber);
    boolean returnwert = false;

    if (fieldname == null || enchentment == null) {
      returnwert = true;
    } else {
      if (complusion && StringUtils.isEmpty(contents)) {
        // nothing to do
      } else {
        returnwert = true;
      }
    }
    return returnwert;
  }

  /**
   * The method <code>htmlInputField</code> returns the input field for html formulars.
   *
   * @param tabpos positionnummber of the field
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @param type action type to handle when this field is changed at least
   * @return html inputfield
   */
  @Override
  public String htmlInputField(final int tabpos, final HttpSession session, final int posnumber, final String type) {
    if (enchentment == null) {
      return StringUtils.EMPTY;
    } else {
      final String htmlfieldname = de.knightsoft.common.StringToHtml.convert(this.getfieldname(posnumber), false, true, true);
      final String classstring = this.checkField(session, posnumber) ? "default" : "faulty";
      final String returnstring = "<textarea class=\"" + classstring + "\" id=\"" + htmlfieldname + "\" name=\"" + htmlfieldname
          + "\" cols=\"" + Integer.toString(fieldlength) + "\" rows=\"" + Integer.toString(fieldheight) + "\" tabindex=\""
          + Integer.toString(tabpos) + "\" onBlur=\"return checkRegEx('" + htmlfieldname + "', /" + regEx + "/, " + complusion
          + ", '" + type + "', false);\">"
          + de.knightsoft.common.StringToHtml.convert(this.getContents(session, posnumber), false, false, true) + "</textarea>";
      return returnstring;
    }
  }
}
