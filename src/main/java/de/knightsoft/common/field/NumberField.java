/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.common.field;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpSession;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Types;

/**
 * The <code>NumberField</code> class is a text input field with check numeric input.
 *
 * @author Manfred Tremmel
 * @version 1.2.1 06.07.2019
 */
public class NumberField extends de.knightsoft.common.field.AbstractBaseField {

  /**
   * Constructor.
   *
   * @param servletname Name of the servlet
   * @param enchentment Label, displayed in the HTML-site
   * @param fieldname Fieldname, used in the HTML-site
   * @param dbfieldname Fieldname, used in the database
   * @param length Not used in this field-type
   * @param fieldlength Not used in this field-type
   * @param complusion Must be filled or not
   */
  public NumberField(final String servletname, final String enchentment, final String fieldname, final String dbfieldname,
      final int length, final int fieldlength, final boolean complusion) {
    super(servletname, enchentment, fieldname, dbfieldname, length, fieldlength, complusion, "^\\d{1,}$", "number");
  }

  /**
   * Constructor.
   *
   * @param servletname Name of the servlet
   * @param enchentment Label, displayed in the HTML-site
   * @param fieldname Fieldname, used in the HTML-site
   * @param dbfieldname Fieldname, used in the database
   * @param length Not used in this field-type
   * @param fieldlength Not used in this field-type
   * @param complusion Must be filled or not
   * @param min minimum input value
   * @param max maximum input value
   */
  public NumberField(final String servletname, final String enchentment, final String fieldname, final String dbfieldname,
      final int length, final int fieldlength, final boolean complusion, final Long min, final Long max) {
    super(servletname, enchentment, fieldname, dbfieldname, length, fieldlength, complusion, "^\\d{1,}$", "number",
        minMaxValue(min, max));
  }

  private static String minMaxValue(final Long min, final Long max) {
    String additionalParam = "";
    if (min != null) {
      additionalParam = " min=\"" + min.toString() + "\"";
    }
    if (max != null) {
      additionalParam += " max=\"" + max.toString() + "\"";
    }
    return additionalParam;
  }

  /**
   * The method <code>sqlPrepareItems</code> adds the entry to the fields of a prepared statement
   *
   * @param myPreparedStatement prepared statement
   * @param thisDatabase the database connection
   * @param pentryNumber number of the field
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @return new EntryNumber
   * @exception java.sql.SQLException
   */
  @Override
  public int sqlPrepareItems(final PreparedStatement myPreparedStatement, final Connection thisDatabase, final int pentryNumber,
      final HttpSession session, final int posnumber) throws java.sql.SQLException {
    int entryNumber = pentryNumber;
    if (fieldname != null && dbfieldname != null) {
      final String contents = this.getContents(session, posnumber);
      if (!complusion && (contents == null || StringUtils.EMPTY.equals(contents))) {
        myPreparedStatement.setNull(entryNumber, Types.NULL);
      } else {
        long contentsLong = 0;
        try {
          contentsLong = Long.parseLong(contents);
        } catch (final NumberFormatException e) {
          contentsLong = 0;
        }

        myPreparedStatement.setLong(entryNumber, contentsLong);
      }

      entryNumber++;
    }
    return entryNumber;
  }

}
