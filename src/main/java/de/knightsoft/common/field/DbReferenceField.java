/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.common.field;

import de.knightsoft.common.Constants;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * The <code>DbReferenceField</code> is additional to another field and provides a reference to a database field in another
 * table.
 *
 * @author Manfred Tremmel
 * @version 1.2.0 22.07.2008
 */
public class DbReferenceField extends de.knightsoft.common.field.AbstractBaseField {

  protected String sqlString;
  protected de.knightsoft.common.field.AbstractBaseField refField;

  /**
   * Constructor.
   *
   * @param servletname Name of the servlet
   * @param enchentment Label, displayd in the HTML-site
   * @param fieldname Fieldname, used in the HTML-site
   * @param dbfieldname Fieldname, used in the database
   * @param length Not used in this field-type
   * @param fieldlength Not used in this field-type
   * @param complusion Must be filled or not
   * @param sqlString SQL-Statement
   */
  public DbReferenceField(final String servletname, final String enchentment, //
      final String fieldname, final String dbfieldname, final int length, final int fieldlength, final boolean complusion,
      final String sqlString) {
    super(servletname, enchentment, fieldname, dbfieldname, length, fieldlength, complusion);
    this.sqlString = sqlString;
    refField = null;
  }

  /**
   * Setup a new DataBase connection.
   *
   * @param pdatabase Database connection
   */
  @Override
  public void setNewDataBase(final Connection pdatabase) throws java.sql.SQLException {
    myDataBase = pdatabase;
  }

  /**
   * Setup a new reference field.
   *
   * @param newRefField A new field to reference to
   */
  public void setReferenceField(final de.knightsoft.common.field.AbstractBaseField newRefField) {
    refField = newRefField;
  }

  /**
   * The method <code>htmlRead</code> reads out the HTML request Data (input of the html form).
   *
   * @param req Request data from the servlet
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   */
  @Override
  public void htmlRead(final HttpServletRequest req, final HttpSession session, //
      final int posnumber) {
    try {
      this.sqlRead(null, session, posnumber);
    } catch (final java.sql.SQLException e) {
      System.err.println("SQL-Fehler beim ermitteln der Referenz: " + e.toString());
    }
  }

  /**
   * The method <code>sqlRead</code> reads out the SQL result Data (SQL select).
   *
   * @param result ResultSet of the SQL read
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @exception SQLException if a database access error occurs
   */
  @Override
  public void sqlRead(final ResultSet result, final HttpSession session, final int posnumber) throws SQLException {
    String contents = null;
    if (refField != null && sqlString != null) {
      if (refField.getContents(session) == null || refField.getContents(session).equals(StringUtils.EMPTY)) {
        contents = " ";
      } else {
        final Integer MandatorInteger = (Integer) session.getAttribute(servletname + Constants.DB_FIELD_GLOBAL_MANDATOR);
        try (final PreparedStatement sqlStatement = myDataBase.prepareStatement(sqlString)) {
          sqlStatement.clearParameters();
          sqlStatement.setInt(1, MandatorInteger == null ? 1 : MandatorInteger.intValue());
          sqlStatement.setString(2, refField.getContents(session, posnumber));
          try (final ResultSet resultx = sqlStatement.executeQuery()) {

            if (resultx.next()) {
              contents = resultx.getString("reference");
            }
          }
        }
      }
    }
    session.setAttribute(servletname + this.getfieldname(posnumber), contents);
  }

  /**
   * The method <code>htmlInputField</code> returns the input field for html formulars.
   *
   * @param tabpos positionnummber of the field
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @param type action type to handle when this field is changed at least
   * @return html inputfield
   */
  @Override
  public String htmlInputField(final int tabpos, final HttpSession session, final int posnumber, final String type) {
    final String contents = this.getContents(session, posnumber);
    String returnstring = "Fehlerhafte Referenz";
    if (contents != null) {
      returnstring = de.knightsoft.common.StringToHtml.convert(contents, false, true, true);
    }
    return returnstring;
  }

  /**
   * The method <code>htmlInputTableSegment</code> returns the input field including enchantement and table structure for html
   * formulars.
   *
   * @param tabpos positionnummber of the field
   * @param session The Data of the current Session
   * @return html inputfield
   */
  @Override
  public String htmlInputTableSegment(final int tabpos, final HttpSession session) {
    return "                    <td colspan=\"2\">" + this.htmlInputField(tabpos, session) + "</td>\n";
  }

  /**
   * The method <code>javascriptCheck</code> returns javascript code to check the input of the field.
   *
   * @param posnumber number of the position or -1 for headerfields
   * @param type action type to handle when this field is changed at least
   * @return html inputfield
   */
  @Override
  public String javascriptCheck(final int posnumber, final String type) {
    return StringUtils.EMPTY;
  }

  /**
   * The method <code>htmlViewField</code> returns the html view field.
   *
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @return html viewfield
   */
  @Override
  public String htmlViewField(final HttpSession session, final int posnumber) {
    return this.htmlInputField(0, session, posnumber, null);
  }

  /**
   * The method <code>htmlViewTableSegment</code> returns the html view field including enchantement and table structure.
   *
   * @param session The Data of the current Session
   * @return html viewfield
   */
  @Override
  public String htmlViewTableSegment(final HttpSession session) {
    return this.htmlInputTableSegment(0, session);
  }

  /**
   * The method <code>checkField</code> checks the contents of the field.
   *
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @return true if the contents is correct
   */
  @Override
  public boolean checkField(final HttpSession session, final int posnumber) {
    final String contents = this.getContents(session, posnumber);
    if (refField.getContents(session) == null || refField.getContents(session).equals(StringUtils.EMPTY)) {
      return true;
    } else {
      return contents != null;
    }
  }

  /**
   * The method <code>sqlUpdateName</code> creates a sql string for updating the database.
   *
   * @param sqlString String to add the update field
   * @return new SQLString
   */
  @Override
  public StringBuilder sqlUpdateName(final StringBuilder sqlString) {
    return sqlString;
  }

  /**
   * The method <code>sqlInsertNames</code> creates a sql string for inserting into the database (fieldname).
   *
   * @param sqlStringNames String to add the fieldname
   * @return new SQLStringNames
   */
  @Override
  public StringBuilder sqlInsertNames(final StringBuilder sqlStringNames) {
    return sqlStringNames;
  }

  /**
   * The method <code>sqlPrepareInsertItems</code> creates a sql string for insert prepare statement.
   *
   * @param sqlStringItems String to add the contents
   * @return new SQLStringItems
   */
  @Override
  public StringBuilder sqlPrepareInsertItems(final StringBuilder sqlStringItems) {
    return sqlStringItems;
  }

  /**
   * The method <code>sqlPrepareItems</code> adds the entry to the fields of a prepared statement
   *
   * @param myPreparedStatement prepared statement
   * @param thisDatabase the database connection
   * @param entryNumber number of the field
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @return new EntryNumber
   * @exception java.sql.SQLException
   */
  @Override
  public int sqlPrepareItems(final PreparedStatement myPreparedStatement, final Connection thisDatabase, final int entryNumber,
      final HttpSession session, final int posnumber) throws java.sql.SQLException {
    return entryNumber;
  }
}
