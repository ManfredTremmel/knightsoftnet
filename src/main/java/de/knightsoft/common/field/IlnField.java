/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.common.field;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpSession;

/**
 * The <code>IlnField</code> class is a TextField input field with check of the IlnField/EAN checksum.
 *
 * @author Manfred Tremmel
 * @version 1.3.0 28.07.2018
 */
public class IlnField extends de.knightsoft.common.field.AbstractBaseField {

  /**
   * Constructor.
   *
   * @param servletname Name of the servlet
   * @param enchentment Label, displayed in the HTML-site
   * @param fieldname Fieldname, used in the HTML-site
   * @param dbfieldname Fieldname, used in the database
   * @param fieldlength Not used in this field-type
   * @param complusion Must be filled or not
   */
  public IlnField(final String servletname, final String enchentment, final String fieldname, final String dbfieldname,
      final int fieldlength, final boolean complusion) {
    super(servletname, enchentment, fieldname, dbfieldname, 13, fieldlength, complusion, "^\\d{13}$", "number");
  }

  /**
   * The method <code>javascriptCheck</code> returns javascript code to check the input of the field.
   *
   * @param posnumber number of the position or -1 for headerfields
   * @param type action type to handle when this field is changed at least
   * @return html inputfield
   */
  @Override
  public String javascriptCheck(final int posnumber, final String type) {
    final StringBuilder returnstring = new StringBuilder(3000);

    if (fieldname == null) {
      returnstring.append(StringUtils.EMPTY);
    } else {
      String htmlfieldname = de.knightsoft.common.StringToHtml.convert(fieldname, false, true, true);
      if (posnumber >= 0) {
        htmlfieldname += Integer.toString(posnumber); // NOPMD
      }
      returnstring.append("                        if (window.document.getElementById('buttonpress')."
          + "value.toLowerCase().substr(0,4) == '" + type + "') {\n" + "                            if (document.forms[0]."
          + htmlfieldname + ".value.length == 0) {\n");

      if (complusion) {
        returnstring.append("                                alert(unescape(\"Bitte f%FCllen Sie das Feld " + enchentment
            + ", es ist ein Zwangsfeld!\"));\n" + "                                document.forms[0]." + htmlfieldname
            + ".focus();\n" + "                                return false;\n");
      }
      returnstring.append("                            } else {\n" + "                                if (document.forms[0]."
          + htmlfieldname + ".value.length != 13) {\n" + "                                    alert(unescape(\"Feld "
          + enchentment + " ist nicht dreizehn Stellen lang,\\nILN m%FCssen mit dreizehn Stellen "
          + "inklusive Pr%FCfziffer angegeben werden!\"));\n" + "                                    document.forms[0]."
          + htmlfieldname + ".focus();\n" + "                                    return false;\n"
          + "                                }\n" + "                                " + htmlfieldname + "_help = 1;\n"
          + "                                for (pos=0;pos<document.forms[0]." + htmlfieldname + ".value.length;++pos) {\n"
          + "                                    if (document.forms[0]." + htmlfieldname + ".value.charAt(pos) < \"0\" ||\n"
          + "                                        document.forms[0]." + htmlfieldname + ".value.charAt(pos) > \"9\")\n"
          + "                                        " + htmlfieldname + "_help = -1;\n" + "                                }\n"
          + "                                if (" + htmlfieldname + "_help == -1) {\n"
          + "                                    alert(\"Feld " + enchentment + " ist nicht numerisch, bitte korrigieren.\");\n"
          + "                                    document.forms[0]." + htmlfieldname + ".focus();\n"
          + "                                    return false;\n" + "                                }\n"
          + "                                cn    =    48;\n"
          + "                                w1    =    (document.forms[0]." + htmlfieldname + ".value.charCodeAt(0) - cn +\n"
          + "                                         document.forms[0]." + htmlfieldname + ".value.charCodeAt(2) - cn +\n"
          + "                                         document.forms[0]." + htmlfieldname + ".value.charCodeAt(4) - cn +\n"
          + "                                         document.forms[0]." + htmlfieldname + ".value.charCodeAt(6) - cn +\n"
          + "                                         document.forms[0]." + htmlfieldname + ".value.charCodeAt(8) - cn +\n"
          + "                                         document.forms[0]." + htmlfieldname + ".value.charCodeAt(10)) - cn;\n"
          + "                                w2    =    (document.forms[0]." + htmlfieldname + ".value.charCodeAt(1) - cn +\n"
          + "                                         document.forms[0]." + htmlfieldname + ".value.charCodeAt(3) - cn +\n"
          + "                                         document.forms[0]." + htmlfieldname + ".value.charCodeAt(5) - cn +\n"
          + "                                         document.forms[0]." + htmlfieldname + ".value.charCodeAt(7) - cn +\n"
          + "                                         document.forms[0]." + htmlfieldname + ".value.charCodeAt(9) - cn +\n"
          + "                                         document.forms[0]." + htmlfieldname + ".value.charCodeAt(11) - cn) * 3;\n"
          + "                                ps    =    (10 - ((w1 + w2) % 10));\n"
          + "                                if (ps > 9)\n" + "                                    ps    =    0;\n"
          + "                                if (document.forms[0]." + htmlfieldname
          + ".value.charAt(12) != String.fromCharCode(ps + cn).charAt(0)) {\n"
          + "                                    alert(unescape(\"Feld " + enchentment
          + " enth%E4lt eine falsche Pr%FCfziffer.\\nErrechnet: '\" + ps + \"', " + "Eingegeben: '\" + document.forms[0]."
          + htmlfieldname + ".value.charAt(12) + \"'\"));\n" + "                                    document.forms[0]."
          + htmlfieldname + ".focus();\n" + "                                    return false;\n"
          + "                                }\n" + "                            }\n" + "                        }\n");
    }
    return returnstring.toString();
  }

  /**
   * The method <code>checkField</code> checks the contents of the field.
   *
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @return true if the contents is correct
   */
  @Override
  public boolean checkField(final HttpSession session, final int posnumber) {
    final String contents = this.getContents(session, posnumber);
    boolean returnwert = false;

    if (fieldname == null) {
      returnwert = true;
    } else {
      if (complusion && (contents == null || contents.equals(StringUtils.EMPTY))) {
        returnwert = false;
      } else {
        returnwert = true;
        if (contents != null && !contents.equals(StringUtils.EMPTY)) {
          returnwert = contents.length() == 13;
          if (returnwert) {
            for (int j = 0; j < contents.length() && returnwert; j++) {
              returnwert = Character.isDigit(contents.charAt(j));
            }
          }
          if (returnwert) {
            final int w1 = contents.charAt(0) - '0' + contents.charAt(2) - '0' + contents.charAt(4) - '0' + contents.charAt(6)
                - '0' + contents.charAt(8) - '0' + contents.charAt(10) - '0';

            final int w2 = (contents.charAt(1) - '0' + contents.charAt(3) - '0' + contents.charAt(5) - '0' + contents.charAt(7)
                - '0' + contents.charAt(9) - '0' //
                + contents.charAt(11) - '0') * 3;

            int ps = 10 - (w1 + w2) % 10;
            if (ps > 9) {
              ps = 0;
            }

            returnwert = contents.charAt(12) == ps + '0';
          }
        }
      }
    }
    return returnwert;
  }

}
