/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.common.field;

import de.knightsoft.common.Constants;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpSession;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * The <code>PopupField</code> class is based on the abstract AbstractBaseField class and implement the popup-button.
 *
 * @author Manfred Tremmel
 * @version 1.2.0, 22.07.2008
 */
public class PopupField extends de.knightsoft.common.field.AbstractBaseField {

  protected final String[] values;
  protected final String[] displayvalues;
  protected final String sessionvalues;
  protected final String sessiondisplayvalues;
  protected final String sqlString;
  protected String sqlWorkString;

  /**
   * Constructor, used for fix PopupField entries.
   *
   * @param servletname Name of the servlet
   * @param enchentment Label, displayed in the HTML-site
   * @param fieldname Fieldname, used in the HTML-site
   * @param dbfieldname Fieldname, used in the database
   * @param length Not used in this field-type
   * @param fieldlength Not used in this field-type
   * @param complusion Must be filled or not
   * @param values Values should be the same as in database
   * @param displayvalues Values to display, can be more readable
   */
  public PopupField(final String servletname, final String enchentment, final String fieldname, final String dbfieldname,
      final int length, final int fieldlength, final boolean complusion, final String[] values, final String[] displayvalues) {
    super(servletname, enchentment, fieldname, dbfieldname, length, fieldlength, complusion);
    this.values = values;
    this.displayvalues = displayvalues;
    myDataBase = null;
    sqlString = null;
    sessionvalues = null;
    sessiondisplayvalues = null;
  }

  /**
   * Constructor, used for fix PopupField entries.
   *
   * @param servletname Name of the servlet
   * @param enchentment Label, displayed in the HTML-site
   * @param fieldname Fieldname, used in the HTML-site
   * @param dbfieldname Fieldname, used in the database
   * @param length Not used in this field-type
   * @param fieldlength Not used in this field-type
   * @param complusion Must be filled or not
   * @param sessionvalues name to read the table from (sessiontable)
   * @param sessiondisplayvalues name to read the display table from (sessiontable)
   */
  public PopupField(final String servletname, final String enchentment, final String fieldname, final String dbfieldname,
      final int length, final int fieldlength, final boolean complusion, final String sessionvalues,
      final String sessiondisplayvalues) {
    super(servletname, enchentment, fieldname, dbfieldname, length, fieldlength, complusion);
    values = null;
    displayvalues = null;
    myDataBase = null;
    sqlString = null;
    this.sessionvalues = sessionvalues;
    this.sessiondisplayvalues = sessiondisplayvalues;
  }

  /**
   * Constructor, used for flexible PopupField entries, taken from the database
   *
   * @param servletname Name of the servlet
   * @param enchentment Label, displayed in the HTML-site
   * @param fieldname Fieldname, used in the HTML-site
   * @param dbfieldname Fieldname, used in the database
   * @param length Not used in this field-type
   * @param fieldlength Not used in this field-type
   * @param complusion Must be filled or not
   * @param sqlString Use a prepared SQL statement that gives back to fields, value and displayvalue. They contain the entries
   *        of the Field.
   */
  public PopupField(final String servletname, final String enchentment, final String fieldname, final String dbfieldname,
      final int length, final int fieldlength, final boolean complusion, final String sqlString) {
    super(servletname, enchentment, fieldname, dbfieldname, length, fieldlength, complusion);
    values = null;
    displayvalues = null;
    this.sqlString = sqlString;
    sessionvalues = null;
    sessiondisplayvalues = null;
  }

  /**
   * Setup a new DataBase connection.
   *
   * @param pnewDataBase Database connection
   */
  @Override
  public void setNewDataBase(final Connection pnewDataBase) throws SQLException {
    super.setNewDataBase(pnewDataBase);

    if (sqlString != null) {
      try {
        final de.knightsoft.common.DataBaseDepending MyDataBaseDepending =
            new de.knightsoft.common.DataBaseDepending(pnewDataBase.getMetaData().getDatabaseProductName());
        sqlWorkString = sqlString.replaceAll("NOW\\(\\)", MyDataBaseDepending.getSqlTimeNow());
      } catch (final Exception e) {
        sqlWorkString = sqlString;
      }
    }
  }

  /**
   * The method <code>htmlInputField</code> returns the input field for html formulars.
   *
   * @param tabpos positionnummber of the field
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @param type action type to handle when this field is changed at least
   * @return html inputfield
   */
  @Override
  public String htmlInputField(final int tabpos, final HttpSession session, final int posnumber, final String type) {
    if (enchentment == null) {
      return StringUtils.EMPTY;
    } else {
      final String htmlfieldname = de.knightsoft.common.StringToHtml.convert(this.getfieldname(posnumber), false, true, true);
      final StringBuilder sb = new StringBuilder();
      sb.append("<select id=\"" + htmlfieldname + "\" name=\"" + htmlfieldname + "\" size=\"" + Integer.toString(fieldlength)
          + "\" tabindex=\"" + Integer.toString(tabpos) + "\" onChange=\"return checkRegEx('" + htmlfieldname + "', /" + regEx
          + "/, " + complusion + ", '" + type + "', false);\">");
      final String contents = this.getContents(session, posnumber);

      String value;
      String displayvalue;
      String selected;

      if (sqlString == null) {
        String[] valuesNow;
        String[] displayvaluesNow;

        if (sessionvalues == null) {
          valuesNow = values;
          displayvaluesNow = displayvalues;
        } else {
          valuesNow = (String[]) session.getAttribute(sessionvalues);
          displayvaluesNow = (String[]) session.getAttribute(sessiondisplayvalues);
        }

        if (valuesNow == null) {
          sb.append(de.knightsoft.common.StringToHtml.convert(contents));
        } else {
          for (int i = 0; i < valuesNow.length; i++) {
            if (contents != null && contents.equals(valuesNow[i])
                || valuesNow[i].equals(StringUtils.EMPTY) && contents == null) {
              selected = " selected";
            } else {
              selected = StringUtils.EMPTY;
            }
            sb.append("<option value=\"" + de.knightsoft.common.StringToHtml.convert(valuesNow[i], false, true, true) + "\""
                + selected + ">" + de.knightsoft.common.StringToHtml.convert(displayvaluesNow[i], false, true, true)
                + "</option>");
          }
        }
      } else {
        if (!complusion) {
          if (contents == null || contents.equals(StringUtils.EMPTY)) {
            sb.append("<option value=\"\" selected>--</option>");
          } else {
            sb.append("<option value=\"\">--</option>");
          }
        }
        try {
          final Integer MandatorInteger = (Integer) session.getAttribute(servletname + Constants.DB_FIELD_GLOBAL_MANDATOR);
          try (final PreparedStatement sqlStatement = myDataBase.prepareStatement(sqlWorkString)) {
            sqlStatement.clearParameters();
            sqlStatement.setInt(1, MandatorInteger == null ? 1 : MandatorInteger.intValue());
            try (final ResultSet result = sqlStatement.executeQuery()) {

              while (result.next()) {
                value = result.getString("value");
                displayvalue = result.getString("valuedisplay");
                if (contents != null && contents.equals(value)) {
                  selected = " selected";
                } else {
                  selected = StringUtils.EMPTY;
                }
                sb.append(
                    "<option value=\"" + de.knightsoft.common.StringToHtml.convert(value, false, true, true) + "\"" + selected
                        + ">" + de.knightsoft.common.StringToHtml.convert(displayvalue, false, true, true) + "</option>");
              }
            }
          }
        } catch (final java.sql.SQLException e) {
          System.err.println("SQL-Fehler beim ermitteln der select-Inhalte: " + e.toString());
        }
      }

      sb.append("</select>");

      return sb.toString();
    }
  }

  /**
   * The method <code>htmlViewField</code> returns the html view field.
   *
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @return html viewfield
   */
  @Override
  public String htmlViewField(final HttpSession session, final int posnumber) {
    if (enchentment == null) {
      return StringUtils.EMPTY;
    } else {
      final String htmlfieldname = de.knightsoft.common.StringToHtml.convert(this.getfieldname(posnumber), false, true, true);
      final String contents = this.getContents(session, posnumber);
      String contentsView = null;
      int pos = 0;
      if (contents != null) {
        if (sqlString == null) {
          String[] valuesNow;
          String[] displayvaluesNow;

          if (sessionvalues == null) {
            valuesNow = values;
            displayvaluesNow = displayvalues;
          } else {
            valuesNow = (String[]) session.getAttribute(sessionvalues);
            displayvaluesNow = (String[]) session.getAttribute(sessiondisplayvalues);
          }
          if (valuesNow == null) {
            contentsView = contents;
          } else {
            for (pos = 0; pos < valuesNow.length && contentsView == null; pos++) {
              if (contents.equals(valuesNow[pos])) {
                contentsView = displayvaluesNow[pos];
              }
            }
          }
        } else {
          try {
            final Integer MandatorInteger = (Integer) session.getAttribute(servletname + Constants.DB_FIELD_GLOBAL_MANDATOR);
            try (final PreparedStatement sqlStatement = myDataBase.prepareStatement(sqlWorkString)) {
              sqlStatement.clearParameters();
              sqlStatement.setInt(1, MandatorInteger == null ? 1 : MandatorInteger.intValue());
              try (final ResultSet result = sqlStatement.executeQuery()) {

                while (result.next() && contentsView == null) {
                  if (result.getString("value").equals(contents)) {
                    contentsView = result.getString("valuedisplay");
                  }
                }
              }
            }
          } catch (final java.sql.SQLException e) {
            System.err.println("SQL-Fehler beim ermitteln der select-Inhalte: " + e.toString());
          }
        }
      }
      if (contentsView == null) {
        contentsView = contents;
      }
      return "<input type=\"hidden\" name=\"" + htmlfieldname + "\" value=\""
          + de.knightsoft.common.StringToHtml.convert(contents, false, true, true) + "\">"
          + de.knightsoft.common.StringToHtml.convert(contentsView, false, true, true);
    }
  }

  /**
   * The method <code>javascriptCheck</code> returns javascript code to check the input of the field.
   *
   * @param posnumber number of the position or -1 for headerfields
   * @param type action type to handle when this field is changed at least
   * @return html inputfield
   */
  @Override
  public String javascriptCheck(final int posnumber, final String type) {
    return StringUtils.EMPTY;
  }

  /**
   * The method <code>checkField</code> checks the contents of the field.
   *
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @return true if the contents is correct
   */
  @Override
  public boolean checkField(final HttpSession session, final int posnumber) {
    return true;
  }
}
