/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.common.field;

import de.knightsoft.common.Constants;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpSession;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * The <code>CostCenterField</code> class is a popup or text input field with check of the CostCenterField against the database.
 *
 * @author Manfred Tremmel
 * @version 1.2.0 22.07.2008
 */
public class CostCenterField extends de.knightsoft.common.field.NumberField {
  protected String popUpAsqlString;
  protected String popUpBsqlRString;
  protected final String sqlString;
  private static final String REFERENCE_TEXT = "referenz_";
  private static final String DEFAULT_TEXT = "vorgabe_";
  private static final String VIEW_ONLY_TEXT = "nur_anzeigen_";
  private static final String SELECTION_TEXT = "auswahl_";
  private static final String SELECTION_REGION_TEXT = "auswahl_region_";
  private static final String SELECTION_RVZ_TXT = "auswahl_rvz_";
  private static final String SELECTION_RVZ_SELECT = "select_rvz_";
  private static final String INPUT_FIELD_TEXT = "eingabefeld_";

  /**
   * Constructor.
   *
   * @param servletname Name of the servlet
   * @param enchentment Label, displayed in the HTML-site
   * @param fieldname Fieldname, used in the HTML-site
   * @param dbfieldname Fieldname, used in the database
   * @param complusion Must be filled or not
   * @param sqlString SQL-Statement to access costcenter-table
   */
  public CostCenterField(final String servletname, final String enchentment, final String fieldname, final String dbfieldname,
      final int fieldlength, final boolean complusion, final String sqlString) {
    super(servletname, enchentment, fieldname, dbfieldname, 13, fieldlength, complusion);
    this.sqlString = sqlString;
  }

  /**
   * Setup a new SQL String for PopupField B (Region).
   *
   * @param ppopUpAsqlString New SQL String
   */
  public void setPopUpAsql(final String ppopUpAsqlString) {
    popUpAsqlString = ppopUpAsqlString;
  }

  /**
   * Setup a new SQL String for PopupField B (Standort).
   *
   * @param ppopUpBsqlRString New SQL String
   */
  public void setPopUpBsql(final String ppopUpBsqlRString) {
    popUpBsqlRString = ppopUpBsqlRString;
  }

  /**
   * Setup a new DataBase connection.
   *
   * @param newDataBase Database connection
   */
  @Override
  public void setNewDataBase(final Connection newDataBase) throws java.sql.SQLException {
    myDataBase = newDataBase;
  }

  /**
   * The method <code>switchToReadOnly</code> switches to the read only mode.
   *
   * @param newDefault new default entry
   * @param session The Data of the current Session
   */
  public void switchToReadOnly(final int newDefault, final HttpSession session) {
    session.setAttribute(servletname + CostCenterField.DEFAULT_TEXT + fieldname, Integer.valueOf(newDefault));
    session.setAttribute(servletname + CostCenterField.VIEW_ONLY_TEXT + fieldname, Boolean.TRUE);
    session.setAttribute(servletname + CostCenterField.SELECTION_TEXT + fieldname, Boolean.FALSE);
    session.setAttribute(servletname + CostCenterField.SELECTION_REGION_TEXT + fieldname, Boolean.FALSE);
    session.setAttribute(servletname + CostCenterField.INPUT_FIELD_TEXT + fieldname, Boolean.FALSE);
    session.setAttribute(servletname + CostCenterField.SELECTION_RVZ_TXT + fieldname, Boolean.FALSE);
  }

  /**
   * The method <code>switchToSelect</code> switches to the PopupField mode (Standort).
   *
   * @param session The Data of the current Session
   */
  public void switchToSelect(final HttpSession session) {
    session.setAttribute(servletname + CostCenterField.VIEW_ONLY_TEXT + fieldname, Boolean.FALSE);
    session.setAttribute(servletname + CostCenterField.SELECTION_TEXT + fieldname, Boolean.TRUE);
    session.setAttribute(servletname + CostCenterField.SELECTION_REGION_TEXT + fieldname, Boolean.FALSE);
    session.setAttribute(servletname + CostCenterField.INPUT_FIELD_TEXT + fieldname, Boolean.FALSE);
    session.setAttribute(servletname + CostCenterField.SELECTION_RVZ_TXT + fieldname, Boolean.FALSE);
  }

  /**
   * The method <code>switchToSelectRegion</code> switches to the PopupField mode (Region).
   *
   * @param session The Data of the current Session
   */
  public void switchToSelectRegion(final HttpSession session) {
    session.setAttribute(servletname + CostCenterField.VIEW_ONLY_TEXT + fieldname, Boolean.FALSE);
    session.setAttribute(servletname + CostCenterField.SELECTION_TEXT + fieldname, Boolean.FALSE);
    session.setAttribute(servletname + CostCenterField.SELECTION_REGION_TEXT + fieldname, Boolean.TRUE);
    session.setAttribute(servletname + CostCenterField.INPUT_FIELD_TEXT + fieldname, Boolean.FALSE);
    session.setAttribute(servletname + CostCenterField.SELECTION_RVZ_TXT + fieldname, Boolean.FALSE);
  }

  /**
   * The method <code>switchToInput</code> switches to the input (TextField) mode.
   *
   * @param session The Data of the current Session
   */
  public void switchToInput(final HttpSession session) {
    session.setAttribute(servletname + CostCenterField.VIEW_ONLY_TEXT + fieldname, Boolean.FALSE);
    session.setAttribute(servletname + CostCenterField.SELECTION_TEXT + fieldname, Boolean.FALSE);
    session.setAttribute(servletname + CostCenterField.SELECTION_REGION_TEXT + fieldname, Boolean.FALSE);
    session.setAttribute(servletname + CostCenterField.INPUT_FIELD_TEXT + fieldname, Boolean.TRUE);
    session.setAttribute(servletname + CostCenterField.SELECTION_RVZ_TXT + fieldname, Boolean.FALSE);
  }

  /**
   * The method <code>switchToRVZ</code> switches to the PopupField mode (RVZ).
   *
   * @param session The Data of the current Session
   * @param costCenter A list of Kostenstellen to select
   * @param popUpName Names displayed in the PopUp Field
   */
  public void switchToRvz(final HttpSession session, final long[] costCenter, final String[] popUpName) {
    session.setAttribute(servletname + CostCenterField.VIEW_ONLY_TEXT + fieldname, Boolean.FALSE);
    session.setAttribute(servletname + CostCenterField.SELECTION_TEXT + fieldname, Boolean.FALSE);
    session.setAttribute(servletname + CostCenterField.SELECTION_REGION_TEXT + fieldname, Boolean.FALSE);
    session.setAttribute(servletname + CostCenterField.INPUT_FIELD_TEXT + fieldname, Boolean.FALSE);
    session.setAttribute(servletname + CostCenterField.SELECTION_RVZ_TXT + fieldname, Boolean.TRUE);
    session.setAttribute(servletname + CostCenterField.SELECTION_RVZ_SELECT + fieldname, costCenter);
    session.setAttribute(servletname + CostCenterField.SELECTION_RVZ_SELECT + fieldname + "Disp", popUpName);
  }

  /**
   * The method <code>htmlInputField</code> returns the input field for html formulars.
   *
   * @param tabpos positionnummber of the field
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @param type action type to handle when this field is changed at least
   * @return html inputfield
   */
  @Override
  public String htmlInputField(final int tabpos, final HttpSession session, final int posnumber, final String type) {
    String returnstring = null;
    final Boolean eingabefeldB = (Boolean) session.getAttribute(servletname + CostCenterField.INPUT_FIELD_TEXT + fieldname);
    final Boolean auswahlB = (Boolean) session.getAttribute(servletname + CostCenterField.SELECTION_TEXT + fieldname);
    final Boolean auswahlRegioB =
        (Boolean) session.getAttribute(servletname + CostCenterField.SELECTION_REGION_TEXT + fieldname);
    final Boolean auswahlRvzB = (Boolean) session.getAttribute(servletname + CostCenterField.SELECTION_RVZ_TXT + fieldname);
    boolean eingabefeld = false;
    boolean auswahl = false;
    boolean auswahlRegion = false;
    boolean auswahlRvz = false;

    if (eingabefeldB != null) {
      eingabefeld = eingabefeldB.booleanValue();
    }

    if (auswahlB != null) {
      auswahl = auswahlB.booleanValue();
    }

    if (auswahlRegioB != null) {
      auswahlRegion = auswahlRegioB.booleanValue();
    }

    if (auswahlRvzB != null) {
      auswahlRvz = auswahlRvzB.booleanValue();
    }

    if (enchentment == null) {
      returnstring = StringUtils.EMPTY;
    } else if (eingabefeld) {
      returnstring = super.htmlInputField(tabpos, session, posnumber, type);
    } else if (auswahl || auswahlRegion || auswahlRvz) {
      String htmlfieldname = de.knightsoft.common.StringToHtml.convert(this.getfieldname(posnumber), false, true, true);
      final String contents = this.getContents(session, posnumber);
      String selected = StringUtils.EMPTY;
      String sqlvalue = StringUtils.EMPTY;
      final StringBuilder sb = new StringBuilder();

      session.setAttribute(servletname + CostCenterField.REFERENCE_TEXT + this.getfieldname(posnumber), StringUtils.EMPTY);

      if (posnumber >= 0) {
        htmlfieldname += Integer.toString(posnumber); // NOPMD
      }

      sb.append("<select id=\"" + htmlfieldname + "\" name=\"" + htmlfieldname + "\" size=\"1\" tabindex=\"" + tabpos
          + "\" onBlur=\"return checkRegEx('" + htmlfieldname + "', /" + regEx + "/, " + complusion + ", '" + type
          + "', false);\">");
      if (!complusion) {
        if (contents == null || contents.equals(StringUtils.EMPTY)) {
          sb.append("<option value=\"\" selected>--</option>");
        } else {
          sb.append("<option value=\"\">--</option>");
        }
      }
      if (auswahl || auswahlRegion) {
        try (final PreparedStatement thisStatement =
            myDataBase.prepareStatement(auswahlRegion ? popUpAsqlString : popUpBsqlRString)) {
          final Integer MandatorInteger = (Integer) session.getAttribute(servletname + Constants.DB_FIELD_GLOBAL_MANDATOR);
          thisStatement.clearParameters();
          thisStatement.setInt(1, MandatorInteger == null ? 1 : MandatorInteger.intValue());
          thisStatement.setString(2, ((Integer) session.getAttribute(servletname + "iphone_standort")).toString());
          try (final ResultSet result = thisStatement.executeQuery()) {

            while (result.next()) {
              sqlvalue = result.getString("value");
              selected = contents != null && contents.equals(sqlvalue) ? " selected" : StringUtils.EMPTY;
              sb.append(
                  "<option value=\"" + de.knightsoft.common.StringToHtml.convert(sqlvalue, false, true, true) + "\"" + selected
                      + ">" + de.knightsoft.common.StringToHtml.convert(result.getString("valuedisplay"), false, true, true)
                      + "</option>");
            }
          }

        } catch (final java.sql.SQLException e) {
          System.err.println("SQL-Fehler beim ermitteln der select-Inhalte: " + e.toString());
        }
      } else {
        final long[] costCenter = (long[]) session.getAttribute(servletname + CostCenterField.SELECTION_RVZ_SELECT + fieldname);
        final String[] costCenterDisplay =
            (String[]) session.getAttribute(servletname + CostCenterField.SELECTION_RVZ_SELECT + fieldname + "Disp");
        if (costCenter != null) {
          for (int i = 0; i < costCenter.length; i++) {
            sqlvalue = Long.toString(costCenter[i]);
            selected = contents != null && contents.equals(sqlvalue) ? " selected" : StringUtils.EMPTY;
            sb.append(
                "<option value=\"" + de.knightsoft.common.StringToHtml.convert(sqlvalue, false, true, true) + "\"" + selected
                    + ">" + de.knightsoft.common.StringToHtml.convert(costCenterDisplay[i], false, true, true) + "</option>");
          }
        }
      }

      sb.append("</select>");
      returnstring = sb.toString();
    } else {
      returnstring = this.htmlViewField(session, posnumber);
    }
    return returnstring;
  }

  /**
   * The method <code>htmlInputTableSegment</code> returns the input field including enchantement and table structure for html
   * formulars.
   *
   * @param tabpos positionnummber of the field
   * @param session The Data of the current Session
   * @return html inputfield
   */
  @Override
  public String htmlInputTableSegment(final int tabpos, final HttpSession session) {
    final StringBuilder returnstring = new StringBuilder(256);
    final Boolean selectionB = (Boolean) session.getAttribute(servletname + CostCenterField.SELECTION_TEXT + fieldname);
    boolean auswahl = false;

    if (selectionB != null) {
      auswahl = selectionB.booleanValue();
    }

    if (enchentment == null) {
      // nothing to do.
    } else if (complusion) {
      returnstring.append("                    <td><label class=\"complusion\" for=\""
          + de.knightsoft.common.StringToHtml.convert(fieldname, false, true, true) + "\">"
          + de.knightsoft.common.StringToHtml.convert(enchentment) + "&nbsp;&sup1;</label></td>\n");
    } else {
      returnstring.append("                    <td><label class=\"free\" for=\""
          + de.knightsoft.common.StringToHtml.convert(fieldname, false, true, true) + "\">"
          + de.knightsoft.common.StringToHtml.convert(enchentment) + "&nbsp;&sup2;</label></td>\n");
    }

    if (auswahl) {
      returnstring.append("                    <td colspan=\"3\">" + this.htmlInputField(tabpos, session) + "</td>\n");
    } else {
      returnstring.append("                    <td>" + this.htmlInputField(tabpos, session) + "</td>\n"
          + "                    <td colspan=\"2\">"
          + de.knightsoft.common.StringToHtml.convert(
              (String) session.getAttribute(servletname + CostCenterField.REFERENCE_TEXT + fieldname), false, true, true)
          + "</td>\n");
    }
    return returnstring.toString();
  }

  /**
   * The method <code>htmlInputTableSegment</code> returns the input field including enchantement and table structure for html
   * formulars.
   *
   * @param tabpos positionnummber of the field
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @param type action type to handle when this field is changed at least
   * @return html inputfield
   */
  @Override
  public String htmlInputTableSegment(final int tabpos, final HttpSession session, final int posnumber, final String type) {
    String returnstring = null;
    final Boolean auswahl_B = (Boolean) session.getAttribute(servletname + CostCenterField.SELECTION_TEXT + fieldname);
    boolean auswahl = false;

    if (auswahl_B != null) {
      auswahl = auswahl_B.booleanValue();
    }

    if (enchentment == null) {
      returnstring = StringUtils.EMPTY;
    } else {
      if (auswahl) {
        returnstring = "                    <td class=\"borderleft\" colspan=\"2\">"
            + this.htmlInputField(tabpos, session, posnumber, type) + "</td>\n";
      } else {
        returnstring = "                    <td class=\"borderleft\">" + this.htmlInputField(tabpos, session, posnumber, type)
            + "</td><td class=\"borderleft\">"
            + de.knightsoft.common.StringToHtml.convert(
                (String) session.getAttribute(servletname + CostCenterField.REFERENCE_TEXT + this.getfieldname(posnumber)))
            + "</td>\n";
      }
    }

    return returnstring;
  }

  /**
   * The method <code>numTDs</code> returns the number td tags the htmlInputTableSegment method creates.
   *
   * @return number TD tags
   */
  @Override
  public int numTDs() {
    return 4;
  }

  /**
   * The method <code>htmlViewField</code> returns the html view field.
   *
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @return html viewfield
   */
  @Override
  public String htmlViewField(final HttpSession session, final int posnumber) {
    if (enchentment == null) {
      return StringUtils.EMPTY;
    } else {
      final String htmlfieldname = de.knightsoft.common.StringToHtml.convert(this.getfieldname(posnumber), false, true, true);
      String htmlcontents = this.htmlStringContents(session, posnumber);
      final Boolean nur_anzeige_B = (Boolean) session.getAttribute(servletname + CostCenterField.VIEW_ONLY_TEXT + fieldname);
      boolean displayOnly = true;
      final Integer Vorgabe_I = (Integer) session.getAttribute(servletname + CostCenterField.DEFAULT_TEXT + fieldname);
      int vorgabe = 0;

      if (nur_anzeige_B != null) {
        displayOnly = nur_anzeige_B.booleanValue();
      }

      if (Vorgabe_I != null) {
        vorgabe = Vorgabe_I.intValue();
      }

      if ((htmlcontents == null || htmlcontents.equals(StringUtils.EMPTY)) && displayOnly && vorgabe > 0) {
        htmlcontents = Integer.toString(vorgabe);
      }

      this.checkField(session, posnumber);
      return "<input type=\"hidden\" name=\"" + htmlfieldname + "\" value=\"" + htmlcontents + "\">" + htmlcontents;
    }
  }

  /**
   * The method <code>htmlViewTableSegment</code> returns the html view field including enchantement and table structure.
   *
   * @param session The Data of the current Session
   * @return html viewfield
   */
  @Override
  public String htmlViewTableSegment(final HttpSession session) {
    if (enchentment == null) {
      return StringUtils.EMPTY;
    } else {
      return "                    <td>" + de.knightsoft.common.StringToHtml.convert(enchentment) + "</td>\n"
          + "                    <td>" + this.htmlViewField(session) + "</td>\n" + "                    <td colspan=\"2\">"
          + de.knightsoft.common.StringToHtml.convert(
              (String) session.getAttribute(servletname + CostCenterField.REFERENCE_TEXT + fieldname), false, true, true)
          + "</td>\n";
    }
  }

  /**
   * The method <code>htmlViewTableSegment</code> returns the html view field including enchantement and table structure.
   *
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @return html viewfield
   */
  @Override
  public String htmlViewTableSegment(final HttpSession session, final int posnumber) {
    if (enchentment == null) {
      return StringUtils.EMPTY;
    } else {
      return "                    <td class=\"borderleft\">" + this.htmlViewField(session, posnumber)
          + "</td><td class=\"borderleft\">"
          + de.knightsoft.common.StringToHtml.convert(
              (String) session.getAttribute(servletname + CostCenterField.REFERENCE_TEXT + this.getfieldname(posnumber)))
          + "</td>\n";
    }
  }

  /**
   * The method <code>checkField</code> checks the contents of the field.
   *
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @return true if the contents is correct
   */
  @Override
  public boolean checkField(final HttpSession session, final int posnumber) {
    final String contents = this.getContents(session, posnumber);
    boolean returnwert = false;
    String referenz = null;
    final Boolean eingabefeld_B = (Boolean) session.getAttribute(servletname + CostCenterField.INPUT_FIELD_TEXT + fieldname);
    boolean eingabefeld = false;

    if (eingabefeld_B != null) {
      eingabefeld = eingabefeld_B.booleanValue();
    }

    if (eingabefeld) {
      if (complusion && (contents == null || contents.equals(StringUtils.EMPTY))) {
        returnwert = false;
      } else {
        returnwert = true;
        if (StringUtils.isNoneEmpty(contents)) {
          for (int j = 0; j < contents.length() && returnwert; j++) {
            returnwert = Character.isDigit(contents.charAt(j));
          }
          if (returnwert) {
            boolean auswahlRegion = false;
            final Boolean auswahlRegioB =
                (Boolean) session.getAttribute(servletname + CostCenterField.SELECTION_REGION_TEXT + fieldname);
            if (auswahlRegioB != null) {
              auswahlRegion = auswahlRegioB.booleanValue();
            }
            try (final PreparedStatement thisStatement =
                myDataBase.prepareStatement(auswahlRegion ? popUpAsqlString : popUpBsqlRString)) {
              final Integer MandatorInteger = (Integer) session.getAttribute(servletname + Constants.DB_FIELD_GLOBAL_MANDATOR);
              thisStatement.clearParameters();
              thisStatement.setInt(1, MandatorInteger == null ? 1 : MandatorInteger.intValue());
              thisStatement.setString(2, contents);
              try (final ResultSet result = thisStatement.executeQuery()) {

                if (result.next()) {
                  referenz = result.getString("reference");
                } else {
                  referenz = "Ungueltige Kostenstelle";
                  returnwert = false;
                }
              }
            } catch (final SQLException e) {
              referenz = "Fehler bei Kostenstellenermittlung: " + e.toString();
            }
          }
        }
      }
    } else {
      returnwert = true;
    }

    session.setAttribute(servletname + CostCenterField.REFERENCE_TEXT + this.getfieldname(posnumber), referenz);

    return returnwert;
  }
}
