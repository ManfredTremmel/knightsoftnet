/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.common.field;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * The <code>CheckBoxField</code> class is a CheckBoxField input field
 *
 * @author Manfred Tremmel
 * @version 1.2.0 22.07.2008
 */
public class CheckBoxField extends de.knightsoft.common.field.AbstractBaseField {

  /**
   * Constructor.
   *
   * @param servletname Name of the servlet
   * @param enchentment Label, displayd in the HTML-site
   * @param fieldname Fieldname, used in the HTML-site
   * @param dbfieldname Fieldname, used in the database
   * @param length maximum length of the number
   * @param fieldlength field length
   * @param complusion Must be filled or not
   */
  public CheckBoxField(final String servletname, final String enchentment, final String fieldname, final String dbfieldname,
      final int length, final int fieldlength, final boolean complusion) {
    super(servletname, enchentment, fieldname, dbfieldname, length, fieldlength, complusion, "^(true|false)$");
  }

  /**
   * Initial Fields.
   *
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   */
  @Override
  public void initField(final HttpSession session, final int posnumber) {
    if (fieldname != null) {
      session.setAttribute(servletname + this.getfieldname(posnumber), "false");
    }
  }

  /**
   * The method <code>htmlRead</code> reads out the HTML request Data (input of the html form).
   *
   * @param req Request data from the servlet
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   */
  @Override
  public void htmlRead(final HttpServletRequest req, final HttpSession session, //
      final int posnumber) {
    if (fieldname != null && enchentment != null) {
      final String thisFieldName = this.getfieldname(posnumber);
      String contents = req.getParameter(thisFieldName);
      if (contents == null || contents.equals("false")) {
        contents = "false";
      } else {
        contents = "true";
      }
      session.setAttribute(servletname + thisFieldName, contents);
    }
  }

  /**
   * The method <code>sqlRead</code> reads out the SQL result Data (SQL select).
   *
   * @param result ResultSet of the SQL read
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @exception SQLException if a database access error occurs
   */
  @Override
  public void sqlRead(final ResultSet result, final HttpSession session, final int posnumber) throws SQLException {
    if (fieldname != null && dbfieldname != null) {
      final boolean contents_b = result.getBoolean(dbfieldname);
      session.setAttribute(servletname + this.getfieldname(posnumber), contents_b ? "true" : "false");
    }
  }

  /**
   * The method <code>getContents</code> returns the contents of the field.
   *
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @return contents of the field
   */
  @Override
  public String getContents(final HttpSession session, final int posnumber) {
    if (fieldname == null) {
      return null;
    } else {
      String returnstring = (String) session.getAttribute(servletname + this.getfieldname(posnumber));
      if (!"true".equals(returnstring)) {
        returnstring = "false";
      }
      return returnstring;
    }
  }

  /**
   * The method <code>htmlInputField</code> returns the input field for html formulars.
   *
   * @param tabpos positionnummber of the field
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @param type action type to handle when this field is changed at least
   * @return html inputfield
   */
  @Override
  public String htmlInputField(final int tabpos, final HttpSession session, final int posnumber, final String type) {
    if (enchentment == null) {
      return StringUtils.EMPTY;
    } else if (fieldname == null) {
      return "&nbsp;";
    } else {
      final String htmlfieldname = de.knightsoft.common.StringToHtml.convert(this.getfieldname(posnumber), false, true, true);
      final String classstring = this.checkField(session, posnumber) ? "default" : "faulty";
      final String selected = this.getContents(session, posnumber).equals("true") ? " checked" : StringUtils.EMPTY;
      final String returnstring = "<input type=\"checkbox\" class=\"" + classstring + "\" id=\"" + htmlfieldname + "\" name=\""
          + htmlfieldname + "\" value=\"true\" tabindex=\"" + Integer.toString(tabpos) + "\"" + selected
          + " onClick=\"document.forms[0].doformcheck.value='true';" + "document.forms[0].buttonpressvalue='" + type + "';\">";
      return returnstring;
    }
  }

  /**
   * The method <code>htmlInputTableSegment</code> returns the input field including enchantement and table structure for html
   * formulars.
   *
   * @param tabpos positionnummber of the field
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @param type action type to handle when this field is changed at least
   * @return html inputfield
   */
  @Override
  public String htmlInputTableSegment(final int tabpos, final HttpSession session, final int posnumber, final String type) {
    if (enchentment == null) {
      return StringUtils.EMPTY;
    } else {
      return "                    <td class=\"bordercenter\">" + this.htmlInputField(tabpos, session, posnumber, type)
          + "</td>\n";
    }
  }

  /**
   * The method <code>javascriptCheck</code> returns javascript code to check the input of the field.
   *
   * @param posnumber number of the position or -1 for headerfields
   * @param type action type to handle when this field is changed at least
   * @return html inputfield
   */
  @Override
  public String javascriptCheck(final int posnumber, final String type) {
    return StringUtils.EMPTY;
  }

  /**
   * The method <code>htmlViewField</code> returns the html view field.
   *
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @return html viewfield
   */
  @Override
  public String htmlViewField(final HttpSession session, final int posnumber) {
    if (enchentment == null) {
      return StringUtils.EMPTY;
    } else if (fieldname == null) {
      return "&nbsp;";
    } else {
      final String htmlfieldname = de.knightsoft.common.StringToHtml.convert(this.getfieldname(posnumber), false, true, true);
      final String htmlcontents = this.htmlStringContents(session, posnumber);
      final String htmlviewcontent = Boolean.parseBoolean(htmlcontents) ? "X" : "&nbsp;";
      return "<input type=\"hidden\" name=\"" + htmlfieldname + "\" value=\"" + htmlcontents + "\">" + htmlviewcontent;
    }
  }

  /**
   * The method <code>htmlViewTableSegment</code> returns the html view field including enchantement and table structure.
   *
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @return html viewfield
   */
  @Override
  public String htmlViewTableSegment(final HttpSession session, final int posnumber) {
    if (enchentment == null) {
      return StringUtils.EMPTY;
    } else {
      return "                    <td class=\"bordercenter\">" + this.htmlViewField(session, posnumber) + "</td>\n";
    }
  }

  /**
   * The method <code>checkField</code> checks the contents of the field.
   *
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @return true if the contents is correct
   */
  @Override
  public boolean checkField(final HttpSession session, final int posnumber) {
    return true;
  }

  /**
   * The method <code>sqlPrepareItems</code> adds the entry to the fields of a prepared statement.
   *
   * @param myPreparedStatement prepared statement
   * @param thisDatabase the database connection
   * @param pentryNumber number of the field
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @return new EntryNumber
   * @exception SQLException
   */
  @Override
  public int sqlPrepareItems(final PreparedStatement myPreparedStatement, final Connection thisDatabase, final int pentryNumber,
      final HttpSession session, final int posnumber) throws SQLException {
    int entryNumber = pentryNumber;
    if (fieldname != null && dbfieldname != null) {
      final String contents = this.getContents(session, posnumber);
      final boolean contentsbool = Boolean.parseBoolean(contents);

      myPreparedStatement.setBoolean(entryNumber, contentsbool);

      entryNumber++;
    }
    return entryNumber;
  }

}
