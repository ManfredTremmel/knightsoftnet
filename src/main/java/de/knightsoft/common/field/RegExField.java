/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.common.field;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpSession;

/**
 * The <code>RegExField</code> class is text input field for regular expressions.
 *
 * @author Manfred Tremmel
 * @version 1.2.0, 22.07.2008
 */
public class RegExField extends de.knightsoft.common.field.AbstractBaseField {

  /**
   * Constructor.
   *
   * @param servletname Name of the servlet
   * @param enchentment Label, displayed in the HTML-site
   * @param fieldname Fieldname, used in the HTML-site
   * @param dbfieldname Fieldname, used in the database
   * @param length Not used in this field-type
   * @param fieldlength Not used in this field-type
   * @param complusion Must be filled or not
   */
  public RegExField(final String servletname, final String enchentment, final String fieldname, final String dbfieldname,
      final int length, final int fieldlength, final boolean complusion) {
    super(servletname, enchentment, fieldname, dbfieldname, length, fieldlength, complusion);
  }

  /**
   * The method <code>checkField</code> checks the contents of the field.
   *
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @return true if the contents is correct
   */
  @Override
  public boolean checkField(final HttpSession session, final int posnumber) {
    final String contents = this.getContents(session, posnumber);
    boolean returnwert = false;

    if (fieldname == null) {
      returnwert = true;
    } else {
      if (complusion && StringUtils.isEmpty(contents)) {
        // nothing to do
      } else {
        if (contents == null || regEx == null) {
          returnwert = true;
        } else {
          try {
            contents.matches(contents);
            returnwert = true;
          } catch (final java.util.regex.PatternSyntaxException e) {
            returnwert = false;
          }
        }
      }
    }
    return returnwert;
  }
}
