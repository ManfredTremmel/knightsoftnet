/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.common.field;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The <code>AbstractBaseField</code> class is AbstractBaseField of all field classes it is is abstract and has to be
 * implemented by the extending classes.
 *
 * @author Manfred Tremmel
 * @version 1.3.0, 28.07.2018
 */
public abstract class AbstractBaseField { // NOPMD

  protected String servletname; // Name of the servlet the field is used
  protected final String enchentment;// FeldBezeichnung (User)
  protected final String fieldname; // Feldname
  protected final String dbfieldname;// Feldname Datenbank
  protected final String regEx; // Regular expression to check input
  protected final Pattern regExPat; // compiled Regular expression
  protected final int length; // Feldlänge
  protected final int fieldlength; // Länge Ausgabefeld
  protected boolean complusion; // Zwangsfeld
  protected final String inputType;
  protected final String additionalParam;

  protected Connection myDataBase;

  /**
   * Constructor.
   *
   * @param servletname Name of the servlet
   * @param enchentment Label, displayd in the HTML-site
   * @param fieldname Fieldname, used in the HTML-site
   * @param dbfieldname Fieldname, used in the database
   * @param length Not used in this field-type
   * @param fieldlength Not used in this field-type
   * @param complusion Must be filled or not
   */
  public AbstractBaseField(final String servletname, final String enchentment, final String fieldname, final String dbfieldname,
      final int length, final int fieldlength, final boolean complusion) {
    this(servletname, enchentment, fieldname, dbfieldname, length, fieldlength, complusion, "..*");
  }

  /**
   * Constructor.
   *
   * @param servletname Name of the servlet
   * @param enchentment Label, displayd in the HTML-site
   * @param fieldname Fieldname, used in the HTML-site
   * @param dbfieldname Fieldname, used in the database
   * @param length Not used in this field-type
   * @param fieldlength Not used in this field-type
   * @param complusion Must be filled or not
   * @param regEx Regular Expression to check input
   */
  public AbstractBaseField(final String servletname, final String enchentment, final String fieldname, final String dbfieldname,
      final int length, final int fieldlength, final boolean complusion, final String regEx) {
    this(servletname, enchentment, fieldname, dbfieldname, length, fieldlength, complusion, regEx, "text");
  }

  /**
   * Constructor.
   *
   * @param servletname Name of the servlet
   * @param enchentment Label, displayd in the HTML-site
   * @param fieldname Fieldname, used in the HTML-site
   * @param dbfieldname Fieldname, used in the database
   * @param length Not used in this field-type
   * @param fieldlength Not used in this field-type
   * @param complusion Must be filled or not
   * @param regEx Regular Expression to check input
   * @param type input field type
   */
  public AbstractBaseField(final String servletname, final String enchentment, final String fieldname, final String dbfieldname,
      final int length, final int fieldlength, final boolean complusion, final String regEx, final String type) {
    this(servletname, enchentment, fieldname, dbfieldname, length, fieldlength, complusion, regEx, type, null);
  }

  /**
   * Constructor.
   *
   * @param servletname Name of the servlet
   * @param enchentment Label, displayd in the HTML-site
   * @param fieldname Fieldname, used in the HTML-site
   * @param dbfieldname Fieldname, used in the database
   * @param length Not used in this field-type
   * @param fieldlength Not used in this field-type
   * @param complusion Must be filled or not
   * @param regEx Regular Expression to check input
   * @param type input field type
   * @param additionalParam additional parameter to add
   */
  public AbstractBaseField(final String servletname, final String enchentment, final String fieldname, final String dbfieldname,
      final int length, final int fieldlength, final boolean complusion, final String regEx, final String type,
      final String additionalParam) {
    this.servletname = servletname;
    this.enchentment = enchentment;
    this.fieldname = fieldname;
    this.dbfieldname = dbfieldname;
    this.length = length;
    this.fieldlength = fieldlength;
    this.complusion = complusion;
    this.regEx = regEx;
    this.additionalParam = additionalParam;
    if (StringUtils.isEmpty(this.regEx)) {
      regExPat = null;
    } else {
      regExPat = Pattern.compile(this.regEx);
    }
    inputType = type;
  }

  /**
   * Setup a new DataBase connection.
   *
   * @param newDataBase Database connection
   */
  public void setNewDataBase(final Connection newDataBase) throws SQLException {
    myDataBase = newDataBase;
  }

  /**
   * Set a new servlet name.
   *
   * @param newServletname The new name of the Servlet
   */
  public void setNewServletname(final String newServletname) {
    servletname = newServletname;
  }

  /**
   * Initial Fields.
   *
   * @param session The Data of the current Session
   */
  public void initField(final HttpSession session) {
    this.initField(session, -1);
  }

  /**
   * Initial Fields.
   *
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   */
  public void initField(final HttpSession session, final int posnumber) {
    if (StringUtils.isNotEmpty(fieldname)) {
      session.setAttribute(servletname + this.getfieldname(posnumber), StringUtils.EMPTY);
    }
  }

  /**
   * Initial Fields.
   *
   * @param session The Data of the current Session
   * @param newEntry new entry for the field
   */
  public void setField(final HttpSession session, final String newEntry) {
    this.setField(session, newEntry, -1);
  }

  /**
   * Initial Fields.
   *
   * @param session The Data of the current Session
   * @param newEntry new entry for the field
   * @param posnumber number of the position or -1 for headerfields
   */
  public void setField(final HttpSession session, final String newEntry, final int posnumber) {
    if (StringUtils.isNotEmpty(fieldname)) {
      session.setAttribute(servletname + this.getfieldname(posnumber), newEntry);
    }
  }

  /**
   * The method <code>htmlRead</code> reads out the HTML request Data (input of the html form).
   *
   * @param req Request data from the servlet
   * @param session The Data of the current Session
   */
  public void htmlRead(final HttpServletRequest req, final HttpSession session) {
    this.htmlRead(req, session, -1);
  }

  /**
   * The method <code>htmlRead</code> reads out the HTML request Data (input of the html form).
   *
   * @param req Request data from the servlet
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   */
  public void htmlRead(final HttpServletRequest req, final HttpSession session, //
      final int posnumber) {
    if (StringUtils.isNotEmpty(fieldname) && StringUtils.isNotEmpty(enchentment)) {
      final String thisFieldName = this.getfieldname(posnumber);
      session.setAttribute(servletname + thisFieldName, req.getParameter(thisFieldName));
    }
  }

  /**
   * The method <code>sqlRead</code> reads out the SQL result Data (SQL select).
   *
   * @param result ResultSet of the SQL read
   * @param session The Data of the current Session
   * @exception SQLException if a database access error occurs
   */
  public void sqlRead(final ResultSet result, final HttpSession session) throws SQLException {
    this.sqlRead(result, session, -1);
  }

  /**
   * The method <code>sqlRead</code> reads out the SQL result Data (SQL select).
   *
   * @param result ResultSet of the SQL read
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @exception SQLException if a database access error occurs
   */
  public void sqlRead(final ResultSet result, final HttpSession session, final int posnumber) throws SQLException {
    if (StringUtils.isNotEmpty(fieldname) && StringUtils.isNotEmpty(dbfieldname)) {
      session.setAttribute(servletname + this.getfieldname(posnumber), result.getString(dbfieldname));
    }
  }

  /**
   * The method <code>getContents</code> returns the contents of the field.
   *
   * @param session The Data of the current Session
   * @return contents of the field
   */
  public String getContents(final HttpSession session) {
    return this.getContents(session, -1);
  }

  /**
   * The method <code>getContents</code> returns the contents of the field.
   *
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @return contents of the field
   */
  public String getContents(final HttpSession session, final int posnumber) {
    if (StringUtils.isEmpty(fieldname)) {
      return null;
    } else {
      return (String) session.getAttribute(servletname + this.getfieldname(posnumber));
    }
  }

  /**
   * The method <code>getEnchentment</code> returns the enchentment of the field.
   *
   * @return enchentment of the field
   */
  public String getEnchentment() {
    return enchentment;
  }

  /**
   * The method <code>isComplusion</code> returns the complusion of the field.
   *
   * @return true if the field must be filled
   */
  public boolean isComplusion() {
    return complusion;
  }

  /**
   * The method <code>setNewComplusion</code> changes the complusion.
   *
   * @param newComplusion the new complusion entry
   */
  public void setNewComplusion(final boolean newComplusion) {
    complusion = newComplusion;
  }

  /**
   * The method <code>getStringContents</code> returns the contents of the field as html.
   *
   * @param session The Data of the current Session
   * @return contents of the field as html
   */
  public String htmlStringContents(final HttpSession session) {
    return this.htmlStringContents(session, -1);
  }

  /**
   * The method <code>getStringContents</code> returns the contents of the field as html.
   *
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @return contents of the field as html
   */
  public String htmlStringContents(final HttpSession session, final int posnumber) {
    return de.knightsoft.common.StringToHtml.convert(this.getContents(session, posnumber), false, true, true);
  }

  /**
   * The method <code>getdbfieldname</code> returns the database fieldname of the field.
   *
   * @return database fieldname
   */
  public String getdbfieldname() {
    return dbfieldname;
  }

  /**
   * The method <code>getfieldname</code> returns the html fieldname of the field.
   *
   * @return html fieldname
   */
  public String getfieldname() {
    return this.getfieldname(-1);
  }

  /**
   * The method <code>getfieldname</code> returns the html fieldname of the field.
   *
   * @param posnumber number of the position or -1 for headerfields
   * @return html fieldname
   */
  public String getfieldname(final int posnumber) {
    if (posnumber >= 0 && StringUtils.isNotEmpty(fieldname)) {
      return fieldname + Integer.toString(posnumber);
    } else {
      return fieldname;
    }
  }

  /**
   * The method <code>htmlInputField</code> returns the input field for html formulars.
   *
   * @param tabpos positionnummber of the field
   * @param session The Data of the current Session
   * @return html inputfield
   */
  public String htmlInputField(final int tabpos, final HttpSession session) {
    return this.htmlInputField(tabpos, session, -1, "save");
  }

  /**
   * The method <code>htmlInputField</code> returns the input field for html formulars.
   *
   * @param tabpos positionnummber of the field
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @param type action type to handle when this field is changed at least
   * @return html inputfield
   */
  public String htmlInputField(final int tabpos, final HttpSession session, final int posnumber, final String type) {
    if (StringUtils.isEmpty(enchentment)) {
      return StringUtils.EMPTY;
    } else if (StringUtils.isEmpty(fieldname)) {
      return "&nbsp;";
    } else {
      final String htmlfieldname = de.knightsoft.common.StringToHtml.convert(this.getfieldname(posnumber), false, true, true);
      final String classstring = this.checkField(session, posnumber) ? "default" : "faulty";
      final String regExInputCheck = StringUtils.isEmpty(regEx) ? StringUtils.EMPTY : " pattern=\"" + regEx + "\"";
      final String required = complusion ? " required=\"required\"" : StringUtils.EMPTY;
      final String returnstring = "<input type=\"" + inputType + "\" class=\"" + classstring + "\" id=\"" + htmlfieldname
          + "\" name=\"" + htmlfieldname + "\" size=\"" + Integer.toString(fieldlength) + "\" maxlength=\""
          + Integer.toString(length) + "\" value=\"" + this.htmlStringContents(session, posnumber) + "\" tabindex=\""
          + Integer.toString(tabpos) + "\"" + required + regExInputCheck + " onBlur=\"return checkRegEx('" + htmlfieldname
          + "', /" + StringUtils.defaultString(regEx, ".*") + "/, " + complusion + ", '" + type + "', false);\" "
          + StringUtils.defaultString(additionalParam) + ">";
      return returnstring;
    }
  }

  /**
   * The method <code>htmlInputTableSegment</code> returns the input field including enchantement and table structure for html
   * formulars.
   *
   * @param tabpos positionnummber of the field
   * @param session The Data of the current Session
   * @return html inputfield
   */
  public String htmlInputTableSegment(final int tabpos, final HttpSession session) {
    String returnstring;
    if (StringUtils.isEmpty(enchentment)) {
      returnstring = StringUtils.EMPTY;
    } else if (StringUtils.isEmpty(fieldname)) {
      returnstring = "                    <td colspan=\"2\">&nbsp;</td>\n";
    } else {
      if (complusion) {
        returnstring = "                    <td><label class=\"complusion\" for=\""
            + de.knightsoft.common.StringToHtml.convert(fieldname, false, true, true) + "\">"
            + de.knightsoft.common.StringToHtml.convert(enchentment) + "&nbsp;&sup1;</label></td>\n"
            + "                    <td>" + this.htmlInputField(tabpos, session) + "</td>\n";
      } else {
        returnstring = "                    <td><label class=\"free\" for=\""
            + de.knightsoft.common.StringToHtml.convert(fieldname, false, true, true) + "\">"
            + de.knightsoft.common.StringToHtml.convert(enchentment) + "&nbsp;&sup2;</label></td>\n"
            + "                    <td>" + this.htmlInputField(tabpos, session) + "</td>\n";
      }
    }
    return returnstring;
  }

  /**
   * The method <code>htmlInputTableSegment</code> returns the input field including enchantement and table structure for html
   * formulars.
   *
   * @param tabpos positionnummber of the field
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @param type action type to handle when this field is changed at least
   * @return html inputfield
   */
  public String htmlInputTableSegment(final int tabpos, final HttpSession session, final int posnumber, final String type) {
    if (StringUtils.isEmpty(enchentment)) {
      return StringUtils.EMPTY;
    } else {
      return "                    <td class=\"borderleft\">" + this.htmlInputField(tabpos, session, posnumber, type)
          + "</td>\n";
    }
  }

  /**
   * The method <code>numTDs</code> returns the number td tags the htmlInputTableSegment method creates.
   *
   * @return number TD tags
   */
  public int numTDs() {
    return 2;
  }

  /**
   * The method <code>javascriptCheck</code> returns javascript code to check the input of the field.
   *
   * @return html inputfield
   */
  public String javascriptCheck() {
    return this.javascriptCheck(-1, "save");
  }

  /**
   * The method <code>javascriptCheck</code> returns javascript code to check the input of the field.
   *
   * @param posnumber number of the position or -1 for headerfields
   * @param type action type to handle when this field is changed at least
   * @return html inputfield
   */
  public String javascriptCheck(final int posnumber, final String type) {
    if (StringUtils.isEmpty(fieldname) || StringUtils.isEmpty(enchentment)) {
      return StringUtils.EMPTY;
    } else {
      final String htmlfieldname = de.knightsoft.common.StringToHtml.convert(this.getfieldname(posnumber), false, true, true);
      final String returnstring = "                        if (!checkRegEx('" + htmlfieldname + "', /" + regEx + "/, "
          + complusion + ", '" + type + "', true))\n" //
          + "                            return false;\n";
      return returnstring;
    }
  }

  /**
   * The method <code>htmlViewField</code> returns the html view field.
   *
   * @param session The Data of the current Session
   * @return html viewfield
   */
  public String htmlViewField(final HttpSession session) {
    return this.htmlViewField(session, -1);
  }

  /**
   * The method <code>htmlViewField</code> returns the html view field.
   *
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @return html viewfield
   */
  public String htmlViewField(final HttpSession session, final int posnumber) {
    if (StringUtils.isEmpty(enchentment)) {
      return StringUtils.EMPTY;
    } else if (StringUtils.isEmpty(fieldname)) {
      return "&nbsp;";
    } else {
      final String htmlfieldname = de.knightsoft.common.StringToHtml.convert(this.getfieldname(posnumber), false, true, true);
      final String htmlcontents = this.htmlStringContents(session, posnumber);
      return "<input type=\"hidden\" name=\"" + htmlfieldname + "\" value=\"" + htmlcontents + "\">" + htmlcontents;
    }
  }

  /**
   * The method <code>htmlViewTableSegment</code> returns the html view field including enchantement and table structure.
   *
   * @param session The Data of the current Session
   * @return html viewfield
   */
  public String htmlViewTableSegment(final HttpSession session) {
    if (StringUtils.isEmpty(enchentment)) {
      return StringUtils.EMPTY;
    } else if (fieldname == null) {
      return "                    <td colspan=\"2\">&nbsp;</td>\n";
    } else {
      return "                    <td>" + de.knightsoft.common.StringToHtml.convert(enchentment) + "</td>\n"
          + "                    <td>" + this.htmlViewField(session) + "</td>\n";
    }
  }

  /**
   * The method <code>htmlViewTableSegment</code> returns the html view field including enchantement and table structure.
   *
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @return html viewfield
   */
  public String htmlViewTableSegment(final HttpSession session, final int posnumber) {
    if (StringUtils.isEmpty(enchentment)) {
      return StringUtils.EMPTY;
    } else {
      return "                    <td class=\"borderleft\">" + this.htmlViewField(session, posnumber) + "</td>\n";
    }
  }

  /**
   * The method <code>checkField</code> checks the contents of the field.
   *
   * @param session The Data of the current Session
   * @return true if the contents is correct
   */
  public boolean checkField(final HttpSession session) {
    return this.checkField(session, -1);
  }

  /**
   * The method <code>checkField</code> checks the contents of the field.
   *
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @return true if the contents is correct
   */
  public boolean checkField(final HttpSession session, final int posnumber) {
    final String contents = this.getContents(session, posnumber);
    boolean returnwert = false;

    if (StringUtils.isEmpty(fieldname) || StringUtils.isEmpty(enchentment)) {
      returnwert = true;
    } else {
      if (complusion && StringUtils.isEmpty(contents)) {
        // nothing to do
      } else {
        if (StringUtils.isEmpty(contents) || regExPat == null) {
          returnwert = true;
        } else {
          final Matcher m = regExPat.matcher(contents);
          returnwert = m.matches();
        }
      }
    }
    return returnwert;
  }

  /**
   * The method <code>sqlUpdateName</code> creates a sql string for updating the database.
   *
   * @param sqlString String to add the update field
   * @return new SQLString
   */
  @Deprecated
  public String sqlUpdateName(final String sqlString) {
    return this.sqlUpdateName(new StringBuilder(sqlString)).toString();
  }

  /**
   * The method <code>sqlUpdateName</code> creates a sql string for updating the database.
   *
   * @param sqlString String to add the update field
   * @return new SQLString
   */
  public StringBuilder sqlUpdateName(final StringBuilder sqlString) {
    if (StringUtils.isNotEmpty(fieldname) && StringUtils.isNotEmpty(dbfieldname)) {
      if (sqlString.length() > 0) {
        sqlString.append(", ");
      }
      sqlString.append(dbfieldname).append(" = ? ");
    }
    return sqlString;
  }

  /**
   * The method <code>sqlInsertNames</code> creates a sql string for inserting into the database (fieldname).
   *
   * @param psqlStringNames String to add the fieldname
   * @return new SQLStringNames
   */
  @Deprecated
  public String sqlInsertNames(final String psqlStringNames) {
    return this.sqlInsertNames(new StringBuilder(StringUtils.defaultString(psqlStringNames))).toString();
  }

  /**
   * The method <code>sqlInsertNames</code> creates a sql string for inserting into the database (fieldname).
   *
   * @param psqlStringNames String to add the fieldname
   * @return new SQLStringNames
   */
  public StringBuilder sqlInsertNames(final StringBuilder psqlStringNames) {
    if (StringUtils.isNotEmpty(fieldname) && StringUtils.isNotEmpty(dbfieldname)) {
      if (psqlStringNames.length() > 0) {
        psqlStringNames.append(", ");
      }
      psqlStringNames.append(dbfieldname);
    }
    return psqlStringNames;
  }

  /**
   * The method <code>sqlPrepareInsertItems</code> creates a sql string for insert prepare statement.
   *
   * @param psqlStringItems String to add the contents
   * @return new SQLStringItems
   */
  @Deprecated
  public String sqlPrepareInsertItems(final String psqlStringItems) {
    return this.sqlPrepareInsertItems(new StringBuilder(StringUtils.defaultString(psqlStringItems))).toString();
  }

  /**
   * The method <code>sqlPrepareInsertItems</code> creates a sql string for insert prepare statement.
   *
   * @param psqlStringItems String to add the contents
   * @return new SQLStringItems
   */
  public StringBuilder sqlPrepareInsertItems(final StringBuilder psqlStringItems) {
    if (StringUtils.isNotEmpty(fieldname) && StringUtils.isNotEmpty(dbfieldname)) {
      if (psqlStringItems.length() > 0) {
        psqlStringItems.append(", ");
      }
      psqlStringItems.append('?');
    }
    return psqlStringItems;
  }

  /**
   * The method <code>sqlPrepareItems</code> adds the entry to the fields of a prepared statement.
   *
   * @param myPreparedStatement the prepared statement
   * @param thisDatabase the database connection
   * @param entryNumber number of the field
   * @param session The Data of the current Session
   * @return new EntryNumber
   * @exception SQLException
   */
  public int sqlPrepareItems(final PreparedStatement myPreparedStatement, final Connection thisDatabase, final int entryNumber,
      final HttpSession session) throws SQLException {
    return this.sqlPrepareItems(myPreparedStatement, thisDatabase, entryNumber, session, -1);
  }

  /**
   * The method <code>sqlPrepareItems</code> adds the entry to the fields of a prepared statement.
   *
   * @param myPreparedStatement the prepared statement
   * @param thisDatabase the database connection
   * @param pentryNumber number of the field
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @return new EntryNumber
   * @exception SQLException
   */
  public int sqlPrepareItems(final PreparedStatement myPreparedStatement, final Connection thisDatabase, final int pentryNumber,
      final HttpSession session, final int posnumber) throws SQLException {
    int entryNumber = pentryNumber;
    if (StringUtils.isNotEmpty(fieldname) && StringUtils.isNotEmpty(dbfieldname)) {
      String contents = this.getContents(session, posnumber);

      if (StringUtils.isEmpty(contents) && !complusion) {
        contents = null;
      }
      myPreparedStatement.setString(entryNumber, contents);

      entryNumber++;
    }
    return entryNumber;
  }
}
