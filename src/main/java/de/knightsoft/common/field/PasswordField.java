/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.common.field;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * The <code>PasswordField</code> class is a PasswordField input field with check double input to be sure no typing mistake
 * changes to. wrong entry
 *
 * @author Manfred Tremmel
 * @version 1.2.0 22.07.2008
 */
public class PasswordField extends de.knightsoft.common.field.AbstractBaseField {

  private static final String REPEAT = "_repeate";
  protected String passworddummy;

  /**
   * Constructor.
   *
   * @param servletname Name of the servlet
   * @param enchentment Label, displayed in the HTML-site
   * @param fieldname Fieldname, used in the HTML-site
   * @param dbfieldname Fieldname, used in the database
   * @param length Not used in this field-type
   * @param fieldlength Not used in this field-type
   * @param complusion Must be filled or not
   */
  public PasswordField(final String servletname, final String enchentment, final String fieldname, final String dbfieldname,
      final int length, final int fieldlength, final boolean complusion) {
    super(servletname, enchentment, fieldname, dbfieldname, length, fieldlength, complusion);
    passworddummy = "password(?)";
  }

  /**
   * Setup a new DataBase connection.
   *
   * @param pdataBase Database connection
   */
  @Override
  public void setNewDataBase(final Connection pdataBase) throws SQLException {
    myDataBase = pdataBase;
    if (pdataBase != null) {
      try {
        final de.knightsoft.common.DataBaseDepending myDataBaseDepending =
            new de.knightsoft.common.DataBaseDepending(pdataBase.getMetaData().getDatabaseProductName());

        passworddummy = myDataBaseDepending.getSqlPassword("?");
      } catch (final Exception e) { // NOPMD
        // do nothing, leave password dummy unchanged
      }
    }
  }

  /**
   * Initial Fields.
   *
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   */
  @Override
  public void initField(final HttpSession session, final int posnumber) {
    if (fieldname != null) {
      super.initField(session, posnumber);
      session.setAttribute(servletname + this.getfieldname(posnumber) + PasswordField.REPEAT, StringUtils.EMPTY);
    }
  }

  /**
   * The method <code>htmlRead</code> reads out the HTML request Data (input of the html form).
   *
   * @param req Request data from the servlet
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   */
  @Override
  public void htmlRead(final HttpServletRequest req, final HttpSession session, //
      final int posnumber) {
    if (fieldname != null && enchentment != null) {
      final String repeate_fieldname = this.getfieldname(posnumber) + PasswordField.REPEAT;
      super.htmlRead(req, session, posnumber);
      session.setAttribute(servletname + repeate_fieldname, req.getParameter(repeate_fieldname));
    }
  }

  /**
   * The method <code>sqlRead</code> reads out the SQL result Data (SQL select).
   *
   * @param result ResultSet of the SQL read
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @exception SQLException if a database access error occurs
   */
  @Override
  public void sqlRead(final ResultSet result, final HttpSession session, final int posnumber) throws java.sql.SQLException {
    this.initField(session, posnumber);
  }

  /**
   * The method <code>htmlInputField</code> returns the input field for html formulars.
   *
   * @param tabpos positionnummber of the field
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @param type action type to handle when this field is changed at least
   * @return html inputfield
   */
  @Override
  public String htmlInputField(final int tabpos, final HttpSession session, final int posnumber, final String type) {
    String returnstring = null;
    if (enchentment == null) {
      returnstring = StringUtils.EMPTY;
    } else if (fieldname == null) {
      returnstring = "&nbsp;";
    } else {
      String htmlfieldname = de.knightsoft.common.StringToHtml.convert(fieldname, false, true, true);
      final String classstring = this.checkField(session, posnumber) ? "default" : "faulty";
      if (posnumber >= 0) {
        htmlfieldname += Integer.toString(posnumber); // NOPMD
      }
      returnstring = "<input type=\"password\" class=\"" + classstring + "\" id=\"" + htmlfieldname + "\" name=\""
          + htmlfieldname + "\" size=\"" + Integer.toString(fieldlength) + "\" maxlength=\"" + Integer.toString(length)
          + "\" value=\"" + this.htmlStringContents(session) + "\" tabindex=\"" + tabpos + "\" onBlur=\"return checkRegEx('"
          + htmlfieldname + "', /" + regEx + "/, " + complusion + ", '" + type + "', false);\">" + "<br>"
          + "<input type=\"password\" class=\"" + classstring + "\" id=\"" + htmlfieldname + PasswordField.REPEAT + "\" name=\""
          + htmlfieldname + PasswordField.REPEAT + "\" size=\"" + Integer.toString(fieldlength) + "\" maxlength=\""
          + Integer.toString(length) + "\" value=\""
          + de.knightsoft.common.StringToHtml
              .convert((String) session.getAttribute(servletname + htmlfieldname + PasswordField.REPEAT), false, true, true)
          + "\" tabindex=\"" + (tabpos + 1) + "\" onBlur=\"return checkRegEx('" + htmlfieldname + PasswordField.REPEAT + "', /"
          + regEx + "/, " + complusion + ", '" + type + "', false);\">";
    }
    return returnstring;
  }

  /**
   * The method <code>htmlInputTableSegment</code> returns the input field including enchantement and table structure for html
   * formulars.
   *
   * @param tabpos positionnummber of the field
   * @param session The Data of the current Session
   * @return html inputfield
   */
  @Override
  public String htmlInputTableSegment(final int tabpos, final HttpSession session) {
    String returnstring;
    if (enchentment == null) {
      returnstring = StringUtils.EMPTY;
    } else if (fieldname == null) {
      returnstring = "                    <td colspan=\"2\">&nbsp;</td>\n";
    } else {
      if (complusion) {
        returnstring = "                    <td><label class=\"complusion\" for=\""
            + de.knightsoft.common.StringToHtml.convert(fieldname, false, true, true) + "\">"
            + de.knightsoft.common.StringToHtml.convert(enchentment) + "&nbsp;&sup1;</label>\n"
            + "                        <br>\n" + "                        <label class=\"complusion\" for=\""
            + de.knightsoft.common.StringToHtml.convert(fieldname, false, true, true) + PasswordField.REPEAT + "\">"
            + de.knightsoft.common.StringToHtml.convert(enchentment) + "&nbsp;best&auml;tigen&nbsp;&sup1;</label>\n"
            + "                    </td>\n" + "                    <td>" + this.htmlInputField(tabpos, session) + "</td>\n";
      } else {
        returnstring = "                    <td><label class=\"free\" for=\""
            + de.knightsoft.common.StringToHtml.convert(fieldname, false, true, true) + "\">"
            + de.knightsoft.common.StringToHtml.convert(enchentment) + "&nbsp;&sup2;</label>\n"
            + "                        <br>\n" + "                        <label class=\"free\" for=\""
            + de.knightsoft.common.StringToHtml.convert(fieldname, false, true, true) + PasswordField.REPEAT + "\">"
            + de.knightsoft.common.StringToHtml.convert(enchentment) + "&nbsp;best&auml;tigen&nbsp;&sup2;</label>\n"
            + "                    </td>\n" + "                    <td>" + this.htmlInputField(tabpos, session) + "</td>\n";
      }
    }
    return returnstring;
  }

  /**
   * The method <code>javascriptCheck</code> returns javascript code to check the input of the field.
   *
   * @param posnumber number of the position or -1 for headerfields
   * @param type action type to handle when this field is changed at least
   * @return html inputfield
   */
  @Override
  public String javascriptCheck(final int posnumber, final String type) {
    final StringBuilder returnstring = new StringBuilder(768);
    if (fieldname != null && enchentment != null) {
      final String htmlfieldname = de.knightsoft.common.StringToHtml.convert(this.getfieldname(posnumber), false, true, true);
      returnstring.append(
          "                        if (window.document.getElementById('buttonpress')" + ".value.toLowerCase().substr(0,4) == '")
          .append(type).append("') {\n");

      if (complusion) {
        returnstring.append("                            if (document.forms[0]." + htmlfieldname + ".value.length == 0) {\n"
            + "                                alert(unescape(\"Bitte f%FCllen Sie das Feld " + enchentment
            + ", es ist ein Zwangsfeld!\"));\n" + "                                document.forms[0]." + htmlfieldname
            + ".focus();\n" + "                                return false;\n" + "                            }\n");
      }
      returnstring.append("                            if ((document.forms[0]." + htmlfieldname + ".value.length > 0 ||\n"
          + "                                 document.forms[0]." + htmlfieldname + PasswordField.REPEAT
          + ".value.length > 0) &&\n" + "                                (document.forms[0]." + htmlfieldname + ".value != \n"
          + "                                 document.forms[0]." + htmlfieldname + PasswordField.REPEAT + ".value)) {\n"
          + "                                alert(unescape(\"'" + enchentment + "' und '" + enchentment
          + " best%E4tigen' m%FCssen identisch gef%FCllt werden!\"));\n" + "                                document.forms[0]."
          + htmlfieldname + ".focus();\n" + "                                return false;\n"
          + "                            }\n" + "                        }\n");
    }
    return returnstring.toString();
  }

  /**
   * The method <code>htmlViewField</code> returns the html view field.
   *
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @return html viewfield
   */
  @Override
  public String htmlViewField(final HttpSession session, final int posnumber) {
    if (enchentment == null) {
      return StringUtils.EMPTY;
    } else if (fieldname == null) {
      return "&nbsp;";
    } else {
      final String htmlfieldname = de.knightsoft.common.StringToHtml.convert(this.getfieldname(posnumber), false, true, true);
      final String htmlcontents = this.htmlStringContents(session, posnumber);
      return "<input type=\"hidden\" name=\"" + htmlfieldname + "\" value=\"" + htmlcontents + "\">" + htmlcontents
          + "<input type=\"hidden\" name=\"" + htmlfieldname + PasswordField.REPEAT + "\" value=\""
          + de.knightsoft.common.StringToHtml.convert(
              (String) session.getAttribute(servletname + htmlfieldname + PasswordField.REPEAT), false, true, true)
          + "\">";
    }
  }

  /**
   * The method <code>checkField</code> checks the contents of the field.
   *
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @return true if the contents is correct
   */
  @Override
  public boolean checkField(final HttpSession session, final int posnumber) {
    final String contents = this.getContents(session, posnumber);
    final String contents_repeate =
        (String) session.getAttribute(servletname + this.getfieldname(posnumber) + PasswordField.REPEAT);
    boolean returnwert = false;

    if (fieldname == null) {
      returnwert = true;
    } else {
      if (contents == null && contents_repeate == null) {
        returnwert = !complusion;
      } else {
        if (contents == null || contents_repeate == null || !contents.equals(contents_repeate)) {
          // nothing to do
        } else {
          returnwert = true;
        }
      }
    }
    return returnwert;
  }

  /**
   * The method <code>sqlUpdateName</code> creates a sql string for updating the database.
   *
   * @param sqlString String to add the update field
   * @return new SQLString
   */
  @Override
  public StringBuilder sqlUpdateName(final StringBuilder sqlString) {
    if (fieldname != null && dbfieldname != null) {
      if (sqlString.length() > 0) {
        sqlString.append(", ");
      }
      sqlString.append(dbfieldname).append(" = ").append(passworddummy).append(' ');
    }
    return sqlString;
  }

  /**
   * The method <code>sqlPrepareInsertItems</code> creates a sql string for insert prepare statement.
   *
   * @param sqlStringItems String to add the contents
   * @return new SQLStringItems
   */
  @Override
  public StringBuilder sqlPrepareInsertItems(final StringBuilder sqlStringItems) {
    if (fieldname != null && dbfieldname != null) {
      if (sqlStringItems.length() > 0) {
        sqlStringItems.append(", ");
      }
      sqlStringItems.append(passworddummy);
    }
    return sqlStringItems;
  }
}
