/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.common.field;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * The <code>PhoneCompress</code> class is a DummyField class to create a compressed, numeric only phone-number field.
 *
 * @author Manfred Tremmel
 * @version 1.2.0, 22.07.2008
 */
public class PhoneCompress extends de.knightsoft.common.field.AbstractBaseField {

  protected de.knightsoft.common.field.AbstractBaseField areaCode;
  protected de.knightsoft.common.field.AbstractBaseField callNumber;

  /**
   * Constructor.
   *
   * @param servletname Name of the servlet
   * @param enchentment Label, displayed in the HTML-site
   * @param fieldname Fieldname, used in the HTML-site
   * @param dbfieldname Fieldname, used in the database
   * @param length Not used in this field-type
   * @param fieldlength Not used in this field-type
   * @param complusion Must be filled or not
   */
  public PhoneCompress(final String servletname, final String enchentment, final String fieldname, final String dbfieldname,
      final int length, final int fieldlength, final boolean complusion) {
    super(servletname, enchentment, fieldname, dbfieldname, length, fieldlength, complusion);
    areaCode = null;
    callNumber = null;
  }

  /**
   * Constructor.
   *
   * @param servletname Name of the servlet
   * @param enchentment Label, displayed in the HTML-site
   * @param fieldname Fieldname, used in the HTML-site
   * @param dbfieldname Fieldname, used in the database
   * @param length Not used in this field-type
   * @param fieldlength Not used in this field-type
   * @param complusion Must be filled or not
   * @param areaCode reference to the field contains the "areaCode"
   * @param callNumber reference to the field contains the "callNumber"
   */
  public PhoneCompress(final String servletname, final String enchentment, final String fieldname, final String dbfieldname,
      final int length, final int fieldlength, final boolean complusion,
      final de.knightsoft.common.field.AbstractBaseField areaCode,
      final de.knightsoft.common.field.AbstractBaseField callNumber) {
    super(servletname, enchentment, fieldname, dbfieldname, length, fieldlength, complusion);
    this.areaCode = areaCode;
    this.callNumber = callNumber;
  }

  /**
   * set a reference to the area code of the phone-number
   *
   * @param areaCode area code input field
   * @see de.knightsoft.common.field.AbstractBaseField
   */
  public void setAreaCodeField(final de.knightsoft.common.field.AbstractBaseField areaCode) {
    this.areaCode = areaCode;
  }

  /**
   * set a reference to the call number of the phone-number
   *
   * @param callNumber call number input field
   * @see de.knightsoft.common.field.AbstractBaseField
   */
  public void setRufnummerField(final de.knightsoft.common.field.AbstractBaseField callNumber) {
    this.callNumber = callNumber;
  }

  /**
   * Initial Fields.
   *
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   */
  @Override
  public void initField(final HttpSession session, final int posnumber) {
    if (fieldname != null) {
      session.setAttribute(servletname + this.getfieldname(posnumber), null);
    }
  }

  /**
   * The method <code>htmlRead</code> reads out the HTML request Data (input of the html form).
   *
   * @param req Request data from the servlet
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   */
  @Override
  public void htmlRead(final HttpServletRequest req, final HttpSession session, //
      final int posnumber) {
    if (fieldname != null) {
      session.setAttribute(servletname + this.getfieldname(posnumber), null);
    }
  }

  /**
   * The method <code>sqlRead</code> reads out the SQL result Data (SQL select).
   *
   * @param result ResultSet of the SQL read
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @exception SQLException if a database access error occurs
   */
  @Override
  public void sqlRead(final ResultSet result, final HttpSession session, final int posnumber) throws SQLException {
    if (fieldname != null) {
      session.setAttribute(servletname + this.getfieldname(posnumber), null);
    }
  }

  /**
   * The method <code>htmlInputField</code> returns the input field for html formulars.
   *
   * @param tabpos positionnummber of the field
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @param type action type to handle when this field is changed at least
   * @return html inputfield
   */
  @Override
  public String htmlInputField(final int tabpos, final HttpSession session, final int posnumber, final String type) {
    return this.htmlStringContents(session, posnumber);
  }

  /**
   * The method <code>htmlInputTableSegment</code> returns the input field including enchantement and table structure for html
   * formulars.
   *
   * @param tabpos positionnummber of the field
   * @param session The Data of the current Session
   * @return html inputfield
   */
  @Override
  public String htmlInputTableSegment(final int tabpos, final HttpSession session) {
    if (enchentment == null) {
      return StringUtils.EMPTY;
    } else {
      return "                    <td colspan=\"2\">&nbsp;</td>\n";
    }
  }

  /**
   * The method <code>htmlInputTableSegment</code> returns the input field including enchantement and table structure for html
   * formulars.
   *
   * @param tabpos positionnummber of the field
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @param type action type to handle when this field is changed at least
   * @return html inputfield
   */
  @Override
  public String htmlInputTableSegment(final int tabpos, final HttpSession session, final int posnumber, final String type) {
    if (enchentment == null) {
      return StringUtils.EMPTY;
    } else {
      return "                    <td>&nbsp;</td>\n";
    }
  }

  /**
   * The method <code>javascriptCheck</code> returns javascript code to check the input of the field.
   *
   * @param posnumber number of the position or -1 for headerfields
   * @param type action type to handle when this field is changed at least
   * @return html inputfield
   */
  @Override
  public String javascriptCheck(final int posnumber, final String type) {
    return StringUtils.EMPTY;
  }

  /**
   * The method <code>htmlViewField</code> returns the html view field.
   *
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @return html viewfield
   */
  @Override
  public String htmlViewField(final HttpSession session, final int posnumber) {
    return this.htmlInputField(0, session, posnumber, null);
  }

  /**
   * The method <code>htmlViewTableSegment</code> returns the html view field including enchantement and table structure.
   *
   * @param session The Data of the current Session
   * @return html viewfield
   */
  @Override
  public String htmlViewTableSegment(final HttpSession session) {
    return this.htmlInputTableSegment(0, session);
  }

  /**
   * The method <code>htmlViewTableSegment</code> returns the html view field including enchantement and table structure.
   *
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @return html viewfield
   */
  @Override
  public String htmlViewTableSegment(final HttpSession session, final int posnumber) {
    return this.htmlInputTableSegment(0, session, posnumber, null);
  }

  /**
   * The method <code>checkField</code> checks the contents of the field.
   *
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @return true if the contents is correct
   */
  @Override
  public boolean checkField(final HttpSession session, final int posnumber) {
    return true;
  }

  /**
   * The method <code>getContents</code> returns the contents of the field.
   *
   * @param session The Data of the current Session
   * @param posnumber number of the position or -1 for headerfields
   * @return contents of the field
   */
  @Override
  public String getContents(final HttpSession session, final int posnumber) {
    String reagionCodeStr = areaCode.getContents(session, posnumber);
    final String callNumberStr = callNumber.getContents(session, posnumber);
    String contents = null;

    if (StringUtils.isNotEmpty(reagionCodeStr) && StringUtils.isNotEmpty(callNumberStr)) {
      if (reagionCodeStr.startsWith("0049")) {
        reagionCodeStr = "0" + reagionCodeStr.substring(4);
      }
      contents =
          de.knightsoft.common.StringToSql.convertNumber(reagionCodeStr + callNumberStr).replaceAll("\\.", StringUtils.EMPTY);
    }
    return contents;
  }
}
