/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.common;

import java.sql.SQLException;

/**
 * The <code>DataBaseDepending</code> class contains database specific SQL strings. It supports MySQL and MSSQL at the moment.
 *
 * @author Manfred Tremmel
 * @version 1.0.2, 05.06.2009
 */
public class DataBaseDepending {
  protected int dbnumber;
  protected int dbDriverNumber;
  protected static final String[][] JDBC_CLASS = {{"MySQL", Constants.JDBC_CLASS_MYSQL, Constants.JDBC_CLASS_MYSQL_OLD,
      Constants.JDBC_CLASS_MYSQL_OLD2, Constants.JDBC_CLASS_MARIADB}, {"MSSQL", Constants.JDBC_CLASS_MSSQL}};

  protected static final String[] SQL_TIME_NOW = {"NOW()", "GETDATE()"};

  protected static final String[] SQL_TIME_OUTDATE = {"NOW() - INTERVAL 1 SECOND", "DATEADD(second, -1, GETDATE())"};

  /**
   * Constructor.
   *
   * @param pjdbcClassToUse JDBC driver class to use
   * @exception SQLException if the JDBCClassToUse is not supported
   */
  public DataBaseDepending(final String pjdbcClassToUse) throws SQLException {
    dbnumber = -1;
    final String jdbcClassToUse;
    if (Constants.JDBC_CLASS_MYSQL_OLD2.equalsIgnoreCase(pjdbcClassToUse)) {
      jdbcClassToUse = Constants.JDBC_CLASS_MYSQL;
    } else if ("MariaDB".equalsIgnoreCase(pjdbcClassToUse)) {
      jdbcClassToUse = Constants.JDBC_CLASS_MARIADB;
    } else {
      jdbcClassToUse = pjdbcClassToUse;
    }
    for (int i = 0; i < DataBaseDepending.JDBC_CLASS.length && dbnumber == -1; i++) {
      for (int j = 0; j < DataBaseDepending.JDBC_CLASS[i].length && dbnumber == -1; j++) {
        if (jdbcClassToUse.equals(DataBaseDepending.JDBC_CLASS[i][j])) {
          dbnumber = i;
          dbDriverNumber = j;
        }
      }
    }
    if (dbnumber == -1) {
      throw new SQLException("This JDBCClass is not Supported");
    }
  }

  /**
   * The <code>getJDBCClass</code> class returns the name of the JDBC Class.
   *
   * @return name of the JDBC Class
   */
  public String getJdbcClass() {
    return DataBaseDepending.JDBC_CLASS[dbnumber][1];
  }

  /**
   * The <code>getSQLTimeNow</code> class returns the SQL function used to get current DateField/TimeField for the currently
   * used database.
   *
   * @return SQL function
   */
  public String getSqlTimeNow() {
    return DataBaseDepending.SQL_TIME_NOW[dbnumber];
  }

  /**
   * The <code>getSQLTimeOutdate</code> class returns SQL function to get current date/time - one second to outdate entries.
   *
   * @return SQL function
   */
  public String getSqlTimeOutdate() {
    return DataBaseDepending.SQL_TIME_OUTDATE[dbnumber];
  }

  /**
   * The <code>getSQLDiffFromNow</code> class returns SQL function to get the difference from a given DateField/TimeField to now
   * in days.
   *
   * @param compareField field to compare with current DateField
   * @return SQL function
   */
  public String getSqlDiffFromNow(final String compareField) {
    String returnString = null;
    switch (dbnumber) { // NOPMD
      case 0:
        returnString = "TO_DAYS(NOW()) - TO_DAYS(" + compareField + ")";
        break;
      default:
        returnString = "DATEDIFF (day, " + compareField + ", GETDATE())";
        break;
    }
    return returnString;
  }

  /**
   * The <code>getSqlPassword</code> class returns SQL function to encrypt passwords if database can do, otherwise fieldname is
   * unchanged.
   *
   * @param encryptField field to encrypt
   * @return SQL function
   */
  public String getSqlPassword(final String encryptField) {
    String returnString = null;
    switch (dbnumber) { // NOPMD
      case 0:
        returnString = "PASSWORD(" + encryptField + ")";
        break;
      default:
        returnString = encryptField;
        break;
    }
    return returnString;
  }

  /**
   * The <code>getSqlOldPassword</code> class returns SQL function to encrypt passwords in old form if database can do,
   * otherwise fieldname is unchanged.
   *
   * @param encryptField field to encrypt
   * @return SQL function
   */
  public String getSqlOldPassword(final String encryptField) {
    String returnString = null;
    switch (dbnumber) { // NOPMD
      case 0:
        if (dbDriverNumber > 0) {
          returnString = "OLD_PASSWORD(" + encryptField + ")";
        } else {
          returnString = "PASSWORD(" + encryptField + ")";
        }
        break;
      default:
        returnString = encryptField;
        break;
    }
    return returnString;
  }

  /**
   * The <code>concatStrings</code> class returns SQL function to concatenate strings.
   *
   * @param pstringTab Table of Strings to concatenate
   * @return SQL function
   */
  public String concatStrings(final String[] pstringTab) {
    final StringBuilder Returnstring = new StringBuilder();
    int pos;
    switch (dbnumber) {
      case 0:
        Returnstring.append("CONCAT(");
        for (pos = 0; pos < pstringTab.length; pos++) {
          if (pos > 0) {
            Returnstring.append(", ");
          }
          Returnstring.append(pstringTab[pos]);
        }
        Returnstring.append(')');
        break;
      case 1:
        Returnstring.append('(');
        for (pos = 0; pos < pstringTab.length; pos++) {
          if (pos > 0) {
            Returnstring.append(" + ");
          }
          Returnstring.append(pstringTab[pos]);
        }
        Returnstring.append(')');
        break;
      default:
        Returnstring.append('(');
        for (pos = 0; pos < pstringTab.length; pos++) {
          if (pos > 0) {
            Returnstring.append(" || ");
          }
          Returnstring.append(pstringTab[pos]);
        }
        Returnstring.append(')');
        break;
    }
    return Returnstring.toString();
  }

  /**
   * The <code>getSQLBoolean</code> class returns SQL entry for boolean fields.
   *
   * @param booleanField field to transform
   * @return SQL field
   */
  public String getSqlBoolean(final boolean booleanField) {
    String returnString = null;
    switch (dbnumber) { // NOPMD
      case 0:
        returnString = booleanField ? "'1'" : "'0'";
        break;
      default:
        returnString = booleanField ? "1" : "0";
        break;
    }
    return returnString;
  }
}
