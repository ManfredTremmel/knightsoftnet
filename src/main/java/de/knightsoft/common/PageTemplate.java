/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.common;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * The <code>PageTemplate</code> is the Template for the ErsaNG Websites, all generated HTML-Pages are based on this template
 *
 * @author Manfred Tremmel
 * @version 1.0.3, 10.02.2009
 */

public class PageTemplate {

  /**
   * The <code>navHTML</code> Method builds the HTML-Navigaten-Sidebar
   *
   * @param myNavStrukt The Navigation Structure
   * @param mySession The current Session
   * @param res HttpServletResponse from the Servlet
   * @param browserdetect structure to detect the browser
   * @return the html string with the Navigation bar
   * @see de.knightsoft.common.NavTabStrukt
   */
  public static final String navHtml(final de.knightsoft.common.NavTabStrukt[] myNavStrukt, final HttpSession mySession,
      final javax.servlet.http.HttpServletResponse res, final de.knightsoft.common.Browserdetection browserdetect) {
    final StringBuilder sb = new StringBuilder(256);
    String dummy = StringUtils.EMPTY;
    int pos = 0;

    if (myNavStrukt == null) {
      sb.append("&nbsp;");
    } else {
      for (pos = 0; pos < myNavStrukt.length; pos++) {
        if (myNavStrukt[pos] != null) {
          switch (myNavStrukt[pos].getType()) {
            case 0:
              sb.append("            <ul id=\"menu\">\n" + "                <li class=\"navtop\"><img src=\""
                  + myNavStrukt[pos].getPicture() + "\" srcset=\"" + myNavStrukt[pos].getPictureHiDpi() + " 2x\" "
                  + "width=\"16\" height=\"16\" alt=\"" + myNavStrukt[pos].getEntry() + "\">" + myNavStrukt[pos].getEntry()
                  + "\n" + "                    <ul>\n");
              break;
            case 1:
              if (myNavStrukt[pos].isIsopen()) {
                if (browserdetect.isIe && browserdetect.getmajor() <= 6) {
                  dummy = "overflow:auto;visibility:visible;";
                } else {
                  dummy = "overflow:hidden;visibility:visible;height:auto;";
                }
              } else {
                dummy = "overflow:hidden;visibility:hidden;height:1px;";
              }
              sb.append("                        <li class=\"navdiv\">"
                  + "<a class=\"nav\" href=\"javascript:hide_or_show('nav_id" + pos + "');\"><img src=\""
                  + myNavStrukt[pos].getPicture() + "\" srcset=\"" + myNavStrukt[pos].getPictureHiDpi() + " 2x\" "
                  + "width=\"16\" height=\"16\" alt=\"" + myNavStrukt[pos].getEntry() + "\">" + myNavStrukt[pos].getEntry()
                  + "</a>\n" + "                            <ul id=\"nav_id" + pos + "\" style=\"" + dummy + "\">\n");
              break;
            case 2:
              sb.append("                            </ul>\n" + "                        </li>\n");
              break;
            case 3:
              if (mySession == null || res == null) {
                dummy = myNavStrukt[pos].getLink();
              } else {
                dummy = res.encodeURL(myNavStrukt[pos].getLink());
              }
              sb.append("                                <li class=\"navdivpoint\"><a class=\"nav\" href=\"" + dummy
                  + "\"><img src=\"" + myNavStrukt[pos].getPicture() + "\" srcset=\"" + myNavStrukt[pos].getPictureHiDpi()
                  + " 2x\" " + "width=\"16\" height=\"16\" alt=\"" + myNavStrukt[pos].getEntry() + "\">"
                  + myNavStrukt[pos].getEntry() + "</a></li>\n");
              break;
            default:
              break;
          }
        }
      }
      sb.append("                    </ul>\n" + "                </li>\n" + "            </ul>\n");
    }

    return sb.toString();
  }

  /**
   * The <code>htmlPage</code> Method builds the complete HTML-Page
   *
   * @param req HttpServletRequest from the Servlet
   * @param prefix path prefix
   * @param servletName Name of the Servlet
   * @param pageTitle Title of the HTML-Page
   * @param additional Additional header stuff
   * @param nocache Is the page cachable (constant)
   * @param title Title displayed on top of the page
   * @param body The HTML Code that has to be shown
   * @param selectedField A form field to be preselected
   * @param myNavStrukt Navigation structure
   * @param copyright Copyright entry
   * @param copyrightMail Set Mailto when clicking on copyright
   * @param email email of the creator
   * @param service Name of the Service
   * @param serviceColor Color to display the Service text
   * @param pictureService picture displayed in the left upper corner
   * @param pictureSizeX picture size x
   * @param pictureSizeY picture size y
   * @param pictureDescription picture description
   * @return the complete html page
   * @see de.knightsoft.common.NavTabStrukt
   */
  public static final String htmlPage(final HttpServletRequest req, final String prefix, final String servletName,
      final String pageTitle, final String additional, final boolean nocache, final String title, final String body,
      final String selectedField, final de.knightsoft.common.NavTabStrukt[] myNavStrukt, final String copyright,
      final String copyrightMail, final String email, final String service, final String serviceColor,
      final String pictureService, final String pictureServiceHiDpi, final String pictureSizeX, final String pictureSizeY,
      final String pictureDescription

  ) {
    return PageTemplate.htmlPage(req, prefix, servletName, pageTitle, additional, nocache, title, body, selectedField,
        myNavStrukt, null, null, copyright, copyrightMail, email, service, serviceColor, pictureService, pictureServiceHiDpi,
        pictureSizeX, pictureSizeY, pictureDescription);
  }

  /**
   * The <code>HTMLPage</code> Method builds the complete HTML-Page
   *
   * @param req HttpServletRequest from the Servlet
   * @param prefix path prefix
   * @param servletName Name of the Servlet
   * @param pageTitle Title of the HTML-Page
   * @param additional Additional header stuff
   * @param nocache Is the page cachable (constant)
   * @param title Title displayed on top of the page
   * @param body The HTML Code that has to be shown
   * @param selectedField A form field to be preselected
   * @param myNavStrukt Navigation structure
   * @param mySession Data from the current session
   * @param res HttpServletResponse from the Servlet
   * @param copyright Copyright entry
   * @param copyrightMail Set Mailto when clicking on copyright
   * @param email email of the creator
   * @param service Name of the Service
   * @param serviceColor Color to display the Service text
   * @param pictureService picture displayed in the left upper corner
   * @param pictureServiceHiDpi same picture for HiDPI displays
   * @param pictureSizeX picture size x
   * @param pictureSizeY picture size y
   * @param pictureDescription picture description
   * @return the complete html page
   * @see de.knightsoft.common.NavTabStrukt
   */
  public static final String htmlPage(final HttpServletRequest req, final String prefix, final String servletName,
      final String pageTitle, final String additional, final boolean nocache, final String title, final String body,
      final String selectedField, final de.knightsoft.common.NavTabStrukt[] myNavStrukt, final HttpSession mySession,
      final HttpServletResponse res, final String copyright, final String copyrightMail, final String email,
      final String service, final String serviceColor, final String pictureService, final String pictureServiceHiDpi,
      final String pictureSizeX, final String pictureSizeY, final String pictureDescription) {
    final de.knightsoft.common.Browserdetection myBrowserdetection =
        new de.knightsoft.common.Browserdetection(req.getHeader("user-agent"));

    final Date today = new Date();

    final StringBuilder sb = new StringBuilder(4096);

    sb.append("<!DOCTYPE html>\n" //
        + "<html><!-- " + servletName + " -->\n" //
        + "    <head>\n" //
        + "        <meta http-equiv=\"Content-Type\" content=\"text/html; " + "charset=UTF-8\">\n"
        + "        <meta http-equiv=\"Content-Style-Type\" content=\"text/css\">\n"
        + "        <meta http-equiv=\"Content-Script-Type\" content=\"text/javascript\">\n"
        + "        <meta http-equiv=\"Content-Language\" content=\"de\">\n");
    if (nocache) {
      sb.append("        <meta http-equiv=\"expires\" content=\"0\">\n"
          + "        <meta http-equiv=\"pragma\" content=\"no-cache\">\n"
          + "        <meta name=\"robots\" content=\"noindex\">\n");
    }
    sb.append(
        "        <meta name=\"MSSmartTagsPreventParsing\" content=\"TRUE\">\n" + "        <meta name=\"description\" content=\""
            + de.knightsoft.common.StringToHtml.convert(pageTitle) + "\">\n" + "        <meta name=\"date\" content=\""
            + new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss z", Locale.GERMANY).format(today) + "\">\n" + "        <!-- "
            + copyright + " -->\n" + "        <title>" + de.knightsoft.common.StringToHtml.convert(pageTitle) + "</title>\n"
            + "        <link rev=\"made\" href=\"mailto:" + email + "\">\n"
            + "        <link rel=\"stylesheet\" type=\"text/css\" href=\"" + prefix + Constants.CSS_URL + "default.css\">\n"
            + "        <link rel=\"shortcut icon\" href=\"/favicon.ico\" type=\"image/x-icon\">\n"
            + "        <script type=\"text/javascript\">\n" + "            <!--\n" + "            if (top!=self)\n"
            + "                top.location=self.location;\n\n" + "            function hide_or_show(element_id) {\n"
            + "                var element = document.getElementById(element_id);\n" + "                var xmlHttp = null;\n\n"
            + "                try{\n" + "                    xmlHttp = new XMLHttpRequest();\n"
            + "                } catch (ms) {\n" + "                    try{\n"
            + "                        xmlHttp = new ActiveXObject(\"Msxml2.XMLHTTP\");\n"
            + "                    } catch (nonms) {\n" + "                        try{\n"
            + "                            xmlHttp = new ActiveXObject(\"Microsoft.XMLHTTP\");\n"
            + "                        } catch (failed){\n" + "                            xmlHttp = null;\n"
            + "                        }\n" + "                    }\n" + "                }\n\n"
            + "                if (element.style.visibility == 'hidden') {\n");

    if (myBrowserdetection.isIe && myBrowserdetection.getmajor() <= 6) {
      sb.append(
          "                    element.style.height = '1px';\n" + "                    element.style.visibility='visible';\n"
              + "                    element.style.overflow='visible';\n" + "                    if (xmlHttp != null)\n");
      if (mySession == null) {
        sb.append("                        xmlHttp.open(\"GET\", '" + prefix + Constants.SERVLET_URL + servletName
            + "?service=showhidemenu&element=' + element_id + '&show=true', true);\n");
      } else {
        sb.append("                        xmlHttp.open(\"GET\", '"
            + res.encodeURL(
                prefix + Constants.SERVLET_URL + servletName + "?service=showhidemenu&element=' + element_id + '&show=true")
            + "', true);\n");
      }
      sb.append("                } else {\n" + "                    element.style.height = '1px';\n"
          + "                    element.style.visibility='hidden';\n"
          + "                    element.style.overflow='hidden';\n" + "                    if (xmlHttp != null)\n");
      if (mySession == null) {
        sb.append("                        xmlHttp.open(\"GET\", '" + prefix + Constants.SERVLET_URL + servletName
            + "?service=showhidemenu&element=' + element_id + '&show=false', true);\n");
      } else {
        sb.append("                        xmlHttp.open(\"GET\", '"
            + res.encodeURL(
                prefix + Constants.SERVLET_URL + servletName + "?service=showhidemenu&element=' + element_id + '&show=false")
            + "', true);\n");
      }
    } else {
      sb.append("                    element.style.height = 'auto';\n"
          + "                    element.style.visibility='visible';\n" + "                    if (xmlHttp != null)\n");
      if (mySession == null) {
        sb.append("                        xmlHttp.open(\"GET\", '" + prefix + Constants.SERVLET_URL + servletName
            + "?service=showhidemenu&element=' + element_id + '&show=true', true);\n");
      } else {
        sb.append("                        xmlHttp.open(\"GET\", '"
            + res.encodeURL(
                prefix + Constants.SERVLET_URL + servletName + "?service=showhidemenu&element=' + element_id + '&show=true")
            + "', true);\n");
      }
      sb.append("                } else {\n" + "                    element.style.height = '1px';\n"
          + "                    element.style.visibility='hidden';\n" + "                    if (xmlHttp != null)\n");
      if (mySession == null) {
        sb.append("                        xmlHttp.open(\"GET\", '" + prefix + Constants.SERVLET_URL + servletName
            + "?service=showhidemenu&element=' + element_id + '&show=false', true);\n");
      } else {
        sb.append("                        xmlHttp.open(\"GET\", '"
            + res.encodeURL(
                prefix + Constants.SERVLET_URL + servletName + "?service=showhidemenu&element=' + element_id + '&show=false")
            + "', true);\n");
      }
    }
    sb.append("                }\n" + "                if (xmlHttp != null) {\n"
        + "                    xmlHttp.onreadystatechange = updateMenu;\n" + "                    xmlHttp.send(null);\n"
        + "                }\n" + "            }\n\n" + "            function updateMenu() {\n"
        + "                // nothing to do\n" + "            }\n" + "            //-->\n" + "        </script>\n");
    if (additional != null) {
      sb.append(additional);
    }
    if (myBrowserdetection.isIe && myBrowserdetection.getmajor() <= 6) {
      sb.append("        <!--[if IE]><style type=\"text/css\">\n" + "            @media screen {\n"
          + "                html, body {\n" + "                    height: 100%; overflow-y: hidden;\n" + "                }\n"
          + "                #body {\n" + "                    height: 100%; width: 100%; overflow: auto;\n"
          + "                }\n" + "                #menu {\n" + "                    position: static;\n"
          + "                }\n" + "            }\n" + "        </style><![endif]-->\n");
    }
    sb.append("    </head>\n" + "    <body");
    if (selectedField != null) {
      sb.append(" onLoad=\"javascript:document.forms[0]." + selectedField + ".focus();\"");
    }
    sb.append(">\n" + "        <div ");
    if (myBrowserdetection.isNav && myBrowserdetection.getmajor() <= 4) {
      sb.append("style=\"position:fixed; width:23%; height:90%; left:0px; top:0px; "
          + "background-color:white; border-style:solid; border-width:1px; border-color:white;\"");
    } else {
      sb.append("id=\"nav\"");
    }
    sb.append(">\n" + "            <a href=\"/index.html\"><img src=\"" + pictureService + "\" srcset=\"" + pictureServiceHiDpi
        + " 2x\" " + "width=\"" + pictureSizeX + "\" height=\"" + pictureSizeY + "\" alt=\"" + pictureDescription + "\"></a>\n"
        + "            <br>\n" + "            <h2 class=\"service\"");
    if (serviceColor != null) {
      sb.append(" style=\"color:" + serviceColor + ";\"");
    }
    sb.append(">" + service + "</h2>\n" + PageTemplate.navHtml(myNavStrukt, mySession, res, myBrowserdetection)
        + "        </div>\n" + "        <div ");
    if (myBrowserdetection.isNav && myBrowserdetection.getmajor() <= 4) {
      sb.append("style=\"position:absolute; width:23%; height:9%; left:0px; top:90%; "
          + "background-color:white; border-style:solid; border-width:1px; border-color:white;\"");
    } else {
      sb.append("id=\"copy\"");
    }
    sb.append('>');
    if (copyrightMail == null) {
      sb.append("&copy; ").append(copyright);
    } else {
      sb.append("<a class=\"copy\" href=\"mailto:" + copyrightMail + "\">&copy; " + copyright + "</a>");
    }
    sb.append("</div>\n" + "        <div ");
    if (myBrowserdetection.isNav && myBrowserdetection.getmajor() <= 4) {
      sb.append("style=\"position:absolute; width:76%; height:5%; left:23%; top:0px; "
          + "background-color:#f7f7f7; border-style:solid; border-width:1px; " + "border-color:#f7f7f7;\"");
    } else {
      sb.append("id=\"head\"");
    }
    sb.append("><h1 class=\"ueb\">");
    if (title == null || title.equals(StringUtils.EMPTY)) {
      sb.append("Ohne Titel");
    } else {
      sb.append(de.knightsoft.common.StringToHtml.convert(title));
    }
    sb.append("</h1></div>\n" + "        <div ");
    if (myBrowserdetection.isNav && myBrowserdetection.getmajor() <= 4) {
      sb.append("style=\"position:absolute; width:76%; height:93%; left:23%; top:5%; "
          + "background-color:white; border-style:solid; border-width:1px; border-color:white;\"");
    } else if (myBrowserdetection.isIe && myBrowserdetection.getmajor() == 7) {
      sb.append("style=\"font-family:Verdana,Arial,Helvetica,sans-serif; font-size:14px; "
          + "position:absolute; left:23%; top:5%; width:76%; height:93%; overflow:auto;" + "    padding-left:5px\"");
    } else {
      sb.append("id=\"body\"");
    }
    sb.append(">\n" + body + "        </div>\n" + "    </body>\n" + "</html>");

    return sb.toString();
  }
}
