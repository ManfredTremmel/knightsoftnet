/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.common;

import java.sql.Timestamp;

/**
 * <code>StringToSql</code> is a class to convert a string to a SQL string.
 *
 * @author Manfred Tremmel
 * @version 1.0.4, 17.08.2005
 */
public class StringToSql {

  /**
   * The <code>convertStringPrepared</code> method replaces &lt;br&gt; with LineFeed and the char 128 (stupid windows euro sign)
   * to 164.
   *
   * @param javaString Java string to convert
   * @return string as SQL
   */
  public static final String convertStringPrepared(final String javaString) {
    return StringToSql.convertStringPrepared(javaString, Constants.JDBC_CLASS_MYSQL);
  }

  /**
   * The <code>convertStringPrepared</code> method replaces &lt;br&gt; with LineFeed and the char 128 (stupid windows euro sign)
   * to 164.
   *
   * @param javaString Java string to convert
   * @param jdbcClass name of the JDBC class to detect DataBase
   * @return string as SQL
   */
  public static final String convertStringPrepared(final String javaString, //
      final String jdbcClass) {
    String sqlString = null;

    if (javaString != null && javaString.length() > 0) {
      final char[] sqlStringTab = new char[2 * javaString.length()];
      final char[] javaStringTab = javaString.toCharArray();
      int pos = 0;

      for (int i = 0; i < javaString.length(); i++) {
        switch (javaStringTab[i]) {
          // case '\n':
          // SQLStringTab[j++] = '\\';
          // SQLStringTab[j++] = 'n';
          // break;
          // case '\t':
          // SQLStringTab[j++] = '\\';
          // SQLStringTab[j++] = 't';
          // break;
          case '\r':
            // SQLStringTab[j++] = '\\';
            // SQLStringTab[j++] = 'r';
            break;
          // case '\b':
          // SQLStringTab[j++] = '\\';
          // SQLStringTab[j++] = 'b';
          // break;
          case '<':
            if (i + 3 < javaString.length() && javaStringTab[i + 1] == 'b' && javaStringTab[i + 2] == 'r'
                && javaStringTab[i + 3] == '>') {
              i += 3;
              // SQLStringTab[j++] = '\\';
              // SQLStringTab[j++] = 'n';
              sqlStringTab[pos++] = '\n';
            } else {
              sqlStringTab[pos++] = javaStringTab[i];
            }
            break;
          // case '\\':
          // case '\'':
          // case '"':
          // SQLStringTab[j++] = '\\';
          // SQLStringTab[j++] = JavaStringTab[pos];
          // break;
          case 128:
            sqlStringTab[pos++] = '€';
            break;
          default:
            sqlStringTab[pos++] = javaStringTab[i];
            break;
        }
      }
      sqlString = new String(sqlStringTab, 0, pos);
    }

    return sqlString;
  }

  /**
   * The <code>convert</code> method converts a Java string to SQL.
   *
   * @param javaString Java string to convert
   * @return string as SQL
   */
  public static final String convert(final String javaString) {
    return StringToSql.convert(javaString, Constants.JDBC_CLASS_MYSQL);
  }

  /**
   * The <code>convert</code> method converts a Java string to SQL.
   *
   * @param javaString Java string to convert
   * @param jdbcClass name of the JDBC class to detect DataBase
   * @return string as SQL
   */
  public static final String convert(final String javaString, final String jdbcClass) {
    String convertedString = null;

    if (jdbcClass == null || jdbcClass.equals(Constants.JDBC_CLASS_MYSQL)) {
      convertedString = StringToSql.convertMySql(javaString);
    } else if (jdbcClass.equals(Constants.JDBC_CLASS_MSSQL)) {
      convertedString = StringToSql.convertMsSql(javaString);
    } else {
      convertedString = StringToSql.convertMySql(javaString);
    }
    return convertedString;
  }

  /**
   * The <code>convertMySQL</code> method converts a Java string to SQL.
   *
   * @param javaString Java string to convert MySQL Style
   * @return string as SQL
   */
  private static final String convertMySql(final String javaString) {
    String sqlString = null;

    if (javaString != null && javaString.length() > 0) {
      final char[] sqlStringTab = new char[2 + 2 * javaString.length()];
      final char[] javaStringTab = javaString.toCharArray();
      int pos = 0;

      sqlStringTab[pos++] = '\'';

      for (int i = 0; i < javaString.length(); i++) {
        switch (javaStringTab[i]) {
          case '\n':
            sqlStringTab[pos++] = '\\';
            sqlStringTab[pos++] = 'n';
            break;
          case '\t':
            sqlStringTab[pos++] = '\\';
            sqlStringTab[pos++] = 't';
            break;
          case '\r':
            sqlStringTab[pos++] = '\\';
            sqlStringTab[pos++] = 'r';
            break;
          case '\b':
            sqlStringTab[pos++] = '\\';
            sqlStringTab[pos++] = 'b';
            break;
          case '<':
            if (i + 3 < javaString.length() && javaStringTab[i + 1] == 'b' && javaStringTab[i + 2] == 'r'
                && javaStringTab[i + 3] == '>') {
              i += 3;
              sqlStringTab[pos++] = '\\';
              sqlStringTab[pos++] = 'n';
            } else {
              sqlStringTab[pos++] = javaStringTab[i];
            }
            break;
          case '"':
          case '\\':
          case '\'':
            sqlStringTab[pos++] = '\\';
            sqlStringTab[pos++] = javaStringTab[i];
            break;
          case 128:
            sqlStringTab[pos++] = '€';
            break;
          default:
            sqlStringTab[pos++] += javaStringTab[i];
            break;
        }
      }
      sqlStringTab[pos++] = '\'';

      sqlString = new String(sqlStringTab, 0, pos);
    }

    return sqlString;
  }

  /**
   * The <code>convertMSSQL</code> method converts a Java string to SQL.
   *
   * @param javaString Java string to convert MSSQL Style
   * @return string as SQL
   */
  private static final String convertMsSql(final String javaString) {
    String sqlString = null;

    if (javaString != null && javaString.length() > 0) {
      final char[] sqlStringTab = new char[2 + 2 * javaString.length()];
      final char[] javaStringTab = javaString.toCharArray();
      int pos = 0;

      sqlStringTab[pos++] = '\'';

      for (int i = 0; i < javaString.length(); i++) {
        switch (javaStringTab[i]) {
          case '\n':
            sqlStringTab[pos++] = '\\';
            sqlStringTab[pos++] = 'n';
            break;
          case '\t':
            sqlStringTab[pos++] = '\\';
            sqlStringTab[pos++] = 't';
            break;
          case '\r':
            sqlStringTab[pos++] = '\\';
            sqlStringTab[pos++] = 'r';
            break;
          case '\b':
            sqlStringTab[pos++] = '\\';
            sqlStringTab[pos++] = 'b';
            break;
          case '<':
            if (i + 3 < javaString.length() && javaStringTab[i + 1] == 'b' && javaStringTab[i + 2] == 'r'
                && javaStringTab[i + 3] == '>') {
              i += 3;
              sqlStringTab[pos++] = '\\';
              sqlStringTab[pos++] = 'n';
            } else {
              sqlStringTab[pos++] = javaStringTab[i];
            }
            break;
          case '\'':
            sqlStringTab[pos++] = javaStringTab[i];
            sqlStringTab[pos++] = javaStringTab[i];
            break;
          case 128:
            sqlStringTab[pos++] = '€';
            break;
          default:
            sqlStringTab[pos++] += javaStringTab[i];
            break;
        }
      }
      sqlStringTab[pos++] = '\'';

      sqlString = new String(sqlStringTab, 0, pos);
    }

    return sqlString;
  }

  /**
   * The <code>convertString</code> is the same as convert.
   *
   * @param javaString Java string to convert
   * @return string as SQL
   */
  public static final String convertString(final String javaString) {
    return StringToSql.convert(javaString);
  }

  /**
   * The <code>convertString</code> is the same as convert.
   *
   * @param javaString Java string to convert
   * @param jdbcClass name of the JDBC class to detect DataBase
   * @return string as SQL
   */
  public static final String convertString(final String javaString, final String jdbcClass) {
    return StringToSql.convert(javaString, jdbcClass);
  }

  /**
   * The <code>convertNumber</code> method converts a Java string, contains a number to SQL.
   *
   * @param javaString Java string to convert
   * @return string as SQL
   */
  public static final String convertNumber(final String javaString) {
    return StringToSql.convertNumber(javaString, Constants.JDBC_CLASS_MYSQL);
  }

  /**
   * The <code>convertNumber</code> method converts a Java string, contains a number to SQL.
   *
   * @param javaString Java string to convert
   * @param jdbcClass name of the JDBC class to detect DataBase
   * @return string as SQL
   */
  public static final String convertNumber(final String javaString, final String jdbcClass) {
    String sqlString = null;

    if (javaString != null && javaString.length() > 0) {
      final char[] sqlStringTab = new char[javaString.length()];
      final char[] javaStringTab = javaString.toCharArray();
      int pos = 0;

      for (int i = 0; i < javaString.length(); i++) {
        switch (javaStringTab[i]) {
          case '0':
          case '1':
          case '2':
          case '3':
          case '4':
          case '5':
          case '6':
          case '7':
          case '8':
          case '9':
          case '.':
            sqlStringTab[pos++] += javaStringTab[i];
            break;
          default:
            break;
        }
      }

      if (pos > 0) {
        sqlString = new String(sqlStringTab, 0, pos);
      }
    }

    return sqlString;
  }

  /**
   * The <code>searchString</code> method prepares a string for searching in a SQL database using = or like.
   *
   * @param dbFieldname database fieldname
   * @param javaString Java string to convert
   * @return string as SQL
   */
  public static final String searchString(final String dbFieldname, final String javaString) {
    return StringToSql.searchString(dbFieldname, javaString, Constants.JDBC_CLASS_MYSQL);
  }

  /**
   * The <code>SearchString</code> method prepares a string for searching in a SQL database using = or like.
   *
   * @param dbFieldname database fieldname
   * @param javaString Java string to convert
   * @param jdbcClass name of the JDBC class to detect DataBase
   * @return string as SQL
   */
  public static final String searchString(final String dbFieldname, final String javaString, final String jdbcClass) {
    String convertedString = null;

    if (jdbcClass == null || jdbcClass.equals(Constants.JDBC_CLASS_MYSQL)) {
      convertedString = StringToSql.searchStringMySql(dbFieldname, javaString);
    } else if (jdbcClass.equals(Constants.JDBC_CLASS_MSSQL)) {
      convertedString = StringToSql.searchStringMsSql(dbFieldname, javaString);
    } else {
      convertedString = StringToSql.searchStringMySql(dbFieldname, javaString);
    }
    return convertedString;
  }

  /**
   * The <code>searchStringMySQL</code> method prepares a string for searching in a SQL database using = or like.
   *
   * @param dbFieldname database fieldname
   * @param javaString Java string to convert
   * @return string as SQL MySQL style
   */
  private static final String searchStringMySql(final String dbFieldname, final String javaString) {
    final StringBuffer sqlString = new StringBuffer(256);

    if (javaString != null && javaString.length() > 0) {
      final char[] sqlStringTab = new char[2 + 2 * javaString.length()];
      final char[] javaStringTab = javaString.toCharArray();
      int pos = 0;
      boolean wildcard = false;

      sqlStringTab[pos++] = '\'';

      for (int i = 0; i < javaString.length(); i++) {
        switch (javaStringTab[i]) {
          case '\n':
            sqlStringTab[pos++] = '\\';
            sqlStringTab[pos++] = 'n';
            break;
          case '\t':
            sqlStringTab[pos++] = '\\';
            sqlStringTab[pos++] = 't';
            break;
          case '\r':
            sqlStringTab[pos++] = '\\';
            sqlStringTab[pos++] = 'r';
            break;
          case '\b':
            sqlStringTab[pos++] = '\\';
            sqlStringTab[pos++] = 'b';
            break;
          case '<':
            if (i + 3 < javaString.length() && javaStringTab[i + 1] == 'b' && javaStringTab[i + 2] == 'r'
                && javaStringTab[i + 3] == '>') {
              i += 3;
              sqlStringTab[pos++] = '\\';
              sqlStringTab[pos++] = 'n';
            } else {
              sqlStringTab[pos++] = javaStringTab[i];
            }
            break;
          case '"':
          case '\\':
          case '\'':
            sqlStringTab[pos++] = '\\';
            sqlStringTab[pos++] = javaStringTab[i];
            break;
          case '*':
            sqlStringTab[pos++] = '%';
            wildcard = true;
            break;
          case '?':
            sqlStringTab[pos++] = '_';
            wildcard = true;
            break;
          case 128:
            sqlStringTab[pos++] = '€';
            break;
          default:
            sqlStringTab[pos++] += javaStringTab[i];
            break;
        }
      }
      sqlStringTab[pos++] = '\'';

      if (wildcard) {
        sqlString.append(dbFieldname).append(" like ").append(new String(sqlStringTab, 0, pos));
      } else {
        sqlString.append(dbFieldname).append('=').append(new String(sqlStringTab, 0, pos));
      }
    }

    return sqlString.toString();
  }

  /**
   * The <code>searchStringMSSQL</code> method prepares a string for searching in a SQL database using = or like.
   *
   * @param dbFieldname database fieldname
   * @param javaString Java string to convert
   * @return string as SQL MSSQL style
   */
  private static final String searchStringMsSql(final String dbFieldname, final String javaString) {
    final StringBuffer sqlString = new StringBuffer(256);

    if (javaString != null && javaString.length() > 0) {
      final char[] sqlStringTab = new char[2 + 2 * javaString.length()];
      final char[] javaStringTab = javaString.toCharArray();
      int pos = 0;
      boolean wildcard = false;

      sqlStringTab[pos++] = '\'';

      for (int i = 0; i < javaString.length(); i++) {
        switch (javaStringTab[i]) {
          case '\n':
            sqlStringTab[pos++] = '\\';
            sqlStringTab[pos++] = 'n';
            break;
          case '\t':
            sqlStringTab[pos++] = '\\';
            sqlStringTab[pos++] = 't';
            break;
          case '\r':
            sqlStringTab[pos++] = '\\';
            sqlStringTab[pos++] = 'r';
            break;
          case '\b':
            sqlStringTab[pos++] = '\\';
            sqlStringTab[pos++] = 'b';
            break;
          case '<':
            if (i + 3 < javaString.length() && javaStringTab[i + 1] == 'b' && javaStringTab[i + 2] == 'r'
                && javaStringTab[i + 3] == '>') {
              i += 3;
              sqlStringTab[pos++] = '\\';
              sqlStringTab[pos++] = 'n';
            } else {
              sqlStringTab[pos++] = javaStringTab[i];
            }
            break;
          case '\'':
            sqlStringTab[pos++] = javaStringTab[i];
            sqlStringTab[pos++] = javaStringTab[i];
            break;
          case '*':
            sqlStringTab[pos++] = '%';
            wildcard = true;
            break;
          case '?':
            sqlStringTab[pos++] = '_';
            wildcard = true;
            break;
          case 128:
            sqlStringTab[pos++] = '€';
            break;
          default:
            sqlStringTab[pos++] += javaStringTab[i];
            break;
        }
      }
      sqlStringTab[pos++] = '\'';

      if (wildcard) {
        sqlString.append(dbFieldname).append(" like ").append(new String(sqlStringTab, 0, pos));
      } else {
        sqlString.append(dbFieldname).append('=').append(new String(sqlStringTab, 0, pos));
      }
    }

    return sqlString.toString();
  }

  /**
   * The <code>convertDatetime</code> method converts a DateTimeField to SQL.
   *
   * @param javaDateTime Timestamp to convert
   * @param jdbcClass name of the JDBC class to detect DataBase
   * @return string as SQL
   */
  public static final String convertDatetime(final Timestamp javaDateTime, final String jdbcClass) {
    String convertedString = null;

    if (jdbcClass == null || jdbcClass.equals(Constants.JDBC_CLASS_MYSQL)) {
      convertedString = StringToSql.convertDatetimeMySql(javaDateTime);
    } else if (jdbcClass.equals(Constants.JDBC_CLASS_MSSQL)) {
      convertedString = StringToSql.convertDatetimeMsSql(javaDateTime);
    } else {
      convertedString = StringToSql.convertDatetimeMySql(javaDateTime);
    }
    return convertedString;
  }

  /**
   * The <code>convertDatetimeMySQL</code> method converts a DateTimeField to SQL.
   *
   * @param javaDateTime Timestamp to convert MySQL Style
   * @return string as SQL
   */
  private static final String convertDatetimeMySql(final Timestamp javaDateTime) {
    final String sqlString;

    if (javaDateTime == null) {
      sqlString = "null";
    } else {
      sqlString = "\'" + new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(javaDateTime) + "\'";
    }

    return sqlString;
  }

  /**
   * The <code>convertDatetimeMSSQL</code> method converts a DateTimeField to SQL.
   *
   * @param javaDateTime Timestamp to convert MSSQL Style
   * @return string as SQL
   */
  private static final String convertDatetimeMsSql(final Timestamp javaDateTime) {
    final String sqlString;

    if (javaDateTime == null) {
      sqlString = "null";
    } else {
      sqlString =
          "CONVERT(DATETIME, '" + new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(javaDateTime) + "', 121)";
    }

    return sqlString;
  }
}
