/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.common;

/**
 * The <code>Browserdetection</code> class is used to detect the browser and os of the user
 *
 * @author Manfred Tremmel
 * @version 1.0.4, 25.06.2011
 */
public class Browserdetection {

  private final String browserdetection;
  private int isMajor;
  private double isMinor;
  public boolean isNav;
  public boolean isIe;
  public boolean isAol;
  public boolean isOpera;
  public boolean isKonq;
  public boolean isSafari;
  public boolean isChrome;
  public boolean isOther;

  public boolean isWin;
  public boolean isWin16;
  public boolean isWin32;
  public boolean isWin95;
  public boolean isWin98;
  public boolean isWinMe;
  public boolean isWinNt;
  public boolean isWin2k;
  public boolean isOS2;
  public boolean isMac;
  public boolean isMac68k;
  public boolean isMacPpc;
  public boolean isSun;
  public boolean isSun4;
  public boolean isSun5;
  public boolean isSuni86;
  public boolean isIrix; // SGI
  public boolean isIrix5;
  public boolean isIrix6;
  public boolean isHpux;
  public boolean isHpux9;
  public boolean isHpux10;
  public boolean isAix; // IBM
  public boolean isAix1;
  public boolean isAix2;
  public boolean isAix3;
  public boolean isAix4;
  public boolean isLinux;
  public boolean isSco;
  public boolean isUnixware;
  public boolean isMpras;
  public boolean isReliant;
  public boolean isDec;
  public boolean isSinix;
  public boolean isFreeBsd;
  public boolean isBsd;
  public boolean isUnix;
  public boolean isVms;
  public boolean isAmiga;
  public boolean isiPad;
  public boolean isiPod;
  public boolean isiPhone;
  public boolean isiOs;
  public boolean isAndroid;
  public boolean isNokia;
  public boolean isBlackBerry;
  public boolean isOtherOs;

  /**
   * Constructor.
   *
   * @param browserkennung the string which is sent by the browser
   */
  public Browserdetection(final String browserkennung) {
    browserdetection = browserkennung;
    if (browserdetection == null) {
      isMajor = 0;
      isMinor = 0.0;
      isNav = false;
      isIe = false;
      isAol = false;
      isOpera = false;
      isKonq = false;
      isSafari = false;
      isChrome = false;
      isOther = true;

      isWin = false;
      isWin16 = false;
      isWin32 = false;
      isWin95 = false;
      isWin98 = false;
      isWinMe = false;
      isWinNt = false;
      isWin2k = false;
      isOS2 = false;
      isMac = false;
      isMac68k = false;
      isMacPpc = false;
      isSun = false;
      isSun4 = false;
      isSun5 = false;
      isSuni86 = false;
      isIrix = false;
      isIrix5 = false;
      isIrix6 = false;
      isHpux = false;
      isHpux9 = false;
      isHpux10 = false;
      isAix = false;
      isAix1 = false;
      isAix2 = false;
      isAix3 = false;
      isAix4 = false;
      isLinux = false;
      isSco = false;
      isUnixware = false;
      isMpras = false;
      isReliant = false;
      isDec = false;
      isSinix = false;
      isFreeBsd = false;
      isBsd = false;
      isUnix = false;
      isVms = false;
      isAmiga = false;
      isiPad = false;
      isiPod = false;
      isiPhone = false;
      isiOs = false;
      isAndroid = false;
      isNokia = false;
      isBlackBerry = false;
      isOtherOs = true;

    } else {
      interpret();
    }
  }

  /**
   * The <code>interpret</code> method does the work, it detects the os and browser out of the string.
   */
  private void interpret() {
    // Browsererkennung
    final String agt = browserdetection.toLowerCase();
    isNav = agt.indexOf("mozilla") != -1 && agt.indexOf("spoofer") == -1 && agt.indexOf("compatible") == -1
        && agt.indexOf("opera") == -1 && agt.indexOf("webtv") == -1;
    isIe = agt.indexOf("msie") != -1;
    isAol = agt.indexOf("aol") != -1;
    isOpera = agt.indexOf("opera") != -1;
    isKonq = agt.indexOf("konqueror") != -1;
    isSafari = agt.indexOf("safari") != -1;
    isChrome = agt.indexOf("chrome") != -1;
    isOther = !(isNav || isIe || isAol || isOpera //
        || isKonq || isSafari || isChrome);

    // OS-Erkennung
    isWin = agt.indexOf("win") != -1 || agt.indexOf("16bit") != -1;
    isWin16 = agt.indexOf("win16") != -1 || agt.indexOf("16bit") != -1 || agt.indexOf("windows 3.1") != -1
        || agt.indexOf("windows 16-bit") != -1;
    isWin95 = agt.indexOf("win95") != -1 || agt.indexOf("windows 95") != -1;
    isWin98 = agt.indexOf("win98") != -1 || agt.indexOf("windows 98") != -1;
    isWinMe = agt.indexOf("winme") != -1 || agt.indexOf("windows me") != -1;
    isWinNt = agt.indexOf("winnt") != -1 || agt.indexOf("windows nt") != -1;
    isWin2k = agt.indexOf("win2000") != -1 || agt.indexOf("windows 2000") != -1;
    isWin32 = isWin95 || isWin98 || isWinMe || isWinNt || isWin2k || agt.indexOf("win32") != -1 || agt.indexOf("32bit") != -1;

    isOS2 = agt.indexOf("os/2") != -1 || agt.indexOf("ibm-webexplorer") != -1;

    isMac = agt.indexOf("mac") != -1;
    isMac68k = isMac && (agt.indexOf("68k") != -1 || agt.indexOf("68000") != -1);
    isMacPpc = isMac && (agt.indexOf("ppc") != -1 || agt.indexOf("powerpc") != -1);

    isSun = agt.indexOf("sunos") != -1;
    isSun4 = agt.indexOf("sunos 4") != -1;
    isSun5 = agt.indexOf("sunos 5") != -1;
    isSuni86 = isSun && agt.indexOf("i86") != -1;
    isIrix = agt.indexOf("irix") != -1; // SGI
    isIrix5 = agt.indexOf("irix 5") != -1;
    isIrix6 = agt.indexOf("irix 6") != -1 || agt.indexOf("irix6") != -1;
    isHpux = agt.indexOf("hp-ux") != -1;
    isHpux9 = isHpux && agt.indexOf("09.") != -1;
    isHpux10 = isHpux && agt.indexOf("10.") != -1;
    isAix = agt.indexOf("aix") != -1; // IBM
    isAix1 = agt.indexOf("aix 1") != -1;
    isAix2 = agt.indexOf("aix 2") != -1;
    isAix3 = agt.indexOf("aix 3") != -1;
    isAix4 = agt.indexOf("aix 4") != -1;
    isLinux = agt.indexOf("inux") != -1;
    isSco = agt.indexOf("sco") != -1 || agt.indexOf("unix_sv") != -1;
    isUnixware = agt.indexOf("unix_system_v") != -1;
    isMpras = agt.indexOf("ncr") != -1;
    isReliant = agt.indexOf("reliantunix") != -1;
    isDec = agt.indexOf("dec") != -1 || agt.indexOf("osf1") != -1 || agt.indexOf("dec_alpha") != -1
        || agt.indexOf("alphaserver") != -1 || agt.indexOf("ultrix") != -1 || agt.indexOf("alphastation") != -1;
    isSinix = agt.indexOf("sinix") != -1;
    isFreeBsd = agt.indexOf("freebsd") != -1;
    isBsd = agt.indexOf("bsd") != -1;
    isUnix = agt.indexOf("x11") != -1 || isKonq || isSun || isIrix || isHpux || isSco || isUnixware || isMpras || isReliant
        || isDec || isSinix || isAix || isLinux || isBsd || isFreeBsd;

    isVms = agt.indexOf("vax") != -1 || agt.indexOf("openvms") != -1;

    isAmiga = agt.indexOf("amiga") != -1;
    isiPad = agt.indexOf("iPad") != -1;
    isiPod = agt.indexOf("iPod") != -1;
    isiPhone = agt.indexOf("iPhone") != -1;
    isiOs = isiPad || isiPod || isiPhone;
    isAndroid = agt.indexOf("android") != -1;
    isNokia = agt.indexOf("Nokia") != -1;
    isBlackBerry = agt.indexOf("BlackBerry") != -1;

    isOtherOs = !(isWin || isOS2 || isMac || isUnix || isVms || isAmiga || isiOs || isAndroid || isNokia || isBlackBerry);

    // Versionsnummer ermitteln
    if (isNav) {
      getversion(agt, "mozilla/", ".", " ");
    } else if (isIe) {
      getversion(agt, "msie ", ".", ";");
    } else if (isAol) {
      getversion(agt, "aol ", ".", ";");
    } else if (isOpera) {
      getversion(agt, "opera/", ".", " ");
    } else if (isKonq) {
      getversion(agt, "konqueror/", ".", ";");
    } else if (isSafari) {
      getversion(agt, "safari/", ".", ";");
    } else if (isChrome) {
      getversion(agt, "chrome/", ".", ";");
    } else {
      isMajor = 0;
      isMinor = 0.0;
    }
  }

  /**
   * The Method <code>getversion</code> is calld from Method interpret and finds out major and minor version number
   *
   * @param agt browser detection string (lowercase!)
   * @param searchfor browser specific string (mozilla/, msie, aol, opera/ or konqueror/)
   * @param separator the sign which seperates major from minor number (.)
   * @param endseparator the sign which ends the version (" " or ;)
   */
  private void getversion(final String agt, final String searchfor, final String separator, final String endseparator) {
    int pos = agt.indexOf(searchfor) + searchfor.length();
    final int j = agt.indexOf(separator, pos);

    if (pos == -1 || j == -1) {
      isMajor = 0;
      isMinor = 0.0;
    } else {
      try {
        isMajor = Integer.parseInt(agt.substring(pos, j));
      } catch (final java.lang.NumberFormatException e) {
        isMajor = 0;
      }

      pos = agt.indexOf(endseparator, j);
      // if (pos == -1)
      isMinor = 0.0;
      // else {
      // try {
      // is_minor = Double.parseDouble(agt.substring((j+1),pos));
      // } catch (java.lang.NumberFormatException e) {
      // is_minor = 0.0;
      // }
      // }
    }
  }

  /**
   * The Method <code>getmajor</code> gives back the major version number of the detected browser.
   *
   * @return major version number of the browser
   */
  public int getmajor() {
    return isMajor;
  }

  /**
   * The Method <code>getminor</code> gives back the minor version number of the detected browser.
   *
   * @return minor version number of the browser
   */
  public double getminor() {
    return isMinor;
  }
}
