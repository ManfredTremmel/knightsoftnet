/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.common;

/**
 * The <code>CsvImportTabStrukt</code> declares a structure that's used for csv imports with CsvKonvert class.
 *
 * @author Manfred Tremmel
 * @version 1.0.2, 06.08.2005
 */
public class CsvImportTabStrukt {

  public boolean complusion;
  public boolean primarykey;
  public boolean isastring;
  public int column;
  public String dbfieldname;
  public String[] csvfieldnames;

  /**
   * Constructor.
   *
   * @param complusion must this field be filled (true/false)
   * @param primarykey is this field a primary key field (true/false)
   * @param isastring is this field a string field (true/false)
   * @param column column number where to find this field
   * @param dbfieldname field name in the database
   * @param csvfieldnames list of possible fieldnames in the csv table
   */
  public CsvImportTabStrukt(final boolean complusion, final boolean primarykey, final boolean isastring, final int column,
      final String dbfieldname, final String[] csvfieldnames) {
    this.complusion = complusion;
    this.primarykey = primarykey;
    this.isastring = isastring;
    this.column = column;
    this.dbfieldname = dbfieldname;
    this.csvfieldnames = csvfieldnames;
  }
}
