/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.common;

import org.apache.commons.lang3.StringUtils;

/**
 * <code>StringToHtml</code> is a class to convert a string to HTML.
 *
 * @author Manfred Tremmel
 * @version 1.0.2, 06.08.2005
 */
public class StringToHtml {

  /**
   * The <code>convert</code> method converts the string to a table of dos charset bytes.
   *
   * @param javaString Java string to convert
   * @return string as HTML
   */
  public static final String convert(final String javaString) {
    return StringToHtml.convert(javaString, false, true, true);
  }

  /**
   * The <code>convert</code> method converts the string to a table of dos charset bytes.
   *
   * @param javaString Java string to convert
   * @param blankwandeln convert blank to &nbsp; (true/false)
   * @return string as HTML
   */
  public static final String convert(final String javaString, final boolean blankwandeln) {
    return StringToHtml.convert(javaString, blankwandeln, true, true);
  }

  /**
   * The <code>convert</code> method converts the string to a table of dos charset bytes.
   *
   * @param javaString Java string to convert
   * @param blankwandeln convert blank to &nbsp; (true/false)
   * @param returnwandeln convert linefeed to <br>
   *        (true/false)
   * @return string as HTML
   */
  public static final String convert(final String javaString, final boolean blankwandeln, final boolean returnwandeln) {
    return StringToHtml.convert(javaString, blankwandeln, returnwandeln, true);
  }

  /**
   * The <code>convert</code> method converts the string to a table of dos charset bytes.
   *
   * @param javaString Java string to convert
   * @param blankwandeln convert blank to &nbsp; (true/false)
   * @param returnwandeln convert linefeed to <br>
   *        (true/false)
   * @param tagswandeln &lt; to &amp;lt; and &gt; to &amp;gt; (true/false)
   * @return string as HTML
   */
  public static final String convert(final String javaString, final boolean blankwandeln, final boolean returnwandeln,
      final boolean tagswandeln) {
    String htmlString = StringUtils.EMPTY;

    if (javaString != null && javaString.length() > 0) {
      final char[] htmlStringTab = new char[8 * javaString.length()];
      final char[] javaStringTab = javaString.toCharArray();
      int pos = 0;

      for (int i = 0; i < javaString.length(); i++) {
        switch (javaStringTab[i]) {
          case '¡':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'i';
            htmlStringTab[pos++] = 'e';
            htmlStringTab[pos++] = 'x';
            htmlStringTab[pos++] = 'c';
            htmlStringTab[pos++] = 'l';
            htmlStringTab[pos++] = ';';
            break;
          case '¢':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'c';
            htmlStringTab[pos++] = 'e';
            htmlStringTab[pos++] = 'n';
            htmlStringTab[pos++] = 't';
            htmlStringTab[pos++] = ';';
            break;
          case '£':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'p';
            htmlStringTab[pos++] = 'o';
            htmlStringTab[pos++] = 'u';
            htmlStringTab[pos++] = 'n';
            htmlStringTab[pos++] = 'd';
            htmlStringTab[pos++] = ';';
            break;
          case '€':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'e';
            htmlStringTab[pos++] = 'u';
            htmlStringTab[pos++] = 'r';
            htmlStringTab[pos++] = 'o';
            htmlStringTab[pos++] = ';';
            break;
          case '¥':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'y';
            htmlStringTab[pos++] = 'e';
            htmlStringTab[pos++] = 'n';
            htmlStringTab[pos++] = ';';
            break;
          case '§':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 's';
            htmlStringTab[pos++] = 'e';
            htmlStringTab[pos++] = 'c';
            htmlStringTab[pos++] = 't';
            htmlStringTab[pos++] = ';';
            break;
          case '©':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'c';
            htmlStringTab[pos++] = 'o';
            htmlStringTab[pos++] = 'p';
            htmlStringTab[pos++] = 'y';
            htmlStringTab[pos++] = ';';
            break;
          case 'ª':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'o';
            htmlStringTab[pos++] = 'r';
            htmlStringTab[pos++] = 'd';
            htmlStringTab[pos++] = 'f';
            htmlStringTab[pos++] = ';';
            break;
          case '«':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'l';
            htmlStringTab[pos++] = 'a';
            htmlStringTab[pos++] = 'q';
            htmlStringTab[pos++] = 'u';
            htmlStringTab[pos++] = 'o';
            htmlStringTab[pos++] = ';';
            break;
          case '¬':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'n';
            htmlStringTab[pos++] = 'o';
            htmlStringTab[pos++] = 't';
            htmlStringTab[pos++] = ';';
            break;
          case '­':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 's';
            htmlStringTab[pos++] = 'h';
            htmlStringTab[pos++] = 'y';
            htmlStringTab[pos++] = ';';
            break;
          case '®':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'r';
            htmlStringTab[pos++] = 'e';
            htmlStringTab[pos++] = 'g';
            htmlStringTab[pos++] = ';';
            break;
          case '¯':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'm';
            htmlStringTab[pos++] = 'a';
            htmlStringTab[pos++] = 'c';
            htmlStringTab[pos++] = 'r';
            htmlStringTab[pos++] = ';';
            break;
          case '°':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'd';
            htmlStringTab[pos++] = 'e';
            htmlStringTab[pos++] = 'g';
            htmlStringTab[pos++] = ';';
            break;
          case '±':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'p';
            htmlStringTab[pos++] = 'l';
            htmlStringTab[pos++] = 'u';
            htmlStringTab[pos++] = 's';
            htmlStringTab[pos++] = 'm';
            htmlStringTab[pos++] = 'n';
            htmlStringTab[pos++] = ';';
            break;
          case '²':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 's';
            htmlStringTab[pos++] = 'u';
            htmlStringTab[pos++] = 'p';
            htmlStringTab[pos++] = '2';
            htmlStringTab[pos++] = ';';
            break;
          case '³':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 's';
            htmlStringTab[pos++] = 'u';
            htmlStringTab[pos++] = 'p';
            htmlStringTab[pos++] = '3';
            htmlStringTab[pos++] = ';';
            break;
          case 'µ':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'm';
            htmlStringTab[pos++] = 'i';
            htmlStringTab[pos++] = 'c';
            htmlStringTab[pos++] = 'r';
            htmlStringTab[pos++] = 'o';
            htmlStringTab[pos++] = ';';
            break;
          case '¶':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'p';
            htmlStringTab[pos++] = 'a';
            htmlStringTab[pos++] = 'r';
            htmlStringTab[pos++] = 'a';
            htmlStringTab[pos++] = ';';
            break;
          case '·':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'm';
            htmlStringTab[pos++] = 'i';
            htmlStringTab[pos++] = 'd';
            htmlStringTab[pos++] = 'd';
            htmlStringTab[pos++] = 'o';
            htmlStringTab[pos++] = 't';
            htmlStringTab[pos++] = ';';
            break;
          case '¹':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 's';
            htmlStringTab[pos++] = 'u';
            htmlStringTab[pos++] = 'p';
            htmlStringTab[pos++] = '1';
            htmlStringTab[pos++] = ';';
            break;
          case 'º':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'o';
            htmlStringTab[pos++] = 'r';
            htmlStringTab[pos++] = 'd';
            htmlStringTab[pos++] = 'm';
            htmlStringTab[pos++] = ';';
            break;
          case '»':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'r';
            htmlStringTab[pos++] = 'a';
            htmlStringTab[pos++] = 'q';
            htmlStringTab[pos++] = 'u';
            htmlStringTab[pos++] = 'o';
            htmlStringTab[pos++] = ';';
            break;
          case '¿':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'i';
            htmlStringTab[pos++] = 'q';
            htmlStringTab[pos++] = 'u';
            htmlStringTab[pos++] = 'e';
            htmlStringTab[pos++] = 's';
            htmlStringTab[pos++] = 't';
            htmlStringTab[pos++] = ';';
            break;
          case 'À':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'A';
            htmlStringTab[pos++] = 'g';
            htmlStringTab[pos++] = 'r';
            htmlStringTab[pos++] = 'a';
            htmlStringTab[pos++] = 'v';
            htmlStringTab[pos++] = 'e';
            htmlStringTab[pos++] = ';';
            break;
          case 'Á':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'A';
            htmlStringTab[pos++] = 'a';
            htmlStringTab[pos++] = 'c';
            htmlStringTab[pos++] = 'u';
            htmlStringTab[pos++] = 't';
            htmlStringTab[pos++] = 'e';
            htmlStringTab[pos++] = ';';
            break;
          case 'Â':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'A';
            htmlStringTab[pos++] = 'c';
            htmlStringTab[pos++] = 'i';
            htmlStringTab[pos++] = 'r';
            htmlStringTab[pos++] = 'c';
            htmlStringTab[pos++] = ';';
            break;
          case 'Ã':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'A';
            htmlStringTab[pos++] = 't';
            htmlStringTab[pos++] = 'i';
            htmlStringTab[pos++] = 'l';
            htmlStringTab[pos++] = 'd';
            htmlStringTab[pos++] = 'e';
            htmlStringTab[pos++] = ';';
            break;
          case 'Ä':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'A';
            htmlStringTab[pos++] = 'u';
            htmlStringTab[pos++] = 'm';
            htmlStringTab[pos++] = 'l';
            htmlStringTab[pos++] = ';';
            break;
          case 'Å':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'A';
            htmlStringTab[pos++] = 'r';
            htmlStringTab[pos++] = 'i';
            htmlStringTab[pos++] = 'n';
            htmlStringTab[pos++] = 'g';
            htmlStringTab[pos++] = ';';
            break;
          case 'Æ':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'A';
            htmlStringTab[pos++] = 'E';
            htmlStringTab[pos++] = 'l';
            htmlStringTab[pos++] = 'i';
            htmlStringTab[pos++] = 'g';
            htmlStringTab[pos++] = ';';
            break;
          case 'Ç':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'C';
            htmlStringTab[pos++] = 'c';
            htmlStringTab[pos++] = 'e';
            htmlStringTab[pos++] = 'd';
            htmlStringTab[pos++] = 'i';
            htmlStringTab[pos++] = 'l';
            htmlStringTab[pos++] = ';';
            break;
          case 'È':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'E';
            htmlStringTab[pos++] = 'g';
            htmlStringTab[pos++] = 'r';
            htmlStringTab[pos++] = 'a';
            htmlStringTab[pos++] = 'v';
            htmlStringTab[pos++] = 'e';
            htmlStringTab[pos++] = ';';
            break;
          case 'É':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'E';
            htmlStringTab[pos++] = 'a';
            htmlStringTab[pos++] = 'c';
            htmlStringTab[pos++] = 'u';
            htmlStringTab[pos++] = 't';
            htmlStringTab[pos++] = 'e';
            htmlStringTab[pos++] = ';';
            break;
          case 'Ê':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'E';
            htmlStringTab[pos++] = 'c';
            htmlStringTab[pos++] = 'i';
            htmlStringTab[pos++] = 'r';
            htmlStringTab[pos++] = 'c';
            htmlStringTab[pos++] = ';';
            break;
          case 'Ë':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'E';
            htmlStringTab[pos++] = 'u';
            htmlStringTab[pos++] = 'm';
            htmlStringTab[pos++] = 'l';
            htmlStringTab[pos++] = ';';
            break;
          case 'Ì':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'I';
            htmlStringTab[pos++] = 'g';
            htmlStringTab[pos++] = 'r';
            htmlStringTab[pos++] = 'a';
            htmlStringTab[pos++] = 'v';
            htmlStringTab[pos++] = 'e';
            htmlStringTab[pos++] = ';';
            break;
          case 'Í':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'I';
            htmlStringTab[pos++] = 'a';
            htmlStringTab[pos++] = 'c';
            htmlStringTab[pos++] = 'u';
            htmlStringTab[pos++] = 't';
            htmlStringTab[pos++] = 'e';
            htmlStringTab[pos++] = ';';
            break;
          case 'Î':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'I';
            htmlStringTab[pos++] = 'c';
            htmlStringTab[pos++] = 'i';
            htmlStringTab[pos++] = 'r';
            htmlStringTab[pos++] = 'c';
            htmlStringTab[pos++] = ';';
            break;
          case 'Ï':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'I';
            htmlStringTab[pos++] = 'u';
            htmlStringTab[pos++] = 'm';
            htmlStringTab[pos++] = 'l';
            htmlStringTab[pos++] = ';';
            break;
          case 'Ð':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'E';
            htmlStringTab[pos++] = 'T';
            htmlStringTab[pos++] = 'H';
            htmlStringTab[pos++] = ';';
            break;
          case 'Ñ':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'N';
            htmlStringTab[pos++] = 't';
            htmlStringTab[pos++] = 'i';
            htmlStringTab[pos++] = 'l';
            htmlStringTab[pos++] = 'd';
            htmlStringTab[pos++] = 'e';
            htmlStringTab[pos++] = ';';
            break;
          case 'Ò':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'O';
            htmlStringTab[pos++] = 'g';
            htmlStringTab[pos++] = 'r';
            htmlStringTab[pos++] = 'a';
            htmlStringTab[pos++] = 'v';
            htmlStringTab[pos++] = 'e';
            htmlStringTab[pos++] = ';';
            break;
          case 'Ó':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'O';
            htmlStringTab[pos++] = 'a';
            htmlStringTab[pos++] = 'c';
            htmlStringTab[pos++] = 'u';
            htmlStringTab[pos++] = 't';
            htmlStringTab[pos++] = 'e';
            htmlStringTab[pos++] = ';';
            break;
          case 'Ô':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'O';
            htmlStringTab[pos++] = 'c';
            htmlStringTab[pos++] = 'i';
            htmlStringTab[pos++] = 'r';
            htmlStringTab[pos++] = 'c';
            htmlStringTab[pos++] = ';';
            break;
          case 'Õ':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'O';
            htmlStringTab[pos++] = 't';
            htmlStringTab[pos++] = 'i';
            htmlStringTab[pos++] = 'l';
            htmlStringTab[pos++] = 'd';
            htmlStringTab[pos++] = 'e';
            htmlStringTab[pos++] = ';';
            break;
          case 'Ö':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'O';
            htmlStringTab[pos++] = 'u';
            htmlStringTab[pos++] = 'm';
            htmlStringTab[pos++] = 'l';
            htmlStringTab[pos++] = ';';
            break;
          case '×':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 't';
            htmlStringTab[pos++] = 'i';
            htmlStringTab[pos++] = 'm';
            htmlStringTab[pos++] = 'e';
            htmlStringTab[pos++] = 's';
            htmlStringTab[pos++] = ';';
            break;
          case 'Ø':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'O';
            htmlStringTab[pos++] = 's';
            htmlStringTab[pos++] = 'l';
            htmlStringTab[pos++] = 'a';
            htmlStringTab[pos++] = 's';
            htmlStringTab[pos++] = 'h';
            htmlStringTab[pos++] = ';';
            break;
          case 'Ù':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'U';
            htmlStringTab[pos++] = 'g';
            htmlStringTab[pos++] = 'r';
            htmlStringTab[pos++] = 'a';
            htmlStringTab[pos++] = 'v';
            htmlStringTab[pos++] = 'e';
            htmlStringTab[pos++] = ';';
            break;
          case 'Ú':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'U';
            htmlStringTab[pos++] = 'a';
            htmlStringTab[pos++] = 'c';
            htmlStringTab[pos++] = 'u';
            htmlStringTab[pos++] = 't';
            htmlStringTab[pos++] = 'e';
            htmlStringTab[pos++] = ';';
            break;
          case 'Û':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'U';
            htmlStringTab[pos++] = 'c';
            htmlStringTab[pos++] = 'i';
            htmlStringTab[pos++] = 'r';
            htmlStringTab[pos++] = 'c';
            htmlStringTab[pos++] = ';';
            break;
          case 'Ü':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'U';
            htmlStringTab[pos++] = 'u';
            htmlStringTab[pos++] = 'm';
            htmlStringTab[pos++] = 'l';
            htmlStringTab[pos++] = ';';
            break;
          case 'Ý':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'Y';
            htmlStringTab[pos++] = 'a';
            htmlStringTab[pos++] = 'c';
            htmlStringTab[pos++] = 'u';
            htmlStringTab[pos++] = 't';
            htmlStringTab[pos++] = 'e';
            htmlStringTab[pos++] = ';';
            break;
          case 'Þ':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'T';
            htmlStringTab[pos++] = 'H';
            htmlStringTab[pos++] = 'O';
            htmlStringTab[pos++] = 'R';
            htmlStringTab[pos++] = 'N';
            htmlStringTab[pos++] = ';';
            break;
          case 'ß':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 's';
            htmlStringTab[pos++] = 'z';
            htmlStringTab[pos++] = 'l';
            htmlStringTab[pos++] = 'i';
            htmlStringTab[pos++] = 'g';
            htmlStringTab[pos++] = ';';
            break;
          case 'à':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'a';
            htmlStringTab[pos++] = 'g';
            htmlStringTab[pos++] = 'r';
            htmlStringTab[pos++] = 'a';
            htmlStringTab[pos++] = 'v';
            htmlStringTab[pos++] = 'e';
            htmlStringTab[pos++] = ';';
            break;
          case 'á':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'a';
            htmlStringTab[pos++] = 'a';
            htmlStringTab[pos++] = 'c';
            htmlStringTab[pos++] = 'u';
            htmlStringTab[pos++] = 't';
            htmlStringTab[pos++] = 'e';
            htmlStringTab[pos++] = ';';
            break;
          case 'â':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'a';
            htmlStringTab[pos++] = 'c';
            htmlStringTab[pos++] = 'i';
            htmlStringTab[pos++] = 'r';
            htmlStringTab[pos++] = 'c';
            htmlStringTab[pos++] = ';';
            break;
          case 'ã':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'a';
            htmlStringTab[pos++] = 't';
            htmlStringTab[pos++] = 'i';
            htmlStringTab[pos++] = 'l';
            htmlStringTab[pos++] = 'd';
            htmlStringTab[pos++] = 'e';
            htmlStringTab[pos++] = ';';
            break;
          case 'ä':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'a';
            htmlStringTab[pos++] = 'u';
            htmlStringTab[pos++] = 'm';
            htmlStringTab[pos++] = 'l';
            htmlStringTab[pos++] = ';';
            break;
          case 'å':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'a';
            htmlStringTab[pos++] = 'r';
            htmlStringTab[pos++] = 'i';
            htmlStringTab[pos++] = 'n';
            htmlStringTab[pos++] = 'g';
            htmlStringTab[pos++] = ';';
            break;
          case 'æ':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'a';
            htmlStringTab[pos++] = 'e';
            htmlStringTab[pos++] = 'l';
            htmlStringTab[pos++] = 'i';
            htmlStringTab[pos++] = 'g';
            htmlStringTab[pos++] = ';';
            break;
          case 'ç':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'c';
            htmlStringTab[pos++] = 'c';
            htmlStringTab[pos++] = 'e';
            htmlStringTab[pos++] = 'd';
            htmlStringTab[pos++] = 'i';
            htmlStringTab[pos++] = 'l';
            htmlStringTab[pos++] = ';';
            break;
          case 'è':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'e';
            htmlStringTab[pos++] = 'g';
            htmlStringTab[pos++] = 'r';
            htmlStringTab[pos++] = 'a';
            htmlStringTab[pos++] = 'v';
            htmlStringTab[pos++] = 'e';
            htmlStringTab[pos++] = ';';
            break;
          case 'é':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'e';
            htmlStringTab[pos++] = 'a';
            htmlStringTab[pos++] = 'c';
            htmlStringTab[pos++] = 'u';
            htmlStringTab[pos++] = 't';
            htmlStringTab[pos++] = 'e';
            htmlStringTab[pos++] = ';';
            break;
          case 'ê':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'e';
            htmlStringTab[pos++] = 'c';
            htmlStringTab[pos++] = 'i';
            htmlStringTab[pos++] = 'r';
            htmlStringTab[pos++] = 'c';
            htmlStringTab[pos++] = ';';
            break;
          case 'ë':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'e';
            htmlStringTab[pos++] = 'u';
            htmlStringTab[pos++] = 'm';
            htmlStringTab[pos++] = 'l';
            htmlStringTab[pos++] = ';';
            break;
          case 'ì':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'i';
            htmlStringTab[pos++] = 'g';
            htmlStringTab[pos++] = 'r';
            htmlStringTab[pos++] = 'a';
            htmlStringTab[pos++] = 'v';
            htmlStringTab[pos++] = 'e';
            htmlStringTab[pos++] = ';';
            break;
          case 'í':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'i';
            htmlStringTab[pos++] = 'a';
            htmlStringTab[pos++] = 'c';
            htmlStringTab[pos++] = 'u';
            htmlStringTab[pos++] = 't';
            htmlStringTab[pos++] = 'e';
            htmlStringTab[pos++] = ';';
            break;
          case 'î':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'i';
            htmlStringTab[pos++] = 'c';
            htmlStringTab[pos++] = 'i';
            htmlStringTab[pos++] = 'r';
            htmlStringTab[pos++] = 'c';
            htmlStringTab[pos++] = ';';
            break;
          case 'ï':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'i';
            htmlStringTab[pos++] = 'u';
            htmlStringTab[pos++] = 'm';
            htmlStringTab[pos++] = 'l';
            htmlStringTab[pos++] = ';';
            break;
          case 'ð':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'e';
            htmlStringTab[pos++] = 't';
            htmlStringTab[pos++] = 'h';
            htmlStringTab[pos++] = ';';
            break;
          case 'ñ':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'n';
            htmlStringTab[pos++] = 't';
            htmlStringTab[pos++] = 'i';
            htmlStringTab[pos++] = 'l';
            htmlStringTab[pos++] = 'd';
            htmlStringTab[pos++] = 'e';
            htmlStringTab[pos++] = ';';
            break;
          case 'ò':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'o';
            htmlStringTab[pos++] = 'g';
            htmlStringTab[pos++] = 'r';
            htmlStringTab[pos++] = 'a';
            htmlStringTab[pos++] = 'v';
            htmlStringTab[pos++] = 'e';
            htmlStringTab[pos++] = ';';
            break;
          case 'ó':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'o';
            htmlStringTab[pos++] = 'a';
            htmlStringTab[pos++] = 'c';
            htmlStringTab[pos++] = 'u';
            htmlStringTab[pos++] = 't';
            htmlStringTab[pos++] = 'e';
            htmlStringTab[pos++] = ';';
            break;
          case 'ô':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'o';
            htmlStringTab[pos++] = 'c';
            htmlStringTab[pos++] = 'i';
            htmlStringTab[pos++] = 'r';
            htmlStringTab[pos++] = 'c';
            htmlStringTab[pos++] = ';';
            break;
          case 'õ':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'o';
            htmlStringTab[pos++] = 't';
            htmlStringTab[pos++] = 'i';
            htmlStringTab[pos++] = 'l';
            htmlStringTab[pos++] = 'd';
            htmlStringTab[pos++] = 'e';
            htmlStringTab[pos++] = ';';
            break;
          case 'ö':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'o';
            htmlStringTab[pos++] = 'u';
            htmlStringTab[pos++] = 'm';
            htmlStringTab[pos++] = 'l';
            htmlStringTab[pos++] = ';';
            break;
          case '÷':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'd';
            htmlStringTab[pos++] = 'i';
            htmlStringTab[pos++] = 'v';
            htmlStringTab[pos++] = 'i';
            htmlStringTab[pos++] = 'd';
            htmlStringTab[pos++] = 'e';
            htmlStringTab[pos++] = ';';
            break;
          case 'ø':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'o';
            htmlStringTab[pos++] = 's';
            htmlStringTab[pos++] = 'l';
            htmlStringTab[pos++] = 'a';
            htmlStringTab[pos++] = 's';
            htmlStringTab[pos++] = 'h';
            htmlStringTab[pos++] = ';';
            break;
          case 'ù':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'u';
            htmlStringTab[pos++] = 'g';
            htmlStringTab[pos++] = 'r';
            htmlStringTab[pos++] = 'a';
            htmlStringTab[pos++] = 'v';
            htmlStringTab[pos++] = 'e';
            htmlStringTab[pos++] = ';';
            break;
          case 'ú':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'u';
            htmlStringTab[pos++] = 'a';
            htmlStringTab[pos++] = 'c';
            htmlStringTab[pos++] = 'u';
            htmlStringTab[pos++] = 't';
            htmlStringTab[pos++] = 'e';
            htmlStringTab[pos++] = ';';
            break;
          case 'û':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'u';
            htmlStringTab[pos++] = 'c';
            htmlStringTab[pos++] = 'i';
            htmlStringTab[pos++] = 'r';
            htmlStringTab[pos++] = 'c';
            htmlStringTab[pos++] = ';';
            break;
          case 'ü':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'u';
            htmlStringTab[pos++] = 'u';
            htmlStringTab[pos++] = 'm';
            htmlStringTab[pos++] = 'l';
            htmlStringTab[pos++] = ';';
            break;
          case 'ý':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'y';
            htmlStringTab[pos++] = 'a';
            htmlStringTab[pos++] = 'c';
            htmlStringTab[pos++] = 'u';
            htmlStringTab[pos++] = 't';
            htmlStringTab[pos++] = 'e';
            htmlStringTab[pos++] = ';';
            break;
          case 'þ':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 't';
            htmlStringTab[pos++] = 'h';
            htmlStringTab[pos++] = 'o';
            htmlStringTab[pos++] = 'r';
            htmlStringTab[pos++] = 'n';
            htmlStringTab[pos++] = ';';
            break;
          case 'ÿ':
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'y';
            htmlStringTab[pos++] = 'u';
            htmlStringTab[pos++] = 'm';
            htmlStringTab[pos++] = 'l';
            htmlStringTab[pos++] = ';';
            break;
          case '\"':
            if (tagswandeln) {
              htmlStringTab[pos++] = '&';
              htmlStringTab[pos++] = 'q';
              htmlStringTab[pos++] = 'u';
              htmlStringTab[pos++] = 'o';
              htmlStringTab[pos++] = 't';
              htmlStringTab[pos++] = ';';
            } else {
              htmlStringTab[pos++] = javaStringTab[i];
            }
            break;
          case '&':
            if (tagswandeln) {
              htmlStringTab[pos++] = '&';
              htmlStringTab[pos++] = 'a';
              htmlStringTab[pos++] = 'm';
              htmlStringTab[pos++] = 'p';
              htmlStringTab[pos++] = ';';
            } else {
              htmlStringTab[pos++] = javaStringTab[i];
            }
            break;
          case '<':
            if (tagswandeln) {
              htmlStringTab[pos++] = '&';
              htmlStringTab[pos++] = 'l';
              htmlStringTab[pos++] = 't';
              htmlStringTab[pos++] = ';';
            } else {
              htmlStringTab[pos++] = javaStringTab[i];
            }
            break;
          case '>':
            if (tagswandeln) {
              htmlStringTab[pos++] = '&';
              htmlStringTab[pos++] = 'g';
              htmlStringTab[pos++] = 't';
              htmlStringTab[pos++] = ';';
            } else {
              htmlStringTab[pos++] = javaStringTab[i];
            }
            break;
          case ' ':
            if (blankwandeln) {
              htmlStringTab[pos++] = '&';
              htmlStringTab[pos++] = 'n';
              htmlStringTab[pos++] = 'b';
              htmlStringTab[pos++] = 's';
              htmlStringTab[pos++] = 'p';
              htmlStringTab[pos++] = ';';
            } else {
              htmlStringTab[pos++] = javaStringTab[i];
            }
            break;
          case 0xa0:
            htmlStringTab[pos++] = '&';
            htmlStringTab[pos++] = 'n';
            htmlStringTab[pos++] = 'b';
            htmlStringTab[pos++] = 's';
            htmlStringTab[pos++] = 'p';
            htmlStringTab[pos++] = ';';
            break;
          case '\r':
            break;
          case '\n':
            if (returnwandeln) {
              htmlStringTab[pos++] = '<';
              htmlStringTab[pos++] = 'b';
              htmlStringTab[pos++] = 'r';
              htmlStringTab[pos++] = '>';
            } else {
              htmlStringTab[pos++] = javaStringTab[i];
            }
            break;
          default:
            if (javaStringTab[i] > 255) {
              final String dummy = Integer.toString(javaStringTab[i]);
              htmlStringTab[pos++] = '&';
              htmlStringTab[pos++] = '#';
              for (int k = 0; k < dummy.length(); k++) {
                htmlStringTab[pos++] = dummy.charAt(k);
              }
              htmlStringTab[pos++] = ';';
            } else {
              htmlStringTab[pos++] = javaStringTab[i];
            }
            break;
        }
      }

      htmlString = new String(htmlStringTab, 0, pos);
    }

    return htmlString;
  }
}
