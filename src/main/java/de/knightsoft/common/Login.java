/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.common;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * The <code>Login</code> class is used for login and logout into, the KnightSoft-Net Application.
 *
 * @author Manfred Tremmel
 * @version 1.0.8, 06.08.2005
 */
public class Login {

  protected final Connection thisDatabase;
  protected final String pathPrefix;
  protected final String servletName;
  protected final String title;
  protected final String service;
  protected String hint;
  protected final String dbTable;
  protected final String dbUserField;
  protected final String dbPasswordField;
  protected final String dbSystemField;
  protected final String dbSystem;
  protected final String dbStep;
  protected final String dbOtherFields;
  protected final DataBaseDepending myDataBaseDepending;
  protected int step;


  protected static final int MAX_LOGIN = 5;

  /**
   * Constructor.
   *
   * @param servletName Name of the Servlet which uses the Login
   * @param ppathPrefix path prefix
   * @param title Title of the Page
   * @param service name of the service to log in
   * @param dbSystem name of the service in the database
   * @param database Database connection has to be established by the servlet
   */
  public Login(final String servletName, final String ppathPrefix, final String title, final String service,
      final String dbSystem, final Connection database) {
    this(servletName, ppathPrefix, title, service, dbSystem, database, "KnightSoft_Berechtigungen", "user", "passwort",
        "system", "stufe", StringUtils.EMPTY);
  }

  /**
   * Constructor.
   *
   * @param servletName Name of the Servlet which uses the Login
   * @param ppathPrefix path prefix
   * @param title Title of the Page
   * @param service name of the service to log in
   * @param dbSystem name of the service in the database
   * @param database Database connection has to be established by the servlet
   */
  public Login(final String servletName, final String ppathPrefix, final String title, final String service,
      final String dbSystem, final Connection database, final String dbTable, final String dbUserField,
      final String dbPasswordField, final String dbSystemField, final String dbStep, final String dbOtherFields) {
    pathPrefix = ppathPrefix;
    this.dbTable = dbTable;
    this.dbUserField = dbUserField;
    this.dbPasswordField = dbPasswordField;
    this.dbSystemField = dbSystemField;
    this.dbStep = dbStep;

    this.servletName = servletName;
    this.title = title;
    this.service = service;
    this.dbSystem = dbSystem;
    thisDatabase = database;
    try {
      myDataBaseDepending = new DataBaseDepending(thisDatabase.getMetaData().getDatabaseProductName());
    } catch (final SQLException e) {
      throw new RuntimeException(e.getMessage(), e);
    }

    hint = null;
    this.dbOtherFields = dbOtherFields;
  }

  /**
   * Output of the HTML formular for login.
   *
   * @param req HttpServletRequest from the servlet
   * @param res HttpServletRespons from the servlet
   * @param out ServletOutputStream, where the HTML page has to be put
   * @param user Username given in
   * @param password Password given in
   * @param passwordNew Password given in for changing the password
   * @param passwordRep Password repeated for changing the password
   * @exception IOException if writing the page doesn't work
   */
  protected void htmlFormular(final HttpServletRequest req, final HttpServletResponse res, final ServletOutputStream out,
      final String user, final String password, final String passwordNew, final String passwordRep) throws IOException {
    out.println("<!DOCTYPE html>");
    out.println("<html><!-- " + de.knightsoft.common.StringToHtml.convert(servletName) + " -->");
    out.println("    <head>");
    out.println("        <meta http-equiv=\"content-type\" " + "content=\"text/html; charset=UTF-8\">");
    out.println("        <meta name=\"MSSmartTagsPreventParsing\" content=\"TRUE\">");
    out.println("        <title>" + de.knightsoft.common.StringToHtml.convert(title) + "</title>");
    out.println("    </head>");
    out.println("    <body style=\"background: white; color: black\">");
    out.println("        <div style=\"text-align:center;\">");
    if (hint == null) {
      hint = title;
    }
    out.println("        <p>" + de.knightsoft.common.StringToHtml.convert(hint) + "</p>");
    out.println("        <p>");
    out.println("        <form action=\"" + res.encodeURL(pathPrefix + Constants.SERVLET_URL + servletName)
        + "\" method=\"POST\" enctype=\"application/x-www-form-urlencoded; charset=utf-8\" " + "accept-charset=\"utf-8\">");
    if (dbStep != null) {
      out.println("            <p><input type=\"hidden\" name=\"" + de.knightsoft.common.StringToHtml.convert(dbStep)
          + "\" value=\"1\"></p>");
    }
    out.println("            <table style=\"margin-left:auto; margin-right:auto;\" border=\"0\">");
    out.println("                <tr>");
    out.println("                    <td style=\"text-align:left;\">Name:</td>");
    if (user == null) {
      out.println("                    <td style=\"text-align:left;\">"
          + "<input type=\"Text\" name=\"User\" size=\"30\" maxlength=\"30\"></td>");
    } else {
      out.println("                    <td style=\"text-align:left;\">"
          + "<input type=\"Text\" name=\"User\" size=\"30\" maxlength=\"30\" value=\""
          + de.knightsoft.common.StringToHtml.convert(user) + "\"></td>");
    }
    out.println("                </tr>");
    out.println("                <tr>");
    out.println("                    <td style=\"text-align:left;\">Passwort:</td>");
    if (password == null) {
      out.println("                    <td style=\"text-align:left;\">"
          + "<input type=\"password\" name=\"Password\" size=\"30\" maxlength=\"30\"></td>");
    } else {
      out.println("                    <td style=\"text-align:left;\">"
          + "<input type=\"password\" name=\"Password\" size=\"30\" maxlength=\"30\" value=\""
          + de.knightsoft.common.StringToHtml.convert(password) + "\"></td>");
    }
    out.println("                </tr>");
    out.println("                <tr>");
    out.println("                    <td style=\"text-align:left;\">Passwort (neu):</td>");
    if (passwordNew == null) {
      out.println("                    <td style=\"text-align:left;\">"
          + "<input type=\"password\" name=\"Password_new\" size=\"30\" maxlength=\"30\"></td>");
    } else {
      out.println("                    <td style=\"text-align:left;\">"
          + "<input type=\"password\" name=\"Password_new\" size=\"30\" maxlength=\"30\" value=\""
          + de.knightsoft.common.StringToHtml.convert(passwordNew) + "\"></td>");
    }
    out.println("                </tr>");
    out.println("                <tr>");
    out.println("                    <td style=\"text-align:left;\">" + "Passwort (neu Best&auml;tigung):</td>");
    if (passwordRep == null) {
      out.println("                    <td style=\"text-align:left;\">"
          + "<input type=\"password\" name=\"Password_rep\" size=\"30\" maxlength=\"30\"></td>");
    } else {
      out.println("                    <td style=\"text-align:left;\">"
          + "<input type=\"password\" name=\"Password_rep\" size=\"30\" maxlength=\"30\" value=\""
          + de.knightsoft.common.StringToHtml.convert(passwordRep) + "\"></td>");
    }
    out.println("                </tr>");
    out.println("                <tr>");
    out.println(
        "                    <td colspan=\"2\" style=\"text-align:center;\">" + "<input type=submit value=\"Absenden\"></td>");
    out.println("                </tr>");
    out.println("            </table>");
    out.println("            <p>Zum &Auml;ndern des Passwortes, das neue Passwort bitte in die"
        + " Felder &raquo;Passwort (neu)&laquo; und &raquo;Passwort (neu Best&auml;tigung)&laquo;" + " eingeben.</p>");
    out.println("        </form>");
    out.println("        </div>");
    out.println("    </body>");
    out.println("</html>");
  }

  /**
   * The <code>passwortaenderung</code> class checks, if the password should be changed and if it's ok.
   *
   * @param ppassword the current given password
   * @param ppasswordNew Password given in for changing the password
   * @param ppasswordRep Password repeated for changing the password
   * @return true if password has changed
   * @exception de.knightsoft.common.TextException when a error in checking the passwords occours
   */
  protected String passwortaenderung(final String ppassword, final String ppasswordNew, final String ppasswordRep)
      throws de.knightsoft.common.TextException {

    String sqlString = " , Passwort=" + myDataBaseDepending.getSqlPassword(StringToSql.convert(ppassword)) + " ";;

    if (StringUtils.isNotEmpty(ppasswordNew) && StringUtils.isNotEmpty(ppasswordRep)) {
      if (ppasswordNew.equals(ppasswordRep)) {
        sqlString = " , Passwort=" + myDataBaseDepending.getSqlPassword(StringToSql.convert(ppasswordNew)) + " ";
        hint = "Das Passwort wurde entsprechend Ihrer Vorgaben geändert";
      } else {
        throw new de.knightsoft.common.TextException(
            "Die Felder »Passwort (neu)« und »Passwort (neu Bestätigung)« " + "wurden nicht identisch gefüllt!");
      }
    }

    return sqlString;
  }

  /**
   * The <code>logonok</code> class checks, if the logon is ok or not.
   *
   * @param result ResultSet of the SQL request
   * @param puser Username given in
   * @param ppassword Password given in
   * @param ppasswordNew Password given in for changing the password
   * @param ppasswordRep Password repeated for changing the password
   * @param psession http session to read set parameters
   * @return true if logon is ok
   * @exception de.knightsoft.common.TextException when a error in checking the passwords occours
   */
  protected boolean loginok(final ResultSet result, final String puser, final String ppassword, final String ppasswordNew,
      final String ppasswordRep, final HttpSession psession) throws de.knightsoft.common.TextException {
    final boolean isok = true;
    final StringBuffer sqlString = new StringBuffer(128);
    sqlString.append("UPDATE    ").append(dbTable).append(' ') //
        .append("SET        datum_login=NOW(),") //
        .append("           anzahl_fehllogin=0,") //
        .append("           datum_fehllogin=NULL") //
        .append(passwortaenderung(ppassword, ppasswordNew, ppasswordRep));

    if (dbSystemField == null) {
      sqlString.append("WHERE    ").append(dbUserField).append('=').append(de.knightsoft.common.StringToSql.convert(puser));
    } else {
      sqlString.append("WHERE    ").append(dbSystemField).append('=').append(de.knightsoft.common.StringToSql.convert(dbSystem))
          .append(" AND    ").append(dbUserField).append('=').append(de.knightsoft.common.StringToSql.convert(puser));
    }
    try {
      thisDatabase.createStatement().executeUpdate(sqlString.toString());
    } catch (final java.sql.SQLException e) {
      throw new de.knightsoft.common.TextException(
          "Fehler beim Datenbankzugriff: " + e.toString() + "\n\nSQL-String:\n" + sqlString, e);
    }

    return isok;
  }

  /**
   * The <code>process</code> class checks, if the user is already logged on and if he isn't, it checks the given parameter.
   *
   * @param req HttpServletRequest from the servlet
   * @param res HttpServletRespons from the servlet
   * @param out ServletOutputStream for HTML Output
   * @return true if logon is ok or user is allready logged on
   * @exception IOException when writing the html-page fails
   */
  public boolean process(final HttpServletRequest req, final HttpServletResponse res, final ServletOutputStream out,
      final HttpSession session) throws IOException {
    int posI = 0;
    int posJ = 0;
    final String user = req.getParameter("User");
    final String password = checkPassword(req.getParameter("Password"));
    final String passwordNew = req.getParameter("Password_new");
    final String passwordRep = req.getParameter("Password_rep");
    final StringBuffer sqlString = new StringBuffer(512);
    boolean loginOk = false;

    hint = null;
    step = 0;

    try {
      if (StringUtils.isEmpty(user) || StringUtils.isEmpty(password)) {
        throw new de.knightsoft.common.TextException("Bitte geben Sie Ihren Benutzernamen und Ihr Passwort ein");
      }

      sqlString.append("SELECT    IF (") //
          .append(myDataBaseDepending.getSqlPassword(StringToSql.convert(password))) //
          .append(" = ").append(dbPasswordField).append(" OR ") //
          .append(myDataBaseDepending.getSqlOldPassword(StringToSql.convert(password))) //
          .append(" = ").append(dbPasswordField).append(',') //
          .append("                    'J',") //
          .append("                    'N'") //
          .append("                ) AS berechtigt, ") //
          .append("            IF (    TO_DAYS(NOW()) - TO_DAYS(datum_fehllogin) >= 1,") //
          .append("                    'J',") //
          .append("                    'N'") //
          .append("                ) AS sperrfrist_abgelaufen, ") //
          .append("            anzahl_fehllogin");
      if (dbOtherFields != null) {
        sqlString.append(dbOtherFields);
      }
      if (dbStep != null) {
        sqlString.append(", ").append(dbStep);
      }
      sqlString.append(" FROM        ").append(dbTable).append(' ');
      if (dbSystemField == null) {
        sqlString.append("WHERE    ").append(dbUserField).append('=').append(de.knightsoft.common.StringToSql.convert(user));
      } else {
        sqlString.append("WHERE    ").append(dbSystemField).append('=')
            .append(de.knightsoft.common.StringToSql.convert(dbSystem)).append(" AND        ").append(dbUserField).append('=')
            .append(de.knightsoft.common.StringToSql.convert(user));
      }

      try (final ResultSet result = thisDatabase.createStatement().executeQuery(sqlString.toString())) {

        while (!loginOk && result.next()) {
          posJ++;
          posI = result.getInt("anzahl_fehllogin");
          if (posI >= Login.MAX_LOGIN) {
            if (result.getString("sperrfrist_abgelaufen").equals("N")) {
              throw new de.knightsoft.common.TextException(
                  "Es gab heute bereits " + Login.MAX_LOGIN + " Fehlversuche, versuchen Sie es morgen wieder");
            } else {
              posI = 0;
            }
          }

          if (result.getString("berechtigt").equals("J")) {
            step = result.getInt(dbStep);
            loginOk = loginok(result, user, password, passwordNew, passwordRep, session);
            if (loginOk) {
              session.setAttribute(servletName + Constants.DB_FIELD_GLOBAL_MANDATOR, Integer.valueOf(1));
              session.setAttribute(servletName + Constants.DB_FIELD_GLOBAL_USER + Constants.DB_FIELD_GLOBAL_USER, user);
            }

          }
        }
      }
      if (posJ == 0) {
        throw new de.knightsoft.common.TextException("Die eingegebene Benutzerkennung existiert nicht");
      } else if (!loginOk) {
        posI++;

        sqlString.delete(0, sqlString.length());
        sqlString.append("UPDATE    ").append(dbTable).append(' ') //
            .append("SET        anzahl_fehllogin=").append(posI).append(',') //
            .append("            datum_fehllogin=NOW() ");
        if (dbSystemField == null) {
          sqlString.append("WHERE    ").append(dbUserField).append('=').append(de.knightsoft.common.StringToSql.convert(user));
        } else {
          sqlString.append("WHERE    ").append(dbSystemField).append('=')
              .append(de.knightsoft.common.StringToSql.convert(dbSystem)).append(" AND        ").append(dbUserField).append('=')
              .append(de.knightsoft.common.StringToSql.convert(user));
        }

        thisDatabase.createStatement().executeUpdate(sqlString.toString());

        if (posI >= Login.MAX_LOGIN) {
          throw new de.knightsoft.common.TextException(
              "Dies war Ihr " + posI + " Fehlversuch, der Zugang wird bis morgen gesperrt!");
        } else {
          throw new de.knightsoft.common.TextException("Dies war Ihr " + posI + " Fehlversuch, nach " + Login.MAX_LOGIN
              + " wird der Zugang für heute gesperrt, Sie haben noch " + (Login.MAX_LOGIN - posI) + " Versuche");
        }
      }
    } catch (final java.sql.SQLException e) {
      hint = "Fehler beim Datenbankzugriff: " + e.toString() + "\n\nSQL-String:\n" + sqlString;
      htmlFormular(req, res, out, user, password, passwordNew, passwordRep);
    } catch (final de.knightsoft.common.TextException e) {
      hint = e.toString();
      htmlFormular(req, res, out, user, password, passwordNew, passwordRep);
    }
    return loginOk;
  }

  /**
   * The <code>processLogout</code> class logs out the system.
   *
   * @param req HttpServletRequest from the servlet
   * @param res HttpServletRespons from the servlet
   * @param out ServletOutputStream for HTML Output
   * @exception IOException when writing the html-page fails
   */
  public void processLogout(final HttpServletRequest req, final HttpServletResponse res, final ServletOutputStream out)
      throws IOException {
    // Session entwerten
    final HttpSession session = req.getSession(false);
    if (session != null) {
      session.invalidate();
    }

    // Jungfräuliches Login-Fenster auf den Schirm
    htmlFormular(req, res, out, null, null, null, null);
  }

  /**
   * The <code>checkPassword</code> method checks the password.
   *
   * @return password
   */
  public String checkPassword(final String pwd) {
    return pwd;
  }

  /**
   * The <code>getstufe</code> method gives back the loginlevel.
   *
   * @return loginlevel
   */
  public int getStep() {
    return step;
  }

  /**
   * The <code>getHint</code> method gives back the Adwise field.
   *
   * @return Adwise
   */
  public String getHint() {
    return hint;
  }
}
