/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.common;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * The <code>Constants</code> class contains some static settings and methodes
 *
 * @author Manfred Tremmel
 * @version 1.0.9, 24.02.2013
 */
public class Constants {

  public static final String COMPANY = "KnightSoft";

  public static final String JDBC_CLASS_MYSQL = "com.mysql.cj.jdbc.Driver";
  public static final String JDBC_CLASS_MYSQL_OLD = "com.mysql.jdbc.Driver";
  public static final String JDBC_CLASS_MYSQL_OLD2 = "org.gjt.mm.mysql.Driver";
  public static final String JDBC_CLASS_MARIADB = "org.mariadb.jdbc.Driver";
  public static final String JDBC_CLASS_MSSQL = "com.microsoft.jdbc.sqlserver.SQLServerDriver";

  // Servlet-Engine-Version
  public static final double SERVLET_ENGINE = 2.0;

  // public static final String HTML_BASE = "../";
  public static final String HTML_BASE = "/";
  public static final String HTML_URL = Constants.HTML_BASE;
  public static final String JAVA_SCRIPT_URL = "js/";
  public static final String CSS_URL = "css/";
  public static final String GIF_URL = "pics/";
  public static final String SERVLET_URL = "servlet/";
  public static final String EMAIL = Constants.COMPANY + "-net@" + Constants.COMPANY + "-net.de";

  public static final String ORGANISATION = "Manfred Tremmel";
  public static final String COPYRIGHT = "© 2002-2020 by " + Constants.ORGANISATION;

  public static final String HOME_PAGE = "http://www." + Constants.COMPANY + ".de";

  public static final String DB_FIELD_GLOBAL_MANDATOR = "Mandator";
  public static final String DB_FIELD_GLOBAL_USER = "Username";
  public static final String DB_FIELD_GLOBAL_DATE_FROM = "Date_from";
  public static final String DB_FIELD_GLOBAL_DATE_TO = "Date_to";

  /**
   * generate password.
   *
   * @param pserviceId id of the service
   * @return password as string.
   */
  public static final String generatePassword(final String pserviceId) {
    final char[] charArray = new char[10];
    final char[] charsArray = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
        't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G',
        'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};

    for (byte i = 0; i < charArray.length; charArray[i++] =
        charsArray[(byte) (java.lang.Math.random() * (charsArray.length - 1))]) {
      ;
    }

    return pserviceId + new String(charArray);
  }

  public static final String htmlStart(final String servletName, final String pageTitle, final String additionalData,
      final boolean nocache) {
    return Constants.htmlStart(servletName, pageTitle, additionalData, nocache, "default.css");
  }

  /**
   * generate first part of html file.
   *
   * @param servletName name of the servlet
   * @param pageTitle page title
   * @param additionalData additional data
   * @param nocache cache/don't cache the page
   * @param cssName css name
   * @return html code
   */
  public static final String htmlStart(final String servletName, final String pageTitle, final String additionalData,
      final boolean nocache, final String cssName) {
    final Date today = new Date();
    final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss z", Locale.GERMANY);

    final StringBuilder sb = new StringBuilder(1024);
    sb.append("<!DOCTYPE html>\n" //
        + "<html><!-- " + servletName + " -->\n" + "    <head>\n"
        + "        <meta http-equiv=\"Content-Type\" content=\"text/html; " + "charset=UTF-8\">\n"
        + "        <meta http-equiv=\"Content-Style-Type\" content=\"text/css\">\n"
        + "        <meta http-equiv=\"Content-Script-Type\" content=\"text/javascript\">\n"
        + "        <meta http-equiv=\"Content-Language\" content=\"de\">\n");
    if (nocache) {
      sb.append("        <meta http-equiv=\"expires\" content=\"0\">\n"
          + "        <meta http-equiv=\"pragma\" content=\"no-cache\">\n"
          + "        <meta name=\"robots\" content=\"noindex\">\n");
    }
    sb.append(
        "        <meta name=\"MSSmartTagsPreventParsing\" content=\"TRUE\">\n" + "        <meta name=\"description\" content=\""
            + pageTitle + "\">\n" + "        <meta name=\"date\" content=\"" + formatter.format(today) + "\">\n"
            + "        <title>" + pageTitle + "</title>\n" + "        <link rel=\"stylesheet\" type=\"text/css\" href=\""
            + Constants.HTML_BASE + Constants.CSS_URL + cssName + "\">\n");

    if (additionalData != null) {
      sb.append(additionalData);
    }

    sb.append("    </head>\n" //
        + "    <body>\n");
    return sb.toString();
  }
}
