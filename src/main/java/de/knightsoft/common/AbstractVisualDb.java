/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.common;

import de.knightsoft.common.field.AbstractBaseField;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * The <code>AbstractVisualDb</code> class is a abstract class for all Web Based pages
 *
 * @author Manfred Tremmel
 * @version 2.2.2, 08.01.2011
 */

public abstract class AbstractVisualDb { // NOPMD

  protected final String pathPrefix;

  protected final String servletName;

  protected final String pictureName;
  protected final String pictureNameHiDpi;

  protected final String serviceName;

  protected final String serviceNameLocal;

  protected final String javaScript;

  protected final boolean useDateSelect;
  protected final boolean useUser;
  protected final boolean positionUseDateSelect;
  protected final boolean positionUseUser;

  protected final String otherOptions;

  protected final String dataBaseTable;
  protected final de.knightsoft.common.field.AbstractBaseField[] dbFields;
  protected final int numberKeyField;
  protected final String keyField;

  protected final String positionDataBaseTable;
  protected final de.knightsoft.common.field.AbstractBaseField[] positionDbFields;
  protected final int positionNumberKeyField;
  protected final int positionNumberReferenceField;
  protected final boolean positionAddRemove;
  protected final boolean positionUpDown;

  protected final Connection myDataBase;

  protected final DataBaseDepending myDataBaseDepending;

  protected static final String DBMIN_TEXT = "dbmin_";
  protected static final String DBMAX_TEXT = "dbmax_";
  protected static final String HTML_OLD = "OLD_";
  protected static final String DEBUG_TEXT_TEXT = "Debug_Text_";
  protected static final String SELECTED_FIELD_TEXT = "SelecteField_";
  protected static final String REQ_SAVE_TEXT = "reqsave_";
  protected static final String NUMBER_POS_TEXT = "number_positions_";

  protected final String emptyButtonHtml;
  protected final String netButtonHtml;
  protected final String saveButtonHtml;
  protected final String deleteButtonHtml;
  protected final String delete2ButtonHtml;
  protected final String searchButtonHtml;
  protected final String resetButtonHtml;
  protected final String fbackButtonHtml;
  protected final String backButtonHtml;
  protected final String switchButtonHtml;
  protected final String forwardButtonHtml;
  protected final String fforwardButtonHtml;

  protected String readMinMaxSql;
  protected String readNextSql;
  protected String readPrevSql;
  protected String readEntrySql;
  protected String readEntryCountSql;
  protected String updateInvalidateEntrySql;
  protected String insertEntrySql;
  protected String updateEntrySql;
  protected String deleteEntrySql;

  protected String readPosCountSql;
  protected String readAllPosSql;
  protected String updateInvalidateallPositionsSql;
  protected String updateInvalidatePositionSql;
  protected String deletePositionSql;
  protected String insertPositionSql;
  protected String updatePositionSql;
  protected String readPositionSql;

  /**
   * Constructor, for applications with no DB access.
   *
   */
  public AbstractVisualDb() throws SQLException, TextException {
    this(null, Constants.HTML_BASE, null, null, null, null, null, null, 0, null, null, null, 0, 0, false, false);
  }

  /**
   * Constructor, for applications with DB access and individual HTML.
   *
   * @param pdatabase Connection to database
   * @param ppathPrefix path prefix
   * @param servletName Name of the Servlet
   * @param pictureName Name of the Picture in the Menu
   * @param serviceName Name of the Service
   * @param serviceNameLocal Name of the Service Localized
   * @exception SQLException if a database access error occurs
   * @exception TextException to give out a text message
   * @see TextException
   */
  public AbstractVisualDb(final Connection pdatabase, final String ppathPrefix, final String servletName,
      final String pictureName, final String serviceName, final String serviceNameLocal)
      throws java.sql.SQLException, TextException {
    this(pdatabase, ppathPrefix, servletName, pictureName, serviceName, serviceNameLocal, null, null, 0, null, null, null, 0, 0,
        false, false);
  }

  /**
   * Constructor, for applications with DB access and standard HTML
   *
   * @param pdatabase Connection to database
   * @param ppathPrefix path prefix
   * @param servletName Name of the Servlet
   * @param pictureName Name of the Picture in the Menu
   * @param serviceName Name of the Service
   * @param serviceNameLocal Name of the Service localized
   * @param dataBaseTable Name of the Database table
   * @param dbFields Field descriptions of the database
   * @param numberKeyField Number of the KeyField in the DBFields_u array
   * @exception SQLException if a database access error occurs
   * @exception TextException to give out a text message
   * @see Connection
   * @see TextException
   * @see de.knightsoft.common.field.AbstractBaseField
   */
  public AbstractVisualDb(final Connection pdatabase, final String ppathPrefix, final String servletName,
      final String pictureName, final String serviceName, final String serviceNameLocal, final String dataBaseTable,
      final de.knightsoft.common.field.AbstractBaseField[] dbFields, final int numberKeyField)
      throws java.sql.SQLException, TextException {
    this(pdatabase, ppathPrefix, servletName, pictureName, serviceName, serviceNameLocal, dataBaseTable, dbFields,
        numberKeyField, null, null, null, 0, 0, false, false);
  }

  /**
   * Constructor, for applications with DB access and standard HTML and one additional function
   *
   * @param pdatabase Connection to database
   * @param ppathPrefix path prefix
   * @param servletName Name of the Servlet
   * @param pictureName Name of the Picture in the Menu
   * @param serviceName Name of the Service
   * @param serviceNameLocal Name of the Service localized
   * @param dataBaseTable Name of the Database table
   * @param dbFields Field descriptions of the database
   * @param numberKeyField Number of the KeyField in the DBFields_u array
   * @param otherOptions Name of a additional Button
   * @exception SQLException if a database access error occurs
   * @exception TextException to give out a text message
   * @see Connection
   * @see TextException
   * @see de.knightsoft.common.field.AbstractBaseField
   */
  public AbstractVisualDb(final Connection pdatabase, final String ppathPrefix, final String servletName,
      final String pictureName, final String serviceName, final String serviceNameLocal, final String dataBaseTable,
      final de.knightsoft.common.field.AbstractBaseField[] dbFields, final int numberKeyField, final String otherOptions)
      throws java.sql.SQLException, TextException {
    this(pdatabase, ppathPrefix, servletName, pictureName, serviceName, serviceNameLocal, dataBaseTable, dbFields,
        numberKeyField, otherOptions, null, null, 0, 0, false, false);
  }

  /**
   * Constructor, for applications with DB access and standard HTML and one additional function
   *
   * @param pdatabase Connection to database
   * @param ppathPrefix path prefix
   * @param servletName Name of the Servlet
   * @param pictureName Name of the Picture in the Menu
   * @param serviceName Name of the Service
   * @param serviceNameLocal Name of the Service localized
   * @param dataBaseTable Name of the Database table
   * @param dbFields Field descriptions of the database
   * @param numberKeyField Number of the KeyField in the DBFields_u array
   * @param otherOptions Name of a additional Button
   * @param positionDataBaseTable Name of the database table for position entries
   * @param positionDbFields Field descriptions of the position database
   * @param positionNumberKeyField where in the table can we find the key field
   * @param positionNumberReferenceField where in the table can we find the reference field
   * @param positionAddRemove are we allowed to add or remove new positions
   * @param positionUpDown are we allowed to change positions of the positions
   * @exception SQLException if a database access error occurs
   * @exception TextException to give out a text message
   * @see Connection
   * @see TextException
   * @see de.knightsoft.common.field.AbstractBaseField
   */
  public AbstractVisualDb(final Connection pdatabase, final String ppathPrefix, final String servletName,
      final String pictureName, final String serviceName, final String serviceNameLocal, final String dataBaseTable,
      final de.knightsoft.common.field.AbstractBaseField[] dbFields, final int numberKeyField, final String otherOptions,
      final String positionDataBaseTable, final de.knightsoft.common.field.AbstractBaseField[] positionDbFields,
      final int positionNumberKeyField, final int positionNumberReferenceField, final boolean positionAddRemove,
      final boolean positionUpDown) throws java.sql.SQLException, TextException {
    this(pdatabase, ppathPrefix, servletName, pictureName, StringUtils.replace(pictureName, "16x16", "32x32"), serviceName,
        serviceNameLocal, dataBaseTable, dbFields, numberKeyField, otherOptions, positionDataBaseTable, positionDbFields,
        positionNumberKeyField, positionNumberReferenceField, positionAddRemove, positionUpDown);
  }

  /**
   * Constructor, for applications with DB access and standard HTML and one additional function
   *
   * @param pdatabase Connection to database
   * @param ppathPrefix path prefix
   * @param servletName Name of the Servlet
   * @param pictureName Name of the Picture in the Menu
   * @param pictureNameHiDpi Name of the Picture in the Menu for HiDPI displays
   * @param serviceName Name of the Service
   * @param serviceNameLocal Name of the Service localized
   * @param dataBaseTable Name of the Database table
   * @param dbFields Field descriptions of the database
   * @param numberKeyField Number of the KeyField in the DBFields_u array
   * @param otherOptions Name of a additional Button
   * @param positionDataBaseTable Name of the database table for position entries
   * @param positionDbFields Field descriptions of the position database
   * @param positionNumberKeyField where in the table can we find the key field
   * @param positionNumberReferenceField where in the table can we find the reference field
   * @param positionAddRemove are we allowed to add or remove new positions
   * @param positionUpDown are we allowed to change positions of the positions
   * @exception SQLException if a database access error occurs
   * @exception TextException to give out a text message
   * @see Connection
   * @see TextException
   * @see de.knightsoft.common.field.AbstractBaseField
   */
  public AbstractVisualDb(final Connection pdatabase, final String ppathPrefix, final String servletName,
      final String pictureName, final String pictureNameHiDpi, final String serviceName, final String serviceNameLocal,
      final String dataBaseTable, final de.knightsoft.common.field.AbstractBaseField[] dbFields, final int numberKeyField,
      final String otherOptions, final String positionDataBaseTable,
      final de.knightsoft.common.field.AbstractBaseField[] positionDbFields, final int positionNumberKeyField,
      final int positionNumberReferenceField, final boolean positionAddRemove, final boolean positionUpDown)
      throws java.sql.SQLException, TextException {
    if (pdatabase == null) {
      throw new RuntimeException("pdatabase can not be null.");
    }
    myDataBase = pdatabase;
    pathPrefix = ppathPrefix;
    this.servletName = servletName;
    this.pictureName = pictureName;
    this.pictureNameHiDpi = pictureNameHiDpi;
    this.serviceName = serviceName;
    this.serviceNameLocal = serviceNameLocal;
    this.dataBaseTable = dataBaseTable;
    this.dbFields = dbFields;
    this.numberKeyField = numberKeyField;
    this.otherOptions = otherOptions;
    this.positionDataBaseTable = positionDataBaseTable;
    this.positionDbFields = positionDbFields;
    this.positionNumberKeyField = positionNumberKeyField;
    this.positionNumberReferenceField = positionNumberReferenceField;
    this.positionAddRemove = positionAddRemove;
    this.positionUpDown = positionUpDown;

    emptyButtonHtml = "                    <td class=\"topnav\"><img src=\"" + pathPrefix + Constants.GIF_URL
        + "32x32/empty.png\" width=\"32\" height=\"32\" alt=\"\"></td>\n";
    netButtonHtml = "                    <td class=\"topnav\"><button type=\"submit\" name=\"navsubmittype\""
        + " value=\"new\" accesskey=\"*\" tabindex=\"30016\" title=\"neuer Eintrag\""
        + " onClick=\"document.forms[0].buttonpress.value='new';" + "document.forms[0].doformcheck.value='false';\"><img src=\""
        + pathPrefix + Constants.GIF_URL + "32x32/filenew.png\" srcset=\"" + pathPrefix + Constants.GIF_URL
        + "64x64/filenew.png 2x\" width=\"32\" height=\"32\" alt=\"neuer Eintrag\""
        + " title=\"neuer Eintrag\"></button></td>\n";
    saveButtonHtml = "                    <td class=\"topnav\"><button type=\"submit\" name=\"navsubmittype\""
        + " value=\"save\" accesskey=\"a\" tabindex=\"30000\" title=\"Speichern\""
        + " onClick=\"document.forms[0].buttonpress.value='save';" + "document.forms[0].doformcheck.value='true';\"><img src=\""
        + pathPrefix + Constants.GIF_URL + "32x32/filesave.png\" srcset=\"" + pathPrefix + Constants.GIF_URL
        + "64x64/filesave.png 2x\" width=\"32\" height=\"32\" alt=\"Speichern\" " + "title=\"Speichern\"></button></td>\n";
    deleteButtonHtml = "                    <td class=\"topnav\"><button type=\"submit\" "
        + "name=\"navsubmittype\" value=\"delete\" accesskey=\"l\" tabindex=\"30001\" "
        + "onClick=\"document.forms[0].buttonpress.value='delete';"
        + "document.forms[0].doformcheck.value='false';\" title=\"L&ouml;schen\"><img src=\"" + pathPrefix + Constants.GIF_URL
        + "32x32/editdelete.png\" srcset=\"" + pathPrefix + Constants.GIF_URL + "64x64/editdelete.png 2x\""
        + " width=\"32\" height=\"32\" alt=\"L&ouml;schen\" " + "title=\"L&ouml;schen\"></button></td>\n";
    delete2ButtonHtml = "                    <td class=\"topnav\"><button type=\"submit\" name=\"navsubmittype\""
        + " value=\"delete confirm\" accesskey=\"l\" tabindex=\"30001\" "
        + "onClick=\"document.forms[0].buttonpress.value='delete confirm';" + "document.forms[0].doformcheck.value='false';\" "
        + "title=\"L&ouml;schen best&auml;tigen\">" + "<img src=\"" + pathPrefix + Constants.GIF_URL
        + "32x32/editdelete.png\" srcset=\"" + pathPrefix + Constants.GIF_URL + "64x64/editdelete.png 2x\""
        + " width=\"32\" height=\"32\" " + "alt=\"L&ouml;schen best&auml;tigen\" title=\"L&ouml;schen best&auml;tigen\">"
        + "</button></td>\n";
    searchButtonHtml = "                    <td class=\"topnav\"><button type=\"submit\" name=\"navsubmittype\""
        + " value=\"search\" accesskey=\"s\" tabindex=\"30003\"" + " onClick=\"document.forms[0].buttonpress.value='search';"
        + "document.forms[0].doformcheck.value='false';\" title=\"Suchen\"><img src=\"" + pathPrefix + Constants.GIF_URL
        + "32x32/find.png\" srcset=\"" + pathPrefix + Constants.GIF_URL + "64x64/find.png 2x\""
        + " width=\"32\" height=\"32\" alt=\"Suchen\" title=\"Suchen\">" + "</button></td>\n";
    resetButtonHtml = "                    <td class=\"topnav\"><button type=\"reset\" "
        + "value=\"Formular zur&uuml;cksetzen\" accesskey=\"z\" tabindex=\"30002\"" + " title=\"Zur&uuml;cksetzen\"><img src=\""
        + pathPrefix + Constants.GIF_URL + "32x32/stop.png\" srcset=\"" + pathPrefix + Constants.GIF_URL + "64x64/stop.png 2x\""
        + " width=\"32\" height=\"32\" alt=\"Zur&uuml;cksetzen\"" + " title=\"Zur&uuml;cksetzen\"></button></td>\n";
    fbackButtonHtml = "                    <td class=\"topnav\"><button type=\"submit\" name=\"navsubmittype\""
        + " value=\"fback\" accesskey=\"1\" tabindex=\"30010\" " + "onClick=\"document.forms[0].buttonpress.value='fback';"
        + "document.forms[0].doformcheck.value='false';\" title=\"springe zum 1. Satz\">" + "<img src=\"" + pathPrefix
        + Constants.GIF_URL + "32x32/fback.png\" srcset=\"" + pathPrefix + Constants.GIF_URL + "64x64/fback.png 2x\""
        + " width=\"32\" height=\"32\" alt=\"springe zum 1. Satz\"" + " title=\"springe zum 1. Satz\"></button></td>\n";
    backButtonHtml = "                    <td class=\"topnav\"><button type=\"submit\" "
        + "name=\"navsubmittype\" value=\"back\" accesskey=\"<\" tabindex=\"30011\" "
        + "onClick=\"document.forms[0].buttonpress.value='back';"
        + "document.forms[0].doformcheck.value='false';\" title=\"Ein Satz zur&uuml;ck\">" + "<img src=\"" + pathPrefix
        + Constants.GIF_URL + "32x32/back.png\" srcset=\"" + pathPrefix + Constants.GIF_URL + "64x64/back.png 2x\""
        + " width=\"32\" height=\"32\" alt=\"Ein Satz zur&uuml;ck\"" + " title=\"Ein Satz zur&uuml;ck\"></button></td>\n";
    switchButtonHtml = "                    <td class=\"topnav\"><button type=\"submit\" name=\"navsubmittype\""
        + " value=\"goto\" accesskey=\"g\" tabindex=\"30013\"" + " onClick=\"document.forms[0].buttonpress.value='goto';"
        + "document.forms[0].doformcheck.value='false';\" " + "title=\"springe zum eigegebenen Satz\">" + "<img src=\""
        + pathPrefix + Constants.GIF_URL + "32x32/button_ok.png\" srcset=\"" + pathPrefix + Constants.GIF_URL
        + "64x64/button_ok.png 2x\"" + " width=\"32\" height=\"32\" " + "alt=\"springe zum eigegebenen Satz\""
        + " title=\"springe zum eigegebenen Satz\"></button></td>\n";
    forwardButtonHtml = "                    <td class=\"topnav\"><button type=\"submit\" name=\"navsubmittype\""
        + " value=\"next\" accesskey=\">\" tabindex=\"30014\"" + " onClick=\"document.forms[0].buttonpress.value='next';"
        + "document.forms[0].doformcheck.value='false';\" title=\"Ein Satz vorw&auml;rts\">" + "<img src=\"" + pathPrefix
        + Constants.GIF_URL + "32x32/forward.png\" srcset=\"" + pathPrefix + Constants.GIF_URL + "64x64/forward.png 2x\""
        + " width=\"32\" height=\"32\" alt=\"Ein Satz vorw&auml;rts\" " + "title=\"Ein Satz vorw&auml;rts\"></button></td>\n";
    fforwardButtonHtml = "                    <td class=\"topnav\"><button type=\"submit\" name=\"navsubmittype\" "
        + "value=\"fnext\" accesskey=\"9\" tabindex=\"30015\" " + "onClick=\"document.forms[0].buttonpress.value='fnext';"
        + "document.forms[0].doformcheck.value='false';\" title=\"Springe zum letzten Satz\">" + "<img src=\"" + pathPrefix
        + Constants.GIF_URL + "32x32/fforward.png\" srcset=\"" + pathPrefix + Constants.GIF_URL + "64x64/fforward.png 2x\""
        + " width=\"32\" height=\"32\" alt=\"Springe zum letzten Satz\" "
        + "title=\"Springe zum letzten Satz\"></button></td>\n";

    keyField = this.dbFields == null || this.dbFields.length <= this.numberKeyField ? null
        : this.dbFields[this.numberKeyField].getdbfieldname();

    boolean tmpUseDateSelect = false;
    boolean tmpUseUser = false;
    for (int i = 0; i < (this.dbFields == null ? 0 : this.dbFields.length); i++) {
      this.dbFields[i].setNewServletname(servletName);
      this.dbFields[i].setNewDataBase(pdatabase);
      if (Constants.DB_FIELD_GLOBAL_DATE_FROM.equals(this.dbFields[i].getdbfieldname())) {
        tmpUseDateSelect = true;
      } else if (Constants.DB_FIELD_GLOBAL_USER.equals(this.dbFields[i].getdbfieldname())) {
        tmpUseUser = true;
      }
    }
    useDateSelect = tmpUseDateSelect;
    useUser = tmpUseUser;
    boolean tmpPosUseDateSelect = false;
    boolean tmpPosUseUser = false;
    for (int i = 0; i < (this.positionDbFields == null ? 0 : this.positionDbFields.length); i++) {
      this.positionDbFields[i].setNewServletname(servletName);
      this.positionDbFields[i].setNewDataBase(pdatabase);
      if (Constants.DB_FIELD_GLOBAL_DATE_FROM.equals(this.positionDbFields[i].getdbfieldname())) {
        tmpPosUseDateSelect = true;
      } else if (Constants.DB_FIELD_GLOBAL_USER.equals(this.positionDbFields[i].getdbfieldname())) {
        tmpPosUseUser = true;
      }
    }
    positionUseDateSelect = tmpPosUseDateSelect;
    positionUseUser = tmpPosUseUser;

    javaScript = initJavaScript(); // NOPMD

    try {
      myDataBaseDepending = new DataBaseDepending(myDataBase.getMetaData().getDatabaseProductName());
    } catch (final Exception e) {
      throw new TextException(e.toString(), e);
    }

    if (this.dataBaseTable != null) {
      if (this.dbFields != null && this.dbFields.length > 0) {

        // Create prepared statments
        // Find Min and Max Entry of the DataBase
        readMinMaxSql = "SELECT MIN(" + keyField + ") AS dbmin, " //
            + "       MAX(" + keyField + ") AS dbmax " //
            + "FROM   " + this.dataBaseTable + " " //
            + "WHERE " + Constants.DB_FIELD_GLOBAL_MANDATOR + " = ? ";
        if (useDateSelect) {
          readMinMaxSql += " AND   " + Constants.DB_FIELD_GLOBAL_DATE_FROM + " <= " + myDataBaseDepending.getSqlTimeNow() + " " //
              + " AND   " + Constants.DB_FIELD_GLOBAL_DATE_TO + "   >= " + myDataBaseDepending.getSqlTimeNow();
        }

        // Read Next entry in DataBase
        readNextSql = "SELECT MIN(" + keyField + ") AS dbnumber " //
            + "FROM   " + this.dataBaseTable + " " //
            + "WHERE  " + Constants.DB_FIELD_GLOBAL_MANDATOR + " = ? " //
            + " AND   " + keyField + " > ? ";
        if (useDateSelect) {
          readNextSql += " AND   " + Constants.DB_FIELD_GLOBAL_DATE_FROM + " <= " + myDataBaseDepending.getSqlTimeNow() + " " //
              + " AND   " + Constants.DB_FIELD_GLOBAL_DATE_TO + "   >= " + myDataBaseDepending.getSqlTimeNow();
        }

        // Read Previous entry in DataBase
        readPrevSql = "SELECT MAX(" + keyField + ") AS dbnumber " + "FROM   " + this.dataBaseTable + " " + "WHERE  "
            + Constants.DB_FIELD_GLOBAL_MANDATOR + " = ? " + " AND   " + keyField + " < ? ";
        if (useDateSelect) {
          readPrevSql += " AND   " + Constants.DB_FIELD_GLOBAL_DATE_FROM + " <= " + myDataBaseDepending.getSqlTimeNow() + " "
              + " AND   " + Constants.DB_FIELD_GLOBAL_DATE_TO + "   >= " + myDataBaseDepending.getSqlTimeNow();
        }

        // Read entry from DataBase
        readEntrySql = "SELECT * " + "FROM   " + this.dataBaseTable + " " + "WHERE  " + Constants.DB_FIELD_GLOBAL_MANDATOR
            + " = ? " + " AND   " + keyField + " = ? ";
        if (useDateSelect) {
          readEntrySql += " AND   " + Constants.DB_FIELD_GLOBAL_DATE_FROM + " <= " + myDataBaseDepending.getSqlTimeNow() + " "
              + " AND   " + Constants.DB_FIELD_GLOBAL_DATE_TO + "   >= " + myDataBaseDepending.getSqlTimeNow();
        }

        // count entries in the DataBase with this key
        readEntryCountSql = "SELECT count(*) AS Entries " //
            + "FROM   " + this.dataBaseTable + " " //
            + "WHERE  " + Constants.DB_FIELD_GLOBAL_MANDATOR + " = ? " //
            + " AND   " + keyField + " = ? ";
        if (useDateSelect) {
          readEntryCountSql +=
              " AND   " + Constants.DB_FIELD_GLOBAL_DATE_FROM + " <= " + myDataBaseDepending.getSqlTimeNow() + " " //
                  + " AND   " + Constants.DB_FIELD_GLOBAL_DATE_TO + "   >= " + myDataBaseDepending.getSqlTimeNow();
        }

        // Invalidate Entry
        if (useDateSelect) {
          updateInvalidateEntrySql = "UPDATE " + this.dataBaseTable + " " //
              + "SET    " + Constants.DB_FIELD_GLOBAL_DATE_TO + "=" + myDataBaseDepending.getSqlTimeOutdate() + " ";
          if (useUser) {
            updateInvalidateEntrySql += ", " + Constants.DB_FIELD_GLOBAL_USER + " = ? ";
          }

          updateInvalidateEntrySql += //
              "WHERE  " + Constants.DB_FIELD_GLOBAL_MANDATOR + " = ? " //
                  + " AND   " + Constants.DB_FIELD_GLOBAL_DATE_FROM + " <= " + myDataBaseDepending.getSqlTimeNow() + " " //
                  + " AND   " + Constants.DB_FIELD_GLOBAL_DATE_TO + "   >= " + myDataBaseDepending.getSqlTimeNow() + " " //
                  + " AND   " + keyField + " = ? ";
        }

        // Insert Entry
        final StringBuilder sqlStringA = new StringBuilder();
        sqlStringA.append(Constants.DB_FIELD_GLOBAL_MANDATOR);
        final StringBuilder sqlStringB = new StringBuilder();
        sqlStringB.append('?');
        for (final AbstractBaseField dbField : this.dbFields) {
          if (dbField.getdbfieldname() == null || dbField.getdbfieldname().equals(Constants.DB_FIELD_GLOBAL_DATE_TO)) {
            // nothing to do
          } else if (dbField.getdbfieldname().equals(Constants.DB_FIELD_GLOBAL_DATE_FROM)) {
            sqlStringA.append(", ").append(Constants.DB_FIELD_GLOBAL_DATE_FROM);
            sqlStringB.append(", ").append(myDataBaseDepending.getSqlTimeNow());
          } else {
            dbField.sqlInsertNames(sqlStringA);
            dbField.sqlPrepareInsertItems(sqlStringB);
          }
        }
        insertEntrySql =
            "INSERT INTO " + this.dataBaseTable + " (" + sqlStringA.toString() + ") VALUES (" + sqlStringB.toString() + ")";

        // Update Entry
        final StringBuilder updateEntrySqlP = new StringBuilder();
        for (int i = 0; i < this.dbFields.length; i++) {
          if (this.numberKeyField != i) {
            this.dbFields[i].sqlUpdateName(updateEntrySqlP);
          }
        }

        updateEntrySql = "UPDATE " + this.dataBaseTable + " " + "SET    " + updateEntrySqlP + " " + "WHERE  "
            + Constants.DB_FIELD_GLOBAL_MANDATOR + " = ? " + " AND   " + keyField + " = ? ";
        if (useDateSelect) {
          updateEntrySql += " AND   " + Constants.DB_FIELD_GLOBAL_DATE_FROM + " <= " + myDataBaseDepending.getSqlTimeNow() + " "
              + " AND   " + Constants.DB_FIELD_GLOBAL_DATE_TO + "   >= " + myDataBaseDepending.getSqlTimeNow();
        }

        // Delete Entry
        deleteEntrySql = "DELETE FROM " + this.dataBaseTable + " " + "WHERE  " + Constants.DB_FIELD_GLOBAL_MANDATOR + " = ? "
            + " AND   " + keyField + " = ? ";
      }
    }

    // Position Statements (if positions are used)
    if (myDataBase != null && this.positionDataBaseTable != null) {

      // Position count
      readPosCountSql = "SELECT COUNT(*) AS num_positions " + "FROM   " + this.positionDataBaseTable + " " + "WHERE  "
          + Constants.DB_FIELD_GLOBAL_MANDATOR + " =  ? " + " AND   "
          + this.positionDbFields[this.positionNumberReferenceField].getdbfieldname() + " = ? ";
      if (positionUseDateSelect) {
        readPosCountSql += " AND   " + Constants.DB_FIELD_GLOBAL_DATE_FROM + " <= " + myDataBaseDepending.getSqlTimeNow() + " "
            + " AND   " + Constants.DB_FIELD_GLOBAL_DATE_TO + "   >= " + myDataBaseDepending.getSqlTimeNow();
      }

      // read all positions
      readAllPosSql =
          "SELECT * " + "FROM   " + this.positionDataBaseTable + " " + "WHERE  " + Constants.DB_FIELD_GLOBAL_MANDATOR + " =  ? "
              + " AND   " + this.positionDbFields[this.positionNumberReferenceField].getdbfieldname() + " = ? ";
      if (positionUseDateSelect) {
        readAllPosSql += " AND   " + Constants.DB_FIELD_GLOBAL_DATE_FROM + " <= " + myDataBaseDepending.getSqlTimeNow() + " "
            + " AND   " + Constants.DB_FIELD_GLOBAL_DATE_TO + "   >= " + myDataBaseDepending.getSqlTimeNow() + " ";
      }
      readAllPosSql += "ORDER BY " + this.positionDbFields[this.positionNumberKeyField].getdbfieldname();

      if (positionUseDateSelect) {

        // invalidate all positions by setting Date_to
        updateInvalidateallPositionsSql =
            "UPDATE " + this.positionDataBaseTable + " " + "SET    " + Constants.DB_FIELD_GLOBAL_DATE_TO + "="
                + myDataBaseDepending.getSqlTimeOutdate() + " " + "WHERE  " + Constants.DB_FIELD_GLOBAL_MANDATOR + " = ? "
                + " AND   " + this.positionDbFields[this.positionNumberReferenceField].getdbfieldname() + " = ? " + " AND    "
                + Constants.DB_FIELD_GLOBAL_DATE_FROM + " <= " + myDataBaseDepending.getSqlTimeNow() + " " + " AND   "
                + Constants.DB_FIELD_GLOBAL_DATE_TO + "   >= " + myDataBaseDepending.getSqlTimeNow() + " ";

        // invalidate a position entry by setting Date_to
        updateInvalidatePositionSql =
            "UPDATE " + this.positionDataBaseTable + " " + "SET    " + Constants.DB_FIELD_GLOBAL_DATE_TO + "="
                + myDataBaseDepending.getSqlTimeOutdate() + " " + "WHERE  " + Constants.DB_FIELD_GLOBAL_MANDATOR + " = ? "
                + " AND   " + this.positionDbFields[this.positionNumberKeyField].getdbfieldname() + " = ? " + " AND   "
                + this.positionDbFields[this.positionNumberReferenceField].getdbfieldname() + " = ? " + " AND    "
                + Constants.DB_FIELD_GLOBAL_DATE_FROM + " <= " + myDataBaseDepending.getSqlTimeNow() + " " + " AND   "
                + Constants.DB_FIELD_GLOBAL_DATE_TO + "   >= " + myDataBaseDepending.getSqlTimeNow() + " ";
      } else {

        // delete a position entry
        deletePositionSql = "DELETE FROM " + this.positionDataBaseTable + " " + "WHERE  " + Constants.DB_FIELD_GLOBAL_MANDATOR
            + " = ? " + " AND   " + this.positionDbFields[this.positionNumberKeyField].getdbfieldname() + " = ? " + " AND   "
            + this.positionDbFields[this.positionNumberReferenceField].getdbfieldname() + " = ? ";
      }

      // insert a new position
      final StringBuilder insertPositionSqlA = new StringBuilder();
      final StringBuilder insertPositionSqlB = new StringBuilder();
      insertPositionSqlA.append(Constants.DB_FIELD_GLOBAL_MANDATOR);
      insertPositionSqlB.append('?');
      for (final AbstractBaseField positionDbField : this.positionDbFields) {
        if (positionDbField.getdbfieldname() == null
            || positionDbField.getdbfieldname().equals(Constants.DB_FIELD_GLOBAL_DATE_TO)) {
          // nothing to do
        } else if (positionDbField.getdbfieldname().equals(Constants.DB_FIELD_GLOBAL_DATE_FROM)) {
          insertPositionSqlA.append(", ").append(Constants.DB_FIELD_GLOBAL_DATE_FROM);
          insertPositionSqlB.append(", ").append(myDataBaseDepending.getSqlTimeNow());
        } else {
          positionDbField.sqlInsertNames(insertPositionSqlA);
          positionDbField.sqlPrepareInsertItems(insertPositionSqlB);
        }
      }
      insertPositionSql = "INSERT INTO " + this.positionDataBaseTable + " (" + insertPositionSqlA.toString() + ") VALUES ("
          + insertPositionSqlB.toString() + ")";

      // update a position
      final StringBuilder updatePositionSqlA = new StringBuilder(32);
      updatePositionSqlA.append("UPDATE ").append(this.positionDataBaseTable).append(" SET    ");
      final StringBuilder updatePositionSqlUpdate = new StringBuilder(32);
      for (int i = 0; i < this.positionDbFields.length; i++) {
        if (this.positionNumberKeyField != i && this.positionNumberReferenceField != i) {
          this.positionDbFields[i].sqlUpdateName(updatePositionSqlUpdate);
        }
      }

      updatePositionSqlA.append(updatePositionSqlUpdate).append(" WHERE  ").append(Constants.DB_FIELD_GLOBAL_MANDATOR)
          .append(" = ? AND   ").append(this.positionDbFields[this.positionNumberKeyField].getdbfieldname()).append(" = ? ")
          .append(" AND   ").append(this.positionDbFields[this.positionNumberReferenceField].getdbfieldname()).append(" = ? ");
      if (positionUseDateSelect) {
        updatePositionSqlA.append(" AND   " + Constants.DB_FIELD_GLOBAL_DATE_FROM + " <= " + myDataBaseDepending.getSqlTimeNow()
            + " " + " AND   " + Constants.DB_FIELD_GLOBAL_DATE_TO + "   >= " + myDataBaseDepending.getSqlTimeNow());
      }

      updatePositionSql = updatePositionSqlA.toString();

      // read a position
      final StringBuilder readPositionSqlA = new StringBuilder(32);
      readPositionSqlA
          .append("SELECT * " + "FROM   " + this.positionDataBaseTable + " " + "WHERE  " + Constants.DB_FIELD_GLOBAL_MANDATOR
              + " = ? " + " AND   " + this.positionDbFields[this.positionNumberReferenceField].getdbfieldname() + " = ? "
              + " AND   " + this.positionDbFields[this.positionNumberKeyField].getdbfieldname() + " = ? ");
      if (positionUseDateSelect) {
        readPositionSqlA.append(" AND   " + Constants.DB_FIELD_GLOBAL_DATE_FROM + " <= " + myDataBaseDepending.getSqlTimeNow()
            + " " + " AND   " + Constants.DB_FIELD_GLOBAL_DATE_TO + "   >= " + myDataBaseDepending.getSqlTimeNow());
      }
      readPositionSql = readPositionSqlA.toString();

    }
  }

  /**
   * build java script for this site.
   *
   * @return javaScript string
   */
  protected String initJavaScript() throws SQLException {
    final StringBuilder javaScriptSb = new StringBuilder(128);
    if (dataBaseTable == null) {
      return StringUtils.EMPTY;
    }
    if (dbFields != null && dbFields.length > 0) {
      javaScriptSb.append("        <script type=\"text/javascript\">\n" + "            <!--\n"
          + "                if (top!=self)\n" + "                    top.location=self.location;\n\n"
          + "                function checkRegEx(field, regex_reg, complusion, type, submit) {\n"
          + "                    var check_field=window.document.getElementById(field);\n"
          + "                    if (check_field != null) {\n" + "                        var buttonpress_field = "
          + "                                 window.document.getElementById('buttonpress');\n"
          + "                        var buttonpress_check = buttonpress_field.value;\n"
          + "                        var check_value = check_field.value;\n" + "                        var do_check = false;\n"
          + "                        if (submit) {\n" + "                            do_check = (type == buttonpress_check);\n"
          + "                        } else {\n"
          + "                            if (type == 'save' || type.substr(0,4) == 'Inse') {\n"
          + "                                document.forms[0].doformcheck.value=\"true\";\n"
          + "                                do_check = true;\n" + "                            } else\n"
          + "                                document.forms[0].doformcheck.value=\"false\";\n" + "                        }\n"
          + "                        if (!do_check) {\n" + "                            return true;\n"
          + "                        } else if (complusion && (type.substr(0,4) == 'Inse') "
          + "                                && check_value == '' && !submit) {\n"
          + "                            window.status='';\n" + "                            check_field.className='faulty';\n"
          + "                            return true;\n"
          + "                        } else if ((!complusion && check_value == '') "
          + "                                 || check_value.search(regex_reg) >= 0) {\n"
          + "                            window.status='';\n" + "                            check_field.className='default';\n"
          + "                            buttonpress_field.value=type;\n" + "                            return true;\n"
          + "                        } else {\n" + "                            if (submit)\n"
          + "                                alert(unescape('Bitte Eingabe pr%FCfen!'));\n"
          + "                            else\n"
          + "                                window.status=unescape('Bitte Eingabe pr%FCfen!');\n"
          + "                            check_field.className='faulty';\n"
          + "                            check_field.focus();\n" + "                            check_field.select();\n"
          + "                            return false;\n" + "                        }\n" + "                    } else\n"
          + "                        return true;\n" + "                }\n\n" + "                function chkForm() {\n"
          + "                    if (document.forms[0].doformcheck.value==\"true\") {\n");
      for (final AbstractBaseField dbField : dbFields) {
        dbField.setNewDataBase(myDataBase);
        dbField.setNewServletname(servletName);
        javaScriptSb.append(dbField.javascriptCheck());
      }
      javaScriptSb.append("                    }\n" + "                    return true;\n" + "                }\n"
          + "            //-->\n" + "        </script>\n");
    } else {
      return StringUtils.EMPTY;
    }

    return javaScriptSb.toString();
  }

  /**
   * The Method <code>fillMinMax</code> reads the min and max keys in the database.
   *
   * @param session HttpSession
   * @exception java.sql.SQLException
   */
  protected void fillMinMax(final HttpSession session) throws java.sql.SQLException {
    if (session != null && readMinMaxSql != null) {
      try (final PreparedStatement preparedStatement = myDataBase.prepareStatement(readMinMaxSql)) {
        final Integer MandatorInteger = (Integer) session.getAttribute(servletName + Constants.DB_FIELD_GLOBAL_MANDATOR);

        preparedStatement.clearParameters();
        preparedStatement.setInt(1, MandatorInteger == null ? 1 : MandatorInteger.intValue());
        try (final ResultSet result = preparedStatement.executeQuery()) {
          if (result.next()) {
            final String dbmin = result.getString("dbmin");
            final String dbmax = result.getString("dbmax");
            String dbnumber = (String) session.getAttribute(servletName + dataBaseTable + "." + keyField);
            if (dbnumber != null && dbnumber.equals(StringUtils.EMPTY)) {
              dbnumber = dbmax;
              session.setAttribute(servletName + dataBaseTable + "." + keyField, dbnumber);
              session.removeAttribute(servletName + AbstractVisualDb.NUMBER_POS_TEXT + serviceName);
            }
            session.setAttribute(servletName + AbstractVisualDb.DBMIN_TEXT + serviceName, dbmin);
            session.setAttribute(servletName + AbstractVisualDb.DBMAX_TEXT + serviceName, dbmax);
          }
        }
      }
    }
  }

  /**
   * The Method <code>getPictureName</code> gives back the picture name for the menu.
   *
   * @return Name of the Picture
   */
  public String getPictureName() {
    return pictureName;
  }


  /**
   * The Method <code>getPictureNameHiDpi</code> gives back the picture name for the menu for HiDPI displays.
   *
   * @return Name of the Picture
   */
  public String getPictureNameHiDpi() {
    return pictureNameHiDpi;
  }

  /**
   * The Method <code>getServiceName</code> gives back the name of the service.
   *
   * @return Name of the Service
   */
  public String getServiceName() {
    return serviceName;
  }

  /**
   * The Method <code>getServiceNameLocal</code> gives back the localized name for the menu.
   *
   * @return Name of the Service localized
   */
  public String getServiceNameLocal() {
    return serviceNameLocal;
  }

  /**
   * The Method <code>getTitle</code> gives back the Title of the application.
   *
   * @return Name of the application
   */
  public String getTitle() {
    return serviceNameLocal;
  }

  /**
   * The Method <code>getTitle</code> gives back the Title of the application.
   *
   * @param session HttpSession
   * @return Name of the application
   */
  public String getTitle(final HttpSession session) {
    return serviceNameLocal;
  }

  /**
   * The Method <code>preventFromCache</code> tells you if this page should be cached or not.
   *
   * @param session HttpSession of the servlet
   * @return don't cache this site = true
   */
  public boolean preventFromCache(final HttpSession session) {
    return true;
  }

  /**
   * The Method <code>getJavaScript</code> returns the JavaScript code to check the input of the page If Session not allows
   * changes a EmptyField string is returned.
   *
   * @param session HttpSession
   * @return JavaScript code
   */
  public String getJavaScript(final HttpSession session) {
    if (session != null && allowedToChange(session)) {
      return javaScript;
    } else {
      return StringUtils.EMPTY;
    }
  }

  /**
   * The Method <code>getSelectedField</code> returns the SelectedField if there is a field preselected in the form.
   *
   * @param session HttpSession
   * @return fieldname of the preselected field
   */
  public String getSelectedField(final HttpSession session) {
    if (session != null && allowedToChange(session)) {
      return (String) session.getAttribute(servletName + AbstractVisualDb.SELECTED_FIELD_TEXT + serviceName);
    } else {
      return null;
    }
  }

  /**
   * The Method <code>allowedToSee</code> tells you if the currently logged in user is allowed to access this application. Has
   * to be redefined by the child.
   *
   * @param session HttpSession
   * @return true if it is allowed for this user
   */
  public boolean allowedToSee(final HttpSession session) {
    return session != null;
  }

  /**
   * The Method <code>allowedToChange</code> tells you if the currently logged in user is allowed to change data in this
   * application. Has to be redefined by the child.
   *
   * @param session HttpSession
   * @return true if it is allowed for this user
   */
  public boolean allowedToChange(final HttpSession session) {
    return session != null;
  }

  /**
   * The Method <code>changesNavagation</code> tells you if this Class can change navigation (must be read again from the
   * session).
   *
   * @param session HttpSession
   * @return true if it is allowed for this user
   */
  public boolean changesNavigation(final HttpSession session) {
    return false;
  }

  /**
   * The Method <code>setDBNumber</code> switches to a new DataBases number.
   *
   * @param session HttpSession
   * @param number The new key/number of the DataBase
   */
  public void setDbNumber(final HttpSession session, final String number) {
    if (session != null) {
      session.setAttribute(servletName + dataBaseTable + "." + keyField, number);
      session.removeAttribute(servletName + AbstractVisualDb.NUMBER_POS_TEXT + serviceName);
    }
  }

  /**
   * The Method <code>readDBNumber</code> reads the last used DB-Key-Field as current number.
   *
   * @param session HttpSession
   */
  public void readDbNumber(final HttpSession session) {
    if (session != null) {
      String dbnumber = (String) session.getAttribute(servletName + dataBaseTable + "." + keyField);
      if (dbnumber == null) {
        dbnumber = (String) session.getAttribute(servletName + AbstractVisualDb.DBMAX_TEXT + serviceName);
        session.setAttribute(servletName + dataBaseTable + "." + keyField, dbnumber);
      }
      session.removeAttribute(servletName + AbstractVisualDb.NUMBER_POS_TEXT + serviceName);
    }
  }

  /**
   * The Method <code>readDBNumberWOOF</code> reads the last used DB-Key-Field as current number without Other Field.
   *
   * @param session HttpSession
   */
  public String readDbNumberWoof(final HttpSession session) {
    String dbnumber = null;
    if (session != null) {
      dbnumber = (String) session.getAttribute(servletName + dataBaseTable + "." + keyField);
    }
    return dbnumber;
  }

  /**
   * The Method <code>nextDBNumber</code> jumps to the next entry in DataBase.
   *
   * @param session HttpSession
   * @exception TextException Textmessage, when something's going wrong
   * @see TextException
   */
  protected void nextDbNumber(final HttpSession session) throws TextException {
    if (session != null) {
      String dbnumber = (String) session.getAttribute(servletName + dataBaseTable + "." + keyField);
      if (dbnumber == null || dbnumber.equals(StringUtils.EMPTY)) {
        dbnumber = (String) session.getAttribute(servletName + AbstractVisualDb.DBMIN_TEXT + serviceName);
      } else {
        try (final PreparedStatement preparedStatement = myDataBase.prepareStatement(readNextSql);) {
          final Integer MandatorInteger = (Integer) session.getAttribute(servletName + Constants.DB_FIELD_GLOBAL_MANDATOR);
          preparedStatement.clearParameters();
          preparedStatement.setInt(1, MandatorInteger == null ? 1 : MandatorInteger.intValue());
          preparedStatement.setString(2, dbnumber);
          try (final ResultSet result = preparedStatement.executeQuery()) {
            if (result.next()) {
              dbnumber = result.getString("dbnumber");
            }
          }
        } catch (final java.sql.SQLException e) {
          throw new TextException("SQL-Fehler in nextDBNumber()\n" + e.toString(), e);
        }
      }
      session.setAttribute(servletName + dataBaseTable + "." + keyField, dbnumber);
      session.removeAttribute(servletName + AbstractVisualDb.NUMBER_POS_TEXT + serviceName);
    }
  }

  /**
   * The Method <code>previousDBNumber</code> jumps to the previous entry in DataBase.
   *
   * @param session HttpSession
   * @exception TextException Textmessage, when something's going wrong
   * @see TextException
   */
  protected void previousDbNumber(final HttpSession session) throws TextException {
    if (session != null) {
      String dbnumber = (String) session.getAttribute(servletName + dataBaseTable + "." + keyField);
      if (dbnumber == null || dbnumber.equals(StringUtils.EMPTY)) {
        dbnumber = (String) session.getAttribute(servletName + AbstractVisualDb.DBMAX_TEXT + serviceName);
      } else {
        try {
          final Integer MandatorInteger = (Integer) session.getAttribute(servletName + Constants.DB_FIELD_GLOBAL_MANDATOR);
          try (final PreparedStatement preparedStatement = myDataBase.prepareStatement(readPrevSql)) {
            preparedStatement.clearParameters();
            preparedStatement.setInt(1, MandatorInteger == null ? 1 : MandatorInteger.intValue());
            preparedStatement.setString(2, dbnumber);
            try (final ResultSet result = preparedStatement.executeQuery()) {
              if (result.next()) {
                dbnumber = result.getString("dbnumber");
              }
            }
          }
        } catch (final java.sql.SQLException e) {
          throw new TextException("SQL-Fehler in previousDBNumber()\n" + e.toString(), e);
        }
      }
      session.setAttribute(servletName + dataBaseTable + "." + keyField, dbnumber);
      session.removeAttribute(servletName + AbstractVisualDb.NUMBER_POS_TEXT + serviceName);
    }
  }

  /**
   * The Method <code>HTML_Navigator</code> Creates the navigation part of the HTML page
   *
   * @param session HttpSession
   * @param navTyp Navigation type
   * @return HTML code of the navigation part
   * @exception java.sql.SQLException
   */
  protected String htmlNavigator(final HttpSession session, final String navTyp) throws java.sql.SQLException {
    return this.htmlNavigator(session, navTyp, true, true, true, true, true, true, true, true, true, true, true);
  }

  /**
   * The Method <code>HTML_Navigator</code> Creates the navigation part of the HTML page
   *
   * @param psession HttpSession
   * @param pnavTyp Navigation type
   * @param pshowNew Show the "new" button, or not
   * @param pshowSave Show the "save" button, or not
   * @param pshowDelete Show the "delete" button, or not
   * @param pshowSearch Show the "search" button, or not
   * @param pshowReset Show the "reset" button, or not
   * @param pshowfBack Show the "fast backward" button, or not
   * @param pshowBack Show the "backward" button, or not
   * @param pshowSwitch Show the "switch to" button, or not
   * @param pshowForward Show the "forward" button, or not
   * @param pshowfForward Show the "fast forward" button, or not
   * @param pshowExtra Show the "extra" button if other options are set, or not
   * @return HTML code of the navigation part
   * @exception java.sql.SQLException
   */
  protected String htmlNavigator(final HttpSession psession, final String pnavTyp, final boolean pshowNew,
      final boolean pshowSave, final boolean pshowDelete, final boolean pshowSearch, final boolean pshowReset,
      final boolean pshowfBack, final boolean pshowBack, final boolean pshowSwitch, final boolean pshowForward,
      final boolean pshowfForward, final boolean pshowExtra) throws java.sql.SQLException {
    fillMinMax(psession);
    String dbPosition = null;
    boolean showDelete2 = false;
    boolean showNew = pshowNew;
    boolean showSave = pshowSave;
    boolean showDelete = pshowDelete;
    boolean showfBack = pshowfBack;
    boolean showBack = pshowBack;
    boolean showSwitch = pshowSwitch;
    boolean showForward = pshowForward;
    boolean showfForward = pshowfForward;
    boolean showExtra = pshowExtra;
    final boolean showPosition = showfBack || showBack || showSwitch || showForward || showfForward;
    String extraButtonHtml = null;
    final String dbmin = (String) psession.getAttribute(servletName + AbstractVisualDb.DBMIN_TEXT + serviceName);
    final String dbmax = (String) psession.getAttribute(servletName + AbstractVisualDb.DBMAX_TEXT + serviceName);
    final String dbnumber = readDbNumberWoof(psession);

    updateSession(psession);

    if (dbnumber == null) {
      showNew = false;
    }
    showSave &= allowedToChange(psession);

    if (showSave) {
      if (StringUtils.isEmpty(dbnumber)) {
        showDelete = false;
      } else {
        if ("delete".equals(pnavTyp)) {
          showDelete2 = true;
        }
      }
    }

    if (!(dbnumber == null && dbmin != null || dbnumber != null && dbmin != null && !dbnumber.equals(dbmin))) {
      showfBack = false;
      showBack = false;
    }

    if (StringUtils.isEmpty(dbnumber)) {
      dbPosition = "akt:&nbsp;Neuer&nbsp;Datensatz";
      showSwitch = false;
    } else {
      dbPosition = "akt:&nbsp;<input type=\"text\" name=\"dbnumber_go\" value=\"" + StringToHtml.convert(dbnumber)
          + "\" accesskey=\"k\" tabindex=\"30012\" " + "onChange=\"document.forms[0].buttonpress.value='goto';\">";
    }

    if (StringUtils.isEmpty(dbnumber) || dbmax == null || StringUtils.equals(dbnumber, dbmax)) {
      showForward = false;
      showfForward = false;
    }

    if (otherOptions == null) {
      showExtra = false;
    } else {
      extraButtonHtml = "                    <td class=\"topnav\"><button type=\"submit\" " + "name=\"navsubmittype\" value=\""
          + StringToHtml.convert(otherOptions) + "\" tabindex=\"30005\" onClick=\"document.forms[0].buttonpress.value='"
          + StringToHtml.convert(otherOptions) + "';\" title=\"" + StringToHtml.convert(otherOptions) + "\"><img src=\""
          + pathPrefix + Constants.GIF_URL + "32x32/userdefined.png\" srcset=\"" + pathPrefix + Constants.GIF_URL
          + "64x64/userdefined.png 2x\" width=\"32\" height=\"32\" alt=\"" + StringToHtml.convert(otherOptions) + "\" title=\""
          + StringToHtml.convert(otherOptions) + "\"></button></td>\n";
    }

    return "            <div>\n" + "                <input type=\"hidden\" name=\"service\" value=\""
        + StringToHtml.convert(serviceName) + "\">\n"
        + "                <input type=\"hidden\" id=\"doformcheck\" name=\"doformcheck\" " + "value=\"false\">\n"
        + "                <input type=\"hidden\" id=\"buttonpress\" name=\"buttonpress\" " + "value=\"save\">\n"
        + "                <input type=\"submit\" id=\"dummysubmit\" name=\"dummysubmitt\" " + "value=\"dummy\">\n"
        + "            </div>\n" + "            <hr>\n" + "            <table class=\"navigationbar\">\n"
        + "                <colgroup span=\"14\">\n" + "                    <col width=\"36px\">\n"
        + "                    <col width=\"36px\">\n" + "                    <col width=\"36px\">\n"
        + "                    <col width=\"36px\">\n" + "                    <col width=\"36px\">\n"
        + "                    <col width=\"10px\">\n" + "                    <col width=\"36px\">\n"
        + "                    <col width=\"36px\">\n" + "                    <col width=\"284px\">\n"
        + "                    <col width=\"36px\">\n" + "                    <col width=\"36px\">\n"
        + "                    <col width=\"36px\">\n" + "                    <col width=\"10px\">\n"
        + "                    <col width=\"36px\">\n" + "                </colgroup>\n" + "                <tr>\n"
        + (showNew ? netButtonHtml : emptyButtonHtml) + (showSave ? saveButtonHtml : emptyButtonHtml)
        + (showDelete ? showDelete2 ? delete2ButtonHtml : deleteButtonHtml : emptyButtonHtml)
        + (pshowSearch ? searchButtonHtml : emptyButtonHtml) + (pshowReset ? resetButtonHtml : emptyButtonHtml)
        + "                    <td class=\"topnav\">&nbsp;</td>\n" + (showfBack ? fbackButtonHtml : emptyButtonHtml)
        + (showBack ? backButtonHtml : emptyButtonHtml) + "\n                    <td class=\"topnav\">"
        + (showPosition
            ? "min:&nbsp;" + StringToHtml.convert(dbmin) + " / max:&nbsp;" + StringToHtml.convert(dbmax) + "<br>" + dbPosition
            : "&nbsp;")
        + "</td>\n" + (showSwitch ? switchButtonHtml : emptyButtonHtml) + (showForward ? forwardButtonHtml : emptyButtonHtml)
        + (showfForward ? fforwardButtonHtml : emptyButtonHtml) + "                    <td class=\"topnav\">&nbsp;</td>\n"
        + (showExtra ? extraButtonHtml : emptyButtonHtml) + "                </tr>\n" + "            </table>\n"
        + "             <hr>\n";
  }

  /**
   * The Method <code>UpdateSession</code> Updates the Key in the Session.
   *
   * @param session HttpSession
   */
  protected void updateSession(final HttpSession session) {
    // nothing to do
  }

  /**
   * The Method <code>HTMLPage</code> generates the html formular.
   *
   * @param res HttpServletResponse from the Servlet
   * @param hint Hint to be displayed on the screen
   * @param session HttpSession
   * @param navTyp Navigation type
   * @return HTML code of the formular
   */
  protected String htmlPage(final HttpServletResponse res, final String hint, final HttpSession session, final String navTyp) {
    int posI = 0;
    int tabpos = 1;
    int countTDs = 0;
    final StringBuilder sb = new StringBuilder(1024);
    String selectedFieldDefault = null;
    String field = "x";
    final String dbnumber = readDbNumberWoof(session);
    final String DebugText = (String) session.getAttribute(servletName + AbstractVisualDb.DEBUG_TEXT_TEXT + serviceName);
    String selectedField = null;

    try {
      if (!allowedToSee(session)) {
        return "<h1>Sie sind nicht berechtigt, diese Anwendung aufzurufen!</h1>\n";
      }
      if (allowedToChange(session)) {
        sb.append("        <form action=\"" + res.encodeURL(pathPrefix + Constants.SERVLET_URL + servletName)
            + "\" method=\"POST\" enctype=\"application/x-www-form-urlencoded; charset=utf-8\" "
            + "accept-charset=\"utf-8\" OnSubmit=\"return chkForm();\">\n" + this.htmlNavigator(session, navTyp));
      } else {
        sb.append("        <form action=\"" + res.encodeURL(pathPrefix + Constants.SERVLET_URL + servletName)
            + "\" method=\"POST\" enctype=\"application/x-www-form-urlencoded; charset=utf-8\" " + "accept-charset=\"utf-8\">\n"
            + this.htmlNavigator(session, navTyp));
      }
    } catch (final java.sql.SQLException e) {
      return "SQL-Fehler in HTMLPage()\n" + e.toString();
    }

    if (DebugText != null) {
      sb.append("                    <p>" + StringToHtml.convert(DebugText) + "</p>\n");
      session.removeAttribute(servletName + AbstractVisualDb.DEBUG_TEXT_TEXT + serviceName);
    }
    if (hint == null) {
      if (allowedToChange(session)) {
        sb.append("            <p>Geben Sie die gew&uuml;nschten Daten ein.</p>\n");
      } else {
        sb.append("            <p>Keine &Auml;nderungsberechtigung, " + "Daten werden nur angezeigt.</p>\n");
      }
    } else {
      sb.append("            <p><b>" + StringToHtml.convert(hint) + "</b></p>\n");
    }

    sb.append("            <table width=\"100%\">\n" + "                <colgroup span=\"4\">\n"
        + "                    <col width=\"20%\">\n" + "                    <col width=\"30%\">\n"
        + "                    <col width=\"20%\">\n" + "                    <col width=\"30%\">\n"
        + "                </colgroup>\n");

    if (dbFields != null) {
      for (posI = 0; posI < dbFields.length; posI++) {
        if (posI == 0 || countTDs % 4 == 0) {
          sb.append("                <tr>\n");
        }

        if (allowedToChange(session) && (posI != numberKeyField || StringUtils.isEmpty(dbnumber))) {
          sb.append(dbFields[posI].htmlInputTableSegment(tabpos++, session));
          if (dbnumber == null && selectedField == null && dbFields[posI].getEnchentment() != null) {
            selectedField = dbFields[posI].getfieldname();
            selectedFieldDefault = dbFields[posI].getfieldname();
          } else {
            if (selectedField == null && !dbFields[posI].checkField(session)) {
              selectedField = dbFields[posI].getfieldname();
            }
            if (selectedFieldDefault == null && dbFields[posI].isComplusion()) {
              selectedFieldDefault = dbFields[posI].getfieldname();
            }
          }
        } else {
          sb.append(dbFields[posI].htmlViewTableSegment(session));
        }

        countTDs += dbFields[posI].numTDs();

        if (countTDs % 4 == 0) {
          sb.append("                </tr>\n");
        }
      }
    }

    if (countTDs % 4 != 0) {
      sb.append("                    <td>&nbsp;</td>\n" + "                    <td>&nbsp;</td>\n" + "                </tr>\n");
    }
    sb.append("        </table>\n");

    if (positionDataBaseTable == null || positionDbFields == null || dbnumber == null) {
      session.removeAttribute(servletName + AbstractVisualDb.NUMBER_POS_TEXT + serviceName);
    } else {
      final Integer num_pos_Integer =
          (Integer) session.getAttribute(servletName + AbstractVisualDb.NUMBER_POS_TEXT + serviceName);
      int numPos = 0;

      sb.append("            <table class=\"fullborder\">\n").append(headerlinePosition(session));

      if (num_pos_Integer == null) {
        // read data from database
        try {
          int posJ = 0;
          final Integer MandatorInteger = (Integer) session.getAttribute(servletName + Constants.DB_FIELD_GLOBAL_MANDATOR);
          try (final PreparedStatement preparedStatement = myDataBase.prepareStatement(readAllPosSql)) {
            preparedStatement.clearParameters();
            preparedStatement.setInt(1, MandatorInteger == null ? 1 : MandatorInteger.intValue());
            preparedStatement.setString(2, dbnumber);
            try (final ResultSet resultAllPos = preparedStatement.executeQuery()) {

              // How many entries are in the resultset?
              resultAllPos.last();
              numPos = resultAllPos.getRow();

              // back to first entry
              resultAllPos.beforeFirst();

              for (posI = 0; posI < numPos; posI++) {
                resultAllPos.next();
                for (posJ = 0; posJ < positionDbFields.length; posJ++) {
                  positionDbFields[posJ].sqlRead(resultAllPos, session, posI);
                }
              }
            }
          }

          for (posJ = 0; posJ < positionDbFields.length; posJ++) {
            if (posJ == positionNumberKeyField && positionDbFields[posJ].getEnchentment() == null) {
              if (numPos == 0) {
                positionDbFields[posJ].setField(session, "0", numPos);
              } else {
                field = positionDbFields[posJ].getContents(session, numPos - 1);
                if (field == null) {
                  posI = numPos - 1;
                } else {
                  posI = Integer.parseInt(field);
                }
                field = Integer.toString(posI + 1);
                positionDbFields[posJ].setField(session, field, numPos);
              }
            } else if (posJ == positionNumberReferenceField) {
              positionDbFields[posJ].setField(session, dbFields[numberKeyField].getContents(session), numPos);
            } else {
              positionDbFields[posJ].initField(session, numPos);
            }
          }
        } catch (final java.sql.SQLException e) {
          sb.append("SQL-Fehler in HTML_Formular(), bitte EDV verst&auml;ndigen:\n").append(e.toString());
        }
      } else {
        // we allready know all data
        numPos = num_pos_Integer.intValue();
      }

      // show all lines
      for (posI = 0; posI < numPos; posI++) {
        session.setAttribute(servletName + AbstractVisualDb.SELECTED_FIELD_TEXT + serviceName, selectedField);
        sb.append(inputlinePosition(session, posI, numPos, tabpos));
        tabpos += positionDbFields.length;
        selectedField = (String) session.getAttribute(servletName + AbstractVisualDb.SELECTED_FIELD_TEXT + serviceName);
      }

      // another line for inserts
      if (positionAddRemove && allowedToChange(session)) {
        session.setAttribute(servletName + AbstractVisualDb.SELECTED_FIELD_TEXT + serviceName, selectedField);
        sb.append(inputlinePosition(session, numPos, numPos, tabpos));
        selectedField = (String) session.getAttribute(servletName + AbstractVisualDb.SELECTED_FIELD_TEXT + serviceName);
      }

      sb.append("            </table>\n");

      session.setAttribute(servletName + AbstractVisualDb.NUMBER_POS_TEXT + serviceName, Integer.valueOf(numPos));
    }

    sb.append("            <p class=\"anmerkung\">&sup1;: Zwangsfelder, diese Felder m&uuml;ssen "
        + "immer gef&uuml;llt werden</p>\n" + "            <p class=\"anmerkung\">&sup2;: Wahlfreie Felder, diese Felder "
        + "k&ouml;nnen leer gelassen werden</p>\n" + "        </form>\n");

    if (allowedToChange(session) && selectedField == null) {
      selectedField = selectedFieldDefault;
    }
    session.setAttribute(servletName + AbstractVisualDb.SELECTED_FIELD_TEXT + serviceName, selectedField);

    return sb.toString();
  }

  /**
   * The Method <code>prepareInsert</code> cleans up the window for a new insert.
   *
   * @param res HttpServletResponse from the Servlet
   * @param session HttpSession
   * @param navTyp Navigation type
   * @return HTML code of the formular
   * @exception TextException text of the error
   */
  protected String prepareInsert(final HttpServletResponse res, final HttpSession session, final String navTyp)
      throws TextException {
    session.setAttribute(servletName + dataBaseTable + "." + keyField, null);
    session.removeAttribute(servletName + AbstractVisualDb.NUMBER_POS_TEXT + serviceName);
    if (dbFields != null) {
      for (int i = 0; i < dbFields.length; dbFields[i++].initField(session)) {
        ;
      }
    }
    return htmlPage(res, "Anlegen eines neuen Datensatzes", session, navTyp);
  }

  /**
   * The Method <code>preparePage</code> fills up the window with current data.
   *
   * @param req HttpServletRequest from the Servlet
   * @param res HttpServletResponse from the Servlet
   * @param hintGiven Text to be displayed
   * @param session HttpSession
   * @param navTyp Navigation type
   * @return HTML code of the formular
   * @exception TextException text of the error
   */
  protected String preparePage(final HttpServletRequest req, final HttpServletResponse res, final String hintGiven,
      final HttpSession session, final String navTyp) throws TextException {
    int posI;
    final String dbmax = (String) session.getAttribute(servletName + AbstractVisualDb.DBMAX_TEXT + serviceName);
    final String dbnumber = (String) session.getAttribute(servletName + dataBaseTable + "." + keyField);
    final StringBuilder hint = new StringBuilder(StringUtils.defaultString(hintGiven));

    if (dbFields != null) {
      for (posI = 0; posI < dbFields.length; dbFields[posI++].initField(session)) {
        ;
      }
    }

    try {
      // Datensätze vorhanden?
      if (dbnumber == null) {
        if (dbmax == null) {
          hint.append("Keine Daten zur Änderung vorhanden!");
        }
        if (allowedToChange(session)) {
          hint.append(" Ein neuer Satz kann eingefügt werden.");
        }
      } else {
        // Lesezugriff auf die Datenbank
        final Integer MandatorInteger = (Integer) session.getAttribute(servletName + Constants.DB_FIELD_GLOBAL_MANDATOR);
        try (final PreparedStatement preparedStatement = myDataBase.prepareStatement(readEntrySql)) {
          preparedStatement.clearParameters();
          preparedStatement.setInt(1, MandatorInteger == null ? 1 : MandatorInteger.intValue());
          preparedStatement.setString(2, dbnumber);
          try (final ResultSet result = preparedStatement.executeQuery()) {
            if (result.next() && dbFields != null) {
              for (posI = 0; posI < dbFields.length; dbFields[posI++].sqlRead(result, session)) {
                ;
              }
            }
          }
        }
      }

      return htmlPage(res, hint.toString(), session, navTyp);
    } catch (final java.sql.SQLException e) {
      // Um die Fehlerbehandlung nicht zweimal zu machen, schicken
      // wir die Meldung einfach weiter
      throw new TextException("SQL-Fehler in preparePage()\n" + e.toString(), e);
    }
  }

  /**
   * The Method <code>doChange</code> updates database with changed data.
   *
   * @param req HttpServletRequest from the Servlet
   * @param res HttpServletResponse from the Servlet
   * @param session HttpSession
   * @param navTyp Navigation type
   * @return HTML code of the formular
   * @exception TextException text of the error
   */
  protected String doChange(final HttpServletRequest req, final HttpServletResponse res, final HttpSession session,
      final String navTyp) throws TextException {
    final String debug = null;
    final StringBuilder hint = new StringBuilder(64);
    boolean allok = true;
    boolean hasChanged = false;
    String dbnumber = (String) session.getAttribute(servletName + dataBaseTable + "." + keyField);
    final String dbnumbersave = dbnumber;
    final Integer mandatorInteger = (Integer) session.getAttribute(servletName + Constants.DB_FIELD_GLOBAL_MANDATOR);
    String dbString = null;
    String htmlString = null;

    int posI;
    int posJ;

    if (dbFields != null) {
      for (posI = 0; posI < dbFields.length; posI++) {
        dbFields[posI].htmlRead(req, session);
        if (!dbFields[posI].checkField(session)) {
          if (hint.length() == 0) {
            hint.append("Fehler bei Feld: ");
          } else {
            hint.append(", ");
          }
          hint.append(dbFields[posI].getEnchentment());
          allok = false;
        }
      }
    }

    if (allok && dbnumber != null && !dbnumber.equals(StringUtils.EMPTY)) {
      try (final PreparedStatement preparedStatement = myDataBase.prepareStatement(readEntrySql)) {
        preparedStatement.clearParameters();
        preparedStatement.setInt(1, mandatorInteger == null ? 1 : mandatorInteger.intValue());
        preparedStatement.setString(2, dbnumber);
        try (final ResultSet result = preparedStatement.executeQuery()) {
          if (result.next() && dbFields != null) {
            for (posI = 0; posI < dbFields.length && !hasChanged; posI++) {
              if (posI != numberKeyField && dbFields[posI].getdbfieldname() != null && dbFields[posI].getfieldname() != null
                  && dbFields[posI].getEnchentment() != null) {
                dbFields[posI].sqlRead(result, session);
                dbString = dbFields[posI].getContents(session);
                dbFields[posI].htmlRead(req, session);
                htmlString = dbFields[posI].getContents(session);
                hasChanged |= !StringUtils.equals(StringUtils.defaultString(htmlString), StringUtils.defaultString(dbString));
              }
            }
          } else {
            hasChanged = true;
          }
        }
      } catch (final SQLException e) {
        hasChanged = true;
      }
    } else {
      hasChanged = true;
    }

    try {
      if (allok && hasChanged) {
        if (StringUtils.isEmpty(dbnumber) || useDateSelect) {

          // Alten Eintrag entwerten
          if (useDateSelect) {
            for (posI = 0; posI < 2; posI++) {
              if (dbnumber != null) {
                try (final PreparedStatement preparedStatement = myDataBase.prepareStatement(updateInvalidateEntrySql)) {
                  preparedStatement.clearParameters();
                  if (useUser) {
                    preparedStatement.setString(1, (String) session
                        .getAttribute(servletName + Constants.DB_FIELD_GLOBAL_USER + Constants.DB_FIELD_GLOBAL_USER));
                    preparedStatement.setInt(2, mandatorInteger == null ? 1 : mandatorInteger.intValue());
                    preparedStatement.setString(3, dbnumber);
                  } else {
                    preparedStatement.setInt(1, mandatorInteger == null ? 1 : mandatorInteger.intValue());
                    preparedStatement.setString(2, dbnumber);
                  }
                  preparedStatement.executeUpdate();
                }
              }

              // Wenn der Key sich geändert hat, neben dem alten Feld auch eventuelle Kollegen dese
              // neuen Keys Invalidieren
              dbnumber = dbFields[numberKeyField].getContents(session);
              if (dbnumber == null || dbnumber.equals(dbnumbersave)) {
                dbnumber = null;
              }
            }
          }

          // Neuen Datensatz einfügen
          posJ = 2;
          if (dbFields != null) {
            try (final PreparedStatement preparedStatement = myDataBase.prepareStatement(insertEntrySql)) {
              preparedStatement.clearParameters();
              preparedStatement.setInt(1, mandatorInteger == null ? 1 : mandatorInteger.intValue());
              for (posI = 0; posI < dbFields.length; posI++) {
                if (!(dbFields[posI].getdbfieldname() == null
                    || dbFields[posI].getdbfieldname().equals(Constants.DB_FIELD_GLOBAL_DATE_FROM)
                    || dbFields[posI].getdbfieldname().equals(Constants.DB_FIELD_GLOBAL_DATE_TO))) {
                  if (dbFields[posI].getdbfieldname().equals(Constants.DB_FIELD_GLOBAL_USER)
                      && (dbFields[posI].getContents(session) == null
                          || dbFields[posI].getContents(session).equals(StringUtils.EMPTY))) {
                    preparedStatement.setString(posJ++, (String) session
                        .getAttribute(servletName + Constants.DB_FIELD_GLOBAL_USER + Constants.DB_FIELD_GLOBAL_USER));
                  } else {
                    posJ = dbFields[posI].sqlPrepareItems(preparedStatement, myDataBase, posJ, session);
                  }
                }
              }
              preparedStatement.executeUpdate();
            }

            dbnumber = dbFields[numberKeyField].getContents(session);
            session.setAttribute(servletName + dataBaseTable + "." + keyField, dbnumber);
            session.removeAttribute(servletName + AbstractVisualDb.NUMBER_POS_TEXT + serviceName);
          }

          fillMinMax(session);

          if (dbnumbersave == null) {
            hint.append("Der Datensatz '").append(dbnumber).append("' wurde eingefügt.");
          } else {
            hint.append("Der Datensatz '").append(dbnumber).append("' wurde geändert.");
          }
        } else {
          posJ = 1;
          if (dbFields != null && dbFields.length > 1) {
            try (final PreparedStatement preparedStatement = myDataBase.prepareStatement(updateEntrySql)) {
              preparedStatement.clearParameters();
              for (posI = 0; posI < dbFields.length; posI++) {
                if (numberKeyField != posI) {
                  if (dbFields[posI].getdbfieldname() != null
                      && dbFields[posI].getdbfieldname().equals(Constants.DB_FIELD_GLOBAL_USER)
                      && (dbFields[posI].getContents(session) == null
                          || dbFields[posI].getContents(session).equals(StringUtils.EMPTY))) {
                    preparedStatement.setString(posJ++, (String) session
                        .getAttribute(servletName + Constants.DB_FIELD_GLOBAL_USER + Constants.DB_FIELD_GLOBAL_USER));
                  } else {
                    posJ = dbFields[posI].sqlPrepareItems(preparedStatement, myDataBase, posJ, session);
                  }
                }
              }
              preparedStatement.setInt(posJ++, mandatorInteger == null ? 1 : mandatorInteger.intValue());
              preparedStatement.setString(posJ++, dbnumber);

              preparedStatement.executeUpdate();
            }
          }
          hint.append("Änderung wurde durchgeführt");
        }
      }
    } catch (final java.sql.SQLException e) {
      // Um die Fehlerbehandlung nicht zweimal zu machen, schicken
      // wir die Meldung einfach weiter
      if (e.toString().toLowerCase().indexOf("duplicate entry") >= 0) {
        hint.append("Ein Eintrag \"").append(dbFields[numberKeyField].getContents(session))
            .append("\" existiert bereits und kann nicht eingefügt werden.");
      } else {
        hint.append("SQL-Fehler in doChange(): ").append(e.toString()).append('\n').append(debug);
      }
    }

    return htmlPage(res, hint.toString(), session, navTyp);
  }

  /**
   * The Method <code>doDeleteResponse</code> is called after delete is selected, the user has to select delete again to make it
   * real.
   *
   * @param req HttpServletRequest from the Servlet
   * @param res HttpServletResponse from the Servlet
   * @param session HttpSession
   * @param navTyp Navigation type
   * @return HTML code of the formular
   * @exception TextException text of the error
   */
  protected String doDeleteResponse(final HttpServletRequest req, final HttpServletResponse res, final HttpSession session,
      final String navTyp) throws TextException {
    return preparePage(req, res, "Bitte Löschung bestätigen", session, navTyp);
  }

  /**
   * The Method <code>doDelete</code> deletes the database Entry, on DateField depending databases, the DateField is changed.
   *
   * @param req HttpServletRequest from the Servlet
   * @param res HttpServletResponse from the Servlet
   * @param session HttpSession
   * @param navTyp Navigation type
   * @return HTML code of the formular
   * @exception TextException text of the error
   */
  protected String doDelete(final HttpServletRequest req, final HttpServletResponse res, final HttpSession session,
      final String navTyp) throws TextException {
    String hint = "Löschung wurde durchgeführt";
    final String dbnumber = (String) session.getAttribute(servletName + dataBaseTable + "." + keyField);

    try {
      final Integer mandatorInteger = (Integer) session.getAttribute(servletName + Constants.DB_FIELD_GLOBAL_MANDATOR);
      if (positionDataBaseTable != null && positionUseDateSelect) {
        try (final PreparedStatement preparedStatement = myDataBase.prepareStatement(updateInvalidateallPositionsSql)) {
          preparedStatement.clearParameters();
          preparedStatement.setInt(1, mandatorInteger == null ? 1 : mandatorInteger.intValue());
          preparedStatement.setString(2, dbnumber);
          preparedStatement.executeUpdate();
        }
      }
      if (useDateSelect) {
        try (final PreparedStatement preparedStatement = myDataBase.prepareStatement(updateInvalidateEntrySql)) {
          preparedStatement.clearParameters();
          if (useUser) {
            preparedStatement.setString(1,
                (String) session.getAttribute(servletName + Constants.DB_FIELD_GLOBAL_USER + Constants.DB_FIELD_GLOBAL_USER));
            preparedStatement.setInt(2, mandatorInteger == null ? 1 : mandatorInteger.intValue());
            preparedStatement.setString(3, dbnumber);
          } else {
            preparedStatement.setInt(1, mandatorInteger == null ? 1 : mandatorInteger.intValue());
            preparedStatement.setString(2, dbnumber);
          }
          preparedStatement.executeUpdate();
        }
      } else {
        try (final PreparedStatement preparedStatement = myDataBase.prepareStatement(deleteEntrySql)) {
          preparedStatement.clearParameters();
          preparedStatement.setInt(1, mandatorInteger == null ? 1 : mandatorInteger.intValue());
          preparedStatement.setString(2, dbnumber);
          preparedStatement.executeUpdate();
        }
      }
      fillMinMax(session);
      previousDbNumber(session);
    } catch (final java.sql.SQLException e) {
      // Um die Fehlerbehandlung nicht zweimal zu machen, schicken
      // wir die Meldung einfach weiter
      hint = "SQL-Fehler in doDelete()\n" + e.toString();
    }

    return preparePage(req, res, hint, session, navTyp);
  }

  /**
   * The Method <code>headerlinePosition</code> creates a html table header file for positions.
   *
   * @param session HttpSession
   * @return html-table headerline for the positions
   */
  protected String headerlinePosition(final HttpSession session) {
    final StringBuilder sb = new StringBuilder(256);
    sb.append("                <tr>\n");
    if (dbFields != null) {
      for (int i = 0; i < positionDbFields.length; i++) {
        if (i != positionNumberReferenceField && positionDbFields[i].getfieldname() != null
            && positionDbFields[i].getEnchentment() != null) {

          sb.append("                    <th class=\"border\">" + StringToHtml.convert(positionDbFields[i].getEnchentment())
              + "</th>\n");
        }
      }
    }
    if (allowedToChange(session)) {
      if (positionAddRemove) {
        sb.append("                    <th class=\"border\">Funktion</th>\n");
      }
      if (positionUpDown) {
        sb.append("                    <th class=\"border\">Pos</th>\n");
      }
    }
    sb.append("                </tr>\n");
    return sb.toString();
  }

  /**
   * The Method <code>inputlinePosition</code> creates a table line with the input fields for each entry.
   *
   * @param session HttpSession
   * @param lineNumber number of the line to create
   * @param numberLines total number of inputlines
   * @param tabposInit position for tabulator
   * @return html-table headerline for the positions
   */
  protected String inputlinePosition(final HttpSession session, final int lineNumber, final int numberLines,
      final int tabposInit) {
    if (lineNumber >= numberLines && !allowedToChange(session)) {
      return StringUtils.EMPTY;
    }
    int tabpos = tabposInit;

    String selectedField = (String) session.getAttribute(servletName + AbstractVisualDb.SELECTED_FIELD_TEXT + serviceName);
    String firstInputField = null;
    final String type = lineNumber < numberLines ? "save" : "Insert " + Integer.toString(lineNumber);
    final StringBuilder sb = new StringBuilder(128);
    sb.append("                <tr>\n");
    for (int i = 0; i < positionDbFields.length; i++) {
      if (i != positionNumberReferenceField && positionDbFields[i].getfieldname() != null
          && positionDbFields[i].getEnchentment() != null) {
        if (!allowedToChange(session) || i == positionNumberKeyField && "save".equals(type)) {
          sb.append(positionDbFields[i].htmlViewTableSegment(session, lineNumber));
        } else {
          sb.append(positionDbFields[i].htmlInputTableSegment(tabpos++, session, lineNumber, type));
          if (selectedField == null && !positionDbFields[i].checkField(session, lineNumber)) {
            selectedField = positionDbFields[i].getfieldname(lineNumber);
          }
          if (firstInputField == null) {
            firstInputField = positionDbFields[i].getfieldname(lineNumber);
          }
        }
      }
    }

    // set first input field in the last line as default, if no other is set
    if (!"save".equals(type) && selectedField == null) {
      selectedField = firstInputField;
    }

    if (allowedToChange(session)) {
      if (positionAddRemove) {
        if (lineNumber < numberLines) {
          sb.append("                    <td class=\"bordercenter\">\n"
              + "                        <button type=\"submit\" name=\"navsubmittype\" " + "value=\"Delete "
              + Integer.toString(lineNumber)
              + "\" title=\"L&ouml;schen\" onClick=\"document.forms[0].buttonpress.value='Delete "
              + Integer.toString(lineNumber) + "';return confirm('Wirklich entfernen?');\"><img src=\"" + pathPrefix
              + Constants.GIF_URL + "16x16/trash.png\" srcset=\"" + pathPrefix + Constants.GIF_URL + "32x32/trash.png 2x\""
              + " width=\"16\" height=\"16\" alt=\"L&ouml;schen\" " + "title=\"L&ouml;schen\"></button>\n"
              + "                    </td>\n");
        } else {
          sb.append("                    <td class=\"bordercenter\">\n"
              + "                        <button type=\"submit\" name=\"navsubmittype\" value=\"" + type
              + "\" title=\"Einf&uuml;gen\" onClick=\"document.forms[0].buttonpress.value='" + type + "';\"><img src=\""
              + pathPrefix + Constants.GIF_URL + "16x16/add.png\" srcset=\"" + pathPrefix + Constants.GIF_URL
              + "32x32/add.png 2x\"" + " width=\"16\" height=\"16\" alt=\"Einf&uuml;gen\" "
              + "title=\"Einf&uuml;gen\"></button>\n" + "                    </td>\n");
        }
      }
      if (positionUpDown) {
        sb.append("                    <td class=\"bordercenter\">\n");
        if (lineNumber > 0 && lineNumber < numberLines) {
          sb.append("                        <button type=\"submit\" name=\"navsubmittype\"" + " value=\"^ "
              + Integer.toString(lineNumber) + "\" onClick=\"document.forms[0].buttonpress.value='^ "
              + Integer.toString(lineNumber) + "';\" title=\"rauf\"><img src=\"" + pathPrefix + Constants.GIF_URL
              + "16x16/1uparrow.png\" srcset=\"" + pathPrefix + Constants.GIF_URL + "32x32/1uparrow.png 2x\""
              + " width=\"16\" height=\"16\" alt=\"rauf\" " + "title=\"rauf\"></button>\n");
        }
        if (lineNumber + 1 < numberLines) {
          sb.append("                        <button type=\"submit\" name=\"navsubmittype\"" + " value=\"v "
              + Integer.toString(lineNumber) + "\" onClick=\"document.forms[0].buttonpress.value='v "
              + Integer.toString(lineNumber) + "';\" title=\"runter\"><img src=\"" + pathPrefix + Constants.GIF_URL
              + "16x16/1downarrow.png\" srcset=\"" + pathPrefix + Constants.GIF_URL + "32x32/1downarrow.png 2x\""
              + " width=\"16\" height=\"16\" alt=\"runter\" " + "title=\"runter\"></button>\n");
        }
        sb.append("                    </td>\n");
      }
    } else {
      if (positionAddRemove) {
        sb.append("                    <td class=\"bordercenter\">&nbsp;</td>\n");
      }
      if (positionUpDown) {
        sb.append("                    <td class=\"bordercenter\">&nbsp;</td>\n");
      }
    }
    session.setAttribute(servletName + AbstractVisualDb.SELECTED_FIELD_TEXT + serviceName, selectedField);
    sb.append("                </tr>\n");
    return sb.toString();
  }

  /**
   * The Method <code>doDeletePosition</code> deletes on of the position items in the database.
   *
   * @param req HttpServletRequest from the Servlet
   * @param session HttpSession
   * @param entry Keyfield of the entry to read data from
   * @return delete ok (true/false)
   */
  protected boolean doDeletePosition(final HttpServletRequest req, final HttpSession session, final int entry) {
    boolean allok = true;
    final String dbnumber = (String) session.getAttribute(servletName + dataBaseTable + "." + keyField);

    try {
      final Integer MandatorInteger = (Integer) session.getAttribute(servletName + Constants.DB_FIELD_GLOBAL_MANDATOR);
      if (positionUseDateSelect) {
        try (final PreparedStatement preparedStatement = myDataBase.prepareStatement(updateInvalidatePositionSql)) {
          preparedStatement.clearParameters();
          preparedStatement.setInt(1, MandatorInteger == null ? 1 : MandatorInteger.intValue());
          preparedStatement.setString(2, positionDbFields[positionNumberKeyField].getContents(session, entry));
          preparedStatement.setString(3, dbnumber);
          preparedStatement.executeUpdate();
        }
      } else {
        try (final PreparedStatement preparedStatement = myDataBase.prepareStatement(deletePositionSql)) {
          preparedStatement.clearParameters();
          preparedStatement.setInt(1, MandatorInteger == null ? 1 : MandatorInteger.intValue());
          preparedStatement.setString(2, positionDbFields[positionNumberKeyField].getContents(session, entry));
          preparedStatement.setString(3, dbnumber);
          preparedStatement.executeUpdate();
        }
      }
      session.removeAttribute(servletName + AbstractVisualDb.NUMBER_POS_TEXT + serviceName);
    } catch (final java.sql.SQLException e) {
      allok = false;
      session.setAttribute(servletName + AbstractVisualDb.DEBUG_TEXT_TEXT + serviceName,
          "SQL-Fehler:\n" + e.toString() + "\nbei doDeletePosition");
    }
    return allok;
  }

  /**
   * The Method <code>doAddPosition</code> adds a new Item to the database, no implementation here, has to be done in child
   * class, if needed.
   *
   * @param req HttpServletRequest from the Servlet
   * @param session HttpSession
   * @param entry Name of the entry to read data from
   * @return replace ok (true/false)
   */
  protected boolean doAddPosition(final HttpServletRequest req, final HttpSession session, final int entry) {
    boolean allok = true;
    int posI;
    int posJ;

    if (req != null) {
      for (posI = 0; posI < positionDbFields.length; posI++) {
        if (positionDbFields[posI].getfieldname() != null && positionDbFields[posI].getEnchentment() != null) {
          positionDbFields[posI].htmlRead(req, session, entry);
        }
      }
      allok = checkPositionValues(req, session, entry);
    }

    try {
      if (allok) {
        final Integer MandatorInteger = (Integer) session.getAttribute(servletName + Constants.DB_FIELD_GLOBAL_MANDATOR);
        try (final PreparedStatement preparedStatement = myDataBase.prepareStatement(insertPositionSql)) {
          preparedStatement.clearParameters();
          preparedStatement.setInt(1, MandatorInteger == null ? 1 : MandatorInteger.intValue());
          posJ = 2;
          for (posI = 0; posI < positionDbFields.length; posI++) {
            if (!(positionDbFields[posI].getdbfieldname() == null
                || positionDbFields[posI].getdbfieldname().equals(Constants.DB_FIELD_GLOBAL_DATE_FROM)
                || positionDbFields[posI].getdbfieldname().equals(Constants.DB_FIELD_GLOBAL_DATE_TO))) {
              if (positionDbFields[posI].getdbfieldname().equals(Constants.DB_FIELD_GLOBAL_USER)
                  && (positionDbFields[posI].getContents(session, entry) == null //
                      || positionDbFields[posI].getContents(session, entry).equals(""))) {
                preparedStatement.setString(posJ++, (String) session
                    .getAttribute(servletName + Constants.DB_FIELD_GLOBAL_USER + Constants.DB_FIELD_GLOBAL_USER));
              } else {
                posJ = positionDbFields[posI].sqlPrepareItems(preparedStatement, myDataBase, posJ, session, entry);
              }
            }
          }
          // session.setAttribute(Servletname + DebugText_text + ServiceName, "Position
          // eingefügt:\n" + SQLString);
          preparedStatement.executeUpdate();
        }
        session.removeAttribute(servletName + AbstractVisualDb.NUMBER_POS_TEXT + serviceName);
      }
    } catch (final java.sql.SQLException e) {
      // Um die Fehlerbehandlung nicht zweimal zu machen, schicken
      // wir die Meldung einfach weiter
      allok = false;
      session.setAttribute(servletName + AbstractVisualDb.DEBUG_TEXT_TEXT + serviceName,
          "SQL-Fehler:\n" + e.toString() + "\nbei doAddPosition");
    }

    return allok;
  }

  /**
   * The Method <code>doReplacePosition</code> replaces a Item in the database.
   *
   * @param req HttpServletRequest from the Servlet
   * @param session HttpSession
   * @param entry Name of the entry
   * @return insert ok (true/false)
   */
  protected boolean doReplacePosition(final HttpServletRequest req, final HttpSession session, final int entry) {
    boolean allok = false;
    if (positionUseDateSelect) {
      allok = doDeletePosition(req, session, entry);
      if (allok) {
        allok = doAddPosition(req, session, entry);
      } else if (session.getAttribute(servletName + AbstractVisualDb.DEBUG_TEXT_TEXT + serviceName) == null) {
        session.setAttribute(servletName + AbstractVisualDb.DEBUG_TEXT_TEXT + serviceName,
            "Konnte alte Position nicht entfernen in Zeile: " + Integer.toString(entry));
      }
    } else {
      final String dbnumber = (String) session.getAttribute(servletName + dataBaseTable + "." + keyField);
      try {
        int pos = 1;
        try (final PreparedStatement preparedStatement = myDataBase.prepareStatement(updatePositionSql)) {
          preparedStatement.clearParameters();
          for (int i = 0; i < positionDbFields.length; i++) {
            if (req != null && positionDbFields[i].getfieldname() != null && positionDbFields[i].getEnchentment() != null) {
              positionDbFields[i].htmlRead(req, session, entry);
            }
            if (i != positionNumberKeyField && i != positionNumberReferenceField) {
              if (positionDbFields[i].getdbfieldname() != null
                  && positionDbFields[i].getdbfieldname().equals(Constants.DB_FIELD_GLOBAL_USER)
                  && (positionDbFields[i].getContents(session) == null //
                      || positionDbFields[i].getContents(session).equals(StringUtils.EMPTY))) {
                preparedStatement.setString(pos++, (String) session
                    .getAttribute(servletName + Constants.DB_FIELD_GLOBAL_USER + Constants.DB_FIELD_GLOBAL_USER));
              } else {
                pos = positionDbFields[i].sqlPrepareItems(preparedStatement, myDataBase, pos, session, entry);
              }
            }
          }
          final Integer mandatorInteger = (Integer) session.getAttribute(servletName + Constants.DB_FIELD_GLOBAL_MANDATOR);
          preparedStatement.setInt(pos++, mandatorInteger == null ? 1 : mandatorInteger.intValue());
          preparedStatement.setString(pos++, positionDbFields[positionNumberKeyField].getContents(session, entry));
          preparedStatement.setString(pos++, dbnumber);
          if (preparedStatement.executeUpdate() > 0) {
            allok = true;
          }
        }
      } catch (final java.sql.SQLException e) {
        // Um die Fehlerbehandlung nicht zweimal zu machen, schicken
        // wir die Meldung einfach weiter
        allok = false;
        session.setAttribute(servletName + AbstractVisualDb.DEBUG_TEXT_TEXT + serviceName,
            "SQL-Fehler:\n" + e.toString() + "\nbei doReplacePosition");
      }
    }

    return allok;
  }

  /**
   * The Method <code>checkPositionHasChanged</code> checks out if the position has changed or not.
   *
   * @param req HttpServletRequest from the Servlet
   * @param session HttpSession
   * @param entry Name of the entry
   * @return has changed (true/false)
   */
  protected boolean checkPositionHasChanged(final HttpServletRequest req, final HttpSession session, final int entry) {
    boolean hasChanged = false;
    String dbString = null;
    String htmlString = null;

    if (allowedToChange(session)) {
      try {
        final String dbnumber = (String) session.getAttribute(servletName + dataBaseTable + "." + keyField);
        final Integer mandatorInteger = (Integer) session.getAttribute(servletName + Constants.DB_FIELD_GLOBAL_MANDATOR);
        try (final PreparedStatement preparedStatement = myDataBase.prepareStatement(readPositionSql)) {
          preparedStatement.clearParameters();
          preparedStatement.setInt(1, mandatorInteger == null ? 1 : mandatorInteger.intValue());
          preparedStatement.setString(2, dbnumber);
          preparedStatement.setString(3, positionDbFields[positionNumberKeyField].getContents(session, entry));
          try (final ResultSet result = preparedStatement.executeQuery()) {
            if (result.next()) {
              for (int i = 0; i < positionDbFields.length && !hasChanged; i++) {
                if (i != positionNumberReferenceField && positionDbFields[i].getdbfieldname() != null
                    && positionDbFields[i].getfieldname() != null) {
                  positionDbFields[i].htmlRead(req, session, entry);
                  htmlString = positionDbFields[i].getContents(session, entry);
                  positionDbFields[i].sqlRead(result, session, entry);
                  dbString = positionDbFields[i].getContents(session, entry);
                  hasChanged |= !StringUtils.equals(StringUtils.defaultString(htmlString), StringUtils.defaultString(dbString));
                }
              }
            } else {
              hasChanged = true;
            }
          }
        }
      } catch (final java.sql.SQLException e) {
        // should not happen
        session.setAttribute(servletName + AbstractVisualDb.DEBUG_TEXT_TEXT + serviceName,
            "SQL-Fehler:\n" + e.toString() + "\nbei checkPositionHasChanged");
      }
    }

    return hasChanged;
  }

  /**
   * The Method <code>doChangePositions</code> changes the position of to Items in the database.
   *
   * @param req HttpServletRequest from the Servlet
   * @param session HttpSession
   * @param entryA Name of the first entry
   * @param entryB Name of the second entry
   * @return insert ok (true/false)
   */
  protected boolean doChangePositions(final HttpServletRequest req, final HttpSession session, final int entryA,
      final int entryB) {
    boolean allok = true;

    // delete old entries
    allok = doDeletePosition(req, session, entryA) & doDeletePosition(req, session, entryB);
    if (allok) {
      // remember Keyfields of both entries
      final String keyFieldA = positionDbFields[positionNumberKeyField].getContents(session, entryA);
      final String keyFieldB = positionDbFields[positionNumberKeyField].getContents(session, entryB);
      // switch Keyfields between entry A and B
      positionDbFields[positionNumberKeyField].setField(session, keyFieldB, entryA);
      positionDbFields[positionNumberKeyField].setField(session, keyFieldA, entryB);
      // save the new entries
      allok &= doAddPosition(null, session, entryA);
      allok &= doAddPosition(null, session, entryB);
    }

    return allok;
  }

  /**
   * The Method <code>doChangePositionsUp</code> changes the position of to Items in the database on position up.
   *
   * @param req HttpServletRequest from the Servlet
   * @param session HttpSession
   * @param entry Name of the current Entry
   * @return insert ok (true/false)
   */
  protected boolean doChangePositionsUp(final HttpServletRequest req, final HttpSession session, final int entry) {
    return doChangePositions(req, session, entry - 1, entry);
  }

  /**
   * The Method <code>doChangePositionsDown</code> changes the position of to Items in the database on position down.
   *
   * @param req HttpServletRequest from the Servlet
   * @param session HttpSession
   * @param entry Name of the current Entry
   * @return insert ok (true/false)
   */
  protected boolean doChangePositionsDown(final HttpServletRequest req, final HttpSession session, final int entry) {
    return doChangePositions(req, session, entry, entry + 1);
  }

  /**
   * The Method <code>changeAllPositions</code> changes all positions when necessary.
   *
   * @param req HttpServletRequest from the Servlet
   * @param session HttpSession
   * @return true if one or more positions have changed
   */
  protected boolean changeAllPositions(final HttpServletRequest req, final HttpSession session) {
    boolean lineChange = false;
    final Integer numPosInteger = (Integer) session.getAttribute(servletName + AbstractVisualDb.NUMBER_POS_TEXT + serviceName);
    int numPos = 0;

    if (numPosInteger != null) {
      numPos = numPosInteger.intValue();
    }

    for (int i = 0; i < numPos; i++) {
      if (checkPositionHasChanged(req, session, i)) {
        lineChange = true;
        if (checkPositionValues(req, session, i)) {
          doReplacePosition(req, session, i);
        }
      }
    }
    return lineChange;
  }

  /**
   * The Method <code>checkPositionValues</code> checks out if all the entries of the position are correct. This has to
   * implemented in the child classe, it's allways false here.
   *
   * @param req HttpServletRequest from the Servlet
   * @param session HttpSession
   * @param entry Name of the entry
   * @return are alle entries in this position ok? (true/false)
   */
  protected boolean checkPositionValues(final HttpServletRequest req, final HttpSession session, final int entry) {
    boolean isOk = true;
    final StringBuilder debugText = new StringBuilder(128);
    debugText.append(
        StringUtils.defaultString((String) session.getAttribute(servletName + AbstractVisualDb.DEBUG_TEXT_TEXT + serviceName)));

    for (int j = 0; j < positionDbFields.length; j++) {
      if (positionDbFields[j].getfieldname() != null) {
        final String oldEntry = positionDbFields[j].getContents(session, j);
        if (req != null) {
          positionDbFields[j].htmlRead(req, session, j);
        }
        if (!positionDbFields[j].checkField(session, entry)) {
          isOk = false;
          debugText
              .append("Fehler in Zeile " + Integer.toString(entry + 1) + " bei Feld: " + positionDbFields[j].getEnchentment()
                  + " mit Inhalt: \"" + positionDbFields[j].getContents(session, entry) + "\"\n");
        }
        if (req != null) {
          positionDbFields[j].setField(session, oldEntry, j);
        }
      }
    }

    session.setAttribute(servletName + AbstractVisualDb.DEBUG_TEXT_TEXT + serviceName, debugText.toString());

    return isOk;
  }

  /**
   * The Method <code>doOtherOptions</code> has to be implemented by the child, it contains other functions if needed.
   *
   * @param req HttpServletRequest from the Servlet
   * @param res HttpServletResponse from the Servlet
   * @param session HttpSession
   * @param navTyp Navigation type
   * @return HTML code of the formular
   * @exception TextException text of the error
   */
  protected String doOtherOptions(final HttpServletRequest req, final HttpServletResponse res, final HttpSession session,
      final String navTyp) throws TextException {
    return preparePage(req, res, "Funktion nicht implementiert", session, navTyp);
  }

  /**
   * The Method <code>checkDBKey</code> is used for manuel switching to another entry.
   *
   * @param newDbKey New Database-Key to change to
   * @param session HttpSession
   * @return null if the new database key exists, otherwise the error text
   */
  protected String checkDbKey(final String newDbKey, final HttpSession session) {
    String hint = null;

    try {
      final Integer mandatorInteger = (Integer) session.getAttribute(servletName + Constants.DB_FIELD_GLOBAL_MANDATOR);
      try (final PreparedStatement preparedStatement = myDataBase.prepareStatement(readEntryCountSql)) {
        preparedStatement.clearParameters();
        preparedStatement.setInt(1, mandatorInteger == null ? 1 : mandatorInteger.intValue());
        preparedStatement.setString(2, newDbKey);
        try (final ResultSet result = preparedStatement.executeQuery()) {

          if (result.next()) {
            if (result.getInt("Entries") == 1) {
              session.setAttribute(servletName + dataBaseTable + "." + keyField, newDbKey);
              session.removeAttribute(servletName + AbstractVisualDb.NUMBER_POS_TEXT + serviceName);
            } else {
              hint = "Eingegebener Key: \"" + newDbKey + "\", existiert nicht, alter Inhalt bleibt bestehen";
            }
          } else {
            hint = "DB-Zugriff fehlgeschlagen, konnte nicht zu dem Datensatz \"" + newDbKey + "\" wechseln";
          }
        }
      }
    } catch (final java.sql.SQLException e) {
      // Um die Fehlerbehandlung nicht zweimal zu machen, schicken
      // wir die Meldung einfach weiter
      hint = "SQL-Fehler in checkDBKey()\n" + e.toString();
    }
    return hint;
  }

  /**
   * The Method <code>prepareSearch</code> displays the search window.
   *
   * @param req HttpServletRequest from the Servlet
   * @param res HttpServletResponse from the Servlet
   * @param session HttpSession
   * @return html code of the formular
   */
  protected String prepareSearch(final HttpServletRequest req, final HttpServletResponse res, final HttpSession session) {
    final StringBuilder sb = new StringBuilder(2048);
    sb.append("        <div style=\"text-align:center;\">\n" //
        + "            <form action=\"" + res.encodeURL(pathPrefix + Constants.SERVLET_URL + servletName)
        + "\" method=\"POST\" enctype=\"application/x-www-form-urlencoded; charset=utf-8\" " + "accept-charset=\"utf-8\">\n"
        + "                <input type=\"hidden\" name=\"buttonpress\" value=\"Suche starten\">\n"
        + "                <p>W&auml;hlen Sie das Suchfeld und den Begriff, " + "nach dem Sie suchen:</p>\n"
        + "                <table>\n" + "                    <tr>\n" + "                        <th>Suchfeld</th>\n"
        + "                        <th>Suchbegriff</th>\n" + "                    </tr>\n" + "                    <tr>\n"
        + "                        <td>\n"
        + "                            <select name=\"Searchfield\" size=\"1\" accesskey=\"u\""
        + " tabindex=\"1\" onChange=\"document.forms[0].buttonpress.value='forcesearch';\">\n");

    if (dbFields != null) {
      for (final AbstractBaseField dbField : dbFields) {
        if (dbField.getEnchentment() != null && dbField.getfieldname() != null && dbField.getdbfieldname() != null) {
          sb.append("                                <option value=\"" + StringToHtml.convert(dbField.getdbfieldname()) + "\">"
              + StringToHtml.convert(dbField.getEnchentment()) + "</option>\n");
        }
      }
    }

    sb.append("                            </select>\n" + "                        </td>\n" + "                        <td>\n"
        + "                            <input type=\"text\" name=\"Searchquerry\" size=\"30\""
        + " maxlength=\"50\" accesskey=\"z\" tabindex=\"2\">\n" + "                        </td>\n"
        + "                    </tr>\n" + "                    <tr>\n"
        + "                        <td style=\"text-align:center;\"><input type=\"submit\" "
        + "name=\"navsubmittype\" value=\"Suche starten\" accesskey=\"s\" tabindex=\"3\"></td>"
        + "                        <td style=\"text-align:center;\"><input type=\"submit\" "
        + "name=\"navsubmittype\" value=\"Zur&uuml;ck zur Anzeige\" accesskey=\"z\" tabindex=\"4\""
        + " onClick=\"document.forms[0].buttonpress.value='';\"></td>" + "                    </tr>\n"
        + "                </table>\n" + "            </form>\n" + "        </div>\n");
    session.setAttribute(servletName + AbstractVisualDb.SELECTED_FIELD_TEXT + serviceName, "Searchquerry");
    return sb.toString();
  }

  /**
   * The Method <code>createSearchSQLString</code> is a helpfunction for creating the SQL Search Querry.
   *
   * @return string with SQL querry
   */
  protected String createSearchSqlString() {
    final StringBuilder sb = new StringBuilder();
    if (dbFields != null) {
      for (final AbstractBaseField dbField : dbFields) {
        if (dbField.isComplusion() && dbField.getEnchentment() != null && dbField.getfieldname() != null
            && dbField.getdbfieldname() != null) {
          sb.append(", ").append(dbField.getdbfieldname());
        }
      }
    }
    return sb.toString();
  }

  /**
   * The Method <code>createSearchHeader</code> is a helpfunction for creating HTML header for the search list.
   *
   * @return HTML string with Database-Header-Files
   */
  protected String createSearchHeader() {
    final StringBuilder sb = new StringBuilder();
    if (dbFields != null) {
      for (final AbstractBaseField dbField : dbFields) {
        if (dbField.isComplusion() && dbField.getEnchentment() != null && dbField.getfieldname() != null
            && dbField.getdbfieldname() != null) {
          sb.append("<th>" + StringToHtml.convert(dbField.getEnchentment()) + "</th>");
        }
      }
    }
    return sb.toString();
  }

  /**
   * The Method <code>readSearchResult</code> is a helpfunction for creating search results as HTML string
   *
   * @return HTML string with search results
   * @exception java.sql.SQLException
   */
  protected String readSearchResult(final ResultSet result) throws java.sql.SQLException {
    final StringBuilder sb = new StringBuilder();
    if (dbFields != null) {
      for (final AbstractBaseField dbField : dbFields) {
        if (dbField.isComplusion() && dbField.getEnchentment() != null && dbField.getfieldname() != null
            && dbField.getdbfieldname() != null) {
          sb.append("<td>" + StringToHtml.convert(result.getString(dbField.getdbfieldname())) + "</td>");
        }
      }
    }
    return sb.toString();
  }

  /**
   * The Method <code>doSearch</code> displays the search window.
   *
   * @param req HttpServletRequest from the Servlet
   * @param res HttpServletResponse from the Servlet
   * @param session HttpSession
   * @return html code of the formular
   * @exception TextException when a error in checking the passwords occours
   */
  protected String doSearch(final HttpServletRequest req, final HttpServletResponse res, final HttpSession session)
      throws TextException {
    final String searchField = req.getParameter("Searchfield");
    final String searchQuerry = req.getParameter("Searchquerry");
    final Integer mandatorInteger = (Integer) session.getAttribute(servletName + Constants.DB_FIELD_GLOBAL_MANDATOR);
    final StringBuilder sqlString = new StringBuilder(80);

    int read;
    String accessKey;

    final StringBuilder sb = new StringBuilder(1024);
    sb.append("        <div style=\"text-align:center;\">\n" + "            <h1>Suchergebnis</h1>\n");

    try {
      sqlString.append("SELECT " + keyField + " AS dbnumber " + createSearchSqlString() + " " + "FROM " + dataBaseTable
          + " WHERE " + Constants.DB_FIELD_GLOBAL_MANDATOR + " = "
          + java.lang.Integer.toString(mandatorInteger == null ? 1 : mandatorInteger.intValue()) + " AND  "
          + de.knightsoft.common.StringToSql.searchString(searchField, searchQuerry,
              myDataBase.getMetaData().getDatabaseProductName()));
      if (useDateSelect) {
        sqlString.append(" AND  " + Constants.DB_FIELD_GLOBAL_DATE_FROM + " <= " + myDataBaseDepending.getSqlTimeNow() + " "
            + " AND  " + Constants.DB_FIELD_GLOBAL_DATE_TO + "   >= " + myDataBaseDepending.getSqlTimeNow());
      }

      try (final ResultSet result = myDataBase.createStatement().executeQuery(sqlString.toString())) {
        read = 0;
        while (result.next()) {
          if (read < 9) {
            accessKey = " accesskey=\"" + (read + 1) + "\"";
            if (read == 0) {
              sb.append("            <table>\n" + "                <tr>" + createSearchHeader() + "<th>Auswahl</th></tr>\n");
            }
          } else {
            accessKey = StringUtils.EMPTY;
          }

          sb.append("                <tr>" + readSearchResult(result) + "\n" + "                    <td>\n"
              + "                        <form action=\"" + res.encodeURL(pathPrefix + Constants.SERVLET_URL + servletName)
              + "\" method=\"POST\" enctype=\"application/x-www-form-urlencoded; charset=utf-8\" "
              + "accept-charset=\"utf-8\">\n" + "                            <input type=\"hidden\" name=\"dbnumber\" value=\""
              + StringToHtml.convert(result.getString("dbnumber")) + "\">\n"
              + "                            <input type=\"submit\" name=\"navsubmittype\" "
              + "value=\"Suchresultat &uuml;bernehmen\" tabindex=\"" + Integer.toString(read + 1) + "\"" + accessKey + ">\n"
              + "                        </form>\n" + "                    </td>\n" + "                </tr>\n");
          read++;
        }
      }
      if (read == 0) {
        sb.append("                <h2>Es wurde nichts gefunden</h2>\n");
      } else {
        sb.append("            </table>\n");
      }
    } catch (final java.sql.SQLException e) {
      // Um die Fehlerbehandlung nicht zweimal zu machen, schicken
      // wir die Meldung einfach weiter
      sb.append("            SQL-Fehler in doSearch()\n" + e.toString() + "\n" + sqlString + "\n");
    }

    sb.append("            <form action=\"" + res.encodeURL(pathPrefix + Constants.SERVLET_URL + servletName)
        + "\" method=\"POST\" enctype=\"application/x-www-form-urlencoded; charset=utf-8\" " + "accept-charset=\"utf-8\">\n"
        + "                <p><input type=\"submit\" name=\"navsubmittype\" "
        + "value=\"Zur&uuml;ck zur Anzeige\" accesskey=\"z\"></p>" + "            </form>\n" + "        </div>\n");

    return sb.toString();
  }

  /**
   * The Method <code>doTheWork</code> coordinates all the work in this class.
   *
   * @param req HttpServletRequest from the Servlet
   * @param res HttpServletResponse from the Servlet
   * @param session HttpSession
   * @return html code of the formular
   * @exception TextException when a error in checking the passwords occours
   */
  public String doTheWork(final HttpServletRequest req, final HttpServletResponse res, final HttpSession session)
      throws TextException {
    String returnValue = null;
    final String buttonpress = req.getParameter("buttonpress");
    final String doformcheck = req.getParameter("doformcheck");
    final String dbmin = (String) session.getAttribute(servletName + AbstractVisualDb.DBMIN_TEXT + serviceName);
    final String dbmax = (String) session.getAttribute(servletName + AbstractVisualDb.DBMAX_TEXT + serviceName);
    String dbnumber = (String) session.getAttribute(servletName + dataBaseTable + "." + keyField);
    boolean lineChange = false;
    String navTyp = req.getParameter("navsubmittype");

    session.removeAttribute(servletName + AbstractVisualDb.SELECTED_FIELD_TEXT + serviceName);

    session.setAttribute(servletName + AbstractVisualDb.REQ_SAVE_TEXT + keyField, req);

    if (dbmin == null) {
      dbnumber = StringUtils.EMPTY;
      try {
        fillMinMax(session);
      } catch (final java.sql.SQLException e) {
        // shouldn't happen
        e.printStackTrace();
      }
    }

    // session.setAttribute(Servletname + DebugText_text + ServiceName,
    // "Inhalt doformcheck = \"" + doformcheck + "\"\n"
    // + "Inhalt buttonpress = \"" + buttonpress + "\"\n"
    // + "Inhalt navsubmittype = \"" + NavTyp + "\"\n"
    // + "Inhalt numberpositions = \"" + nrLinesStr + "\"\n"
    // + "Inhalt dbnumber = \"" + dbnumber + "\"\n"
    // + "Inhalt dbnumber (session) = \"" + (String)session.getAttribute(Servletname + DataBaseTable
    // + "." + KeyField) +
    // "\"\n");
    if (StringUtils.isEmpty(buttonpress)) {
      if ("true".equals(doformcheck)) {
        navTyp = "save";
      }
    } else {
      navTyp = buttonpress;
    }

    if (navTyp == null) {
      if (StringUtils.isEmpty(dbnumber)) {
        readDbNumber(session);
      }
      // neu in der Anwendung, Positionen immer neu aufbauen.
      session.removeAttribute(servletName + AbstractVisualDb.NUMBER_POS_TEXT + serviceName);
      returnValue = preparePage(req, res, null, session, navTyp);
    } else if ("save".equals(navTyp) || Boolean.parseBoolean(navTyp)) {
      if (positionDataBaseTable != null) {
        lineChange = changeAllPositions(req, session);
      }
      if (Boolean.parseBoolean(doformcheck) || StringUtils.isEmpty(dbnumber)) {
        returnValue = doChange(req, res, session, navTyp);
      } else {
        if (lineChange) {
          returnValue = preparePage(req, res, "Positionen wurden geändert.", session, navTyp);
        } else {
          returnValue = preparePage(req, res, "Keine Änderungen, da keine Eingaben", session, navTyp);
        }
      }
    } else if ("delete".equals(navTyp)) {
      returnValue = doDeleteResponse(req, res, session, navTyp);
    } else if ("delete confirm".equals(navTyp)) {
      returnValue = doDelete(req, res, session, navTyp);
    } else if ("goto".equals(navTyp)) {
      returnValue = preparePage(req, res, checkDbKey(req.getParameter("dbnumber_go"), session), session, navTyp);
    } else if ("search".equals(navTyp)) {
      returnValue = prepareSearch(req, res, session);
    } else if ("forcesearch".equals(navTyp) || "Suche starten".equals(navTyp)) {
      returnValue = doSearch(req, res, session);
    } else if ("Suchresultat &uuml;bernehmen".equals(navTyp) || "Suchresultat übernehmen".equals(navTyp)) {
      dbnumber = req.getParameter("dbnumber");
      setDbNumber(session, dbnumber);
      returnValue = preparePage(req, res, null, session, navTyp);
    } else if ("fback".equals(navTyp)) {
      setDbNumber(session, dbmin);
      returnValue = preparePage(req, res, null, session, navTyp);
    } else if ("back".equals(navTyp)) {
      previousDbNumber(session);
      returnValue = preparePage(req, res, null, session, navTyp);
    } else if ("next".equals(navTyp)) {
      nextDbNumber(session);
      returnValue = preparePage(req, res, null, session, navTyp);
    } else if ("fnext".equals(navTyp)) {
      setDbNumber(session, dbmax);
      returnValue = preparePage(req, res, null, session, navTyp);
    } else if ("new".equals(navTyp)) {
      setDbNumber(session, null);
      returnValue = preparePage(req, res, null, session, navTyp);
    } else if (navTyp.length() > 7 && navTyp.substring(0, 7).equals("Delete ")) {
      if (doDeletePosition(req, session, Integer.parseInt(navTyp.substring(7)))) {
        returnValue = preparePage(req, res, "Position wurde gelöscht", session, navTyp);
      } else {
        returnValue = preparePage(req, res, "Position konnte nicht gelöscht werden", session, navTyp);
      }
    } else if (navTyp.length() > 7 && navTyp.substring(0, 7).equals("Insert ")) {
      if (doAddPosition(req, session, Integer.parseInt(navTyp.substring(7)))) {
        returnValue = preparePage(req, res, "Position wurde eingefügt", session, navTyp);
      } else {
        returnValue = preparePage(req, res, "Position konnte nicht eingefügt werden " + navTyp.substring(7), session, navTyp);
      }
    } else if (navTyp.length() > 2 && navTyp.substring(0, 2).equals("^ ")) {
      if (doChangePositionsUp(req, session, Integer.parseInt(navTyp.substring(2)))) {
        returnValue = preparePage(req, res, "Position wurde verschoben", session, navTyp);
      } else {
        returnValue = preparePage(req, res, "Position konnte nicht verschoben werden", session, navTyp);
      }
    } else if (navTyp.length() > 2 && navTyp.substring(0, 2).equals("v ")) {
      if (doChangePositionsDown(req, session, Integer.parseInt(navTyp.substring(2)))) {
        returnValue = preparePage(req, res, "Position wurde verschoben", session, navTyp);
      } else {
        returnValue = preparePage(req, res, "Position konnte nicht verschoben werden", session, navTyp);
      }
    } else if (otherOptions != null && navTyp.equals(otherOptions)) {
      returnValue = doOtherOptions(req, res, session, navTyp);
    } else {
      returnValue = preparePage(req, res, null, session, navTyp);
    }

    // if (dbnumber == null)
    // session.removeKeyField(DataBaseTable + "." + KeyField);
    // else
    // session.addKeyField(DataBaseTable + "." + KeyField, dbnumber);

    return returnValue;
  }

  /**
   * The Method <code>checkNumericField</code> checks if the value is correct for nummeric fields.
   *
   * @param value the value to check
   * @param complusion must be filed or not
   * @return true if everything is ok
   */
  protected boolean checkNumericField(final int value, final boolean complusion) {
    return !(complusion && value == 0);
  }

  /**
   * The Method <code>checkNumericField</code> checks if the value is correct for nummeric fields.
   *
   * @param value the value to check
   * @param complusion must be filed or not
   * @return true if everything is ok
   */
  protected boolean checkNumericField(final String value, final boolean complusion) {
    boolean isok = false;
    if (!(complusion && (StringUtils.isEmpty(value) || "0".equals(value)))) {
      isok = true;
      if (value != null) {
        for (int j = 0; j < value.length() && isok; j++) {
          isok = java.lang.Character.isDigit(value.charAt(j)) || value.charAt(j) == '.';
        }
      }
    }
    return isok;
  }

  /**
   * The Method <code>checktext</code> checks if the value is correct for Text fields.
   *
   * @param value the value to check
   * @param complusion must be filed or not
   * @return true if everything is ok
   */
  protected boolean checktext(final String value, final boolean complusion) {
    return !(complusion && (value == null || value.equals(StringUtils.EMPTY)));
  }

  /**
   * The Method <code>checkRegexField</code> checks if the value is correct for Regex fields.
   *
   * @param value the value to check
   * @param complusion must be filed or not
   * @return true if everything is ok
   */
  protected boolean checkRegexField(final String value, final boolean complusion) {
    boolean isok = true;
    if (value == null || value.equals(StringUtils.EMPTY)) {
      isok = !complusion;
    } else {
      try {
        AbstractVisualDb.HTML_OLD.matches(value);
      } catch (final java.util.regex.PatternSyntaxException e) {
        isok = false;
      }

    }
    return isok;
  }
}
