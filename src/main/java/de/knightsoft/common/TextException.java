/*
 * Copyright (C) 2002-2020 Manfred Tremmel
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package de.knightsoft.common;

/**
 * The <code>TextException</code> class contains a simple class to throw a text message.
 *
 * @author Manfred Tremmel
 * @version 1.0.3, 06.08.2005
 */
public class TextException extends Exception {

  private static final long serialVersionUID = -6676084084281557855L;
  private final String reason;

  /**
   * Constructor.
   *
   * @param preason error message
   */
  public TextException(final String preason) {
    super();
    reason = preason;
  }

  /**
   * Constructor.
   *
   * @param reason error message
   * @param exception the exception which triggered the problem
   */
  public TextException(final String reason, final Exception exception) {
    super(exception);
    this.reason = reason;
  }

  /**
   * <code>getMessage</code> returns the value as string.
   *
   * @return entry as string
   */
  @Override
  public String getMessage() {
    return reason;
  }

  /**
   * <code>toString</code> returns the value as string.
   *
   * @return entry as string
   */
  @Override
  public String toString() {
    return reason;
  }

  /**
   * <code>toHTML</code> returns the value as html string.
   *
   * @return entry as html string
   */
  public String toHtml() {
    return de.knightsoft.common.StringToHtml.convert(reason);
  }
}
