package de.knightsoft.common;

import org.junit.Assert;
import org.junit.Test;

import java.text.NumberFormat;
import java.util.Locale;


public class StringToHtmlTest {

  @Test
  public void testUmlautConvert() {
    // given
    final String source = "äöüÄÖÜß";

    // when
    final String target = StringToHtml.convert(source);

    // then
    Assert.assertEquals("&auml;&ouml;&uuml;&Auml;&Ouml;&Uuml;&szlig;", target);
  }

  @Test
  public void testMoneyConvert() {
    // given
    final NumberFormat moneyFormat = NumberFormat.getCurrencyInstance(Locale.GERMANY);
    final String source = moneyFormat.format(12.34);

    // when
    final String target = StringToHtml.convert(source, true);

    // then
    Assert.assertEquals("12,34&nbsp;&euro;", target);
  }

  @Test
  public void testPercentConvert() {
    // given
    final NumberFormat percentFormat = NumberFormat.getPercentInstance(java.util.Locale.GERMAN);
    final String source = percentFormat.format(0.666666666);

    // when
    final String target = StringToHtml.convert(source, true);

    // then
    Assert.assertEquals("67&nbsp;%", target);
  }
}
