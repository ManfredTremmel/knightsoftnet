package de.knightsoft.common;

import static org.junit.Assert.assertNotNull;

import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;


public class SendEMailTest {

  @Ignore
  @Test
  public void testSendToIiv() throws IOException {
    // given
    final String from = "info@knightsoft-net.de";
    final String organization = "KnightSoft oHG";
    final String to = "mtremmel@knightsoft-net.de";
    final String replyTo = null;
    final String subject = "Testmail";
    final String mailtext = "Test text der Mail\nZweite Zeile\n";
    final String smtpServer = "smtp.ionos.de";
    final boolean smtpAuth = true;
    final String smtpUser = "mtremmel@knightsoft-net.de";
    final String smtpPassword = "*************************";
    final int smtpPort = 465;

    // when
    final SendEMail mail = new SendEMail(from, organization, to, replyTo, subject, mailtext, smtpServer, smtpAuth, smtpUser,
        smtpPassword, smtpPort);

    // then
    assertNotNull(mail);
  }
}
